HA$PBExportHeader$w_pas_plant_dddw.srw
forward
global type w_pas_plant_dddw from w_base_sheet_ext
end type
type dw_plant from u_base_dw_ext within w_pas_plant_dddw
end type
end forward

global type w_pas_plant_dddw from w_base_sheet_ext
int Width=970
int Height=585
WindowType WindowType=child!
boolean TitleBar=false
boolean ControlMenu=false
boolean MinBox=false
boolean MaxBox=false
boolean Resizable=false
dw_plant dw_plant
end type
global w_pas_plant_dddw w_pas_plant_dddw

type prototypes
Function UINT GetSystemMetrics(UINT aIndex) Library "user.exe"
end prototypes

type variables
u_base_dw	idw_focus
end variables

forward prototypes
public function long wf_getypos ()
public function long wf_getxpos ()
end prototypes

public function long wf_getypos ();return idw_focus.Y + Long(idw_focus.Object.DataWindow.Header.Height) + &
			((idw_focus.GetRow() - Long(idw_focus.Object.DataWindow.FirstRowOnPage) + 1) * &
			Long(idw_focus.Object.DataWindow.Detail.Height)) + &
			Long(idw_focus.Describe(idw_focus.GetColumnName() + ".Y"))
end function

public function long wf_getxpos ();Boolean	lb_VScrollBar

Double	ld_RowCount, &
			ld_VScrollBarWidth, &
			ld_ScrolledToLeft, &
			ld_Count, &
			ld_ColumnCount, &
			ld_TempMaxScrollWidth, &
			ld_MaxScrollWidth, &
			ld_MaxScrollHPos, &
			ld_CurrentScrollHPos, &
			ld_DW_Width

Environment	le_env


ld_DW_Width = idw_focus.Width
ld_RowCount = idw_focus.RowCount()
If ld_RowCount = 0 Then return 0

ld_ColumnCount = Double(idw_focus.Object.DataWindow.Column.Count)
For ld_Count = 1 to ld_ColumnCount
	ld_TempMaxScrollWidth = Double(idw_focus.Describe("#" + String(ld_Count) + &
				".X")) + Double(idw_focus.Describe("#" + String(ld_Count) + ".Width"))
	If ld_TempMaxScrollWidth > ld_MaxScrollWidth Then 
		ld_MaxScrollWidth = ld_TempMaxScrollWidth
	End if
Next

ld_MaxScrollHPos = Double(idw_focus.Object.DataWindow.HorizontalScrollMaximum)
ld_CurrentScrollHPos = Double(idw_focus.Object.DataWindow.HorizontalScrollPosition)


If Double(idw_focus.Object.DataWindow.FirstRowOnPage) >= 1 Or &
		Double(idw_focus.Object.DataWindow.LastRowOnPage) < ld_RowCount Then
	lb_VScrollBar = idw_focus.VScrollBar
Else
	lb_VScrollBar = False
End if

GetEnvironment(le_env)

If lb_VScrollBar And le_env.Win16 Then
	// Make this multiplatform later.
	// GetSystemMetrics(2) returns the width of the vscrollbar
	ld_VScrollBarWidth = PixelsToUnits(GetSystemMetrics(2), XPixelsToUnits!)
Else
	ld_VScrollBarWidth = 0
End if

If (ld_MaxScrollHPos = 1) Or (ld_MaxScrollWidth < ld_DW_Width) Then
	ld_ScrolledToLeft = 0
Else
	// watch the precedence here!
	ld_ScrolledToLeft = (ld_CurrentScrollHPos * (ld_MaxScrollWidth - ld_DW_Width - &
								ld_VScrollBarWidth)) / ld_MaxScrollHPos
End if

return idw_focus.X + Double(idw_focus.Describe(idw_focus.GetColumnName() + ".X")) - &
				ld_ScrolledToLeft
end function

event open;call super::open;GraphicObject 	which_control
Long				ll_row
String			ls_temp

dw_plant.ImportString(Message.StringParm)
ls_temp = Message.StringParm
Message.StringParm = ""
If dw_plant.RowCount() = 0 Then
	Close(This)
	Return
End If

which_control = GetFocus( ) 
If Not IsValid(which_control) Then
	Close(This)
	Return
End If

CHOOSE CASE TypeOf(which_control)
CASE DataWindow!
	idw_focus = which_control

	ll_row = dw_plant.Find("location_code = '" + idw_focus.GetItemString( &
					idw_focus.GetRow(), idw_focus.GetColumn()) + "'", 1, dw_plant.RowCount())
	If ll_row > 0 Then
		dw_plant.SelectRow(ll_row, True)
		dw_plant.ScrollToRow(ll_row)
	Else
		dw_plant.SelectRow(1, True)
	End if
END CHOOSE

This.Move(wf_GetXPos(), wf_GetYPos())

idw_focus.SetFocus()
end event

on closequery;// comment
end on

on w_pas_plant_dddw.create
int iCurrent
call w_base_sheet_ext::create
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_plant
end on

on w_pas_plant_dddw.destroy
call w_base_sheet_ext::destroy
destroy(this.dw_plant)
end on

type dw_plant from u_base_dw_ext within w_pas_plant_dddw
int X=1
int Y=1
int Width=961
int Height=577
string DataObject="d_pas_plant_dddw"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event clicked;call super::clicked;Long	ll_row
Window	lw_parent


ll_row = row
If ll_row = 0 Then return
Message.StringParm = This.GetItemString(ll_row, "location_code")
If IsValid(idw_focus) Then
	TriggerEvent(idw_focus, "ue_change_plant")
End if
Close(Parent)
end event

on rowfocuschanged;call u_base_dw_ext::rowfocuschanged;This.SelectRow(0, False)
This.SelectRow(This.GetRow(), True)
end on

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

