HA$PBExportHeader$w_pas_instances_inq.srw
$PBExportComments$Instances Inquire Window
forward
global type w_pas_instances_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_pas_instances_inq
end type
type dw_instances_inq from u_base_dw_ext within w_pas_instances_inq
end type
type cbx_hs from checkbox within w_pas_instances_inq
end type
end forward

global type w_pas_instances_inq from w_base_response_ext
integer x = 73
integer y = 260
integer width = 2263
integer height = 912
string title = "Manufacturing Instances Inquire"
long backcolor = 67108864
event ue_query pbm_custom01
dw_plant dw_plant
dw_instances_inq dw_instances_inq
cbx_hs cbx_hs
end type
global w_pas_instances_inq w_pas_instances_inq

type variables
Boolean	ib_valid_return
u_ws_pas1	iu_ws_pas1


Int		ii_previous_char_key, &
			ii_PAPast

Private:
datawindowchild 		idddw_child_state, &
							idddw_child_status, &
							idddw_child_shift
w_pas_instances		iw_parent_window
DataWindowChild		idwc_chars


end variables

forward prototypes
public function boolean wf_product_state_changed (string as_product_state)
public function boolean wf_product_status_changed (string as_product_status)
public function boolean wf_get_chars ()
end prototypes

on ue_query;call w_base_response_ext::ue_query;wf_get_chars()
end on

public function boolean wf_product_state_changed (string as_product_state);string 	ls_product_state_desc
			
long 		ll_row

ll_row = idddw_child_state.Find('type_code = "' + Trim(as_product_state) + '"', 1, idddw_child_state.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(as_product_state + " is an Invalid Product State")

	dw_instances_inq.SetFocus()
	dw_instances_inq.SelectText(1, Len(as_product_state))
	
	return false
End If

ls_product_state_desc = idddw_child_state.GetItemString(ll_row, 'type_short_desc')
dw_instances_inq.setitem(1,"product_state_description",(ls_product_state_desc))
SetProfileString( iw_frame.is_UserINI, "Pas", "LastProductState",Trim(as_product_state))

//This.SetFocus()
//This.SelectText(1, Len(as_product_state))
//
return true

end function

public function boolean wf_product_status_changed (string as_product_status);string 	ls_product_status_desc
			
long 		ll_row

ll_row = idddw_child_status.Find('type_code = "' + Trim(as_product_status) + '"', 1, idddw_child_status.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(as_product_status + " is an Invalid Product Status")

	dw_instances_inq.SetFocus()
	dw_instances_inq.SelectText(1, Len(as_product_status))
	
	return false
End If

ls_product_status_desc = idddw_child_status.GetItemString(ll_row, 'type_short_desc')
dw_instances_inq.setitem(1,"product_status_description",(ls_product_status_desc))
SetProfileString( iw_frame.is_UserINI, "Pas", "LastProductStatus",Trim(as_product_status))

//This.SetFocus()
//This.SelectText(1, Len(as_product_status))
//
return true

end function

public function boolean wf_get_chars ();Boolean	lb_ret

Char		lc_product_type

Long		ll_row_count

String	ls_input_string, &
			ls_fab_product_descr, &
			ls_char_string, &
			ls_plant, &
			ls_product, & 
			ls_state, &
			ls_status,ls_product_type

	
ls_plant = dw_plant.GetItemString(1, "location_code")
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then return false

ls_input_string = ls_plant

ls_product = dw_instances_inq.GetItemString(1, "fab_product_code")
If iw_frame.iu_string.nf_IsEmpty(ls_product) Then return False


ls_state = dw_instances_inq.GetItemString(1, "product_state")
ls_status = dw_instances_inq.GetItemString(1, "product_status")
If iw_frame.iu_string.nf_IsEmpty(ls_state) Then return False

SetPointer(HourGlass!)

ls_input_string += '~t' + ls_product + '~t' + ls_state + '~t' + ls_status +'~r~n'

iw_parent_window.istr_error_info.se_event_name = "Instances Char Inquire"
iw_parent_window.istr_error_info.se_procedure_name = "nf_inq_instances_chars"
iw_parent_window.istr_error_info.se_message = Space(70)

//lb_ret = iw_parent_window.iu_pas202.nf_inq_instances_chars( &
//					iw_parent_window.istr_error_info, &
//					ls_input_string, &
//					lc_product_type, &
//					ls_fab_product_descr, &
//					ls_char_string)
ls_product_type = lc_product_type
lb_ret = iu_ws_pas1.nf_pasp14fr( &
					iw_parent_window.istr_error_info, &
					ls_input_string, &
					ls_product_type, &
					ls_fab_product_descr, &
					ls_char_string)
lc_product_type = ls_product_type

If Not lb_ret Then 
	// If The product had focus, set focus back there
	// This is safe since either plant or product change called this event
	If ClassName(GetFocus()) = "dw_instances_inq" Then
		dw_instances_inq.SetColumn('fab_product_code')
		dw_instances_inq.SetFocus()
	Else
		dw_plant.SetFocus()
	End if
	return False
End if

dw_instances_inq.SetItem(1, "product_type", lc_product_type)
dw_instances_inq.SetItem(1, "fab_product_descr", ls_fab_product_descr)

ll_row_count = idwc_chars.ImportString(ls_char_string)
If ll_row_count < 1 Then
	iw_frame.SetMicroHelp("No Characteristics are defined for Plant " + ls_plant + &
								" and Product " + ls_product)
End if
If idwc_chars.Find("char_key = " + String(ii_previous_char_key), 1, ll_row_count) > 0 Then
	dw_instances_inq.SetItem(1, 'char_key', ii_previous_char_key)
End if
return true
end function

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_header
iu_ws_pas1 = Create u_ws_pas1

Date		ldt_temp

Int		li_tab

String	ls_header, &
			ls_temp, &
			ls_PaRange, &
			ls_display_status

This.SetRedraw(False)

If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "INSTPAST", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
End if
ii_PAPast = Integer(ls_PARange)

dw_instances_inq.GetChild("char_key", idwc_chars)
//idwc_chars.Reset()

iw_parent_window.dw_header.GetChild("char_key", ldwc_header)
If ldwc_header.RowCount() > 0 Then
	idwc_chars.ImportString(ldwc_header.Describe("DataWindow.Data"))
End if

ls_header = iw_parent_window.dw_header.Describe("DataWindow.Data")

// There is nothing in the string, so put initial values in here
If iw_frame.iu_string.nf_IsEmpty(ls_header) Or Left(ls_header,1) = '~t' Then 
	dw_instances_inq.SetItem(1, "begin_date", Today())
	ls_display_status = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "InstancesDisplayStatus", "D")
	dw_instances_inq.SetItem(1, "display_status", ls_display_status)
	dw_plant.SetFocus()
	This.SetRedraw(True)
	return
End if

// plant code is the first three chars
li_tab = Pos(ls_header, '~t', Pos(ls_header, '~t') + 1)
ls_temp = Mid(ls_header, 1, li_tab - 1)
dw_plant.Reset()
dw_plant.ImportString(ls_temp)
ls_header = Right(ls_header, Len(ls_header) - li_tab)

dw_instances_inq.Reset()
dw_instances_inq.ImportString(ls_header)
ldt_temp = dw_instances_inq.GetItemDate(1, "begin_date")
If ldt_temp < RelativeDate(Today(), 0 - ii_PAPast)  Or IsNull(ldt_temp) Then
	dw_instances_inq.SetItem(1, "begin_date", Today())
End if

dw_instances_inq.SetColumn('Fab_product_code')
dw_instances_inq.SetFocus()
This.SetRedraw(True)

end event

event close;call super::close;DataWindowChild	ldwc_header
Destroy iu_ws_pas1

If Not ib_valid_return Then
	Message.StringParm = ""
	return
End if

iw_parent_window.dw_header.GetChild("char_key", ldwc_header)
ldwc_header.Reset()
ldwc_header.ImportString(idwc_chars.Describe("DataWindow.Data"))

end event

event open;call super::open;iw_parent_window = Message.PowerObjectParm

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if
If dw_instances_inq.RowCount() = 0 Then
	dw_instances_inq.InsertRow(0)
End if
dw_plant.SetFocus()

// This is definitely a PowerBuilder 8.0.1.90???? hack.  This needs to be fixed.  REM.
	cbx_hs.checked = Profileint(gw_base_frame.is_userini, "InstanceHScroll", "ib_hs",  1) = 1
//
end event

on w_pas_instances_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_instances_inq=create dw_instances_inq
this.cbx_hs=create cbx_hs
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_instances_inq
this.Control[iCurrent+3]=this.cbx_hs
end on

on w_pas_instances_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_instances_inq)
destroy(this.cbx_hs)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date		ldt_temp

Int		li_temp

String	ls_temp, &
			ls_inquire, &
			ls_plant, &
			ls_type_code

If dw_plant.AcceptText() = -1 Then return
If dw_instances_inq.AcceptText() = -1 Then return

// validate all fields

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetColumn("location_code")
	dw_plant.SelectText(1, Len(dw_plant.GetItemString(1, "location_code")))
	dw_plant.SetFocus()
	return
End if

ls_inquire = dw_plant.Describe("DataWindow.Data")+ '~t'

ls_temp = dw_instances_inq.GetItemString(1, "fab_product_code")
If iw_frame.iu_string.nf_isEmpty(ls_temp) Then
	iw_frame.SetMicroHelp("Product Code is a required field")
	dw_instances_inq.SetColumn("fab_product_code")
	dw_instances_inq.SelectText(1, Len(ls_temp))
	dw_instances_inq.SetFocus()
	return
End if

ls_temp = dw_instances_inq.GetItemString(1, "product_state")
If iw_frame.iu_string.nf_isEmpty(ls_temp) Then
	iw_frame.SetMicroHelp("Product State is a required field")
	dw_instances_inq.SetColumn("product_state")
	dw_instances_inq.SelectText(1, Len(ls_temp))
	dw_instances_inq.SetFocus()
	return
End if

ls_temp = dw_instances_inq.GetItemString(1, "product_status")
If iw_frame.iu_string.nf_isEmpty(ls_temp) Then
	iw_frame.SetMicroHelp("Product Status is a required field")
	dw_instances_inq.SetColumn("product_status")
	dw_instances_inq.SelectText(1, Len(ls_temp))
	dw_instances_inq.SetFocus()
	return
End if

li_temp = dw_instances_inq.GetItemNumber(1, "char_key")
If li_temp < 1 or IsNull(li_temp) Then
	iw_frame.SetMicroHelp("Characteristics is a required field")
	dw_instances_inq.SetColumn("char_key")
	dw_instances_inq.SelectText(1, Len(String(li_temp)))
	dw_instances_inq.SetFocus()
	return
End if

ls_temp = dw_instances_inq.GetItemString(1, "shift")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	iw_frame.SetMicroHelp("Shift is a required field")
	dw_instances_inq.SetColumn("shift")
	dw_instances_inq.SelectText(1, Len(ls_temp))
	dw_instances_inq.SetFocus()
	return
End if

ldt_temp = dw_instances_inq.GetItemDate(1, "begin_date")
If IsNull(ldt_temp) Then
	iw_frame.SetMicroHelp("Begin Date is a required field")
	dw_instances_inq.SetColumn("begin_date")
	dw_instances_inq.SelectText(1, Len(String(ldt_temp)))
	dw_instances_inq.SetFocus()
	return
End if

If dw_instances_inq.GetItemString(1, "display_status") = 'W' Then
	ls_plant = dw_plant.GetItemString(1, "location_code")

	SELECT tutltypes.type_code 
		INTO :ls_type_code	
		FROM tutltypes
		WHERE (tutltypes.record_type = 'PAINSTWK') AND
				  ((tutltypes.type_code = :ls_plant) or (tutltypes.type_code = 'ALL'));
				  
	If sqlca.sqlcode = 100 Then
		MessageBox("Plant Code Error", "Plant code " + ls_plant + " is not set up for Long Term by Day of Week")
	End If
End if


ls_inquire += dw_instances_inq.Describe("DataWindow.Data")

SetProfileString(gw_netwise_frame.is_Userini,Message.nf_Get_App_ID(), "InstancesDisplayStatus", dw_instances_inq.GetItemString(1, "display_status"))

ib_valid_return = True
CloseWithReturn(This, ls_inquire)



end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_instances_inq
integer x = 1906
integer y = 272
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_instances_inq
integer x = 1906
integer y = 148
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_instances_inq
integer x = 1906
integer y = 24
integer taborder = 40
end type

type dw_plant from u_plant within w_pas_instances_inq
integer x = 169
integer y = 20
integer width = 1454
integer taborder = 10
end type

event itemchanged;call super::itemchanged;Int	li_null
Long	ll_result
String	ls_type_code

ll_result = Super::Event itemchanged(row, dwo, data)

ii_previous_char_key = dw_instances_inq.GetItemNumber(1, 'char_key')

SetNull(li_null)
idwc_chars.Reset()
dw_instances_inq.SetItem(1, "char_key", li_null)

If dw_instances_inq.GetItemString(1, "display_status") = 'W' Then

	SELECT tutltypes.type_code 
		INTO :ls_type_code	
		FROM tutltypes
		WHERE (tutltypes.record_type = 'PAINSTWK') AND
				  ((tutltypes.type_code = :data) or (tutltypes.type_code = 'ALL'));
				  
	If sqlca.sqlcode = 100 Then
//		MessageBox("Plant Code Error", "Plant code " + data + " is not set up for Long Term by Day of Week")
		dw_instances_inq.SetItem(1, "display_status", "L")
	End If
//	Return 
End if

Parent.PostEvent("ue_query")

return ll_result
end event

type dw_instances_inq from u_base_dw_ext within w_pas_instances_inq
integer y = 104
integer width = 1856
integer height = 688
integer taborder = 20
string dataobject = "d_pas_instances_inq"
boolean border = false
boolean ib_db_reconnectable = true
end type

event itemchanged;call super::itemchanged;Int		li_null
string 	ls_product_state, &
			ls_product_status, &
			ls_plant, &
			ls_type_code

Choose Case This.GetColumnName()
	Case "fab_product_code"
		ii_previous_char_key = This.GetItemNumber(1, 'char_key')
		
		idwc_chars.Reset()
		SetNull(li_null)
		This.SetItem(1, "char_key", li_null)
		Parent.PostEvent("ue_query")
		
	Case 'begin_date'
		If Date(This.GetText()) < RelativeDate(Today(), 0 - ii_PaPast) Then
			iw_frame.SetMicroHelp('Begin Date must be within ' + String(ii_paPast) + &
						' days of Today')
			Return 1
		End if
		
	case "product_state" 
			ls_product_state = data
			ii_previous_char_key = This.GetItemNumber(1, 'char_key')
			
			idwc_chars.Reset()
			SetNull(li_null)
			This.SetItem(1, "char_key", li_null)
			Parent.PostEvent("ue_query")
						
			If Len(Trim(ls_product_state)) = 0 Then 
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				This.ScrollToRow(This.RowCount())
				This.SetItem(1,"product_state","")
				this.setitem(1,"product_state_description",(""))
				return 
			End if

			IF NOT wf_product_state_changed(ls_product_state) Then
				return 1
			End If
			
	case "product_status" 
			ls_product_status = data
			ii_previous_char_key = This.GetItemNumber(1, 'char_key')
			
			idwc_chars.Reset()
			SetNull(li_null)
			This.SetItem(1, "char_key", li_null)
			Parent.PostEvent("ue_query")
						
			If Len(Trim(ls_product_status)) = 0 Then 
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				This.ScrollToRow(This.RowCount())
				This.SetItem(1,"product_status","")
				this.setitem(1,"product_status_description",(""))
				return 
			End if

			IF NOT wf_product_status_changed(ls_product_status) Then
				return 1
			End If
			
	case "display_status"		
		If data = 'W' Then
			dw_plant.AcceptText()
			ls_plant = dw_plant.GetItemString(1, "location_code")

			SELECT tutltypes.type_code 
				INTO :ls_type_code	
				FROM tutltypes
				WHERE (tutltypes.record_type = 'PAINSTWK') AND
						  ((tutltypes.type_code = :ls_plant) or (tutltypes.type_code = 'ALL'));
						  
			If sqlca.sqlcode = 100 Then
				MessageBox("Plant Code Error", "Plant code " + ls_plant + " is not set up for Long Term by Day of Week")
				Return 1
			End If
		End if
			
End Choose
end event

event itemerror;call super::itemerror;Return 1
end event

event ue_retrieve;call super::ue_retrieve;string ls_Last_State, ls_Last_Status

This.GetChild("product_state", idddw_child_state)

idddw_child_state.SetTransObject(SQLCA)
idddw_child_state.Retrieve("PRDSTATE")
idddw_child_state.SetSort("type_code")
idddw_child_state.Sort()

//idddw_child.InsertRow(0)
This.InsertRow(0)

ls_Last_State = ProfileString( iw_frame.is_UserINI, "Pas", "LastProductState","1")
This.SetItem(1,"product_state",ls_Last_State)
wf_product_state_changed(ls_Last_State)


This.GetChild("product_status", idddw_child_status)

idddw_child_status.SetTransObject(SQLCA)
idddw_child_status.Retrieve("PRODSTAT")
idddw_child_status.SetSort("type_code")
idddw_child_status.Sort()
//
////idddw_child.InsertRow(0)
//This.InsertRow(0)

ls_Last_Status = ProfileString( iw_frame.is_UserINI, "Pas", "LastProductStatus","G")
This.SetItem(1,"product_status",ls_Last_Status)
wf_product_status_changed(ls_Last_Status)

//Jenn 12-13

This.GetChild("shift", idddw_child_shift)

idddw_child_shift.SetTransObject(SQLCA)
idddw_child_shift.Retrieve("SHIFT")
idddw_child_shift.SetSort("type_code")
idddw_child_shift.Sort()
//This.InsertRow(0)
end event

type cbx_hs from checkbox within w_pas_instances_inq
integer x = 73
integer y = 620
integer width = 343
integer height = 72
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Show Split:"
boolean checked = true
boolean lefttext = true
end type

event clicked;// This is definitely a PowerBuilder 8.0.1.90???? hack.  This needs to be fixed.  REM.
	integer	li_Checked
	if this.checked then
		li_Checked = 1
	else
		li_Checked = 0
	end if
	
	SetProfileString(gw_base_frame.is_userini, "InstanceHScroll", "ib_hs",  string(li_Checked)) 
//
end event

