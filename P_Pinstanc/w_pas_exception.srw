HA$PBExportHeader$w_pas_exception.srw
forward
global type w_pas_exception from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_pas_exception
end type
type dw_char from u_base_dw_ext within w_pas_exception
end type
type dw_plant from u_base_dw_ext within w_pas_exception
end type
end forward

global type w_pas_exception from w_base_sheet_ext
integer width = 3013
integer height = 1636
string title = "Step Exceptions"
long backcolor = 67108864
event ue_getdata pbm_custom01
dw_header dw_header
dw_char dw_char
dw_plant dw_plant
end type
global w_pas_exception w_pas_exception

type variables
DataWindowChild	idwc_plant_dddw, &
		idwc_step_dddw
DataStore	ids_exceptions

u_pas201		iu_pas201

u_ws_pas3			iu_ws_pas3

s_error		istr_error_info

String		is_characteristic
end variables

forward prototypes
public function string wf_getheaderdata ()
public function string wf_getstepdddwdata ()
public subroutine wf_setstepdddwdata (string as_data)
public subroutine wf_add_upd_ind (string as_shift, long al_clicked_row, string as_plant_code, string as_update_ind)
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

event ue_getdata;call super::ue_getdata;DataWindowChild	ldwc_steps


Choose Case Lower(String(Message.LongParm, "address"))
Case "header"
	Message.StringParm = dw_header.Object.DataWindow.Data
Case "steplist"
	dw_header.GetChild('mfg_step_sequence', ldwc_steps)
	Message.StringParm = ldwc_steps.Describe("DataWindow.Data")
End Choose
end event

public function string wf_getheaderdata ();return dw_header.Describe("DataWindow.Data")
end function

public function string wf_getstepdddwdata ();return idwc_step_dddw.Describe("DataWindow.Data")
end function

public subroutine wf_setstepdddwdata (string as_data);idwc_step_dddw.Reset()
idwc_step_dddw.ImportString(as_data)
return
end subroutine

public subroutine wf_add_upd_ind (string as_shift, long al_clicked_row, string as_plant_code, string as_update_ind);Long		ll_clicked_row, &
			ll_excp_row

String	ls_char_key, &
			ls_find, &
			ls_shift, &
			ls_original_value


ls_char_key = String(dw_char.GetItemNumber(al_clicked_row, 'char_key'))
ls_find = "plant_code = '" + as_plant_code + "' and char_key = " + ls_char_key + &
		" and shift = '" + as_shift + "'"
ll_excp_row = ids_exceptions.Find(ls_find, 1, ids_exceptions.RowCount())

If ll_excp_row > 0 Then
	ls_original_value = ids_exceptions.GetItemString(ll_excp_row, 'update_ind', &
																	Primary!, True)
	If as_update_ind = 'D' Then
		// If the original value was not 'O' then just delete it
		If ls_original_value = 'O' Then
			ids_exceptions.SetItem(ll_excp_row, 'update_ind', 'D')
		Else
			ids_exceptions.DeleteRow(ll_excp_row)
			return
		End if
	Else
		// adding, if originally existed, set it back to 'O'
		ids_exceptions.SetItem(ll_excp_row, 'update_ind', 'O')
	End if
Else
	If as_update_ind <> 'D' Then
		ids_exceptions.ImportString(as_plant_code + '~t' + ls_char_key + '~t' + as_shift + &
				'~t' + as_update_ind)
	End If
End if

end subroutine

public function boolean wf_retrieve ();DataWindowChild	ldwc_steps

String		ls_header, &
				ls_Left, &
				ls_Right, &
				ls_plant, &
				ls_exception, &
				ls_steps,&
				ls_product_code,&
				ls_product_desc,&
				ls_product_state,&
				ls_product_state_desc,&
				ls_mfg_step_sequence, &
				ls_product_status, &
				ls_product_status_desc
				
				

Call w_base_sheet::closequery
If Message.ReturnValue = -1 Then return False


OpenWithParm(w_pas_product_step_inq, This)
ls_Header = Message.StringParm
If iw_frame.iu_string.nf_IsEmpty(ls_Header) Then return False

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

iw_frame.iu_string.nf_ParseLeftRight(ls_Header, '~r~n', ls_Header, ls_Steps)

dw_header.GetChild('mfg_step_sequence', ldwc_steps)
ldwc_steps.Reset()
ldwc_Steps.ImportString(ls_Steps)

dw_header.Reset()
dw_header.ImportString(ls_Header)

// assuming the string is good data
// and that the step dddw is filled in by the inquire box
dw_plant.Reset()
dw_plant.InsertRow(0)
idwc_plant_dddw.Reset()
dw_char.Reset()
ids_exceptions.Reset()

is_characteristic = ''

ls_product_code = iw_frame.iu_string.nf_gettoken(ls_header, "~t")
ls_product_desc = iw_frame.iu_string.nf_gettoken(ls_header, "~t")
ls_product_state = iw_frame.iu_string.nf_gettoken(ls_header, "~t")
ls_product_state_desc = iw_frame.iu_string.nf_gettoken(ls_header, "~t")
ls_product_status = iw_frame.iu_string.nf_gettoken(ls_header, "~t")
ls_product_status_desc = iw_frame.iu_string.nf_gettoken(ls_header, "~t")
ls_mfg_step_sequence = iw_frame.iu_string.nf_gettoken(ls_header, "~t")

ls_header = ls_product_code + "~t" + ls_product_state + "~t" + &
				ls_product_status + "~t" + ls_mfg_step_sequence + '~r~n'
			
istr_error_info.se_event_name = "wf_retrieve"
//If Not iu_pas201.nf_inq_exception(istr_error_info, &
//											ls_header, &
//											ls_plant, &
//											is_characteristic, &
//											ls_exception) Then return False
											
If Not iu_ws_pas3.uf_pasp41fr(istr_error_info, &
											ls_header, &
											ls_plant, &
											is_characteristic, &
											ls_exception) Then return False											

dw_char.ImportString(is_characteristic)
idwc_plant_dddw.ImportString(ls_plant)

ids_exceptions.ImportString(ls_exception)
ids_exceptions.ResetUpdate()

dw_plant.SetFocus()
iw_frame.SetMicroHelp("Inquire Successful")
return True



end function

public function boolean wf_update ();Int		li_counter

Long		ll_row_count

String	ls_header, &
			ls_exception


SetPointer(HourGlass!)

If dw_header.RowCount() < 1 Then
	return False
End if
ls_header = dw_header.GetItemString(1, 'fab_product_code') + '~t' + &
				dw_header.GetItemString(1, 'product_state') + '~t' + &
				dw_header.GetItemString(1, 'product_status') + '~t' + &				
				String(dw_header.GetItemNumber(1, 'mfg_step_sequence')) + '~r~n'

ids_exceptions.SetFilter('update_ind <> "O"')
ids_exceptions.Filter()

ls_exception = ids_exceptions.Describe('DataWindow.Data')

ids_exceptions.SetFilter("")
ids_exceptions.RowsMove(1, ids_exceptions.FilteredCount(), Filter!, ids_exceptions, 1, Primary!)

istr_error_info.se_event_name = 'wf_update'

iw_frame.SetMicroHelp("Updating Database ...")
//If Not iu_pas201.nf_upd_exception(istr_error_info, ls_header, ls_exception) Then return False
If Not iu_ws_pas3.uf_pasp42fr(istr_error_info, ls_header, ls_exception) Then return False

ids_exceptions.SetFilter("update_ind <> 'D'")
ids_exceptions.Filter()
ids_exceptions.RowsDiscard(1, ids_exceptions.FilteredCount(), Filter!)
ids_exceptions.SetFilter("update_ind = 'A'")
ids_exceptions.Filter()

ll_row_count = ids_exceptions.RowCount()
For li_counter = 1 to ll_row_count
	ids_exceptions.SetItem(li_counter, 'update_ind', 'O')
Next

ids_exceptions.SetFilter("")
ids_exceptions.RowsMove(1, ids_exceptions.FilteredCount(), Filter!, ids_exceptions, 1, Primary!)

iw_frame.SetMicroHelp("Update Successful")
ids_exceptions.ResetUpdate()
return true
end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy iu_pas201
If IsValid(iu_ws_pas3) Then Destroy iu_ws_pas3

end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')

end event

event ue_postopen;call super::ue_postopen;iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

ids_exceptions = Create datastore
ids_exceptions.DataObject = 'd_pas_exception'

dw_header.GetChild('mfg_step_sequence', idwc_step_dddw)
dw_plant.GetChild('plant_code', idwc_plant_dddw)

istr_error_info.se_user_id = sqlca.userid
istr_error_info.se_app_name  = Message.nf_Get_app_Id()
istr_error_info.se_window_name = "Excption"

iw_frame.im_menu.m_file.m_inquire.TriggerEvent(Clicked!)

end event

on w_pas_exception.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_char=create dw_char
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_char
this.Control[iCurrent+3]=this.dw_plant
end on

on w_pas_exception.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_char)
destroy(this.dw_plant)
end on

event open;call super::open;dw_header.InsertRow(0)
dw_plant.InsertRow(0)


end event

type dw_header from u_base_dw_ext within w_pas_exception
integer x = 402
integer width = 1609
integer height = 352
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_pas_exception_header"
boolean border = false
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

type dw_char from u_base_dw_ext within w_pas_exception
integer x = 1134
integer y = 352
integer width = 1755
integer height = 992
integer taborder = 10
string dataobject = "d_pas_exception_char"
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;Long		ll_clicked_row

String	ls_update_ind, &
			ls_plant_code, &
			ls_column_name

ll_clicked_row = row
If ll_clicked_row < 1 Then return

ls_plant_code = dw_plant.GetItemString(1, 'plant_code')
If iw_frame.iu_string.nf_IsEmpty(ls_plant_code) Then
	iw_frame.SetMicroHelp('Please select a plant before choosing characteristics')
	return
End if

ls_column_name = dwo.name

If This.IsSelected(ll_Clicked_row) Then
	IF Not (ls_column_name = 'shift_a' Or &
			ls_column_name = 'shift_b' Or &
			ls_column_name = 'shift_c' Or &
			ls_column_name = 'shift_d') Then
		This.SelectRow(ll_clicked_row, False)
		This.SetItem(ll_clicked_row, 'shift_a', 'F')
		This.SetItem(ll_clicked_row, 'shift_b', 'F')
		This.SetItem(ll_clicked_row, 'shift_c', 'F')
		This.SetItem(ll_clicked_row, 'shift_d', 'F')
		ls_update_ind = 'D'
		wf_add_upd_ind('A', ll_clicked_row, ls_plant_code, ls_update_ind)
		wf_add_upd_ind('B', ll_clicked_row, ls_plant_code, ls_update_ind)
		wf_add_upd_ind('C', ll_clicked_row, ls_plant_code, ls_update_ind)
		wf_add_upd_ind('D', ll_clicked_row, ls_plant_code, ls_update_ind)
		Return
	End IF
Else
	This.SelectRow(ll_clicked_row, True)
	IF Not (ls_column_name = 'shift_a' Or &
			ls_column_name = 'shift_b' Or &
			ls_column_name = 'shift_c' Or & 
			ls_column_name = 'shift_d') Then
		This.SetItem(ll_clicked_row, 'shift_a', 'T')
		This.SetItem(ll_clicked_row, 'shift_b', 'T')
		This.SetItem(ll_clicked_row, 'shift_c', 'T')
		This.SetItem(ll_clicked_row, 'shift_d', 'T')
		ls_update_ind = 'A'
		wf_add_upd_ind('A', ll_clicked_row, ls_plant_code, ls_update_ind)
		wf_add_upd_ind('B', ll_clicked_row, ls_plant_code, ls_update_ind)
		wf_add_upd_ind('C', ll_clicked_row, ls_plant_code, ls_update_ind)
		wf_add_upd_ind('D', ll_clicked_row, ls_plant_code, ls_update_ind)
		Return
	End IF
End if	


end event

on constructor;call u_base_dw_ext::constructor;ib_updateable = False

end on

event itemchanged;call super::itemchanged;Long		ll_clicked_row

String	ls_update_ind, &
			ls_plant_code, &
			ls_column_name
			
			
ls_plant_code = dw_plant.GetItemString(1, 'plant_code')
If iw_frame.iu_string.nf_IsEmpty(ls_plant_code) Then
	iw_frame.SetMicroHelp('Please select a plant before choosing characteristics')
	return 1
End if

ll_clicked_row = row
If ll_clicked_row < 1 Then return

ls_column_name = dwo.name

// This may seem backwards, but the itemchanged hasn't happened.
IF data = 'T' Then
	ls_update_ind = 'A' 
Else
	ls_update_ind = 'D'
End If

Choose Case ls_column_name
	Case 'shift_a'
		wf_add_upd_ind('A', ll_clicked_row, ls_plant_code, ls_update_ind)
		If ls_update_ind = 'D' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_b') <> 'T' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_c') <> 'T' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_d') <> 'T' Then
			This.SelectRow(ll_clicked_row, False)
		End If			
	Case 'shift_b'
		wf_add_upd_ind('B', ll_clicked_row, ls_plant_code, ls_update_ind)
		If ls_update_ind = 'D' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_a') <> 'T' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_c') <> 'T' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_d') <> 'T' Then
			This.SelectRow(ll_clicked_row, False)
		End If			
	Case 'shift_c'
		wf_add_upd_ind('C', ll_clicked_row, ls_plant_code, ls_update_ind)
		If ls_update_ind = 'D' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_a') <> 'T' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_b') <> 'T' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_d') <> 'T' Then
			This.SelectRow(ll_clicked_row, False)
		End If			
	Case 'shift_d'
		wf_add_upd_ind('D', ll_clicked_row, ls_plant_code, ls_update_ind)
		If ls_update_ind = 'D' and &
		      dw_char.GetItemString(ll_clicked_row, 'shift_a') <> 'T' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_b') <> 'T' and &
				dw_char.GetItemString(ll_clicked_row, 'shift_c') <> 'T' Then
			This.SelectRow(ll_clicked_row, False)
		End If		
End Choose



end event

event itemerror;call super::itemerror;Return 1
end event

type dw_plant from u_base_dw_ext within w_pas_exception
integer x = 5
integer y = 408
integer width = 1125
integer height = 172
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_pas_exception_plant"
boolean border = false
end type

event itemerror;call super::itemerror;Return 1
end event

on constructor;call u_base_dw_ext::constructor;ib_updateable = False

end on

event itemchanged;call super::itemchanged;Long	ll_row, &
		ll_excp_row_count, &
		ll_char_row, &
		ll_char_row_count, &
		ll_shift_row, &
		ll_char_key

String	ls_text, &
			ls_description, ls_temp 


ls_text = This.GetText()
If Len(Trim(ls_text)) = 0 Then 
	This.SetItem(1, "plant_description", "")	
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	return
End if

ll_row = idwc_plant_dddw.Find('plant_code = "' + ls_text + '"', 1, idwc_plant_dddw.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(ls_text + " is not a Valid Plant Code for this Product and Step")
	ls_description = ""
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	return 1
Else
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	ls_Description = idwc_plant_dddw.GetItemString(ll_row, "plant_description")
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

This.SetItem(1, "plant_description", ls_Description)

dw_char.SetRedraw(False)
dw_char.Reset()
dw_char.ImportString(is_characteristic)
dw_char.SelectRow(0, False)

ll_char_row_count = dw_char.RowCount()
ll_excp_row_count = ids_exceptions.RowCount()

ll_row = 0
Do
	ls_temp = 'plant_code = "' + ls_text + '" and update_ind <> "D"'
	ll_row = ids_exceptions.Find('plant_code = "' + ls_text + &
			'" and update_ind <> "D"', ll_row + 1, ll_excp_row_count + 1)
	If ll_row < 1 Then Exit

	// if we find a row, we need to hilight that characteristic	
	ll_char_key = ids_exceptions.GetItemNumber(ll_row, 'char_key')
	ll_char_row = dw_char.Find('char_key = ' + String(ll_char_key), 1, ll_char_row_count)
	If ll_char_row > 0 Then
		dw_char.SelectRow(ll_char_row, True)
		ll_shift_row = ids_exceptions.Find('plant_code = "' + ls_text + '" and char_key = ' + &
				String(ll_char_key) + ' and shift = "A"', 1, ll_excp_row_count)
		If ll_shift_row > 0 Then dw_char.SetItem(ll_char_row, 'shift_a', 'T')

		ll_shift_row = ids_exceptions.Find('plant_code = "' + ls_text + '" and char_key = ' + &
				String(ll_char_key) + ' and shift = "B"', 1, ll_excp_row_count)
		If ll_shift_row > 0 Then dw_char.SetItem(ll_char_row, 'shift_b', 'T')

		ll_shift_row = ids_exceptions.Find('plant_code = "' + ls_text + '" and char_key = ' + &
				String(ll_char_key) + ' and shift = "C"', 1, ll_excp_row_count)
		If ll_shift_row > 0 Then dw_char.SetItem(ll_char_row, 'shift_c', 'T')
		
		ll_shift_row = ids_exceptions.Find('plant_code = "' + ls_text + '" and char_key = ' + &
				String(ll_char_key) + ' and shift = "D"', 1, ll_excp_row_count)
		If ll_shift_row > 0 Then dw_char.SetItem(ll_char_row, 'shift_d', 'T')
	End if
Loop while ll_row > 0 and ll_row < ll_excp_row_count

dw_char.SetRedraw(True)


end event

