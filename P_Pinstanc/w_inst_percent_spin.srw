HA$PBExportHeader$w_inst_percent_spin.srw
forward
global type w_inst_percent_spin from window
end type
type st_ratio_t from statictext within w_inst_percent_spin
end type
type em_ratio from editmask within w_inst_percent_spin
end type
type cb_help from u_help_cb within w_inst_percent_spin
end type
type em_boxes from editmask within w_inst_percent_spin
end type
type st_4 from statictext within w_inst_percent_spin
end type
type em_pcs_per_box from editmask within w_inst_percent_spin
end type
type st_pieces_per_box from statictext within w_inst_percent_spin
end type
type cb_ok from commandbutton within w_inst_percent_spin
end type
type em_pieces from editmask within w_inst_percent_spin
end type
type st_pieces from statictext within w_inst_percent_spin
end type
type em_percent from editmask within w_inst_percent_spin
end type
type st_1 from statictext within w_inst_percent_spin
end type
end forward

global type w_inst_percent_spin from window
integer x = 247
integer y = 376
integer width = 1865
integer height = 480
boolean titlebar = true
string title = "Piece Count"
boolean controlmenu = true
windowtype windowtype = popup!
long backcolor = 12632256
event ue_help pbm_custom01
st_ratio_t st_ratio_t
em_ratio em_ratio
cb_help cb_help
em_boxes em_boxes
st_4 st_4
em_pcs_per_box em_pcs_per_box
st_pieces_per_box st_pieces_per_box
cb_ok cb_ok
em_pieces em_pieces
st_pieces st_pieces
em_percent em_percent
st_1 st_1
end type
global w_inst_percent_spin w_inst_percent_spin

type variables
Decimal	idc_piece_count, &
	idc_one_percent


end variables

on ue_help;iw_frame.wf_context(this.title)
end on

event open;Decimal		ldc_number

String		ls_text, &
				ls_fab_uom, &
				ls_step_descr

DataStore	lds_pieces
DataWindowChild	ldwc_uom

ls_text = Message.StringParm
Window      lw_parent 

lw_parent = this.parentwindow()
 
This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
            This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)  

// String is Current Percent, Piece Count, pieces per box, ratio, and Fab UOM
em_percent.Text = String(Dec(iw_frame.iu_string.nf_gettoken(ls_text,'~t')), "0.00")
idc_piece_count = Dec(iw_frame.iu_string.nf_gettoken(ls_text, '~t'))
em_pcs_per_box.Text = iw_frame.iu_string.nf_gettoken(ls_text, '~t')
em_ratio.Text = String(Dec(iw_frame.iu_string.nf_gettoken(ls_text, '~t')),"0.000000")
ls_fab_uom = iw_frame.iu_string.nf_gettoken(ls_text, '~t')
ls_step_descr = iw_frame.iu_string.nf_gettoken(ls_text, '~t')

This.Title = 'Piece Count for Step ' + ls_step_descr

st_pieces.Text = iw_frame.iu_string.nf_wordcap(ls_fab_uom)
st_pieces_per_box.Text = iw_frame.iu_string.nf_wordcap(ls_fab_uom) + " Per Box"

If idc_piece_Count <= 0 Then
	iw_frame.SetMicroHelp("Percent Calculator is not available when Piece Count is 0")
	Close(This)
	return
End if

lds_pieces = Create DataStore
lds_pieces.DataObject = 'd_inst_percent_spin'

lds_pieces.GetChild("fab_uom", ldwc_uom)
ldwc_uom.SetTransObject(SQLCA)
ldwc_uom.Retrieve("FABUOM")
//iw_frame.iu_netwise_data.nf_GetTypeDesc('FABUOM', ldwc_uom)
lds_pieces.InsertRow(0)
lds_pieces.SetItem(1,'fab_uom',ls_fab_uom)

st_pieces.Text = iw_frame.iu_string.nf_wordcap(lds_pieces.Describe("Evaluate('LookUpDisplay(fab_uom)',1)"))

em_percent.TriggerEvent(Modified!)

em_percent.SetFocus()
end event

on w_inst_percent_spin.create
this.st_ratio_t=create st_ratio_t
this.em_ratio=create em_ratio
this.cb_help=create cb_help
this.em_boxes=create em_boxes
this.st_4=create st_4
this.em_pcs_per_box=create em_pcs_per_box
this.st_pieces_per_box=create st_pieces_per_box
this.cb_ok=create cb_ok
this.em_pieces=create em_pieces
this.st_pieces=create st_pieces
this.em_percent=create em_percent
this.st_1=create st_1
this.Control[]={this.st_ratio_t,&
this.em_ratio,&
this.cb_help,&
this.em_boxes,&
this.st_4,&
this.em_pcs_per_box,&
this.st_pieces_per_box,&
this.cb_ok,&
this.em_pieces,&
this.st_pieces,&
this.em_percent,&
this.st_1}
end on

on w_inst_percent_spin.destroy
destroy(this.st_ratio_t)
destroy(this.em_ratio)
destroy(this.cb_help)
destroy(this.em_boxes)
destroy(this.st_4)
destroy(this.em_pcs_per_box)
destroy(this.st_pieces_per_box)
destroy(this.cb_ok)
destroy(this.em_pieces)
destroy(this.st_pieces)
destroy(this.em_percent)
destroy(this.st_1)
end on

type st_ratio_t from statictext within w_inst_percent_spin
integer x = 617
integer y = 16
integer width = 219
integer height = 76
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Ratio"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_ratio from editmask within w_inst_percent_spin
event ue_enchange pbm_enchange
integer x = 850
integer y = 16
integer width = 311
integer height = 88
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###.0000###"
boolean spin = true
string displaydata = "$$HEX1$$c400$$ENDHEX$$"
end type

event ue_enchange;This.TriggerEvent(Modified!)
end event

event getfocus;This.SelectText(1, Len(This.Text))
end event

event modified;em_pieces.Text = String((idc_piece_count / 100.00) * Dec(em_percent.Text) * Dec(This.Text), '0.00')
If Dec(em_pcs_per_box.Text) > 0 Then
	em_boxes.Text = String(Dec(em_pieces.Text) / Dec(em_pcs_per_box.Text), '0.00')
End if
end event

type cb_help from u_help_cb within w_inst_percent_spin
integer x = 1518
integer y = 248
integer height = 108
integer taborder = 70
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
end type

type em_boxes from editmask within w_inst_percent_spin
event ue_enchange pbm_enchange
integer x = 1381
integer y = 120
integer width = 375
integer height = 88
integer taborder = 50
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###.00"
boolean spin = true
double increment = 1
string minmax = "0~~"
end type

on ue_enchange;This.TriggerEvent(Modified!)
end on

event modified;If Dec(em_pcs_per_box.Text) > 0 Then
	em_pieces.Text = String(Dec(This.Text) * Dec(em_pcs_per_box.Text), '0.00')
	If Dec(em_ratio.Text) > 0 Then
		em_percent.Text = String((100.00 * Dec(em_pieces.Text)) / idc_piece_count / Dec(em_ratio.Text), '0.00')
	End If
End if
end event

on getfocus;This.SelectText(1, Len(This.Text))
end on

type st_4 from statictext within w_inst_percent_spin
integer x = 1198
integer y = 124
integer width = 169
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Boxes"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_pcs_per_box from editmask within w_inst_percent_spin
event ue_enchange pbm_enchange
integer x = 288
integer y = 120
integer width = 311
integer height = 88
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,##0"
boolean spin = true
string displaydata = ""
double increment = 1
string minmax = "0~~"
end type

on ue_enchange;This.TriggerEvent(Modified!)
end on

on modified;em_boxes.TriggerEvent(Modified!)
end on

on getfocus;This.SelectText(1, Len(This.Text))
end on

type st_pieces_per_box from statictext within w_inst_percent_spin
integer y = 128
integer width = 279
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Pcs Per Box"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_inst_percent_spin
integer x = 1239
integer y = 248
integer width = 247
integer height = 108
integer taborder = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&OK"
boolean cancel = true
boolean default = true
end type

on clicked;Close(Parent)
end on

type em_pieces from editmask within w_inst_percent_spin
event ue_enchange pbm_enchange
integer x = 1381
integer y = 12
integer width = 375
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###.00"
boolean spin = true
double increment = 1
string minmax = "0~~"
end type

on ue_enchange;This.TriggerEvent(Modified!)
end on

event modified;
If Dec(em_ratio.Text) > 0 Then
	em_percent.Text = String((100.00 * Dec(This.Text)) / idc_piece_count / Dec(em_ratio.Text), '0.00')
End If
If Dec(em_pcs_per_box.Text) > 0 Then
	em_boxes.Text = String(Dec(This.Text) / Dec(em_pcs_per_box.Text), '0.00')
End if
end event

on getfocus;This.SelectText(1, Len(This.Text))
end on

type st_pieces from statictext within w_inst_percent_spin
integer x = 1175
integer y = 16
integer width = 192
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Pieces"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_percent from editmask within w_inst_percent_spin
event ue_enchange pbm_enchange
integer x = 288
integer y = 12
integer width = 311
integer height = 88
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###,###.00"
boolean spin = true
double increment = 1
string minmax = "0~~100"
end type

on ue_enchange;This.TriggerEvent(Modified!)
end on

event modified;em_pieces.Text = String((idc_piece_count / 100.00) * Dec(em_ratio.Text) * Dec(This.Text), '0.00')
If Dec(em_pcs_per_box.Text) > 0 Then
	em_boxes.Text = String(Dec(em_pieces.Text) / Dec(em_pcs_per_box.Text), '0.00')
End if
end event

on getfocus;This.SelectText(1, Len(This.Text))
end on

type st_1 from statictext within w_inst_percent_spin
integer x = 87
integer y = 20
integer width = 192
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Percent"
alignment alignment = right!
boolean focusrectangle = false
end type

