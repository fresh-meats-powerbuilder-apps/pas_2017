HA$PBExportHeader$w_pas_instance_date_range.srw
forward
global type w_pas_instance_date_range from w_base_response_ext
end type
type dw_dates from u_base_dw_ext within w_pas_instance_date_range
end type
end forward

global type w_pas_instance_date_range from w_base_response_ext
integer width = 864
integer height = 688
string title = "Add Date Range"
long backcolor = 12632256
dw_dates dw_dates
end type
global w_pas_instance_date_range w_pas_instance_date_range

type variables
Boolean	ib_valid_return

Int	ii_PaPast

w_pas_instances		iw_parent_window

Date		idt_EndDate
end variables

event ue_postopen;call super::ue_postopen;String	ls_PARange


If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "PAST", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "PA Range for PAST not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
End if
ii_PAPast = Integer(ls_PARange)

If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "INST END", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "PA Range for INST END not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
End if
idt_EndDate = iw_parent_window.idt_instances_end_date
dw_dates.SetItem(1, "end_date", idt_EndDate)

dw_dates.SetItem(1, "pattern", "N")

If iw_parent_window.dw_header.GetItemString(1, "display_status") = "W" Then
	dw_dates.Object.pattern.Visible = True
	dw_dates.Object.t_1.Visible = True
	dw_dates.SetItem(1, "pattern", "Y")
Else
	dw_dates.Object.pattern.Visible = False
	dw_dates.Object.t_1.Visible = False
	dw_dates.SetItem(1, "pattern", "N")
End If

end event

event open;call super::open;iw_parent_window = Message.PowerObjectParm

dw_dates.SetFocus()
end event

on w_pas_instance_date_range.create
int iCurrent
call super::create
this.dw_dates=create dw_dates
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_dates
end on

on w_pas_instance_date_range.destroy
call super::destroy
destroy(this.dw_dates)
end on

on close;call w_base_response_ext::close;If Not ib_valid_return Then
	Message.StringParm = ""
End if
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date		ldt_begin, &
			ldt_end

String	ls_temp, &
			ls_return


If dw_dates.AcceptText() = -1 Then return 

ldt_begin = dw_dates.GetItemDate(1, "begin_date")
ls_temp = String(ldt_begin, "yyyy-mm-dd")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	iw_frame.SetMicroHelp("Begin Date is a required field")
	dw_dates.SetColumn("begin_date")
	dw_dates.SetFocus()
	return 
End if
ls_return = Trim(ls_temp) + '~t'

ldt_end = dw_dates.GetItemDate(1, "end_date")
ls_temp = String(ldt_end, "yyyy-mm-dd")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	iw_frame.SetMicroHelp("End Date is a required field")
	dw_dates.SetColumn("end_date")
	dw_dates.SetFocus()
	return 
End if
ls_return += Trim(ls_temp) + '~t'

If ldt_end < ldt_begin Then
	iw_frame.SetMicroHelp("End Date cannot be less than Begin Date")
	dw_dates.SetFocus()
	return 
End if

If ldt_end > idt_EndDate Then
	iw_frame.SetMicroHelp("End Date cannot be greater than the maximum instances end date")
	dw_dates.SetFocus()
	return 
End if

If (iw_parent_window.dw_header.GetItemString(1, "display_status") = "W") and (dw_dates.GetItemString(1, "pattern") = "Y") Then
	If DayNumber(dw_dates.GetItemDate(1, "begin_date")) <> 2 Then
		iw_frame.SetMicroHelp("Begin Date must be a Monday for a pattern")
		dw_dates.SetColumn("begin_date")
		dw_dates.SetFocus()
		return 
	End If
End If

If (iw_parent_window.dw_header.GetItemString(1, "display_status") = "W") and (dw_dates.GetItemString(1, "pattern") = "Y") Then
	If DayNumber(dw_dates.GetItemDate(1, "end_date")) <> 1 Then
		iw_frame.SetMicroHelp("End Date must be a Sunday for a pattern")
		dw_dates.SetColumn("end_date")
		dw_dates.SetFocus()
		return 
	End If
End If

ls_return += dw_dates.GetItemString(1, "pattern")
	
ib_valid_return = True
CloseWithReturn(This, ls_return)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_instance_date_range
integer x = 576
integer y = 464
integer width = 219
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_instance_date_range
integer x = 288
integer y = 464
integer width = 247
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_instance_date_range
integer x = 27
integer y = 464
integer width = 219
integer taborder = 40
end type

type dw_dates from u_base_dw_ext within w_pas_instance_date_range
integer x = 5
integer y = 12
integer width = 777
integer height = 396
integer taborder = 30
string dataobject = "d_pas_instance_date_range"
boolean border = false
end type

event itemerror;call super::itemerror;return 1
end event

event itemchanged;call super::itemchanged;String		ls_column

Choose Case dwo.name
		
	Case 	"begin_date",  "end_date"

		If Date(This.GetText()) < RelativeDate(Today(), 0 - ii_PaPast) Then
			iw_frame.SetMicroHelp("Date cannot be less than " + &
						String(RelativeDate(Today(), 0 - ii_PaPast), "mm/dd/yyyy"))
			This.SelectText(1, 100)
			Return 1
		End If 
End Choose		

end event

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;This.SelectText(1, 12)
end on

event constructor;call super::constructor;This.InsertRow(0)
This.SetItem(1, 'Begin_date', Today())
//This.SetItem(1, 'End_date', Date('2999-12-31'))
end event

on getfocus;call u_base_dw_ext::getfocus;This.SelectText(1, 12)
end on

