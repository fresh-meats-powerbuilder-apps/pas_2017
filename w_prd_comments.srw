$PBExportHeader$w_prd_comments.srw
forward
global type w_prd_comments from w_base_sheet_ext
end type
type dw_prd_comments_dtl from u_base_dw_ext within w_prd_comments
end type
type dw_header from u_base_dw_ext within w_prd_comments
end type
end forward

global type w_prd_comments from w_base_sheet_ext
integer width = 2597
integer height = 1744
string title = "CommentsUpdate"
long backcolor = 67108864
dw_prd_comments_dtl dw_prd_comments_dtl
dw_header dw_header
end type
global w_prd_comments w_prd_comments

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
DataStore	ids_tree

Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product	
				


nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

Boolean	ib_first_time, &
			ib_expand_all, &
			ib_set_redraw = False, &
			ib_update_successful, &
			ib_inquire_required, &
			ib_endoftree, &
			ib_compute_yields, &
			ib_first_compute

s_error		istr_error_info

u_pas201		iu_pas201
u_pas203		iu_pas203

String		is_input, &
				is_debug, &
				is_chain, &
				is_name_string
	
DataWindowChild		idwc_sect_desc

u_sect_functions		iu_sect_functions
end variables

forward prototypes
public function boolean wf_addrow ()
public function boolean wf_retrieve ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function boolean wf_deleterow ()
public subroutine wf_delete ()
end prototypes

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row
				
String		ls_ind, &
				ls_filter

If dw_prd_comments_dtl.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_row = dw_prd_comments_dtl.InsertRow(0)	

dw_prd_comments_dtl.ScrollToRow(ll_row)
dw_prd_comments_dtl.SetItem(ll_row, "update_flag", 'A')
dw_prd_comments_dtl.SetColumn("comments")
dw_prd_comments_dtl.SetFocus()
This.SetRedraw(True)

return true



end function

public function boolean wf_retrieve (); Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_chain, &
			ls_output_values, ls_temp, &
			ls_name_code, ls_comments
			
Long		ll_value, &
			ll_rtn, &
			ll_row, ll_qty, &
			ll_hours, ll_speed
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_prd_comments_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")

//wf_get_sect_desc()

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			String(This.dw_header.GetItemString(1, 'area_name_code')) + '~t' + &
			String(dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + &
			This.dw_header.GetItemString(1, "Shift") + '~r~n'

is_input = ls_input

li_ret = iu_pas201.nf_pasp62cr_inq_sched_comments(istr_error_info, &
									is_input, &
									ls_output_values)

This.dw_prd_comments_dtl.Reset()
If li_ret = 0 Then
	This.dw_prd_comments_dtl.ImportString(ls_output_values)
End If

li_row_count = dw_prd_comments_dtl.RowCount()


ll_value = dw_prd_comments_dtl.RowCount()
If ll_value < 0 Then ll_value = 0
ll_row = 1 
DO WHILE ll_Row <= ll_value
	ls_comments = RightTrim(dw_prd_comments_dtl.GetItemString(ll_row, "comments"))
  	dw_prd_comments_dtl.SetItem(ll_row, "comments", ls_comments)
	ll_row ++
LOOP

dw_prd_comments_dtl.ResetUpdate()

IF ll_value > 0 THEN
	dw_prd_comments_dtl.SetFocus()
	dw_prd_comments_dtl.ScrollToRow(1)
	dw_prd_comments_dtl.SetColumn( "comments" )
	dw_prd_comments_dtl.TriggerEvent("RowFocusChanged")
END IF

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )

dw_prd_comments_dtl.ResetUpdate()

Return True

end function

public function boolean wf_validate (long al_row);Long					ll_rtn, &
						ll_nbrrows, &
						ll_ModifiedRow
						
									
						
IF dw_prd_comments_dtl.AcceptText() = -1 THEN Return( False )

ll_nbrrows = dw_prd_comments_dtl.RowCount()


If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True
//dw_prd_comments_dtl.SetItem(al_row, "update_flag", 'A')
dw_prd_comments_dtl.SelectRow(0, False)
Return True
end function
public function boolean wf_update ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp

string			ls_Update_string, &
					ls_header_string, &
					ls_output_string, &
					ls_ind, &
					ls_input, &
					ls_flag
										

IF dw_prd_comments_dtl.AcceptText() = -1 THEN Return( False )
ll_temp = dw_prd_comments_dtl.DeletedCount()
//IF dw_prd_comment_dtl.ModifiedCount() + dw_prd_comment_dtl.DeletedCount() <= 0 THEN Return( False )

ll_NbrRows = dw_prd_comments_dtl.RowCount( )
if ll_NbrRows <= 0 And ll_temp <=0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

//ll_NbrRows = dw_prd_comment_dtl.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_prd_comments_dtl.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP
			
			 
ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			String(This.dw_header.GetItemString(1, 'area_name_code')) + '~t' + &
			String(dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + &
			This.dw_header.GetItemString(1, "Shift") + '~r~n'
			

is_input = ls_input
ls_header_string = is_input
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_prd_comments_dtl)
//string(dw_prd_comment_dtl.getItemNumber(1, "qty_compute")) + '~r~n'
istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp63cr_upd_sched_comments"
istr_error_info.se_message = Space(71)

If not iu_pas201.nf_pasp63cr_upd_sched_comments(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then 
	Return False
	end if

ib_ReInquire = True
This.post wf_retrieve()
dw_prd_comments_dtl.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = idwc_sect_desc.RowCount()
//For li_counter = 1 to ll_RowCount
//	idwc_sect_desc.SetItem(li_Counter, 'available_ind', 'Y')
//Next	
//
ll_RowCount = dw_prd_comments_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
//	ls_name_code = String(dw_prd_comments_dtl.GetItemNumber(li_counter, "name_code"))
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code, 'N') 
	dw_prd_comments_dtl.SetItem(li_Counter, 'update_flag', ' ')
Next
//ib_ReInquire = True
//wf_set_filter('N')
dw_prd_comments_dtl.ResetUpdate()
//dw_prd_comments_dtl.SetSort("sequence A, product A, state A, char_key A")
//dw_prd_comments_dtl.Sort()
dw_prd_comments_dtl.SetFocus()
dw_prd_comments_dtl.SetReDraw(True)

Return( True )
end function
public function boolean wf_deleterow ();Boolean		lb_ret
//
//Long			ll_row
//
//This.SetRedraw( False )
//ll_row = dw_prd_comments_dtl.GetRow()
//
//If ll_row < 1 Then
//	Return True
//End If
//
//dw_prd_comments_dtl.SetItem(ll_row, "update_flag", 'D')
//
//dw_prd_comments_dtl.SetFocus()
//lb_ret = super::wf_deleterow()
//
//dw_prd_comments_dtl.SelectRow(0,False)
//
//This.SetRedraw( True )
return lb_ret
end function
public subroutine wf_delete ();//wf_deleterow()
end subroutine
event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)




end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;

wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event
event open;call super::open;String				ls_text

dw_header.InsertRow(0)
//dw_prd_carcass_chain.InsertRow(0)
//dw_prd_carcass_speed.InsertRow(0)
dw_prd_comments_dtl.InsertRow(0)


end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Schedule Comments"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")
end event

on w_prd_comments.create
int iCurrent
call super::create
this.dw_prd_comments_dtl=create dw_prd_comments_dtl
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prd_comments_dtl
this.Control[iCurrent+2]=this.dw_header
end on

on w_prd_comments.destroy
call super::destroy
destroy(this.dw_prd_comments_dtl)
destroy(this.dw_header)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Name Type'
		Message.StringParm = dw_header.GetItemString(1, 'name_type')
	Case 'Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'area_name_code')
	Case 'Sched Date'
		Message.StringParm = string(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
	Case 'Shift'
		Message.StringParm = dw_header.GetItemString(1, 'Shift')	
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Name Type'
		dw_header.SetItem(1, 'name_type', as_value)
	Case 'Name Code'
		dw_header.SetItem(1, 'area_name_code', as_value)
	Case 'Type Descr'
		dw_header.SetItem(1, 'type_descr', as_value)	
	Case 'Sched Date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
	Case 'Shift'
		dw_header.SetItem(1, 'Shift', as_value)		
End Choose


end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_prd_comments_dtl.x * 2) + 30 
li_y = dw_prd_comments_dtl.y + 115

if width > li_x Then
	dw_prd_comments_dtl.width	= width - li_x
end if

if height > li_y then
	dw_prd_comments_dtl.height	= height - li_y
end if
end event

type dw_prd_comments_dtl from u_base_dw_ext within w_prd_comments
integer y = 352
integer width = 2345
integer height = 408
integer taborder = 10
string dataobject = "d_prd_comments_detail"
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_temp
				
Date			ldt_temp	

nvuo_pa_business_rules	u_rule


ll_source_row	= GetRow()
il_ChangedRow = 0

IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

//parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0


end event
type dw_header from u_base_dw_ext within w_prd_comments
integer y = 12
integer width = 1216
integer height = 324
integer taborder = 0
boolean enabled = false
string dataobject = "d_prd_carcass_header"
boolean border = false
end type

event constructor;call super::constructor;this.ib_updateable = False
DataWindowChild		ldwc_type


This.GetChild("product_state", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PRDSTATE")


end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

