HA$PBExportHeader$w_product_translation.srw
forward
global type w_product_translation from w_netwise_sheet
end type
type dw_prodtran from u_prodtran_code within w_product_translation
end type
type dw_detail from u_base_dw_ext within w_product_translation
end type
type dw_division from u_division within w_product_translation
end type
end forward

global type w_product_translation from w_netwise_sheet
integer x = 0
integer y = 0
integer width = 3081
integer height = 1840
string title = "Product Translation"
long backcolor = 12632256
dw_prodtran dw_prodtran
dw_detail dw_detail
dw_division dw_division
end type
global w_product_translation w_product_translation

type variables
s_error	istr_error_info

u_pas203		iu_pas203
u_ws_pas_share	iu_ws_pas_share
u_ws_pas3		iu_ws_pas3

// These are used in wf_update_modify()
// to build the update string
string		is_update_string = ""
integer		ii_rec_count
boolean		ib_updating

end variables

forward prototypes
public subroutine wf_filenew ()
public function boolean wf_deleterow ()
public subroutine wf_delete ()
public function boolean wf_update ()
public function boolean wf_check_required ()
public function boolean wf_addrow ()
public function boolean wf_retrieve ()
public function boolean wf_update_modify (ref string as_header, ref long al_row, ref character ac_update_flag)
end prototypes

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_deleterow ();Long	ll_row

ll_row = dw_detail.GetRow()

If ll_row < 1 Then return true

String ls_ret
ls_ret = dw_detail.Describe("Evaluate('IsRowNew()', " + String(ll_row) + ")")
If Trim(Lower(ls_ret)) = 'true' Then
	dw_detail.RowsDiscard(ll_row,ll_row,Primary!)
Else
	dw_detail.SelectRow(ll_row,True)
	If MessageBox("Product Translation","Are you sure you want to delete the current row?", &
		Question!,YesNo!) = 1 Then
		dw_detail.RowsMove(ll_row,ll_row,Primary!,dw_detail,10000,Delete!)
	End If
	dw_detail.SelectRow(0,False)
End If

return true
end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_update ();long				ll_count

long				ll_Row, &
					ll_modrows, &
					ll_delrows

Char				lc_status_ind					

string			ls_header, &
					ls_division, &
					ls_prodtran

IF dw_detail.AcceptText() = -1 Then Return False

SetPointer(HourGlass!)

ls_division = dw_division.uf_get_division()

If iw_frame.iu_string.nf_IsEmpty(ls_division) Then 
	iw_frame.SetMicroHelp("Please enter a division before updating")
	Return False
End If


ls_prodtran = dw_prodtran.uf_get_prodtran()

If iw_frame.iu_string.nf_IsEmpty(ls_prodtran) Then 
	iw_frame.SetMicroHelp("Please enter a product translation before updating")
	Return False
End If

ls_header = ls_division + '~t' + &
				ls_prodtran + '~t' + '~r~n'
//				"~tMH~r~n"
//ls_header += "~tMH~r~n"

ll_modrows = dw_detail.ModifiedCount()
ll_delrows = dw_detail.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0 Then Return False

ib_updating = True
If Not This.wf_check_required() Then 
	ib_updating = False
	Return False
End If
is_update_string = ""
ii_rec_count = 0

For ll_count = 1 To ll_delrows 

	lc_status_ind = 'D'

	If Not This.wf_update_modify(ls_header, ll_count, lc_status_ind) Then 
		ib_updating = False
		Return False
	End If

Next

ll_row = 0

For ll_count = 1 To ll_modrows

	ll_Row = dw_detail.GetNextModified(ll_Row, Primary!) 

//	If Not wf_validate(ll_row) Then 
//		ib_updating = False
//		Return False
//	End If

	Choose Case dw_detail.GetItemStatus(ll_row, 0, Primary!)
		CASE NewModified!
			lc_status_ind = 'A'
		CASE DataModified!
			lc_status_ind = 'M'
	END CHOOSE	

	If Not This.wf_update_modify(ls_header, ll_row, lc_status_ind) Then 
		ib_updating = False
		Return False
	End If

Next


//dw_detail.SetRedraw(False)
If ii_rec_count > 0 Then
//	IF Not iu_pas203.nf_upd_product_translation(istr_error_info, ls_header, &
//												is_update_string) Then 
	IF Not iu_ws_pas3.uf_pasp53fr(istr_error_info, ls_header, &
												is_update_string, "ProdTran") Then 												
		ib_updating = False
		dw_detail.SetRedraw(True)
		Return False
	End If
End If

ib_updating = False
iw_frame.SetMicroHelp("Modification Successful")
dw_detail.ResetUpdate()
dw_detail.SetRedraw(True)

Return( True )

end function

public function boolean wf_check_required ();Long	ll_row = 0
String	ls_column, &
			ls_temp, &
			ls_default_age_ck, & 
			ls_default_age  

ll_row = dw_detail.Find("Len(Trim(product_code)) = 0 or Len(Trim(sku_product_code)) = 0 or Len(Trim(product_state)) > 0" + &
		" or IsNull(product_code) or IsNull(sku_product_code)", &
		1, dw_detail.RowCount())
		
If ll_row > 0 Then
	Beep(1)
	dw_detail.ScrollToRow(ll_row)
	ls_temp = dw_detail.GetItemString(ll_row,"product_code")
	If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
		dw_detail.SetColumn("product_code")
		iw_frame.SetMicroHelp("Material Handling Product Code is a required field")
		return false
	Else
		ls_temp = dw_detail.GetItemString(ll_row,"sku_product_code")
		If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
			dw_detail.SetColumn("sku_product_code")
			iw_frame.SetMicroHelp("SKU Product Code is a required field")
			return false
		End IF
	End If
End If
If ll_row > 0 Then
ls_default_age_ck = dw_detail.GetValue("default_age", 1)
ls_default_age = dw_detail.GetItemString(ll_row,"default_age")
	IF ls_default_age = ' ' then
		If iw_frame.iu_string.nf_IsEmpty(ls_default_age_ck) Then
	//IF ls_default_age = ' ' and ls_default_age_ck <> ' ' then
			Else
				iw_frame.SetMicroHelp("Default age is not valid")
				dw_detail.SelectText(1, 100)
				dw_detail.ScrollToRow(ll_row)
				dw_detail.SetColumn("default_age")
				dw_detail.SetFocus()
				This.SetRedraw(True)
			return False
		End IF	
	End IF
End IF
return true
end function

public function boolean wf_addrow ();Long ll_row 
string  ls_prodtran

ll_row = dw_detail.InsertRow(0)
dw_detail.SetFocus()
dw_detail.ScrollToRow(ll_row)
dw_detail.SetColumn("product_code")

ls_prodtran = dw_prodtran.GetItemString(1,"prodtran_code")
if ls_prodtran = 'RM' then
	dw_detail.SetItem(ll_row,"translation_type",dw_prodtran.GetItemString(1,"prodtran_code"))
end if

return true
end function

public function boolean wf_retrieve ();Int			li_ret

String		ls_detail_string, &
				ls_division, & 
				ls_prodtran
				
Long			ll_rec_count
				
Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return false

OpenWithParm(w_product_translation_inq, This)

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

ls_division = left(Message.StringParm, 2)
ls_prodtran = right(Message.StringParm, 2)
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then Return False

dw_detail.Reset()
dw_division.uf_set_division(ls_division)
dw_prodtran.uf_set_prodtran(ls_prodtran)
if ls_prodtran = 'RM' then
   	dw_detail.Object.default_age.Visible = True
	dw_detail.Object.product_state.Visible = True
	dw_detail.Object.conversion_ratio.Visible = True
	dw_detail.Object.conversion_type.Visible = True
else
	dw_detail.Object.default_age.Visible = False
	dw_detail.Object.product_state.Visible = False
	dw_detail.Object.conversion_ratio.Visible = False
	dw_detail.Object.conversion_type.Visible = False	
End IF
	

istr_error_info.se_event_name = "wf_retrieve"
//If Not iu_pas203.nf_inq_product_translation(istr_error_info, &
//											ls_division + '~t' + &
//											ls_prodtran + '~t' + &
//											"~r~n", &
//											ls_detail_string) Then Return False
											
If Not iu_ws_pas3.uf_pasp52fr(istr_error_info, &
											ls_division, &
											ls_prodtran, &
											"ProdTran", + &
											ls_detail_string) Then Return False											

SetRedraw (False)

ls_detail_string = Trim(ls_detail_string)
ll_rec_count = dw_detail.ImportString(ls_detail_string)

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved"  )
		dw_detail.ResetUpdate()
	
	dw_detail.Sort()
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If	
//dw_detail.SetColumn('default_age')
SetRedraw(True)

Return True
end function

public function boolean wf_update_modify (ref string as_header, ref long al_row, ref character ac_update_flag);DWBuffer						ldwb_buffer

String			ls_default_age, ls_default_age_ck, ls_translation
string			ls_sku, ls_product, ls_descr, ls_state, ls_default, ls_conversion_ratio, ls_conversion_type
SetPointer(HourGlass!)

IF ac_update_flag = "D" THEN
	ldwb_buffer = delete!
ELSE
	ldwb_buffer = primary!
End If

ls_default_age_ck = dw_detail.GetValue("default_age", 1)
ls_default_age = dw_detail.GetItemString(al_row,"default_age")
	IF ls_default_age = ' ' then
		If iw_frame.iu_string.nf_IsEmpty(ls_default_age_ck) Then
			Else
				iw_frame.SetMicroHelp("Default age is not valid")
				dw_detail.SelectText(1, 100)
				dw_detail.ScrollToRow(al_row)
				dw_detail.SetColumn("default_age")
				dw_detail.SetFocus()
				This.SetRedraw(True)
			return False
		End IF	
	End IF
ls_translation = dw_detail.GetItemString(al_row,"translation_type")
ls_sku = dw_detail.GetItemString(al_row,"sku_product_code")
ls_product = dw_detail.GetItemString(al_row,"product_code")
ls_descr = trim(dw_detail.GetItemString(al_row,"product_descr"))
ls_state = dw_detail.GetItemString(al_row,"product_state")
if ls_translation = 'RM' then
	ls_default = dw_detail.GetItemString(al_row,"default_age")
	ls_state = dw_detail.GetItemString(al_row,"product_state")
	 ls_conversion_ratio = String(dw_detail.GetItemNumber(al_row, "conversion_ratio"))
	 ls_conversion_type = dw_detail.GetItemString(al_row, "conversion_type")
else
	ls_default = ' '
	ls_state = ' '
	ls_conversion_ratio = ' '
	ls_conversion_type =	 ' '
end if
if IsNull(ls_default) then
		ls_default = ' '
end if
if IsNull(ls_conversion_ratio) then
	ls_conversion_ratio = ' '
end if
if IsNull(ls_default) then
		ls_default = ' '
end if
if IsNull(ls_conversion_type) then
	ls_conversion_type = ' '
end if
is_update_string += dw_detail.GetItemString &
	(al_row, "translation_type",ldwb_buffer, False) + "~t" + &
	dw_detail.GetItemString &
	(al_row, "sku_product_code",ldwb_buffer, False) + "~t" + &
	dw_detail.GetItemString &
	(al_row, "product_code",ldwb_buffer, False) + "~t" + &
	ls_descr + "~t" + &
	ls_state + "~t" + &
	ls_default + "~t" + &
	ls_conversion_ratio + "~t" + &
	ls_conversion_type + "~t" + &
	ac_update_flag + "~r~n"
//	dw_detail.GetItemString &
//	(al_row, "product_descr",ldwb_buffer, False) + "~t" + &	
	
//	dw_detail.GetItemString &
//	(al_row, "product_state",ldwb_buffer, False) + "~t" + &
//	dw_detail.GetItemString &
//	(al_row, "default_age",ldwb_buffer, False) + "~t" + &	
//	dw_detail.GetItemString &
//	(al_row, "code_id",ldwb_buffer, False) + "~t" + &
	
ii_rec_count ++

If ii_rec_count = 100 Then
//	IF Not iu_pas203.nf_upd_product_translation( &
//			istr_error_info, &
//			as_header, &
//			is_update_string) 			THEN Return False
	IF Not iu_ws_pas3.uf_pasp53fr( &
			istr_error_info, &
			as_header, &
			is_update_string, &
		 	"ProdTran") 			THEN Return False
	ii_rec_count = 0
	is_update_string = ""
END IF

Return True
end function

event resize;call super::resize;dw_detail.width = newwidth - 66
dw_detail.height = newheight - 160

end event

on w_product_translation.create
int iCurrent
call super::create
this.dw_prodtran=create dw_prodtran
this.dw_detail=create dw_detail
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodtran
this.Control[iCurrent+2]=this.dw_detail
this.Control[iCurrent+3]=this.dw_division
end on

on w_product_translation.destroy
call super::destroy
destroy(this.dw_prodtran)
destroy(this.dw_detail)
destroy(this.dw_division)
end on

event ue_postopen;call super::ue_postopen;iu_pas203 = create u_pas203
iu_ws_pas3 = create u_ws_pas3
iu_ws_pas_share = create u_ws_pas_share
If Message.ReturnValue = -1 Then
	Close(This)
	Return
End If

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "ProdTrans"
istr_error_info.se_user_id				= sqlca.userid

This.Post wf_retrieve()
end event

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF
IF IsValid( iu_ws_pas3 ) THEN
	DESTROY iu_ws_pas3
END IF

Destroy iu_ws_pas_share

end event

event ue_fileprint;call super::ue_fileprint;DataStore	lds_print


If Not ib_print_ok Then Return

lds_print = Create u_print_datastore
lds_print.DataObject = "d_product_translation_print"
dw_detail.ShareData(lds_print)
lds_print.Modify("division_t.Text='" + dw_division.uf_get_division() + "'")
lds_print.Modify("division_descr_t.Text='" + dw_division.uf_get_division_descr() + "'")

lds_print.Modify("prodtran_t.Text='" + dw_prodtran.uf_get_prodtran() + "'")
lds_print.Modify("prodtran_descr_t.Text='" + dw_prodtran.uf_get_prodtran_descr() + "'")

lds_print.Print(True)

Destroy lds_print
return
end event

type dw_prodtran from u_prodtran_code within w_product_translation
integer x = 32
integer y = 128
integer taborder = 0
end type

event constructor;call super::constructor;ib_updateable = False
This.Modify("prodtran_code.Protect='1' prodtran_code.Background.Color='12632256' prodtran_code.Pointer='Arrow!'")

end event

type dw_detail from u_base_dw_ext within w_product_translation
integer y = 216
integer width = 2949
integer height = 1200
integer taborder = 10
string dataobject = "d_product_translation"
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;Long		ll_findrow
Boolean	lb_ret
Double	ld_value 
String		ls_sku_product, ls_default_age_ck, &
			ls_default_age, ls_translation_type, &
			ls_age_code, ls_frz_age_code, ls_product_state, &
			ls_sku_product_code, ls_temp,  ls_age_schedule_code
integer  i

data = Trim(data)

Choose Case Lower(Trim(dwo.Name))
	Case "product_code"
		ll_findrow = dw_detail.Find("Trim(product_code) = '" + Trim(data) + "'",1,dw_detail.RowCount())
		If ll_findrow > 0 And ll_findrow <> row And &
						Len(Trim(data)) > 0 Then
			SelectText(1, 100)
			iw_frame.SetMicroHelp("Material Handling Product Code must be unique")
			return 1
		End If
		dwItemStatus ldwis
		ldwis = This.GetItemStatus(row,0,Primary!)
		If ldwis = DataModified! OR ldwis = NotModified! Then
			ls_temp = This.GetItemString(row,"product_code")
			
			// Must do a delete and insert
			ll_findrow = This.InsertRow(0)
			This.SetItem(ll_findrow,"product_code",Trim(data))
			This.SetItem(ll_findrow,"product_descr",This.GetItemString(row,"product_descr"))
			This.SetItem(ll_findrow,"sku_product_code",This.GetItemString(row,"sku_product_code"))
			This.SetItem(ll_findrow,"product_state",This.GetItemString(row,"product_state"))
			This.SetItem(ll_findrow,"default_age",This.GetItemString(row,"default_age"))
			This.SetItem(row,"product_code",ls_temp)
			This.RowsMove(row,row,Primary!,dw_detail,10000,Delete!)
			This.Sort()
		End If

	Case "product_descr"
		If Len(Trim(data)) > 30 Then
			iw_frame.SetMicroHelp("Product Description must be 30 or fewer characters")
			This.SelectText(1, 100)
			return 1
		End If
	Case "sku_product_code"
	//	lb_ret = iu_pas203.nf_inq_sku_product(istr_error_info,data,ls_sku_product) iu_ws_pas3
		lb_ret = iu_ws_pas_share.nf_pasp45fr(istr_error_info,data,ls_sku_product)
		If Not lb_ret Then
			iw_frame.SetMicroHelp(data + " is not an SKU Product Code")
			This.SelectText(1, 100)
			return 1
		Else
			iw_frame.SetMicroHelp("Ready")
		End If
	Case "product_state"	
//		This.SetItem(row,"default_age",' ')
//		This.ClearValues("default_age")
	ls_sku_product_code = dw_detail.GetItemString(row, 'sku_product_code') 
	ls_product_state = dw_detail.GetItemString(row, 'product_state') 
	ls_translation_type = dw_detail.GetItemString(row, 'translation_type') 
	if ls_translation_type = 'RM' Then
		SELECT age_schedule_code
				,frz_age_schd_code
		INTO :ls_age_code
			 ,:ls_frz_age_code
		FROM sku_products 
  		WHERE ( sku_product_code = :ls_sku_product_code ) ;
		If SQLCA.SQLCode = 00 Then
			This.SetItem(row,"default_age",' ')
			CHOOSE CASE ls_product_state	
					CASE '1'
						ls_age_schedule_code = ls_age_code
					CASE '3'	
						ls_age_schedule_code = ls_age_code
					CASE '2'	
						ls_age_schedule_code = ls_frz_age_code	
					CASE '4'	
						ls_age_schedule_code = ls_frz_age_code		
					END CHOOSE		
						
					DECLARE option_cur CURSOR FOR
					SELECT 	age_code
					FROM 		mt_tskuage
					Where 	(mt_tskuage.age_schedule_code = :ls_age_schedule_code)
					Order by  age_code;
				// Declare a destination variable for the options.
				// Open The Cursor.
					OPEN option_cur;
				// Fetch the first row from the result set.
					FETCH option_cur INTO :ls_temp;
				// Loop through result set until exhausted.
					DO WHILE sqlca.sqlcode = 0
						dw_detail.SetValue("default_age", i, &
							ls_temp) 
							i = i + 1
						FETCH option_cur INTO :ls_temp;
					LOOP
				// All done, so close the cursor.
				CLOSE option_cur;	
				dw_detail.SetItem(row, 'default_age', ' ') 
				AcceptText()
			End IF
		End IF	
	Case "default_age"
			ls_default_age_ck = dw_detail.GetValue("default_age", 1)
			ls_default_age = dw_detail.GetItemString(row,"default_age")
			//IF data = ' ' and ls_default_age_ck <> ' ' then
			IF data = ' ' then
				if ls_default_age_ck <> '"' then
					iw_frame.SetMicroHelp("Default age is not valid")
					This.SelectText(1, 100)
					return 1
				End IF
			End IF	
	Case "conversion_ratio"	
			If Not IsNumber(data) Then 
				This.selecttext(1, 100)
				iw_frame.SetMicroHelp('Conversion Ratio must be a number between -99999.9999 and 99999.9999')
				Return 1
			End If
			
			ld_value = Double(data)
			
			If ld_value > 99999.9999 or ld_value < -99999.9999 then
				This.selecttext(1, 100)
				iw_frame.SetMicroHelp('Conversion Ratio must be a number between -99999.9999 and 99999.9999')
				Return 1
			End If
	Case Else
		
End Choose
return 0
end event

event itemerror;call super::itemerror;Integer li_length
String	ls_name

Choose Case dwo.Name
	Case "product_descr"
		li_length = 30
		ls_name = "Product Description"
	Case "product_code"
		li_length = 18
		ls_name = "Material Handling Product Code"
	Case "sku_product_code"
		li_length = 10
		ls_name = "SKU Product Code"
	Case "product_state"
		li_length = 2
		ls_name = "Product State"	
	Case "default_age"
		li_length = 1
		ls_name = "Default Age"		
	Case "conversion_type"
		li_length = 3
		ls_name = "Conversion Type"			
	Case "conversion_ratio"
		li_length = 20
		ls_name = "Conversion Ratio"			
End Choose

If Len(Trim(data)) <= li_length Then
//	This.SetItem(row,String(dwo.Name),Trim(data))
	return 2
Else
	iw_frame.SetMicroHelp(ls_name + " must be " + String(li_length) + " or fewer characters")
	This.SelectText(1,Len(data))
End If

return 1
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1,Len(This.GetText()))
end event

event getfocus;call super::getfocus;This.SelectText(1,Len(This.GetText()))
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state, ldwc_type

string 						ls_translation_type
long							ll_row

dw_detail.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()

//dw_detail.GetChild('translation_type', ldwc_state)
//ll_row = This.GetRow()
//ls_translation_type = dw_detail.GetItemString(LL_row, 'translation_type')
//if ls_translation_type = 'RM' then
//	This.object.default_age.Background.Color = 16777215
//	This.object.default.Protect = 0
//Else
//	This.object.default_age.Background.Color = 12632256
//	This.object.default_age.Protect = 1
//End If
//

dw_detail.GetChild('conversion_type', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PRODTRUM")
ldwc_type.SetSort("type_code")
ldwc_type.Sort()


end event

event clicked;call super::clicked;String 	ls_state, &	
			ls_ColumnName, &
			ls_sku_product_code, &
			ls_temp_string, &
			ls_product_state, &
			ls_translation_type, &
			ls_age_code, &
			ls_frz_age_code, &
			ls_default_age, &
			ls_age_schedule_code, &
			ls_default_age_string, &
			ls_default_age_ck
integer  i = 1
			

long		ll_rtn

Integer	li_rtn, &
			li_start_pos

DataWindowChild			ldwc_age

dw_detail.SetRedraw(False)

ldwc_age.Reset()
ll_rtn = 0
li_rtn = 0

//ll_row = This.GetRow()
if row > 0 then

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

ls_sku_product_code = dw_detail.GetItemString(row, 'sku_product_code') 
ls_product_state = dw_detail.GetItemString(row, 'product_state') 
ls_translation_type = dw_detail.GetItemString(row, 'translation_type') 
if ls_columnName = "product_state" then
	dw_detail.SetItem(row, 'default_age', ' ') 
End IF
//	
If ls_ColumnName = "default_age" Then
//If ls_ColumnName = "product_state" Then
	if ls_translation_type = 'RM' Then
		dw_detail.ClearValues("default_age")
		SELECT age_schedule_code
				,frz_age_schd_code
		INTO :ls_age_code
			 ,:ls_frz_age_code
		FROM sku_products 
  		WHERE ( sku_product_code = :ls_sku_product_code ) ;
		If SQLCA.SQLCode = 00 Then
			This.SetItem(row,"default_age",' ')
			CHOOSE CASE ls_product_state	
					CASE '1'
						ls_age_schedule_code = ls_age_code
					CASE '3'	
						ls_age_schedule_code = ls_age_code
					CASE '2'	
						ls_age_schedule_code = ls_frz_age_code	
					CASE '4'	
						ls_age_schedule_code = ls_frz_age_code		
					END CHOOSE		
						
					DECLARE option_cur CURSOR FOR
					SELECT 	age_code
					FROM 		mt_tskuage
					Where 	(mt_tskuage.age_schedule_code = :ls_age_schedule_code)
					Order By  age_code;
				// Declare a destination variable for the options.
					string	ls_temp 
				// Open The Cursor.
					OPEN option_cur;
				// Fetch the first row from the result set.
					FETCH option_cur INTO :ls_temp;
				// Loop through result set until exhausted.
					DO WHILE sqlca.sqlcode = 0
						dw_detail.SetValue("default_age", i, &
							ls_temp) 
							i = i + 1
						FETCH option_cur INTO :ls_temp;
					LOOP
				// All done, so close the cursor.
				CLOSE option_cur;	
				dw_detail.SetItem(row, 'default_age', ' ') 
				AcceptText()
			End IF
		End IF	
	End IF
End IF

dw_detail.SetRedraw(True)
dw_detail.AcceptText()
	

end event

type dw_division from u_division within w_product_translation
integer x = 32
integer y = 32
integer width = 1687
integer taborder = 0
end type

event constructor;call super::constructor;ib_updateable = False
This.Modify("division_code.Protect='1' division_code.Background.Color='12632256' division_code.Pointer='Arrow!'")

end event

