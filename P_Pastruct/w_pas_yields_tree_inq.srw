HA$PBExportHeader$w_pas_yields_tree_inq.srw
forward
global type w_pas_yields_tree_inq from w_base_response_ext
end type
type dw_inquire from u_base_dw_ext within w_pas_yields_tree_inq
end type
type dw_fab_product_code from u_fab_product_code within w_pas_yields_tree_inq
end type
end forward

global type w_pas_yields_tree_inq from w_base_response_ext
integer x = 293
integer y = 480
integer width = 2395
integer height = 776
long backcolor = 67108864
dw_inquire dw_inquire
dw_fab_product_code dw_fab_product_code
end type
global w_pas_yields_tree_inq w_pas_yields_tree_inq

type variables
Boolean		ib_need_inquire

String		is_parent_string, &
				is_input_dates_string
				
Private:
u_pas201		iu_pas201
u_pas203		iu_pas203
u_ws_pas3		iu_ws_pas3

s_error		istr_error_info

datawindowchild			idddw_child
nvuo_fab_product_code	invuo_fab_product_code
end variables

forward prototypes
public function boolean wf_convert_dates (ref string as_input_dates_string)
end prototypes

public function boolean wf_convert_dates (ref string as_input_dates_string);DataStore			lds_dates
Integer				li_count
Long					ll_rowcount, &
						ll_row
Date					ldt_end_date, &
						ldt_begin_date, &
						ldt_tomorrow

String				ls_pending_ind
						

//All this code is hacked up, because I had to convert the date fields to string fields.  I 
//had to do this because of the problem with Powerbuilder using the date format used the the
//regional settings.  If you have any better ideas please feel free.  There is also a description
// of this problem in the itemchanged event of dw_inquire.
lds_dates = Create Datastore
lds_dates.dataobject = 'd_dates_dddw'

lds_dates.ImportString(as_input_dates_string)

ls_pending_ind = dw_inquire.GetItemString(1, 'effective_pending_ind')
ldt_tomorrow = RelativeDate(Today(), +1)
ll_rowcount = lds_dates.RowCount()

If ll_rowcount > 1 Then
	For li_count = 1 to ll_rowcount - 1
		ldt_begin_date = Date(lds_dates.GetItemString(li_count, 'begin_date'))
		ldt_end_date = RelativeDate(Date(lds_dates.GetItemString(li_count + 1, 'begin_date')), -1)
		lds_dates.SetItem(li_count, 'end_date', String(ldt_end_date, 'mm/dd/yyyy'))
		lds_dates.SetItem(li_count, 'begin_date', String(ldt_begin_date, 'mm/dd/yyyy'))
		If ldt_end_date < ldt_begin_date Then 
			lds_dates.DeleteRow(li_count)
			ll_rowcount --
			li_count --
		End If
		IF ldt_begin_date < ldt_tomorrow and ls_pending_ind = 'P' Then
			lds_dates.DeleteRow(li_count)
			ll_rowcount --
			li_count --
		End If
	Next
Else
	li_count = 1
End IF

lds_dates.SetItem(ll_rowcount, 'begin_date', String(Date(lds_dates.GetItemString(li_count, 'begin_date')), 'mm/dd/yyyy'))
lds_dates.SetItem(ll_rowcount, 'end_date', '12/31/2999')

as_input_dates_string = lds_dates.object.datawindow.data
Return True

end function

event ue_postopen;call super::ue_postopen;String						ls_begin_date, &
								ls_end_date, &
								ls_dates_string, &
								ls_product

DataWindowChild			ldwc_char, &
								ldwc_temp

u_string_functions		lu_string

u_conversion_functions	lu_conversions

ib_need_inquire = True

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Yields Tree Inq"
istr_error_info.se_user_id 		= sqlca.userid


IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

IF Not IsValid( iu_ws_pas3 ) THEN
	iu_ws_pas3	=  CREATE u_ws_pas3
END IF

dw_inquire.InsertRow(0)

iw_parentwindow.Event ue_get_data('fab_product_code')
ls_product = Message.StringParm
dw_fab_product_code.SetItem(1, 'fab_product_code', ls_product)

iw_parentwindow.Event ue_get_data('fab_product_description')
dw_fab_product_code.SetItem(1, 'fab_product_description', Message.StringParm)

iw_parentwindow.Event ue_get_data('dwc_char')
ldwc_temp = message.PowerObjectParm
dw_inquire.GetChild('characteristics', ldwc_char)
ldwc_temp.ShareData(ldwc_char)

iw_parentwindow.Event ue_get_data('parent_string')
is_parent_string = message.StringParm

dw_inquire.GetChild('parent_fab_product', ldwc_temp)
ldwc_temp.Reset()
ldwc_temp.ImportString(is_parent_string)

iw_parentwindow.Event ue_get_data('parent_fab_product')
dw_inquire.SetItem(1, 'parent_fab_product', message.StringParm)

iw_parentwindow.Event ue_get_data('ib_inquire_required')
ib_need_inquire = lu_conversions.nf_boolean(message.StringParm)

IF Not ib_need_inquire Then
	iw_parentwindow.Event ue_get_data('dates_string')
	is_input_dates_string = message.StringParm
	
	If Not lu_string.nf_IsEmpty(is_input_dates_string) Then
		dw_inquire.GetChild('effective_begin_date', ldwc_temp)
		ldwc_temp.Reset()
		ldwc_temp.ImportString(is_input_dates_string)
	End If
	iw_parentwindow.Event ue_get_data('effective_begin_date')
	ls_begin_date = message.StringParm
	If Not lu_string.nf_IsEmpty(ls_begin_date) Then
		dw_inquire.SetItem(1, 'effective_begin_date', ls_begin_date)
	End If
	
	iw_parentwindow.Event ue_get_data('effective_end_date')
	ls_end_date = message.StringParm
	If Not lu_string.nf_IsEmpty(ls_end_date) Then
		dw_inquire.SetItem(1, 'effective_end_date', ls_end_date)
	End If
	
	iw_parentwindow.Event ue_get_data('update_begin_date')
	If Not lu_string.nf_IsEmpty(Message.StringParm) Then
		dw_inquire.SetItem(1, 'update_begin_date', Date(message.StringParm))
	End If
	
	iw_parentwindow.Event ue_get_data('update_end_date')
	
	If Not lu_string.nf_IsEmpty(Message.StringParm) Then
		dw_inquire.SetItem(1, 'update_end_date', Date(message.StringParm))
	End If
End If

iw_parentwindow.Event ue_get_data('characteristics')
dw_inquire.SetItem(1, 'characteristics', Integer(message.StringParm))

iw_parentwindow.Event ue_get_data('effective_pending_ind')
dw_inquire.SetItem(1, 'effective_pending_ind', message.StringParm)

dw_inquire.SelectText(1, 100)
end event

event ue_base_cancel;call super::ue_base_cancel;Close (This)
end event

event ue_base_ok;call super::ue_base_ok;Integer		li_temp

Date			ldt_effective_begin_date, &
				ldt_effective_end_date, &
				ldt_update_begin_date, &
				ldt_update_end_date
				

String		ls_characteristic, &
				ls_product, &
				ls_product_state, &
				ls_product_status, &
				ls_parent_product, &
				ls_product_desc, &
				ls_findstring, &
				ls_parent_string, &
				ls_effective_begin_date, &
				ls_effective_end_date

Long			ll_findrow, &
				ll_row, ll_rowcount

DataWindowChild		ldwc_temp
				
				
u_string_functions		lu_string_functions

If dw_inquire.AcceptText() = -1 Then return
If dw_fab_product_code.AcceptText() = -1 Then return

ls_product = dw_fab_product_code.GetItemString(1, 'fab_product_code')
If lu_string_functions.nf_IsEmpty(ls_product) Then
	iw_frame.SetMicroHelp("Product Code is a required field")
	dw_fab_product_code.SetFocus()
	dw_fab_product_code.SetColumn('fab_product_code')
	dw_fab_product_code.SelectText(1, 100)
	return
End If
ls_product_state = dw_fab_product_code.GetItemString(1, 'product_state')
If lu_string_functions.nf_IsEmpty(ls_product_state) Then
	iw_frame.SetMicroHelp("Product State is a required field")
	dw_fab_product_code.SetFocus()
	dw_fab_product_code.SetColumn('product_state')
	dw_fab_product_code.SelectText(1, 100)
	return
End If
ls_product_status = dw_fab_product_code.GetItemString(1, 'product_status')
If lu_string_functions.nf_IsEmpty(ls_product_state) Then
	iw_frame.SetMicroHelp("Product Status is a required field")
	dw_fab_product_code.SetFocus()
	dw_fab_product_code.SetColumn('product_status')
	dw_fab_product_code.SelectText(1, 100)
	return
End If


ls_parent_product = dw_inquire.GetItemString(1, 'parent_fab_product')
If lu_string_functions.nf_IsEmpty(ls_parent_product) Then
	dw_inquire.SetColumn('parent_fab_product')
	If ib_need_inquire Then
		dw_inquire.Event ue_dwndropdown()
	End If
	dw_inquire.GetChild('parent_fab_product', ldwc_temp)
	IF ldwc_temp.RowCount() <> 0 Then
		iw_frame.SetMicroHelp("Parent Product is a required field")
		dw_inquire.SetFocus()
		dw_inquire.SetColumn('parent_fab_product')
		dw_inquire.SelectText(1, 100)
		return
	End If
End If


ls_effective_begin_date = dw_inquire.GetItemString(1, 'effective_begin_date')

If lu_string_functions.nf_IsEmpty(ls_effective_begin_date) Then
	iw_frame.SetMicroHelp("Effective Begin Date is a required field" )
	dw_inquire.SetFocus()
	dw_inquire.SetColumn('effective_begin_date')
	dw_inquire.SelectText(1, 100)
	return
End If

ls_effective_end_date = dw_inquire.GetItemString(1, 'effective_end_date')

If lu_string_functions.nf_IsEmpty(ls_effective_end_date) Then
	iw_frame.SetMicroHelp("Effective End Date is a required field" )
	dw_inquire.SetFocus()
	dw_inquire.SetColumn('effective_end_date')
	dw_inquire.SelectText(1, 100)
	return
End If

ldt_effective_begin_date = Date(ls_effective_begin_date)
ldt_effective_end_date = Date(ls_effective_end_date)
ldt_update_begin_date = dw_inquire.GetItemDate(1, 'update_begin_date')
ldt_update_end_date = dw_inquire.GetItemDate(1, 'update_end_date')

If ldt_update_begin_date < ldt_effective_begin_date &
		or ldt_update_begin_date > ldt_effective_end_date Then
	iw_frame.SetMicroHelp("Update Begin Date must be between Effective Begin Date and " + &
			"Effective End Date" )
	dw_inquire.SetFocus()
	dw_inquire.SetColumn('update_begin_date')
	dw_inquire.SelectText(1, 100)
	return
End If
	
If ldt_update_end_date < ldt_effective_begin_date &
		or ldt_update_end_date > ldt_effective_end_date Then
	iw_frame.SetMicroHelp("Update End Date must be between Effective Begin Date and " + &
			"Effective End Date" )
	dw_inquire.SetFocus()
	dw_inquire.SetColumn('update_end_date')
	dw_inquire.SelectText(1, 100)
	return
End If
	
If ldt_update_end_date < ldt_update_begin_date Then
	iw_frame.SetMicroHelp("Update Begin Date must be less than or equal to Update End Date" )
	dw_inquire.SetFocus()
	dw_inquire.SetColumn('update_begin_date')
	dw_inquire.SelectText(1, 100)
	return
End If
	
iw_parentwindow.Event ue_set_data('fab_product_code', ls_product)

iw_parentwindow.Event ue_set_data('fab_product_description', &
		dw_fab_product_code.GetItemString(1, 'fab_product_description'))
		
iw_parentwindow.Event ue_set_data('product_state', ls_product_state)

iw_parentwindow.Event ue_set_data('product_state_description', &
		dw_fab_product_code.GetItemString(1, 'product_state_description'))

iw_parentwindow.Event ue_set_data('product_status', ls_product_status)

iw_parentwindow.Event ue_set_data('product_status_description', &
		dw_fab_product_code.GetItemString(1, 'product_status_description'))		
		

ls_parent_product = dw_inquire.GetItemString(1, 'parent_fab_product')
//ls_parent_product = lu_string_functions.nf_GetToken(ls_parent_product, ' ')
iw_parentwindow.Event ue_set_data('parent_fab_product', &
		ls_parent_product)

dw_inquire.GetChild('parent_fab_product', ldwc_temp)
ll_row = ldwc_temp.GetRow()
If ll_row > 0 Then
	li_temp = ldwc_temp.GetItemNumber(ll_row, 'mfg_step')
	iw_parentwindow.Event ue_set_data('ii_step', &
			String(ldwc_temp.GetItemNumber(ll_row, 'mfg_step')))
End If


iw_parentwindow.Event ue_set_data('effective_begin_date', &
		String(ldt_effective_begin_date, 'yyyy-mm-dd'))

iw_parentwindow.Event ue_set_data('effective_end_date', &
		String(ldt_effective_end_date, 'yyyy-mm-dd'))

iw_parentwindow.Event ue_set_data('update_begin_date', &
		String(ldt_update_begin_date, 'yyyy-mm-dd'))

iw_parentwindow.Event ue_set_data('update_end_date', &
		String(ldt_update_end_date, 'yyyy-mm-dd'))

iw_parentwindow.Event ue_set_data('characteristics', &
		String(dw_inquire.GetItemNumber(1, 'characteristics')))

iw_parentwindow.Event ue_set_data('effective_pending_ind', &
		dw_inquire.GetItemString(1, 'effective_pending_ind'))

iw_parentwindow.Event ue_set_data('effective_pending_ind', &
		dw_inquire.GetItemString(1, 'effective_pending_ind'))

iw_parentwindow.Event ue_set_data('parent_string', is_parent_string)

iw_parentwindow.Event ue_set_data('dates_string', is_input_dates_string)

ib_ok_to_close = True

Close(This)

end event

on w_pas_yields_tree_inq.create
int iCurrent
call super::create
this.dw_inquire=create dw_inquire
this.dw_fab_product_code=create dw_fab_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_inquire
this.Control[iCurrent+2]=this.dw_fab_product_code
end on

on w_pas_yields_tree_inq.destroy
call super::destroy
destroy(this.dw_inquire)
destroy(this.dw_fab_product_code)
end on

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	Destroy iu_pas203
END IF

IF IsValid( iu_pas201 ) THEN
	Destroy iu_pas201
END IF
		
IF IsValid( iu_ws_pas3 ) THEN
	Destroy iu_ws_pas3
END IF

iw_parentwindow.Event ue_set_data('ib_inquire_required', String(ib_need_inquire))

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_yields_tree_inq
integer x = 2075
integer y = 300
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_yields_tree_inq
integer x = 2075
integer y = 164
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_yields_tree_inq
integer x = 2075
integer y = 28
integer taborder = 30
end type

type dw_inquire from u_base_dw_ext within w_pas_yields_tree_inq
event ue_dwndropdown pbm_dwndropdown
event ue_post_dropdown ( )
integer width = 2011
integer height = 704
integer taborder = 20
string dataobject = "d_yields_tree_inq"
boolean border = false
end type

event ue_dwndropdown;String			ls_input_string

Integer			li_rtn

DataWindowChild	ldwc_parents, &
						ldwc_dates

u_String_Functions		lu_string

This.PostEvent('ue_post_dropdown')

Choose Case This.GetColumnName()
	Case 'parent_fab_product', 'effective_begin_date', 'effective_end_date'
		If Not ib_need_inquire Then Return
		
		If lu_string.nf_IsEmpty(dw_fab_product_code.GetItemString(1, 'fab_product_code')) Then
			Return
		End If
		
		This.GetChild('parent_fab_product', ldwc_parents)
		ldwc_parents.Reset()

		This.GetChild('effective_begin_date', ldwc_dates)
		ldwc_dates.Reset()

		istr_error_info.se_event_name    = "itemchanged"
		
		
		ls_input_string = dw_fab_product_code.GetItemString(1, 'fab_product_code') + '~t' + &
								String(This.GetItemNumber(1, 'characteristics')) + '~t' + &
								dw_fab_product_code.GetItemString(1, 'product_state') + '~t' + &
								dw_fab_product_code.GetItemString(1, 'product_status') + '~t' + &								
								This.GetItemString(1, 'effective_pending_ind')
								
		is_parent_string = ''
		is_input_dates_string = ''
		
//		li_rtn = iu_pas203.nf_pasp69br_inq_pas_yield_tree_inq(istr_error_info, &
//																			ls_input_string, &
//																			is_input_dates_string, &
//																			is_parent_string)

		li_rtn = iu_ws_pas3.uf_pasp69fr(istr_error_info, &
													ls_input_string, &
													is_input_dates_string, &
													is_parent_string)
		
		If li_rtn < 0 Then Return
		
		ldwc_parents.ImportString(is_parent_string)
		
		If lu_string.nf_IsEmpty(is_input_dates_string) Then
			IF dw_inquire.GetItemString(1, 'effective_pending_ind') = 'P' Then
				is_input_dates_string = String(RelativeDate(Today(), 1), 'mm/dd/yyyy') + &
						'~t12/31/2999'
			Else
				is_input_dates_string = String(Today(), 'mm/dd/yyyy') + &
						'~t12/31/2999'
			End If
		Else
			Parent.wf_convert_dates(is_input_dates_string)
		End If
		ldwc_dates.ImportString(is_input_dates_string)
		
		ib_need_inquire = False

End Choose

end event

event ue_post_dropdown();Long							ll_findrow, &
								ll_mfg_step

DataWindowChild			ldwc_temp

u_String_Functions		lu_string


If lu_string.nf_IsEmpty(dw_fab_product_code.GetItemString(1, 'fab_product_code')) Then
	MessageBox('Empty Product Code', 'You must enter a product code.')
	dw_fab_product_code.SetColumn('fab_product_code')
	dw_fab_product_code.SelectText(1, 100)
End If


end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_begin_date, &
								ldwc_end_date, &
								ldwc_parent_product, &
								ldwc_parent_step, &
								ldwc_parent_descr
								

dw_inquire.GetChild('effective_begin_date', ldwc_begin_date)
dw_inquire.GetChild('effective_end_date', ldwc_End_date)
ldwc_begin_date.ShareData(ldwc_end_date)

dw_inquire.GetChild('parent_fab_product', ldwc_parent_product)
dw_inquire.GetChild('parent_product_descr', ldwc_parent_descr)
ldwc_parent_product.ShareData(ldwc_parent_descr)

dw_inquire.GetChild('parent_mfg_step', ldwc_parent_step)
ldwc_parent_product.ShareData(ldwc_parent_step)


end event

event itemchanged;call super::itemchanged;Boolean			lb_return
String			ls_product_desc = Space(42), &
					ls_product, &
					ls_find_string, &
					ls_begin_date, &
					ls_end_date
					
Long				ll_findrow, &
					ll_mfg_step
					
Date				ldt_begin_date, &
   				ldt_end_date
DataWindowChild	ldwc_temp

Choose Case dwo.name
	Case 'effective_pending_ind'
		ib_need_inquire = True
		This.SetRedraw(False)
		SetNull(ldt_begin_date)
		This.SetItem(1, 'effective_begin_date', ldt_begin_date)
		This.SetColumn('effective_begin_date')
		This.SetText('')
		This.SetItem(1, 'update_begin_date', ldt_begin_date)
		This.SetColumn('update_begin_date')
		This.SetText('')
		SetNull(ldt_end_date)
		This.SetItem(1, 'effective_end_date', ldt_end_date)
		This.SetColumn('effective_end_date')
		This.SetText('')
		This.SetItem(1, 'update_end_date', ldt_end_date)
		This.SetColumn('update_end_date')
		This.SetText('')
		This.SetColumn('effective_pending_ind')
		This.SetRedraw(True)
	Case 'fab_product_code'
		// **IBDKEEM ** 08/30/2002 ** Use nvuo_fab_product_code to check product.
		ls_product = data
				
		If not invuo_fab_product_code.uf_check_product(ls_product) then
			If	invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
				Return 1
			Else
				This.SetFocus()
				This.SelectText(1, 100)
				iw_frame.SetMicroHelp(trim(ls_product) + ' is an invalid product')
				ls_product_desc = ''
				Return 1			
			End If
		End If
		
		ls_product_desc = invuo_fab_product_code.uf_get_product_description( )
		
		This.SetItem (1, 'fab_product_description' , ls_product_desc)
		This.SetItem(1, 'parent_fab_product', '')
		ib_need_inquire = True

//The effective dates are strings because I couldn't set the format for these fields if the
//regional settings was set to the wrong format.  On the dropdowns, It would always use what
//was set in the regional settings.  Some users couldn't change their regional settings to 
//something other than m/d/yy.  Which would translate 2999 to 1999.
	Case 'effective_begin_date'
		This.GetChild('effective_begin_date', ldwc_temp)
		ls_begin_date = String(Date(data), 'mm/dd/yyyy')
		ls_end_date = String (Date(ldwc_temp.GetItemString(ldwc_temp.GetRow(), 'end_date')), 'mm/dd/yyyy')
		This.SetItem(1, 'effective_end_date', ls_end_date)
		This.SetItem(1, 'update_begin_date', Date(data))
		This.SetItem(1, 'update_end_date', Date(ls_end_date))

	Case 'effective_end_date'
		This.GetChild('effective_end_date', ldwc_temp)
		ll_findrow = ldwc_temp.GetRow()
		ls_begin_date = String(Date(ldwc_temp.GetItemString(ldwc_temp.GetRow(), 'begin_date')), 'mm/dd/yyyy')
		ls_end_date = String (Date(data), 'yyyy-mm-dd')
		This.SetItem(1, 'effective_begin_date', ls_begin_date)
		This.SetItem(1, 'update_begin_date', Date(ls_begin_date))
		This.SetItem(1, 'update_end_date', Date(data))
End choose

end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;// **IBDKEEM ** 08/30/2002 ** Fresh Only Product State.
//this.modify( 'product_state.protected=0')

//This.GetChild("product_state", idddw_child)
//idddw_child.SetTransObject(SQLCA)
//idddw_child.Retrieve("PRDSTATE")
//This.InsertRow(0)
end event

type dw_fab_product_code from u_fab_product_code within w_pas_yields_tree_inq
integer x = 320
integer y = 52
integer height = 284
integer taborder = 10
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;Boolean			lb_return
String			ls_product_desc = Space(42), &
					ls_product, &
					ls_product_state, &
					ls_product_status, &
					ls_find_string					
					
Long				ll_findrow, &
					ll_mfg_step
					
Date				ldt_begin_date, &
   				ldt_end_date
DataWindowChild	ldwc_temp

Choose Case dwo.name
	
	Case 'fab_product_code'
		// **IBDKEEM ** 08/30/2002 ** Use nvuo_fab_product_code to check product.
		ls_product = data
				
		If not invuo_fab_product_code.uf_check_product(ls_product) then
			If	invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
				Return 1
			Else
				This.SetFocus()
				This.SelectText(1, 100)
				iw_frame.SetMicroHelp(trim(ls_product) + ' is an invalid product')
				ls_product_desc = ''
				Return 1			
			End If
		End If
		
		ls_product_desc = invuo_fab_product_code.uf_get_product_description( )
		
		This.SetItem (1, 'fab_product_description' , ls_product_desc)
		dw_inquire.SetItem(1, 'parent_fab_product', '')
		ib_need_inquire = True
Case 'product_state'
	
		ls_product_state = data
				
//		If not invuo_fab_product_code.uf_check_product(ls_product) then
//			If	invuo_fab_product_code.ib_error_occurred then
//				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
//				Return 1
//			Else
//				This.SetFocus()
//				This.SelectText(1, 100)
//				iw_frame.SetMicroHelp(trim(ls_product) + ' is an invalid product')
//				ls_product_desc = ''
//				Return 1			
//			End If
//		End If
		
//		ls_product_desc = invuo_fab_product_code.uf_get_product_description( )
		
		This.SetItem (1, 'product_state' , ls_product_state)
		dw_inquire.SetItem(1, 'parent_fab_product', '')
		ib_need_inquire = True

Case 'product_status'
	
		ls_product_status = data
				
//		If not invuo_fab_product_code.uf_check_product(ls_product) then
//			If	invuo_fab_product_code.ib_error_occurred then
//				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
//				Return 1
//			Else
//				This.SetFocus()
//				This.SelectText(1, 100)
//				iw_frame.SetMicroHelp(trim(ls_product) + ' is an invalid product')
//				ls_product_desc = ''
//				Return 1			
//			End If
//		End If
		
//		ls_product_desc = invuo_fab_product_code.uf_get_product_description( )
		
		This.SetItem (1, 'product_status' , ls_product_status)
		dw_inquire.SetItem(1, 'parent_fab_product', '')
		ib_need_inquire = True

End choose

end event

