HA$PBExportHeader$w_pas_product_step_inq.srw
$PBExportComments$Inquire window with product and step list
forward
global type w_pas_product_step_inq from w_base_response_ext
end type
type dw_plant_step_inq from datawindow within w_pas_product_step_inq
end type
type dw_fab_product_code from u_fab_product_code within w_pas_product_step_inq
end type
end forward

global type w_pas_product_step_inq from w_base_response_ext
integer x = 124
integer y = 429
integer width = 1573
integer height = 707
string title = "Inquire"
long backcolor = 67108864
event ue_post_itemchanged ( )
dw_plant_step_inq dw_plant_step_inq
dw_fab_product_code dw_fab_product_code
end type
global w_pas_product_step_inq w_pas_product_step_inq

type variables
private:
Boolean		ib_IsValidReturn
u_pas201		iu_pas201
u_ws_pas1	iu_ws_pas1
s_error		istr_error_info
string		is_last_product_code

end variables

event ue_post_itemchanged();Boolean	lb_ret

Char	lc_fab_product_descr[30], &
		lc_fab_group[2]

DataWindowChild	dwc_steps

Int		li_ret

String	ls_mfg_steps, &
			ls_GetText, &
			ls_fab_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_header_string,ls_fab_product_descr,ls_fab_group

If not dw_fab_product_code.uf_validate( ) Then return

SetPointer(HourGlass!)

ls_fab_product_code 	= dw_fab_product_code.uf_get_product_code( )
ls_product_state 		= dw_fab_product_code.uf_get_product_state( )
ls_product_status 		= dw_fab_product_code.uf_get_product_status( )
	
dw_plant_step_inq.SetRedraw(False)
dw_plant_step_inq.Reset()
dw_plant_step_inq.InsertRow(0)

li_ret = dw_plant_step_inq.GetChild("mfg_step_sequence", dwc_steps)	
dwc_steps.Reset()		

dw_plant_step_inq.SetRedraw(True)

ls_header_string = ls_fab_product_code + '~t' + ls_product_state + '~t' + ls_product_status + '~t'

istr_error_info.se_event_name = "Itemchanged in dw_step_inq"
istr_error_info.se_procedure_name = "nf_pasp04br"
istr_error_info.se_message = Space(70)

//lb_ret = iu_pas201.nf_pasp04br(istr_error_info, &
//										ls_header_string, &
//										lc_fab_product_descr, &
//										lc_fab_group, &
//										ls_mfg_steps)
										
lb_ret = iu_ws_pas1.nf_pasp04fr(istr_error_info, &
										ls_header_string, &
										ls_fab_product_descr, &
										ls_fab_group, &
										ls_mfg_steps)
										
If not lb_ret Then
	return
End if

dw_plant_step_inq.SetItem(1, "fab_group",ls_fab_group)
dwc_steps.ImportString(ls_mfg_steps)
dwc_steps.Sort()
//dw_plant_step_inq.SetFocus()

is_last_product_code = dw_fab_product_code.uf_get_product_code( )

return
end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

event close;call super::close;If IsValid(iu_pas201) Then Destroy iu_pas201
destroy iu_ws_pas1

If Not ib_IsValidReturn Then
	Message.StringParm = ""
End if
end event

event ue_postopen;call super::ue_postopen;DataWindowChild	dwc_inquire

String	ls_header_data,&
			ls_product_code,&
			ls_product_desc,&
			ls_product_state,&
			ls_product_state_desc, &
			ls_product_status, &
			ls_product_status_desc, &
			ls_mfg_step_seq
						


iu_pas201 = Create u_pas201
iu_ws_pas1	= Create	u_ws_pas1


dw_plant_step_inq.GetChild("mfg_step_sequence", dwc_inquire)

dwc_inquire.Reset()
iw_ParentWindow.TriggerEvent("ue_GetData", 0, "steplist")
dwc_inquire.ImportString(Message.StringParm)
dwc_inquire.Sort()

iw_ParentWindow.TriggerEvent("ue_GetData", 0, "header")
ls_header_data = Message.StringParm

ls_product_code			= iw_frame.iu_string.nf_gettoken(ls_header_data, "~t")
ls_product_desc			= iw_frame.iu_string.nf_gettoken(ls_header_data, "~t")
ls_product_state			= iw_frame.iu_string.nf_gettoken(ls_header_data, "~t")
ls_product_state_desc	= iw_frame.iu_string.nf_gettoken(ls_header_data, "~t")
ls_product_status 		= iw_frame.iu_string.nf_gettoken(ls_header_data, "~t")
ls_product_status_desc	= iw_frame.iu_string.nf_gettoken(ls_header_data, "~t")
ls_mfg_step_seq     = iw_frame.iu_string.nf_gettoken(ls_header_data, "~t")

if len(ls_product_code) > 0 Then
	dw_fab_product_code.uf_importstring(ls_product_code, ls_product_desc, &
													ls_product_state,ls_product_state_desc, &
													ls_product_status, ls_product_status_desc, true)
end if

dw_plant_step_inq.Reset()
//dw_plant_step_inq.ImportString(ls_Header_Data)
//JXR 9/10/18 - if mfg step = 0 then a copy step was aborted, set mfg_step_seq to null
If ls_mfg_step_seq = '0' Then
	ls_mfg_step_seq = ''
End If

dw_plant_step_inq.ImportString(ls_mfg_step_seq)

If dw_plant_step_inq.rowcount( ) = 0 Then
	dw_plant_step_inq.InsertRow(0)
End If

dw_plant_step_inq.SelectText(1, Len(dw_plant_step_inq.GetText()))
is_last_product_code = ls_product_code
end event

on w_pas_product_step_inq.create
int iCurrent
call super::create
this.dw_plant_step_inq=create dw_plant_step_inq
this.dw_fab_product_code=create dw_fab_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant_step_inq
this.Control[iCurrent+2]=this.dw_fab_product_code
end on

on w_pas_product_step_inq.destroy
call super::destroy
destroy(this.dw_plant_step_inq)
destroy(this.dw_fab_product_code)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;DataWindowChild	dwc_inquire

Int	li_number

String	ls_ReturnString

If not dw_fab_product_code.uf_validate( ) Then 
	dw_fab_product_code.SetFocus()
	return
end if

If dw_plant_step_inq.AcceptText() = -1 then 
	dw_plant_step_inq.SetFocus()
	return
End if

IF is_last_product_code <> dw_fab_product_code.uf_get_product_code( ) Then
	postevent("ue_post_itemchanged")
	iw_frame.SetMicroHelp("Step Description is a required field")
	dw_plant_step_inq.SetColumn("mfg_step_sequence")
	dw_plant_step_inq.SetFocus()
	return
END IF

li_number = dw_plant_step_inq.GetItemNumber(1, "mfg_step_sequence")
If li_number < 1 Or IsNull(li_number) Then
	iw_frame.SetMicroHelp("Step Description is a required field")
	dw_plant_step_inq.SetColumn("mfg_step_sequence")
	dw_plant_step_inq.SetFocus()
	return
End if

ib_IsValidReturn = True

dw_plant_step_inq.GetChild("mfg_step_sequence", dwc_inquire)

ls_ReturnString = dw_fab_product_code.uf_exportstring( ) + "~t"
ls_ReturnString += dw_plant_step_inq.Object.DataWindow.Data 
If Right(ls_ReturnString, 2) <> '~r~n' Then
	ls_ReturnString += '~r~n'
End if

ls_ReturnString += dwc_inquire.Describe("DataWindow.Data")

CloseWithReturn(This,  ls_returnString)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_product_step_inq
integer x = 1254
integer y = 480
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_product_step_inq
integer x = 962
integer y = 480
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_product_step_inq
integer x = 666
integer y = 480
integer taborder = 30
end type

type dw_plant_step_inq from datawindow within w_pas_product_step_inq
integer x = 4
integer y = 298
integer width = 1562
integer height = 163
integer taborder = 20
string dataobject = "d_pas_product_step_inq"
boolean border = false
end type

event itemerror;If This.GetColumnName() = "mfg_step_sequence"  Then
	Return 1
End if
end event

on itemfocuschanged;This.SelectText(1, Len(This.GetText()))
end on

event constructor;This.InsertRow(0)

end event

event getfocus;dw_fab_product_code.accepttext( )
end event

type dw_fab_product_code from u_fab_product_code within w_pas_product_step_inq
integer width = 1536
integer height = 288
integer taborder = 10
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;If AncestorReturnValue = 0 then
	parent.postevent("ue_post_itemchanged")
End If

return AncestorReturnValue
end event

