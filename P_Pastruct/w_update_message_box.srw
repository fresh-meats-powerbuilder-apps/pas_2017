HA$PBExportHeader$w_update_message_box.srw
forward
global type w_update_message_box from window
end type
type st_message from statictext within w_update_message_box
end type
type cb_2 from commandbutton within w_update_message_box
end type
type cb_1 from commandbutton within w_update_message_box
end type
end forward

global type w_update_message_box from window
integer x = 832
integer y = 360
integer width = 1998
integer height = 404
boolean titlebar = true
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
st_message st_message
cb_2 cb_2
cb_1 cb_1
end type
global w_update_message_box w_update_message_box

event open;String				ls_saveanyway, &
						ls_data, &
						ls_message, &
						ls_title
						
Window      lw_parent 

lw_parent = this.parentwindow()						

u_String_functions	lu_strings

ls_data = Message.StringParm

lu_strings.nf_parseLeftRight(ls_data, '~t', ls_title, ls_message)

This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
	This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2) 

This.Title = ls_title
st_message.Text = ls_message

ls_saveanyway = ProfileString ( 'ibpuser.ini', 'PAS', 'w_pas_yield_tree.SaveAnyway', 'N' ) 

If ls_saveanyway = 'Y' Then
	cb_2.Enabled = True
End If
end event

on w_update_message_box.create
this.st_message=create st_message
this.cb_2=create cb_2
this.cb_1=create cb_1
this.Control[]={this.st_message,&
this.cb_2,&
this.cb_1}
end on

on w_update_message_box.destroy
destroy(this.st_message)
destroy(this.cb_2)
destroy(this.cb_1)
end on

type st_message from statictext within w_update_message_box
integer x = 69
integer y = 44
integer width = 1829
integer height = 76
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_update_message_box
integer x = 1001
integer y = 160
integer width = 389
integer height = 108
integer taborder = 2
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
boolean enabled = false
string text = "Update Anyway"
end type

event clicked;CloseWithReturn(Parent, 'SaveAnyway')
end event

type cb_1 from commandbutton within w_update_message_box
integer x = 558
integer y = 160
integer width = 389
integer height = 108
integer taborder = 1
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Ok"
boolean cancel = true
boolean default = true
end type

event clicked;Close(Parent)
end event

