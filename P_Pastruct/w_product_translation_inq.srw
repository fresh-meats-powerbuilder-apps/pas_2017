HA$PBExportHeader$w_product_translation_inq.srw
forward
global type w_product_translation_inq from w_netwise_response
end type
type dw_division from u_division within w_product_translation_inq
end type
type dw_prodtran from u_prodtran_code within w_product_translation_inq
end type
end forward

global type w_product_translation_inq from w_netwise_response
integer x = 1075
integer y = 485
integer width = 1678
integer height = 528
string title = "Product Translation Inquire"
long backcolor = 12632256
dw_division dw_division
dw_prodtran dw_prodtran
end type
global w_product_translation_inq w_product_translation_inq

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"")
end event

on w_product_translation_inq.create
int iCurrent
call super::create
this.dw_division=create dw_division
this.dw_prodtran=create dw_prodtran
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
this.Control[iCurrent+2]=this.dw_prodtran
end on

on w_product_translation_inq.destroy
call super::destroy
destroy(this.dw_division)
destroy(this.dw_prodtran)
end on

event ue_base_ok;call super::ue_base_ok;String	ls_division, &	
			ls_prodtran

If dw_division.AcceptText() = -1 Then return
if dw_prodtran.AcceptText() = -1 Then return

ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End if

ls_prodtran = dw_prodtran.uf_get_prodtran()
If iw_frame.iu_string.nf_IsEmpty(ls_prodtran) Then
	iw_frame.SetMicroHelp("Product Translation is a required field")
	dw_prodtran.SetFocus()
	return
End if

//iw_parent.Event ue_Set_Data('Shift', ls_temp)

//This.Event ue_Set_Data('prodtran_code', ls_prodtran)
CloseWithReturn(This, ls_division + ls_prodtran)
end event

type cb_base_help from w_netwise_response`cb_base_help within w_product_translation_inq
integer x = 1307
integer y = 300
integer taborder = 50
end type

type cb_base_cancel from w_netwise_response`cb_base_cancel within w_product_translation_inq
integer x = 1010
integer y = 304
integer width = 265
integer taborder = 40
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_product_translation_inq
integer x = 686
integer y = 300
integer taborder = 30
end type

type dw_division from u_division within w_product_translation_inq
integer x = 32
integer y = 40
integer taborder = 10
end type

type dw_prodtran from u_prodtran_code within w_product_translation_inq
integer x = 37
integer y = 144
integer taborder = 20
boolean bringtotop = true
end type

