HA$PBExportHeader$w_pas_tree.srw
forward
global type w_pas_tree from w_base_sheet_ext
end type
type dw_fab_product_code from u_fab_product_code within w_pas_tree
end type
type tv_tree from treeview within w_pas_tree
end type
end forward

global type w_pas_tree from w_base_sheet_ext
integer x = 0
integer y = 0
integer width = 2793
string title = "Product Tree"
long backcolor = 67108864
event ue_getdata pbm_custom01
dw_fab_product_code dw_fab_product_code
tv_tree tv_tree
end type
global w_pas_tree w_pas_tree

type variables
Private:
s_error		istr_error_info
u_pas201		iu_pas201
DataStore	ids_tree
Boolean		ib_first_time
u_ws_pas1	iu_ws_pas1

end variables

forward prototypes
public function long wf_get_parent (ref long handle)
public function boolean wf_retrieve ()
end prototypes

event ue_getdata;Choose Case Lower(String(Message.LongParm, "product"))
	Case 'product'
		Message.StringParm = dw_fab_product_code.uf_exportstring( )
End Choose
end event

public function long wf_get_parent (ref long handle);String			ls_product_code, &
					ls_product_state, &
					ls_product_status, &
					ls_tree_products, &
					ls_this_product,&
					ls_temp_item
					
Boolean			lb_ret

Integer			li_counter

Long				ll_tvnext, &
					ll_tvparent

TreeViewItem	ltvi_child, &
					ltvi_clicked

tv_tree.GetItem(handle,ltvi_clicked)

If ltvi_clicked.Level <> 1 Then return 0

SetPointer(Hourglass!)

ls_temp_item = string(ltvi_clicked.Data)
ls_product_code = iw_frame.iu_string.nf_gettoken(ls_temp_item, "~t")
ls_product_state = iw_frame.iu_string.nf_gettoken(ls_temp_item, "~t") 
ls_product_status = iw_frame.iu_string.nf_gettoken(ls_temp_item, "~t")

ids_tree.Reset()

istr_error_info.se_event_name = "tv_clicked"
istr_error_info.se_procedure_name = "nf_pasp02fr"
istr_error_info.se_message = space(70)

//lb_ret = iu_pas201.nf_pasp02br(	istr_error_info, &
//											ls_product_code + '~t' + ls_product_state + '~t' + ls_product_status + '~tO', &
//											ls_this_product, &
//											ls_tree_products)
											
lb_ret = iu_ws_pas1.nf_pasp02fr(	istr_error_info, &
											ls_product_code + '~t' + ls_product_state + '~t' + ls_product_status + '~tO', &
											ls_this_product, &
											ls_tree_products)											
if Not lb_ret Then return 1

If LEN(TRIM(ls_tree_products)) = 0 Then return handle

If ids_tree.ImportString(ls_tree_products) < 0 Then
	messagebox(this.title,"Data did not pass validation.")
	return handle
End If

This.SetRedraw(False)

Do While handle <> -1
	tv_tree.DeleteItem(handle)
	handle = tv_tree.FindItem(RootTreeItem!,0)
Loop

For li_counter = 1 to ids_tree.RowCount()
	ltvi_child.Label = ids_tree.GetItemString(li_counter,"fab_product_code") + " " + &
			ids_tree.GetItemString(li_counter,"product_state") + " " + &
			ids_tree.GetItemString(li_counter,"product_status") + " " + &			
			ids_tree.GetItemString(li_counter,"fab_type") + " " + &
			ids_tree.GetItemString(li_counter,"fab_product_description")
			
	ltvi_child.Children = True	
	If ids_tree.GetItemString(li_counter,"new_step_ind") = '*' Then
		ltvi_child.PictureIndex = 2
		ltvi_child.SelectedPictureIndex = 2
	Else
		ltvi_child.PictureIndex = 1
		ltvi_child.SelectedPictureIndex = 1
	End If
	ltvi_child.Data = ids_tree.GetItemString(li_counter,"fab_product_code") + "~t" + &
							ids_tree.GetItemString(li_counter,"product_state") + "~t" + &
							ids_tree.GetItemString(li_counter,"product_status") + "~tP"							
	
	If li_counter = 1 Then
		ll_tvnext = tv_tree.InsertItemFirst(0,ltvi_child)
	Else
		ll_tvnext = tv_tree.InsertItem(0, ll_tvnext,ltvi_child)
	End If
Next

This.SetRedraw(True)

SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

Return 1
end function

public function boolean wf_retrieve ();Boolean	lb_ret

Int		li_counter,li_rows

Long		ll_row, &
			ll_row_count

Long		ll_tvroot, &
			ll_tvnext = 0

TreeViewItem	ltvi_temp

String	ls_inquire_string, &
			ls_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_product_type,&
			ls_product_description,&
			ls_dw_product_product, &
			ls_tree_products

OpenWithParm(w_pas_tree_inq, This)
ls_inquire_string = Message.StringParm

If iw_frame.iu_string.nf_IsEmpty(ls_inquire_string) Then return false

dw_fab_product_code.uf_importstring(ls_inquire_string,true)
ls_product_code  = dw_fab_product_code.uf_get_product_code( )
ls_product_state = dw_fab_product_code.uf_get_product_state( )
ls_product_status = dw_fab_product_code.uf_get_product_status( )
	
This.SetRedraw(False)
SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

ll_tvroot = 0
Do while ll_tvroot <> -1
	ll_tvroot = tv_tree.FindItem(RootTreeItem!,0)
	If ll_tvroot <> -1 Then tv_tree.DeleteItem(ll_tvroot)
Loop

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp02br"

istr_error_info.se_message = Space(70)

//lb_ret = iu_pas201.nf_pasp02br(	istr_error_info, &
//										ls_product_code + '~t' + ls_product_state + '~t' + ls_product_status + '~tI', &
//										ls_dw_product_product, &
//										ls_tree_products)
										
lb_ret = iu_ws_pas1.nf_pasp02fr(	istr_error_info, &
										ls_product_code + '~t' + ls_product_state + '~t' + ls_product_status + '~tI', &
										ls_dw_product_product, &
										ls_tree_products)										
										


If Not lb_ret And Left(istr_error_info.se_message, 6) <> 'PAS008' Then
	This.PostEvent("ue_query")
	This.SetRedraw(True)
	return True
End if

//Add the Root Node
ll_row = ids_tree.InsertRow(0)

ls_product_type = iw_frame.iu_string.nf_gettoken(ls_dw_product_product, "~t")
ls_product_description = iw_frame.iu_string.nf_gettoken(ls_dw_product_product, "~t")

ltvi_temp.Label = ls_product_code + " " + ls_product_state + " " + ls_product_status + " " + &
						ls_product_type + " " + TRIM(ls_product_description)
			
ltvi_temp.Children = True
ltvi_temp.PictureIndex = 1
ltvi_temp.SelectedPictureIndex = 1

ltvi_temp.Data = ls_product_code + "~t" + &
					  ls_product_state + "~t" + &
					  ls_product_status + "~tN"
					  
ll_tvroot = tv_tree.InsertItemFirst(0,ltvi_temp)

If IsNull(ls_tree_products) or LEN(TRIM(ls_tree_products)) = 0 Then
	This.SetRedraw(True)
	return false
end if

ids_tree.Reset()
If ids_tree.ImportString(ls_tree_products) < 0 Then
	messagebox(this.title,"Data did not pass validation.")
	return false
End If

ll_row_count = ids_tree.RowCount()
For li_counter = 1 to ll_row_count		
	
	ltvi_temp.Label = ids_tree.GetItemString(li_counter,"fab_product_code") + " " +&
							ids_tree.GetItemString(li_counter,"product_state") + " "  +&
							ids_tree.GetItemString(li_counter,"product_status") + " "  +&							
							ids_tree.GetItemString(li_counter,"fab_type") + " "  +&
							TRIM(ids_tree.GetItemString(li_counter,"fab_product_description"))
		
	ltvi_temp.Children = True
	
	If ids_tree.GetItemString(li_counter,"new_step_ind") = '*' Then
		ltvi_temp.PictureIndex = 2
		ltvi_temp.SelectedPictureIndex = 2
	Else
		ltvi_temp.PictureIndex = 1
		ltvi_temp.SelectedPictureIndex = 1
	End If

	ltvi_temp.Data = ids_tree.GetItemString(li_counter,"fab_product_code") + "~t" + &
						  ids_tree.GetItemString(li_counter,"product_state") + "~t" + &
						  ids_tree.GetItemString(li_counter,"product_status") + "~tN"						  
			
	If ll_tvnext = 0 Then
		ll_tvnext = tv_tree.InsertItemFirst(ll_tvroot,ltvi_temp)
	Else
		ll_tvnext = tv_tree.InsertItem(ll_tvroot,ll_tvnext,ltvi_temp)
	End If
Next

ib_first_time = true
tv_tree.ExpandItem(ll_tvroot)

This.SetRedraw(True)
This.SetMicroHelp("Ready ")
return true
end function

event ue_postopen;call super::ue_postopen;istr_error_info.se_user_id = sqlca.userid
istr_error_info.se_app_name = Message.nf_Get_App_ID()
istr_error_info.se_window_name = "Tree"

// Instantiate user object for pasp20ar
iu_pas201 = Create u_pas201
iu_ws_pas1 = Create u_ws_pas1

ids_tree = Create DataStore
ids_tree.DataObject = 'd_pas_tree'

This.PostEvent("ue_query")
end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = 50 + tv_tree.x
li_y = 125 + tv_tree.Y

If (This.Width > li_x) Then tv_tree.width = This.Width - li_x
IF (This.Height > li_y) Then tv_tree.height = This.Height - li_y

end event

event close;call super::close;If IsValid(iu_pas201) Then Destroy iu_pas201
If IsValid(ids_tree) Then Destroy ids_tree
Destroy iu_ws_pas1
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_save')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_save')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

on w_pas_tree.create
int iCurrent
call super::create
this.dw_fab_product_code=create dw_fab_product_code
this.tv_tree=create tv_tree
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fab_product_code
this.Control[iCurrent+2]=this.tv_tree
end on

on w_pas_tree.destroy
call super::destroy
destroy(this.dw_fab_product_code)
destroy(this.tv_tree)
end on

event ue_fileprint;call super::ue_fileprint;Long		ll_root, &
			ll_currentrow

TreeViewItem	ltvi_temp

DataStore		lds_print


If Not ib_print_ok Then Return

lds_print = Create u_print_datastore
lds_print.DataObject = 'd_pas_tree_print'

lds_print.Reset()
ll_root = tv_tree.FindItem(RootTreeItem!,0)
Do While ll_root <> -1
	tv_tree.GetItem(ll_root,ltvi_temp)
	ll_currentrow = lds_print.InsertRow(0)
	If ltvi_temp.PictureIndex = 2 Then
		lds_print.SetItem(ll_currentrow,"step_ind","*")
	Else
		lds_print.SetItem(ll_currentrow,"step_ind"," ")
	End If
	lds_print.SetItem(ll_currentrow,"line_string",ltvi_temp.Label)
	lds_print.SetItem(ll_currentrow,"level_no",ltvi_temp.Level)
	ll_root = tv_tree.FindItem(NextVisibleTreeItem!,ll_root)
Loop

lds_print.Print()

Destroy lds_print
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'product'
		Message.StringParm = dw_fab_product_code.uf_exportstring( )
End Choose
end event

event open;call super::open;dw_fab_product_code.uf_disable( )
end event

type dw_fab_product_code from u_fab_product_code within w_pas_tree
integer width = 1609
integer height = 288
integer taborder = 30
boolean ib_updateable = true
end type

type tv_tree from treeview within w_pas_tree
integer y = 320
integer width = 2743
integer height = 1088
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 16777215
borderstyle borderstyle = stylelowered!
integer indent = 4
boolean linesatroot = true
string picturename[] = {"dots.bmp","Step!","Custom028!","Custom029!","Custom030!","Custom031!"}
integer picturewidth = 16
integer pictureheight = 16
long picturemaskcolor = 12632256
long statepicturemaskcolor = 553648127
end type

event itemexpanding;String			ls_product_code, &
					ls_product_state, & 
					ls_product_status, &
					ls_tree_products, &
					ls_this_product,&
					ls_temp_item
					
Boolean			lb_ret

Integer			li_counter

Long				ll_tvnext, &
					ll_tvparent, &
					ll_tvchild, &
					ll_tvchildnext, &
					ll_tvchildlevel

TreeViewItem	ltvi_temp, &
					ltvi_child, &
					ltvi_tempchild

If ib_first_time then 
	ib_first_time = False
	Return 0
End If


This.GetItem(handle,ltvi_tempchild)
ll_tvchild = This.FindItem(ChildTreeItem!,handle)

ls_temp_item = String(ltvi_tempchild.Data)
If iw_frame.iu_string.nf_IsEmpty(ls_temp_item) Then 
	SetMicroHelp("Item error. Null Data")
	return 0
End If

If RIGHT(ls_temp_item,1) <> "P" Then
	If ll_tvchild <> -1 then 
		SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
		return 0
	End If
	SetPointer(Hourglass!)
Else
	SetPointer(Hourglass!)
	Do While ll_tvchild > 0
		This.DeleteItem(ll_tvchild)
		ll_tvchild = This.FindItem(ChildTreeItem!,handle)
	Loop
End If

This.GetItem(handle,ltvi_temp)

ls_temp_item = string(ltvi_temp.Data)
If iw_frame.iu_string.nf_IsEmpty(ls_temp_item) Then 
	SetMicroHelp("Item error. Null Data")
	return 0
End If

ls_product_code = iw_frame.iu_string.nf_gettoken(ls_temp_item, "~t")
ls_product_state = iw_frame.iu_string.nf_gettoken(ls_temp_item, "~t") 
ls_product_status = iw_frame.iu_string.nf_gettoken(ls_temp_item, "~t")

//Update flag
ltvi_temp.Data = ls_product_code + "~t" + &
					  ls_product_state + "~t" + &
					  ls_product_status + "~tN"
					  
This.SetItem(handle,ltvi_temp)

istr_error_info.se_event_name = "tv_itemexpanding"
istr_error_info.se_procedure_name = "nf_pasp02br"
istr_error_info.se_message = space(70)

//
//lb_ret = iu_pas201.nf_pasp02br(	istr_error_info, &
//										ls_product_code + '~t' + ls_product_state + '~t' + ls_product_status + '~tI', &
//										ls_this_product, &
//										ls_tree_products)
lb_ret = iu_ws_pas1.nf_pasp02fr(	istr_error_info, &
										ls_product_code + '~t' + ls_product_state + '~t' + ls_product_status + '~tI', &
										ls_this_product, &
										ls_tree_products)
If Not lb_ret Then
	IF Left(istr_error_info.se_message, 6) = 'PAS008' Then
		ltvi_temp.Children = False
		This.SetItem(handle,ltvi_temp)
	End If
	
	return 1
End If

ids_tree.Reset()

If LEN(TRIM(ls_tree_products)) = 0 Then return 0

If ids_tree.ImportString(ls_tree_products) < 0 Then
	messagebox(parent.title,"Data did not pass validation.")
	return 0
End If
	
For li_counter = 1 to ids_tree.RowCount()
	ltvi_child.Label = ids_tree.GetItemString(li_counter,"fab_product_code") + " " + &
							ids_tree.GetItemString(li_counter,"product_state") + " " + &
							ids_tree.GetItemString(li_counter,"product_status") + " " + &							
							ids_tree.GetItemString(li_counter,"fab_type") + " " + &
							TRIM(ids_tree.GetItemString(li_counter,"fab_product_description"))
			
			ltvi_child.Children = True	
	If ids_tree.GetItemString(li_counter,"new_step_ind") = '*' Then
		ltvi_child.PictureIndex = 2
		ltvi_child.SelectedPictureIndex = 2
	Else
		ltvi_child.PictureIndex = 1
		ltvi_child.SelectedPictureIndex = 1
	End If
	
	ltvi_child.Data = ids_tree.GetItemString(li_counter,"fab_product_code") + "~t" + &
							ids_tree.GetItemString(li_counter,"product_state") + "~t" + &
							ids_tree.GetItemString(li_counter,"product_status") + "~tN"
	
	If li_counter = 1 Then
		ll_tvnext = This.InsertItemFirst(handle,ltvi_child)
	Else
		ll_tvnext = This.InsertItem(handle, ll_tvnext,ltvi_child)
	End If
Next

SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

Return 0

end event

event rightclicked;return wf_get_parent(handle)
end event

event doubleclicked;return wf_get_parent(handle)
end event

