HA$PBExportHeader$u_begin_end_weights.sru
forward
global type u_begin_end_weights from u_base_dw_ext
end type
end forward

global type u_begin_end_weights from u_base_dw_ext
integer width = 352
integer height = 172
string dataobject = "d_begin_end_weights"
boolean border = false
end type
global u_begin_end_weights u_begin_end_weights

forward prototypes
public function integer uf_get_begin_wgt ()
public function integer uf_get_end_wgt ()
public subroutine uf_set_begin_wgt (integer ai_begin_wgt)
public subroutine uf_set_end_wgt (integer ai_end_wgt)
public function integer uf_enable (boolean ab_enable)
end prototypes

public function integer uf_get_begin_wgt ();Return This.GetItemNumber(1, 'begin_wgt')
end function

public function integer uf_get_end_wgt ();Return This.GetItemNumber(1, 'end_wgt')
end function

public subroutine uf_set_begin_wgt (integer ai_begin_wgt);This.SetItem(1,"begin_wgt",ai_begin_wgt)
end subroutine

public subroutine uf_set_end_wgt (integer ai_end_wgt);This.SetItem(1,"end_wgt",ai_end_wgt)
end subroutine

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.begin_wgt.Background.Color = 16777215
	This.object.end_wgt.Background.Color = 16777215
	This.object.begin_wgt.Protect = 0
	This.object.end_wgt.Protect = 0
Else
	This.object.begin_wgt.Background.Color = 67108864
	This.object.end_wgt.Background.Color = 67108864
	This.object.begin_wgt.Protect = 1
	This.object.end_wgt.Protect = 1
	This.ib_updateable = False
End If

Return 1


end function

event constructor;call super::constructor;This.insertRow(0)
end event

event itemchanged;call super::itemchanged;String				ls_columnname
Long					ll_value, &
						ll_max = 1500,&
						ll_min = 300,&	
						ll_compare
//Date					ldt_value

ls_columnname = dwo.name

Choose Case ls_columnname 
	Case "begin_wgt" 
		ll_value = long(Data) 
		ll_compare = Mod(ll_value, 5)
		If ll_compare > 0 Then
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("All weights must be multiples of 5")   
			Return 1
		End If
		IF Not ll_value = 0 Then
			If ll_value > ll_max or ll_value < ll_min  Then
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("All weights must be less than " + string(ll_max) + " and greater than " + String(ll_min))   
				Return 1
			End If
		End If
		If GetItemStatus(row, "end_wgt", primary!) = &
				DataModified! Then
			If ll_value > GetitemNumber(row,"end_wgt") Then
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("From Weight must be less than To Weight")   
				Return 1
			End If
		End If
	Case "end_wgt" 
		ll_value = long(Data)
		ll_compare = Mod(ll_value, 5)
		If ll_compare > 0 Then
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("All weights must be multiples of 5")   
			Return 1
		End If
		IF Not ll_value = 0 Then
			If ll_value > ll_max or ll_value < ll_min  Then
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("All weights must be less than " + string(ll_max) + " and greater than " + String(ll_min))   
				Return 1
			End If
		End If
		If GetItemStatus(row, "begin_wgt", primary!) = &
				DataModified! Then
			If ll_value < GetitemNumber(row,"begin_wgt") Then
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("To Weight must be greater than the From Weight")				
				Return 1
			End If
		End If

End Choose


end event

event itemerror;call super::itemerror;Return 1
end event

on u_begin_end_weights.create
end on

on u_begin_end_weights.destroy
end on

