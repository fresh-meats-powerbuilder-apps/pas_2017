HA$PBExportHeader$w_hot_box_actual_inq.srw
forward
global type w_hot_box_actual_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_hot_box_actual_inq
end type
type dw_dates from u_begin_end_dates within w_hot_box_actual_inq
end type
end forward

global type w_hot_box_actual_inq from w_base_response_ext
integer width = 1573
integer height = 520
long backcolor = 67108864
dw_plant dw_plant
dw_dates dw_dates
end type
global w_hot_box_actual_inq w_hot_box_actual_inq

on w_hot_box_actual_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_dates=create dw_dates
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_dates
end on

on w_hot_box_actual_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_dates)
end on

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
 				ls_plant_desc, &
				ls_begin_date,&
				ls_end_date
				
Int			li_pa_range				
Date			ldt_begin_date,ldt_relative_date
u_string_functions		lu_strings



If dw_plant.AcceptText() = -1 or &
	dw_dates.AcceptText() = -1 Then return 

ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End if

ls_begin_date = String(dw_dates.uf_get_begin_date())
If lu_strings.nf_IsEmpty(ls_begin_date) Then
	iw_frame.SetMicroHelp("Begin Date is a required field")
	dw_dates.SetFocus()
	Return
End if

ls_end_date = String(dw_dates.uf_get_end_date())
If lu_strings.nf_IsEmpty(ls_end_date) Then
	iw_frame.SetMicroHelp("End Date is a required field")
	dw_dates.SetFocus()
	Return
Else
	ldt_begin_date = date(ls_begin_date)
	ldt_relative_date = RelativeDate(date(ls_end_date), -59)
	If ldt_begin_date < ldt_relative_date Then
		iw_frame.SetMicroHelp("You may not inquire on more then 60 days worth of data")
		dw_dates.SetFocus()
		Return
	End If
End if

iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('begin_date',ls_begin_date)
iw_parentwindow.Event ue_set_data('end_date',ls_end_date)


ib_ok_to_close = True

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('begin_date')
dw_dates.uf_set_begin_date(Date(Message.StringParm))

iw_parentwindow.Event ue_get_data('end_date')
dw_dates.uf_set_end_date(Date(Message.StringParm))

dw_dates.uf_enable(True)




end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_hot_box_actual_inq
integer x = 1262
integer y = 304
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_hot_box_actual_inq
integer x = 974
integer y = 304
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_hot_box_actual_inq
integer x = 686
integer y = 304
integer taborder = 20
end type

type dw_plant from u_plant within w_hot_box_actual_inq
integer x = 110
integer y = 12
integer taborder = 10
boolean bringtotop = true
end type

type dw_dates from u_begin_end_dates within w_hot_box_actual_inq
integer y = 104
integer taborder = 11
boolean bringtotop = true
end type

event constructor;call super::constructor;This.InsertRow ( 0 )
end event

event itemchanged;String				ls_columnname

Date					ldt_value

// I have disabled the ansestor code
ls_columnname = dwo.name

Choose Case ls_columnname 
	Case "begin_date" 
		ldt_value = Date(Data)
		If ldt_value > today() Then
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("Begin Date must be less than or equal to today")   
			Return 1
		End If
		If GetItemStatus(row, "end_date", primary!) = &
				DataModified! Then
			If ldt_value > Getitemdate(row,"end_date") Then
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("Begin Date must be less than End Date")   
				Return 1
			End If
		End If
	Case "end_date" 
		ldt_value = Date(Data)
		If ldt_value > today() Then
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("End Date must be less than or equal to today")   
			Return 1
		End If
		If GetItemStatus(row, "begin_date", primary!) = &
				DataModified! Then
			If ldt_value < Getitemdate(row,"begin_date") Then
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("End Date must be greater than the Begin Date")				
				Return 1
			End If
		End If

End Choose


end event

