HA$PBExportHeader$w_carcass_tracking_view.srw
$PBExportComments$IBDKDLD
forward
global type w_carcass_tracking_view from w_base_sheet_ext
end type
type cb_save_sources from commandbutton within w_carcass_tracking_view
end type
type cb_retrieve_sources from commandbutton within w_carcass_tracking_view
end type
type rb_deselect from radiobutton within w_carcass_tracking_view
end type
type rb_select from radiobutton within w_carcass_tracking_view
end type
type dw_transfer_date from u_transfer_date within w_carcass_tracking_view
end type
type dw_satellite_cattle from u_base_dw_ext within w_carcass_tracking_view
end type
type dw_plant from u_plant within w_carcass_tracking_view
end type
end forward

global type w_carcass_tracking_view from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 3342
integer height = 1772
long backcolor = 67108864
cb_save_sources cb_save_sources
cb_retrieve_sources cb_retrieve_sources
rb_deselect rb_deselect
rb_select rb_select
dw_transfer_date dw_transfer_date
dw_satellite_cattle dw_satellite_cattle
dw_plant dw_plant
end type
global w_carcass_tracking_view w_carcass_tracking_view

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
end prototypes

type variables
u_rmt001		iu_rmt001
u_ws_pas4	iu_ws_pas4

s_error		istr_error_info

String		is_update_string,&
		is_detail,&
		is_column_headings,&
		is_hold_column_headings, &
		is_cb_resource, &
		is_process_date, &
		is_header, is_col

Integer		il_rec_count
Long		il_SelectedColor,&
		il_SelectedTextColor, il_row

Boolean		ib_update_inquire,&
		Ib_whatifm, &
		ib_retreive_resource, &
		ib_save_resource
		

Window		iw_parentwindow,&		
		iw_temp		

DataStore	iw_data, &
				ids_retrieved_resources

end variables

forward prototypes
public subroutine wf_reset_datawindow ()
public subroutine wf_set_initial_values ()
public function boolean wf_whatif ()
public subroutine wf_calc_new_beg_inv (long al_row)
public subroutine wf_whatif_inquire ()
public subroutine wf_calc_perpetual_inv_down (long al_row, string as_column_name, string as_data)
public subroutine wf_calc_b_shift_avail_change (long al_row, string as_column_name, string as_data)
public subroutine wf_populate_column_headings ()
public subroutine wf_populate_column_headings_resource ()
public function boolean wf_update_modify (long al_row, string as_column_name)
public function boolean wf_retrieve ()
public subroutine wf_get_header_data ()
public subroutine wf_get_row_col ()
public subroutine wf_get_col_names (string ls_char_names)
public function string wf_get_modifyable_data ()
public subroutine wf_calc_b_shift_avail (long al_row)
public subroutine wf_call_downgrade_window (long al_row, string as_column_name)
public function boolean wf_retrieve_resources ()
public function boolean wf_check_for_row_selected ()
public subroutine wf_calc_perpetual_inv (long al_row)
public function boolean wf_update ()
end prototypes

public subroutine wf_reset_datawindow ();String	ls_TitleModify, ls_describe,ls_temp
Long 		ll_column = 1


ls_describe = "col_" + String(ll_column) + ".Visible"
Do While dw_satellite_cattle.Describe(ls_describe) > '0'
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 0 " + &
		" col_" + String(ll_column) + ".Visible = 0 " 
	If ll_column = 1 Then
		ls_TitleModify += " co_col_" + String(ll_column) + ".Visible = 0 " + &
			" t_total_" + String(ll_column) + ".Visible = 0 " + &
			" detail_text_" + String(ll_column) + ".Visible = 0 " 
	End If
	ll_column ++
	ls_describe = "col_" + String(ll_column) + ".Visible"
Loop
////messagebox('reset values',ls_titlemodify)
////messagebox('modify result',dw_satellite_cattle.Modify (ls_TitleModify))
////messagebox('problem',Mid ( ls_TitleModify, 124 ))
dw_satellite_cattle.Modify ( ls_TitleModify )

end subroutine

public subroutine wf_set_initial_values ();//Long ll_row = 1
//
//DO UNTIL ll_row > 10
//	dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","N")
//	ll_row ++
//LOOP
//
end subroutine

public function boolean wf_whatif ();//Integer li_row
//
//This.SetRedraw(False) 
//
//li_row = 1
//DO UNTIL li_row > 10
//	CHOOSE CASE li_row
//	CASE 5,7,9,10
//		dw_satellite_cattle.SetItem(li_row, "whatif_row_protected", "Y")
////		dw_satellite_cattle.
//	END CHOOSE
//	li_row ++
//LOOP
//
//This.SetRedraw(True) 
//dw_satellite_cattle.ResetUpdate()
//Ib_whatif = True
//


Return True
end function

public subroutine wf_calc_new_beg_inv (long al_row);String		ls_name, &
				ls_detail_text, &
				ls_name_full, &
				ls_new_amt
Dec			ld_new_amt
long			ll_row, &
				ll_beg_inv, &
				ll_on_tram, &
				ll_downgrade_from, &
				ll_downgrade_to, &
				ll_cold_trans, &
				ll_cut_a_shift, &
				ll_cut_b_shift, &
				ll_perpetual_inv, &
				ll_b_shift_avail, &
				ll_trans_in
long				ll_column_num

Char				lc_status_ind					

string			ls_column, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name


// I did this to make the code more genericls_name_full = dwo.name
//ls_name_full = dwo.name
//ls_name = Left(ls_name_full, Len(ls_name_full) - 2)

//  get values to calculate perpetual inventory	IF first occurrance, use beg inv, else use actual and loop
				
	ll_row = al_row - 9
	ll_column_num = 1
	ls_describe = "col_" + String(ll_column_num) + ".Visible"
	Do While dw_satellite_cattle.Describe(ls_describe) > '0'
		ls_column_name = "col_" + String(ll_column_num)
		//If dw_satellite_cattle.GetItemStatus(ll_row,ls_column_name, Primary!) = DataModified! Then
			ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row, ls_column_name)
			ll_row = al_row - 8
			ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row, ls_column_name)
		//End If
		ll_column_num ++
		ls_describe = "col_" + String(ll_column_num) + ".Visible"
	Loop
////				ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row, ls_column)
//				ll_row = 2
//				ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_row = 4
//				ll_downgrade_from = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_row =5
//				ll_downgrade_to = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_row = 10
//				ll_cut_b_shift = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_perpetual_inv = ( ll_beg_inv + ll_on_tram + ll_trans_in - ll_downgrade_from + ll_downgrade_to - ll_cut_a_shift - ll_cut_b_shift )
//				ll_row = al_row - 5
//				dw_satellite_cattle.SetItem(ll_row, ls_name_full, ll_perpetual_inv)
//			end if	
//	if al_row > 16 then 
//		//loop
//	end if
//

	
//		ll_column_count = 1	
//		DO UNTIL ll_column_count > il_rec_count
//			If ll_column_count > 1 Then ls_return_string += '~t'
//			ls_return_string += String(dw_satellite_cattle.GetItemNumber(ll_row,"col_" + String(ll_column_count)))
//			ll_column_count ++
//		LOOP
//			ls_return_string += '~r~n'
//	END CHOOSE
//	ll_row ++
//LOOP

//messagebox('return data',ls_return_string)

//ls_return_string = Mid(ls_return_string,1,len(ls_return_string) - 2 )

//Return ls_string true
end subroutine

public subroutine wf_whatif_inquire ();Long						ll_rec_count, &
							ll_row, &
							ll_count 
							
							
Int	il_temp							
							
String					ls_plant, &
							ls_column, & 
							ls_detail, & 
							ls_column_headings, & 
							ls_header, &
							ls_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_describe
Boolean					lb_no_characteristics							
u_string_functions 	lu_string				

	
If dw_satellite_cattle.AcceptText() = -1 Then Return
				
SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait.. Calculating your Scenario")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_whatif_inquire"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr27mr_inq_whatif_view"

ls_header  = dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_transfer_date.uf_get_transfer_date(), 'yyyy-mm-dd') + '~t'

ls_column_headings = is_column_headings

ls_detail = wf_get_modifyable_data()

wf_reset_datawindow()

dw_satellite_cattle.reset()

//If Not iu_rmt001.uf_rmtr27mr_inq_cartracking_whatif(istr_error_info, & 
//										is_hold_column_headings, &
//										ls_detail, &
//										ls_header) Then 
//										This.SetRedraw(True) 
//										Return
//End If			

////Populate the Column Headings
is_column_headings = is_hold_column_headings
wf_populate_column_headings()

ll_rec_count = dw_satellite_cattle.ImportString(ls_detail)

//dw_satellite_cattle.SetItem(1, "back_color", il_SelectedColor)
//dw_satellite_cattle.SetItem(1, "text_color", il_SelectedTextColor)

If ll_rec_count > 0 and Not lb_no_characteristics Then 
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

// Must do because of the reinquire
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '700'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '700'

This.SetRedraw(True) 

dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.SetFocus()

Return 

end subroutine

public subroutine wf_calc_perpetual_inv_down (long al_row, string as_column_name, string as_data);String		ls_name, &
				ls_detail_text, &
				ls_name_full, &
				ls_new_amt, &
				ls_dtl_name_ind, &
				ls_dtl_perp_row_ind, &
				ls_first_time, &
				ls_dtl_row_changed_id 
				
Dec			ld_new_amt
long			ll_row, &
				ll_beg_inv, &
				ll_on_tram, &
				ll_downgrade_from, &
				ll_downgrade_to, &
				ll_cold_trans, &
				ll_cut_a_shift, &
				ll_cut_b_shift, &
				ll_perpetual_inv, &
				ll_b_shift_avail, &
				ll_trans_in, &
				ll_column_num, &
				ll_row_loop, &
				ll_row_count, &
				ll_save_row, &
				ll_save_perp_row, &
				ll_perp_row, &
				ll_save_perpetual_inv, &
				ll_save_beg_inv

Char				lc_status_ind					

string			ls_column, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name, ls_dtl_row_id, &
					ls_dtl_perp_row_id,&
					ls_changed_data, &
					ls_save_dtl_row_id



	ll_row_count = dw_satellite_cattle.RowCount()		
	
   ls_column_name = as_column_name  
	ll_row_loop = al_row
	ls_save_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
	
	if ll_row_loop < 10 then
	   ll_row_loop = 1
	end if
	
//	ls_first_time = 'Y'
	ll_save_perpetual_inv = 0
	ls_changed_data = as_data
	//ls_dtl_row_changed_id = al_row
	// use beginning inv if first row, otherwise use perp inv to start with
	if ll_row_loop = 1 then
		ll_save_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
		//continue you are on the first row
	else	
		// find perpetual beg perpetual inv row
		do while ls_dtl_row_id  <> 'PI' 
			ll_row_loop = ll_row_loop - 1
			ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
		loop
	end if
	if ls_dtl_row_id = 'PI' then
		ll_save_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
	end if
	// start on next row past beg/perp inv
	ll_row_loop = ll_row_loop + 1
	ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
	Do While ll_row_loop < ll_row_count
				if ls_dtl_row_id = 'AA' or ls_dtl_row_id = 'EA' then
					if ll_row_loop  = al_row then
			//			if ls_first_time = 'Y' then
							ll_cut_a_shift = long(ls_changed_data)
			//				ls_first_time = 'N'
			//			end if
					else
				      ll_cut_a_shift = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
					end if	
				end if
				if ls_dtl_row_id = 'AB' or ls_dtl_row_id = 'EB' then
					if ll_row_loop = al_row then
					//	if ls_first_time = 'Y' then
							ll_cut_b_shift = long(ls_changed_data)
					//		ls_first_time = 'N'
					//	end if
					else
				      ll_cut_B_shift = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
					end if	
				end if
//				
				if ls_dtl_row_id = 'DF' then
					ll_downgrade_from = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'DT' then
					ll_downgrade_to = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'AT' or ls_dtl_row_id = 'ET' then
					ll_trans_in = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'TM' then
					ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'PI' then
					ll_perpetual_inv = ( ll_save_beg_inv + ll_on_tram + ll_trans_in - ll_downgrade_from + ll_downgrade_to - ll_cut_a_shift - ll_cut_b_shift )
					dw_satellite_cattle.SetItem(ll_row_loop, ls_column_name, ll_perpetual_inv)
					ll_save_beg_inv = ll_perpetual_inv	
			end if	
				ll_row_loop = ll_row_loop + 1
				ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
		Loop
//
end subroutine

public subroutine wf_calc_b_shift_avail_change (long al_row, string as_column_name, string as_data);String		ls_name, &
				ls_detail_text, &
				ls_name_full, &
				ls_dtl_row_changed_id 				
Dec			ld_new_amt
long			ll_row, &
				ll_cut_b_shift, &
				ll_orig_cut, &
				ll_column_num, &
				ll_row_loop, &
				ll_b_shift_avail, &
				ll_beg_inv, &
				ll_on_tram, &
				ll_downgrade_from, &
				ll_downgrade_to, &
				ll_cut_a_shift, &
				ll_cold_tran, &
				ll_row_count
				

string			ls_column, &
					ls_dtl_row_id, &
					ls_changed_data, &
					ls_column_name




	ll_row_count = dw_satellite_cattle.RowCount()		

   ls_column_name = as_column_name  
	ll_row_loop = al_row
	ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
	ls_changed_data = as_data
	ls_dtl_row_changed_id = ls_dtl_row_id
	if ls_dtl_row_id = 'AA' or ls_dtl_row_id = 'EA' then
		do while ls_dtl_row_id  <> 'BA'
			if ls_dtl_row_id = 'AA' or ls_dtl_row_id = 'EA' then
				ll_orig_cut = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
			end if
//		if ls_dtl_row_id = 'AB' or ls_dtl_row_id = 'EB' then
//			ll_orig_cut = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
//			ll_row_loop = ll_row_loop - 2
//		end if
			ll_row_loop = ll_row_loop + 1
			ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
		loop
		ll_cut_b_shift = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
		ll_cut_b_shift	= ll_cut_b_shift + ll_orig_cut - long(ls_changed_data)	
		dw_satellite_cattle.SetItem(ll_row_loop, ls_column_name, ll_cut_b_shift)
	end if
	
	ll_row_loop = ll_row_loop + 1
	do while ll_row_loop < ll_row_count
		ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
		if ls_dtl_row_id = 'PI' then
			ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
		end if
		if ls_dtl_row_id = 'TM' then
			ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
		end if
		if ls_dtl_row_id = 'DF' then
			ll_downgrade_from = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
		end if
		if ls_dtl_row_id = 'DT' then
			ll_downgrade_to = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
		end if
		if ls_dtl_row_id = 'AA' or ls_dtl_row_id = 'EA' then
		   ll_cut_a_shift = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
		end if
		if ls_dtl_row_id = 'CT' then
		   ll_cold_tran = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
		end if
		if ls_dtl_row_id = 'BA' then 
			ll_b_shift_avail = ( ll_beg_inv + ll_on_tram - ll_downgrade_from + ll_downgrade_to - ll_cut_a_shift + ll_cold_tran)
			dw_satellite_cattle.SetItem(ll_row_loop, ls_column_name, ll_b_shift_avail)
		end if
		ll_row_loop = ll_row_loop + 1
		
		Loop

end subroutine

public subroutine wf_populate_column_headings ();Long						ll_column = 1
String					ls_TitleModify,ls_temp, ls_trim
u_string_functions 	lu_string				

is_hold_column_headings = is_column_headings

il_rec_count = 0
DO WHILE Not lu_string.nf_amiempty(is_column_headings) or ll_column > 50
	ls_temp = lu_string.nf_gettoken(is_column_headings,"~t")
	ls_trim = /* right trim to remove spaces*/RightTrim(ls_temp)
	
	ls_TitleModify += 'col_'  + String(ll_column) + "_t.text = '" + ls_trim + "'" &
		            + " col_"  + String(ll_column) + "_t.Visible = 1 " &
 		            + " col_"  + String(ll_column) + ".Visible = 1 " &
						+ " col_"  + string(ll_column) + ".tag = '" + ls_trim + "'" // set the tag of the columns
						 
		If ll_column = 1 Then
			ls_TitleModify += " co_col_"      + String(ll_column) + ".Visible = 1 " &
				            +  " t_total_"     + String(ll_column) + ".Visible = 1 " &
				            +  " detail_text_" + String(ll_column) + ".Visible = 1 " 
		End If
	ll_column ++
LOOP
il_rec_count = ll_column - 1
//messagebox('record count', string(il_rec_count))
////messagebox('update values',ls_titlemodify)
////messagebox('modify result',dw_satellite_cattle.Modify (ls_TitleModify))
////messagebox('problem',Mid ( ls_TitleModify, 130 ))
dw_satellite_cattle.Modify ( ls_TitleModify )

end subroutine

public subroutine wf_populate_column_headings_resource ();Long						ll_column = 1
String					ls_TitleModify,ls_temp, ls_trim
u_string_functions 	lu_string				

is_hold_column_headings = is_column_headings

il_rec_count = 0
DO WHILE Not lu_string.nf_amiempty(is_column_headings) or ll_column > 50
	ls_temp = lu_string.nf_gettoken(is_column_headings,"~t")
	ls_trim = /* right trim to remove spaces*/RightTrim(ls_temp)
	
	ls_TitleModify += 'col_'  + String(ll_column) + "_t.text = '" + ls_trim + "'" &
		            + " col_"  + String(ll_column) + "_t.Visible = 1 " &
 		            + " col_"  + String(ll_column) + ".Visible = 1 " &
						+ " col_"  + string(ll_column) + ".tag = '" + ls_trim + "'" // set the tag of the columns
						 
		If ll_column = 1 Then
			ls_TitleModify += " co_col_"      + String(ll_column) + ".Visible = 1 " &
				            +  " t_total_"     + String(ll_column) + ".Visible = 1 " &
				            +  " detail_text_" + String(ll_column) + ".Visible = 1 " 
		End If
	ll_column ++
LOOP
il_rec_count = ll_column - 1
//messagebox('record count', string(il_rec_count))
////messagebox('update values',ls_titlemodify)
////messagebox('modify result',dw_satellite_cattle.Modify (ls_TitleModify))
////messagebox('problem',Mid ( ls_TitleModify, 130 ))
ids_retrieved_resources.Modify ( ls_TitleModify )

end subroutine

public function boolean wf_update_modify (long al_row, string as_column_name);String						ls_describe, &
								ls_select_cb

DWBuffer						ldwb_buffer


ldwb_buffer = Primary!


ls_select_cb = dw_satellite_cattle.GetItemString(al_row,"select_cb")
if ib_save_resource = true then
	ls_select_cb = 'Y'
else
	ls_select_cb = 'N'
end if
ls_describe = as_column_name + "_t" + ".text"	
is_update_string += is_process_date + "~t"
is_update_string += dw_satellite_cattle.GetItemString(al_row,"row_name_id") + "~t"

is_update_string += ls_select_cb + "~t"
is_update_string += dw_satellite_cattle.Describe(ls_describe) + "~t"
is_update_string += &
	String(dw_satellite_cattle.GetItemNumber(al_row,as_column_name,ldwb_buffer,False)) + "~r~n"	 

Return True
end function

public function boolean wf_retrieve ();Long						ll_rec_count, &
							ll_row, &
							ll_count 
							
							
Int	il_temp							
							
String					ls_plant, &
							ls_column, & 
							ls_header, &
							ls_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_describe, &
							ls_source_ind
							
Boolean					lb_no_characteristics							
u_string_functions 	lu_string				

	
If dw_plant.AcceptText() = -1 Then Return False
If dw_transfer_date.AcceptText() = -1 Then Return False
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

If Not ib_update_inquire and not ib_retreive_resource Then
	OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
End If

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)
//If ib_update_inquire Then
//	iw_frame.SetMicroHelp("Modification Successful on Update... Wait.. Reinquiring Database")
//	ib_update_inquire = False
//Else	
	iw_frame.SetMicroHelp("Wait.. Inquiring Database")
//End If
//ib_whatif = false
ib_retreive_resource = false

This.SetRedraw(False) 
// clear out prev values reset the datawindow
wf_reset_datawindow()

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr20mr_inq_carcass_view"
if is_cb_resource = 'R' then
    ls_source_ind = 'R'
else
	 ls_source_ind = ' '
end if

ls_header  = dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_transfer_date.uf_get_transfer_date(), 'yyyy-mm-dd') + '~t'
ls_header += ls_source_ind + '~t'


dw_satellite_cattle.reset()

/*If Not iu_rmt001.uf_rmtr20mr_inq_car_tracking_view(istr_error_info, & 
										is_column_headings, &
										is_detail, &
										ls_header) Then 
										This.SetRedraw(True) 
										Return False
End If			
*/
If Not iu_ws_pas4.NF_RMTR20NR(istr_error_info, & 
										is_column_headings, &
										is_detail, &
										ls_header) Then 
										This.SetRedraw(True) 
										Return False
End If	


If lu_string.nf_amiempty(is_column_headings) Then
	MessageBox('No Plant Characteristics','There are no PA/Plant Characteristics assigned for Plant ' + dw_plant.uf_get_plant_code() + &
	'.  You will not be able to use this window until they have been entered.',StopSign!,OK!)
	lb_no_characteristics = True 
End If
//Populate the Column Headings
wf_populate_column_headings()


ll_rec_count = dw_satellite_cattle.ImportString(is_detail)

//dw_satellite_cattle.SetItem(1, "back_color", il_SelectedColor)
//dw_satellite_cattle.SetItem(1, "text_color", il_SelectedTextColor)

If ll_rec_count > 0 and Not lb_no_characteristics Then 
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if
wf_get_modifyable_data()
is_cb_resource = ' '

// Must do because of the reinquire
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '1140'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '1140'

This.SetRedraw(True) 

dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.SetFocus()

Return True

end function

public subroutine wf_get_header_data ();
	Message.StringParm = is_header
end subroutine

public subroutine wf_get_row_col ();message.Stringparm = string(il_row) + '~t' + is_col
end subroutine

public subroutine wf_get_col_names (string ls_char_names);u_string_functions		lu_strings
string ls_char, ls_col_names, ls_col, ls_test
integer li_col

ls_col_names = ''
do while len(ls_char_names) > 0
	ls_char = lu_strings.nf_gettoken(ls_char_names, '~t')
	li_col = 1
	do while li_col < 51
		ls_col = "col_" + string(li_col)
		ls_test = dw_satellite_cattle.describe(ls_col + '.tag')
		if trim(ls_test) = trim(ls_char) then
			ls_col_names += ls_col + '~t'
		elseif ls_test = '?' then
			li_col = 51
		end if
		li_col++
	loop
loop

Message.StringParm = ls_col_names
end subroutine

public function string wf_get_modifyable_data ();Long 		ll_row = 1, ll_column_count, &
			ll_perp_inv
String 	ls_return_string = "", &
			ls_detail_text, &
			ls_sched_release, &
			ls_status


DO UNTIL ll_row > dw_satellite_cattle.RowCount()
  	ls_detail_text = dw_satellite_cattle.GetItemString(ll_row,"row_name_id")
	ls_sched_release = dw_satellite_cattle.GetItemString(ll_row,"prod_release_sched")
//	CASE 1, 5, 7, 9, 10
	//	messagebox(' ', string(dw_satellite_cattle.getItemStatus(ll_row,1,Primary!)))
	   
	Choose case dw_satellite_cattle.getItemStatus(ll_row, 0, Primary!)
		Case DataModified!
			ls_status = 'MODI'
		Case NewModified!
			ls_status = 'NEWM'
		Case NotModified!
			ls_status = 'NOTM'
		Case New!
			ls_status = 'NEW '
	End Choose
		if ls_sched_release = 'Y' then
			dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","Y")
		else	
		if ls_detail_text = 'AA' then 
				dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","N")
			//	wf_calc_perpetual_inv(ll_row)
		else
			if ls_detail_text = 'AB' then
				dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","N")
			else
				if ls_detail_text = 'EA' then
					dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","N")
				else
					if ls_detail_text = 	'EB' then
						dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","N")
					else	
						if ls_detail_text = 'BA' then 	
							dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","H")
					//		dw_satellite_cattle.SelectRow(ll_row, True)
						else	
							if ls_detail_text = 'AI' then
								dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","H")
 //							dw_satellite_cattle.SelectRow(ll_row, True)
							else
								if ls_detail_text = 'PI'   then 
									dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","H")
									wf_calc_perpetual_inv(ll_row)
									wf_calc_b_shift_avail(ll_row)
								else	
									if ls_detail_text = 'BI' then 
										dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","Y")
										wf_calc_b_shift_avail(ll_row)
										
			//						dw_satellite_cattle.SelectRow(ll_row, True)
								else	
									dw_satellite_cattle.SetItem(ll_row, "whatif_row_protected","Y")
								end if
							end if
						end if
					end if
				end if	
			end if
		end if
	end if
end if	
	
//		ll_column_count = 1	
//		DO UNTIL ll_column_count > il_rec_count
//			If ll_column_count > 1 Then ls_return_string += '~t'
//			ls_return_string += String(dw_satellite_cattle.GetItemNumber(ll_row,"col_" + String(ll_column_count)))
//			ll_column_count ++
//		LOOP
//			ls_return_string += '~r~n'
//	END CHOOSE
if ls_status = 'NEWM' then
	dw_satellite_cattle.SetItemStatus(ll_row,0,Primary!,NEWMODIFIED!)
else
	if	ls_status = 'NEW ' then
		dw_satellite_cattle.SetItemStatus(ll_row,0,Primary!,NEW!)
	else
		if	ls_status = 'NOTM' then
		dw_satellite_cattle.SetItemStatus(ll_row,0,Primary!,NOTMODIFIED!)
		end if
	end if
end if

	ll_row ++
LOOP

//messagebox('return data',ls_return_string)

//ls_return_string = Mid(ls_return_string,1,len(ls_return_string) - 2 )

Return ls_return_string
end function

public subroutine wf_calc_b_shift_avail (long al_row);long			ll_beg_inv, &
				ll_on_tram, &
				ll_downgrade_from, &
				ll_downgrade_to, &
				ll_cold_trans, &
				ll_cut_a_shift, &
				ll_b_shift_avail, &
				ll_cold_tran, &
				ll_column_num, &
				ll_row_loop

string		ls_column, &
				ls_describe, &
				ls_column_name, &
				ls_dtl_row_id


   ll_row_loop = al_row
	ll_column_num = 1
	ls_describe = "col_" + String(ll_column_num) + ".Visible"
	Do While dw_satellite_cattle.Describe(ls_describe) > '0'
		ls_column_name = "col_" + String(ll_column_num)
			ll_row_loop = al_row
			ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
		   Do While ls_dtl_row_id <> 'BA'
				if ls_dtl_row_id = 'BI' or ls_dtl_row_id = 'AI' then
					if ll_row_loop = 1 then
						ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
					end if
				end if
				if ls_dtl_row_id = 'PI' then
						ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'TM' then
					ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'DF' then
					ll_downgrade_from = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'DT' then
					ll_downgrade_to = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'AA' or ls_dtl_row_id = 'EA' then
				   ll_cut_a_shift = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				
				if ls_dtl_row_id = 'CT' then
				   ll_cold_tran = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				ll_row_loop = ll_row_loop + 1
		//		if ll_row_loop > 0 then
					ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
		//		end if
		Loop
		ll_b_shift_avail = ( ll_beg_inv + ll_on_tram - ll_downgrade_from + ll_downgrade_to - ll_cut_a_shift + ll_cold_tran)
		dw_satellite_cattle.SetItem(ll_row_loop, ls_column_name, ll_b_shift_avail)
		ll_column_num ++
		ls_describe = "col_" + String(ll_column_num) + ".Visible"
	Loop
dw_satellite_cattle.SetItemStatus(ll_row_loop,0,Primary!,NOTMODIFIED!)
end subroutine

public subroutine wf_call_downgrade_window (long al_row, string as_column_name);String		ls_name, &
				ls_detail_text, &
				ls_name_full, &
				ls_column, &
				ls_header, &
					ls_describe,ls_column_name, ls_dtl_row_id, &
					ls_dtl_perp_row_id,&
					ls_from_to_ind, &
					ls_save_dtl_row_id, &
					ls_process_date, &
					ls_Char_name
					
long			ll_row
Window	lw_temp



//ls_describe = as_column_name + "_t" + ".text"	
//is_update_string += is_process_date + "~t"
//is_update_string += dw_satellite_cattle.GetItemString(al_row,"row_name_id") + "~t"
//is_update_string += dw_satellite_cattle.GetItemString(al_row,"select_cb") + "~t"
//is_update_string += dw_satellite_cattle.Describe(ls_describe) + "~t"
//	
//   ls_column_name = as_column_name  
	ll_row = al_row
	ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row,"row_name_id")
	if ls_dtl_row_Id = 'DF' then
		ls_from_to_ind = 'F' 
	else
		ls_from_to_ind = 'T'
	end if
	ls_char_name = as_column_name //dw_satellite_cattle.GetItemString(ll_row, as_column_name)
	ls_header  = dw_plant.uf_get_plant_code() + '~t'
//	ls_process_date = '          '
	do until ls_process_date > ' ' and ls_process_date > '          '
		ls_process_date = dw_satellite_cattle.GetItemString(ll_row,"process_date")
		ll_row = ll_row - 1
	loop
	
	ls_header += dw_plant.uf_get_plant_descr( ) + '~t' + ls_process_date + '~t' + ls_char_name + '~t' + ls_from_to_ind + '~r~n'
	
	is_header = ls_header
	//as_header = ls_header
	
	
	
	
//	OpenSheetWithParm (lw_temp, ls_header,This,"w_carcass_downgrade",iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)
	//OpenSheetWithParm(lw_temp, ls_header, "w_carcass_downgrade",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
	
	
	//OpenSheetWithParm(lw_temp, this, "w_carcass_downgrade",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
	OpenWithParm(w_carcass_downgrade, iw_parentwindow)
	
//	dw_satellite_cattle.SetItemStatus(al_row, 'status', primary!, notmodified!)
	//messagebox('', ls_header)
//	ls_header = + 

//ls_describe = as_column_name + "_t" + ".text"	
//is_update_string += is_process_date + "~t"
//is_update_string += dw_satellite_cattle.GetItemString(al_row,"row_name_id") + "~t"
//is_update_string += dw_satellite_cattle.GetItemString(al_row,"select_cb") + "~t"
	
	//wf_get_modifyable_data()

	return
	
	
end subroutine

public function boolean wf_retrieve_resources ();Long						ll_rec_count, &
							ll_row, &
							ll_count, &
							ll_column_num, &
							ll_qty, &
							ll_column_name
							
Int						il_temp							
							
String					ls_plant, &
							ls_column, & 
							ls_header, &
							ls_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_describe, &
							ls_source_ind, &
							ls_row_id, &
							ls_select_cb, &
							ls_column_name, &
							ls_qty, &
							ls_retrieve_selected
							
Boolean					lb_no_characteristics							
u_string_functions 	lu_string		

ids_retrieved_resources = Create DataStore
ids_retrieved_resources.DataObject = "d_carcass_tracking_view"

	
If dw_plant.AcceptText() = -1 Then Return False
//If dw_transfer_date.AcceptText() = -1 Then Return False
				
//This.TriggerEvent('closequery') 

//If Message.ReturnValue <> 0 Then Return False

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

//ib_whatif = false
ib_retreive_resource = false

This.SetRedraw(False) 
// clear out prev values reset the datawindow
//wf_reset_datawindow()

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr20mr_inq_carcass_view"
if is_cb_resource = 'R' then
    ls_source_ind = 'R'
else
	 ls_source_ind = ' '
end if

ls_header  = dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_transfer_date.uf_get_transfer_date(), 'yyyy-mm-dd') + '~t'
ls_header += ls_source_ind + '~t'

ll_rec_count = dw_satellite_cattle.rowCount()
ll_row = 1
do while ll_row < ll_rec_count
	ls_select_cb = dw_satellite_cattle.GetItemString(ll_row,"select_cb")
	if ls_select_cb = 'Y' then
		ls_retrieve_selected = 'Y'
		ll_row = ll_rec_count
	else
		ls_retrieve_selected = 'N'
	end if
	ll_row = ll_row + 1
loop
	
if ls_retrieve_selected = 'N' then
	SetMicroHelp("No rows selected to retrieve sources")
	This.SetRedraw(True) 
	return true
end if
	
//dw_satellite_cattle.reset()

/*If Not iu_rmt001.uf_rmtr20mr_inq_car_tracking_view(istr_error_info, & 
										is_column_headings, &
										is_detail, &
										ls_header) Then 
										This.SetRedraw(True) 
										Return False
End If		
*/
If Not iu_ws_pas4.NF_RMTR20NR(istr_error_info, & 
										is_column_headings, &
										is_detail, &
										ls_header) Then 
										This.SetRedraw(True) 
										Return False
End If	




wf_populate_column_headings_resource()

ll_rec_count = ids_retrieved_resources.ImportString(is_detail)
ll_row = 1
do while ll_row < ll_rec_count
	ls_row_Id = ids_retrieved_resources.GetItemString(ll_row,"row_name_id")
	ls_select_cb = dw_satellite_cattle.GetItemString(ll_row,"select_cb")
	if ls_row_id = 'AA' or ls_row_id = 'EA' or ls_row_id = 'AB' or ls_row_id = 'EB' then
		if ls_select_cb = 'Y' then
			ll_column_num = 1
			ls_describe = "col_" + String(ll_column_num) + ".Visible"
			Do While ids_retrieved_resources.Describe(ls_describe) > '0'
				ls_column_name = "col_" + String(ll_column_num)
	//		ls_qty = string(dw_satellite_cattle.GetItemNumber(ll_row,ls_column_name,ids_retrieved_resources,False))
				ll_qty = ids_retrieved_resources.GetItemNumber(ll_row, ls_column_name)
	//sap			if ll_qty <> 0 then
					dw_satellite_cattle.SetItem(ll_row, ls_column_name, ll_qty)
					dw_satellite_cattle.SetItemStatus(ll_row,ls_column_name, Primary!,DataModified!) 
	//sap			end if
				ll_column_num ++
				ls_describe = "col_" + String(ll_column_num) + ".Visible"
			Loop
		end if
	end if
//			ll_qty = ids_retrieved_resources.GetItem("

//	ls_row_id = 
	ll_row = ll_row + 1
loop

//dw_satellite_cattle.SetItem(1, "back_color", il_SelectedColor)
//dw_satellite_cattle.SetItem(1, "text_color", il_SelectedTextColor)

If ll_rec_count > 0 and Not lb_no_characteristics Then 
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if
wf_get_modifyable_data()
is_cb_resource = ' '

// Must do because of the reinquire
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '1140'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '1140'

This.SetRedraw(True) 
//is_cb_resource = false

dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.SetFocus()

Return True

end function

public function boolean wf_check_for_row_selected ();long				ll_Row, &
					ll_row_count

Char				lc_status_ind					

string			ls_checked_row, &
					ls_checkbox, &
					ls_row_name

//dwItemStatus	ldwis_status			
//u_string_functions u_string



IF dw_satellite_cattle.AcceptText() = -1 Then 
	Return False
End if
//If Ib_whatif = True Then
// 	SetMicroHelp("You can not Update after running what if scenarios ... Please Reinquire first")
//	Return False
//End If
//

//This.SetRedraw(False)

ll_row_count = dw_satellite_cattle.RowCount()
ls_checked_row = 'N'
ll_row = 1

do while ll_row < ll_row_count and ls_checked_row = 'N'
	ls_row_name = dw_satellite_cattle.GetItemString(ll_row,"row_name_id")
	if ls_row_name = 'AA' or ls_row_name = 'EA' or ls_row_name = 'AB' or ls_row_name = 'EB' then
		ls_checkbox = dw_satellite_cattle.GetItemString(ll_row,"select_cb")
		if ls_checkbox = 'Y' then
			ls_checked_row = 'Y'
		 end if
	end if
	ll_row ++
Loop
//MessageBox('Update String',is_update_string)
If ls_checked_row = 'N' Then
 	SetMicroHelp("You must select a row to Save Sources first")
	 ib_save_resource = false
	Return (False)
End If



//This.SetRedraw(True)

// I did this so the user wouldn't have to reinquire after the update

Return( True )

end function

public subroutine wf_calc_perpetual_inv (long al_row);String		ls_name, &
				ls_detail_text, &
				ls_name_full, &
				ls_new_amt, &
				ls_dtl_name_ind
Dec			ld_new_amt
long			ll_row, &
				ll_beg_inv, &
				ll_on_tram, &
				ll_downgrade_from, &
				ll_downgrade_to, &
				ll_cold_trans, &
				ll_cut_a_shift, &
				ll_cut_b_shift, &
				ll_perpetual_inv, &
				ll_b_shift_avail, &
				ll_trans_in, &
				ll_column_num, &
				ll_row_loop, &
				ll_count

Char				lc_status_ind					

string			ls_column, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name, ls_dtl_row_id


// I did this to make the code more genericls_name_full = dwo.name
//ls_name_full = dwo.name
//ls_name = Left(ls_name_full, Len(ls_name_full) - 2)

//  get values to calculate perpetual inventory	IF first occurrance, use beg inv, else use actual and loop
				
//	ls_dtl_name_ind = dw_satellite_cattle.GetItemnumber(ll_row, ls_column)
   ll_row_loop = al_row
	ll_column_num = 1
	ls_describe = "col_" + String(ll_column_num) + ".Visible"
	Do While dw_satellite_cattle.Describe(ls_describe) > '0'
		ls_column_name = "col_" + String(ll_column_num)
		//If dw_satellite_cattle.GetItemStatus(ll_row,ls_column_name, Primary!) = DataModified! Then
			ll_row_loop = al_row - 1
			ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
			ll_count = 0
			Do While (ll_count < 11 and ll_row_loop > 0)
				if ls_dtl_row_id = 'AB' or ls_dtl_row_id = 'EB' then
				   ll_cut_b_shift = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'AA' or ls_dtl_row_id = 'EA' then
				   ll_cut_a_shift = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				
				if ls_dtl_row_id = 'DF' then
					ll_downgrade_from = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'DT' then
					ll_downgrade_to = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'AT' or ls_dtl_row_id = 'ET' then
					ll_trans_in = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'TM' then
					ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
				end if
				if ls_dtl_row_id = 'PI' then
					ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
					ll_count = 11
				end if
				if ls_dtl_row_id = 'BI' or ls_dtl_row_id = 'AI' then
					if ll_row_loop = 1 then
						ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row_loop, ls_column_name)
					end if
				end if
				
//				ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row, ls_column_name)
//				ll_row = al_row - 8
//				ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row, ls_column_name)
				ll_row_loop = ll_row_loop - 1
				if ll_row_loop > 0 then
					ls_dtl_row_id = dw_satellite_cattle.GetItemString(ll_row_loop,"row_name_id")
					ll_count = ll_count + 1
				end if
			Loop
		ll_perpetual_inv = ( ll_beg_inv + ll_on_tram + ll_trans_in - ll_downgrade_from + ll_downgrade_to - ll_cut_a_shift - ll_cut_b_shift )
		dw_satellite_cattle.SetItem(al_row, ls_column_name, ll_perpetual_inv)
	//	ll_perpetual_inv = dw_satellite_cattle.GetItemNumber(ll_row, ls_column_name)
		//End If
		ll_column_num ++
		ls_describe = "col_" + String(ll_column_num) + ".Visible"
	//	ll_row ++
	Loop

end subroutine

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num, &
					ll_row_count, &
					ll_pos, &
					ll_current_row, &
					ll_700th_row, &
					ll_selected_row

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name, &
					ls_row_name, &
					ls_process_date, &
					ls_select_cb, &
					ls_rpc_detail
integer			li_Counter


u_string_functions	lu_string_functions				
//u_project_functions	lu_project_functions

dwItemStatus	ldwis_status



IF dw_satellite_cattle.AcceptText() = -1 Then 
	Return False
End if
//If Ib_whatif = True Then
// 	SetMicroHelp("You can not Update after running what if scenarios ... Please Reinquire first")
//	Return False
//End If
//
ls_header = ''
ls_header += dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_transfer_date.uf_get_transfer_date(), 'yyyy-mm-dd') + '~t'

ll_row_count = dw_satellite_cattle.RowCount()
ll_modrows = dw_satellite_cattle.ModifiedCount()

//IF ll_modrows <= 0 Then 
//	SetMicroHelp('No Update Necessary')
//	Return False
//Else
 	SetMicroHelp("Wait... Updating Database")
	SetPointer(HourGlass!)
//End if

This.SetRedraw(False)


is_update_string = ""
ll_row = 1
is_process_date = ' '
dw_satellite_cattle.SelectRow(0,False)
ls_row_name = dw_satellite_cattle.GetItemString(ll_row,"row_name_id")
do while ll_row < ll_row_count
	if ls_row_name = 'BI' then
		ls_process_date = dw_satellite_cattle.GetItemString(ll_row, "process_date")	
		if ls_process_date = '          ' then
		else
			is_process_date = ls_process_date
		 end if
	else
		if ls_row_name = 'AI' then
			ls_process_date = dw_satellite_cattle.GetItemString(ll_row, "process_date")	
			if ls_process_date = '          '  then 
			else
				is_process_date = ls_process_date
		 	end if
		else
			if ls_row_name = 'PI' then
				ls_process_date = dw_satellite_cattle.GetItemString(ll_row, "process_date")	
				if ls_process_date = '          ' then 
				else
					is_process_date = ls_process_date
				 end if
			end if
		end if
	end if
	if ls_row_name = 'AA' or ls_row_name = 'EA' or ls_row_name = 'AB' or ls_row_name = 'EB' then
		ll_column_num = 1
		ls_describe = "col_" + String(ll_column_num) + ".Visible"
		Do While dw_satellite_cattle.Describe(ls_describe) > '0'
			ls_column_name = "col_" + String(ll_column_num)
			If dw_satellite_cattle.GetItemStatus(ll_row,ls_column_name, Primary!) = DataModified! Then
				wf_update_modify(ll_row,ls_column_name) 
			else
				ls_select_cb =  dw_satellite_cattle.GetItemString(ll_row, "select_cb")
				if ls_select_cb = 'Y'then
					wf_update_modify(ll_row,ls_column_name) 
				end if
			End If
			ll_column_num ++
			ls_describe = "col_" + String(ll_column_num) + ".Visible"
		Loop
	end if
	ll_row ++
	ls_row_name = dw_satellite_cattle.GetItemString(ll_row,"row_name_id")
Loop
//MessageBox('Update String',is_update_string)

Do
	ll_700th_row = lu_string_functions.nf_nPos( is_update_string, "~r~n",1,700)
	IF ll_700th_Row > 0 Then
			ls_RPC_Detail = Left( is_update_string, ll_700th_Row + 1)
			is_update_string = Mid( is_update_string, ll_700th_Row + 2)
		ELSE
			ls_Rpc_detail = is_update_string
			is_update_string = ''
		END IF
		
	/*IF Not iu_rmt001.uf_rmtr21mr_upd_car_tracking_view(istr_error_info, ls_header, &
											ls_rpc_detail) THEN 
		This.SetRedraw(True)
		Return False
	End If
	*/
	IF Not iu_ws_pas4.NF_RMTR21NR(istr_error_info, ls_header, &
											ls_rpc_detail) THEN 
		This.SetRedraw(True)
		Return False
	End If
		
	Loop While Not lu_string_functions.nf_IsEmpty( is_update_string)	
	

//IF Not iu_rmt001.uf_rmtr21mr_upd_car_tracking_view(istr_error_info, ls_header, &
//											is_update_string) THEN 
//	This.SetRedraw(True)
//	Return False
//End If


SetMicroHelp("Modification Successful")
dw_satellite_cattle.ResetUpdate()
This.SetRedraw(True)

// I did this so the user wouldn't have to reinquire after the update
//ib_update_inquire = True
//wf_retrieve()

Return( True )

end function

on w_carcass_tracking_view.create
int iCurrent
call super::create
this.cb_save_sources=create cb_save_sources
this.cb_retrieve_sources=create cb_retrieve_sources
this.rb_deselect=create rb_deselect
this.rb_select=create rb_select
this.dw_transfer_date=create dw_transfer_date
this.dw_satellite_cattle=create dw_satellite_cattle
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_save_sources
this.Control[iCurrent+2]=this.cb_retrieve_sources
this.Control[iCurrent+3]=this.rb_deselect
this.Control[iCurrent+4]=this.rb_select
this.Control[iCurrent+5]=this.dw_transfer_date
this.Control[iCurrent+6]=this.dw_satellite_cattle
this.Control[iCurrent+7]=this.dw_plant
end on

on w_carcass_tracking_view.destroy
call super::destroy
destroy(this.cb_save_sources)
destroy(this.cb_retrieve_sources)
destroy(this.rb_deselect)
destroy(this.rb_select)
destroy(this.dw_transfer_date)
destroy(this.dw_satellite_cattle)
destroy(this.dw_plant)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;String 					ls_temp


ls_temp = Message.StringParm	

This.Title = 'Carcass Tracking View'

dw_transfer_date.uf_enable(false)
dw_plant.disable()
dw_transfer_date.uf_set_text('Transfer Date:')


end event

event ue_get_data(string as_value);call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'transfer_date'
		message.StringParm = String(dw_transfer_date.uf_get_transfer_date(), 'YYYY-MM-dd')
	Case 'date_object_text'
		message.StringParm = 'Transfer Date:' + '~t' + 'PA' + '~t'
	Case 'data'
//		message.StringParm = is_data 
End choose

end event

event ue_postopen;call super::ue_postopen;Environment	le_env
u_sdkcalls				lu_sdk
integer li_rtn

//This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "CARVIEW"
istr_error_info.se_user_id 		= sqlca.userid

// get the users color setup
lu_sdk = Create u_sdkcalls
GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
	il_SelectedColor = lu_sdk.nf_GetSysColor(13)
	il_SelectedTextColor = lu_sdk.nf_GetSysColor(14)
Else
 	il_SelectedColor = 255
	il_SelectedTextColor = 0
End if

// open inquire window
is_inquire_window_name = 'w_carcass_tracking_view_inq'

iu_rmt001 = Create u_rmt001
iu_ws_pas4	= Create u_ws_pas4

wf_retrieve()

dw_satellite_cattle.ResetUpdate()


end event

event ue_set_data(string as_data_item, string as_value);call super::ue_set_data;
Choose Case as_data_item
	Case 'whatif'
		wf_whatif()
	Case 'whatifinquire'
		wf_whatif_inquire()
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'transfer_date'
		dw_transfer_date.uf_set_transfer_date( Date(as_value ))
	Case 'reset_data'
		dw_satellite_cattle.SetRedraw(False)
		dw_satellite_cattle.reset()
		//wf_reset_datawindow()
		wf_populate_column_headings()
		dw_satellite_cattle.ImportString(is_detail)
		wf_set_initial_values()
		dw_satellite_cattle.SetRedraw(True)
		dw_satellite_cattle.ResetUpdate()
End Choose


end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288

integer li_x		= 100
integer li_y		= 300

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

  
//dw_satellite_cattle.width	= newwidth - (30 + li_x)
//dw_satellite_cattle.height	= newheight - (30 + li_y)

dw_satellite_cattle.width	= newwidth - (li_x)
dw_satellite_cattle.height	= newheight - (li_y)


// Must do because of the split horizonal bar 
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '1140'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '1140'


end event

type cb_save_sources from commandbutton within w_carcass_tracking_view
integer x = 2226
integer y = 124
integer width = 421
integer height = 92
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Save Sources"
end type

event clicked;ib_save_resource = true
wf_check_for_row_selected()
is_cb_resource = 'U'
if ib_save_resource = false then
	return 1
end if
//ib_retreive_resource = true
wf_update()
end event

type cb_retrieve_sources from commandbutton within w_carcass_tracking_view
integer x = 2226
integer width = 421
integer height = 92
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Retrieve Sources"
end type

event clicked;//is_cb_resource = 'R'
ib_retreive_resource = true
ib_save_resource = false
//wf_retrieve()
//is_cb_resource = ' '


//ib_save_resource = true
wf_check_for_row_selected()
is_cb_resource = 'R'
//if ib_save_resource = false then
//	return 1
//end if
//ib_retreive_resource = true
wf_retrieve_resources()
end event

type rb_deselect from radiobutton within w_carcass_tracking_view
integer x = 2679
integer y = 96
integer width = 343
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Deselect All"
boolean checked = true
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_satellite_cattle.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_satellite_cattle.SetItem(li_Counter, 'select_cb', 'N')
Next	
end event

type rb_select from radiobutton within w_carcass_tracking_view
integer x = 2679
integer width = 343
integer height = 84
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Select All"
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_satellite_cattle.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_satellite_cattle.SetItem(li_Counter, 'select_cb', 'Y')
Next	
end event

type dw_transfer_date from u_transfer_date within w_carcass_tracking_view
integer x = 1531
integer width = 608
integer taborder = 10
end type

type dw_satellite_cattle from u_base_dw_ext within w_carcass_tracking_view
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
event ue_calc_perpetual_inv pbm_dwnitemchange
event ue_calc_new_beg_inv pbm_dwnitemchange
integer x = 41
integer y = 224
integer width = 3058
integer height = 1212
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_carcass_tracking_view"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event ue_post_itemchanged(long al_row, dwobject adwo_dwo);String				ls_status, &
						ls_new


This.SetItemStatus(al_row, 0, Primary!, NOTMODIFIED!)


end event

event ue_calc_perpetual_inv;String		ls_name, &
				ls_detail_text, &
				ls_name_full, &
				ls_new_amt
Dec			ld_new_amt
long			ll_row, &
				ll_beg_inv, &
				ll_on_tram, &
				ll_downgrade_from, &
				ll_downgrade_to, &
				ll_cold_trans, &
				ll_cut_a_shift, &
				ll_cut_b_shift, &
				ll_perpetual_inv, &
				ll_b_shift_avail, &
				ll_trans_in


// I did this to make the code more genericls_name_full = dwo.name
ls_name_full = dwo.name
ls_name = Left(ls_name_full, Len(ls_name_full) - 2)

//  get values to calculate perpetual inventory	IF first occurrance, use beg inv, else use actual and loop
			if row = 16 then
				ll_row = 1
				ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
				ll_row = 2
				ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
				ll_row = 4
				ll_downgrade_from = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
				ll_row =5
				ll_downgrade_to = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
				ll_row = 10
				ll_cut_b_shift = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
				ll_perpetual_inv = ( ll_beg_inv + ll_on_tram + ll_trans_in - ll_downgrade_from + ll_downgrade_to - ll_cut_a_shift - ll_cut_b_shift )
				ll_row = row - 5
				dw_satellite_cattle.SetItem(ll_row, ls_name_full, ll_perpetual_inv)
			end if	
	if row > 16 then 
		//loop
	end if
	
end event

event itemchanged;call super::itemchanged;String		ls_name, &
				ls_detail_text, &
				ls_name_full, &
				ls_new_amt, &
				ls_status
Dec			ld_new_amt
long			ll_row, &
				ll_beg_inv, &
				ll_on_tram, &
				ll_downgrade_from, &
				ll_downgrade_to, &
				ll_cold_trans, &
				ll_cut_a_shift, &
				ll_cut_b_shift, &
				ll_perpetual_inv, &
				ll_b_shift_avail, &
				ll_trans_in


// I did this to make the code more genericls_name_full = dwo.name
ls_name_full = dwo.name
ls_name = Left(ls_name_full, Len(ls_name_full) - 2)

CHOOSE CASE ls_name
	Case 'col'
		IF Not IsNumber(data) Then
			iw_frame.SetMicroHelp("All Columns must be numeric")
			This.SelectText(1, 100)
			Return 1
		End IF	
//		IF Dec(data) < 0 Then
//			iw_frame.SetMicroHelp("No Cattle Characteristic's Column can be negative")
//			This.SelectText(1, 100)
//			Return 1
//		End IF	
End Choose
	ls_detail_text = dw_satellite_cattle.GetItemString(row,"row_name_id")
 	//ls_detail_text = dw_satellite_cattle.GetItemString(row,"detail_text_1")
//	CASE 1, 5, 7, 9, 10
//	if ls_detail_text =  'Act Beginning Inventory  "  then 
//		if row > 1 then
//			This.Event ue_calc_new_beg_inv(row, dwo, data)	
//		end if
//	end if
if ls_name_full = 'select_cb' then
	Choose case This.getItemStatus(row, 0, Primary!)
		Case DataModified!
			ls_status = 'MODI'
		Case NewModified!
			ls_status = 'NEWM'
		Case NotModified!
			ls_status = 'NOTM'
		Case New!
			ls_status = 'NEW '
	End Choose
	if ls_status = 'NOTM' then This.event Post ue_post_itemchanged(row,dwo)
else
	if ls_detail_text = 'AA' or ls_detail_text = 'EA' then
		wf_calc_perpetual_inv_down(row, ls_name_full, data)
		wf_calc_b_shift_avail_change(row, ls_name_full, data)
	end if
	
	if ls_detail_text = 'AB' or ls_detail_text = 'EB' then 
		wf_calc_perpetual_inv_down(row, ls_name_full, data)
//		row = row + 1
		wf_calc_b_shift_avail_change(row, ls_name_full, data)
	end if
end if
	
//  get values to calculate perpetual inventory	IF first occurrance, use beg inv, else use actual and loop
//			if row = 16 then
	//			This.Event ue_calc_perpetual_inv(row, dwo, data)	
//				PostEvent('ue_calc_perpetual_inv')
//				ll_row = 1
//				ll_beg_inv = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_row = 2
//				ll_on_tram = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_row = 4
//				ll_downgrade_from = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_row =5
//				ll_downgrade_to = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_row = 10
//				ll_cut_b_shift = dw_satellite_cattle.GetItemnumber(ll_row, ls_name_full)
//				ll_perpetual_inv = ( ll_beg_inv + ll_on_tram + ll_trans_in - ll_downgrade_from + ll_downgrade_to - ll_cut_a_shift - ll_cut_b_shift )
//				ll_row = row - 4
//				dw_satellite_cattle.SetItem(ll_row, ls_name_full, ll_perpetual_inv)
//			end if	
//	end if
	
	
end event

event constructor;call super::constructor;iw_parentwindow = Parent


end event

event itemerror;call super::itemerror;Return 2
end event

event rbuttondown;call super::rbuttondown;Window	lw_temp
String		ls_detail_row_id, &
			ls_char_name

	ls_detail_row_id = dw_satellite_cattle.GetItemString(row,"row_name_id")
	if ls_detail_row_id = 'DF' or ls_detail_row_id = 'DT' then
		//do nothing
	else
		SetMicroHelp("You must be on a downgrade from/to field to inquire on downgrades")
		return 1
	end if
	
	string name 
	name = string(dwo.name)
	if(name = 'datawindow') then
		SetMicroHelp("You clicked between fields. Please right click in a field.")
		return row
	end if
	
	il_row = row
	is_col = name
	
	SetMicroHelp("")
	string s 
	ls_char_name = dw_satellite_cattle.describe(dwo.name + '.tag')

wf_call_downgrade_window(row, ls_char_name)
//wf_call_downgrade_window(row, dw_satellite_cattle.object.col_1_t.text)
//wf_call_downgrade_window(row, dwo.name)
//	OpenSheetWithParm (lw_temp,This,"w_carcass_downgrade",iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)

//return
end event

type dw_plant from u_plant within w_carcass_tracking_view
integer height = 92
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()

end event

