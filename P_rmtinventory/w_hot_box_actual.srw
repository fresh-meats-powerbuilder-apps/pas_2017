HA$PBExportHeader$w_hot_box_actual.srw
forward
global type w_hot_box_actual from w_base_sheet_ext
end type
type dw_hot_box from u_base_dw_ext within w_hot_box_actual
end type
type dw_plant from u_plant within w_hot_box_actual
end type
type dw_wgt_1 from u_begin_end_weights within w_hot_box_actual
end type
type dw_wgt_2 from u_begin_end_weights within w_hot_box_actual
end type
type dw_wgt_3 from u_begin_end_weights within w_hot_box_actual
end type
type dw_wgt_4 from u_begin_end_weights within w_hot_box_actual
end type
type dw_wgt_5 from u_begin_end_weights within w_hot_box_actual
end type
type dw_wgt_6 from u_begin_end_weights within w_hot_box_actual
end type
type dw_wgt_7 from u_begin_end_weights within w_hot_box_actual
end type
type st_1 from statictext within w_hot_box_actual
end type
type dw_dates from u_begin_end_dates within w_hot_box_actual
end type
end forward

global type w_hot_box_actual from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 2959
integer height = 1523
long backcolor = 67108864
event ue_keydown pbm_dwnkey
dw_hot_box dw_hot_box
dw_plant dw_plant
dw_wgt_1 dw_wgt_1
dw_wgt_2 dw_wgt_2
dw_wgt_3 dw_wgt_3
dw_wgt_4 dw_wgt_4
dw_wgt_5 dw_wgt_5
dw_wgt_6 dw_wgt_6
dw_wgt_7 dw_wgt_7
st_1 st_1
dw_dates dw_dates
end type
global w_hot_box_actual w_hot_box_actual

type variables
s_error			istr_error_info

datastore			ids_tree,&
			ids_desc

u_rmt001			iu_rmt001
u_pas203			iu_pas203
u_ws_pas4		iu_ws_pas4

Boolean			ib_updating,&
			ib_char_key

Long			il_char_count, &
			il_rec_count, &
			il_SelectedColor, &
			il_SelectedTextColor 



String			is_update_string,&  
			is_desc
Window			iw_temp
end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_wgt_ranges ()
public function boolean wf_update ()
public function integer wf_validate_wgts (string as_wgts)
public function boolean wf_get_wgt_range (ref string as_wgts)
end prototypes

public function boolean wf_retrieve ();Long					ll_rec_count_pas,ll_num
				
String				ls_plant, &
						ls_plant_desc, & 
						ls_header, &
						ls_detail, &
						ls_temp
u_string_functions	lu_string
//This.TriggerEvent('closequery') 
// windows may have been modified
IF dw_wgt_1.AcceptText() = -1 or &
	dw_wgt_2.AcceptText() = -1 or &
	dw_wgt_3.AcceptText() = -1 or &
	dw_wgt_4.AcceptText() = -1 or &
	dw_wgt_5.AcceptText() = -1 or &
	dw_wgt_6.AcceptText() = -1 or &
	dw_wgt_7.AcceptText() = -1 THEN Return( False )

If wf_get_wgt_range(ls_temp) = False Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then Return FAlse

ls_header  = ''
ls_header  = 	dw_plant.uf_get_plant_code() + '~t'
ls_header += 	String(dw_dates.uf_get_begin_date(),'yyyy-mm-dd') + '~t'
ls_header += 	String(dw_dates.uf_get_end_date(),'yyyy-mm-dd') + '~t'
ls_header += ls_temp + '~r~n' 

//messagebox('check function',ls_header) 

iw_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"

dw_hot_box.reset()

/*If iu_rmt001.uf_rmtr25mr_inq_hot_box_actual(istr_error_info, & 
										ls_header, &
										ls_detail) = -1 Then
										This.SetRedraw(True) 
										Return False
End If			
*/
If iu_ws_pas4.NF_RMTR25NR(istr_error_info, & 
										ls_header, &
										ls_detail) = -1 Then
										This.SetRedraw(True) 
										Return False
End If			

//ls_header = '2000~t'
//dw_hot_box.Object.beg_inv_1st_row_co.Expression=ls_header
//ls_detail = &
//'1999-05-10~t2000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-11~t3000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-12~t4000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-13~t5000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-14~t6000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-15~t7000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-16~t8000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-17~t9000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-18~t10000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-19~t11000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-20~t12000~t16~t410~t6560~t16~t410~t6400~r~n' + &
//'1999-05-21~t13000~t16~t410~t6560~t16~t410~t6400~r~n' 
//

//MessageBox("ls_detail",ls_detail)
FOR ll_num = 1 TO 7
	ls_temp = lu_string.nf_gettoken(ls_header,'~t')
	CHOOSE CASE ll_num
		CASE 1
			dw_hot_box.object.co_head_avg_1.expression = ls_temp
		CASE 2
			dw_hot_box.object.co_head_avg_2.expression = ls_temp
		CASE 3
			dw_hot_box.object.co_head_avg_3.expression = ls_temp
		CASE 4
			dw_hot_box.object.co_head_avg_4.expression = ls_temp
		CASE 5
			dw_hot_box.object.co_head_avg_5.expression = ls_temp
		CASE 6
			dw_hot_box.object.co_head_avg_6.expression = ls_temp
		CASE 7
			dw_hot_box.object.co_head_avg_7.expression = ls_temp
	END CHOOSE
NEXT

ll_rec_count_pas = dw_hot_box.ImportString(ls_detail)

//dw_hot_box.SetItem(1, "back_color", il_SelectedColor)
//dw_hot_box.SetItem(1, "text_color", il_SelectedTextColor)
//
If ll_rec_count_pas > 0 Then 
	SetMicroHelp(String(ll_rec_count_pas) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True) 

dw_hot_box.ResetUpdate()
dw_hot_box.SetFocus()


Return True

end function

public function integer wf_wgt_ranges ();Long 		ll_count , &
			ll_max = 7, &
			ll_temp, &
			ll_low_hold, &
			ll_high_hold,&
			ll_error
String	ls_registry_string,ls_installed,ls_temp	
Int		li_null,li_rtn

Boolean	lb_error,lb_one_valid,lb_empty,lb_invalid,lb_token

//Datawindow	ldw_temp
//u_begin_end_weights luo_temp
u_string_functions  lu_strings

SetPointer(HourGlass!)
ls_installed = ""
DO WHILE ll_count < ll_max
	ll_count ++
	CHOOSE CASE ll_count
		CASE 1
			ll_temp = dw_wgt_1.uf_get_begin_wgt()
			If lu_strings.nf_amiempty(String(ll_temp)) Then
				ls_Installed += '~t'
				ll_temp = dw_wgt_1.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					lb_error = True
					lb_invalid = True
					dw_wgt_1.SelectText(1,100)
					dw_wgt_1.SetFocus( )
					Exit
				End IF
			Else
				ll_low_hold = ll_temp
				ls_Installed += String(ll_temp) + '~t' 
				ll_temp = dw_wgt_1.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					ll_high_hold = ll_temp
					IF ll_low_hold > ll_high_hold Then
						dw_wgt_1.SelectText(1,100)
						dw_wgt_1.SetFocus( )
						lb_error = True
						lb_one_valid = True
						Exit
					Else
						ls_Installed += String(ll_temp) + '~t' 
					End if
				End If
			End If
		CASE 2
			ll_temp = dw_wgt_2.uf_get_begin_wgt()
			If lu_strings.nf_amiempty(String(ll_temp)) Then
				ls_Installed += '~t'
				ll_temp = dw_wgt_2.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					lb_error = True
					lb_invalid = True
					dw_wgt_2.SelectText(1,100)
					dw_wgt_2.SetFocus( )
					Exit
				End IF
			Else
				ll_low_hold = ll_temp
				ls_Installed += String(ll_temp) + '~t' 
				ll_temp = dw_wgt_2.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					ll_high_hold = ll_temp
					IF ll_low_hold > ll_high_hold Then
						dw_wgt_2.SelectText(1,100)
						dw_wgt_2.SetFocus( )
						lb_error = True
						Exit
					Else
						ls_Installed += String(ll_temp) + '~t' 
						lb_one_valid = True
					End if
				End If
			End If
		CASE 3
			ll_temp = dw_wgt_3.uf_get_begin_wgt()
			If lu_strings.nf_amiempty(String(ll_temp)) Then
				ls_Installed += '~t'
				ll_temp = dw_wgt_3.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					lb_error = True
					lb_invalid = True
					dw_wgt_3.SelectText(1,100)
					dw_wgt_3.SetFocus( )
					Exit
				End IF
			Else
				ll_low_hold = ll_temp
				ls_Installed += String(ll_temp) + '~t' 
				ll_temp = dw_wgt_3.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					ll_high_hold = ll_temp
					IF ll_low_hold > ll_high_hold Then
						dw_wgt_3.SelectText(1,100)
						dw_wgt_3.SetFocus( )
						lb_error = True
						Exit
					Else
						ls_Installed += String(ll_temp) + '~t' 
						lb_one_valid = True
					End if
				End If
			End If
		CASE 4
			ll_temp = dw_wgt_4.uf_get_begin_wgt()
			If lu_strings.nf_amiempty(String(ll_temp)) Then
				ls_Installed += '~t'
				ll_temp = dw_wgt_4.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					lb_error = True
					lb_invalid = True
					dw_wgt_4.SelectText(1,100)
					dw_wgt_4.SetFocus( )
					Exit
				End IF
			Else
				ll_low_hold = ll_temp
				ls_Installed += String(ll_temp) + '~t' 
				ll_temp = dw_wgt_4.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					ll_high_hold = ll_temp
					IF ll_low_hold > ll_high_hold Then
						dw_wgt_4.SelectText(1,100)
						dw_wgt_4.SetFocus( )
						lb_error = True
						Exit
					Else
						ls_Installed += String(ll_temp) + '~t' 
						lb_one_valid = True
					End if
				End If
			End If
		CASE 5
			ll_temp = dw_wgt_5.uf_get_begin_wgt()
			If lu_strings.nf_amiempty(String(ll_temp)) Then
				ls_Installed += '~t'
				ll_temp = dw_wgt_5.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					lb_error = True
					lb_invalid = True
					dw_wgt_5.SelectText(1,100)
					dw_wgt_5.SetFocus( )
					Exit
				End IF
			Else
				ll_low_hold = ll_temp
				ls_Installed += String(ll_temp) + '~t' 
				ll_temp = dw_wgt_5.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					ll_high_hold = ll_temp
					IF ll_low_hold > ll_high_hold Then
						dw_wgt_5.SelectText(1,100)
						dw_wgt_5.SetFocus( )
						lb_error = True
						Exit
					Else
						ls_Installed += String(ll_temp) + '~t' 
						lb_one_valid = True
					End if
				End If
			End If
		CASE 6
			ll_temp = dw_wgt_6.uf_get_begin_wgt()
			If lu_strings.nf_amiempty(String(ll_temp)) Then
				ls_Installed += '~t'
				ll_temp = dw_wgt_6.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					lb_error = True
					lb_invalid = True
					dw_wgt_6.SelectText(1,100)
					dw_wgt_6.SetFocus( )
					Exit
				End IF
			Else
				ll_low_hold = ll_temp
				ls_Installed += String(ll_temp) + '~t' 
				ll_temp = dw_wgt_6.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					ll_high_hold = ll_temp
					IF ll_low_hold > ll_high_hold Then
						dw_wgt_6.SelectText(1,100)
						dw_wgt_6.SetFocus( )
						lb_error = True
						Exit
					Else
						ls_Installed += String(ll_temp) + '~t' 
						lb_one_valid = True
					End if
				End If
			End If
		CASE 7
			ll_temp = dw_wgt_7.uf_get_begin_wgt()
			If lu_strings.nf_amiempty(String(ll_temp)) Then
				ls_Installed += '~t'
				ll_temp = dw_wgt_7.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					lb_error = True
					lb_invalid = True
					dw_wgt_7.SelectText(1,100)
					dw_wgt_7.SetFocus( )
					Exit
				End IF
			Else
				ll_low_hold = ll_temp
				ls_Installed += String(ll_temp) + '~t' 
				ll_temp = dw_wgt_7.uf_get_end_wgt()
				If lu_strings.nf_amiempty(String(ll_temp)) Then
					ls_installed += '~t'
				Else
					ll_high_hold = ll_temp
					IF ll_low_hold > ll_high_hold Then
						dw_wgt_7.SelectText(1,100)
						dw_wgt_7.SetFocus( )
						lb_error = True
						Exit
					Else
						ls_Installed += String(ll_temp) + '~t' 
						lb_one_valid = True
					End if
				End If
			End If
	End Choose
Loop


IF lb_error Then
	IF lb_one_valid  THEN
		iw_frame.SetMicroHelp("The Begin Weight can not be greater than the End Weight")
	ELSEIF lb_invalid THEN
			iw_frame.SetMicroHelp("Both the Begin Weight and the End Weight must have weights in them")
		Else		
			ll_temp = messagebox('Weight Ranges','You must have at least one From and To Weight Range For this Window. ~r~n Press Yes to return to the defaults or No to Edit the weights yourself.',Question!, YesNo! )
//			set defaults else return
			If ll_temp = 1 Then
				ls_installed = ""
				ls_Registry_String = "HKEY_CURRENT_USER\SOFTWARE\IBP\RAW MATERIAL TRACKING"
				li_rtn = RegistryGet(ls_Registry_String,"WGTRANGES",ls_Installed)
				If li_rtn = 1 Then
					ls_temp = lu_strings.nf_gettoken(ls_Installed,'~t')
					ll_count = 1
					DO UNTIL ll_count > 14
						If IsNumber(ls_temp) and Not lb_token Then
							li_rtn = Integer (ls_temp)
							CHOOSE CASE ll_count
								CASE 1 
									dw_wgt_1.uf_set_begin_wgt(li_rtn)	
								CASE 2 
									dw_wgt_1.uf_set_end_wgt(li_rtn)	
								CASE 3 
									dw_wgt_2.uf_set_begin_wgt(li_rtn)	
								Case 4	
									dw_wgt_2.uf_set_end_wgt(li_rtn)	
								CASE 5 
									dw_wgt_3.uf_set_begin_wgt(li_rtn)	
								Case 6
									dw_wgt_3.uf_set_end_wgt(li_rtn)	
								Case 7		
									dw_wgt_4.uf_set_begin_wgt(li_rtn)	
								Case 8
									dw_wgt_4.uf_set_end_wgt(li_rtn)	
								CASE 9 
									dw_wgt_5.uf_set_begin_wgt(li_rtn)	
								Case 10
									dw_wgt_5.uf_set_end_wgt(li_rtn)	
								Case 11		
									dw_wgt_6.uf_set_begin_wgt(li_rtn)	
								Case 12
									dw_wgt_6.uf_set_end_wgt(li_rtn)	
								Case 13		
									dw_wgt_7.uf_set_begin_wgt(li_rtn)	
								Case 14
									dw_wgt_7.uf_set_end_wgt(li_rtn)	
							END CHOOSE
						else	
							CHOOSE CASE ll_count
								CASE 1 
									dw_wgt_1.uf_set_begin_wgt(li_null)	
								CASE 2 
									dw_wgt_1.uf_set_end_wgt(li_null)	
								CASE 3 
									dw_wgt_2.uf_set_begin_wgt(li_null)	
								Case 4	
									dw_wgt_2.uf_set_end_wgt(li_null)	
								CASE 5 
									dw_wgt_3.uf_set_begin_wgt(li_null)	
								Case 6
									dw_wgt_3.uf_set_end_wgt(li_null)	
								Case 7		
									dw_wgt_4.uf_set_begin_wgt(li_null)	
								Case 8
									dw_wgt_4.uf_set_end_wgt(li_null)	
								CASE 9 
									dw_wgt_5.uf_set_begin_wgt(li_null)	
								Case 10
									dw_wgt_5.uf_set_end_wgt(li_null)	
								Case 11		
									dw_wgt_6.uf_set_begin_wgt(li_null)	
								Case 12
									dw_wgt_6.uf_set_end_wgt(li_null)	
								Case 13		
									dw_wgt_7.uf_set_begin_wgt(li_null)	
								Case 14
									dw_wgt_7.uf_set_end_wgt(li_null)	
							END CHOOSE
						End If
						ls_temp = lu_strings.nf_gettoken(ls_Installed,'~t')
						If Not IsNumber(ls_temp) Then
							lb_token = True
						End If	
						ll_count ++
					LOOP
					dw_wgt_1.ResetUpdate()
					dw_wgt_2.ResetUpdate()
					dw_wgt_3.ResetUpdate()
					dw_wgt_4.ResetUpdate()
					dw_wgt_5.ResetUpdate()
					dw_wgt_6.ResetUpdate()
					dw_wgt_7.ResetUpdate()
				Else
					// should never happen			
					messagebox('Registry Settings','Registry retrieval has failed please contact Applications Programming there is a severe problem.',StopSign!)
					Return 1
				End If
			Else
				iw_frame.SetMicroHelp("Please update your Weight Ranges")
				Return 1
			End If
		End If
		
//END CHOOSE
Else
	ls_Registry_String = "HKEY_CURRENT_USER\SOFTWARE\IBP\RAW MATERIAL TRACKING"
	li_rtn = RegistrySet(  ls_Registry_String,"WGTRANGES",ls_installed)
	If li_rtn <> 1 Then
		// should never happen			
		messagebox('Registry Settings','Registry update has failed please contact Applications Programming there is a severe problem.',StopSign!)
		Return 1
	Else
		dw_wgt_1.ResetUpdate()
		dw_wgt_2.ResetUpdate()
		dw_wgt_3.ResetUpdate()
		dw_wgt_4.ResetUpdate()
		dw_wgt_5.ResetUpdate()
		dw_wgt_6.ResetUpdate()
		dw_wgt_7.ResetUpdate()
	End If
End if		

Return 0

end function

public function boolean wf_update ();iw_frame.SetMicroHelp("Wait.. Updating Registry")

IF dw_wgt_1.AcceptText() = -1 or &
	dw_wgt_2.AcceptText() = -1 or &
	dw_wgt_3.AcceptText() = -1 or &
	dw_wgt_4.AcceptText() = -1 or &
	dw_wgt_5.AcceptText() = -1 or &
	dw_wgt_6.AcceptText() = -1 or &
	dw_wgt_7.AcceptText() = -1 THEN Return( False )

IF wf_wgt_ranges() = 1 Then Return False

iw_frame.SetMicroHelp("Update Successful")

Return True


end function

public function integer wf_validate_wgts (string as_wgts);Long 						ll_counter , &
							ll_max = 7, &
							ll_temp, &
							ll_low_hold, &
							ll_high_hold
String					ls_installed,ls_temp	
Boolean					lb_error,lb_one_valid
u_string_functions  	u_string

ls_installed = as_wgts
ls_temp = u_string.nf_gettoken(ls_Installed,'~t')
ll_counter = 1
DO UNTIL ll_counter > 14
	If IsNumber(ls_temp) Then 
		ll_temp= Integer (ls_temp)
		CHOOSE CASE ll_counter
			CASE 1 
				ll_low_hold = ll_temp 
			CASE 2 
				ll_high_hold = ll_temp 
				IF ll_low_hold > ll_high_hold Then
					iw_frame.SetMicroHelp("The Begin Weight can not be greater than the End Weight")
					dw_wgt_1.SelectText(1,100)
					dw_wgt_1.SetFocus( )
					Return 1 
					Exit
				End If
			CASE 3 
				ll_low_hold = ll_temp 
			Case 4	
				ll_high_hold = ll_temp 
				IF ll_low_hold > ll_high_hold Then
					iw_frame.SetMicroHelp("The Begin Weight can not be greater than the End Weight")
					dw_wgt_2.SelectText(1,100)
					dw_wgt_2.SetFocus( )
					Return 1 
					Exit
				End If
			CASE 5 
				ll_low_hold = ll_temp 
			Case 6
				ll_high_hold = ll_temp 
				IF ll_low_hold > ll_high_hold Then
					iw_frame.SetMicroHelp("The Begin Weight can not be greater than the End Weight")
					dw_wgt_3.SelectText(1,100)
					dw_wgt_3.SetFocus( )
					Return 1 
					Exit
				End If
			Case 7		
				ll_low_hold = ll_temp 
			Case 8
				ll_high_hold = ll_temp 
				IF ll_low_hold > ll_high_hold Then
					iw_frame.SetMicroHelp("The Begin Weight can not be greater than the End Weight")
					dw_wgt_4.SelectText(1,100)
					dw_wgt_4.SetFocus( )
					Return 1 
					Exit
				End If
			CASE 9 
				ll_low_hold = ll_temp 
			Case 10
				ll_high_hold = ll_temp 
				IF ll_low_hold > ll_high_hold Then
					iw_frame.SetMicroHelp("The Begin Weight can not be greater than the End Weight")
					dw_wgt_5.SelectText(1,100)
					dw_wgt_5.SetFocus( )
					Return 1 
					Exit
				End If
			Case 11		
				ll_low_hold = ll_temp 
			Case 12
				ll_high_hold = ll_temp 
				IF ll_low_hold > ll_high_hold Then
					iw_frame.SetMicroHelp("The Begin Weight can not be greater than the End Weight")
					dw_wgt_6.SelectText(1,100)
					dw_wgt_6.SetFocus( )
					Return 1 
					Exit
				End If
			Case 13		
				ll_low_hold = ll_temp 
			Case 14
				ll_high_hold = ll_temp 
				IF ll_low_hold > ll_high_hold Then
					iw_frame.SetMicroHelp("The Begin Weight can not be greater than the End Weight")
					dw_wgt_7.SelectText(1,100)
					dw_wgt_7.SetFocus( )
					Return 1 
					Exit
				End If
		END CHOOSE
	End If
	ls_temp = u_string.nf_gettoken(ls_Installed,'~t')
	ll_counter ++
LOOP

Return 0
end function

public function boolean wf_get_wgt_range (ref string as_wgts);// I did this to allow the users to query based on the way they have there wgts 
as_wgts  = ''
as_wgts	= String(dw_wgt_1.uf_get_begin_wgt()) + '~t'	
as_wgts += String(dw_wgt_1.uf_get_end_wgt()) + '~t'	
as_wgts += String(dw_wgt_2.uf_get_begin_wgt()) + '~t'	
as_wgts += String(dw_wgt_2.uf_get_end_wgt()) + '~t'	
as_wgts += String(dw_wgt_3.uf_get_begin_wgt()) + '~t'	
as_wgts += String(dw_wgt_3.uf_get_end_wgt()) + '~t'	
as_wgts += String(dw_wgt_4.uf_get_begin_wgt()) + '~t'	
as_wgts += String(dw_wgt_4.uf_get_end_wgt()) + '~t'	
as_wgts += String(dw_wgt_5.uf_get_begin_wgt()) + '~t'	
as_wgts += String(dw_wgt_5.uf_get_end_wgt()) + '~t'	
as_wgts += String(dw_wgt_6.uf_get_begin_wgt()) + '~t'	
as_wgts += String(dw_wgt_6.uf_get_end_wgt()) + '~t'	
as_wgts += String(dw_wgt_7.uf_get_begin_wgt()) + '~t'	
as_wgts += String(dw_wgt_7.uf_get_end_wgt()) 

// insure they are valid
If wf_validate_wgts(as_wgts) = 1 Then Return False

Return True
end function

on w_hot_box_actual.create
int iCurrent
call super::create
this.dw_hot_box=create dw_hot_box
this.dw_plant=create dw_plant
this.dw_wgt_1=create dw_wgt_1
this.dw_wgt_2=create dw_wgt_2
this.dw_wgt_3=create dw_wgt_3
this.dw_wgt_4=create dw_wgt_4
this.dw_wgt_5=create dw_wgt_5
this.dw_wgt_6=create dw_wgt_6
this.dw_wgt_7=create dw_wgt_7
this.st_1=create st_1
this.dw_dates=create dw_dates
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_hot_box
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_wgt_1
this.Control[iCurrent+4]=this.dw_wgt_2
this.Control[iCurrent+5]=this.dw_wgt_3
this.Control[iCurrent+6]=this.dw_wgt_4
this.Control[iCurrent+7]=this.dw_wgt_5
this.Control[iCurrent+8]=this.dw_wgt_6
this.Control[iCurrent+9]=this.dw_wgt_7
this.Control[iCurrent+10]=this.st_1
this.Control[iCurrent+11]=this.dw_dates
end on

on w_hot_box_actual.destroy
call super::destroy
destroy(this.dw_hot_box)
destroy(this.dw_plant)
destroy(this.dw_wgt_1)
destroy(this.dw_wgt_2)
destroy(this.dw_wgt_3)
destroy(this.dw_wgt_4)
destroy(this.dw_wgt_5)
destroy(this.dw_wgt_6)
destroy(this.dw_wgt_7)
destroy(this.st_1)
destroy(this.dw_dates)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
//iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')


end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;dw_wgt_1.uf_enable(True)	
dw_wgt_2.uf_enable(True)	
dw_wgt_3.uf_enable(True)	
dw_wgt_4.uf_enable(True)	
dw_wgt_5.uf_enable(True)	
dw_wgt_6.uf_enable(True)	
dw_wgt_7.uf_enable(True)		
dw_dates.uf_enable(False)	

This.Title = 'Hot Box Actual'


dw_dates.uf_set_begin_date(today())
dw_dates.uf_set_end_date(today())

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'begin_date'
		message.StringParm = String(dw_dates.uf_get_begin_date(),'mm-dd-yyyy')
	Case 'end_date'
		message.StringParm = String(dw_dates.uf_get_end_date(),'mm-dd-yyyy')
End choose

end event

event ue_postopen;call super::ue_postopen;Environment	le_env
u_sdkcalls				lu_sdk

String		ls_installed,ls_RegistryString,ls_temp
Int			li_rtn,li_null
Long 			ll_counter,ll_pos,ll_count
Boolean		lb_token
u_string_functions 	u_string


//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "hotbox"
istr_error_info.se_user_id 		= sqlca.userid

// get the users color setup
lu_sdk = Create u_sdkcalls
GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = lu_sdk.nf_GetSysColor(13)
	il_SelectedTextColor = lu_sdk.nf_GetSysColor(14)
Else
 	il_SelectedColor = 255
	il_SelectedTextColor = 0
End if
Destroy(lu_sdk)

is_inquire_window_name = 'w_hot_box_actual_inq'

iu_rmt001 = Create u_rmt001

// Check the Registry for user settings
ls_installed = ""
ls_RegistryString = "HKEY_CURRENT_USER\SOFTWARE\IBP\RAW MATERIAL TRACKING"
li_rtn = RegistryGet(ls_RegistryString,"WGTRANGES",ls_Installed)
// If nothing found use defaults
IF Len(Trim(ls_Installed)) < 1 Then
	dw_wgt_1.uf_set_begin_wgt(300)	
	dw_wgt_1.uf_set_end_wgt(525)	
	dw_wgt_2.uf_set_begin_wgt(530)	
	dw_wgt_2.uf_set_end_wgt(735)	
	dw_wgt_3.uf_set_begin_wgt(740)	
	dw_wgt_3.uf_set_end_wgt(950)	
	dw_wgt_4.uf_set_begin_wgt(955)	
	dw_wgt_4.uf_set_end_wgt(1500)	
	//Setnull(li_null)
	dw_wgt_5.uf_set_begin_wgt(0)	
	dw_wgt_5.uf_set_end_wgt(0)	
	dw_wgt_6.uf_set_begin_wgt(0)	
	dw_wgt_6.uf_set_end_wgt(0)	
	dw_wgt_7.uf_set_begin_wgt(0)	
	dw_wgt_7.uf_set_end_wgt(0)	
Else
	//strip the registry string down
	ls_temp = u_string.nf_gettoken(ls_Installed,'~t')
	ll_counter = 1
	DO UNTIL ll_counter > 14
		If IsNumber(ls_temp) Then//and Not lb_token Then
			li_rtn = Integer (ls_temp)
			CHOOSE CASE ll_counter
				CASE 1 
					dw_wgt_1.uf_set_begin_wgt(li_rtn)	
				CASE 2 
					dw_wgt_1.uf_set_end_wgt(li_rtn)	
				CASE 3 
					dw_wgt_2.uf_set_begin_wgt(li_rtn)	
				Case 4	
					dw_wgt_2.uf_set_end_wgt(li_rtn)	
				CASE 5 
					dw_wgt_3.uf_set_begin_wgt(li_rtn)	
				Case 6
					dw_wgt_3.uf_set_end_wgt(li_rtn)	
				Case 7		
					dw_wgt_4.uf_set_begin_wgt(li_rtn)	
				Case 8
					dw_wgt_4.uf_set_end_wgt(li_rtn)	
				CASE 9 
					dw_wgt_5.uf_set_begin_wgt(li_rtn)	
				Case 10
					dw_wgt_5.uf_set_end_wgt(li_rtn)	
				Case 11		
					dw_wgt_6.uf_set_begin_wgt(li_rtn)	
				Case 12
					dw_wgt_6.uf_set_end_wgt(li_rtn)	
				Case 13		
					dw_wgt_7.uf_set_begin_wgt(li_rtn)	
				Case 14
					dw_wgt_7.uf_set_end_wgt(li_rtn)	
			END CHOOSE
		else	
			CHOOSE CASE ll_counter
				CASE 1 
					dw_wgt_1.uf_set_begin_wgt(0)	
				CASE 2 
					dw_wgt_1.uf_set_end_wgt(0)	
				CASE 3 
					dw_wgt_2.uf_set_begin_wgt(0)	
				Case 4	
					dw_wgt_2.uf_set_end_wgt(0)	
				CASE 5 
					dw_wgt_3.uf_set_begin_wgt(0)	
				Case 6
					dw_wgt_3.uf_set_end_wgt(0)	
				Case 7		
					dw_wgt_4.uf_set_begin_wgt(0)	
				Case 8
					dw_wgt_4.uf_set_end_wgt(0)	
				CASE 9 
					dw_wgt_5.uf_set_begin_wgt(0)	
				Case 10
					dw_wgt_5.uf_set_end_wgt(0)	
				Case 11		
					dw_wgt_6.uf_set_begin_wgt(0)	
				Case 12
					dw_wgt_6.uf_set_end_wgt(0)	
				Case 13		
					dw_wgt_7.uf_set_begin_wgt(0)	
				Case 14
					dw_wgt_7.uf_set_end_wgt(0)	
			END CHOOSE
		End If
		ls_temp = u_string.nf_gettoken(ls_Installed,'~t')
		If Not IsNumber(ls_temp) Then
			ls_temp = '0'
		End If	
		ll_counter ++
	LOOP
End If

dw_wgt_1.ResetUpdate()
dw_wgt_2.ResetUpdate()
dw_wgt_3.ResetUpdate()
dw_wgt_4.ResetUpdate()
dw_wgt_5.ResetUpdate()
dw_wgt_6.ResetUpdate()
dw_wgt_7.ResetUpdate()
iu_ws_pas4 = Create u_ws_pas4

wf_retrieve()

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'begin_date'
		dw_dates.uf_set_begin_date(Date(as_value))
	Case 'end_date'
		dw_dates.uf_set_end_date(Date(as_value))
End Choose


end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288

integer li_x		= 57
integer li_y		= 318

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

//  
//dw_hot_box.width	= newwidth - (30 + li_x)
//dw_hot_box.height	= newheight - (30 + li_y)
//
  
dw_hot_box.width	= newwidth - (li_x)
dw_hot_box.height	= newheight - (li_y)
end event

type dw_hot_box from u_base_dw_ext within w_hot_box_actual
event ue_dwndropdown pbm_dwndropdown
integer y = 387
integer width = 2823
integer height = 947
integer taborder = 0
boolean bringtotop = true
string dataobject = "d_hot_box_actual"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;//is_selection = '1'
end event

event itemchanged;call super::itemchanged;//DataWindowChild 		ldwc_child
//DataStore 				lds_Tmp
//Long 						ll_count,ll_row,ll_find
//String					ls_temp
//Decimal					ld_temp
//u_string_functions 	u_string
//
//CHOOSE CASE dwo.name
//	CASE 'desc'
//		If u_string.nf_countoccurrences(data,'~r~n') > 0 Then
//			iw_frame.SetMicroHelp("You can not enter a carrage return(Enter) in Long Description")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'short_desc'
//		If u_string.nf_countoccurrences(data,'~r~n') > 0 Then
//			iw_frame.SetMicroHelp("You can not enter a carrage return(Enter) in Short Description")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//		ll_find = dw_cattle.Find ( "short_desc = '" + data + "'", 1, dw_cattle.RowCount() )
//		If ll_find > 0 Then
//			iw_frame.SetMicroHelp("This Short Description has already been used -- please choose another")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'grade'
//		dw_cattle.GetChild("grade", ldwc_child)
//		ll_find = ldwc_child.Find ( "grade = '" + data + "'", 1, ldwc_child.RowCount() )
//		If ll_find = 0 then
//			iw_frame.SetMicroHelp("The Grade is not a valid grade")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'min_wght'
//		IF Not IsNumber(data) Then
//			iw_frame.SetMicroHelp("Min Weight Range must be numeric")
//			This.SelectText(1, 100)
//			Return 1
//		End IF	
//		ld_temp =  dw_cattle.GetItemDecimal(row,'max_wght')
//		If dec(data) > ld_temp and ld_temp > 0 Then
//			iw_frame.SetMicroHelp("Min Weight Range can not be greater then the Max Weight Range")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'max_wght'
//		IF Not IsNumber(data) Then
//			iw_frame.SetMicroHelp("Max Weight Range must be numeric")
//			This.SelectText(1, 100)
//			Return 1
//		End IF	
//		ld_temp =  dw_cattle.GetItemDecimal(row,'min_wght')
//		If dec(data) < ld_temp Then
//			iw_frame.SetMicroHelp("Max Weight Range can not be less then the Min Weight Range")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'min_grade'
//		ld_temp =  dw_cattle.GetItemDecimal(row,'max_grade')
//		If dec(data) > ld_temp and ld_temp > 0 Then
//			iw_frame.SetMicroHelp("Min Yield Grade can not be greater then the Max Yield Grade")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'max_grade'
//		ld_temp =  dw_cattle.GetItemDecimal(row,'min_grade')
//		If dec(data) < ld_temp Then
//			iw_frame.SetMicroHelp("Max Yield Grade can not be less then the Min Yield Grade")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'destination'
//		dw_cattle.GetChild("destination", ldwc_child)
//		ll_find = ldwc_child.Find ( "destination = '" + data + "'", 1, ldwc_child.RowCount() )
//		If ll_find = 0 then
//			iw_frame.SetMicroHelp("The Destination is not a valid destination")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'sex'
//		dw_cattle.GetChild("sex", ldwc_child)
//		ll_find = ldwc_child.Find ( "sex = '" + data + "'", 1, ldwc_child.RowCount() )
//		If ll_find = 0 then
//			iw_frame.SetMicroHelp("The Sex is not a valid sex")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//END CHOOSE
//
//iw_frame.SetMicroHelp("Ready")
Return 0
end event

event itemerror;call super::itemerror;//Return 2

end event

type dw_plant from u_plant within w_hot_box_actual
integer x = 179
integer width = 1503
integer height = 90
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;disable()
end event

type dw_wgt_1 from u_begin_end_weights within w_hot_box_actual
integer x = 208
integer y = 224
integer width = 358
integer height = 173
integer taborder = 10
boolean bringtotop = true
end type

type dw_wgt_2 from u_begin_end_weights within w_hot_box_actual
integer x = 567
integer y = 224
integer width = 358
integer height = 173
integer taborder = 20
boolean bringtotop = true
end type

type dw_wgt_3 from u_begin_end_weights within w_hot_box_actual
integer x = 933
integer y = 224
integer width = 358
integer height = 173
integer taborder = 30
boolean bringtotop = true
end type

type dw_wgt_4 from u_begin_end_weights within w_hot_box_actual
integer x = 1287
integer y = 224
integer width = 358
integer height = 173
integer taborder = 40
boolean bringtotop = true
end type

type dw_wgt_5 from u_begin_end_weights within w_hot_box_actual
integer x = 1657
integer y = 224
integer width = 358
integer height = 173
integer taborder = 50
boolean bringtotop = true
end type

type dw_wgt_6 from u_begin_end_weights within w_hot_box_actual
integer x = 2022
integer y = 224
integer width = 358
integer height = 173
integer taborder = 60
boolean bringtotop = true
end type

type dw_wgt_7 from u_begin_end_weights within w_hot_box_actual
integer x = 2381
integer y = 224
integer width = 358
integer height = 173
integer taborder = 70
boolean bringtotop = true
end type

type st_1 from statictext within w_hot_box_actual
integer x = 241
integer y = 154
integer width = 2472
integer height = 77
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "----------------------------------------------------------------------------- Weight Ranges ----------------------------------------------------------------------------"
boolean focusrectangle = false
end type

type dw_dates from u_begin_end_dates within w_hot_box_actual
integer x = 2063
integer height = 170
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;This.insertrow(0)
end event

