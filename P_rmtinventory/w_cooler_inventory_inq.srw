HA$PBExportHeader$w_cooler_inventory_inq.srw
forward
global type w_cooler_inventory_inq from w_base_response_ext
end type
type dw_date from u_base_dw_ext within w_cooler_inventory_inq
end type
type dw_complex from u_complex2 within w_cooler_inventory_inq
end type
end forward

global type w_cooler_inventory_inq from w_base_response_ext
integer width = 1673
integer height = 476
string title = "Cooler Inventory Inquire"
long backcolor = 67108864
dw_date dw_date
dw_complex dw_complex
end type
global w_cooler_inventory_inq w_cooler_inventory_inq

on w_cooler_inventory_inq.create
int iCurrent
call super::create
this.dw_date=create dw_date
this.dw_complex=create dw_complex
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_date
this.Control[iCurrent+2]=this.dw_complex
end on

on w_cooler_inventory_inq.destroy
call super::destroy
destroy(this.dw_date)
destroy(this.dw_complex)
end on

event ue_base_ok;call super::ue_base_ok;String					ls_complex

Date						ld_start_date
							
u_string_functions	lu_strings

dw_date.AcceptText()
dw_complex.AcceptText()

ls_complex = dw_complex.uf_get_complex_code()

If lu_strings.nf_IsEmpty(ls_complex) Then 
	iw_frame.SetMicroHelp("Complex code is a required field")
	dw_complex.SetFocus()
	Return
End If	

ld_start_date = dw_date.GetItemDate(1, "start_date")

If (ld_start_date > Today() OR ld_start_date < RelativeDate(Today(),  - 7))  Then
	iw_frame.SetMicroHelp("Date must be between 7 days prior and current date")
	dw_date.SetFocus()
	Return
End If	
	
ib_ok_to_close = True

iw_parentwindow.Event ue_set_data('complex_code', ls_complex)

iw_parentwindow.Event ue_set_data('complex_name', dw_complex.uf_get_complex_descr())

iw_parentwindow.Event ue_set_data('start_date',String(dw_date.GetItemDate(1,"start_date"),"mm/dd/yyyy"))

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen;call super::ue_postopen;
String	ls_complex_code

ls_complex_code = ProfileString( iw_frame.is_UserINI, "Pas", "Lastcomplex","")

if ls_complex_code = '' then
	ls_complex_code = string('   ')
end if

dw_complex.SetItem(1, "complex_code", ls_complex_code)
dw_complex.uf_set_complex_desc(ls_complex_code)


iw_parentwindow.Event ue_get_data('start_date')

If Message.StringParm = '' Then
	dw_date.SetItem(1, "start_date", Today())
Else
	dw_date.SetItem(1, "start_date", Date(Message.StringParm))
End If





end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_cooler_inventory_inq
boolean visible = false
integer x = 1262
integer y = 304
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_cooler_inventory_inq
integer x = 1339
integer y = 240
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_cooler_inventory_inq
integer x = 1051
integer y = 240
integer taborder = 20
end type

type dw_date from u_base_dw_ext within w_cooler_inventory_inq
integer x = 110
integer y = 128
integer width = 549
integer height = 96
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_cooler_inv_date"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_complex from u_complex2 within w_cooler_inventory_inq
integer x = 73
integer taborder = 10
boolean bringtotop = true
end type

