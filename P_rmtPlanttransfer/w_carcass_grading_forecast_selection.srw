HA$PBExportHeader$w_carcass_grading_forecast_selection.srw
forward
global type w_carcass_grading_forecast_selection from w_base_response_ext
end type
type dw_formula_grading from u_base_dw_ext within w_carcass_grading_forecast_selection
end type
end forward

global type w_carcass_grading_forecast_selection from w_base_response_ext
integer width = 1456
integer height = 1277
long backcolor = 67108864
dw_formula_grading dw_formula_grading
end type
global w_carcass_grading_forecast_selection w_carcass_grading_forecast_selection

type variables
s_error			istr_error_info

u_rmt001			iu_rmt001
u_ws_pas4		iu_ws_pas4


String			is_dropdown_value

DataStore		ids_formula_grading

end variables

forward prototypes
public subroutine wf_default_row (long al_row)
protected subroutine wf_get_characteristics ()
end prototypes

public subroutine wf_default_row (long al_row);String				ls_formula_type

ls_formula_type = dw_formula_grading.object.formula_type_t.text
dw_formula_grading.SetItem(al_row, 'formula_type', ls_formula_type)
dw_formula_grading.SetItem(al_row, 'char_name', dw_formula_grading.object.char_t.text)
dw_formula_grading.SetItem(al_row, 'category', dw_formula_grading.object.category_t.text)
dw_formula_grading.SetItem(al_row, 'formula_char_name', ' ')
dw_formula_grading.SetItem(al_row, 'update_flag', 'I')


end subroutine

protected subroutine wf_get_characteristics ();String					ls_header, &
							ls_characteristics, &
							ls_current_char, &
							ls_char, &
							ls_category, &
							ls_filter_string, &
							ls_filter, &
							ls_formula_char, &
							ls_formula_type, &
							ls_temp
							
Long						ll_row, &
							ll_find_row, &
							ll_new_row
							
DataWindowChild		ldwc_formula_characteristcs


istr_error_info.se_window_name = "w_carcass_grading_forcast_selection"
istr_error_info.se_event_name = "wf_get_characteristics"
istr_error_info.se_message = Space(71)

istr_error_info.se_procedure_name = "u_rmt001.uf_rmtr30mr_plant_char_inq"
iu_rmt001 = Create u_rmt001

ls_current_char = dw_formula_grading.object.char_t.text

ls_category = dw_formula_grading.object.category_t.text

ls_header = dw_formula_grading.object.plant_t.text + '~t' + &
				ls_char + '~t' + ls_category + '~r~n'
/*
If Not iu_rmt001.uf_rmtr30mr_plant_char_inq(istr_error_info, ls_header,ls_characteristics)  Then
	MessageBox('DropDown Datawindows', 'There is a problem getting the characteristics. Please call Applications.')
	return
end If
*/
If Not iu_ws_pas4.NF_RMTR30NR(istr_error_info, ls_header,ls_characteristics)  Then
	MessageBox('DropDown Datawindows', 'There is a problem getting the characteristics. Please call Applications.')
	return
end If



dw_formula_grading.GetChild('formula_char_name', ldwc_formula_characteristcs)
ldwc_formula_characteristcs.ImportString(ls_characteristics)

ll_find_row = ldwc_formula_characteristcs.Find("code = '" + ls_char + "'", 1, ldwc_formula_characteristcs.RowCount())
if ll_find_row > 0 then
	ldwc_formula_characteristcs.DeleteRow(ll_find_row)
End if

dw_formula_grading.ImportString(ids_formula_grading.object.datawindow.data)

ls_formula_type = dw_formula_grading.object.formula_type_t.text
ls_filter_string = "char_name = '" + ls_current_char + &
		"' and category = '" + ls_category + &
		"' and formula_type = '" + Trim(ls_formula_type) + "'" 
dw_formula_grading.SetFilter(ls_filter_string)
dw_formula_grading.Filter()

ls_filter_string = "category = '" + ls_category +  "'" 
ids_formula_grading.SetFilter(ls_filter_string)
ids_formula_grading.Filter()

ls_temp = ids_formula_grading.object.datawindow.data

if ldwc_formula_characteristcs.RowCount() > 0 then
	for ll_row = 1 to ldwc_formula_characteristcs.RowCount()
		ls_char = Trim(ldwc_formula_characteristcs.GetItemString(ll_row, 'code'))
		ll_find_row = ids_formula_grading.Find("formula_char_name = '" + ls_char + "' OR " + &
																	"char_name = '" + ls_char + "'"	, 1, ids_formula_grading.RowCount())
		if ll_find_row > 0 then
			if ids_formula_grading.GetItemString(ll_find_row, 'update_flag') = 'D' then
				if ls_char = ls_current_char or ls_char = Trim(ids_formula_grading.GetItemString(ll_find_row, 'char_name')) then
					ldwc_formula_characteristcs.DeleteRow(ll_row)
					ll_row --
				end if	
			Else
				ldwc_formula_characteristcs.DeleteRow(ll_row)
				ll_row --
			end if	
		end if
	next
end if

if dw_formula_grading.RowCount() > 0 then
	for ll_row = 1 to dw_formula_grading.RowCount()
		if dw_formula_grading.GetItemString(ll_row, 'update_flag') = 'D' then
			dw_formula_grading.DeleteRow(ll_row)			
			ll_row --
		End If
	next
End If
		

ldwc_formula_characteristcs.SetSort('code A')
ldwc_formula_characteristcs.Sort()



end subroutine

on w_carcass_grading_forecast_selection.create
int iCurrent
call super::create
this.dw_formula_grading=create dw_formula_grading
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_formula_grading
end on

on w_carcass_grading_forecast_selection.destroy
call super::destroy
destroy(this.dw_formula_grading)
end on

event open;call super::open;String			ls_title, &
					ls_char


iw_frame.SetMicroHelp("Ready")


end event

event ue_postopen;call super::ue_postopen;
String 					ls_find_str, &
							ls_char, &
							ls_category, &
							ls_filter_string, &
							ls_formula_string, &
							ls_formula_type
							
Long						ll_find, &
							ll_new_row, &
							ll_row_count

DataWindowChild		ldwc_formula_type
iu_ws_pas4 = Create u_ws_pas4


ib_ok_to_close = False

iw_parentwindow.Event ue_get_data('plant')
dw_formula_grading.object.plant_t.text = message.StringParm

iw_parentwindow.Event ue_get_data('Characteristic')
ls_char = message.StringParm
dw_formula_grading.object.char_t.text = ls_char

iw_parentwindow.Event ue_get_data('category')
ls_category = message.StringParm
dw_formula_grading.object.category_t.text = ls_category

dw_formula_grading.SetRedraw(false)

ids_formula_grading =  Create DataStore
ids_formula_grading.DataObject = "d_grade_formula"
ids_formula_grading.Reset()

iw_parentwindow.Event ue_get_data('GRD')
ls_formula_string = Message.StringParm
ids_formula_grading.ImportString(ls_formula_string)

iw_parentwindow.Event ue_get_data('formula_type')
ls_formula_type = message.StringParm
dw_formula_grading.object.formula_type_t.text = ls_formula_type
wf_get_characteristics()

dw_formula_grading.GetChild('formula_type', ldwc_formula_type)
ldwc_formula_type.SetTransObject(SQLCA)
ldwc_formula_type.Retrieve()

dw_formula_grading.ResetUpdate()

ll_row_count = dw_formula_grading.RowCount()

if  ll_row_count > 0 then
	ll_new_row = dw_formula_grading.InsertRow(ll_row_count + 1)
else
	ll_new_row = dw_formula_grading.InsertRow(0)
end if

wf_default_row(ll_new_row)

dw_formula_grading.SetRedraw(true)

This.Title = "Grading Formula Selection"

end event

event ue_base_ok;call super::ue_base_ok;String		ls_formula_string, &
				ls_formula_type, &
				ls_char_name

Long			ll_row


ls_char_name = Trim(dw_formula_grading.GetItemString(1, 'formula_char_name'))
if Len(ls_char_name ) = 0 then
	ls_formula_type = '    '
else
	ls_formula_type = dw_formula_grading.object.formula_type_t.text
end if
iw_parentwindow.Event ue_set_data('formula_type', ls_formula_type)

if dw_formula_grading.RowCount() > 0 then
	if Len(Trim(ls_formula_type )) = 0 and Len(Trim(ls_char_name )) > 0 then
		MessageBox("", "Please select a Formula Type")
		Return
	End IF
End If

dw_formula_grading.SetRedraw(false)

dw_formula_grading.RowsDiscard(dw_formula_grading.RowCount(), dw_formula_grading.RowCount(), Primary!)

dw_formula_grading.SetFilter("")
dw_formula_grading.Filter()

ls_formula_string = ''
for ll_row = 1 to dw_formula_grading.DeletedCount()
	ls_formula_string += dw_formula_grading.GetItemString(ll_row, 'formula_type', Delete!, false) + '~t'
	ls_formula_string += dw_formula_grading.GetItemString(ll_row, 'char_name', Delete!, false) + '~t'
	ls_formula_string += dw_formula_grading.GetItemString(ll_row, 'category', Delete!, false) + '~t'
	ls_formula_string += dw_formula_grading.GetItemString(ll_row, 'formula_char_name', Delete!, false) + '~t'
	ls_formula_string += dw_formula_grading.GetItemString(ll_row, 'update_flag', Delete!, false) + '~r~n'
Next


ls_formula_string += dw_formula_grading.object.datawindow.data + '~r~n'

iw_parentwindow.Event ue_set_data('GRD',ls_formula_string)

ib_ok_to_close = True

Close(This)

end event

event ue_base_cancel;call super::ue_base_cancel;ib_ok_to_close = False

Close(This)
end event

event close;call super::close;String			ls_setvalue


If ib_ok_to_close Then
	ls_setvalue = 'true'
Else
	ls_setvalue = 'false'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('selection', ls_setvalue)
End IF

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_carcass_grading_forecast_selection
boolean visible = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_carcass_grading_forecast_selection
integer x = 1112
integer y = 954
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_carcass_grading_forecast_selection
integer x = 834
integer y = 954
end type

type dw_formula_grading from u_base_dw_ext within w_carcass_grading_forecast_selection
event ue_dwndropdown pbm_dwndropdown
integer x = 4
integer y = 10
integer width = 1353
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_grade_formula"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_dwndropdown;is_dropdown_value = this.GetItemString(This.GetRow(), 'formula_char_name')
end event

event itemchanged;call super::itemchanged;String				ls_find_string

Long					ll_find_row, &
						ll_new_row, &
						ll_row

DataWindowChild	ldwc_formula_characteristcs

This.GetChild('formula_char_name', ldwc_formula_characteristcs)

Choose Case dwo.name
	case 'formula_char_name'
		if Len(Trim(is_dropdown_value)) > 0 then
			ll_find_row = ldwc_formula_characteristcs.Find("code = '" + is_dropdown_value + &
					"'", 1, ldwc_formula_characteristcs.RowCount())
			if ll_find_row = 0 then
				ll_new_row = ldwc_formula_characteristcs.InsertRow(0)
				ldwc_formula_characteristcs.SetItem(ll_new_row, 'code', is_dropdown_value)
				ldwc_formula_characteristcs.SetSort('code A')
				ldwc_formula_characteristcs.Sort()
			End if
		end if
		
		ll_find_row = ldwc_formula_characteristcs.Find("code = '" + data + "'", 1, ldwc_formula_characteristcs.RowCount())
		if ll_find_row > 0 then
			ldwc_formula_characteristcs.DeleteRow(ll_find_row)
		End if

	case 'formula_type'
		for ll_row = 1 to this.RowCount()
			This.SetItem(ll_row, 'formula_type', data)
			if This.GetItemString(ll_row, 'update_flag') = 'I' Then
				//do nothing
			else
				This.SetItem(ll_row, 'update_flag', 'U')
			end if
		next
		this.object.formula_type_t.text = data
end Choose

if row = this.RowCount() then
	ll_new_row = this.insertrow(this.RowCount() + 1)
	wf_default_row(ll_new_row)
End if

end event

event clicked;call super::clicked;String						ls_formula_char

Long							ll_find_row, &
								ll_new_row, &
								ll_row_count

DataWindowChild			ldwc_formula_characteristcs

This.GetChild('formula_char_name', ldwc_formula_characteristcs)

choose case dwo.name
	case 't_delete'
		ls_formula_char = Trim(this.GetItemString(row, 'formula_char_name'))
		if Len(ls_formula_char) > 0 then
			ll_find_row = ldwc_formula_characteristcs.Find("code = '" + ls_formula_char + "'", 1, ldwc_formula_characteristcs.RowCount())
			if ll_find_row = 0 then
				ll_new_row = ldwc_formula_characteristcs.InsertRow(0)
				ldwc_formula_characteristcs.SetItem(ll_new_row, 'code', ls_formula_char)
				ldwc_formula_characteristcs.SetSort('code A')
				ldwc_formula_characteristcs.Sort()
			End if
		end if
		ll_row_count = This.RowCount()
		if row = ll_row_count then
			if row = 1 then
				this.object.formula_type_t.text = '    '
			end if 
			ll_new_row = this.insertrow(0)
			wf_default_row(ll_new_row)
		End if
		This.SetItem(row, 'update_flag', 'D')
		This.DeleteRow(row)
End Choose		
		
end event

