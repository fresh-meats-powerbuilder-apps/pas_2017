HA$PBExportHeader$w_carcass_grading_estimate.srw
$PBExportComments$Ibdkdld
forward
global type w_carcass_grading_estimate from w_base_sheet_ext
end type
type dw_satellite_cattle from u_base_dw_ext within w_carcass_grading_estimate
end type
type dw_plant from u_plant within w_carcass_grading_estimate
end type
type dw_last_updated from u_last_updated_combined within w_carcass_grading_estimate
end type
type dw_effective_date from u_effective_date within w_carcass_grading_estimate
end type
type dw_shift from u_shift within w_carcass_grading_estimate
end type
type cbx_update_b from checkbox within w_carcass_grading_estimate
end type
end forward

global type w_carcass_grading_estimate from w_base_sheet_ext
boolean visible = false
integer x = 1463
integer y = 4
integer width = 1467
integer height = 1632
long backcolor = 67108864
dw_satellite_cattle dw_satellite_cattle
dw_plant dw_plant
dw_last_updated dw_last_updated
dw_effective_date dw_effective_date
dw_shift dw_shift
cbx_update_b cbx_update_b
end type
global w_carcass_grading_estimate w_carcass_grading_estimate

type variables
u_rmt001		iu_rmt001
u_ws_pas5		iu_ws_pas5

s_error		istr_error_info

String		is_update_string, &
		is_update_string1

Long		il_rec_count

Window		iw_parentwindow

Boolean		ib_update_inquire
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_validate ()
public function boolean wf_update_modify (long al_row)
end prototypes

public function boolean wf_retrieve ();Long						ll_rec_count, &
							ll_row, &
							ll_message, &
							ll_count, &
							ll_column = 1,&
							ll_column1 = 2
							
String					ls_plant, &
							ls_column, & 
							ls_header, &
							ls_column_headings, &
							ls_detail, &
							ls_date, &
							ls_inquire_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_time, &
							ls_describe
							
u_string_functions 	lu_string	
Date						ld_date
DateTime 				ldt_datetime
	
If dw_plant.AcceptText() = -1 or &
	dw_shift.AcceptText() = -1 or &
	dw_effective_date.AcceptText() = -1 Then Return False
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

// I did this so the user would get the updated data displayed
// see wf_update
If Not ib_update_inquire Then
	OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
End If

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)
If ib_update_inquire Then
	iw_frame.SetMicroHelp("Modification Successful on Update... Wait.. Reinquiring Database")
	ib_update_inquire = False
Else	
	iw_frame.SetMicroHelp("Wait.. Inquiring Database")
End If
	
This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr18mr_inq_car_grade_estimate"

//Populate header-string

ls_plant	  = dw_plant.uf_get_plant_code()
ls_header  = ls_plant + '~t'
ls_header += String(dw_effective_date.uf_get_effective_date()) + '~t'
ls_header += dw_shift.uf_get_shift() + '~t'

//MessageBox("header",ls_header)

dw_satellite_cattle.reset()
//If iu_rmt001.uf_rmtr18mr_inq_car_grade_estimate(istr_error_info, & 
//										ls_header, &
//										ls_detail) = - 1 Then 
//										This.SetRedraw(True) 
//										Return False

If iu_ws_pas5.uf_rmtr18nr(istr_error_info, & 
										ls_header, &
										ls_detail) = - 1 Then 
										This.SetRedraw(True) 
										Return False
End If			

//MessageBox("Return header",ls_header)

//ls_header = '1999/09/09 12:48 AM IBDKDLD'
dw_last_updated.uf_set_last_updated(ls_header)

//ls_detail = &
//'PRIME~t~t9~tG~r~n' + &
//'PRIME~tPRI~t2~t~r~n' + &
//'PRIME~tITOM PRI~t3~t~r~n' + &
//'PRIME~tNIPP PRI~t4~t~r~n' + &
//'CHOICE~t~t83~tG~r~n' + &
//'CHOICE~tLT CAB~t80~t~r~n' + &
//'CHOICE~tLT CHO~t2~t~r~n' + &
//'CHOICE~tPREST CH~t1~t~r~n' + &
//'NR~t~t8~tG~r~n' +&
//'SELECT~t~t10~tG~r~n' + &
//'SELECT~tLT SEL~t9~t~r~n' + &
//'SELECT~tHVY SEL~t1~t~r~n' 
//MessageBox("ls_detail",ls_detail)

ll_rec_count = dw_satellite_cattle.ImportString(ls_detail)

If ll_rec_count > 0 Then
		SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End If
dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.SetFocus()
This.SetRedraw(True) 
Return True

end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_moddate, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name,ls_column_text,ls_temp
dwItemStatus	ldwis_status			
u_string_functions u_string

IF dw_satellite_cattle.AcceptText() = -1 Then
	Return False
End If

ll_modrows = dw_satellite_cattle.ModifiedCount()
//ll_delrows = dw_satellite_cattle.DeletedCount()

IF ll_modrows <= 0 /*and ll_delrows <= 0*/ Then 
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

If Not wf_validate() Then
	Return False
End If

ls_header = ''
ls_header += dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_effective_date.uf_get_effective_date()) + '~t'
ls_temp = dw_shift.uf_get_shift()
ls_header += ls_temp + '~t'
If ls_temp = "A" Then
	If cbx_update_b.checked then
		ls_header += "C" + '~t'
	Else
		ls_header += " " + '~t'
	End If
Else
	ls_header += " " + '~t'
End If
	
SetMicroHelp("Wait, Updating ...")
SetPointer(HourGlass!)
This.SetRedraw(False)
is_update_string = ""

//For ll_count = 1 To ll_delrows 
//	lc_status_ind = 'D'
//	If Not This.wf_update_modify(ll_row,lc_status_ind) Then 
//		This.SetRedraw(True)
//		Return False
//	End if
//Next

ll_row = 0
dw_satellite_cattle.SelectRow(0,False)
//////////////
lc_status_ind = ' '
For ll_count = 1 To ll_modrows
	ll_Row = dw_satellite_cattle.GetNextModified(ll_Row, Primary!) 
	If Not This.wf_update_modify(ll_row) Then 
		This.SetRedraw(True)
		Return False
	End If
Next

/////////////

//MessageBox('header',ls_header)
//MessageBox('Update String',is_update_string)
//
//IF Not iu_rmt001.uf_rmtr19mr_upd_car_grade_estimate(istr_error_info, &
//								ls_header, is_update_string) THEN 
//	This.SetRedraw(True)
//	Return False
//End if

IF Not iu_ws_pas5.nf_rmtr19nr(istr_error_info, &
								ls_header, is_update_string) THEN 
	This.SetRedraw(True)
	Return False
End if

dw_satellite_cattle.ResetUpdate()
This.SetRedraw(True)

// I did this so the user wouldn't have to reinquire after the update
ib_update_inquire = True
wf_retrieve()

Return( True )
end function

public function boolean wf_validate ();Long		ll_row,ll_row_count
Decimal	ld_100_percent = 100.00,ld_temp

ll_row_count = dw_satellite_cattle.RowCount()
//dw_satellite_cattle.SetRedraw(False)
//add up all of the values
FOR ll_row  = 1 TO ll_row_count
	If dw_satellite_cattle.GetItemString(ll_row,'group_ind') = 'G' Then	
		ld_temp  += dw_satellite_cattle.GetItemDecimal(ll_row,'percent')
	End IF
NEXT

If ld_temp = ld_100_percent Then
	Return True
Else
	iw_frame.SetMicroHelp("The total for Percents must equal 100 percent" + &
	" the current total is " + String(ld_temp) + " percent")
	Return False
End If

end function

public function boolean wf_update_modify (long al_row);DWBuffer						ldwb_buffer

//CHOOSE CASE as_buffer
//	CASE 'D'
//		ldwb_buffer = Delete!
//	CASE ELSE
ldwb_buffer = Primary!
//END CHOOSE

is_update_string += &
	dw_satellite_cattle.GetItemString(al_row,"char_group",ldwb_buffer, False) + "~t" 
is_update_string += &
	String(dw_satellite_cattle.GetItemNumber(al_row,"percent",ldwb_buffer, False)) + "~r~n"	 

Return True
end function

on w_carcass_grading_estimate.create
int iCurrent
call super::create
this.dw_satellite_cattle=create dw_satellite_cattle
this.dw_plant=create dw_plant
this.dw_last_updated=create dw_last_updated
this.dw_effective_date=create dw_effective_date
this.dw_shift=create dw_shift
this.cbx_update_b=create cbx_update_b
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_satellite_cattle
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_last_updated
this.Control[iCurrent+4]=this.dw_effective_date
this.Control[iCurrent+5]=this.dw_shift
this.Control[iCurrent+6]=this.cbx_update_b
end on

on w_carcass_grading_estimate.destroy
call super::destroy
destroy(this.dw_satellite_cattle)
destroy(this.dw_plant)
destroy(this.dw_last_updated)
destroy(this.dw_effective_date)
destroy(this.dw_shift)
destroy(this.cbx_update_b)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

end event

event open;call super::open;This.Title = 'Carcass Grading Estimated'

dw_plant.disable()
dw_effective_date.uf_enable(false)
dw_effective_date.uf_set_text('Transfer Date:')
dw_effective_date.uf_set_effective_date(Today())
dw_shift.uf_enable(False)
dw_shift.uf_set_shift('A')

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'effective_date'
		message.StringParm = String(dw_effective_date.uf_get_effective_date())
	Case 'shift'
		message.StringParm = dw_shift.uf_get_shift()
	Case 'date_object_text'
		message.StringParm = 'Transfer Date:' + '~t' + 'PA' + '~t'
End choose

end event

event ue_postopen;call super::ue_postopen;//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "CarGrEst"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_carcass_estimated_inq'

iu_rmt001 = Create u_rmt001
iu_ws_pas5 = Create u_ws_pas5

wf_retrieve()


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'shift'
		dw_shift.uf_set_shift(as_value)
		Choose Case trim(as_value)
			Case 'A'
				cbx_update_b.Show()
			Case Else
				cbx_update_b.Hide()
		End Choose	
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date(Date(as_value))
End Choose


end event

event close;call super::close;Destroy iu_ws_pas5
end event

type dw_satellite_cattle from u_base_dw_ext within w_carcass_grading_estimate
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
integer y = 364
integer width = 1129
integer height = 1124
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_carcass_grading_estimate"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;Long 		ll_row_count,ll_dump,ll_row
String	ls_column,ls_column_name,ls_temp
Decimal	ld_column_total,ld_100_percent= 100.00,ld_compute,ld_temp,ld_temp1,ld_temp2

This.SetRedraw(False) 
// Insure data is valid
If Not IsNumber(data) Then
	iw_frame.SetMicroHelp("All Columns must be numeric")
	This.SelectText(1, 100)
	This.SetRedraw(True) 
	Return 1
End If
IF Sign(Integer(data)) = -1 Then
	iw_frame.SetMicroHelp("The Number must be Positive")
	This.SelectText(1, 100)
	This.SetRedraw(True) 
	Return 1
End If

//ll_row_count = This.RowCount()
//// Reset the values before proceeding
//FOR ll_row = 1 TO ll_row_count
//	This.SetItem(ll_row,'from_wgt','N')
//	This.SetItem(ll_row,'to_wgt','N')
//NEXT
//
//CHOOSE CASE dwo.name
//	CASE 'from_wgt'
//		ld_temp  = Dec(data)
//		ld_temp1 = This.GetItemDecimal(row,'to_wgt')
//		If ld_temp >= ld_temp1 Then
//			iw_frame.SetMicroHelp("The From Weight of " + string(ld_temp) + &
//				" can't be greater then or equal to the To Weight of " + String(ld_temp1))
//			This.SetItem(row,'color_from','Y')
//			This.SetRedraw(True) 
//			Return 0
//		Else
//			This.SetItem(row,'color_from','N')
//		End If
//		If Not row = ll_row_count Then
//			ld_temp2 = This.GetItemDecimal(row + 1,'from_wgt')
//			If ld_temp1 >= ld_temp2 Then
//				iw_frame.SetMicroHelp("The From Weight of " + string(ld_temp2) + &
//					" can't be less then or equal to the To Weight of " + String(ld_temp1) + &
//					" from the previous row")
//				This.SetItem(row,'color_to','Y')
//				This.SetRedraw(True) 
//				Return 0
//			Else
//				This.SetItem(row,'color_to','N')
//			End If
//		End If
//	CASE 'to_wgt'
//		ld_temp  = Dec(data)
//		ld_temp1 = This.GetItemDecimal(row,'from_wgt')
//		If ld_temp <= ld_temp1 Then
//			iw_frame.SetMicroHelp("The To Weight of " + string(ld_temp) + &
//				" can't be less then or equal to the From Weight of " + String(ld_temp1))
//			This.SetItem(row,'color_from','Y')
//			This.SetRedraw(True) 
//			Return 0
//		else
//			This.SetItem(row,'color_from','N')
//		End If
//		If Not row = ll_row_count Then
//			ld_temp2 = This.GetItemDecimal(row + 1,'from_wgt')
//			If ld_temp >= ld_temp2 Then
//				iw_frame.SetMicroHelp("The From Weight of " + string(ld_temp2) + &
//					" can't be less then or equal to the To Weight of " + String(ld_temp) + &
//					" from the previous row")
//				This.SetItem(row,'color_to','Y')
//				This.SetRedraw(True) 
//				Return 0
//			Else
//				This.SetItem(row,'color_to','N')
//			End If
//		End If
//END CHOOSE

This.SetRedraw(True) 
Return 0

end event

event constructor;call super::constructor; iw_parentwindow = Parent
end event

event itemerror;call super::itemerror;Return 2
end event

type dw_plant from u_plant within w_carcass_grading_estimate
integer width = 1449
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()

end event

type dw_last_updated from u_last_updated_combined within w_carcass_grading_estimate
integer x = 55
integer y = 172
integer taborder = 40
boolean bringtotop = true
end type

type dw_effective_date from u_effective_date within w_carcass_grading_estimate
integer y = 84
integer width = 718
integer taborder = 20
boolean bringtotop = true
end type

type dw_shift from u_shift within w_carcass_grading_estimate
integer x = 727
integer y = 92
integer width = 329
integer taborder = 30
boolean bringtotop = true
end type

type cbx_update_b from checkbox within w_carcass_grading_estimate
integer y = 252
integer width = 677
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Update B Shift with A Shift:"
boolean checked = true
boolean lefttext = true
end type

