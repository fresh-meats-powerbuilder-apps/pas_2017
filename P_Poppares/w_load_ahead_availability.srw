HA$PBExportHeader$w_load_ahead_availability.srw
forward
global type w_load_ahead_availability from w_netwise_sheet
end type
type ole_load_ahead_availability from olecustomcontrol within w_load_ahead_availability
end type
end forward

global type w_load_ahead_availability from w_netwise_sheet
integer x = 0
integer y = 0
integer width = 3022
integer height = 1812
string title = "Load Ahead Availability"
long backcolor = 79741120
ole_load_ahead_availability ole_load_ahead_availability
end type
global w_load_ahead_availability w_load_ahead_availability

type variables
s_error	istr_error_info
string	Is_order
boolean	ib_OLE_Error

w_load_ahead_availability_detail    iw_load_ahead_availability_detail
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();boolean blnReturn

if ib_OLE_Error then return true

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "ole_load_ahead_availability.Inquire"

setPointer(HourGlass!)

if Is_order = 'order' then
	blnReturn = ole_load_ahead_availability.object.Inquire2()
else
	blnReturn = ole_load_ahead_availability.object.Inquire()
end if

SetPointer(Arrow!)

return  blnReturn
end function

public function boolean wf_update ();boolean lbln_Return

if ib_OLE_Error then return true

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "ole_load_ahead_availability.Save"

SetPointer(HourGlass!)

lbln_Return = ole_load_ahead_availability.object.Save()

SetPointer(Arrow!)

return lbln_Return
end function

on w_load_ahead_availability.create
int iCurrent
call super::create
this.ole_load_ahead_availability=create ole_load_ahead_availability
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_load_ahead_availability
end on

on w_load_ahead_availability.destroy
call super::destroy
destroy(this.ole_load_ahead_availability)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')

iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')
	
ole_load_ahead_availability.SetFocus()
end event

event resize;if sizetype	<> 1 then
	if newwidth  > 10 then
		ole_load_ahead_availability.Width = newwidth - 10
	end if
	
	if newheight > 10 then
		ole_load_ahead_availability.Height = newheight - 10
	end if
end if
end event

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_load_ahead_availability"
istr_error_info.se_user_id 		= sqlca.userid

if this.width  > 10 then
	ole_load_ahead_availability.Width = this.width - 50
end if

if this.height > 10 then
	ole_load_ahead_availability.Height = this.height - 150
end if

wf_retrieve()
return
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_deleterow')
end event

event ue_fileprint;call super::ue_fileprint;ole_load_ahead_availability.object.PrintReport()

end event

event ue_printwithsetup;call super::ue_printwithsetup;//This.TriggerEvent("ue_FilePrint")



end event

event closequery;integer	li_prompt_onsave

//gets the settings values
gw_base_frame.iu_base_data.Trigger Event ue_get_sheetsettings(li_prompt_onsave)
IF gw_base_frame.ib_exit and li_prompt_onsave = 0 THEN RETURN

// Do a security check for save and do nothing if the user does not have priviliges
IF NOT gw_base_frame.im_base_menu.m_file.m_save.enabled THEN RETURN

// Changes were made to one of the datawindows
//If ole_load_ahead_availability.object.QuerySaveChanges() = 2 then //vbCancel
	// Cancel the closing of window
//	Message.ReturnValue = 1
//end if

Return
end event

event open;call super::open;is_order = Message.StringParm	

if is_order = 'order' then
	this.Title = 'Load Ahead Availability by Order'
else
	this.Title = 'Load Ahead Availability'
end if

end event

type ole_load_ahead_availability from olecustomcontrol within w_load_ahead_availability
event messageposted ( string strinfomessage )
event changeorder ( string strchangeorderparams )
event showdetails ( string strorder,  string strdetails,  ref boolean blnshowdetailform )
integer width = 2789
integer height = 1688
integer taborder = 30
boolean bringtotop = true
boolean border = false
long backcolor = 80269524
boolean focusrectangle = false
string binarykey = "w_load_ahead_availability.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event messageposted;
iw_frame.SetMicroHelp(strinfomessage)
end event

event changeorder(string strchangeorderparams);
Window				lw_Temp	

SetPointer(HourGlass!)

OpenSheetWithParm(lw_temp, strchangeorderparams,"w_change_order_production_dates_new", iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)


end event

event showdetails(string strorder, string strdetails, ref boolean blnshowdetailform);string ls_passed_data

if ib_OLE_Error then return

SetPointer(HourGlass!)
blnshowdetailform = false

ls_passed_data = strorder + '~~' + strdetails

OpenSheetWithParm(iw_load_ahead_availability_detail, ls_passed_data,"w_load_ahead_availability_detail", iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)

iw_load_ahead_availability_detail.wf_inquire(strorder,strdetails)


end event

event getfocus;if ib_OLE_Error then return

ole_load_ahead_availability.object.SetFocusBack()
end event

event error;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

if errornumber <> 20535 then
	lstr_rpc_error_info.se_app_name = "PAS"
	lstr_rpc_error_info.se_window_name = ""
	lstr_rpc_error_info.se_function_name = ""
	lstr_rpc_error_info.se_event_name = errorwindowmenu
	lstr_rpc_error_info.se_procedure_name = errorobject
	lstr_rpc_error_info.se_user_id = ""
	lstr_rpc_error_info.se_return_code = ""
	lstr_rpc_error_info.se_message = "[" + string(errornumber) + "] "  + errortext + "~nThe window will be closed."
	
	lstr_rpc_error_info.se_rval = 0
	lstr_rpc_error_info.se_commerror = 0
	lstr_rpc_error_info.se_commerrmsg = ""
	lstr_rpc_error_info.se_neterror = 0
	lstr_rpc_error_info.se_primaryerror = 0
	lstr_rpc_error_info.se_secondaryerror = 0
	lstr_rpc_error_info.se_neterrmsg = space(100)
	
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
end if

action = ExceptionIgnore! 
close(parent)
end event

event externalexception;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

lstr_rpc_error_info.se_app_name = "PAS"
lstr_rpc_error_info.se_window_name = ""
lstr_rpc_error_info.se_function_name = ""
lstr_rpc_error_info.se_event_name = parent.title
lstr_rpc_error_info.se_procedure_name = source
lstr_rpc_error_info.se_user_id = ""
lstr_rpc_error_info.se_return_code = ""
lstr_rpc_error_info.se_message = "[" + string(resultcode) + "] "  + "[" + string(exceptioncode) + "] "  + description + "~nThe window will be closed."

lstr_rpc_error_info.se_rval = 0
lstr_rpc_error_info.se_commerror = 0
lstr_rpc_error_info.se_commerrmsg = ""
lstr_rpc_error_info.se_neterror = 0
lstr_rpc_error_info.se_primaryerror = 0
lstr_rpc_error_info.se_secondaryerror = 0
lstr_rpc_error_info.se_neterrmsg = space(100)

openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")

action = ExceptionIgnore! 
close(parent)
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
09w_load_ahead_availability.bin 
2C00000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff00000003000000000000000000000000000000000000000000000000000000002458d16001cd94e300000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a0000000200000001000000041332cdca11d5b67f010034a0db902802000000002458d16001cd94e32458d16001cd94e3000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
2Effffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400003f0c000800034757f20affffffe00065005f00740078006e00650079007400002b9d000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
19w_load_ahead_availability.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
