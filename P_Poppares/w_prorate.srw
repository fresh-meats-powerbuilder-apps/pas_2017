HA$PBExportHeader$w_prorate.srw
forward
global type w_prorate from w_base_sheet_ext
end type
type cb_1 from commandbutton within w_prorate
end type
type dw_fab from u_fab_product_code within w_prorate
end type
type dw_plant from u_plant within w_prorate
end type
type dw_header from u_base_dw_ext within w_prorate
end type
type em_prorate_pct from editmask within w_prorate
end type
type dw_detail from u_base_dw_ext within w_prorate
end type
end forward

global type w_prorate from w_base_sheet_ext
integer x = 32
integer y = 36
integer width = 3045
integer height = 1868
string title = "Prorate Orders"
long backcolor = 67108864
event changeval ( string column_name )
cb_1 cb_1
dw_fab dw_fab
dw_plant dw_plant
dw_header dw_header
em_prorate_pct em_prorate_pct
dw_detail dw_detail
end type
global w_prorate w_prorate

type variables
Boolean	ib_no_inquire = FALSE

u_pas203		iu_pas203

s_error		istr_error_info

String		is_pr_qty_backcolor, &
		is_pr_qty_protect, &
		is_output_invt_string
		
u_ws_pas5		iu_ws_pas5

DataStore	ids_inv_table, &
				ids_order_table, &
				ids_applied_table
end variables

forward prototypes
public subroutine wf_filter_blocked ()
public subroutine wf_protect_pr_qty (string as_autocalculate)
public subroutine wf_checkloadweight (long al_row)
public function string wf_build_update_string ()
public function boolean wf_update ()
public function boolean wf_retrieve ()
public subroutine wf_new_load_weight_check (long al_row)
public subroutine wf_autocalculate (string as_autocalculate, string as_no_prorate)
public subroutine wf_new_invt_calc ()
public subroutine wf_calc_prorate_percent ()
public subroutine wf_apply_invt_to_excludes ()
public subroutine wf_apply_invt_to_non_excludes ()
public subroutine wf_reallocate_shortages ()
public subroutine wf_apply_excess_inventory ()
public subroutine wf_apply_excess_inventory_any ()
end prototypes

public subroutine wf_filter_blocked ();String	ls_filter

ls_filter = ""
Choose Case True
	Case dw_header.GetItemString(1,"blocked") = "Y" and &
		dw_header.GetItemString(1,"unblocked") = "Y"	
		ls_filter = ""
	Case dw_header.GetItemString(1,"blocked") = "Y"
		ls_filter = "blocked = 'Y'"
	Case dw_header.GetItemString(1,"unblocked") = "Y"
		ls_filter = "blocked = 'N'"
	Case Else
		ls_filter = "blocked = 'Y' and blocked = 'N'"		// Note that this filters everything out
End Choose
dw_detail.SetFilter(ls_filter)
dw_detail.Filter()
dw_detail.Sort()
end subroutine

public subroutine wf_protect_pr_qty (string as_autocalculate);String			ls_describe, ls_temp 


ls_describe = dw_detail.Describe("prorated_qty.Background.color")
If as_autocalculate = 'Y' Then
	dw_detail.Modify("prorated_qty.Background.color ='12632256'")
	dw_detail.Modify("prorated_qty.Protect='1'")
Else
	ls_temp = dw_detail.Modify("prorated_qty.Background.color = '0 ~t if(exclude= ~~'Y~~' or type_of_sale=~~'N~~', 12632256, 16777215)'")
	ls_temp = dw_detail.Modify("prorated_qty.Protect = '0 ~t if(exclude= ~~'Y~~' or type_of_sale=~~'N~~',1, 0)'")
End If

end subroutine

public subroutine wf_checkloadweight (long al_row);Boolean			lb_underweight

String			ls_primary_order_no, &
					ls_exclude

Decimal			ld_minimum_weight, &
					ld_load_weight

Long				ll_temp, &
					ll_findrow

u_string_functions		lu_string


If not al_row > 0 then Return

wf_new_load_weight_check(al_row)

ls_primary_order_no = dw_detail.GetItemString(al_row, 'primary_order_no')
If lu_string.nf_IsEmpty(ls_primary_order_no) Then Return

ls_exclude = dw_detail.GetItemString(al_row,"type_of_sale")
If ls_exclude = "U" or & 
		ls_exclude = "A" or & 
		ls_exclude = "I" or &
		ls_exclude = "E" or &
		ls_exclude = "T" or &
		ls_exclude = "C" or &
		ls_exclude = "G" or &
		ls_exclude = "B" Then Return

lb_underweight = False

ld_minimum_weight = dw_header.GetItemDecimal(1, 'plant_total_weight')

ld_load_weight = dw_detail.GetItemDecimal(al_row, 'pro_total_weight')

If ld_load_weight < ld_minimum_weight Then
//	If dw_detail.GetItemNumber(al_row, 'Prorated_qty') <> &
//			dw_detail.GetItemNumber(al_row, 'quantity') Then
//		dw_detail.SetItem(al_row, 'underweight_ind', 1)
//		lb_underweight = True
//	Else
//		dw_detail.SetItem(al_row, 'underweight_ind', 0)
//	End If
	dw_detail.SetItem(al_row, 'underweight_ind', 1)
	lb_underweight = True
		
Else
	ll_findrow = dw_detail.find('primary_order_no = "' + ls_primary_order_no + &
										'" And underweight_ind = 1', 1, dw_detail.RowCount())
	Do While ll_findrow > 0
		dw_detail.SetItem(ll_findrow, 'underweight_ind', 0)
		ll_findrow ++
		ll_findrow = dw_detail.find('primary_order_no = "' + ls_primary_order_no + &
										'" And underweight_ind = 1', ll_findrow, dw_detail.RowCount())
	Loop
	lb_underweight = False
End If

If lb_underweight Then 
	iw_frame.SetMicroHelp('The Primary Order Numbers in red are underweight')
Else
	iw_frame.SetMicroHelp('Ready')
End If

Return

end subroutine

public function string wf_build_update_string ();String			ls_update_string

Long				ll_rowcount, &
					ll_FindRow

ll_rowcount = dw_detail.RowCount()
ll_FindRow = 0
Do 
	ll_findrow ++
	ll_FindRow = dw_detail.Find('exclude = "N" And prorated_qty < quantity', &
			ll_FindRow, ll_RowCount + 1)
	If ll_FindRow > 0 Then
		ls_update_string += dw_detail.GetItemString(ll_FindRow, 'order_num') + '~t' + &
								dw_detail.GetItemString(ll_FindRow, 'line_number') + '~t' + &
								String(dw_detail.GetItemNumber(ll_FindRow, 'quantity')) + '~t' + &
								String(dw_detail.GetItemNumber(ll_FindRow, 'prorated_qty')) + '~r~n' 
	End If
Loop While ll_findrow > 0

Return ls_update_string

end function

public function boolean wf_update ();Integer	li_ret
String	ls_input_header, &
			ls_input_detail
			
If dw_detail.AcceptText() < 1 Then Return False

SetPointer(HourGlass!)

If dw_detail.Find('underweight_ind > 0', 1, dw_detail.RowCount() + 1) > 0 Then
	If MessageBox('Underweight Flag', 'There is a Primary Order Number that is underweight.  ' + &
			'Do you want to continue?', Question!, YesNo!, 2) = 2 Then Return False
End If


ls_input_header  = dw_plant.uf_get_plant_code() + "~t"
// state changes ibdkdld 10/03/02
//ls_input_header += dw_product.uf_getproductcode() + "~t"
ls_input_header += dw_fab.uf_get_product_code() + "~t"
ls_input_header += String(dw_header.GetItemDate(1,"ship_date"),"yyyy-mm-dd") + "~t"

ls_input_detail = wf_build_update_string()

//li_ret = iu_pas203.nf_pasp57br_upd_prorate(istr_error_info, &
//															ls_input_header, &
//															ls_input_detail)
li_ret = iu_ws_pas5.nf_pasp57fr(istr_error_info, ls_input_header, ls_input_detail)
li_ret = 0															
If li_ret <> 0 Then
	return False
End If

dw_detail.ResetUpdate()
iw_frame.SetMicroHelp("Modification Successful")

return True
end function

public function boolean wf_retrieve ();Decimal	ld_min_plant_weight, &
			ld_available

String	ls_input, &
			ls_header_output, &
			ls_output, &
			ls_filter, &
			ls_exclude, &
			ls_autocalculate, &
			ls_prorate_zero, &
			ls_output_invt_string	

Integer	li_ret, &
			li_counter

u_string_functions		lu_strings


This.TriggerEvent('closequery')


If Not ib_no_inquire Then 
	OpenWithParm(w_prorate_inq,This)
	If iw_frame.iu_string.nf_IsEmpty(Message.StringParm) Then return False
Else
	ib_no_inquire = False
	If dw_header.AcceptText() = -1 Then return False
	If dw_plant.AcceptText() = -1 Then return False
// product state ibdkdld 10/02/02
//If dw_product.AcceptText() = -1 Then return False
	If not dw_fab.uf_validate( ) Then return False
End If

SetPointer(HourGlass!)

ls_input  = dw_plant.uf_get_plant_code() + "~t"
// product state ibdkdld 10/02/02
//ls_input += dw_product.uf_getproductcode() + "~t"
ls_input += dw_fab.uf_get_product_code() + "~t"
ls_input += String(dw_header.GetItemDate(1,"ship_date"),"yyyy-mm-dd") + "~t"
// product state ibdkdld 10/02/02
ls_input += dw_fab.uf_get_product_state()  + "~t"
ls_input += dw_fab.uf_get_product_status()

//li_ret = iu_pas203.nf_pasp56br_inq_prorate(istr_error_info, &
//														ls_input, &
//														ls_header_output, &
//														ls_output)

is_output_invt_string = ''
li_ret = iu_ws_pas5.nf_pasp56fr(istr_error_info, ls_input, ls_header_output, ls_output, is_output_invt_string)

If li_ret <> 0 Then
	return False
End If

//ls_output = "c2244~t100~tAlbertson~t100~t0~tT~tN~t     ~t0~t0~t0~r~n" + &
//				"c2355~t300~tAlbertson~t100~t0~tT~tN~t     ~t0~t0~t0~r~n" + &
//				"c2274~t800~tAlbertson~t100~t0~tT~tN~t     ~t0~t0~t0~r~n" + &
//				"c2245~t200~tAlbertson~t100~t0~tT~tN~tB4515~t25000~t5000~t45~r~n" + &
//				"c2246~t100~tAlbertson~t100~t0~tT~tN~tB4515~t25000~t5000~t45~r~n" + &
//				"c2248~t100~tAlbertson~t100~t0~tT~tN~tB2004~t30000~t5000~t45~r~n" + &
//				"c2248~t200~tAlbertson~t100~t0~tT~tN~tB2004~t30000~t5000~t45~r~n" + &
//				"c2248~t300~tAlbertson~t100~t0~tT~tN~tB2004~t30000~t5000~t45~r~n" + &
//				"c2248~t400~tAlbertson~t100~t0~tT~tN~tB2004~t30000~t5000~t45~r~n" + &
//				"c2249~t400~tAlbertson~t100~t0~tT~tN~tB2004~t30000~t5000~t45~r~n" + &
//				"c2249~t600~tAlbertson~t100~t0~tT~tN~tB2004~t30000~t5000~t45~r~n" 


dw_detail.SetRedraw(False)
dw_detail.Reset()
dw_detail.ImportString(ls_output)
wf_filter_blocked()

ld_min_plant_weight = Double(lu_strings.nf_GetToken(ls_header_output, '~t'))
dw_header.SetItem(1, 'plant_total_weight', ld_min_plant_weight)

ld_available = Double(lu_strings.nf_GetToken(ls_header_output, '~t'))
dw_detail.object.avail.expression = String(ld_available)

For li_counter = 1 to dw_detail.RowCount()
	ls_exclude = dw_detail.GetItemString(li_counter,"type_of_sale")
	If ls_exclude = "A" or & 
		   ls_exclude = "I" or &
		   ls_exclude = "E" or &
		   ls_exclude = "T" or &
		   ls_exclude = "C" or &
		   ls_exclude = "G" or &
		   ls_exclude = "N" or &
		   ls_exclude = "B" Then
		dw_detail.SetItem(li_counter,"exclude","Y")
		dw_detail.SetItem(li_counter,'prorated_qty', &
				dw_detail.GetItemNumber(li_counter, 'quantity'))
	Else
		dw_detail.SetItem(li_counter,"exclude","N")
	End If
Next


ls_autocalculate = dw_header.GetItemString(1,"autocalculate")
If ls_autocalculate = "Y" Then
	wf_protect_pr_qty('Y')
Else
	wf_protect_pr_qty('N')
End If

If dw_detail.RowCount() > 0 Then
	iw_frame.SetMicroHelp(String(dw_detail.RowCount()) + " records retrieved")
	em_prorate_pct.Text = String(dw_detail.GetItemNumber(1,"prorate_pct") * 100.00)
Else
	iw_frame.SetMicroHelp("0 records retrieved")
End If

If dw_detail.RowCount() > 0 Then
	dw_detail.SetSort("#4 D")
	dw_detail.Sort()
	dw_detail.SetRedraw(true)
	dw_detail.GroupCalc()
End If

dw_detail.SetRedraw(True)

ls_prorate_zero = dw_header.GetItemString(1,"no_prorate")
This.Post wf_autocalculate(ls_autocalculate, ls_prorate_zero)

//get prorate to zero
//dw_header.AcceptText()
//ls_prorate_zero = dw_header.GetItemString(1,"no_prorate")
//This.Post wf_noprorate(ls_prorate_zero,0)

return true
end function

public subroutine wf_new_load_weight_check (long al_row);Integer	li_old_quantity, li_new_quantity
			
Long		ll_sub, ll_rowcount			

Decimal	{2}	ldc_pro_line_weight, ldc_pro_total_weight, ldc_old_pro_line_weight


// set the prorated line weight for the changed line,  this is the weight to subtract from the total load weight.
li_old_quantity = dw_detail.GetItemNumber(al_row, "quantity")
li_new_quantity = dw_detail.GetItemNumber(al_row, "prorated_qty")
ldc_old_pro_line_weight = dw_detail.GetItemNumber(al_row, "pro_line_weight")

If li_new_quantity < li_old_quantity Then
	ldc_pro_line_weight  = (li_old_quantity - li_new_quantity)  * dw_detail.GetItemNumber(al_row, "new_weight")
	dw_detail.SetItem(al_row, "pro_line_weight", ldc_pro_line_weight)
else
	dw_detail.SetItem(al_row, "pro_line_weight", 0)
End If
dw_detail.SetItem(al_row, "pro_total_weight", dw_detail.GetItemNumber(al_row, "total_load_weight"))

ll_rowcount = dw_detail.RowCount()


//loop through calculating new load weight - prorated weight, same primary order may appear more than once,

If dw_detail.GetItemString(al_row, "primary_order_no") > '     '  Then
	if dw_detail.GetItemNumber(al_row, "pro_line_weight") > 0 Then
		ldc_pro_total_weight = dw_detail.GetItemNumber(al_row, "total_load_weight")
		For ll_sub = 1 to ll_rowcount
			If (dw_detail.GetItemString(ll_sub, "primary_order_no") = dw_detail.GetItemString(al_row, "primary_order_no")) AND &
					(dw_detail.GetItemNumber(ll_sub, "pro_line_weight") > 0) Then
				ldc_pro_line_weight  = dw_detail.GetItemNumber(ll_sub, "pro_line_weight")
				ldc_pro_total_weight = ldc_pro_total_weight - ldc_pro_line_weight
			End If
		Next	
	//set the pro_total_weight for each detail line with matching primary_order_no		
		For ll_sub = 1 to ll_rowcount
			If (dw_detail.GetItemString(ll_sub, "primary_order_no") = dw_detail.GetItemString(al_row, "primary_order_no")) Then
				if Isnull(dw_detail.GetItemNumber(ll_sub, "pro_total_weight")) OR (dw_detail.GetItemNumber(ll_sub, "pro_total_weight") <> ldc_pro_total_weight) then
					dw_detail.SetItem(ll_sub, "pro_total_weight", ldc_pro_total_weight)
					if ll_sub <> al_row then
//						MessageBox('Post issued', 'Post Issued for ' + string(ll_sub))
						Post wf_CheckLoadWeight(ll_sub)
					End If
				End If
			End If
		Next
	Else
		if ldc_old_pro_line_weight > 0 then
			ldc_pro_total_weight  = dw_detail.GetItemNumber(al_row, "pro_total_weight") + ldc_old_pro_line_weight
			dw_detail.SetItem(al_row, "pro_total_weight", ldc_pro_total_weight)
			For ll_sub = 1 to ll_rowcount
				If (dw_detail.GetItemString(ll_sub, "primary_order_no") = dw_detail.GetItemString(al_row, "primary_order_no")) Then
					if IsNull(dw_detail.GetItemNumber(ll_sub, "pro_total_weight")) OR (dw_detail.GetItemNumber(ll_sub, "pro_total_weight") <> ldc_pro_total_weight) Then
						dw_detail.SetItem(ll_sub, "pro_total_weight", ldc_pro_total_weight)
						if ll_sub <> al_row then
//							MessageBox('Post issued', 'Post Issued for ' + string(ll_sub))
							Post wf_CheckLoadWeight(ll_sub)
						end if
					end if
				End If
			Next			
		End If
	End If		
	
End If


end subroutine

public subroutine wf_autocalculate (string as_autocalculate, string as_no_prorate);String	ls_exclude 

Integer	li_counter, &
			li_last_included = 0, &
			li_ZeroRow, &
			li_GoodRow, &
			li_start_pos

Long		ll_quantity, &
			ll_prorate_quantity, &
			ll_prorate_pct, &
			ll_prorate_qty, &
			ll_leftover, &
			ll_temp
			
Real		lr_prorate_pct

String		ls_find_zero_row, ls_find_good_row


If dw_detail.RowCount() < 1 Then return

If as_autocalculate = 'Y' Then
	em_prorate_pct.text = String(dw_detail.GetItemNumber(1,"prorate_pct") &
			* 100.00)
End IF

//dw_detail.SetSort("#1 A")
//dw_detail.Sort()
//dw_detail.SetRedraw(False)
//dw_detail.GroupCalc()

lr_prorate_pct = Real(em_prorate_pct.text) / 100.00

For li_counter = 1 to dw_detail.RowCount()
	ll_quantity = dw_detail.GetItemNumber(li_counter,"quantity")
	If dw_detail.GetRow() = li_counter And dw_detail.GetColumnName() = 'exclude' Then
		ls_exclude = dw_detail.GetText()
	Else
		ls_exclude = dw_detail.GetItemString(li_counter,"exclude")
	End If
	If ls_exclude = "N" Then
		ll_prorate_quantity = long(ll_quantity * (1 - lr_prorate_pct))
		If ll_prorate_quantity < 0 Then 
				ll_prorate_quantity = 0
		End If
		dw_detail.SetItem(li_counter,"prorated_qty", ll_prorate_quantity)
		li_last_included = li_counter
	Else
		dw_detail.SetItem(li_counter,"prorated_qty", ll_quantity)
	End If
	wf_checkloadweight(li_counter)
Next

li_counter --

If as_autocalculate = 'Y' Then
	ll_leftover = dw_detail.GetItemNumber(1,"avail") - &
			dw_detail.GetItemNumber(1, 'total_prorated_qty')
	If ll_leftover < 0 Then
		MessageBox("Quantity Overflow","Cannot autocalculate prorated quantities when " + &
							"excluded quantities are greater than the available amount.")
		wf_protect_pr_qty('N')
		dw_header.SetItem(1, 'autocalculate','N')
//		dw_header.SetItem(1,'no_prorate','N')
		Return
	End IF
End If

// Now make sure that the total prorated quantity matches available (might not due to rounding)
Do while ll_leftover > 0 and li_counter > 0
	ll_quantity = dw_detail.GetItemNumber(li_counter,"quantity")
	ll_prorate_qty = dw_detail.GetItemNumber(li_counter, "prorated_qty")

	If dw_detail.GetItemString(li_counter,"exclude") = "N" and &
			ll_quantity > 0 and &
			ll_prorate_qty < ll_quantity Then
		dw_detail.SetItem(li_counter,"prorated_qty", ll_prorate_qty + 1)
		ll_leftover --
		wf_checkloadweight(li_counter)
	End If
	li_counter --
Loop

If as_no_prorate = 'Y' Then
	ls_find_zero_row = "prorated_qty = 0 and exclude = '" + "N" + "'"
	ls_find_good_row = "prorated_qty > 1 and exclude = '" + "N" + "'"
	For li_start_pos = 1 to dw_detail.RowCount()
		li_ZeroRow = dw_detail.Find(ls_find_zero_row,1,dw_detail.RowCount())
		li_GoodRow = dw_detail.Find(ls_find_good_row,li_start_pos,dw_detail.RowCount())		
		Do While (li_ZeroRow) > 0 and (li_GoodRow > 0)
			ll_prorate_qty = dw_detail.GetItemNumber(li_GoodRow, "prorated_qty")
			dw_detail.SetItem(li_GoodRow, "prorated_qty", ll_prorate_qty -1)
			wf_checkloadweight(li_GoodRow)
			dw_detail.SetItem(li_ZeroRow, "prorated_qty", 1)
			wf_checkloadweight(li_ZeroRow)
			li_ZeroRow = dw_detail.Find(ls_find_zero_row,1,dw_detail.RowCount())		
			li_GoodRow = dw_detail.Find(ls_find_good_row, li_start_pos, dw_detail.RowCount())
			li_start_pos = li_GoodRow
		Loop		
	Next
End If

wf_calc_prorate_percent()

//dw_detail.SetSort("#4 D")
//dw_detail.Sort()
//dw_detail.SetRedraw(true)
//dw_detail.GroupCalc()

return

end subroutine

public subroutine wf_new_invt_calc ();Integer		li_order_count, &
				li_inv_count, &
				li_order_sub, &
				li_inv_sub, &
				li_order_sub2, &
				li_total_applied_qty, &
				li_total_ship, &
				li_included
				
String			ls_ord_find_string, &
				ls_temp

Real			lr_percent
				
// recalculate based on auto calculate rules

wf_autocalculate("Y", dw_header.GetItemString(1,"no_prorate"))

ids_order_table = Create DataStore
ids_order_table.DataObject = "d_prorate_order_table"
ids_order_table.Reset()

ids_applied_table = Create DataStore
ids_applied_table.DataObject = "d_prorate_applied_table"
ids_applied_table.Reset()
				
li_order_count = dw_detail.RowCount() 				

For li_order_sub = 1 to li_order_count
	li_order_sub2 = ids_order_table.InsertRow(0)
	ids_order_table.SetItem(li_order_sub2, "order_number", dw_detail.GetItemString(li_order_sub, "order_num"))
	ids_order_table.SetItem(li_order_sub2, "order_line", dw_detail.GetItemString(li_order_sub, "line_number"))
	ids_order_table.SetItem(li_order_sub2, "exclude", dw_detail.GetItemString(li_order_sub, "exclude"))
	ids_order_table.SetItem(li_order_sub2, "sched_qty", dw_detail.GetItemNumber(li_order_sub, "quantity"))
	ids_order_table.SetItem(li_order_sub2, "prorate_qty", dw_detail.GetItemNumber(li_order_sub, "prorated_qty"))
	ids_order_table.SetItem(li_order_sub2, "from_prod_date", dw_detail.GetItemDate(li_order_sub, "from_prod_date"))
	ids_order_table.SetItem(li_order_sub2, "to_prod_date", dw_detail.GetItemDate(li_order_sub, "to_prod_date"))
	ids_order_table.SetItem(li_order_sub2, "applied_qty", 0)
	ids_order_table.SetItem(li_order_sub2, "remaining_qty", dw_detail.GetItemNumber(li_order_sub, "prorated_qty"))
	ids_order_table.SetItem(li_order_sub2, "ord_flag", "N")
Next
li_order_count = ids_order_table.RowCount()

ids_order_table.SetSort("from_prod_date as, exclude ds to_prod_date ds prorate_qty ds")
ids_order_table.Sort()

ids_inv_table = Create DataStore
ids_inv_table.DataObject = "d_prorate_inv_table"

ids_inv_table.Reset()
ids_inv_table.ImportString(is_output_invt_string)

li_inv_count = ids_inv_table.RowCount()

For li_inv_sub = 1 to li_inv_count
	ids_inv_table.SetItem(li_inv_sub, "inv_applied_qty", 0)
	ids_inv_table.SetItem(li_inv_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty"))
Next

ids_inv_table.SetSort("inv_date as")
ids_inv_table.Sort()

//MessageBox("Order Table - Initial", ids_order_table.Describe("DataWindow.Data"))
//MessageBox("Invr Table - Initial", ids_inv_table.Describe("DataWindow.Data"))

//  This function loops through the inventory amounts, oldest days first, applying to excluded orders first.

wf_apply_invt_to_excludes()

For li_order_sub = 1 to li_order_count
	If (ids_order_table.GetItemString(li_order_sub, "exclude") = "Y") and (ids_order_table.GetItemNumber(li_order_sub, "remaining_qty") > 0) Then
		MessageBox("Excluded Order Overflow", "cannot prorate by production date and satisfy excluded order")
		wf_protect_pr_qty('N')
		dw_header.SetItem(1, "autocalculate", "N")
		Return
	End If
Next

//  This loop loops through the inventory amounts, oldest days first, applying to non-excluded

wf_apply_invt_to_non_excludes()

//li_applied_count = ids_applied_table.RowCount()

ids_order_table.SetSort("exclude ds ord_remaining_qty ds")
ids_order_table.Sort()

//MessageBox("Order Table - After 1st Pass - Sorted", ids_order_table.Describe("DataWindow.Data"))
//MessageBox("Inv Table - After 1st Pass", ids_inv_table.Describe("DataWindow.Data"))

ids_applied_table.SetSort("order_number as order_line as inv_date as")
ids_applied_table.Sort()

//MessageBox("Applied Table - After 1st Pass - Sorted", ids_applied_table.Describe("DataWindow.Data"))

//  This function loops through any order shortages, finding applied inventory that the short order can use
wf_reallocate_shortages()

ids_order_table.SetSort("ord_remaining_qty ds")
ids_order_table.Sort()

//MessageBox("Order Table - After 2nd Pass", ids_order_table.Describe("DataWindow.Data"))
//MessageBox("Invr Table - After 2nd Pass", ids_inv_table.Describe("DataWindow.Data"))

//  This function loops through any remaining inventory amounts, applying it where it can.
wf_apply_excess_inventory()

//  The previous function can still leave excess inventory, applying to any order that can accept it.
wf_apply_excess_inventory_any()

//MessageBox("Order Table - After 3rd pass", ids_order_table.Describe("DataWindow.Data"))
//MessageBox("Invr Table- After 3rd Pass", ids_inv_table.Describe("DataWindow.Data"))	

For li_order_sub = 1 to li_order_count
	If (ids_order_table.GetItemString(li_order_sub, "exclude") = "Y") and (ids_order_table.GetItemNumber(li_order_sub, "remaining_qty") > 0) Then
		MessageBox("Excluded Order Overflow", "cannot prorate by production date and satisfy excluded order")
		wf_protect_pr_qty('N')
		dw_header.SetItem(1, "autocalculate", "N")
		Return
	End If
Next

li_total_applied_qty = 0
For li_order_sub = 1 to li_order_count
	li_total_applied_qty += ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty")
	ls_ord_find_string = "order_num = '" + ids_order_table.GetItemString(li_order_sub, "order_number") + "'" 
	ls_ord_find_string += " AND line_number = '" + ids_order_table.GetItemString(li_order_sub, "order_line") + "'" 
	li_order_sub2 = dw_detail.Find(ls_ord_find_string, 1, dw_detail.RowCount())
	If li_order_sub2 > 0 Then
		dw_detail.SetItem(li_order_sub2 ,"prorated_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty"))
		wf_checkloadweight(li_order_sub2)
	Else	
		MessageBox ("Error finding order number", "Find String = " + ls_ord_find_string)
	End If	
Next

li_total_ship = dw_detail.Object.total_ship[1]
li_included = dw_detail.Object.included[1]				
				
if (li_total_ship - li_total_applied_qty) > 0 Then
	lr_percent = (li_total_ship - li_total_applied_qty) * 100 / li_included
else
	lr_percent = 0
End If				
				
ls_temp = String(lr_percent, "##0.00") 

em_prorate_pct.Text = ls_temp		
				
				
end subroutine

public subroutine wf_calc_prorate_percent ();Integer	li_total_ship, &			
			li_avail, &
			li_included
	
Real		lr_percent			

String		ls_temp

			
li_total_ship = dw_detail.Object.total_ship[1]
li_avail = Integer(dw_detail.Object.avail.Expression)
li_included = dw_detail.Object.included[1]

if li_included = 0 Then
	lr_percent = 0
Else
	if (li_total_ship - li_avail) > 0 Then
		lr_percent = (li_total_ship - li_avail) * 100 / li_included
	else
		lr_percent = 0
	End If
End If

ls_temp = String(lr_percent, "##0.00") 

em_prorate_pct.Text = ls_temp
end subroutine

public subroutine wf_apply_invt_to_excludes ();Integer		li_inv_sub, &
				li_order_sub, &
				li_applied_sub, &
				li_inv_count, &
				li_order_count, &
				li_ord_new_start_find, &
				li_qty_to_apply
				
String			ls_ord_find_string				
				
li_inv_count = ids_inv_table.RowCount()
li_order_count = ids_order_table.RowCount()



For li_inv_sub = 1 to li_inv_count
	ls_ord_find_string = "from_prod_date <= date('" + string(ids_inv_table.GetItemDate(li_inv_sub, "inv_date"),"yyyy-mm-dd") + "')"
	ls_ord_find_string += " AND to_prod_date >= date('" + string(ids_inv_table.GetItemDate(li_inv_sub, "inv_date"),"yyyy-mm-dd") + "')"
	ls_ord_find_string += " AND ord_remaining_qty > 0 and exclude = 'Y'"
	
	li_order_sub = ids_order_table.Find(ls_ord_find_string, 1, li_order_count)
	Do While (li_order_sub > 0) 
		li_ord_new_start_find = li_order_sub
		li_ord_new_start_find ++

		If ids_inv_table.GetItemNumber( li_inv_sub, "inv_remaining_qty") > ids_order_table.GetItemNumber(li_order_sub,  "ord_remaining_qty") Then
			li_qty_to_apply = ids_order_table.GetItemNumber(li_order_sub,  "ord_remaining_qty")
			ids_order_table.SetItem(li_order_sub, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_qty_to_apply)
			ids_order_table.SetItem(li_order_sub, "ord_remaining_qty", 0)
		Else
			li_qty_to_apply = ids_inv_table.GetItemNumber( li_inv_sub, "inv_remaining_qty") 
			ids_order_table.SetItem(li_order_sub, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") +li_qty_to_apply)
			ids_order_table.SetItem(li_order_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty") - li_qty_to_apply)
		End If
		
//		ids_inv_table.SetItem(li_inv_sub, "inv_applied_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_applied_qty") + li_qty_to_apply)
		ids_inv_table.SetItem(li_inv_sub, "inv_total_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty") - li_qty_to_apply)
		ids_inv_table.SetItem(li_inv_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") - li_qty_to_apply)		
		
		li_applied_sub = ids_applied_table.InsertRow(0)
		ids_applied_table.SetItem(li_applied_sub, "inv_date", ids_inv_table.GetItemDate(li_inv_sub, "inv_date"))
		ids_applied_table.SetItem(li_applied_sub, "inv_total_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty"))
		ids_applied_table.SetItem(li_applied_sub, "order_number", ids_order_table.GetItemString(li_order_sub, "order_number"))
		ids_applied_table.SetItem(li_applied_sub, "order_line", ids_order_table.GetItemString(li_order_sub, "order_line"))
		ids_applied_table.SetItem(li_applied_sub, "exclude", ids_order_table.GetItemString(li_order_sub, "exclude"))
		ids_applied_table.SetItem(li_applied_sub, "sched_qty", ids_order_table.GetItemNumber(li_order_sub, "sched_qty"))
		ids_applied_table.SetItem(li_applied_sub, "prorate_qty", ids_order_table.GetItemNumber(li_order_sub, "prorate_qty"))
		ids_applied_table.SetItem(li_applied_sub, "inv_applied_qty", li_qty_to_apply)
		ids_applied_table.SetItem(li_applied_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty"))
		ids_applied_table.SetItem(li_applied_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty"))

		If li_ord_new_start_find > li_order_count Then
			li_order_sub = 0
		Else	
			If ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") = 0 Then
				li_order_sub = 0
			Else
				li_order_sub = ids_order_table.Find(ls_ord_find_string, li_ord_new_start_find, li_order_count)
			End If
		End If
	Loop		
Next




end subroutine

public subroutine wf_apply_invt_to_non_excludes ();Integer	li_inv_sub, &
			li_order_sub, &
			li_applied_sub, &
			li_inv_count, &
			li_order_count, &
			li_total_remaining_qty, &
			li_ord_new_start_find, &
			li_added_qty, &
			li_qty_to_apply
			
			
String		ls_ord_find_string			

Decimal{4}	ldc_qty_to_apply	
			
li_inv_count = ids_inv_table.RowCount()		
li_order_count = ids_order_table.RowCount()		

For li_inv_sub = 1 to li_inv_count
	ls_ord_find_string = "from_prod_date <= date('" + string(ids_inv_table.GetItemDate(li_inv_sub, "inv_date"),"yyyy-mm-dd") + "')"
	ls_ord_find_string += " AND to_prod_date >= date('" + string(ids_inv_table.GetItemDate(li_inv_sub, "inv_date"),"yyyy-mm-dd") + "')"
	ls_ord_find_string += " AND ord_remaining_qty > 0"
	li_total_remaining_qty = 0

	li_order_sub = ids_order_table.Find(ls_ord_find_string, 1, li_order_count)
	If li_order_sub > 0 Then
		Do While (li_order_sub > 0) 
			li_ord_new_start_find = li_order_sub
			li_ord_new_start_find ++
			
			li_total_remaining_qty += ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty")
			
			If li_ord_new_start_find > li_order_count Then
				li_order_sub = 0
			Else	
				If ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") = 0 Then
					li_order_sub = 0
				Else
					li_order_sub = ids_order_table.Find(ls_ord_find_string, li_ord_new_start_find, li_order_count)
				End If
			End If		
		Loop

		If li_total_remaining_qty > 0 Then
			li_added_qty = 0
			li_order_sub = ids_order_table.Find(ls_ord_find_string, 1, li_order_count)
			Do While (li_order_sub > 0)
				li_ord_new_start_find = li_order_sub
				li_ord_new_start_find ++
			
				ldc_qty_to_apply = (ids_order_table.GetItemNumber(li_order_sub,  "ord_remaining_qty") * ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty")) / li_total_remaining_qty
				li_qty_to_apply = Round(ldc_qty_to_apply,0)
				
				if li_qty_to_apply > ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty") - li_added_qty then
					li_qty_to_apply = ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty") - li_added_qty
				end if
				
				If li_qty_to_apply > ids_order_table.GetItemNumber(li_order_sub,  "ord_remaining_qty") Then
					li_qty_to_apply =  ids_order_table.GetItemNumber(li_order_sub,  "ord_remaining_qty")
				End If
				
				ids_order_table.SetItem(li_order_sub, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") +li_qty_to_apply)
				ids_order_table.SetItem(li_order_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty") - li_qty_to_apply)
				ids_inv_table.SetItem(li_inv_sub, "inv_applied_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_applied_qty") + li_qty_to_apply)
				ids_inv_table.SetItem(li_inv_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") - li_qty_to_apply)		
				
				li_applied_sub = ids_applied_table.InsertRow(0)
				ids_applied_table.SetItem(li_applied_sub, "inv_date", ids_inv_table.GetItemDate(li_inv_sub, "inv_date"))
				ids_applied_table.SetItem(li_applied_sub, "inv_total_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty"))
				ids_applied_table.SetItem(li_applied_sub, "order_number", ids_order_table.GetItemString(li_order_sub, "order_number"))
				ids_applied_table.SetItem(li_applied_sub, "order_line", ids_order_table.GetItemString(li_order_sub, "order_line"))
				ids_applied_table.SetItem(li_applied_sub, "exclude", ids_order_table.GetItemString(li_order_sub, "exclude"))
				ids_applied_table.SetItem(li_applied_sub, "sched_qty", ids_order_table.GetItemNumber(li_order_sub, "sched_qty"))
				ids_applied_table.SetItem(li_applied_sub, "prorate_qty", ids_order_table.GetItemNumber(li_order_sub, "prorate_qty"))
				ids_applied_table.SetItem(li_applied_sub, "inv_applied_qty", li_qty_to_apply)
				ids_applied_table.SetItem(li_applied_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty"))
				ids_applied_table.SetItem(li_applied_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty"))
				
				li_added_qty += li_qty_to_apply
			
				If li_ord_new_start_find > li_order_count Then
					li_order_sub = 0
				Else	
					If ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") = 0 Then
						li_order_sub = 0
					Else
						li_order_sub = ids_order_table.Find(ls_ord_find_string, li_ord_new_start_find, li_order_count)
					End If
				End If
			Loop
		End If		
	End If		
Next

end subroutine

public subroutine wf_reallocate_shortages ();Integer		li_inv_sub, &
				li_inv_count, &
				li_applied_sub, &
				li_order_sub, &
				li_order_sub2, &
				li_order_count, &
				li_applied_count, &
				li_order_short_qty, &
				li_subtracted_qty, &
				li_total_avail_invt, &
				li_inv_new_start_find, &
				li_order_applied_invt, &
				li_applied_new_start_find, &
				li_subtract_qty, &
				li_additional_qty
				
String			ls_inv_find_string, &
				ls_ord_find_string, &
				ls_applied_find_string, &
				ls_found_order, &
				ls_found_order_line
				
Decimal{4}	ldc_subtract_qty					
				
li_inv_count = ids_inv_table.RowCount()
li_order_count = ids_order_table.RowCount()
li_applied_count = ids_applied_table.RowCount()

For li_order_sub = 1 to li_order_count
	If (ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty") > 0) and (ids_order_table.GetItemString(li_order_sub, "ord_flag") = 'N') Then
		li_order_short_qty = ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty")
		li_subtracted_qty = 0
//     total all available inventory that could satisfy the shortage based on the short order's to/from production dates		
		li_total_avail_invt = 0
		ls_inv_find_string = "inv_date >= date('" + string(ids_order_table.GetItemDate(li_order_sub, "from_prod_date"),"yyyy-mm-dd") + "')"
		ls_inv_find_string += " AND inv_date <= date('" + string(ids_order_table.GetItemDate(li_order_sub, "to_prod_date"),"yyyy-mm-dd") + "')"
		
		li_inv_sub = ids_inv_table.Find(ls_inv_find_string,1, li_inv_count)
		
		Do While (li_inv_sub > 0)
			li_inv_new_start_find = li_inv_sub
			li_inv_new_start_find ++
			li_total_avail_invt += ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty")
			If li_inv_new_start_find > li_inv_count Then
				li_inv_sub = 0
			Else
				li_inv_sub = ids_inv_table.Find(ls_inv_find_string, li_inv_new_start_find, li_inv_count) 
			End If
		Loop	

		IF li_total_avail_invt > 0 Then

			ls_applied_find_string = "inv_date >= date('" + string(ids_order_table.GetItemDate(li_order_sub, "from_prod_date"),"yyyy-mm-dd") + "')"
			ls_applied_find_string += " AND inv_date <= date('" + string(ids_order_table.GetItemDate(li_order_sub, "to_prod_date"),"yyyy-mm-dd") + "')"
			ls_applied_find_string += " AND order_number <> '"  + ids_order_table.GetItemString(li_order_sub, "order_number") + "'"
			ls_applied_find_string += " AND exclude <> 'Y'" 			

			li_applied_sub = ids_applied_table.Find(ls_applied_find_string, 1, li_applied_count)

			Do While (li_applied_sub > 0)
				ls_found_order = ids_applied_table.GetItemString(li_applied_sub, "order_number")
				ls_found_order_line = ids_applied_table.GetItemString(li_applied_sub, "order_line")
				li_order_applied_invt = 0
								
				Do While (li_applied_sub > 0)
					li_applied_new_start_find = li_applied_sub
					li_applied_new_start_find ++
					
					li_order_applied_invt += ids_applied_table.GetItemNumber(li_applied_sub, "inv_applied_qty")
					
					ls_applied_find_string = "inv_date >= date('" + string(ids_order_table.GetItemDate(li_order_sub, "from_prod_date"),"yyyy-mm-dd") + "')"
					ls_applied_find_string += " AND inv_date <= date('" + string(ids_order_table.GetItemDate(li_order_sub, "to_prod_date"),"yyyy-mm-dd") + "')"
					ls_applied_find_string += " AND order_number = '"  + ls_found_order + "'"
					ls_applied_find_string += " AND order_line = '"  + ls_found_order_line + "'"
					
					If li_applied_new_start_find > li_applied_count Then
						li_applied_sub = 0
					Else
						li_applied_sub = ids_applied_table.Find(ls_applied_find_string, li_applied_new_start_find, li_applied_count)
					End If
				Loop
				
				ldc_subtract_qty = (li_order_applied_invt * li_order_short_qty) / li_total_avail_invt
				li_subtract_qty = Round(ldc_subtract_qty, 0)
				If li_subtract_qty > (li_order_short_qty - li_subtracted_qty) Then
					li_subtract_qty = li_order_short_qty - li_subtracted_qty	
				End if						
				
				ls_ord_find_string = "order_number =  '" + ls_found_order + "'"
				ls_ord_find_string += "AND order_line =  '" + ls_found_order_line + "'"
				//ls_ord_find_string += "AND ord_flag = '" + "N" + "'"
				li_order_sub2 = ids_order_table.Find(ls_ord_find_string, 1, li_order_count)		
				
				ids_order_table.SetItem(li_order_sub2, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub2, "ord_applied_qty") -  li_subtract_qty)
				ids_order_table.SetItem(li_order_sub2, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub2, "ord_remaining_qty") + li_subtract_qty)	
				ids_order_table.SetItem(li_order_sub2, "ord_flag", "Y")
				
				li_subtracted_qty += li_subtract_qty
				
				ls_applied_find_string = "inv_date >= date('" + string(ids_order_table.GetItemDate(li_order_sub, "from_prod_date"),"yyyy-mm-dd") + "')"
				ls_applied_find_string += " AND inv_date <= date('" + string(ids_order_table.GetItemDate(li_order_sub, "to_prod_date"),"yyyy-mm-dd") + "')"
				ls_applied_find_string += " AND order_number <> '"  + ids_order_table.GetItemString(li_order_sub, "order_number") + "'"
				ls_applied_find_string += " AND exclude <> 'Y'" 				
				
				If li_applied_new_start_find > li_applied_count Then
					li_applied_sub = 0
				Else
					li_applied_sub = ids_applied_table.Find(ls_applied_find_string, li_applied_new_start_find, li_applied_count)
				End If				
				
			Loop				
				
			li_additional_qty =  li_subtracted_qty	
			 
			ids_order_table.SetItem(li_order_sub, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") +  li_additional_qty)
			ids_order_table.SetItem(li_order_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty") + li_additional_qty)	
			ids_order_table.SetItem(li_order_sub, "ord_flag", "Y")			 
				
		End If	
	End If
Next
end subroutine

public subroutine wf_apply_excess_inventory ();Integer	li_inv_sub, &
			li_order_sub, &
			li_applied_sub, &
			li_inv_count, &
			li_order_count, &
			li_applied_count, &
			li_excess_qty, &
			li_added_qty, &
			li_applied_new_start_find, &
			li_additional_qty
			
String		ls_applied_find_string, &
			ls_found_order, &
			ls_found_order_line, &
			ls_ord_find_string
			
Decimal{4}		ldc_additional_qty

li_inv_count = ids_inv_table.RowCount()
li_order_count = ids_order_table.RowCount()
li_applied_count = ids_applied_table.RowCount()

For li_inv_sub = 1 to li_inv_count
	If (ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") > 0) Then
		li_excess_qty = ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty")
		li_added_qty = 0
	
		ls_applied_find_string = "inv_date = date('" + string(ids_inv_table.GetItemDate(li_inv_sub, "inv_date"),"yyyy-mm-dd") + "')"
		ls_applied_find_string += " AND exclude <> 'Y'" 	
		li_applied_sub = ids_applied_table.Find(ls_applied_find_string, 1, li_applied_count)
	
		If li_applied_sub > 0 Then
			Do While (li_applied_sub > 0) 
				li_applied_new_start_find = li_applied_sub
				li_applied_new_start_find ++
				
				ls_found_order = ids_applied_table.GetItemString(li_applied_sub, "order_number")
				ls_found_order_line = ids_applied_table.GetItemString(li_applied_sub, "order_line")
		
				ldc_additional_qty =  (ids_applied_table.GetItemNumber(li_applied_sub, "inv_applied_qty") * li_excess_qty) / ids_inv_table.GetItemNumber(li_inv_sub, "inv_total_qty") 
				li_additional_qty  = Round(ldc_additional_qty, 0)
						
				If li_additional_qty > (li_excess_qty - li_added_qty)  Then
					li_additional_qty = li_excess_qty - li_added_qty	
				End If
				
				ls_ord_find_string = "order_number =  '" + ls_found_order + "'"
				ls_ord_find_string += "AND order_line =  '" + ls_found_order_line + "'"
				li_order_sub = ids_order_table.Find(ls_ord_find_string, 1, li_order_count)
				
				If ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_additional_qty > ids_order_table.GetItemNumber(li_order_sub, "sched_qty") Then
					li_additional_qty = ids_order_table.GetItemNumber(li_order_sub, "sched_qty") - ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty")
				End If
				
				ids_order_table.SetItem(li_order_sub, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_additional_qty)
				ids_order_table.SetItem(li_order_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty") - li_additional_qty)	
	
				ids_inv_table.SetItem(li_inv_sub, "inv_applied_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_applied_qty") + li_additional_qty)
				ids_inv_table.SetItem(li_inv_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") - li_additional_qty)		
					
				li_added_qty += li_additional_qty
				
				If li_applied_new_start_find > li_applied_count Then
					li_applied_sub = 0
				Else
					li_applied_sub = ids_applied_table.Find(ls_applied_find_string, li_applied_new_start_find, li_applied_count)
				End If			
			Loop			
	
			If li_applied_sub = 0 Then
				If  li_excess_qty - li_added_qty	> 0 Then
					li_additional_qty = li_excess_qty - li_added_qty
					
					If ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_additional_qty > ids_order_table.GetItemNumber(li_order_sub, "sched_qty") Then
						li_additional_qty = ids_order_table.GetItemNumber(li_order_sub, "sched_qty") - ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty")
					End If				
					
					ids_order_table.SetItem(li_order_sub, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_additional_qty)
					ids_order_table.SetItem(li_order_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty") - li_additional_qty)	
		
					ids_inv_table.SetItem(li_inv_sub, "inv_applied_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_applied_qty") + li_additional_qty)
					ids_inv_table.SetItem(li_inv_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") - li_additional_qty)						
				End If
			End If
		End if
	End If
Next 

end subroutine

public subroutine wf_apply_excess_inventory_any ();Integer		li_inv_sub, &
				li_order_sub, &
				li_applied_sub, &
				li_inv_count, &
				li_order_count, &
				li_applied_count, &
				li_excess_qty, &
				li_added_qty, &
				li_applied_new_start_find, &
				li_additional_qty
				
String			ls_applied_find_string, &
				ls_found_order, &
				ls_found_order_line, &
				ls_ord_find_string
				
li_inv_count = ids_inv_table.RowCount()	
li_order_count = ids_order_table.RowCount()	
li_applied_count = ids_applied_table.RowCount()	

For li_inv_sub = 1 to li_inv_count
	If (ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") > 0) Then
		li_excess_qty = ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty")
		li_added_qty = 0
	
		ls_applied_find_string = "inv_date = date('" + string(ids_inv_table.GetItemDate(li_inv_sub, "inv_date"),"yyyy-mm-dd") + "')"
		ls_applied_find_string += " AND exclude <> 'Y'" 	
		li_applied_sub = ids_applied_table.Find(ls_applied_find_string, 1, li_applied_count)
	
		If li_applied_sub > 0 Then
			Do While (li_applied_sub > 0) 
				li_applied_new_start_find = li_applied_sub
				li_applied_new_start_find ++
				
				ls_found_order = ids_applied_table.GetItemString(li_applied_sub, "order_number")
				ls_found_order_line = ids_applied_table.GetItemString(li_applied_sub, "order_line")
		
				li_additional_qty = li_excess_qty
						
				If li_additional_qty > (li_excess_qty - li_added_qty)  Then
					li_additional_qty = li_excess_qty - li_added_qty	
				End If
				
				ls_ord_find_string = "order_number =  '" + ls_found_order + "'"
				ls_ord_find_string += "AND order_line =  '" + ls_found_order_line + "'"
				li_order_sub = ids_order_table.Find(ls_ord_find_string, 1, li_order_count)
				
				If ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_additional_qty > ids_order_table.GetItemNumber(li_order_sub, "sched_qty") Then
					li_additional_qty = ids_order_table.GetItemNumber(li_order_sub, "sched_qty") - ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty")
				End If
				
				ids_order_table.SetItem(li_order_sub, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_additional_qty)
				ids_order_table.SetItem(li_order_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty") - li_additional_qty)	
	
				ids_inv_table.SetItem(li_inv_sub, "inv_applied_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_applied_qty") + li_additional_qty)
				ids_inv_table.SetItem(li_inv_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") - li_additional_qty)		
					
				li_added_qty += li_additional_qty
				
				If li_applied_new_start_find > li_applied_count Then
					li_applied_sub = 0
				Else
					li_applied_sub = ids_applied_table.Find(ls_applied_find_string, li_applied_new_start_find, li_applied_count)
				End If			
			Loop			
	
			If li_applied_sub = 0 Then
				If  li_excess_qty - li_added_qty	> 0 Then
					li_additional_qty = li_excess_qty - li_added_qty
					
					If ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_additional_qty > ids_order_table.GetItemNumber(li_order_sub, "sched_qty") Then
						li_additional_qty = ids_order_table.GetItemNumber(li_order_sub, "sched_qty") - ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty")
					End If				
					
					ids_order_table.SetItem(li_order_sub, "ord_applied_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_applied_qty") + li_additional_qty)
					ids_order_table.SetItem(li_order_sub, "ord_remaining_qty", ids_order_table.GetItemNumber(li_order_sub, "ord_remaining_qty") - li_additional_qty)	
		
					ids_inv_table.SetItem(li_inv_sub, "inv_applied_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_applied_qty") + li_additional_qty)
					ids_inv_table.SetItem(li_inv_sub, "inv_remaining_qty", ids_inv_table.GetItemNumber(li_inv_sub, "inv_remaining_qty") - li_additional_qty)						
				End If
			End If
		End if
	End If
Next 


end subroutine

on w_prorate.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_fab=create dw_fab
this.dw_plant=create dw_plant
this.dw_header=create dw_header
this.em_prorate_pct=create em_prorate_pct
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_fab
this.Control[iCurrent+3]=this.dw_plant
this.Control[iCurrent+4]=this.dw_header
this.Control[iCurrent+5]=this.em_prorate_pct
this.Control[iCurrent+6]=this.dw_detail
end on

on w_prorate.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_fab)
destroy(this.dw_plant)
destroy(this.dw_header)
destroy(this.em_prorate_pct)
destroy(this.dw_detail)
end on

event open;call super::open;String	ls_inquire

ls_inquire = Message.StringParm

Long ll_row

dw_header.Reset()
ll_row = dw_header.InsertRow(0)
dw_header.SetItem(ll_row,"blocked","Y")
dw_header.SetItem(ll_row,"unblocked","Y")
dw_header.SetItem(ll_row,"autocalculate","Y")
dw_header.SetItem(ll_row,"no_prorate","Y")
dw_header.SetItem(ll_row, 'ship_date', Today())
//


If iw_frame.iu_string.nf_IsEmpty(ls_inquire) Or &
		ls_inquire = 'w_prorate' then return

dw_plant.uf_set_plant_code(iw_frame.iu_string.nf_gettoken(ls_inquire,"~t"))
// product state ibdkdld 10/02/02
//dw_product.uf_setproductcode(iw_frame.iu_string.nf_gettoken(ls_inquire,"~t"))
//dw_product.uf_setproductdesc(iw_frame.iu_string.nf_gettoken(ls_inquire,"~t"))
dw_fab.uf_set_product_code(iw_frame.iu_string.nf_gettoken(ls_inquire,"~t"))
dw_fab.uf_set_product_desc(iw_frame.iu_string.nf_gettoken(ls_inquire,"~t"))
dw_header.SetItem(ll_row,"ship_date",Date(iw_frame.iu_string.nf_gettoken(ls_inquire,"~t")))
dw_fab.uf_set_product_state(iw_frame.iu_string.nf_gettoken(ls_inquire,"~t"))
dw_fab.uf_set_product_status(iw_frame.iu_string.nf_gettoken(ls_inquire,"~t"))

//**IBDKEEM**11/06/0/2002
string ls_temp 
ls_temp = iw_frame.iu_string.nf_gettoken(ls_inquire,"~t")
if len(ls_temp )> 0 then
	dw_fab.uf_set_product_state_desc(ls_temp)
	ls_temp = iw_frame.iu_string.nf_gettoken(ls_inquire,"~t")
	dw_fab.uf_set_product_status_desc(ls_temp)
end if

ib_no_inquire = True

end event

event ue_postopen;call super::ue_postopen;iu_pas203 = Create u_pas203
iu_ws_pas5 = Create u_ws_pas5
If Message.ReturnValue = -1 Then Close(This)

This.wf_retrieve()
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
 
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')

end event

event close;call super::close;If IsValid(iu_pas203) Then
	Destroy iu_pas203
End If
end event

event ue_fileprint;call super::ue_fileprint;dw_detail.Print()
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event resize;call super::resize;constant integer li_x		= 20
constant integer li_y		= 416

constant integer li_em_y   = 95
  
dw_detail.width	= newwidth - li_x
dw_detail.height	= newheight - li_y

em_prorate_pct.y	= newheight - li_em_y

em_prorate_pct.bringtotop = true
end event

type cb_1 from commandbutton within w_prorate
integer x = 2249
integer y = 268
integer width = 654
integer height = 92
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Prorate by Production Dates"
end type

event clicked;wf_new_invt_calc()
end event

type dw_fab from u_fab_product_code within w_prorate
integer y = 112
integer width = 1509
integer taborder = 20
end type

event constructor;call super::constructor;this.uf_enable( False)
end event

type dw_plant from u_plant within w_prorate
integer x = 178
integer y = 32
integer width = 1431
integer taborder = 0
end type

event constructor;call super::constructor;ib_updateable = False
This.Disable()

end event

type dw_header from u_base_dw_ext within w_prorate
integer x = 1627
integer y = 4
integer width = 1280
integer height = 372
integer taborder = 10
string dataobject = "d_prorate_header"
boolean border = false
borderstyle borderstyle = styleshadowbox!
boolean ib_updateable = true
end type

event constructor;call super::constructor;ib_updateable = False
This.Modify("ship_date.Background.Color = '67108864' ship_date.Protect='1'")

end event

event itemchanged;call super::itemchanged;Real		lr_total_excluded
long  		li_rtn
String		ls_prorate_zero

Choose Case String(dwo.Name)
	Case "ship_date"
		If Date(data) < Date(Today()) Then
			iw_frame.SetMicroHelp("Ship Date must be today or later")
			return 1
		End If
	Case "blocked" 
		IF dw_header.GetItemString(1, 'unblocked') = 'N' and data = 'N' Then
			iw_frame.SetMicroHelp('Please uncheck blocked or unblocked not both')
			Return 1
		End If
		dw_header.SetItem(1,String(dwo.Name),String(data))
		wf_filter_blocked()
	Case "unblocked"
		IF dw_header.GetItemString(1, 'blocked') = 'N' and data = 'N' Then
			iw_frame.SetMicroHelp('Please uncheck blocked or unblocked not both')
			Return 1
		End If
		dw_header.SetItem(1,String(dwo.Name),String(data))
		wf_filter_blocked()
	Case "autocalculate"
		If dw_detail.RowCount() < 1 Then Return
		If data = "Y" Then 
			lr_total_excluded = Real(dw_detail.Describe("Evaluate('Sum(quantity * if(exclude = ~~~'Y~~~',1,0) for all)',1)"))
			If lr_total_excluded > dw_detail.GetItemNumber(1,"avail") Then
				MessageBox("Quantity Overflow","Cannot autocalculate prorated quantities when " + &
					"excluded quantities are greater than the available amount.",Exclamation!)
				return 1
			End If
			ls_prorate_zero = dw_header.GetItemString(1,"no_prorate")
			Parent.wf_autocalculate(data,ls_prorate_zero)
//			Parent.wf_noprorate(dw_header.GetItemString(1,"no_prorate"),0)
		End If
		wf_protect_pr_qty(data)
	Case "no_prorate"
		If dw_detail.RowCount() < 1 Then Return
		wf_autocalculate(dw_header.GetItemString(1,"autocalculate"), data)
//		li_rtn = Parent.wf_noprorate(data,0)
		Return li_rtn
End Choose
end event

event itemerror;call super::itemerror;return 1
end event

type em_prorate_pct from editmask within w_prorate
integer x = 946
integer y = 1588
integer width = 347
integer height = 76
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 16777215
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "###.00"
string displaydata = ""
end type

event modified;DwObject		ldwo_column
Double		ld_text
String			ls_prorate_zero

ldwo_column = dw_header.Object.autocalculate

ld_text = Double(This.Text)

If ld_text > 100 or ld_text < 0 then
	iw_frame.SetMicroHelp('The Percentage must be between 0 and ' + &
								'100')
	This.SelectText(1, 100)
	This.SetFocus()
	Return 1
End If

If dw_header.GetItemString(1,"autocalculate") = "Y" Then
	dw_header.SetItem(1, 'autocalculate', 'N')
	dw_header.Event ItemChanged(1, ldwo_column, 'N')
End If

ls_prorate_zero = dw_header.GetItemString(1,"no_prorate")
wf_autocalculate('N', ls_prorate_zero)
//wf_noprorate(dw_header.GetItemString(1,"no_prorate"),0)

end event

event getfocus;This.SelectText(1,Len(This.Text))
end event

type dw_detail from u_base_dw_ext within w_prorate
event ue_postitemchanged ( long al_row,  string as_columnname )
integer y = 416
integer width = 2414
integer height = 1264
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_prorate_detail"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;String		ls_autocalculate, ls_prorate_zero
Real lr_total_excluded


Choose Case String(dwo.Name)
	Case "exclude"
		ls_autocalculate = dw_header.GetItemString(1,"autocalculate")
		ls_prorate_zero = dw_header.GetItemString(1,"no_prorate")
		Parent.Post wf_autocalculate(ls_autocalculate, ls_prorate_zero)
//		Parent.Post wf_noprorate(dw_header.GetItemString(1,"no_prorate"),row)
		If data = "Y" Then
			If ls_autocalculate = "Y" Then
				lr_total_excluded = Real(dw_detail.Describe("Evaluate('Sum(quantity * if(exclude = ~~~'Y~~~',1,0) for all)',1)"))
				lr_total_excluded += dw_detail.GetItemNumber(row,"quantity")
				If lr_total_excluded > dw_detail.GetItemNumber(1,"avail") Then
					If MessageBox("Quantity Overflow","Cannot autocalculate prorated quantities when " + &
						"excluded quantities are greater than the available amount.  Continue with " + &
						"autocalculate mode off?",Question!,YesNo!) = 1 Then
						wf_protect_pr_qty('N')
						dw_header.SetItem(1,"autocalculate","N")
						dw_detail.SetItem(row,"prorated_qty",dw_detail.GetItemNumber(row,"quantity"))
						Parent.Post wf_CheckLoadWeight(row)
						return 0
					Else
						return 1
					End If
				End If
				If dw_detail.GetItemNumber(1,"total_prorated_qty") > dw_detail.GetItemNumber(1,"avail") Then
					If MessageBox("Quantity Overflow","Cannot autocalculate prorated quantities when " + &
						"excluded quantities are greater than the available amount.  Continue with " + &
						"autocalculate mode off?",Question!,YesNo!) = 1 Then
						wf_protect_pr_qty('N')
						dw_header.SetItem(1,"autocalculate","N")
						dw_detail.SetItem(row,"prorated_qty",dw_detail.GetItemNumber(row,"quantity"))
						Parent.Post wf_CheckLoadWeight(row)
						return 0
					Else
						return 1
					End If
				End If
			End If
		End If
		
	Case "prorated_qty"
		Parent.Post wf_CheckLoadWeight(row)
		If dw_header.GetItemString(1,"autocalculate") = "Y" Then
			If MessageBox("Quantity Changed","Changing a prorated quantity does not allow " + &
				"autocalculation.  Continue without autocalculate mode on?",Question!,YesNo!) = 1 Then
				dw_header.SetItem(1,"autocalculate","N")
				return 0
			Else
				return 1
			End If
		End If
		If Long(data) > dw_detail.GetItemNumber(row, 'quantity') Then
			iw_frame.SetMicroHelp('Please enter a quantity that is less than the original ' + &
					'quantity')
			This.SetFocus()
			This.SelectText(1, 100)
			Return 1
		End If
	Case Else

End Choose

end event

event itemerror;call super::itemerror;return 1
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1,10000)
end event

event getfocus;call super::getfocus;This.SelectText(1,10000)
end event

