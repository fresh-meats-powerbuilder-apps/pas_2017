HA$PBExportHeader$w_load_ahead_availability_detail_new.srw
forward
global type w_load_ahead_availability_detail_new from w_base_sheet_ext
end type
type dw_2 from u_base_dw_ext within w_load_ahead_availability_detail_new
end type
type dw_1 from u_base_dw_ext within w_load_ahead_availability_detail_new
end type
type cbx_1 from checkbox within w_load_ahead_availability_detail_new
end type
type dw_load_ahead_availability_detail from u_base_dw_ext within w_load_ahead_availability_detail_new
end type
end forward

global type w_load_ahead_availability_detail_new from w_base_sheet_ext
integer width = 2107
integer height = 1464
string title = "Load Availability Detail"
long backcolor = 67108864
dw_2 dw_2
dw_1 dw_1
cbx_1 cbx_1
dw_load_ahead_availability_detail dw_load_ahead_availability_detail
end type
global w_load_ahead_availability_detail_new w_load_ahead_availability_detail_new

type variables
string 	is_order_number, is_order_line, is_data
end variables

on w_load_ahead_availability_detail_new.create
int iCurrent
call super::create
this.dw_2=create dw_2
this.dw_1=create dw_1
this.cbx_1=create cbx_1
this.dw_load_ahead_availability_detail=create dw_load_ahead_availability_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_2
this.Control[iCurrent+2]=this.dw_1
this.Control[iCurrent+3]=this.cbx_1
this.Control[iCurrent+4]=this.dw_load_ahead_availability_detail
end on

on w_load_ahead_availability_detail_new.destroy
call super::destroy
destroy(this.dw_2)
destroy(this.dw_1)
destroy(this.cbx_1)
destroy(this.dw_load_ahead_availability_detail)
end on

event open;call super::open;String ls_temp, ls_primary_order, ls_prod_state
long ll_total_rows
ls_temp = Message.StringParm
is_data = ls_temp
dw_1.InsertRow(0)

dw_2.Reset()
//dw_1.Reset()

If Len(ls_temp) > 0 then //and lower(ls_temp) <> 'w_load_ahead_availability_detail_new'  Then
	//ll_total_rows = dw_load_ahead_availability_detail.ImportString(ls_temp)
	ll_total_rows =  dw_2.ImportString(ls_temp)
End if

ls_primary_order = dw_2.GetItemString(1, "order_number")
dw_1.SetItem(1, 'order', ls_primary_order)
ls_prod_state = dw_2.GetItemString(1, "product_state")
dw_1.SetItem(1, 'product_state', ls_prod_state)

dw_2.SetSort('line_number asc')
dw_2.Sort()
end event

event ue_postopen;call super::ue_postopen;string ls_temp, ls_sel
dw_2.ib_updateable = False
dw_1.ib_updateable = False
dw_1.SetSort('line_number asc')
dw_1.Sort()
//dw_1.ResetUpdate()
//dw_2.ResetUpdate()
ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "variance", "")

dw_1.AcceptText()
//ls_sel = dw_1.GetItemString(1, "Show_variance_only")
//Do While li_row <= li_row_count
//	ls_value = string(dw_2.GetItemNumber(li_row, "variance"))
	If ls_temp = 'Y' then
		dw_2.SetFilter("variance > 0")
		dw_2.Filter()
		dw_1.SetItem(1, "Show_variance_only", ls_temp)
	end if
//	li_row ++ 
//	ls_value = ""
//Loop
if ls_temp = 'N' then
	dw_2.SetFilter("")
	dw_2.Filter()
	dw_1.SetItem(1, "Show_variance_only", ls_temp)
	//dw_2.ImportString(is_data)
end if

end event

event close;call super::close;dw_load_ahead_availability_detail.Reset()
end event

type dw_2 from u_base_dw_ext within w_load_ahead_availability_detail_new
integer x = 41
integer y = 184
integer width = 1970
integer height = 1100
integer taborder = 30
string dataobject = "d_load_ahead_detail_dtl"
boolean vscrollbar = true
boolean border = false
end type

type dw_1 from u_base_dw_ext within w_load_ahead_availability_detail_new
integer x = 41
integer y = 48
integer width = 2080
integer height = 116
integer taborder = 20
string dataobject = "d_load_ahead_detail_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_value, ls_sel
integer li_row_count, li_row

li_row_count = dw_2.RowCount()
li_row = 1
//ls_value = string(dw_2.GetItemNumber(
dw_1.AcceptText()
ls_sel = dw_1.GetItemString(1, "Show_variance_only")
//Do While li_row <= li_row_count
//	ls_value = string(dw_2.GetItemNumber(li_row, "variance"))
	If data = 'Y' then
		dw_2.SetFilter("variance > 0")
		dw_2.Filter()
	end if
//	li_row ++ 
//	ls_value = ""
//Loop
if data = 'N' then
	dw_2.SetFilter("")
	dw_2.Filter()
	//dw_2.ImportString(is_data)
end if
SetProfileString(iw_frame.is_UserINI, "Pas", "variance", data)

end event

type cbx_1 from checkbox within w_load_ahead_availability_detail_new
boolean visible = false
integer x = 1298
integer y = 72
integer width = 82
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

event clicked;Long ll_row_count, ll_count
string ls_filter


ll_row_count = dw_load_ahead_availability_detail.RowCount()
ls_filter = "variance > 0"
//ll_count
//do while ll_count <= ll_row_count
dw_load_ahead_availability_detail.SetFilter(ls_filter)
dw_load_ahead_availability_detail.filter()
dw_load_ahead_availability_detail.SetRedraw(True)

	
end event

type dw_load_ahead_availability_detail from u_base_dw_ext within w_load_ahead_availability_detail_new
boolean visible = false
integer y = 48
integer width = 2025
integer height = 732
integer taborder = 10
string dataobject = "d_load_ahead_availability_fill"
boolean vscrollbar = true
boolean border = false
end type

