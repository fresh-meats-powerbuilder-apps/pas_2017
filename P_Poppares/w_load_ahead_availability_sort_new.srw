HA$PBExportHeader$w_load_ahead_availability_sort_new.srw
forward
global type w_load_ahead_availability_sort_new from w_base_response_ext
end type
type rb_8 from radiobutton within w_load_ahead_availability_sort_new
end type
type rb_7 from radiobutton within w_load_ahead_availability_sort_new
end type
type ddlb_4 from dropdownlistbox within w_load_ahead_availability_sort_new
end type
type st_4 from statictext within w_load_ahead_availability_sort_new
end type
type rb_6 from radiobutton within w_load_ahead_availability_sort_new
end type
type rb_5 from radiobutton within w_load_ahead_availability_sort_new
end type
type ddlb_3 from dropdownlistbox within w_load_ahead_availability_sort_new
end type
type st_3 from statictext within w_load_ahead_availability_sort_new
end type
type rb_4 from radiobutton within w_load_ahead_availability_sort_new
end type
type rb_3 from radiobutton within w_load_ahead_availability_sort_new
end type
type ddlb_2 from dropdownlistbox within w_load_ahead_availability_sort_new
end type
type st_2 from statictext within w_load_ahead_availability_sort_new
end type
type st_1 from statictext within w_load_ahead_availability_sort_new
end type
type rb_2 from radiobutton within w_load_ahead_availability_sort_new
end type
type rb_1 from radiobutton within w_load_ahead_availability_sort_new
end type
type ddlb_1 from dropdownlistbox within w_load_ahead_availability_sort_new
end type
type dw_1 from u_base_dw_ext within w_load_ahead_availability_sort_new
end type
type gb_1 from groupbox within w_load_ahead_availability_sort_new
end type
type gb_2 from groupbox within w_load_ahead_availability_sort_new
end type
type gb_3 from groupbox within w_load_ahead_availability_sort_new
end type
type gb_4 from groupbox within w_load_ahead_availability_sort_new
end type
end forward

global type w_load_ahead_availability_sort_new from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1915
integer height = 1908
string title = "Load Ahead Availability Sort"
long backcolor = 67108864
rb_8 rb_8
rb_7 rb_7
ddlb_4 ddlb_4
st_4 st_4
rb_6 rb_6
rb_5 rb_5
ddlb_3 ddlb_3
st_3 st_3
rb_4 rb_4
rb_3 rb_3
ddlb_2 ddlb_2
st_2 st_2
st_1 st_1
rb_2 rb_2
rb_1 rb_1
ddlb_1 ddlb_1
dw_1 dw_1
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
gb_4 gb_4
end type
global w_load_ahead_availability_sort_new w_load_ahead_availability_sort_new

type variables
string		is_sort1, is_sort2, is_sort3, is_sort4, is_sort_string
//w_base_sheet		iw_parentwindow
end variables

on w_load_ahead_availability_sort_new.create
int iCurrent
call super::create
this.rb_8=create rb_8
this.rb_7=create rb_7
this.ddlb_4=create ddlb_4
this.st_4=create st_4
this.rb_6=create rb_6
this.rb_5=create rb_5
this.ddlb_3=create ddlb_3
this.st_3=create st_3
this.rb_4=create rb_4
this.rb_3=create rb_3
this.ddlb_2=create ddlb_2
this.st_2=create st_2
this.st_1=create st_1
this.rb_2=create rb_2
this.rb_1=create rb_1
this.ddlb_1=create ddlb_1
this.dw_1=create dw_1
this.gb_1=create gb_1
this.gb_2=create gb_2
this.gb_3=create gb_3
this.gb_4=create gb_4
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_8
this.Control[iCurrent+2]=this.rb_7
this.Control[iCurrent+3]=this.ddlb_4
this.Control[iCurrent+4]=this.st_4
this.Control[iCurrent+5]=this.rb_6
this.Control[iCurrent+6]=this.rb_5
this.Control[iCurrent+7]=this.ddlb_3
this.Control[iCurrent+8]=this.st_3
this.Control[iCurrent+9]=this.rb_4
this.Control[iCurrent+10]=this.rb_3
this.Control[iCurrent+11]=this.ddlb_2
this.Control[iCurrent+12]=this.st_2
this.Control[iCurrent+13]=this.st_1
this.Control[iCurrent+14]=this.rb_2
this.Control[iCurrent+15]=this.rb_1
this.Control[iCurrent+16]=this.ddlb_1
this.Control[iCurrent+17]=this.dw_1
this.Control[iCurrent+18]=this.gb_1
this.Control[iCurrent+19]=this.gb_2
this.Control[iCurrent+20]=this.gb_3
this.Control[iCurrent+21]=this.gb_4
end on

on w_load_ahead_availability_sort_new.destroy
call super::destroy
destroy(this.rb_8)
destroy(this.rb_7)
destroy(this.ddlb_4)
destroy(this.st_4)
destroy(this.rb_6)
destroy(this.rb_5)
destroy(this.ddlb_3)
destroy(this.st_3)
destroy(this.rb_4)
destroy(this.rb_3)
destroy(this.ddlb_2)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.rb_2)
destroy(this.rb_1)
destroy(this.ddlb_1)
destroy(this.dw_1)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.gb_3)
destroy(this.gb_4)
end on

event ue_base_ok;call super::ue_base_ok;String		ls_sort_string, ls_sort_1, ls_sort_2, ls_sort_3, ls_sort_4, ls_temp


is_sort1 = ddlb_1.Text
is_sort2 = ddlb_2.Text
is_sort3 = ddlb_3.Text
is_sort4 = ddlb_4.Text


ls_sort_1 = is_sort1
ls_sort_2 = is_sort2
ls_sort_3 = is_sort3
ls_sort_4 = is_sort4

If is_sort1 = "Number of Lines" then
	is_sort1 = "compute_1" 
end if
if is_sort1 = "Transit hours" then
	is_sort1 = "transit_hours"
end if
if is_sort1 = "Weight status" then
	is_sort1 = "weight_status"
end if
if is_sort1 = "Multiplant" then
	is_sort1 = "multi_plant"
end if
if is_sort1 = "Credit Status" then
	is_sort1 = "credit"
end if
if is_sort1 = "Load Status" then
	is_sort1 = "load_status"
end if
if is_sort1 = "Deadline Departure Date" then
	is_sort1 = "deadline_date"
end if

If is_sort2 = "Number of Lines" then
	is_sort2 = "compute_1" 
end if
if is_sort2 = "Transit Hours" then
	is_sort2 = "transit_hours"
end if
if is_sort2 = "Weight status" then
	is_sort2 = "weight_status"
end if
if is_sort2 = "Multiplant" then
	is_sort2 = "multi_plant"
end if
if is_sort2 = "Credit Status" then
	is_sort2 = "credit"
end if
if is_sort2 = "Load Status" then
	is_sort2 = "load_status"
end if
if is_sort2 = "Deadline Departure Date" then
	is_sort2 = "deadline_date"
end if

If is_sort3 = "Number of Lines" then
	is_sort3 = "compute_1" 
end if
if is_sort3 = "Transit Hours" then
	is_sort3 = "transit_hours"
end if
if is_sort3 = "Weight status" then
	is_sort3 = "weight_status"
end if
if is_sort3 = "Multiplant" then
	is_sort3 = "multi_plant"
end if
if is_sort3 = "Credit Status" then
	is_sort3 = "credit"
end if
if is_sort3 = "Load Status" then
	is_sort3 = "load_status"
end if
if is_sort3 = "Deadline Departure Date" then
	is_sort3 = "deadline_date"
end if

If is_sort4 = "Number of Lines" then
	is_sort4 = "compute_1" 
end if
if is_sort4 = "Transit Hours" then
	is_sort4 = "transit_hours"
end if
if is_sort4 = "Weight status" then
	is_sort4 = "weight_status"
end if
if is_sort4 = "Multiplant" then
	is_sort4 = "multi_plant"
end if
if is_sort4 = "Credit Status" then
	is_sort4 = "credit"
end if
if is_sort4 = "Load Status" then
	is_sort4 = "load_status"
end if
if is_sort4 = "Deadline Departure Date" then
	is_sort4 = "deadline_date"
end if



	If is_sort2 = "" and is_sort3 = "" and is_sort4 = "" then
		if rb_1.checked then //and rb_3.checked and rb_5.checked then
			is_sort_string = is_sort1 + ' ' + 'A' + ', ' + 'primaty_order A'
		else
			is_sort_string = is_sort1 + ' ' + 'D' + ', ' + 'primaty_order A'
		end if
	else
		if is_sort3 = "" and is_sort4 = "" then
			if rb_1.checked and rb_3.checked then
				is_sort_string = is_sort1 +' ' + 'A'+ ', ' + is_sort2 + ' ' + 'A' + ', ' + 'primaty_order A'
			end if
			if rb_1.checked and rb_4.checked then
				is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 + ' ' + 'D' + ', ' + 'primaty_order A'
			end if
			if rb_2.checked and rb_4.checked then
				is_sort_string = is_sort1 +' ' + 'D'+ ', ' + is_sort2 + ' ' + 'D' + ', ' + 'primaty_order A'
			end if
			if rb_2.checked and rb_3.checked then
				is_sort_string = is_sort1 +' ' + 'D'+ ', ' + is_sort2 + ' ' + 'A' + ', ' + 'primaty_order A'
			end if
		else
			if is_sort4 = "" then 
				if rb_1.checked and rb_3.checked and rb_5.checked then
					is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_6.checked then
					is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and  rb_4.checked and rb_5.checked then
					is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_3.checked and rb_6.checked then
					is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
				end if
		
				if rb_2.checked and rb_4.checked and rb_6.checked then
					is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_6.checked then
					is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_4.checked and rb_5.checked then
					is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_5.checked then
					is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
				end if
			else 
		
				if rb_1.checked and rb_3.checked and rb_5.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + 'primaty_order A'
				end if
				if rb_1.checked and rb_3.checked and rb_5.checked and rb_8.checked then 
					is_sort_string = is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + 'primaty_order A'
				end if
				if rb_1.checked and rb_3.checked and rb_6.checked and rb_8.checked then
					is_sort_string =   is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_6.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_5.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_5.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_3.checked and rb_6.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_6.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
		

				if rb_2.checked and rb_4.checked and rb_6.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_4.checked and rb_6.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_4.checked and rb_5.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_5.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_6.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_6.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_4.checked and rb_5.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_5.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
			end if
		end if
	end if
		
	
//	if is_sort3= "" and is_sort4 = "" then
//		if rb_1.checked and rb_3.checked then
//			is_sort_string = is_sort1 +' ' + 'A'+ ', ' + is_sort2 + ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_1.checked and rb_4.checked then
//			is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 + ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_4.checked then
//			is_sort_string = is_sort1 +' ' + 'D'+ ', ' + is_sort2 + ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_3.checked then
//			is_sort_string = is_sort1 +' ' + 'D'+ ', ' + is_sort2 + ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//	end if
//	
//	
//	if is_sort4 = "" then
//		if rb_1.checked and rb_3.checked and rb_5.checked then
//			is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_1.checked and rb_4.checked and rb_6.checked then
//			is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_1.checked and  rb_4.checked and rb_5.checked then
//			is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_1.checked and rb_3.checked and rb_6.checked then
//			is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		
//		if rb_2.checked and rb_4.checked and rb_6.checked then
//			is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_3.checked and rb_6.checked then
//			is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_4.checked and rb_5.checked then
//			is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_3.checked and rb_5.checked then
//			is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//	else 
//		
//		if rb_1.checked and rb_3.checked and rb_5.checked and rb_7.checked then
//			is_sort_string =  'primaty_order A, '+  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A'
//		end if
//		if rb_1.checked and rb_3.checked and rb_5.checked and rb_8.checked then 
//			is_sort_string = 'primaty_order A, ' + is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' 
//		end if
//		if rb_1.checked and rb_3.checked and rb_6.checked and rb_8.checked then
//			is_sort_string =  'primaty_order A, ' + is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' 
//		end if
//		if rb_1.checked and rb_4.checked and rb_6.checked and rb_8.checked then
//			is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_1.checked and rb_4.checked and rb_5.checked and rb_8.checked then
//			is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_1.checked and rb_4.checked and rb_5.checked and rb_7.checked then
//			is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_1.checked and rb_3.checked and rb_6.checked and rb_7.checked then
//			is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_1.checked and rb_4.checked and rb_6.checked and rb_7.checked then
//			is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		
//		
//		
//		if rb_2.checked and rb_4.checked and rb_6.checked and rb_8.checked then
//			is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_4.checked and rb_6.checked and rb_7.checked then
//			is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_4.checked and rb_5.checked and rb_7.checked then
//			is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_3.checked and rb_5.checked and rb_7.checked then
//			is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_3.checked and rb_6.checked and rb_7.checked then
//			is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_3.checked and rb_6.checked and rb_8.checked then
//			is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_4.checked and rb_5.checked and rb_8.checked then
//			is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//		if rb_2.checked and rb_3.checked and rb_5.checked and rb_8.checked then
//			is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
//		end if
//	end if
//	
	

	
	If rb_1.checked  then
		ls_temp = 'Y'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB1", ls_temp)
	else 
		ls_temp = 'N'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB1", ls_temp)
	end if
	
	If rb_2.checked  then
		ls_temp = 'Y'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB2", ls_temp)
	else 
		ls_temp = 'N'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB2", ls_temp)
	end if
		
	If rb_3.checked  then
		ls_temp = 'Y'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB3", ls_temp)
	else 
		ls_temp = 'N'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB3", ls_temp)
	end if		
	
	
	If rb_4.checked  then
		ls_temp = 'Y'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB4", ls_temp)
	else 
		ls_temp = 'N'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB4", ls_temp)
	end if

	If rb_5.checked  then
		ls_temp = 'Y'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB5", ls_temp)
	else 
		ls_temp = 'N'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB5", ls_temp)
	end if
	
	If rb_6.checked  then
		ls_temp = 'Y'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB6", ls_temp)
	else 
		ls_temp = 'N'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB6", ls_temp)
	end if
	
	If rb_7.checked  then
		ls_temp = 'Y'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB7", ls_temp)
	else 
		ls_temp = 'N'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB7", ls_temp)
	end if
	
	If rb_8.checked  then
		ls_temp = 'Y'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB8", ls_temp)
	else 
		ls_temp = 'N'
		SetProfileString(iw_frame.is_UserINI, "PAS", "RB8", ls_temp)
	end if
		
		


SetProfileString(iw_frame.is_UserINI, "PAS", "Sort1", ls_sort_1)
SetProfileString(iw_frame.is_UserINI, "PAS", "Sort2", ls_sort_2)
SetProfileString(iw_frame.is_UserINI, "PAS", "Sort3", ls_sort_3)
SetProfileString(iw_frame.is_UserINI, "PAS", "Sort4", ls_sort_4)



CloseWithReturn(this, is_sort_string)
//iw_parentwindow.Event ue_set_data('sort', is_sort_string)
//iw_parentwindow.Event ue_set_data('close', "true")
//ib_ok_to_close = True
//Close(this)
end event

event close;call super::close;//closewithreturn(this, is_sort_string)
end event

event open;call super::open;String ls_sort_parms
iw_ParentWindow = Message.PowerObjectParm
ls_sort_parms = Message.StringParm
//This.Title = iw_ParentWindow.title + " Sort"
iw_frame.SetMicroHelp("Ready")
end event

event ue_base_cancel;call super::ue_base_cancel;
CloseWithReturn(this, is_sort_string)
//iw_parentwindow.Event ue_set_data('sort', is_sort_string)
//iw_parentwindow.Event ue_set_data('close', "true")
//ib_ok_to_close = True
//Close(this)
end event

event ue_postopen;call super::ue_postopen;String ls_temp, ls_sort_string, ls_sort_1, ls_sort_2, ls_sort_3, ls_sort_4
//dw_1.InsertRow(0)
rb_1.Checked = True
//rb2_.Checked = True
rb_3.Checked = True
//rb4_.Checked = True
rb_5.Checked = True
//rb6_.Checked = True
rb_7.Checked = True
//rb8_.Checked = True

ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "Sort1", "")
ddlb_1.SelectItem(ls_temp, 1)
//ddlb_1.Text = ls_temp

ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "Sort2", "")
ddlb_2.SelectItem(ls_temp, 1)

ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "Sort3", "")
ddlb_3.SelectItem(ls_temp, 1)

ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "Sort4", "")
ddlb_4.SelectItem(ls_temp, 1)

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "RB1", "")
If ls_temp = 'Y' then
	rb_1.Checked = True
else 
	rb_1.Checked = False
end if

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "RB2", "")
If ls_temp = 'Y' then
	rb_2.Checked = True
else 
	rb_2.Checked = False
end if

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "RB3", "")
If ls_temp = 'Y' then
	rb_3.Checked = True
else 
	rb_3.Checked = False
end if

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "RB4", "")
If ls_temp = 'Y' then
	rb_4.Checked = True
else 
	rb_4.Checked = False
end if

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "RB5", "")
If ls_temp = 'Y' then
	rb_5.Checked = True
else 
	rb_5.Checked = False
end if

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "RB6", "")
If ls_temp = 'Y' then
	rb_6.Checked = True
else 
	rb_6.Checked = False
end if

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "RB7", "")
If ls_temp = 'Y' then
	rb_7.Checked = True
else 
	rb_7.Checked = False
end if

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "RB8", "")
If ls_temp = 'Y' then
	rb_8.Checked = True
else 
	rb_8.Checked = False
end if






is_sort1 = ddlb_1.Text
is_sort2 = ddlb_2.Text
is_sort3 = ddlb_3.Text
is_sort4 = ddlb_4.Text


ls_sort_1 = is_sort1
ls_sort_2 = is_sort2
ls_sort_3 = is_sort3
ls_sort_4 = is_sort4







If is_sort1 = "Number of Lines" then
	is_sort1 = "compute_1" 
end if
if is_sort1 = "Transit hours" then
	is_sort1 = "transit_hours"
end if
if is_sort1 = "Weight status" then
	is_sort1 = "weight_status"
end if
if is_sort1 = "Multiplant" then
	is_sort1 = "multi_plant"
end if
if is_sort1 = "Credit Status" then
	is_sort1 = "credit"
end if
if is_sort1 = "Load Status" then
	is_sort1 = "load_status"
end if
if is_sort1 = "Deadline Departure Date" then
	is_sort1 = "deadline_date"
end if

If is_sort2 = "Number of Lines" then
	is_sort2 = "compute_1" 
end if
if is_sort2 = "Transit Hours" then
	is_sort2 = "transit_hours"
end if
if is_sort2 = "Weight status" then
	is_sort2 = "weight_status"
end if
if is_sort2 = "Multiplant" then
	is_sort2 = "multi_plant"
end if
if is_sort2 = "Credit Status" then
	is_sort2 = "credit"
end if
if is_sort2 = "Load Status" then
	is_sort2 = "load_status"
end if
if is_sort2 = "Deadline Departure Date" then
	is_sort2 = "deadline_date"
end if

If is_sort3 = "Number of Lines" then
	is_sort3 = "compute_1" 
end if
if is_sort3 = "Transit Hours" then
	is_sort3 = "transit_hours"
end if
if is_sort3 = "Weight status" then
	is_sort3 = "weight_status"
end if
if is_sort3 = "Multiplant" then
	is_sort3 = "multi_plant"
end if
if is_sort3 = "Credit Status" then
	is_sort3 = "credit"
end if
if is_sort3 = "Load Status" then
	is_sort3 = "load_status"
end if
if is_sort3 = "Deadline Departure Date" then
	is_sort3 = "deadline_date"
end if

If is_sort4 = "Number of Lines" then
	is_sort4 = "compute_1" 
end if
if is_sort4 = "Transit Hours" then
	is_sort4 = "transit_hours"
end if
if is_sort4 = "Weight status" then
	is_sort4 = "weight_status"
end if
if is_sort4 = "Multiplant" then
	is_sort4 = "multi_plant"
end if
if is_sort4 = "Credit Status" then
	is_sort4 = "credit"
end if
if is_sort4 = "Load Status" then
	is_sort4 = "load_status"
end if
if is_sort4 = "Deadline Departure Date" then
	is_sort4 = "deadline_date"
end if



	If is_sort2 = "" and is_sort3 = "" and is_sort4 = "" then
		if rb_1.checked then //and rb_3.checked and rb_5.checked then
			is_sort_string = is_sort1 + ' ' + 'A' + ', ' + 'primaty_order A'
		else
			is_sort_string = is_sort1 + ' ' + 'D' + ', ' + 'primaty_order A'
		end if
	else
		if is_sort3 = "" and is_sort4 = "" then
			if rb_1.checked and rb_3.checked then
				is_sort_string = is_sort1 +' ' + 'A'+ ', ' + is_sort2 + ' ' + 'A' + ', ' + 'primaty_order A'
			end if
			if rb_1.checked and rb_4.checked then
				is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 + ' ' + 'D' + ', ' + 'primaty_order A'
			end if
			if rb_2.checked and rb_4.checked then
				is_sort_string = is_sort1 +' ' + 'D'+ ', ' + is_sort2 + ' ' + 'D' + ', ' + 'primaty_order A'
			end if
			if rb_2.checked and rb_3.checked then
				is_sort_string = is_sort1 +' ' + 'D'+ ', ' + is_sort2 + ' ' + 'A' + ', ' + 'primaty_order A'
			end if
		else
			if is_sort4 = "" then 
				if rb_1.checked and rb_3.checked and rb_5.checked then
					is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_6.checked then
					is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and  rb_4.checked and rb_5.checked then
					is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_3.checked and rb_6.checked then
					is_sort_string = is_sort1 + ' ' + 'A' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
				end if
		
				if rb_2.checked and rb_4.checked and rb_6.checked then
					is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_6.checked then
					is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_4.checked and rb_5.checked then
					is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'D' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_5.checked then
					is_sort_string = is_sort1 + ' ' + 'D' + ', ' + is_sort2 +' ' + 'A' + ', '+ is_sort3 + ' ' + 'A' + ', ' + 'primaty_order A'
				end if
			else 
		
				if rb_1.checked and rb_3.checked and rb_5.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + 'primaty_order A'
				end if
				if rb_1.checked and rb_3.checked and rb_5.checked and rb_8.checked then 
					is_sort_string = is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + 'primaty_order A'
				end if
				if rb_1.checked and rb_3.checked and rb_6.checked and rb_8.checked then
					is_sort_string =   is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_6.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_5.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_5.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_3.checked and rb_6.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_1.checked and rb_4.checked and rb_6.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'A'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
		

				if rb_2.checked and rb_4.checked and rb_6.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_4.checked and rb_6.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_4.checked and rb_5.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_5.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_6.checked and rb_7.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'A' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_6.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'D'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_4.checked and rb_5.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'D'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
				if rb_2.checked and rb_3.checked and rb_5.checked and rb_8.checked then
					is_sort_string =  is_sort1 +' ' + 'D'+', ' + is_sort2 + ' ' + 'A'+ ', ' + is_sort3 + ' '+ 'A'+ ', ' + is_sort4+ ' ' + 'D' + ', ' + 'primaty_order A'
				end if
			end if
		end if
	end if


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_load_ahead_availability_sort_new
boolean visible = false
integer x = 2171
integer y = 1256
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_load_ahead_availability_sort_new
integer x = 905
integer y = 1676
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_load_ahead_availability_sort_new
integer x = 535
integer y = 1676
end type

type rb_8 from radiobutton within w_load_ahead_availability_sort_new
integer x = 1353
integer y = 1460
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Descending"
end type

type rb_7 from radiobutton within w_load_ahead_availability_sort_new
integer x = 1358
integer y = 1328
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ascending"
end type

type ddlb_4 from dropdownlistbox within w_load_ahead_availability_sort_new
integer x = 137
integer y = 1320
integer width = 1029
integer height = 324
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean sorted = false
boolean vscrollbar = true
string item[] = {"","Transit Hours","Transit Hours","Weight status","Multiplant","Credit Status","Load Status","Deadline Departure Date"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;is_sort4 = ddlb_4.Text
end event

type st_4 from statictext within w_load_ahead_availability_sort_new
integer x = 142
integer y = 1236
integer width = 343
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Then By:"
boolean focusrectangle = false
end type

type rb_6 from radiobutton within w_load_ahead_availability_sort_new
integer x = 1339
integer y = 1060
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Descending"
end type

type rb_5 from radiobutton within w_load_ahead_availability_sort_new
integer x = 1344
integer y = 924
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ascending"
end type

type ddlb_3 from dropdownlistbox within w_load_ahead_availability_sort_new
integer x = 137
integer y = 924
integer width = 1029
integer height = 324
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean sorted = false
boolean vscrollbar = true
string item[] = {"","Number of Lines","Transit Hours","Weight status","Multiplant","Credit Status","Load Status","Deadline Departure Date"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;is_sort3 = ddlb_3.Text
end event

type st_3 from statictext within w_load_ahead_availability_sort_new
integer x = 128
integer y = 832
integer width = 343
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Then By:"
boolean focusrectangle = false
end type

type rb_4 from radiobutton within w_load_ahead_availability_sort_new
integer x = 1349
integer y = 648
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Descending"
end type

type rb_3 from radiobutton within w_load_ahead_availability_sort_new
integer x = 1344
integer y = 524
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ascending"
end type

type ddlb_2 from dropdownlistbox within w_load_ahead_availability_sort_new
integer x = 137
integer y = 552
integer width = 1029
integer height = 324
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean sorted = false
boolean vscrollbar = true
string item[] = {"","Number of Lines","Transit Hours","Weight status","Multiplant","Credit Status","Load Status","Deadline Departure Date"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;is_sort2 = ddlb_2.Text
end event

type st_2 from statictext within w_load_ahead_availability_sort_new
integer x = 123
integer y = 432
integer width = 343
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Then By"
boolean focusrectangle = false
end type

type st_1 from statictext within w_load_ahead_availability_sort_new
integer x = 114
integer y = 56
integer width = 343
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Sort By:"
boolean focusrectangle = false
end type

type rb_2 from radiobutton within w_load_ahead_availability_sort_new
integer x = 1344
integer y = 272
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Descending"
end type

type rb_1 from radiobutton within w_load_ahead_availability_sort_new
integer x = 1344
integer y = 156
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ascending"
end type

type ddlb_1 from dropdownlistbox within w_load_ahead_availability_sort_new
integer x = 142
integer y = 172
integer width = 1024
integer height = 300
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean sorted = false
boolean vscrollbar = true
string item[] = {"Number of Lines","Transit hours","Weight status","Multiplant","Credit Status","Load Status","Deadline Departure Date"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;//is_sort1 = ddlb_1.Text
end event

type dw_1 from u_base_dw_ext within w_load_ahead_availability_sort_new
boolean visible = false
integer x = 41
integer y = 116
integer width = 1198
integer height = 1468
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_load_ahead_sort"
boolean border = false
end type

event itemchanged;call super::itemchanged;//is_sort1 = dw_1.getItemString(1, "sort_1")
//is_sort2 = dw_1.getItemString(1, "sort_2")
//is_sort3 = dw_1.getItemString(1, "sort_3")
//is_sort4 = dw_1.getItemString(1, "sort_4")
end event

type gb_1 from groupbox within w_load_ahead_availability_sort_new
integer x = 1262
integer y = 92
integer width = 544
integer height = 324
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_2 from groupbox within w_load_ahead_availability_sort_new
integer x = 1271
integer y = 456
integer width = 517
integer height = 324
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_3 from groupbox within w_load_ahead_availability_sort_new
integer x = 1271
integer y = 848
integer width = 512
integer height = 324
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_4 from groupbox within w_load_ahead_availability_sort_new
integer x = 1275
integer y = 1252
integer width = 507
integer height = 324
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

