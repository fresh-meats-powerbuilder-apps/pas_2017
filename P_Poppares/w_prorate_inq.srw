HA$PBExportHeader$w_prorate_inq.srw
forward
global type w_prorate_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_prorate_inq
end type
type dw_header from u_base_dw_ext within w_prorate_inq
end type
type dw_fab from u_fab_product_code within w_prorate_inq
end type
end forward

global type w_prorate_inq from w_base_response_ext
integer x = 146
integer y = 400
integer width = 2025
integer height = 912
long backcolor = 67108864
dw_plant dw_plant
dw_header dw_header
dw_fab dw_fab
end type
global w_prorate_inq w_prorate_inq

type variables
w_prorate		iw_parent_window

end variables

on w_prorate_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_header=create dw_header
this.dw_fab=create dw_fab
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_header
this.Control[iCurrent+3]=this.dw_fab
end on

on w_prorate_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_header)
destroy(this.dw_fab)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"")
end event

event ue_base_ok;call super::ue_base_ok;If dw_plant.AcceptText() = -1 Then return


If iw_frame.iu_string.nf_IsEmpty(dw_plant.uf_get_plant_code()) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End If
// product State ibdkdld 10/02/02
//If dw_product.AcceptText() = -1 Then return
if not dw_fab.uf_validate( ) Then Return

// product State ibdkdld 10/02/02
//If iw_frame.iu_string.nf_IsEmpty(dw_product.uf_getproductcode()) Then
//	iw_frame.SetMicroHelp("Product Code is a required field")
//	dw_product.SetFocus()
//	return
//End If

If iw_frame.iu_string.nf_IsEmpty(dw_fab.uf_get_product_code( )) Then
	iw_frame.SetMicroHelp("Product Code is a required field")
	dw_fab.SetFocus()
	return
End If

If iw_frame.iu_string.nf_IsEmpty(dw_fab.uf_get_product_state( )) Then
	iw_frame.SetMicroHelp("Product State is a required field")
	dw_fab.SetFocus()
	return
End If

If iw_frame.iu_string.nf_IsEmpty(dw_fab.uf_get_product_status( )) Then
	iw_frame.SetMicroHelp("Product Status is a required field")
	dw_fab.SetFocus()
	return
End If

If dw_header.AcceptText() = -1 Then return

If iw_frame.iu_string.nf_IsEmpty(String(dw_header.GetItemDate(1,"ship_date"))) Then
	iw_frame.SetMicroHelp("Ship Date is a required field")
	dw_header.SetFocus()
	dw_header.SetColumn("ship_date")
	return
End If

iw_parent_window.dw_plant.uf_set_plant_code(dw_plant.uf_get_plant_code())
// product State ibdkdld 10/02/02
//iw_parent_window.dw_product.uf_setproductcode(dw_product.uf_getproductcode())
//iw_parent_window.dw_product.uf_setproductdesc(dw_product.uf_getproductdesc())
iw_parent_window.dw_fab.uf_set_product_code(dw_fab.uf_get_product_code( ))
iw_parent_window.dw_fab.uf_set_product_desc(dw_fab.uf_get_product_desc())
iw_parent_window.dw_fab.uf_set_product_state(dw_fab.uf_get_product_state( ))
iw_parent_window.dw_fab.uf_set_product_status(dw_fab.uf_get_product_status( ))
iw_parent_window.dw_fab.uf_set_product_state_desc(dw_fab.uf_get_product_state_desc())
iw_parent_window.dw_fab.uf_set_product_status_desc(dw_fab.uf_get_product_status_desc())

CloseWithReturn(This,"Y")
end event

event open;call super::open;String	ls_temp

iw_parent_window = Message.PowerObjectParm

This.Title = iw_parent_window.Title + " Inquire"

dw_plant.uf_set_plant_code(iw_parent_window.dw_plant.uf_get_plant_code())
//product state ibdkdld 10/02/02
//dw_product.uf_setproductcode(iw_parent_window.dw_product.uf_getproductcode())

dw_fab.uf_set_product_code(iw_parent_window.dw_fab.uf_get_product_code())
dw_fab.uf_set_product_desc(iw_parent_window.dw_fab.uf_get_product_desc())

//product state ibdkdld 10/02/02 removed this is not right
//dw_product.TriggerEvent(ItemChanged!)
iw_parent_window.dw_header.ShareData(This.dw_header)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_prorate_inq
integer x = 1659
integer y = 300
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_prorate_inq
integer x = 1659
integer y = 176
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_prorate_inq
integer x = 1659
integer y = 52
integer taborder = 40
end type

type dw_plant from u_plant within w_prorate_inq
integer x = 178
integer y = 32
integer width = 1472
integer taborder = 10
end type

type dw_header from u_base_dw_ext within w_prorate_inq
integer x = 261
integer y = 436
integer width = 1285
integer height = 348
integer taborder = 30
string dataobject = "d_prorate_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;Real	lr_total_excluded

Choose Case String(dwo.Name)
	Case "ship_date"
		If Date(data) < Date(String(Today(), 'yyyy-mm-dd')) Then
			iw_frame.SetMicroHelp("Ship Date must be greater than or equal to today")
			This.SetFocus()
			This.SelectText(1, 100)
			return 1
		End If
	Case "blocked" 
		IF dw_header.GetItemString(1, 'unblocked') = 'N' and data = 'N' Then
			iw_frame.SetMicroHelp('Please uncheck blocked or unblocked not both')
			This.SetFocus()
			Return 1
		End If
	Case "unblocked"
		IF dw_header.GetItemString(1, 'blocked') = 'N' and data = 'N' Then
			iw_frame.SetMicroHelp('Please uncheck blocked or unblocked not both')
			This.SetFocus()
			Return 1
		End If
End Choose
end event

event itemerror;call super::itemerror;Return 1

end event

event constructor;call super::constructor;dw_header.object.plant_minimum_total_weight_t.visible = 0
dw_header.object.plant_total_weight.visible = 0

dw_header.object.ship_date_t.y = '52'
dw_header.object.ship_date.y = '52'

dw_header.object.autocalculate.y = '132'

dw_header.InsertRow(0)


end event

type dw_fab from u_fab_product_code within w_prorate_inq
integer y = 108
integer width = 1518
integer taborder = 11
boolean bringtotop = true
end type

