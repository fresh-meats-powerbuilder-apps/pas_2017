HA$PBExportHeader$w_load_ahead_availability_order_new_inq.srw
forward
global type w_load_ahead_availability_order_new_inq from w_base_response_ext
end type
type st_1 from statictext within w_load_ahead_availability_order_new_inq
end type
type sle_1 from singlelineedit within w_load_ahead_availability_order_new_inq
end type
end forward

global type w_load_ahead_availability_order_new_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1225
integer height = 440
long backcolor = 67108864
st_1 st_1
sle_1 sle_1
end type
global w_load_ahead_availability_order_new_inq w_load_ahead_availability_order_new_inq

type variables
string 	is_order_number
end variables

on w_load_ahead_availability_order_new_inq.create
int iCurrent
call super::create
this.st_1=create st_1
this.sle_1=create sle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.sle_1
end on

on w_load_ahead_availability_order_new_inq.destroy
call super::destroy
destroy(this.st_1)
destroy(this.sle_1)
end on

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm
This.Title = iw_ParentWindow.title + "Inquire"
iw_frame.SetMicroHelp("Ready")
end event

event ue_base_ok;call super::ue_base_ok;u_string_functions 	lu_strings
If lu_strings.nf_IsEmpty(sle_1.text) then
	iw_frame.SetMicroHelp("Order Number is a required field")
	sle_1.SetFocus()
	return
End if
is_order_number = sle_1.text
iw_parentwindow.Event ue_set_data('order_number', is_order_number)
ib_ok_to_close = True
Close(This)
end event

event ue_base_cancel;call super::ue_base_cancel;Close(this)
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('order_number')
sle_1.text = Message.StringParm
sle_1.text = ""
sle_1.SetFocus()
end event

event close;call super::close;closewithreturn(this, is_order_number)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_load_ahead_availability_order_new_inq
boolean visible = false
integer x = 1669
integer y = 336
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_load_ahead_availability_order_new_inq
integer x = 887
integer y = 208
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_load_ahead_availability_order_new_inq
integer x = 887
integer y = 64
integer height = 104
end type

type st_1 from statictext within w_load_ahead_availability_order_new_inq
integer x = 91
integer y = 88
integer width = 343
integer height = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Order Number"
boolean focusrectangle = false
end type

type sle_1 from singlelineedit within w_load_ahead_availability_order_new_inq
integer x = 448
integer y = 68
integer width = 343
integer height = 92
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
textcase textcase = upper!
integer limit = 7
borderstyle borderstyle = stylelowered!
end type

