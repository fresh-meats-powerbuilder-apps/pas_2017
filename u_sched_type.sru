$PBExportHeader$u_sched_type.sru
forward
global type u_sched_type from datawindow
end type
end forward

global type u_sched_type from datawindow
integer width = 946
integer height = 100
integer taborder = 10
string dataobject = "d_sched_type"
boolean border = false
boolean livescroll = true
end type
global u_sched_type u_sched_type

forward prototypes
public function integer uf_enable (boolean ab_enable)
public function integer uf_modified ()
public function string uf_get_type ()
public subroutine uf_set_type (string as_sched_type)
end prototypes

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.shift.Background.Color = 16777215
	This.object.shift.Protect = 0
Else
	This.object.shift.Background.Color = 12632256
	This.object.shift.Protect = 1
End If

Return 1
end function

public function integer uf_modified ();return 0
end function

public function string uf_get_type ();Return This.GetItemString(1, "sched_type")
end function

public subroutine uf_set_type (string as_sched_type);	This.SetItem(1,"sched_type",as_sched_type)
  
end subroutine

event constructor;DataWindowChild		ldwc_type


This.GetChild("sched_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PPS SECT")
This.InsertRow(0)

end event
on u_sched_type.create
end on

on u_sched_type.destroy
end on

