$PBExportHeader$w_prd_sched.srw
forward
global type w_prd_sched from w_base_sheet_ext
end type
type cb_recreate from commandbutton within w_prd_sched
end type
type dw_row_choice from u_row_choice within w_prd_sched
end type
type dw_header from u_base_dw_ext within w_prd_sched
end type
type dw_sched_dtl from u_base_dw_ext within w_prd_sched
end type
end forward

global type w_prd_sched from w_base_sheet_ext
integer width = 4169
integer height = 1660
string title = "Update Schedule"
long backcolor = 67108864
event ue_recreate pbm_custom70
cb_recreate cb_recreate
dw_row_choice dw_row_choice
dw_header dw_header
dw_sched_dtl dw_sched_dtl
end type
global w_prd_sched w_prd_sched

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product	

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

u_pas201		iu_pas201
u_pas203		iu_pas203

String		is_input, &
				is_debug, &
				is_name_string
	

u_sect_functions		iu_sect_functions

DataWindowChild		idwc_sub_desc
end variables

forward prototypes
public function boolean wf_get_sub_desc ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function boolean wf_recreate ()
public function boolean wf_retrieve ()
end prototypes

event ue_recreate;wf_recreate()
end event

public function boolean wf_get_sub_desc ();Integer	li_row_Count
				
String	ls_input, &
			ls_output_values
						
u_string_functions u_string

DataWindowChild	ldwc_temp


idwc_sub_desc.Reset()

ls_input = 'SUBSECT~r~n'

ls_output_values = iu_sect_functions.uf_get_names(ls_input)

li_row_count = idwc_sub_desc.ImportString(ls_output_values)

Return True

end function

public function boolean wf_validate (long al_row);Date					ldt_from_prod_date, &
						ldt_to_prod_date, &
						ldt_sched_date
						
Long					ll_rtn, &
						ll_nbrrows, &
						ll_qty

String				ls_division_code, &
						ls_temp


ll_nbrrows = dw_sched_dtl.RowCount()

//check that the quantity is >= zero and < 10000000
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "quantity") 
If ll_qty > 9999999 Then 
	iw_Frame.SetMicroHelp("Quantity must be less than 9999999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("quantity")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("Quantity must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("quantity")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the uom qty is >= zero and < 100000
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "uom_qty") 
If ll_qty > 9999 Then 
	iw_Frame.SetMicroHelp("UOM Qty must be less than 9999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("uom_qty")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("UOM Qty must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("uom_qty")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the pcs lbs >= 0 and < 999999999
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "pcs_lbs") 
If ll_qty > 999999999 Then 
	iw_Frame.SetMicroHelp("PCS LBS must be less than 999999999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("pcs_lbs")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("PCS LBS must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("pcs_lbs")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the mix pct >= 0 and < 999
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "mix_pct") 
If ll_qty > 999 Then 
	iw_Frame.SetMicroHelp("MIX PCT must be less than 999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("mix_pct")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("MIX PCT must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("mix_pct")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the act sched >= 0 and < 9999999
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "act_sched") 
If ll_qty > 9999999 Then 
	iw_Frame.SetMicroHelp("Act Sched must be less than 9999999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("act_sched")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("Act Sched must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("act_sched")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the act prod >= 0 and < 9999999
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "act_prod") 
If ll_qty > 9999999 Then 
	iw_Frame.SetMicroHelp("Act Prod must be less than 9999999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("act_prod")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("Act Prod must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("act_prod")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If


// Check that the production dates are <= the schedule date

ldt_from_prod_date= dw_sched_dtl.GetItemDate(al_row, "from_prod_date")
ldt_sched_date = dw_header.GetItemDate(1, "sched_date")
IF dw_sched_dtl.GetItemString(al_row, "date_ind") = 'N' Then
	IF IsNull(ldt_from_prod_date) or ldt_from_prod_date<= Date('00/00/0000') Then 
		iw_Frame.SetMicroHelp("Please enter a From Date greater than 00/00/0000")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("from_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End If	
	IF ldt_from_prod_date> ldt_sched_date Then 
		iw_Frame.SetMicroHelp("Please enter a From Date less than the Schedule Date")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("from_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
	If ldt_from_prod_date> dw_sched_dtl.GetItemDate(al_row, "to_prod_date") Then
		iw_Frame.SetMicroHelp("Please enter a From Date less than the To Date")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("from_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End IF

// Check that the end date has a value and is < schedule date.
	ldt_to_prod_date = dw_sched_dtl.GetItemDate(al_row, "to_prod_date")
	IF IsNull(ldt_to_prod_date) or ldt_to_prod_date <= Date('00/00/0000') Then 
		iw_Frame.SetMicroHelp("Please enter a To Date greater than 00/00/0000")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("to_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
	IF ldt_to_prod_date > ldt_sched_date Then 
		iw_Frame.SetMicroHelp("Please enter a To Date less than the Schedule Date")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("to_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
	//If ldt_temp < dw_sched_dtl.GetItemDate(al_row, "from_prod_date") Then
	//		iw_Frame.SetMicroHelp("Please enter a To Date greater than the From Date")
	//		This.SetRedraw(False)
	//		dw_sched_dtl.ScrollToRow(al_row)
	//		dw_sched_dtl.SetColumn("to_prod_date")
	//		dw_sched_dtl.SetFocus()
	//		This.SetRedraw(True)
	//		Return False
	//End If
End If

If dw_sched_dtl.GetItemString(al_row, "qty_chg_ind") = 'Q' Then
	If ldt_sched_Date < today() Then
		If MessageBox("Confirm", "Quantity has changed for a schedule date prior to current date. Continue?", + &
			Question!, YesNo!,1) <> 1 Then
			Return False
		End if
	End If
End If

If dw_sched_dtl.GetItemString(al_row, "act_chg_ind") = 'C' Then
	If MessageBox("Confirm", "Actual Scheduled or Actual Produced fields have changed. Continue?", + &
		Question!, YesNo!,1) <> 1 Then
		Return False
	End If
End If

If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

dw_sched_dtl.SelectRow(0, False)

Return True
end function

public function boolean wf_update ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp

string			ls_Update_string, &
					ls_header_string, &
					ls_output_string, &
					ls_ind, &
					ls_name_code, &
					ls_copy_plant, &
					ls_input
					

IF dw_sched_dtl.AcceptText() = -1 THEN
	Return( False )
End If
ll_temp = dw_sched_dtl.DeletedCount()


SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_sched_dtl.RowCount( )

dw_sched_dtl.SetReDraw(False)
ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_sched_dtl.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then
			iw_frame.SetMicroHelp("Update Canceled")
			dw_sched_dtl.SetReDraw(True)
			Return False
		End If
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_header_string = is_input
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_sched_dtl)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp65cr_upd_schedule"
istr_error_info.se_message = Space(71)

If not iu_pas201.nf_pasp65cr_upd_schedule(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then  Return False

iw_frame.SetMicroHelp("Update Successful")


ll_RowCount = dw_sched_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_sched_dtl.SetItem(li_Counter, 'update_flag', ' ')
	dw_sched_dtl.SetItem(li_Counter, 'qty_chg_ind', ' ')
	dw_sched_dtl.SetItem(li_Counter, 'act_chg_ind', ' ')
Next

dw_sched_dtl.ResetUpdate()
dw_sched_dtl.SetSort("sched_seq, product_code, product_state")
dw_sched_dtl.Sort()
dw_sched_dtl.SetFocus()
dw_sched_dtl.SetReDraw(True)

Return( True )
end function

public function boolean wf_recreate ();Integer		li_counter

Long			ll_rowcount
String		ls_input, &
				ls_header_string, &
				ls_update_string, &
				ls_output_string
				
dw_sched_dtl.SetReDraw(False)


ls_input = dw_header.GetItemString(1, 'plant_code')+ '~t' + &
			dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			dw_header.GetItemString(1, 'sect_name_code') + '~t' 
			

ls_header_string = ls_input
ls_Update_string = String(dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + &
dw_header.GetItemString(1, 'sched_shift') + '~t' + & 
'S~r~n'

istr_error_info.se_event_name = "wf_update_sched"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp61cr_upd_create_rel_sched"
istr_error_info.se_message = Space(71)

If not iu_pas201.nf_pasp61cr_upd_create_rel_sched(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then  Return False

iw_frame.SetMicroHelp("ReCreate Successful")

ib_ReInquire = True
wf_retrieve()

dw_sched_dtl.SetFocus()
dw_sched_dtl.SetReDraw(True)

Return( True )

end function

public function boolean wf_retrieve ();Boolean	lb_show_message

Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_output_values, ls_temp, &
			ls_name_code, &
			ls_filter, &
			ls_row_option
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_prd_sched_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	lb_show_message = True
Else
	lb_show_message = False
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
if lb_show_message Then
	SetMicroHelp("Retrieving ...")
end If

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			This.dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			This.dw_header.GetItemString(1, 'sect_name_code') + '~t' + &
			String(This.dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + & 
			This.dw_header.GetItemString(1, 'shift') + '~r~n'
						
is_input = ls_input

li_ret = iu_pas201.nf_pasp64cr_inq_schedule(istr_error_info, &
									is_input, &
									ls_output_values) 
						

This.dw_sched_dtl.Reset()

IF ll_value > 0 THEN
	dw_sched_dtl.SetFocus()
	dw_sched_dtl.ScrollToRow(1)
	dw_sched_dtl.SetColumn( "sched_date" )
	dw_sched_dtl.TriggerEvent("RowFocusChanged")
END IF

If lb_show_message then
	SetMicroHelp(String(ll_value) + " rows retrieved")
End If


If li_ret = 0 Then
	This.dw_sched_dtl.ImportString(ls_output_values)
	ls_row_option = dw_row_choice.GetItemString(1, 'choice')
	If ls_row_option = 'G' Then
		ls_filter = "(quantity > 0) And (act_sched > 0) And (act_prod > 0)" 
		This.dw_sched_dtl.SetFilter(ls_filter)
		This.dw_sched_dtl.Filter()
	Else
		This.dw_sched_dtl.SetFilter("")
		This.dw_sched_dtl.Filter()
	End if
End If

ll_value = dw_sched_dtl.RowCount()
If ll_value < 0 Then ll_value = 0

dw_sched_dtl.SetSort("sched_seq, product_code, product_state")
dw_sched_dtl.Sort()

dw_sched_dtl.ResetUpdate()
dw_row_choice.ResetUpdate()

IF ll_value > 0 THEN
	dw_sched_dtl.SetFocus()
	dw_sched_dtl.ScrollToRow(1)
	dw_sched_dtl.SetColumn( "quantity" )
	dw_sched_dtl.TriggerEvent("RowFocusChanged")
END IF

This.SetRedraw( True )

Return True

end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_pas203) Then Destroy(iu_pas203)


end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_sched_dtl.InsertRow(0)




end event

event ue_postopen;call super::ue_postopen;String						ls_char

DataWindowChild			ldwc_type, &
								ldwc_char, &
								ldwc_uom
								
Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Create Release Schedule"
istr_error_info.se_user_id = sqlca.userid

idwc_sub_desc.Reset()
dw_sched_dtl.GetChild("subsect_name_code", idwc_sub_desc)
//populate the description drop down
wf_get_sub_desc()

//populate the type drop down
dw_sched_dtl.GetChild('subsect_type', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PPSSTYPE")

// populate the characteristics drop down

iu_pas203 = Create u_pas203

istr_error_info.se_event_name = "ue_postconstructor"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_sched_dtl.GetChild("characteristic", ldwc_char)
ldwc_char.ImportString(ls_char)

//populate the type drop down
dw_sched_dtl.GetChild('uom', ldwc_uom)
ldwc_uom.SetTransObject(SQLCA)
ldwc_uom.Retrieve("FABUOM")


This.PostEvent("ue_query")


end event

on w_prd_sched.create
int iCurrent
call super::create
this.cb_recreate=create cb_recreate
this.dw_row_choice=create dw_row_choice
this.dw_header=create dw_header
this.dw_sched_dtl=create dw_sched_dtl
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_recreate
this.Control[iCurrent+2]=this.dw_row_choice
this.Control[iCurrent+3]=this.dw_header
this.Control[iCurrent+4]=this.dw_sched_dtl
end on

on w_prd_sched.destroy
call super::destroy
destroy(this.cb_recreate)
destroy(this.dw_row_choice)
destroy(this.dw_header)
destroy(this.dw_sched_dtl)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Area Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'area_name_code')
	Case 'Sect Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'sect_name_code')
	Case 'sched_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
	Case 'shift'
		Message.StringParm = dw_header.GetItemString(1, 'shift')
	Case 'Row Option'
		Message.StringParm = dw_row_choice.GetItemString(1, 'choice')
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Area Name Code'
		dw_header.SetItem(1, 'area_name_code', as_value)
	Case 'Area Descr'
		dw_header.SetItem(1, 'area_descr', as_value)
	Case 'Sect Name Code'
		dw_header.SetItem(1, 'sect_name_code', as_value)
	Case 'Sect Descr'
		dw_header.SetItem(1, 'sect_descr', as_value)
	Case 'sched_date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
	Case 'shift'
		dw_header.SetItem(1, 'shift', as_value)
	Case 'Row Option'
		dw_row_choice.SetItem(1, 'choice', as_value)
End Choose


end event

event resize;call super::resize;//integer li_x		
//integer li_y		
//
//li_x = (dw_sched_dtl.x * 2) + 30 
//li_y = dw_sched_dtl.y + 115
//
//if width > li_x Then
//	dw_sched_dtl.width	= width - li_x
//end if
//
//if height > li_y then
//	dw_sched_dtl.height	= height - li_y
//end if
end event

type cb_recreate from commandbutton within w_prd_sched
integer x = 2551
integer y = 96
integer width = 425
integer height = 92
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Recreate Section"
end type

event clicked;Date			ldt_sched_Date

Long			ll_row_count

dw_sched_dtl.AcceptText()
IF dw_sched_dtl.ModifiedCount() > 0 Then
	If MessageBox("Confirm", "Do you want to save changes before recreating section?", + &
			Question!, YesNo!,1) = 1 Then
			wf_update()
	End if
End If

ll_row_count = dw_sched_dtl.RowCount()

ldt_sched_Date = dw_header.GetItemDate(1,"sched_date")
if ldt_sched_date < today() Then
	SetMicroHelp("Cannot Recreate Schedule for Prior Days")
Else
	If ll_row_count > 0 Then
		Parent.PostEvent("ue_recreate")
	End If
End If

end event
type dw_row_choice from u_row_choice within w_prd_sched
integer x = 1554
integer y = 32
integer taborder = 30
end type

event itemchanged;call super::itemchanged;
String		ls_row_option, &
				ls_filter


dw_sched_dtl.SetReDraw(False)
ls_row_option = data
If ls_row_option = 'G' Then
	dw_sched_dtl.SetFilter("")
	dw_sched_dtl.Filter()
	ls_filter = "(quantity > 0) and (act_sched > 0) and (act_prod > 0)" 
	dw_sched_dtl.SetFilter(ls_filter)
	dw_sched_dtl.Filter()
Else
	dw_sched_dtl.SetFilter("")
	dw_sched_dtl.Filter()
End if

dw_sched_dtl.AcceptText()
dw_sched_dtl.SetSort("sched_sequence, product_code, product_state")
dw_sched_dtl.Sort()
dw_row_choice.ResetUpdate()
dw_sched_dtl.ResetUpdate()
dw_sched_dtl.SetReDraw(True)
end event

event constructor;call super::constructor;ib_updateable = False
end event

type dw_header from u_base_dw_ext within w_prd_sched
integer y = 4
integer width = 1289
integer height = 412
integer taborder = 20
boolean enabled = false
string dataobject = "d_prd_sched_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False




end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_sched_dtl from u_base_dw_ext within w_prd_sched
integer x = 18
integer y = 440
integer width = 4105
integer height = 1068
integer taborder = 10
string dataobject = "d_prd_sched_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;Long		ll_rtn
String	ls_temp, &
			ls_ColumnName, &
			ls_sched_date, &
			ls_shift
			
Boolean 	lb_color

If dwo.Type <> "column" and dwo.Type <> "button" Then Return

ls_temp = dwo.Type
If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return






end event

event constructor;call super::constructor;ib_updateable = True

end event

event itemerror;call super::itemerror;return (1)
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

long			ll_source_row, &
				ll_RowCount

String		ls_ColName
				

nvuo_pa_business_rules	u_rule

ls_ColName = GetColumnName()
ll_source_row	= GetRow()
il_ChangedRow = 0

If ls_ColName = "quantity" Then
	This.SetItem(ll_source_row, "qty_chg_ind", "Q")
End If

If ls_ColName = "act_sched" or ls_ColName = "act_prod" Then
	This.SetItem(ll_source_row, "act_chg_ind", "C")
End If

// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

