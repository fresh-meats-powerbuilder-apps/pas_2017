HA$PBExportHeader$u_complex2.sru
forward
global type u_complex2 from u_netwise_dw
end type
end forward

global type u_complex2 from u_netwise_dw
integer width = 1527
integer height = 96
string dataobject = "d_complex2"
boolean border = false
event ue_graph pbm_custom01
event ue_graph_type pbm_custom02
event ue_graph_name pbm_custom03
event ue_graph_data pbm_custom04
event ue_revisions ( )
end type
global u_complex2 u_complex2

type variables
datawindowchild idddw_child
end variables

forward prototypes
public function string uf_get_complex_code ()
public subroutine uf_disable ()
public subroutine uf_enable ()
public function string uf_get_complex_descr ()
public subroutine nf_set_complex_name (string as_complex_code)
public subroutine uf_set_complex_desc (string as_complex_code)
end prototypes

on ue_graph;call u_netwise_dw::ue_graph;//w_graph		w_grp_child

//OpenSheetWithParm( w_grp_child, this, iw_frame, 0, original! )
end on

event ue_graph_type;call super::ue_graph_type;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_type, lst_graph_axis.dw, iw_frame )
end event

event ue_graph_name;call super::ue_graph_name;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_name, lst_graph_axis, iw_frame )
end event

event ue_graph_data;call super::ue_graph_data;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_data, lst_graph_axis, iw_frame )
end event

event ue_revisions;/*****************************************************************
**   REVISION NUMBER: rev#01
**   PROJECT NUMBER:  support
**   DATE:				 july 99            
**   PROGRAMMER:      David Deal
**   PURPOSE:         added code in the constructor and ue_keydown
**                    event to write and check the ibpuser.ini
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

public function string uf_get_complex_code ();return Trim(This.GetItemString(1, "complex_code"))
end function

public subroutine uf_disable ();This.object.complex_code.Background.Color = 12632256
This.object.complex_code.Protect = 1

end subroutine

public subroutine uf_enable ();This.object.complex_code.Background.Color = 16777215
This.object.complex_code.Protect = 0

end subroutine

public function string uf_get_complex_descr ();return Trim(This.GetItemString(1, "complex_name"))
end function

public subroutine nf_set_complex_name (string as_complex_code);
end subroutine

public subroutine uf_set_complex_desc (string as_complex_code);Long	ll_row

ll_row = idddw_child.Find('type_code = "' + Trim(as_complex_code) + '"', 1, idddw_child.RowCount())

If ll_row > 0 Then
	This.SetItem(1, 'complex_name', idddw_child.GetItemString(ll_row, 'tutltypes_type_desc'))
	This.SetFocus()
	This.SelectText(1, Len(as_complex_code))
	SetProfileString( iw_frame.is_UserINI, "Pas", "Lastcomplex",Trim(as_complex_code))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End If


end subroutine

event constructor;call super::constructor;String				ls_complex_code
integer				li_sqlcode

ib_updateable = False

This.GetChild("complex_code", idddw_child)

idddw_child.SetTransObject(SQLCA)
li_sqlcode = idddw_child.Retrieve()

This.InsertRow(0)




end event

event ue_keydown;call super::ue_keydown;Long	ll_CurrentRow, &
		ll_CurrentColumn

String	ls_ColumnType, &
			ls_ColumnName, &
			ls_Text,&
			ls_value
			
// Get the current row and column that has focus
ll_CurrentRow = This.GetRow()
ll_CurrentColumn = This.GetColumn()
		
// Get the current column name
ls_ColumnName = GetColumnName()

// Get the column type of the clicked field	
ls_ColumnType = Lower(This.Describe("#" + String( &
									ll_CurrentColumn) + ".ColType"))

ls_Text = GetText()

Choose Case ls_ColumnType
	Case "date", "datetime"
		//rev#01 added if
		ls_value = ProfileString('ibpuser.ini', "PAS",'u_base_dw_ext.ue_keydown', "None") 
		If ls_value = 'Y'	Then
			Choose Case True
				Case KeyDown(KeyDownArrow!)
					SetText(String(RelativeDate(Date(ls_Text),-1)))				
				Case KeyDown(KeyUpArrow!)
					SetText(String(RelativeDate(Date(ls_Text),1)))			
			End Choose
		End If
End Choose
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1, 100)
end event

on u_complex2.create
end on

on u_complex2.destroy
end on

event itemchanged;call super::itemchanged;Long	ll_row

//If Len(Trim(data)) = 0 Then 
//	// User entered an empty plant code -- set fields to blank
//	// The last row is an empty row
//	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
//	This.ScrollToRow(This.RowCount())
//	This.SetItem(1,"complex_code","")
//	This.SetItem(1,"complex_name","")
//	return 
//End if
//
// Find the entered plant code in the table -- don't count the last row, it's blank
ll_row = idddw_child.Find('type_code = "' + Trim(data) + '"', 1, idddw_child.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(data + " is an Invalid Complex Code")
	This.SetFocus()
	This.SelectText(1, Len(data))
	return 1
Else
	This.SetItem(1, 'complex_name', idddw_child.GetItemString(ll_row, 'tutltypes_type_desc'))
	This.SetFocus()
	This.SelectText(1, Len(data))
	SetProfileString( iw_frame.is_UserINI, "Pas", "Lastcomplex",Trim(data))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

return 

end event

event getfocus;call super::getfocus;This.SelectText(1, Len(This.GetText()))
end event

event itemerror;call super::itemerror;Return 1
end event

