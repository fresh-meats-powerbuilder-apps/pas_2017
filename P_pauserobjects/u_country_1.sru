HA$PBExportHeader$u_country_1.sru
forward
global type u_country_1 from datawindow
end type
end forward

global type u_country_1 from datawindow
integer width = 731
integer height = 88
integer taborder = 10
string dataobject = "d_country"
boolean border = false
boolean livescroll = true
end type
global u_country_1 u_country_1

forward prototypes
public function string uf_get_country ()
public subroutine uf_set_country (string as_country)
public function long uf_enable (boolean ab_enable)
public function string uf_get_country_desc ()
end prototypes

public function string uf_get_country ();return This.GetItemString(1, "country")
end function

public subroutine uf_set_country (string as_country);This.SetItem(1,"country",as_country)
end subroutine

public function long uf_enable (boolean ab_enable);If ab_enable Then
	This.object.country.Background.Color = 16777215
	This.object.country.Protect = 0
Else
	This.object.country.Background.Color = 12632256
	This.object.country.Protect = 1
End If

Return 1
end function

public function string uf_get_country_desc ();Datawindowchild			ldwc_temp

String						ls_country_type

Long							ll_findrow

u_string_functions		lu_string


ls_country_type = This.GetItemString(1, 'country')

If lu_string.nf_IsEmpty(ls_country_type) Then Return ''

This.GetChild('country', ldwc_temp)
ll_findrow = ldwc_temp.Find('type_short_desc = "' + ls_country_type + '"', 1, ldwc_temp.RowCount())
If ll_findrow <= 0 Then Return ''

Return ldwc_temp.GetItemString(ll_findrow, 'type_desc')

end function

event constructor;DataWindowChild		ldwc_type

String	ls_country

This.GetChild("country", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("COUNTRYG")
This.InsertRow(0)

ls_country = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastCountry", "  ")
This.uf_set_country(ls_country)
end event

event destructor;SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastCountry", &
			This.uf_get_country())
end event

on u_country_1.create
end on

on u_country_1.destroy
end on

