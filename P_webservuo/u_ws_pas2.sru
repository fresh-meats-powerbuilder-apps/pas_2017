HA$PBExportHeader$u_ws_pas2.sru
forward
global type u_ws_pas2 from u_pas_webservice
end type
end forward

global type u_ws_pas2 from u_pas_webservice
end type
global u_ws_pas2 u_ws_pas2

forward prototypes
public function integer nf_pasp31fr (string as_header, string as_plant, ref string as_paspdav, ref s_error astr_error_info)
public function integer nf_pasp32fr (string as_header, string as_paspdav, s_error astr_error_info)
public function integer nf_pasp85gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function integer nf_pasp70gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_pasp71gr (string as_header_string, ref string as_input_string, ref s_error astr_error_info)
public function integer nf_pasp62fr (string as_input, ref string as_output, ref s_error astr_error_info)
public function boolean nf_pasp72gr (ref string as_input_string, ref s_error astr_error_info)
public function integer nf_pasp47gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_pasp48gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function integer nf_pasp45gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_pasp46gr (string as_update_string, ref string as_output_string, string as_header_string, ref s_error astr_error_info)
public function integer nf_pasp43gr (string as_input_string, ref string as_header_string, ref string as_output_string, ref s_error astr_error_info)
public function integer nf_pasp44gr (string as_update_string, string as_header_string_in, ref string as_header_string_out, ref s_error astr_error_info)
public function integer nf_pasp40gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_pasp41gr (string as_update_string, string as_header, s_error astr_error_info)
public function integer nf_pasp83gr (string as_header_string, ref string as_detail_string, ref s_error astr_error_info)
public function integer nf_pasp42gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function integer nf_pasp38gr (string as_input_string, ref string as_headerstring, ref string as_detailstring, ref string as_rmshstring, ref s_error astr_error_info)
public function boolean nf_pasp39gr (string as_update_string, ref string as_output_string, ref string as_header_string, ref s_error astr_error_info)
public function integer nf_pasp15gr (string as_input_strng, ref s_error astr_error_info)
public function integer nf_pasp36gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_pasp37gr (string as_update_string, ref string as_output_string, string as_header_string, ref s_error astr_error_info)
public function boolean nf_pasp32gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_pasp33gr (string as_update_string, string as_header_string, s_error astr_error_info)
public function boolean nf_pasp34gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_pasp35gr (string as_update_string, string as_header_string, ref s_error astr_error_info)
public function integer nf_pasp25gr (ref string as_outputstring, ref s_error astr_error_info)
public function integer nf_pasp26gr (string as_input_string, ref s_error astr_error_info)
end prototypes

public function integer nf_pasp31fr (string as_header, string as_plant, ref string as_paspdav, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring, ls_detail_info,ls_output, ls_stringind

integer li_rval

ls_program_name='PASP31FR'
ls_tran_id ='P31F'

ls_inputstring =as_header + '~t'

li_rval = uf_rpccall(ls_inputstring, as_paspdav, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp32fr (string as_header, string as_paspdav, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP32FR'
ls_tran_id ='P32F'

ls_inputstring =as_header + '~h7F' + as_paspdav 

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function integer nf_pasp85gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_detail_info,ls_output, ls_stringind

integer li_rval

ls_program_name='PASP85GR'
ls_tran_id ='P85G'

li_rval = uf_rpccall(as_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp70gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP70GR'
ls_tran_id ='P70G'

li_rval = uf_rpccall(as_input_string,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function boolean nf_pasp71gr (string as_header_string, ref string as_input_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP71GR'
ls_tran_id ='P71G'

ls_inputstring = as_header_string +  '~h7F' + as_input_string + '~h7F'

li_rval = uf_rpccall(ls_inputstring,ls_outputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 then
	return True
else
	return False
End If


end function

public function integer nf_pasp62fr (string as_input, ref string as_output, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP62FR'
ls_tran_id ='P62F'

li_rval = uf_rpccall(as_input,as_output,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function boolean nf_pasp72gr (ref string as_input_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP72GR'
ls_tran_id ='P72G'

li_rval = uf_rpcupd(as_input_string,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 then
	return true
else
	return false
end If

end function

public function integer nf_pasp47gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP47GR'
ls_tran_id ='P47G'


li_rval = uf_rpccall(as_input_string,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function boolean nf_pasp48gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP48GR'
ls_tran_id ='P48G'

li_rval = uf_rpccall(as_input_string,as_output_string, astr_error_info,ls_program_name,ls_tran_id)

IF li_rval = 0 then
	return true
else
	return false
END If
end function

public function integer nf_pasp45gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP45GR'
ls_tran_id ='P45G'


li_rval = uf_rpccall(as_input_string,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

return li_rval

end function

public function boolean nf_pasp46gr (string as_update_string, ref string as_output_string, string as_header_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP46GR'
ls_tran_id ='P46G'

ls_inputstring = as_header_string + '~h7F' + as_update_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If

end function

public function integer nf_pasp43gr (string as_input_string, ref string as_header_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output
u_String_Functions  lu_string
integer li_rval

ls_program_name='PASP43GR'
ls_tran_id ='P43G'

ls_inputstring = as_input_string + '~h7F' + as_header_string

li_rval = uf_rpccall(ls_inputstring,ls_outputstring,astr_error_info,ls_program_name,ls_tran_id)

as_header_string=''
as_output_string=''

IF li_rval = 0 Then
	ls_stringind=Mid(ls_outputstring,2,1)
	ls_outputstring = Mid(ls_outputstring,3)
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring,'~h7F')
		Choose Case ls_stringind
			Case 'H'
				as_header_string += ls_output
			Case 'D'
				as_output_string += ls_output
		End Choose
		ls_stringind = Left(ls_outputstring,1)
		ls_outputstring = Mid(ls_outputstring,2)
	Loop
End IF

return li_rval
end function

public function integer nf_pasp44gr (string as_update_string, string as_header_string_in, ref string as_header_string_out, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP44GR'
ls_tran_id ='P44G'

ls_inputstring = as_update_String + '~h7F' + as_header_string_in

li_rval = uf_rpccall(ls_inputstring,as_header_string_out,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function integer nf_pasp40gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP40GR'
ls_tran_id ='P40G'

li_rval = uf_rpccall(as_input_string,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

return li_rval

end function

public function boolean nf_pasp41gr (string as_update_string, string as_header, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP41GR'
ls_tran_id ='P41G'

ls_inputstring = as_update_string +'~h7F'+ as_header

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If

end function

public function integer nf_pasp83gr (string as_header_string, ref string as_detail_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP83GR'
ls_tran_id ='P83G'

li_rval = uf_rpccall(as_header_string,as_detail_string,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function integer nf_pasp42gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP42GR'
ls_tran_id ='P42G'

li_rval = uf_rpccall(as_input_string,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function integer nf_pasp38gr (string as_input_string, ref string as_headerstring, ref string as_detailstring, ref string as_rmshstring, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

u_String_Functions  lu_string

integer li_rval

ls_program_name='PASP38GR'
ls_tran_id ='P38G'

li_rval = uf_rpccall(as_input_string,ls_outputstring,astr_error_info,ls_program_name,ls_tran_id)

as_headerstring=''
as_detailstring=''
as_rmshstring=''

IF li_rval = 0 Then
	ls_stringind=Mid(ls_outputstring,2,1)
	ls_outputstring = Mid(ls_outputstring,3)
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring,'~h7F')
		Choose Case ls_stringind
			Case 'H'
				as_headerstring += ls_output
			Case 'D'
				as_detailstring += ls_output
			Case 'R'
				as_rmshstring += ls_output
		End Choose
		ls_stringind = Left(ls_outputstring,1)
		ls_outputstring = Mid(ls_outputstring,2)
	Loop
End IF

return li_rval
end function

public function boolean nf_pasp39gr (string as_update_string, ref string as_output_string, ref string as_header_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP39GR'
ls_tran_id ='P39G'

ls_inputstring = as_header_string + '~h7F'+ as_update_string

li_rval = uf_rpccall(ls_inputstring,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

if li_rval=0 then
	return true
else
	return false
end if
end function

public function integer nf_pasp15gr (string as_input_strng, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP15GR'
ls_tran_id ='P15G'

li_rval = uf_rpcupd(as_input_strng,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function integer nf_pasp36gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP36GR'
ls_tran_id ='P36G'

li_rval = uf_rpccall(as_input_string,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function boolean nf_pasp37gr (string as_update_string, ref string as_output_string, string as_header_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP37GR'
ls_tran_id ='P37G'

ls_inputstring = as_header_string + '~h7F' + as_update_string

li_rval = uf_rpccall(ls_inputstring,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

if li_rval = 0 Then
	return true
else
	return false
end if
end function

public function boolean nf_pasp32gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP32GR'
ls_tran_id ='P32G'


li_rval = uf_rpccall(as_input_string,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

if li_rval=0 then
	return true
else
	return false
end if
end function

public function boolean nf_pasp33gr (string as_update_string, string as_header_string, s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP33GR'
ls_tran_id ='P33G'

ls_inputstring = as_header_string + '~h7F' + as_update_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

if li_rval = 0 Then
	return true
else
	return false
end if
end function

public function boolean nf_pasp34gr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP34GR'
ls_tran_id ='P34G'


li_rval = uf_rpccall(as_input_string,as_output_string,astr_error_info,ls_program_name,ls_tran_id)

if li_rval=0 then
	return true
else
	return false
end if
end function

public function boolean nf_pasp35gr (string as_update_string, string as_header_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring

integer li_rval

ls_program_name='PASP35GR'
ls_tran_id ='P35G'

ls_inputstring = as_header_string + '~h7F' + as_update_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

if li_rval=0 then
	return true
else
	return false
end if
end function

public function integer nf_pasp25gr (ref string as_outputstring, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP25GR'
ls_tran_id ='P25G'

li_rval = uf_rpccall(ls_inputstring,as_outputstring,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

public function integer nf_pasp26gr (string as_input_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output, as_outputstring

integer li_rval

ls_program_name='PASP26GR'
ls_tran_id ='P26G'

li_rval = uf_rpccall(as_input_string,as_outputstring,astr_error_info,ls_program_name,ls_tran_id)

return li_rval
end function

on u_ws_pas2.create
call super::create
end on

on u_ws_pas2.destroy
call super::destroy
end on

