HA$PBExportHeader$u_ws_orp5.sru
forward
global type u_ws_orp5 from u_orp_webservice
end type
end forward

global type u_ws_orp5 from u_orp_webservice
end type
global u_ws_orp5 u_ws_orp5

forward prototypes
public function boolean nf_cfmc32fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info)
public function boolean nf_cfmc33fr (string as_input_string, ref s_error astr_error_info)
public function integer nf_orpo68fr (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, ref string as_inc_exec)
end prototypes

public function boolean nf_cfmc32fr (string as_input_string, ref string as_output_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval
u_String_Functions lu_string

ls_program_name = 'CFMC32FR'
ls_tran_id = 'C32F'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 then
	return true
else
	return false
end If
end function

public function boolean nf_cfmc33fr (string as_input_string, ref s_error astr_error_info);string ls_program_name, ls_tran_id
integer li_rval
u_String_Functions lu_string

ls_program_name = 'CFMC33FR'
ls_tran_id = 'C33F'

li_rval = uf_rpcupd(as_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer nf_orpo68fr (ref s_error astr_error_info, ref string as_header_in, ref string as_detail_in, ref string as_inc_exec);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval
string ls_stringsep

u_String_Functions lu_string

ls_program_name = 'ORPO68FR'
ls_tran_id = 'O68F'

ls_input_string = as_header_in + '~h7F' + as_detail_in + '~h7F'

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

ls_stringsep = Left(ls_OutputString, 1)

as_header_in = ""
as_detail_in = ""
as_inc_exec = ""

if li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, ls_stringsep)
		Choose Case ls_stringInd
			Case 'H' 
				as_header_in += ls_output
			Case 'D'
				as_detail_in += ls_output
			Case 'I' 
				as_inc_exec += ls_output
				
		End Choose
		ls_stringInd = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop
end if

return li_rval
end function

on u_ws_orp5.create
call super::create
end on

on u_ws_orp5.destroy
call super::destroy
end on

