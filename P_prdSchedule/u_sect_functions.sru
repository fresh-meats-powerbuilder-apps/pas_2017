HA$PBExportHeader$u_sect_functions.sru
$PBExportComments$This object will control the single row selection or multiple row selection of a datawindow
forward
global type u_sect_functions from nonvisualobject
end type
end forward

global type u_sect_functions from nonvisualobject autoinstantiate
end type

type variables
Private:
DataWindowChild		idwc_sect_desc
Integer		ii_SelectionType
Long		il_LastClickedRow, &
		il_selected
		
u_pas201		iu_pas201

s_error		istr_error_info
u_ws_pas5		iu_ws_pas5
end variables

forward prototypes
public function datawindowchild uf_set_visible_ind (ref datawindowchild adw_data_child, string as_name_code, string as_ind)
public function string uf_get_names (string as_input)
public function string uf_condense (string as_in_string, ref string as_out_string)
end prototypes

public function datawindowchild uf_set_visible_ind (ref datawindowchild adw_data_child, string as_name_code, string as_ind);
/* --------------------------------------------------------

<DESC>	Compare the list of name codes to what is in the
			the drop down
</DESC>

<ARGS>	DataWindow:DataWindow,
			SelectionType:Integer
</ARGS>
			
<USAGE>	Call this function to Initialize the object
			Valid selection types are:
			0 - Do not select any rows
			1 - Select only one row
			2 - Select multiple rows, one at a time
			3 - Select multiple rows with CTRL and SHIFT support for blocks
			4 - Select multiple rows with SHIFT support for blocks
</USAGE>
-------------------------------------------------------- */
Long			ll_rtn

String		ls_SearchString, &
				ls_avail_ind
				

idwc_sect_desc = adw_Data_child
ls_Searchstring = "name_code = "
ls_Searchstring += as_name_code
								
ll_rtn = idwc_sect_desc.Find  &
				( ls_SearchString, 1, idwc_sect_desc.RowCount())

If ll_rtn > 0 Then
	idwc_sect_desc.SetItem(ll_rtn, "available_ind", as_ind)
End If

Return idwc_sect_desc
end function

public function string uf_get_names (string as_input);
/* --------------------------------------------------------

<DESC>	Call PASP51CR to get the TPPSNAME records
</DESC>

<ARGS>	as_input:String
</ARGS>
			
<USAGE>	Call this function whenever you need to populate the datawindow
</USAGE>
-------------------------------------------------------- */

Integer	li_ret

String	ls_output_values


//iu_pas201 = Create u_pas201
//li_ret = iu_pas201.nf_pasp51cr_inq_area_sect_names(istr_error_info, &
//									as_input, &
//									ls_output_values) 
iu_ws_pas5 = Create u_ws_pas5
li_ret = iu_ws_pas5.nf_pasp51gr(istr_error_info, as_input, ls_output_values)

If li_ret > 0 Then
	ls_output_values = ''
End If

If IsValid(iu_pas201) Then Destroy(iu_pas201)
If IsValid(iu_ws_pas5) Then Destroy(iu_ws_pas5)
return ls_output_values
						

end function

public function string uf_condense (string as_in_string, ref string as_out_string);Boolean lb_found
String ls_char, ls_out
Long ll_pos

lb_found = FALSE

FOR ll_pos = 1 TO Len( as_in_string )
  ls_char = Mid( as_in_string, ll_pos, 1 )
  IF Asc(ls_char) >= Asc('!') AND Asc(ls_char) <= Asc('}') THEN
    ls_out += Upper(ls_char)
  ELSE
	 If lb_found Then
		if ls_char = '~n' then
			ls_out += '|'
		end if
		lb_found = FALSE
    Else
	 	If ls_char = '~r' Then
	  		lb_found = TRUE
	 	else
	   	ls_out += ' '
		end If
	 End if
  END IF
NEXT

//as_out_string = ls_out

RETURN ls_out
//Return true

end function

on u_sect_functions.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_sect_functions.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;
/* --------------------------------------------------------
u_DWSelect

<OBJECT>	This object is a utility object for selecting
			rows in a DataWindow. 
			The valid selection methods are:
			0 - Do not select any rows
			1 - Select only one row
			2 - Select multiple rows, one at a time
			3 - Select multiple rows with CTRL and SHIFT support for blocks
			4 - Select multiple rows with SHIFT support for blocks
			</OBJECT>
			
<USAGE>	Create this object and call uf_initialize().
 			The first parameter to uf_initialize is the 
			 DataWindow to act upon.  The second parameter
			 is the selection method.</USAGE>

<AUTH>	Tim Bornholtz	</AUTH>

--------------------------------------------------------- */

end event

