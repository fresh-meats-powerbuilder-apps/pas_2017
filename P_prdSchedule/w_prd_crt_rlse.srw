HA$PBExportHeader$w_prd_crt_rlse.srw
forward
global type w_prd_crt_rlse from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_prd_crt_rlse
end type
type dw_crt_rlse_dtl from u_base_dw_ext within w_prd_crt_rlse
end type
end forward

global type w_prd_crt_rlse from w_base_sheet_ext
integer width = 5440
integer height = 2172
string title = "Create Release Schedule"
long backcolor = 67108864
dw_header dw_header
dw_crt_rlse_dtl dw_crt_rlse_dtl
end type
global w_prd_crt_rlse w_prd_crt_rlse

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product	

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

u_pas201		iu_pas201
u_ws_pas5		iu_ws_pas5
u_pas203		iu_pas203

String		is_input, &
				is_debug, &
				is_name_string
	

u_sect_functions		iu_sect_functions
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update_sched (long al_row, string as_type)
end prototypes

public function boolean wf_retrieve ();Boolean	lb_show_message

Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_output_values, ls_temp, &
			ls_name_code
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_prd_crt_rlse_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	lb_show_message = True
Else
	lb_show_message = False
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
if lb_show_message Then
	SetMicroHelp("Retrieving ...")
end If

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			This.dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			String(This.dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~r~n'
			
is_input = ls_input

//li_ret = iu_pas201.nf_pasp60cr_inq_create_rel_sched(istr_error_info, &
//									is_input, &
//									ls_output_values) 

li_ret = iu_ws_pas5.nf_pasp60gr(istr_error_info, &
									is_input, &
									ls_output_values) 
						

This.dw_crt_rlse_dtl.Reset()


If li_ret = 0 Then
	This.dw_crt_rlse_dtl.ImportString(ls_output_values)
End If

dw_crt_rlse_dtl.ResetUpdate()

IF ll_value > 0 THEN
	dw_crt_rlse_dtl.SetFocus()
	dw_crt_rlse_dtl.ScrollToRow(1)
	dw_crt_rlse_dtl.SetColumn( "sched_date" )
	dw_crt_rlse_dtl.TriggerEvent("RowFocusChanged")
END IF

If lb_show_message then
	SetMicroHelp(String(ll_value) + " rows retrieved")
End If

This.SetRedraw( True )

dw_crt_rlse_dtl.ResetUpdate()

Return True

end function

public function boolean wf_update_sched (long al_row, string as_type);Integer		li_counter

Long			ll_rowcount
	
String		ls_input, &
				ls_header_string, &
				ls_update_string, &
				ls_output_string, &
				ls_dummy_num
		
				
dw_crt_rlse_dtl.SetReDraw(False)

Choose case upper(as_type)
	case 'C'  
	iw_frame.SetMicroHelp("Wait...Creating Schedule")
	//modified 10-12 jac
	case 'R' 
	   iw_frame.SetMicroHelp("Wait...Releasing Schedule")	
   case 'P'  
	   iw_frame.SetMicroHelp("Wait...Previewing Schedule")	
	// pjm 09/05/2004 - added for Material Handling
	case 'M'
		iw_frame.SetMicroHelp("Wait...Sending Schedule")
End Choose

SetPointer(HourGlass!)

ls_dummy_num = '0'

ls_input = dw_header.GetItemString(1, 'plant_code')+ '~t' + &
			dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			ls_dummy_num + '~t'
is_input = ls_input


ls_header_string = is_input
ls_Update_string = String(dw_crt_rlse_dtl.GetItemDate(al_row, 'sched_date'), 'yyyy-mm-dd') + '~t' + &
dw_crt_rlse_dtl.GetItemString(al_row, 'sched_shift') + '~t' + & 
as_type +'~r~n'

istr_error_info.se_event_name = "wf_update_sched"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp61cr_upd_create_rel_sched"
istr_error_info.se_message = Space(71)

//If not iu_pas201.nf_pasp61cr_upd_create_rel_sched(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then
If not iu_ws_pas5.nf_pasp61gr(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then
	dw_crt_rlse_dtl.SetReDraw(True)
	Return False
End If

//iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_crt_rlse_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_crt_rlse_dtl.SetItem(li_Counter, 'update_flag', ' ')
Next

dw_crt_rlse_dtl.ResetUpdate()
ib_ReInquire = True
wf_retrieve()

dw_crt_rlse_dtl.SetFocus()
dw_crt_rlse_dtl.SetReDraw(True)

Return( True )
end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_pas203) Then Destroy(iu_pas203)

If IsValid(iu_ws_pas5) Then Destroy(iu_ws_pas5)


end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_crt_rlse_dtl.InsertRow(0)




end event

event ue_postopen;call super::ue_postopen;String						ls_char

DataWindowChild			ldwc_type, &
								ldwc_char, &
								ldwc_state
								
Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if
iu_ws_pas5 = Create u_ws_pas5

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Create Release Schedule"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")

end event

on w_prd_crt_rlse.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_crt_rlse_dtl=create dw_crt_rlse_dtl
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_crt_rlse_dtl
end on

on w_prd_crt_rlse.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_crt_rlse_dtl)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Area Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'area_name_code')
	Case 'sched_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Area Name Code'
		dw_header.SetItem(1, 'area_name_code', as_value)
	Case 'Area Descr'
		dw_header.SetItem(1, 'type_descr', as_value)
	Case 'sched_date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
End Choose


end event

event resize;call super::resize;integer li_x		
integer li_y
integer ly_height = 115

li_x = (dw_crt_rlse_dtl.x * 2) + 30 
//li_y = dw_crt_rlse_dtl.y + 115

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ly_height Then
                ly_height = il_BorderPaddingHeight
End If

li_y = dw_crt_rlse_dtl.y + ly_height

if width > li_x Then
	dw_crt_rlse_dtl.width	= width - li_x
end if

if height > li_y then
	dw_crt_rlse_dtl.height	= height - li_y
end if
end event

type dw_header from u_base_dw_ext within w_prd_crt_rlse
integer y = 4
integer width = 1371
integer height = 240
integer taborder = 0
boolean enabled = false
string dataobject = "d_prd_carcass_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False




end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_crt_rlse_dtl from u_base_dw_ext within w_prd_crt_rlse
integer y = 384
integer width = 5326
integer height = 1068
integer taborder = 10
string dataobject = "d_crt_rlse_dtl"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;Long		ll_rtn
String	ls_temp, &
			ls_ColumnName, &
			ls_sched_date, &
			ls_shift
			
Boolean 	lb_color

If dwo.Type <> "column" and dwo.Type <> "button" Then Return

ls_temp = dwo.Type
If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return

if dwo.name = 'create_b' Then
	ls_sched_date = String(GetItemDate(row, "sched_date"))
	ls_shift = GetItemString(row, "sched_shift")
	If MessageBox("Confirm", "Create schedule for " + ls_sched_date +  &
		", " + ls_shift + " shift", Question!, YesNo!,1) <> 1 Then
		Return
	Else
		wf_update_sched(row,'C')
	End if
End if

if dwo.name = 'release_b' Then
	ls_sched_date = String(GetItemDate(row, "sched_date"))
	If MessageBox("release_b", "Release schedule for " + ls_sched_date,  &
		Question!, YesNo!,1) <> 1 Then
		Return
	Else
		wf_update_sched(row,'R')
	End If
end if

//modified 10-12 jac
if dwo.name = 'preview_b' Then
	ls_sched_date = String(GetItemDate(row, "sched_date"))
	If MessageBox("preview_b", "Preview schedule for " + ls_sched_date,  &
		Question!, YesNo!,1) <> 1 Then
		Return
	Else
		wf_update_sched(row,'P')
	End If
end if

// pjm 09/05/2014 added for pasp60cr
if dwo.name = 'send_to_mh_b' Then
	ls_sched_date = String(GetItemDate(row, "send_to_MH_date"))
	If MessageBox("Send to Material Handling", "Send schedule for " + ls_sched_date,  &
		Question!, YesNo!,1) <> 1 Then
		Return
	Else
		wf_update_sched(row,'M')
	End If
end if



end event

event constructor;call super::constructor;ib_updateable = True

end event

event itemerror;call super::itemerror;return (1)
end event

