HA$PBExportHeader$u_sched_area.sru
forward
global type u_sched_area from datawindow
end type
end forward

global type u_sched_area from datawindow
integer width = 1106
integer height = 100
integer taborder = 10
string dataobject = "d_prd_area"
boolean maxbox = true
boolean border = false
boolean livescroll = true
end type
global u_sched_area u_sched_area

type variables
u_pas201		iu_pas201

s_error		istr_error_info

DataWindowChild		idwc_type
u_ws_pas5		iu_ws_pas5
end variables

forward prototypes
public function integer uf_enable (boolean ab_enable)
public function integer uf_modified ()
public function string uf_get_type ()
public subroutine uf_set_type (string as_sched_type)
public function string nf_get_name_code ()
public function string uf_get_name_code ()
public function string uf_get_type_descr ()
end prototypes

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.shift.Background.Color = 16777215
	This.object.shift.Protect = 0
Else
	This.object.shift.Background.Color = 12632256
	This.object.shift.Protect = 1
End If

Return 1
end function

public function integer uf_modified ();return 0
end function

public function string uf_get_type ();Return This.GetItemString(1, "sched_type")
end function

public subroutine uf_set_type (string as_sched_type);	This.SetItem(1,"sched_type",as_sched_type)
  
end subroutine

public function string nf_get_name_code ();return This.GetItemString(1, "name_code")
end function

public function string uf_get_name_code ();return This.GetItemString(1, "name_code")
end function

public function string uf_get_type_descr ();Long			ll_rtn

String		ls_SearchString, &
				ls_temp

ls_Searchstring = "name_code = "
ls_Searchstring += "'" + This.GetItemString(1, "name_code") + "'"
								
ll_rtn = idwc_type.Find  &
				( ls_SearchString, 1, idwc_type.RowCount())
				
ls_temp = idwc_type.GetItemString(ll_rtn, "type_descr")

Return ls_temp
end function

event constructor;Integer	li_ret, &
			li_rtn

String	ls_output_values, &
			ls_input


iu_pas201 = Create u_pas201
iu_ws_pas5	= Create u_ws_pas5

ls_input = 'AREA~r~n'

//li_ret = iu_pas201.nf_pasp51cr_inq_area_sect_names(istr_error_info, &
//									ls_input, &
//									ls_output_values) 

li_ret = iu_ws_pas5.nf_pasp51gr(istr_error_info, ls_input, ls_output_values)

If li_ret > 0 Then
	ls_output_values = ''
End If

If IsValid(iu_pas201) Then Destroy(iu_pas201)
If IsValid(iu_ws_pas5) Then Destroy(iu_ws_pas5)
li_rtn = This.GetChild('name_code', idwc_type)
idwc_type.Reset()
li_rtn = idwc_type.ImportString(ls_output_values)
						
This.InsertRow(0)

end event

on u_sched_area.create
end on

on u_sched_area.destroy
end on

event getfocus;If This.GetColumnName() = "name_code" Then
	This.SelectText(1, Len(This.GetText()))
End if
end event

event itemerror;Return 1
end event

