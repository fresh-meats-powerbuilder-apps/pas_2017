HA$PBExportHeader$w_prd_area_plt_inq.srw
forward
global type w_prd_area_plt_inq from w_base_response
end type
type dw_plant from u_plant within w_prd_area_plt_inq
end type
type dw_area from u_sched_area within w_prd_area_plt_inq
end type
end forward

global type w_prd_area_plt_inq from w_base_response
integer width = 1495
integer height = 632
string title = "Schedule Section Names Inquire by Plant"
long backcolor = 67108864
event ue_query pbm_custom70
dw_plant dw_plant
dw_area dw_area
end type
global w_prd_area_plt_inq w_prd_area_plt_inq

type variables
Boolean		ib_valid_return
w_base_sheet	iw_parent

u_pas201		iu_pas201

s_error		istr_error_info

DataWindowChild	idwc_temp

u_sect_functions	iu_sect_functions
end variables

on w_prd_area_plt_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_area=create dw_area
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_area
end on

on w_prd_area_plt_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_area)
end on

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

//This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
//			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))


Window      lw_parent 

lw_parent = this.parentwindow()

This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
	This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

If dw_area.RowCount() = 0 Then
	dw_area.InsertRow(0)
End If

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;String	ls_temp, &
			ls_SearchString
Long 		ll_rtn
Integer	li_rtn, &
			li_temp

If dw_plant.AcceptText() = -1 or &
	dw_area.AcceptText() = -1 Then
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_area.GetItemString(1, "name_code")) Then
	iw_frame.SetMicroHelp("Area is a required field")
	dw_area.SetFocus()
	return
End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant Desc', ls_temp)

ls_temp = 'AREA'
iw_parent.Event ue_Set_Data('Name Type', ls_temp)

ls_temp = dw_area.uf_get_name_code()
iw_parent.Event ue_Set_Data('Name Code', ls_temp)

ls_temp = dw_area.uf_get_type_descr()
iw_parent.Event ue_Set_Data('Type Descr', ls_temp)

ib_valid_return = True
Close(This)
end event

event ue_postopen;call super::ue_postopen;Int							li_pos, &
								li_ret
								
String						ls_sect_name, &
								ls_temp

u_string_functions		lu_string

Environment					le_env


iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If

iw_parent.Event ue_Get_Data('Name Code')
ls_sect_name = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_sect_name) Then 
	dw_area.SetItem(1,"name_code",ls_sect_name)
End If
  
istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Schedule Section Names by Plant"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")



end event

type cb_base_help from w_base_response`cb_base_help within w_prd_area_plt_inq
boolean visible = false
integer x = 1129
integer y = 596
integer taborder = 0
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_prd_area_plt_inq
integer x = 631
integer y = 340
integer taborder = 40
end type

type cb_base_ok from w_base_response`cb_base_ok within w_prd_area_plt_inq
integer x = 183
integer y = 340
integer taborder = 30
end type

type dw_plant from u_plant within w_prd_area_plt_inq
integer x = 5
integer y = 64
integer taborder = 10
boolean bringtotop = true
end type

type dw_area from u_sched_area within w_prd_area_plt_inq
integer y = 152
integer taborder = 20
boolean bringtotop = true
end type

