HA$PBExportHeader$w_prd_sched.srw
forward
global type w_prd_sched from w_base_sheet_ext
end type
type dw_dtl_sum_choice from u_base_dw_ext within w_prd_sched
end type
type cb_1 from commandbutton within w_prd_sched
end type
type cb_recreate from commandbutton within w_prd_sched
end type
type dw_row_choice from u_row_choice within w_prd_sched
end type
type dw_header from u_base_dw_ext within w_prd_sched
end type
type dw_sched_dtl from u_base_dw_ext within w_prd_sched
end type
end forward

global type w_prd_sched from w_base_sheet_ext
integer x = 389
integer y = 468
integer width = 4996
integer height = 1661
string title = "Update Schedule"
long backcolor = 67108864
event ue_recreate pbm_custom70
dw_dtl_sum_choice dw_dtl_sum_choice
cb_1 cb_1
cb_recreate cb_recreate
dw_row_choice dw_row_choice
dw_header dw_header
dw_sched_dtl dw_sched_dtl
end type
global w_prd_sched w_prd_sched

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_cancel_save, &
				ib_field_changed

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count


Long			il_ChangedRow, &
				il_count

s_error		istr_error_info

u_pas201		iu_pas201
u_pas203		iu_pas203
u_ws_pas3		iu_ws_pas3
u_ws_pas5		iu_ws_pas5
u_ws_pas1		iu_ws_pas1

String		is_input, &
				is_debug, &
				is_name_string
	

u_sect_functions		iu_sect_functions

DataWindowChild		idwc_sub_desc
end variables

forward prototypes
public function boolean wf_get_sub_desc ()
public function boolean wf_recreate ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_validate (long al_row)
end prototypes

event ue_recreate;wf_recreate()
end event

public function boolean wf_get_sub_desc ();Integer	li_row_Count
				
String	ls_input, &
			ls_output_values
						
u_string_functions u_string

DataWindowChild	ldwc_temp


idwc_sub_desc.Reset()

ls_input = 'SUBSECT~r~n'

ls_output_values = iu_sect_functions.uf_get_names(ls_input)

li_row_count = idwc_sub_desc.ImportString(ls_output_values)

Return True

end function

public function boolean wf_recreate ();Integer		li_counter

Long			ll_rowcount
String		ls_input, &
				ls_header_string, &
				ls_update_string, &
				ls_output_string
				
dw_sched_dtl.SetReDraw(False)

ls_input = dw_header.GetItemString(1, 'plant_code')+ '~t' + &
			dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			dw_header.GetItemString(1, 'sect_name_code') + '~t' 
			

ls_header_string = ls_input
ls_Update_string = String(dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + &
dw_header.GetItemString(1, 'shift') + '~t' + & 
'S~r~n'

istr_error_info.se_event_name = "wf_update_sched"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp61cr_upd_create_rel_sched"
istr_error_info.se_message = Space(71)

//If not iu_pas201.nf_pasp61cr_upd_create_rel_sched(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then 
If not iu_ws_pas5.nf_pasp61gr(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then
	dw_sched_dtl.SetReDraw(True)
	Return False
End If

iw_frame.SetMicroHelp("ReCreate Successful")

ib_ReInquire = True
wf_retrieve()

dw_sched_dtl.SetFocus()
dw_sched_dtl.SetReDraw(True)

Return( True )

end function

public function boolean wf_retrieve ();Boolean	lb_show_message

Integer	li_ret, &
			li_row_Count, &
			li_counter, &
			li_visible
			
String	ls_input, &
			ls_output_values, ls_temp, &
			ls_name_code, &
			ls_filter, &
			ls_row_option, &
			ls_dtl_sum_option
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_prd_sched_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	lb_show_message = True
Else
	idwc_sub_desc.Reset()
	dw_sched_dtl.GetChild("subsect_name_code", idwc_sub_desc)
	//populate the description drop down
	wf_get_sub_desc()
	lb_show_message = False
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
if lb_show_message Then
	SetMicroHelp("Retrieving ...")
end If

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			This.dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			This.dw_header.GetItemString(1, 'sect_name_code') + '~t' + &
			String(This.dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + & 
			This.dw_header.GetItemString(1, 'shift') + '~t' + &
			This.dw_dtl_sum_choice.GetItemString(1,'choice') + '~r~n'
						
is_input = ls_input

//li_ret = iu_pas201.nf_pasp64cr_inq_schedule(istr_error_info, &
//									is_input, &
//									ls_output_values) 

li_ret = iu_ws_pas5.nf_pasp64gr(istr_error_info, &
									is_input, &
									ls_output_values) 
						
						

This.dw_sched_dtl.Reset()

IF ll_value > 0 THEN
	dw_sched_dtl.SetFocus()
	dw_sched_dtl.ScrollToRow(1)
	dw_sched_dtl.SetColumn( "sched_date" )
	dw_sched_dtl.TriggerEvent("RowFocusChanged")
END IF

If lb_show_message then
	SetMicroHelp(String(ll_value) + " rows retrieved")
End If


If li_ret = 0 Then
	This.dw_sched_dtl.ImportString(ls_output_values)
	ls_row_option = dw_row_choice.GetItemString(1, 'choice')
	If ls_row_option = 'G' Then
		ls_filter = "(quantity > 0) or (act_sched > 0) or (act_prod > 0)" 
		This.dw_sched_dtl.SetFilter(ls_filter)
		This.dw_sched_dtl.Filter()
	Else
		This.dw_sched_dtl.SetFilter("")
		This.dw_sched_dtl.Filter()
	End if
End If

// if summary option was chosen then turn off visibility for the following fields.
ls_dtl_sum_option = dw_dtl_sum_choice.GetItemString(1, 'choice')
If ls_dtl_sum_option = 'S' Then
	li_visible = 0
	//cb_1.Enabled = false
else
	li_visible = 1
	//cb_1.Enabled = true
end if

This.dw_sched_dtl.object.subsect_name_code.Visible = li_visible
This.dw_sched_dtl.object.subsect_type.Visible = li_visible
This.dw_sched_dtl.object.characteristic.Visible = li_visible
This.dw_sched_dtl.object.from_prod_date.Visible = li_visible
This.dw_sched_dtl.object.to_prod_date.Visible = li_visible
This.dw_sched_dtl.object.pcs_lbs.Visible = li_visible
This.dw_sched_dtl.object.uom.Visible = li_visible
This.dw_sched_dtl.object.mix_pct.Visible = li_visible
This.dw_sched_dtl.object.short_comment.Visible = li_visible
This.dw_sched_dtl.object.char_t.Visible = li_visible
This.dw_sched_dtl.object.date_t.Visible = li_visible
This.dw_sched_dtl.object.pcs_lbs_t.Visible = li_visible
This.dw_sched_dtl.object.uom_t.Visible = li_visible
This.dw_sched_dtl.object.mix_pct_t.Visible = li_visible
This.dw_sched_dtl.object.comment_t.Visible = li_visible

ll_value = dw_sched_dtl.RowCount()
If ll_value < 0 Then ll_value = 0

This.dw_sched_dtl.Event ue_set_hspscroll()

dw_sched_dtl.ResetUpdate()
dw_row_choice.ResetUpdate()
dw_dtl_sum_choice.ResetUpdate()

IF ll_value > 0 THEN
	dw_sched_dtl.SetFocus()
	dw_sched_dtl.ScrollToRow(1)
	dw_sched_dtl.SetColumn( "quantity" )
	dw_sched_dtl.TriggerEvent("RowFocusChanged")
END IF
//unselect current row because sort and groupcalc will not unselect the currentrow or reset row index
dw_sched_dtl.SELECTROW(dw_sched_dtl.GETROW(),false)

dw_sched_dtl.SetSort("sched_sequence, subsect_name_code, subsect_type, product_code, product_state")
dw_sched_dtl.Sort()
dw_sched_dtl.GroupCalc()


Return True

end function

public function boolean wf_update ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp

string			ls_Update_string, &
					ls_header_string, &
					ls_output_string, &
					ls_ind, &
					ls_name_code, &
					ls_copy_plant, &
					ls_input, &
					ls_row_option, &
					ls_filter
					

IF dw_sched_dtl.AcceptText() = -1 THEN
	Return( False )
End If
ll_temp = dw_sched_dtl.DeletedCount()


SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_sched_dtl.RowCount( )

dw_sched_dtl.SetReDraw(False)
ll_row = 0

ib_field_changed = FALSE

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_sched_dtl.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then
			If ib_cancel_save Then
				iw_frame.SetMicroHelp("Update Canceled")
			End If
			dw_sched_dtl.SetReDraw(True)
			Return False
		End If
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

If ib_field_changed Then
	If MessageBox("Confirm", "Actual Scheduled or Actual Produced fields have changed. Continue?", + &
		Question!, YesNo!,1) <> 1 Then
		iw_frame.SetMicroHelp("Update Canceled")
		dw_sched_dtl.SetReDraw(True)
		Return False
	End If
End If


ls_header_string = is_input
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_sched_dtl)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp65cr_upd_schedule"
istr_error_info.se_message = Space(71)

//If not iu_pas201.nf_pasp65cr_upd_schedule(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then  Return False
If not iu_ws_pas1.nf_pasp65gr(istr_error_info, ls_Update_string, ls_header_string) Then  Return False

iw_frame.SetMicroHelp("Update Successful")


ll_RowCount = dw_sched_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_sched_dtl.SetItem(li_Counter, 'update_flag', ' ')
	dw_sched_dtl.SetItem(li_Counter, 'qty_chg_ind', ' ')
	dw_sched_dtl.SetItem(li_Counter, 'act_chg_ind', ' ')
Next

ls_row_option = dw_row_choice.GetItemString(1, 'choice')
If ls_row_option = 'G' Then
	ls_filter = "(quantity > 0) or (act_sched > 0) or (act_prod > 0)" 
	This.dw_sched_dtl.SetFilter(ls_filter)
	This.dw_sched_dtl.Filter()
Else
	This.dw_sched_dtl.SetFilter("")
	This.dw_sched_dtl.Filter()
End if

dw_sched_dtl.ResetUpdate()
dw_sched_dtl.SetFocus()
dw_sched_dtl.SetReDraw(True)

Return( True )
end function

public function boolean wf_validate (long al_row);Date					ldt_from_prod_date, &
						ldt_to_prod_date, &
						ldt_sched_date
						
Long					ll_rtn, &
						ll_nbrrows, &
						ll_qty

String				ls_division_code, &
						ls_temp


ll_nbrrows = dw_sched_dtl.RowCount()

//check that the quantity is >= zero and < 10000000
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "quantity") 
If ll_qty > 9999999 Then 
	iw_Frame.SetMicroHelp("Quantity must be less than 9999999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("quantity")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("Quantity must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("quantity")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the uom qty is >= zero and < 100000
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "uom_qty") 
If ll_qty > 999999999 Then 
	iw_Frame.SetMicroHelp("UOM Qty must be less than 999999999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("uom_qty")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("UOM Qty must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("uom_qty")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the pcs lbs >= 0 and < 999999999
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "pcs_lbs") 
If ll_qty > 9999 Then 
	iw_Frame.SetMicroHelp("PCS LBS must be less than 9999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("pcs_lbs")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("PCS LBS must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("pcs_lbs")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the mix pct >= 0 and < 999
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "mix_pct") 
If ll_qty > 9999 Then 
	iw_Frame.SetMicroHelp("MIX PCT must be less than 9999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("mix_pct")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("MIX PCT must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("mix_pct")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the act sched >= 0 and < 9999999
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "act_sched") 
If ll_qty > 9999999 Then 
	iw_Frame.SetMicroHelp("Act Sched must be less than 9999999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("act_sched")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("Act Sched must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("act_sched")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check that the act prod >= 0 and < 9999999
ll_qty = dw_sched_dtl.GetItemNumber(al_row, "act_prod") 
If ll_qty > 9999999 Then 
	iw_Frame.SetMicroHelp("Act Prod must be less than 9999999")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("act_prod")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ll_qty < 0 Then
	iw_Frame.SetMicroHelp("Act Prod must be greater than zero")
	This.SetRedraw(False)
	dw_sched_dtl.ScrollToRow(al_row)
	dw_sched_dtl.SetColumn("act_prod")
	dw_sched_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If


// Check that the production dates are <= the schedule date

ldt_from_prod_date= dw_sched_dtl.GetItemDate(al_row, "from_prod_date")
ldt_sched_date = dw_header.GetItemDate(1, "sched_date")
IF dw_sched_dtl.GetItemString(al_row, "date_ind") = 'N' Then
	IF IsNull(ldt_from_prod_date) or string(ldt_from_prod_date) <= ('00/00/0000') Then 
		iw_Frame.SetMicroHelp("Please enter a From Date greater than 00/00/0000")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("from_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End If	
	IF ldt_from_prod_date> ldt_sched_date Then 
		iw_Frame.SetMicroHelp("Please enter a From Date less than the Schedule Date")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("from_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
	If ldt_from_prod_date> dw_sched_dtl.GetItemDate(al_row, "to_prod_date") Then
		iw_Frame.SetMicroHelp("Please enter a From Date less than the To Date")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("from_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End IF

// Check that the end date has a value and is < schedule date.
	ldt_to_prod_date = dw_sched_dtl.GetItemDate(al_row, "to_prod_date")
	IF IsNull(ldt_to_prod_date) or string(ldt_to_prod_date) <= ('00/00/0000') Then 
		iw_Frame.SetMicroHelp("Please enter a To Date greater than 00/00/0000")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("to_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
	IF ldt_to_prod_date > ldt_sched_date Then 
		iw_Frame.SetMicroHelp("Please enter a To Date less than the Schedule Date")
		This.SetRedraw(False)
		dw_sched_dtl.ScrollToRow(al_row)
		dw_sched_dtl.SetColumn("to_prod_date")
		dw_sched_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
	//If ldt_temp < dw_sched_dtl.GetItemDate(al_row, "from_prod_date") Then
	//		iw_Frame.SetMicroHelp("Please enter a To Date greater than the From Date")
	//		This.SetRedraw(False)
	//		dw_sched_dtl.ScrollToRow(al_row)
	//		dw_sched_dtl.SetColumn("to_prod_date")
	//		dw_sched_dtl.SetFocus()
	//		This.SetRedraw(True)
	//		Return False
	//End If
End If

ib_cancel_save = False

If dw_sched_dtl.GetItemString(al_row, "qty_chg_ind") = 'Q' and il_count > 0 Then
	If ldt_sched_Date < today() Then
		If MessageBox("Confirm", "Quantity has changed for a schedule date prior to current date. Continue?", + &
			Question!, YesNo!,1) <> 1 Then
			ib_cancel_save = True
			Return False
		End if
		il_count = 0
	End If
End If

If dw_sched_dtl.GetItemString(al_row, "act_chg_ind") = 'C' Then
//	If MessageBox("Confirm", "Actual Scheduled or Actual Produced fields have changed. Continue?", + &
//		Question!, YesNo!,1) <> 1 Then
//		ib_cancel_save = True
//		Return False
//	End If
	ib_field_changed = TRUE
End If

If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

dw_sched_dtl.SelectRow(0, False)

Return True
end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_pas203) Then Destroy(iu_pas203)

If IsValid(iu_ws_pas3) Then Destroy(iu_ws_pas3)

If IsValid(iu_ws_pas5) Then Destroy(iu_ws_pas5)

If IsValid(iu_ws_pas1) Then Destroy(iu_ws_pas1)


end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_sched_dtl.InsertRow(0)




end event

event ue_postopen;call super::ue_postopen;String						ls_char

DataWindowChild			ldwc_type, &
								ldwc_char, &
								ldwc_uom
								
Environment		le_env

iu_pas201 = Create u_pas201
iu_ws_pas5 = Create u_ws_pas5
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Create Release Schedule"
istr_error_info.se_user_id = sqlca.userid

idwc_sub_desc.Reset()
dw_sched_dtl.GetChild("subsect_name_code", idwc_sub_desc)
//populate the description drop down
wf_get_sub_desc()

//populate the type drop down
dw_sched_dtl.GetChild('subsect_type', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PPSSTYPE")

// populate the characteristics drop down

iu_pas203 = Create u_pas203
iu_ws_pas3 = Create u_ws_pas3
iu_ws_pas1	= Create u_ws_pas1

istr_error_info.se_event_name = "ue_postconstructor"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

//If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
If iu_ws_pas3.uf_pasp61fr(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_sched_dtl.GetChild("characteristic", ldwc_char)
ldwc_char.ImportString(ls_char)

//populate the type drop down
dw_sched_dtl.GetChild('uom', ldwc_uom)
ldwc_uom.SetTransObject(SQLCA)
ldwc_uom.Retrieve("FABUOM")


This.PostEvent("ue_query")


end event

on w_prd_sched.create
int iCurrent
call super::create
this.dw_dtl_sum_choice=create dw_dtl_sum_choice
this.cb_1=create cb_1
this.cb_recreate=create cb_recreate
this.dw_row_choice=create dw_row_choice
this.dw_header=create dw_header
this.dw_sched_dtl=create dw_sched_dtl
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_dtl_sum_choice
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_recreate
this.Control[iCurrent+4]=this.dw_row_choice
this.Control[iCurrent+5]=this.dw_header
this.Control[iCurrent+6]=this.dw_sched_dtl
end on

on w_prd_sched.destroy
call super::destroy
destroy(this.dw_dtl_sum_choice)
destroy(this.cb_1)
destroy(this.cb_recreate)
destroy(this.dw_row_choice)
destroy(this.dw_header)
destroy(this.dw_sched_dtl)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Area Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'area_name_code')
	Case 'Sect Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'sect_name_code')
	Case 'sched_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
	Case 'shift'
		Message.StringParm = dw_header.GetItemString(1, 'shift')
	Case 'Row Option'
		Message.StringParm = dw_row_choice.GetItemString(1, 'choice')
	Case 'Detail Sum Option'
		Message.StringParm = dw_dtl_sum_choice.GetItemString(1, 'choice')
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Area Name Code'
		dw_header.SetItem(1, 'area_name_code', as_value)
	Case 'Area Descr'
		dw_header.SetItem(1, 'area_descr', as_value)
	Case 'Sect Name Code'
		dw_header.SetItem(1, 'sect_name_code', as_value)
	Case 'Sect Descr'
		dw_header.SetItem(1, 'sect_descr', as_value)
	Case 'sched_date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
	Case 'shift'
		dw_header.SetItem(1, 'shift', as_value)
	Case 'Row Option'
		dw_row_choice.SetItem(1, 'choice', as_value)
	Case 'Detail Sum Option'
		dw_dtl_sum_choice.SetItem(1, 'choice', as_value)
End Choose


end event

event resize;call super::resize;integer li_x		
integer li_y
integer ly_height = 115

li_x = (dw_sched_dtl.x * 2) + 30 
//li_y = dw_sched_dtl.y + 115

if il_BorderPaddingHeight > ly_height Then
                ly_height = il_BorderPaddingHeight
End If


li_y = dw_sched_dtl.y + ly_height


if width > li_x Then
	dw_sched_dtl.width	= width - li_x
end if

if height > li_y then
	dw_sched_dtl.height	= height - li_y
end if

This.dw_sched_dtl.Event ue_set_hspscroll()
end event

type dw_dtl_sum_choice from u_base_dw_ext within w_prd_sched
integer x = 1349
integer y = 48
integer width = 432
integer height = 221
integer taborder = 30
boolean enabled = false
string dataobject = "d_dtl_sum_choice"
boolean border = false
end type

event constructor;call super::constructor;if this.rowcount() = 0 then
	this.insertrow(0)
end if

end event

type cb_1 from commandbutton within w_prd_sched
integer x = 3131
integer y = 96
integer width = 377
integer height = 93
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Adjust Sched"
end type

event clicked;long		ll_rowcount, &
			li_index, &
			ll_value

dwItemStatus	le_RowStatus

ll_rowcount  =  dw_sched_dtl.RowCount()

For li_index = 1 to ll_rowcount
		IF dw_sched_dtl.IsSelected(li_index) Then
			il_count = il_count +1	
			ll_value = dw_sched_dtl.GetItemNumber(li_index,'act_prod')
			dw_sched_dtl.SetItem(li_index,'act_sched',ll_value)
			dw_sched_dtl.SetItem(li_index, "act_chg_ind", "C")
			dw_sched_dtl.SetItem(li_index,'quantity',ll_value)
			dw_sched_dtl.SetItem(li_index, "qty_chg_ind", "Q")
			le_RowStatus = dw_sched_dtl.GetItemStatus(li_index, 0, Primary!)
				CHOOSE CASE le_RowStatus
				CASE NewModified!, New!
					dw_sched_dtl.SetItem(li_index, "update_flag", "A")
				CASE DataModified!, NotModified!
					dw_sched_dtl.SetItem(li_index, "update_flag", "U")
				END CHOOSE
		END IF
	NEXT

IF il_count > 0 then
	MessageBox('Information','Selected Records Have Been Changed')
ELSE
	MessageBox('Information','You must choose at least one Record')
END IF	
end event

type cb_recreate from commandbutton within w_prd_sched
integer x = 2553
integer y = 96
integer width = 424
integer height = 93
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Recreate Section"
end type

event clicked;Date			ldt_sched_Date

Long			ll_row_count

dw_sched_dtl.AcceptText()
IF dw_sched_dtl.ModifiedCount() > 0 Then
	If MessageBox("Confirm", "Do you want to save changes before recreating section?", + &
			Question!, YesNo!,1) = 1 Then
			wf_update()
	End if
End If

ll_row_count = dw_sched_dtl.RowCount()

ldt_sched_Date = dw_header.GetItemDate(1,"sched_date")
if ldt_sched_date < today() Then
	SetMicroHelp("Cannot Recreate Schedule for Prior Days")
Else
	If ll_row_count > 0 Then
		Parent.PostEvent("ue_recreate")
	End If
End If

end event

type dw_row_choice from u_row_choice within w_prd_sched
integer x = 1825
integer y = 51
integer width = 666
integer height = 218
integer taborder = 40
end type

event itemchanged;call super::itemchanged;
String		ls_row_option, &
				ls_filter


dw_sched_dtl.SetReDraw(False)
ls_row_option = data
If ls_row_option = 'G' Then
	dw_sched_dtl.SetFilter("")
	dw_sched_dtl.Filter()
	ls_filter = "(quantity > 0) or (act_sched > 0) or (act_prod > 0)" 
	dw_sched_dtl.SetFilter(ls_filter)
	dw_sched_dtl.Filter()
Else
	dw_sched_dtl.SetFilter("")
	dw_sched_dtl.Filter()
End if

dw_sched_dtl.SELECTROW(dw_sched_dtl.GETROW(),false)

dw_sched_dtl.AcceptText()
dw_sched_dtl.SetSort("sched_sequence, subsect_name_code, subsect_type, product_code, product_state")
dw_sched_dtl.Sort()
dw_sched_dtl.GroupCalc()
dw_row_choice.ResetUpdate()
//dw_sched_dtl.ResetUpdate()
dw_sched_dtl.SetReDraw(True)
end event

event constructor;call super::constructor;ib_updateable = False
end event

type dw_header from u_base_dw_ext within w_prd_sched
integer y = 3
integer width = 1287
integer height = 413
integer taborder = 20
boolean enabled = false
string dataobject = "d_prd_sched_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False




end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_sched_dtl from u_base_dw_ext within w_prd_sched
event ue_set_hspscroll ( )
integer x = 18
integer y = 442
integer width = 4776
integer height = 880
integer taborder = 10
string dataobject = "d_prd_sched_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_set_hspscroll();Int	li_position, &
		li_position2


if this.hsplitscroll then
	If Integer(This.Object.DataWindow.HorizontalScrollMaximum) <= 1 Then return
	
	li_position = Integer(This.Object.characteristic.X) + &
						Integer(This.Object.characteristic.Width) + 15
	li_position2 = Integer(This.Object.characteristic.X) + &
						Integer(This.Object.characteristic.Width) + 15
	
	This.Object.DataWindow.HorizontalScrollSplit = String(li_position)
	This.Object.DataWindow.HorizontalScrollPosition2 =  String(li_position2)
end if
end event

event clicked;call super::clicked;Long		ll_rtn
String	ls_temp, &
			ls_ColumnName, &
			ls_sched_date, &
			ls_shift
			
Boolean 	lb_color

If dwo.Type <> "column" and dwo.Type <> "button" Then Return

ls_temp = dwo.Type
If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return






end event

event constructor;call super::constructor;ib_updateable = True
is_selection = '4'
end event

event itemerror;call super::itemerror;return (1)
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

long			ll_source_row, &
				ll_RowCount

String		ls_ColName
				

nvuo_pa_business_rules	u_rule

ls_ColName = GetColumnName()
ll_source_row	= GetRow()
il_ChangedRow = 0

If ls_ColName = "quantity" Then
	This.SetItem(ll_source_row, "qty_chg_ind", "Q")
End If

If ls_ColName = "act_sched" or ls_ColName = "act_prod" Then
	This.SetItem(ll_source_row, "act_chg_ind", "C")
End If

// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

