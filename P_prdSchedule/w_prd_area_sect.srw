HA$PBExportHeader$w_prd_area_sect.srw
forward
global type w_prd_area_sect from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_prd_area_sect
end type
type dw_sched_name_dtl from u_base_dw_ext within w_prd_area_sect
end type
end forward

global type w_prd_area_sect from w_base_sheet_ext
integer width = 987
integer height = 1028
string title = "Schedule Section Names"
long backcolor = 67108864
dw_header dw_header
dw_sched_name_dtl dw_sched_name_dtl
end type
global w_prd_area_sect w_prd_area_sect

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire			

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

u_pas201		iu_pas201

String		is_input, &
				is_debug
				
w_base_sheet	iw_order_detail
u_ws_pas5 	iu_ws_pas5
end variables

forward prototypes
public subroutine wf_delete ()
public function string wf_getheaderinfo ()
public function boolean wf_validate (long al_row)
public function boolean wf_deleterow ()
public function boolean wf_update ()
public function boolean wf_addrow ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function string wf_getheaderinfo ();String		ls_string


ls_string = dw_header.GetItemString(1, 'sched_type') + '~t' 

return ls_string


end function

public function boolean wf_validate (long al_row);Long					ll_rtn, &
						ll_nbrrows, &
						ll_ModifiedRow
						
String				ls_searchstring, &
						ls_temp, &
						ls_update_flag, &
						ls_sched_type, &
						ls_area_name
											
						
IF dw_sched_name_dtl.AcceptText() = -1 THEN Return( False )

ll_nbrrows = dw_sched_name_dtl.RowCount()
ls_sched_type = dw_header.GetItemString(1,"sched_type")

ls_temp = dw_sched_name_dtl.GetItemString(al_row, "type_descr")
IF IsNull(ls_temp) Then 
	MessageBox("type_descr", "Please enter an area name.")
	This.SetRedraw(False)
	dw_sched_name_dtl.ScrollToRow(al_row)
	dw_sched_name_dtl.SetColumn("type_descr")
	dw_sched_name_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
Else
// allow a blank name for 'SUB SECT'
	If ls_sched_type = 'SUB SECT' Then
		//do nothing
	Else
		If Len(trim(ls_temp)) = 0 Then
			MessageBox("type_descr", "Please enter an area name.")
			This.SetRedraw(False)
			dw_sched_name_dtl.ScrollToRow(al_row)
			dw_sched_name_dtl.SetColumn("type_descr")
			dw_sched_name_dtl.SetFocus()
			This.SetRedraw(True)
			Return False
		End If
	End If
End If

If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

ls_area_name = dw_sched_name_dtl.GetItemString(al_row, "type_descr")
						
ls_SearchString	=	"type_descr = '" + ls_area_name + "'"


CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_sched_name_dtl.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_sched_name_dtl.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_sched_name_dtl.Find  &
			(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_sched_name_dtl.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE

If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "There are duplicate area names")
		dw_sched_name_dtl.SetRedraw(False)
		dw_sched_name_dtl.ScrollToRow(al_row)
		dw_sched_name_dtl.SetColumn("type_descr")
		dw_sched_name_dtl.SetRow(al_row)
		dw_sched_name_dtl.SelectRow(ll_rtn, True)
		dw_sched_name_dtl.SelectRow(al_row, True)
		dw_sched_name_dtl.SetRedraw(True)
		Return False
End if

Return True
end function

public function boolean wf_deleterow ();Boolean		lb_ret

Long			ll_row

ll_row = dw_sched_name_dtl.GetRow()
dw_sched_name_dtl.SetItem(ll_row, "update_flag", 'D')

dw_sched_name_dtl.SetFocus()
lb_ret = super::wf_deleterow()

dw_sched_name_dtl.SelectRow(0,False)
dw_sched_name_dtl.SelectRow(dw_sched_name_dtl.GetRow(),True)

return lb_ret
end function

public function boolean wf_update ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp

string			ls_Update_string, &
					ls_header_string, &
					ls_output_string
					

IF dw_sched_name_dtl.AcceptText() = -1 THEN Return( False )
ll_temp = dw_sched_name_dtl.DeletedCount()
IF dw_sched_name_dtl.ModifiedCount() + dw_sched_name_dtl.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_sched_name_dtl.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_sched_name_dtl.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_header_string = is_input
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_sched_name_dtl)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp52cr_upd_area_sect_names"
istr_error_info.se_message = Space(71)

//If not iu_pas201.nf_pasp52cr_upd_area_sect_names(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then  Return False
If not iu_ws_pas5.nf_pasp52gr(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then Return False
dw_sched_name_dtl.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")
ll_RowCount = dw_sched_name_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_sched_name_dtl.SetItem(li_Counter, 'update_flag', ' ')
Next

dw_sched_name_dtl.ResetUpdate()
dw_sched_name_dtl.Sort()
dw_sched_name_dtl.SetFocus()
dw_sched_name_dtl.SetReDraw(True)
dw_sched_name_dtl.GroupCalc()
ib_reinquire = True
wf_retrieve()

Return( True )
end function

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row

If dw_sched_name_dtl.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_current_row = dw_sched_name_dtl.GetRow()

If ll_current_row > 0 Then
	ll_row = dw_sched_name_dtl.InsertRow(ll_current_row + 1)
Else
	ll_row = dw_sched_name_dtl.InsertRow(0)	
End If

dw_sched_name_dtl.ScrollToRow(ll_row)
dw_sched_name_dtl.SetItem(ll_row, "update_flag", 'A')
dw_sched_name_dtl.SetItem(ll_row, "name_code", ' ')
dw_sched_name_dtl.SetColumn("type_descr")
dw_sched_name_dtl.SetFocus()
This.SetRedraw(True)

return true



end function

public function boolean wf_retrieve ();Integer	li_ret, &
			li_row_Count
String	ls_input, &
			ls_output_values, ls_temp
			
Long		ll_value, &
			ll_rtn
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

ls_input = Message.StringParm
If Not ib_ReInquire Then
	OpenWithParm(w_prd_area_sect_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")
is_debug = This.dw_header.GetItemString(1, 'sched_type')

ls_input = This.dw_header.GetItemString(1, "sched_type") + '~r~n'
							
is_input = ls_input

//li_ret = iu_pas201.nf_pasp51cr_inq_area_sect_names(istr_error_info, &
//									is_input, &
//									ls_output_values) 
				
li_ret = iu_ws_pas5.nf_pasp51gr(istr_error_info, &
									is_input, &
									ls_output_values)				

This.dw_sched_name_dtl.Reset()

If li_ret = 0 Then
	This.dw_sched_name_dtl.ImportString(Trim(ls_output_values))
End If

ll_value = dw_sched_name_dtl.RowCount()
If ll_value < 0 Then ll_value = 0

dw_sched_name_dtl.ResetUpdate()

IF ll_value > 0 THEN
	dw_sched_name_dtl.SetFocus()
	dw_sched_name_dtl.ScrollToRow(1)
	dw_sched_name_dtl.SetColumn( "type_descr" )
	dw_sched_name_dtl.TriggerEvent("RowFocusChanged")
END IF

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )

dw_sched_name_dtl.ResetUpdate()

Return True

end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)
If isvalid(iu_ws_pas5) Then destroy(iu_ws_pas5)




end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;
wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event open;call super::open;String				ls_text, &
						ls_input_string, &
						ls_name

Long					ll_len

dw_header.InsertRow(0)
dw_sched_name_dtl.InsertRow(0)


ls_input_string = Message.StringParm

ll_len = len(ls_input_string)
if ll_len > 1 then
	if Message.StringParm = 'OK' then
		ib_reinquire = False
	else
		dw_header.SetItem(1, "sched_type", ls_input_string)
		dw_header.acceptText()
		ib_reinquire = True
	End if	
	dw_header.ib_Updateable=False
	Message.StringParm = ''
End if
end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_pas201 = Create u_pas201
iu_ws_pas5 = create u_ws_pas5
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Schedule Section Names"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")

end event

on w_prd_area_sect.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_sched_name_dtl=create dw_sched_name_dtl
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_sched_name_dtl
end on

on w_prd_area_sect.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_sched_name_dtl)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'sched_type'
		Message.StringParm = dw_header.GetItemString(1, 'sched_type')	
	End Choose



end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'sched_type'
		dw_header.SetItem(1, 'sched_type', as_value)	
End Choose

end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_sched_name_dtl.x * 2) + 30 
li_y = dw_sched_name_dtl.y + 145

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


if width > li_x Then
	dw_sched_name_dtl.width	= width - li_x
end if

if height > li_y then
	dw_sched_name_dtl.height	= height - li_y
end if
end event

type dw_header from u_base_dw_ext within w_prd_area_sect
integer y = 4
integer width = 859
integer height = 72
integer taborder = 0
boolean enabled = false
string dataobject = "d_sched_type"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
DataWindowChild		ldwc_type

This.Modify("sched_type.BackGround.Color = 67108864")
//if (update_flag = 'A', 16777215, 67108864) 


end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_sched_name_dtl from u_base_dw_ext within w_prd_area_sect
integer y = 124
integer width = 901
integer height = 772
integer taborder = 20
string dataobject = "d_sched_sect_names"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return


end event

event constructor;call super::constructor;ib_updateable = True

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

//dw_rmt_detail.GetChild('uom', ldwc_state)
//ldwc_state.SetTransObject(SQLCA)
//ldwc_state.Retrieve("RAWUOM")
//
//dw_rmt_detail.GetChild('product_state', ldwc_state)
//ldwc_state.SetTransObject(SQLCA)
//ldwc_state.Retrieve("PRDSTATE")
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

long			ll_source_row, &
				ll_RowCount
				

nvuo_pa_business_rules	u_rule


ll_source_row	= GetRow()
il_ChangedRow = 0


// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;return (1)
end event

event getfocus;call super::getfocus;This.selecttext(1,100)	
end event

event doubleclicked;call super::doubleclicked;This.selecttext(1,100)	
end event

