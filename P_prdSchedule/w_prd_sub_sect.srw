HA$PBExportHeader$w_prd_sub_sect.srw
forward
global type w_prd_sub_sect from w_base_sheet_ext
end type
type dw_find_characteristic from u_base_dw_ext within w_prd_sub_sect
end type
type cb_find_char from commandbutton within w_prd_sub_sect
end type
type cb_find_product from commandbutton within w_prd_sub_sect
end type
type cb_resequence from commandbutton within w_prd_sub_sect
end type
type dw_sub_section_copy from u_base_dw_ext within w_prd_sub_sect
end type
type cb_copy_to_subsection from commandbutton within w_prd_sub_sect
end type
type cb_copy_plant from commandbutton within w_prd_sub_sect
end type
type rb_deselect from radiobutton within w_prd_sub_sect
end type
type rb_select from radiobutton within w_prd_sub_sect
end type
type dw_copy_plant from u_plant within w_prd_sub_sect
end type
type dw_find_product from u_base_dw_ext within w_prd_sub_sect
end type
type dw_header from u_base_dw_ext within w_prd_sub_sect
end type
type dw_sub_sect_dtl from u_base_dw_ext within w_prd_sub_sect
end type
type gb_select from groupbox within w_prd_sub_sect
end type
end forward

global type w_prd_sub_sect from w_base_sheet_ext
integer width = 5198
integer height = 1732
string title = "Schedule Sub Section "
long backcolor = 67108864
event ue_resequence pbm_custom70
event ue_find_product pbm_custom70
event ue_find_characteristic ( )
dw_find_characteristic dw_find_characteristic
cb_find_char cb_find_char
cb_find_product cb_find_product
cb_resequence cb_resequence
dw_sub_section_copy dw_sub_section_copy
cb_copy_to_subsection cb_copy_to_subsection
cb_copy_plant cb_copy_plant
rb_deselect rb_deselect
rb_select rb_select
dw_copy_plant dw_copy_plant
dw_find_product dw_find_product
dw_header dw_header
dw_sub_sect_dtl dw_sub_sect_dtl
gb_select gb_select
end type
global w_prd_sub_sect w_prd_sub_sect

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_copy_plant, &
				ib_copy_sub_sect

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count, &
		ii_row_count


s_error		istr_error_info

u_pas201		iu_pas201
u_pas203		iu_pas203
u_ws_pas3		iu_ws_pas3
u_ws_pas5		iu_ws_pas5
String		is_input, &
				is_debug, &
				is_name_string, &
				is_data
				
long 			il_product_seq
	
DataWindowChild		idwc_sub_desc, &
							idwc_total_name_desc, &
							idwc_sub_desc_copy


u_sect_functions		iu_sect_functions
end variables

forward prototypes
public function boolean wf_unstring_output (string as_output_string)
public function boolean wf_set_plant_copy ()
public function boolean wf_validate_type ()
public subroutine wf_delete ()
public function boolean wf_populate_sub_section_header ()
public function boolean wf_build_sub_section ()
public function boolean wf_duplicate_prod_seq ()
public function boolean wf_validate_display ()
public function boolean wf_check_dups ()
public function boolean wf_set_sub_copy ()
public function boolean wf_addrow ()
public function boolean wf_find_product ()
public function boolean wf_get_sub_desc ()
public function boolean wf_resequence ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function boolean wf_retrieve ()
public function boolean wf_find_characteristic ()
public function boolean wf_deleterow ()
end prototypes

event ue_resequence;

choose case MessageBox ( this.title, "Do you want to Resequence", &
						Question!, YesNo!)
		case 1
			wf_resequence()
		case 2
			return 0
		end choose
//end if





//wf_resequence()
end event

event ue_find_product;wf_find_product()
end event

event ue_find_characteristic();wf_find_characteristic()
end event

public function boolean wf_unstring_output (string as_output_string);Integer				li_counter

Long					ll_rtn, &
						ll_nbrrows, &
						ll_value

String				ls_product_code, &
						ls_Searchstring
												

u_string_functions	lu_string_functions
					
ll_nbrrows = dw_sub_sect_dtl.RowCount()

dw_sub_sect_dtl.SelectRow(0,False)

ls_product_code = lu_string_functions.nf_gettoken(as_output_string, '~t')

ls_SearchString = 	"product_code = '"+ ls_product_code +  "'" 
							
ll_rtn = dw_sub_sect_dtl.Find  &
					( ls_SearchString, 1, ll_nbrrows)
		
If ll_rtn > 0 Then
	dw_sub_sect_dtl.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(ll_rtn)
	dw_sub_sect_dtl.SetRow(ll_rtn)
	dw_sub_sect_dtl.SelectRow(ll_rtn, True)
	dw_sub_sect_dtl.SetRedraw(True)
End If

as_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function

public function boolean wf_set_plant_copy ();Long			ll_row

ll_row = 1
ll_row = dw_sub_sect_dtl.GetSelectedRow(0)
If ll_row = 0 Then
	iw_frame.SetMicroHelp("No rows have been selected to copy")
	Return False
End If

Do While ll_row <> 0
	IF ll_row <> 0 THEN 
		dw_sub_sect_dtl.SetItem(ll_row, 'update_flag', 'A')
	End If
	
	ll_row = dw_sub_sect_dtl.GetSelectedRow(ll_row)
Loop

ib_copy_plant = True
Return True
end function

public function boolean wf_validate_type ();Boolean		lb_first_seq

Integer		li_counter, &
				li_subsect_seq 

Long			ll_row_count

String		ls_name_type
//				ls_display_ind


//The 'input raw material' type must have the lowest sequence number

dw_sub_sect_dtl.SetSort("subsect_seq")
dw_sub_sect_dtl.Sort()

ll_row_count = dw_sub_sect_dtl.RowCount()

li_counter = 1

if ll_row_count < li_counter then Return True

ls_name_type = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_type")
li_subsect_seq = dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_seq")
lb_first_seq = True

Do While li_counter <= ll_Row_count 
	if lb_first_seq = True Then
		lb_first_seq = False
	Else
		if ls_name_type = 'INRAWMAT' Then
			If li_subsect_seq  = dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_seq") Then
			  //do nothing
			Else
				iw_frame.SetMicroHelp("Input Raw Material must be before all other sequence numbers")
				dw_sub_sect_dtl.ScrollToRow(li_counter)
				dw_sub_sect_dtl.SetRow(li_counter)
				Return False
			End If
		End If
	End If
	li_counter = li_counter + 1
	
	If li_counter <= ll_Row_count Then
		
		If li_subsect_seq  = dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_seq") Then
			//do nothing
		Else
			ls_name_type = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_type")
			li_subsect_seq  = dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_seq")
			lb_first_seq = True
		End If
	End if
Loop
Return True
end function

public subroutine wf_delete ();wf_deleterow()



end subroutine

public function boolean wf_populate_sub_section_header ();Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_SearchString, &
			ls_filter, &
			ls_name_code, &
			ls_update_flag
			
Long		ll_value, &
			ll_rtn, &
			ll_row


//reset all rows to "N" - not available
dw_sub_sect_dtl.AcceptText()
dw_sub_section_copy.SetReDraw(False)
idwc_sub_desc_copy.SetFilter("")
idwc_sub_desc_copy.Filter()
li_Row_count = idwc_sub_desc_copy.RowCount()
For li_counter = 1 to li_Row_count
	idwc_sub_desc_copy.SetItem(li_counter, "available_ind", 'N')
next

//check to see if on detail window - set to "Y"es if they are
li_Row_count = dw_sub_Sect_dtl.RowCount()

For li_counter = 1 to li_Row_count
	ls_name_code = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code"))
	ls_Searchstring = "name_code = "
   ls_Searchstring += ls_name_code
	ll_rtn = idwc_sub_desc_copy.Find  &
				( ls_SearchString, 1, idwc_sub_desc_copy.RowCount())
//
   
	If ll_rtn > 0 Then
			idwc_sub_desc_copy.SetItem(ll_rtn, "available_ind", 'Y')

	End if
Next
//filter on "Y"es - available - this means they are on the detail screen
//li_Row_count = idwc_sub_desc_copy.RowCount()
//idwc_sub_desc_copy.SetFilter("")
//idwc_sub_desc_copy.Filter()
ls_filter = "available_ind = 'Y'" 
idwc_sub_desc_copy.SetFilter(ls_filter)
idwc_sub_desc_copy.Filter()
li_Row_count = idwc_sub_desc_copy.RowCount()
ll_rtn = idwc_sub_desc_copy.InsertRow(li_row_count + 1)
idwc_sub_desc_copy.SetItem(ll_rtn, "available_ind", 'Y')
idwc_sub_desc_copy.SetItem(ll_rtn, "name_code", ' ')
idwc_sub_desc_copy.SetItem(ll_rtn, "type_descr", ' ')
idwc_sub_desc_copy.SetSort("type_descr")
idwc_sub_desc_copy.Sort()
li_Row_count = idwc_sub_desc_copy.RowCount()

dw_sub_section_copy.AcceptText()
dw_sub_section_copy.SetReDraw(True)
Return True

end function

public function boolean wf_build_sub_section ();Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_build_string, &
			ls_output_values, ls_temp, &
			ls_name_code, &
			ls_total_ind, &
			ls_namecode, &
			ls_char
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string

//ls_build_string = ' '
li_Row_count = dw_sub_sect_dtl.RowCount()

If li_Row_count < 0 Then li_Row_count = 0

For li_counter = 1 to li_Row_count
	ls_name_code = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code"))
	if ls_name_code > ' ' then
		ls_build_string = ls_build_string  + ls_name_code + '~r~n'
	end if
	//idwc_sub_desc = iu_sect_functions.uf_set_visible_ind(idwc_sub_desc, ls_name_code, 'N') 
	//dw_sub_section_copy.SetItem(li_counter, "sub_section", ls_name_code)
Next	

if ls_build_string > ' ' then
//	dw_sub_section_copy.ImportString(ls_build_string)
	dw_sub_section_copy.GetChild("sub_section", idwc_sub_desc_copy)
	idwc_sub_desc_copy.Reset()
	idwc_sub_desc_copy.ImportString(ls_build_string)	
end if

Return True

end function

public function boolean wf_duplicate_prod_seq ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp, &
					ll_rtn, &
					ll_current_row, &
					ll_next_row

string			ls_name_code, &
					ls_product_seq, &
					ls_char_copy, &
					ls_searchstring
					
u_string_functions		lu_string_functions

IF dw_sub_sect_dtl.AcceptText() = -1 &
	And dw_copy_plant.AcceptText() = -1 &
	And dw_sub_section_copy.AcceptText() = -1 THEN
	Return( False )
End If


ll_RowCount = dw_sub_sect_dtl.RowCount()	

For li_Counter = 1 to ll_RowCount
	dw_sub_sect_dtl.SetItem(li_counter, "prod_seq_color", 'N')	
next

ll_next_row = 1

For li_Counter = 1 to ll_RowCount
	ls_name_code = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code"))
	ls_product_seq = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "product_seq"))
	ls_SearchString = "subsect_name_code = "
	ls_Searchstring += ls_name_code 
	ls_SearchString += " and product_seq = "
	ls_Searchstring += ls_product_seq 
	ll_next_row = ll_next_row + 1
	if ll_next_row <= ll_rowCount then
		ll_rtn = dw_sub_sect_dtl.Find  &
		( ls_SearchString, ll_next_row, dw_sub_sect_dtl.RowCount())
		If ll_rtn > 0 Then
			dw_sub_sect_dtl.SetItem(ll_rtn, "prod_seq_color", 'Y')
			dw_sub_sect_dtl.SetItem(li_counter, "prod_seq_color", 'Y')
		end if
	end if
Next

//dw_sub_sect_dtl.ResetUpdate()
//dw_sub_sect_dtl.ResetUpdate()
//dw_find_product.ResetUpdate()
//dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
//dw_sub_sect_dtl.Sort()
//dw_sub_sect_dtl.SetFocus()
//dw_sub_sect_dtl.SetReDraw(True)
//dw_sub_sect_dtl.ScrollToRow(ll_current_row)
//
Return( True )
end function

public function boolean wf_validate_display ();Boolean		lb_yes_found

Integer		li_counter, &
				li_product_seq

Long			ll_row_count

String		ls_namecode, &
				ls_display_ind

DataStore	lds_detail


//at least one row must have a display ind = 'Y' for each
//subsect name code/product seq combination
lds_detail = Create DataStore
lds_detail.DataObject = "d_sched_sub_sect"

//dw_sub_sect_dtl.SetSort("subsect_name_code, product_seq")
dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()

ll_row_count = dw_sub_sect_dtl.RowCount()

li_counter = 1

if ll_row_count < li_counter then Return True

ls_namecode = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code"))
li_product_seq = dw_sub_sect_dtl.GetItemNumber(li_counter, "product_seq")
lb_yes_found = False

Do While li_counter <= ll_Row_count 
	ls_display_ind = trim(dw_sub_sect_dtl.GetItemString(li_counter, "display_ind"))
	if ls_display_ind = 'Y' Then
		lb_yes_found = True
	End If
	li_counter = li_counter + 1
	
	If li_counter <= ll_Row_count Then
		If ls_namecode = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code")) &
			and li_product_seq = dw_sub_sect_dtl.GetItemNumber(li_counter, "product_seq") Then
			ls_display_ind = dw_sub_sect_dtl.GetItemString(li_counter, "display_ind")
			if ls_display_ind = 'Y' Then
				lb_yes_found = True
			End If
		Else
			If lb_yes_found = False Then
				iw_frame.SetMicroHelp("At least one product in a product seq group must be Display Yes")
				dw_sub_sect_dtl.ScrollToRow(li_counter - 1)
				dw_sub_sect_dtl.SetRow(li_counter - 1)
				Return False
			End If
			lb_yes_found = False
			ls_namecode = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code"))
			li_product_seq = dw_sub_sect_dtl.GetItemNumber(li_counter, "product_seq")
		End If
	End if
		
Loop
Return True
end function

public function boolean wf_check_dups ();Boolean		lb_yes_found

Integer		li_counter				

Long			ll_row_count, &
				ll_rtn

String		ls_namecode, &
				ls_product_code, &
				ls_product_state, &
				ls_SearchString


//product code/state cannot occur more than once under the same
//sub section name code

dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()

ll_row_count = dw_sub_sect_dtl.RowCount()

li_counter = 1

ls_namecode = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code"))
ls_product_code = dw_sub_sect_dtl.GetItemString(li_counter, "product_code")
ls_product_code = dw_sub_sect_dtl.GetItemString(li_counter, "product_state")

ls_SearchString = "subsect_name_code = "
ls_Searchstring += ls_namecode 
ls_SearchString += "product_code = "
ls_Searchstring += "'" + ls_product_code + "'"
ls_SearchString = "product_state = "
ls_Searchstring += "'" + ls_product_state + "'"

For li_counter = 1 to ll_row_Count
	
	CHOOSE CASE li_counter
		CASE 1
			ll_rtn = dw_sub_sect_dtl.Find  &
				( ls_SearchString, li_counter + 1, ll_row_count)
		CASE 2 to (ll_row_count - 1)
			ll_rtn = dw_sub_sect_dtl.Find ( ls_SearchString, li_counter - 1, 1)
			If ll_rtn = 0 Then ll_rtn = dw_sub_sect_dtl.Find  &
					(ls_SearchString, li_counter + 1, ll_row_count)
		CASE ll_row_count
			ll_rtn = dw_sub_sect_dtl.Find ( ls_SearchString, li_counter - 1, 1)
	END CHOOSE

	If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "There are duplicate product codes within the same subsection")
		dw_sub_sect_dtl.SetRedraw(False)
		dw_sub_sect_dtl.ScrollToRow(li_counter)
		dw_sub_sect_dtl.SetColumn("product_code")
		dw_sub_sect_dtl.SetRow(li_counter)
		dw_sub_sect_dtl.SelectRow(ll_rtn, True)
		dw_sub_sect_dtl.SelectRow(li_counter, True)
		dw_sub_sect_dtl.SetRedraw(True)
		Return False
	End if
Next

Return True
end function

public function boolean wf_set_sub_copy ();dwItemStatus	le_RowStatus
Integer		li_temp, &
				li_counter

Long			ll_row, &
				ll_current_row, &
				ll_temp, &
				ll_NbrRows, &
				ll_rtn, &
				ll_seq
				
String		ls_ind, &
				ls_temp, &
				ls_sub_sect_copy, &
				ls_char_copy, &
				ls_searchstring, &
				ls_product_seq, &
				ls_sub_sect_type, &
				ls_product_code
							
							
ls_sub_sect_copy = string(dw_sub_section_copy.GetItemNumber(1,"sub_section"))
ls_char_copy = string(dw_sub_section_copy.GetItemNumber(1,"characteristics"))

if isNull(ls_char_copy) then
	ls_char_copy = '0'
end if

IF  ( IsNull(ls_sub_sect_copy) or ls_sub_sect_copy = '         ' ) then 
		iw_frame.SetMicroHelp("You must select a sub section to copy to")
		Return False
end if

li_counter = 1
il_product_seq = 0

ls_SearchString = "subsect_name_code = "
ls_Searchstring += ls_sub_sect_copy 
ls_SearchString += " and characteristic = "
ls_Searchstring += ls_char_copy 

ll_rtn = dw_sub_sect_dtl.Find  &
			( ls_SearchString, 1, dw_sub_sect_dtl.RowCount())

   
If ll_rtn > 0 Then
	ls_sub_sect_type = dw_sub_sect_dtl.GetItemString(ll_rtn, "subsect_type")
	ll_seq = dw_sub_sect_dtl.GetItemNumber(ll_rtn, "subsect_seq")
else
	iw_Frame.SetMicroHelp( "Not a valid Subsection, Characteristic to copy to")
	Return False
End if


ll_NbrRows = dw_sub_sect_dtl.RowCount( )

ll_current_row = dw_sub_sect_dtl.GetSelectedRow(0)
If ll_current_row = 0 Then
	iw_frame.SetMicroHelp("No rows have been selected to copy")
	Return False
End If

This.SetRedraw(False)
Do While ll_current_row <> 0
	if ll_current_row <> 0 then
		// check see if product already exists on sub section 
		// if does do not insert new row
		ls_product_code = dw_sub_sect_dtl.GetItemString(ll_current_row, "product_code")
		ls_SearchString = "subsect_name_code = "
		ls_Searchstring += ls_sub_sect_copy 
		ls_SearchString += " and characteristic = "
		ls_Searchstring += ls_char_copy 
		ls_SearchString += " and product_code = "
		ls_Searchstring += "'" + ls_product_code + "'"
				ll_rtn = dw_sub_sect_dtl.Find  &
					( ls_SearchString, ll_rtn, dw_sub_sect_dtl.RowCount())
		if ll_rtn = 0 then
			ll_row = ll_NbrRows + 1
			ll_row = dw_sub_sect_dtl.InsertRow(ll_row)
			ll_temp = dw_sub_section_copy.GetItemNumber(1, "sub_section")
			dw_sub_sect_dtl.SetItem(ll_row, "subsect_name_code", ll_temp)
			dw_sub_sect_dtl.SetItem(ll_row, "subsect_type", ls_sub_sect_type)
			ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "product_code")
			dw_sub_sect_dtl.SetItem(ll_row, "product_code", ls_temp)
			ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "product_state")
			dw_sub_sect_dtl.SetItem(ll_row, "product_state", ls_temp)
			dw_sub_sect_dtl.SetItem(ll_row, "subsect_seq", ll_seq) 
			ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "display_ind")
			dw_sub_sect_dtl.SetItem(ll_row, "display_ind", ls_temp)
			ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "schd_miss_ind")
			dw_sub_sect_dtl.SetItem(ll_row, "schd_miss_ind", ls_temp)
			ll_temp = dw_sub_section_copy.GetItemNumber(1, "characteristics")
			dw_sub_sect_dtl.SetItem(ll_row, "characteristic", ll_temp)
			ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "mix_pct_ind")
			dw_sub_sect_dtl.SetItem(ll_row, "mix_pct_ind", ls_temp)
			dw_sub_sect_dtl.SetItem(ll_row, "update_flag", 'A')
			ll_temp = dw_sub_sect_dtl.GetItemNumber(ll_current_row, "product_seq")
			ls_product_seq = string(dw_sub_sect_dtl.GetItemNumber(ll_current_row, "product_seq"))
			ls_SearchString = "subsect_name_code = "
			ls_Searchstring += ls_sub_sect_copy 
			ls_SearchString += " and characteristic = "
			ls_Searchstring += ls_char_copy 
			ls_SearchString += " and product_seq = "
			ls_Searchstring += ls_product_seq 
			ll_rtn = dw_sub_sect_dtl.Find  &
			( ls_SearchString, ll_rtn, dw_sub_sect_dtl.RowCount())
			If ll_rtn > 0 Then
				do while ll_rtn <> 0
					ll_temp = ll_temp + 1
					ls_product_seq =  string(ll_temp)
					ls_SearchString = "subsect_name_code = "
					ls_Searchstring += ls_sub_sect_copy 
					ls_SearchString += " and characteristic = "
					ls_Searchstring += ls_char_copy 
					ls_SearchString += " and product_seq = "
					ls_Searchstring += ls_product_seq 
					ll_rtn = dw_sub_sect_dtl.Find  &
						( ls_SearchString, ll_rtn, dw_sub_sect_dtl.RowCount())
				loop
			end if
			ll_temp = long(ls_product_seq)
			dw_sub_sect_dtl.SetItem(ll_row, "product_seq", ll_temp) 
			dw_sub_sect_dtl.SelectRow(ll_current_row, False)
		else
			ls_product_seq = string(dw_sub_sect_dtl.GetItemNumber(ll_rtn, "product_seq"))	
		end if
	end if	
	ll_current_row = dw_sub_sect_dtl.GetSelectedRow(ll_current_row)
LOOP

This.SetRedraw(True)

dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()
ls_SearchString = "subsect_name_code = "
			ls_Searchstring += ls_sub_sect_copy 
			ls_SearchString += " and characteristic = "
			ls_Searchstring += ls_char_copy 
			ls_SearchString += " and product_seq = "
			ls_Searchstring += ls_product_seq 
ll_row = dw_sub_sect_dtl.Find  &
						( ls_SearchString, 1, dw_sub_sect_dtl.RowCount())
if ll_row > 0 then
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(ll_row)
	dw_sub_sect_dtl.SelectRow(ll_row, True)
	This.SetRedraw(True)
end if

ib_copy_sub_sect = True

Return True
	
end function

public function boolean wf_addrow ();Integer		li_temp

Long			ll_row, &
				ll_current_row, &
				ll_temp
				
String		ls_ind, &
				ls_temp

If dw_sub_sect_dtl.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_current_row = dw_sub_sect_dtl.GetSelectedRow(0)
if ll_current_row > 0 then
	//do nothing - insert after this row
else
	
//if dw_sub_sect_dtl.il_last_Clicked_row > 0 then
//	ll_current_row = dw_sub_sect_dtl.il_last_Clicked_row 
//Else
	ll_current_row = dw_sub_sect_dtl.GetRow()
End If

If ll_current_row > 0 Then
	ll_row = dw_sub_sect_dtl.InsertRow(ll_current_row + 1)
	lL_temp = dw_sub_sect_dtl.GetItemNumber(ll_current_row, "subsect_name_code")
	dw_sub_sect_dtl.SetItem(ll_row, "subsect_name_code", ll_temp)
	ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "subsect_type")
	dw_sub_sect_dtl.SetItem(ll_row, "subsect_type", ls_temp)
	ll_temp = dw_sub_sect_dtl.GetItemNumber(ll_current_row, "subsect_seq")
	dw_sub_sect_dtl.SetItem(ll_row, "subsect_seq", ll_temp) 
	ll_temp = dw_sub_sect_dtl.GetItemNumber(ll_current_row, "product_seq")
	ll_temp = ll_temp + 1
	dw_sub_sect_dtl.SetItem(ll_row, "product_seq", ll_temp) 
	ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "display_ind")
	dw_sub_sect_dtl.SetItem(ll_row, "display_ind", ls_temp)
	ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "schd_miss_ind")
	dw_sub_sect_dtl.SetItem(ll_row, "schd_miss_ind", ls_temp)
	ll_temp = dw_sub_sect_dtl.GetItemNumber(ll_current_row, "characteristic")
	dw_sub_sect_dtl.SetItem(ll_row, "characteristic", ll_temp)
	ls_temp = dw_sub_sect_dtl.GetItemString(ll_current_row, "mix_pct_ind")
	dw_sub_sect_dtl.SetItem(ll_row, "mix_pct_ind", ls_temp)
Else
	ll_row = dw_sub_sect_dtl.InsertRow(0)	
End If

dw_sub_sect_dtl.ScrollToRow(ll_row)
dw_sub_sect_dtl.SetItem(ll_row, "update_flag", 'A')
dw_sub_sect_dtl.SetItem(ll_row, "product_code", ' ')
dw_sub_sect_dtl.SetColumn("product_code")
dw_sub_sect_dtl.SetFocus()
This.SetRedraw(True)

wf_populate_sub_section_header()

return true



end function

public function boolean wf_find_product ();Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
string	ls_prod_code, & 
			ls_data, &
			ls_update_flag, &
			ls_reset
integer	li_counter
Boolean	lb_yes_found


ll_row_count = dw_sub_sect_dtl.RowCount()
li_counter = 1
Do While li_counter <= ll_Row_count 
	dw_sub_sect_dtl.SelectRow(li_counter, False)
	li_counter = li_counter + 1
loop

li_counter = ii_row_count + 1

if ii_row_count > ll_row_count then
	li_counter = 1
end if

if ll_row_count < li_counter then Return True
ls_data = is_data

lb_yes_found = False

Do While li_counter <= ll_Row_count 
		ls_prod_code = dw_sub_sect_dtl.GetItemString(li_counter, "product_code")
		If pos(ls_prod_code, ls_data) > 0 and pos(ls_prod_code, ls_data) < 2 then
			lb_yes_found=True
		else	
			lb_yes_found=False
		end if
		if lb_yes_found = True then
			ii_row_count = li_counter
			dw_sub_sect_dtl.SetRedraw(False)
 			dw_sub_sect_dtl.ScrollToRow(li_counter)
			dw_sub_sect_dtl.SelectRow(li_counter, True)
			dw_sub_sect_dtl.SetRow(li_counter + 1)
			dw_sub_sect_dtl.SetRedraw(True)
			li_counter = ll_row_count + 1
		else	
			dw_sub_sect_dtl.SelectRow(li_counter, False)
			li_counter = li_counter + 1	
		end if
Loop

if lb_yes_found=False then
	iw_Frame.SetMicroHelp("No matching products found")
	ii_row_count = ll_row_count + 1
else
	iw_Frame.SetMicroHelp("Product found")	
end if

Return True

end function

public function boolean wf_get_sub_desc ();Integer	li_row_Count
				
String	ls_input, &
			ls_output_values
						
u_string_functions u_string

DataWindowChild	ldwc_temp


idwc_sub_desc.Reset()
idwc_sub_desc_copy.Reset()

ls_input = 'SUBSECT~r~n'

ls_output_values = iu_sect_functions.uf_get_names(ls_input)

li_row_count = idwc_sub_desc.ImportString(ls_output_values)

li_row_count = idwc_sub_desc_copy.ImportString(ls_output_values)


idwc_total_name_desc.Reset()

ls_input = 'TOTAL~r~n'

ls_output_values = iu_sect_functions.uf_get_names(ls_input)

li_row_count = idwc_total_name_desc.ImportString(ls_output_values)

Return True

end function

public function boolean wf_resequence ();Long			ll_row, &				
				ll_row_cnt, &
				ll_hold_seq, &
				ll_skip_seq, &
				ll_new_seq, &
				ll_seq, &
				ll_seq_count, &
				ll_start_row, &
				ll_end_row, &
				ll_start_subsect, &
				ll_end_subsect, &
				ll_start_product_seq, &
				ll_end_product_seq, &
				ll_no_of_rows, &
				ll_seq_by, &
				ll_continue


//This logic will recalculate the sequence number


ll_row_cnt = dw_sub_sect_dtl.RowCount()

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Resequencing Rows")

ll_seq_count = 0
ll_start_row = dw_sub_sect_dtl.GetSelectedRow(0)
If ll_start_row = 0 Then
	iw_frame.SetMicroHelp("No rows have been selected to resequence")
	Return False
End If

ll_end_row = dw_sub_sect_dtl.GetSelectedRow(ll_start_row)
If ll_end_row = 0 Then
	iw_frame.SetMicroHelp("No rows have been selected to resequence")
	Return False
End If

ll_continue = ll_end_row

do 
   if dw_sub_sect_dtl.isSelected(ll_end_row) then
  		 ll_end_row = ll_end_row + 1
		 ll_continue = ll_end_row
	else
		ll_continue = ll_row_cnt + 1
		ll_end_row = ll_end_row  - 1
	end if
loop while ll_continue <= ll_row_cnt

			  
if ll_end_row > ll_row_cnt then
	ll_end_row = ll_row_cnt
end if

ll_start_subsect = dw_sub_sect_dtl.GetItemNumber(ll_start_row, "subsect_name_code")
ll_end_subsect = dw_sub_sect_dtl.GetItemNumber(ll_end_row, "subsect_name_code")

ll_start_product_seq = dw_sub_sect_dtl.GetItemNumber(ll_start_row, "product_seq")
ll_end_product_seq = dw_sub_sect_dtl.GetItemNumber(ll_end_row, "product_seq")

if ll_start_subsect <> ll_end_subsect then
		iw_frame.SetMicroHelp("Rows must have same sub section in order to resequence")
	Return False
End If


ll_no_of_rows = ll_end_row - ll_start_row
if ll_no_of_rows < 3 then
	iw_frame.SetMicroHelp("You must select more than 2 rows in order to resequence")
	return False
end if
dw_sub_sect_dtl.SetRedraw(False)
ll_seq_by = ((ll_end_product_seq - ll_start_product_seq) / ll_no_of_rows)
if ll_seq_by > 1000 then
	ll_seq_by = round(ll_seq_by / 100, 0)
	ll_seq_by = ll_seq_by * 100
else
	if ll_seq_by > 100 then
		ll_seq_by = round(ll_seq_by / 10, 0)
		ll_seq_by = ll_seq_by * 10
	end if
end if

ll_start_row = ll_start_row + 1
ll_new_seq = ll_start_product_seq
do
	ll_new_seq = ll_new_seq + ll_seq_by
	dw_sub_sect_dtl.SetItem(ll_start_row, "product_seq", ll_new_seq)
	dw_sub_sect_dtl.SetItem(ll_start_row, "update_flag", 'U')
	ll_start_row = ll_start_row + 1
Loop while ll_start_row <=  ll_end_row
//

iw_frame.SetMicroHelp("Resequence complete")
dw_sub_sect_dtl.SetRedraw(True)
dw_sub_sect_dtl.accepttext()

Return True

end function

public function boolean wf_validate (long al_row);Boolean				lb_duplicate

Integer				li_temp
						
Long					ll_rtn, &
						ll_nbrrows, &
						ll_ModifiedRow, &
						ll_search_row, &
						ll_temp
						
String				ls_searchstring, &
						ls_temp, &
						ls_update_flag, &
						ls_subsect, &
						ls_subsect_find, &
						ls_type, &
						ls_type_find, &
						ls_sequence, &
						ls_total_ind
											
						
IF dw_sub_sect_dtl.AcceptText() = -1 THEN Return( False )

IF al_row < 0 THEN Return( True )

ll_nbrrows = dw_sub_sect_dtl.RowCount()

ls_subsect = string(dw_sub_sect_dtl.GetItemNumber(al_row, "subsect_name_code"))
IF IsNull(ls_subsect) Then 
	iw_frame.SetMicroHelp("Please choose a sub section description.")
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("subsect_name_code")
	dw_sub_sect_dtl.SetFocus()
	Return False
End If

ls_type = dw_sub_sect_dtl.GetItemString(al_row, "subsect_type")
IF IsNull(ls_type) Then 
	iw_frame.SetMicroHelp("Please choose a sub section type.")
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("subsect_type")
	dw_sub_sect_dtl.SetFocus()
	Return False
End If

ll_temp = dw_sub_sect_dtl.GetItemNumber(al_row, "subsect_seq")
IF ll_temp > 0 Then 
	//do nothing
Else
	iw_frame.SetMicroHelp("Please enter a valid sub section sequence number.")
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("subsect_seq")
	dw_sub_sect_dtl.SetFocus()
	Return False
End If

ll_temp = dw_sub_sect_dtl.GetItemNumber(al_row, "product_seq")
IF ll_temp > 0 Then 
	//do nothing
Else
	iw_frame.SetMicroHelp("Please enter a valid product sequence number.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("product_seq")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

IF ib_copy_plant = False THEN
	ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "product_code")
	If not invuo_fab_product_code.uf_check_product(ls_temp) then
		If	invuo_fab_product_code.ib_error_occurred then
			iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
			Return False
		Else
			iw_frame.SetMicroHelp(ls_temp + " is an invalid Product Code")
			dw_sub_sect_dtl.ScrollToRow(al_row)
			dw_sub_sect_dtl.SetColumn("product_code")
			dw_sub_sect_dtl.SetFocus()
			Return False
		End If
	End If
END IF

ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "product_state")
IF IsNull(ls_temp) Then 
	iw_frame.SetMicroHelp("Please choose a valid product state.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("product_state")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "display_ind")
IF IsNull(ls_temp) Then 
	iw_frame.SetMicroHelp("Display indicator must be Yes or No.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("display_ind")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "schd_miss_ind")
IF IsNull(ls_temp) Then 
	iw_frame.SetMicroHelp("Schedule miss indicator must be Yes or No.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("schd_miss_ind")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = string(dw_sub_sect_dtl.GetItemNumber(al_row, "characteristic"))
If ls_type = 'OUTUNGRD' or ls_type = 'NOT SCHD' Then
	Return True
Else
	IF IsNull(ls_temp) Then 
		iw_frame.SetMicroHelp("Please enter a valid characteristic.")
		This.SetRedraw(False)
		dw_sub_sect_dtl.ScrollToRow(al_row)
		dw_sub_sect_dtl.SetColumn("characteristic")
		dw_sub_sect_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	End if
End If
ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "mix_pct_ind")
IF IsNull(ls_temp) Then 
	iw_frame.SetMicroHelp("Mix percent indicator must be Yes or No.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("mix_pct_ind")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_total_ind = dw_sub_sect_dtl.GetItemString(al_row, "total_ind")
IF IsNull(ls_total_ind) Then 
	iw_frame.SetMicroHelp("Total indicator must be Yes or No.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("total_ind")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

IF ls_total_ind = 'Y' then
	ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "total_name")
	IF IsNull(ls_temp) then
		iw_frame.SetMicroHelp("Please select a Total Name if Total is Yes.")
		This.SetRedraw(False)
		dw_sub_sect_dtl.ScrollToRow(al_row)
		dw_sub_sect_dtl.SetColumn("total_name")
		dw_sub_sect_dtl.SetFocus()
		This.SetRedraw(True)
		Return False
	else	
		IF ls_temp = '         ' then
			iw_frame.SetMicroHelp("Please select a Total Name if Total is Yes.")
			This.SetRedraw(False)
			dw_sub_sect_dtl.ScrollToRow(al_row)
			dw_sub_sect_dtl.SetColumn("total_name")
			dw_sub_sect_dtl.SetFocus()
			This.SetRedraw(True)
			Return False
		else	
			IF ls_temp = '000000000' then
				iw_frame.SetMicroHelp("Please select a Total Name if Total is Yes.")
				This.SetRedraw(False)
				dw_sub_sect_dtl.ScrollToRow(al_row)
				dw_sub_sect_dtl.SetColumn("total_name")
				dw_sub_sect_dtl.SetFocus()
				This.SetRedraw(True)
				Return False	
			End If
		End if
	End if
End if 
Return True
end function

public function boolean wf_update ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp, &
					ll_rtn, &
					ll_300th_row, &
					ll_current_row, &
					ll_modified_count

string			ls_Update_string, &
					ls_header_string, &
					ls_output_string, &
					ls_ind, &
					ls_name_code, &
					ls_copy_plant, &
					ls_input, &
					ls_RPC_detail, &
					ls_sub_sect_copy, &
					ls_char_copy
					
u_string_functions		lu_string_functions

IF dw_sub_sect_dtl.AcceptText() = -1 &
	And dw_copy_plant.AcceptText() = -1 &
	And dw_sub_section_copy.AcceptText() = -1 THEN
	Return( False )
End If
ll_temp = dw_sub_sect_dtl.DeletedCount()

ll_current_row = dw_sub_sect_dtl.GetRow()

ll_modified_count = dw_sub_sect_dtl.ModifiedCount()

if ib_copy_sub_sect then
    // do not do plant check or count check
else	
	ls_copy_plant = dw_copy_plant.GetItemString(1,"location_code")
	if IsNull(ls_copy_plant) or ls_copy_plant = '   ' or &
		ls_copy_plant = dw_header.GetItemString(1,"plant_code")  then
		IF dw_sub_sect_dtl.ModifiedCount() + dw_sub_sect_dtl.DeletedCount() <= 0 THEN 
			Return( False )
		End If
	End If
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_sub_sect_dtl.RowCount( )

dw_sub_sect_dtl.SetReDraw(False)
ll_row = 0

ls_copy_plant = dw_copy_plant.GetItemString(1,"location_code")
if IsNull(ls_copy_plant) or ls_copy_plant = '   ' or &
	ls_copy_plant = dw_header.GetItemString(1,"plant_code") Then
	is_input = is_input + 'N~t' + ls_copy_plant + '~t'
Else
	if not wf_set_plant_copy() Then
		dw_sub_sect_dtl.SetReDraw(True)
		Return False
	End If
	ls_input = ls_copy_plant + '~t' + &
			This.dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			This.dw_header.GetItemString(1, 'sect_name_code') + '~t' + &
			'Y~t' + dw_header.GetItemString(1,"plant_code") + '~t'
	is_input = ls_input
End If

ll_row = 1
DO WHILE ll_Row <= ll_NbrRows 
	ls_ind = dw_sub_sect_dtl.GetItemString(ll_row,"update_flag")
	IF ls_ind > ' ' Then
		If Not wf_validate(ll_row) Then
			dw_sub_sect_dtl.SetReDraw(True)
			Return False
		End If
	END IF
	ll_row = ll_row + 1
LOOP
// validate the input raw material type sequence
If not wf_validate_type() Then
	dw_sub_sect_dtl.SetReDraw(True)
	Return False
End If
//validate the display ind
If not wf_validate_display() Then
	dw_sub_sect_dtl.SetReDraw(True)
	Return False
End If

dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()

ls_header_string = is_input
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_sub_sect_dtl)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp57cr_upd_sched_sub_sects"
istr_error_info.se_message = Space(71)

//DO 

	ll_300th_Row = lu_string_functions.nf_nPos( ls_Update_string, "~r~n",1,300)
	IF ll_300th_Row > 0 Then
		ls_RPC_Detail = Left( ls_Update_string, ll_300th_Row + 1)
		ls_Update_string = Mid( ls_Update_string, ll_300th_Row + 2)
	ELSE
		ls_Rpc_detail = ls_Update_string
		ls_Update_string = ''
	END IF

//	if not iu_pas201.nf_pasp57cr_upd_sched_sub_sects(istr_error_info, ls_RPC_detail,ls_output_string, ls_header_string) Then
//		if ls_output_string > '' Then
//			wf_unstring_output(ls_output_string)
//		End If
		
	if not iu_ws_pas5.nf_pasp57gr(istr_error_info, ls_RPC_detail,ls_output_string, ls_header_string) Then
		if ls_output_string > '' Then
			wf_unstring_output(ls_output_string)
		End If
		
		if ib_copy_plant Then
	   	dw_sub_sect_dtl.ResetUpdate()
			dw_find_product.ResetUpdate()
			dw_find_characteristic.ResetUpdate()
			ib_copy_plant = False
		End If
		if ib_copy_sub_sect Then
	   	dw_sub_sect_dtl.ResetUpdate()
			dw_find_product.ResetUpdate()
			dw_find_characteristic.ResetUpdate()
			ib_copy_sub_sect = False
		End If
		Return False
	end If

//Loop While Not lu_string_functions.nf_IsEmpty( ls_Update_string)

iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_sub_sect_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
	ls_name_code = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code"))
	idwc_sub_desc = iu_sect_functions.uf_set_visible_ind(idwc_sub_desc, ls_name_code, 'N')
	dw_sub_sect_dtl.SetItem(li_Counter, 'update_flag', ' ')
Next

wf_duplicate_prod_seq()

dw_sub_sect_dtl.ResetUpdate()
dw_find_product.ResetUpdate()
dw_find_characteristic.ResetUpdate()
dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()
dw_sub_sect_dtl.SetFocus()
dw_sub_sect_dtl.SetReDraw(True)
dw_sub_sect_dtl.ScrollToRow(ll_current_row)

wf_populate_sub_section_header()


Return( True )
end function

public function boolean wf_retrieve ();Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_output_values, ls_temp, &
			ls_name_code, &
			ls_total_ind, &
			ls_namecode, &
			ls_char, &
			ls_SearchString, &
			ls_filter
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string
Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

ls_input = Message.StringParm
If Not ib_ReInquire Then
	OpenWithParm(w_prd_sub_sect_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
	Message.StringParm = ''
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			This.dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			This.dw_header.GetItemString(1, 'sect_name_code') + '~t'
			
is_input = ls_input

//li_ret = iu_pas201.nf_pasp56cr_inq_sched_sub_sects(istr_error_info, &
//									is_input, &
//									ls_output_values)

li_ret = iu_ws_pas5.nf_pasp56gr(istr_error_info, &
									is_input, &
									ls_output_values) 
						

This.dw_sub_sect_dtl.Reset()
This.dw_copy_plant.Reset()
This.dw_copy_plant.InsertRow(0)
This.dw_sub_section_copy.InsertRow(0)

If li_ret = 0 Then
	This.dw_sub_sect_dtl.ImportString(ls_output_values)
End If
ll_value = idwc_sub_desc.RowCount()
if ll_value < 0 Then ll_value = 0

For li_counter = 1 to ll_value
	ls_name_code = String(idwc_sub_desc.GetItemNumber(li_counter, "name_code"))
	idwc_sub_desc = iu_sect_functions.uf_set_visible_ind(idwc_sub_desc, ls_name_code, 'Y') 
Next	

li_Row_count = dw_sub_sect_dtl.RowCount()

If li_Row_count < 0 Then li_Row_count = 0

For li_counter = 1 to li_Row_count
	ls_name_code = string(dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_name_code"))
	idwc_sub_desc = iu_sect_functions.uf_set_visible_ind(idwc_sub_desc, ls_name_code, 'N') 
Next	
	
dw_sub_sect_dtl.ResetUpdate()

IF li_Row_count > 0 THEN
	dw_sub_sect_dtl.SetFocus()
	dw_sub_sect_dtl.ScrollToRow(1)
	dw_sub_sect_dtl.SetColumn( "subsect_name_code" )
	dw_sub_sect_dtl.TriggerEvent("RowFocusChanged")
END IF

SetMicroHelp(String(li_Row_count) + " rows retrieved")

This.SetRedraw( True )

dw_sub_sect_dtl.ResetUpdate()
dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()
dw_copy_plant.AcceptText()

wf_populate_sub_section_header()

wf_duplicate_prod_seq()

dw_sub_sect_dtl.ResetUpdate()


Return True

end function

public function boolean wf_find_characteristic ();Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
//ls_char, & 
			//ls_data, &
string				ls_update_flag, &
			ls_reset
integer	li_counter, li_data, li_char
Boolean	lb_yes_found


ll_row_count = dw_sub_sect_dtl.RowCount()
li_counter = 1
Do While li_counter <= ll_Row_count 
	dw_sub_sect_dtl.SelectRow(li_counter, False)
	li_counter = li_counter + 1
loop

li_counter = ii_row_count + 1

if ii_row_count > ll_row_count then
	li_counter = 1
end if

if ll_row_count < li_counter then Return True

//ls_data = is_data
li_data = dw_find_characteristic.GetItemNumber(1, "characteristics")

lb_yes_found = False

Do While li_counter <= ll_Row_count 
	
		li_char = dw_sub_sect_dtl.GetItemNumber(li_counter, "characteristic")

		if(li_char = li_data) then
			lb_yes_found=True
			ii_row_count = li_counter			
			dw_sub_sect_dtl.SetRedraw(False)
 			dw_sub_sect_dtl.ScrollToRow(li_counter)
			dw_sub_sect_dtl.SelectRow(li_counter, True)
			dw_sub_sect_dtl.SetRow(li_counter + 1)
			dw_sub_sect_dtl.SetRedraw(True)
			li_counter = ll_row_count
		else	
			lb_yes_found=False
			dw_sub_sect_dtl.SelectRow(li_counter, False)
		end if

		li_counter = li_counter + 1
Loop

if not lb_yes_found then
	iw_Frame.SetMicroHelp("No matching characteristics found")
	ii_row_count = ll_row_count + 1
else
	iw_Frame.SetMicroHelp("characteristics found")	
end if

Return True
end function

public function boolean wf_deleterow ();Boolean		lb_ret

Long			ll_row, &
				ll_RowCount, &
				ll_counter

String		ls_ind, &
				ls_filter, &
				ls_name_code

ll_row = dw_sub_sect_dtl.GetRow()

If ll_row < 1 Then
	Return True
End If

ll_row = dw_sub_sect_dtl.GetSelectedRow(0)
If ll_row = 0 Then
		iw_frame.SetMicroHelp("Select at least 1 row to delete")
	return True
End If

If MessageBox("Delete Rows", "Are you sure you want to delete the selected rows?", &
		Question!, YesNo!, 2) = 2 Then Return True

This.SetRedraw( False )

ll_RowCount = dw_sub_sect_dtl.RowCount()

For ll_counter = 1 to ll_RowCount
	
	If dw_sub_sect_dtl.IsSelected(ll_counter) Then
		
		If dw_sub_sect_dtl.GetItemString(ll_counter, "update_flag") = 'A' Then
			ls_name_code = string(dw_sub_sect_dtl.GetItemNumber(ll_counter, "subsect_name_code"))
			idwc_sub_desc = iu_sect_functions.uf_set_visible_ind(idwc_sub_desc, ls_name_code,'Y') 
		End if
	
		dw_sub_sect_dtl.SetItem(ll_counter, "update_flag", 'D')
		
	End IF
Next

For ll_counter = ll_RowCount to 1 Step -1
	
	If dw_sub_sect_dtl.GetItemString(ll_counter, "update_flag") = 'D' Then 
		
		dw_sub_sect_dtl.DeleteRow(ll_counter)
		ll_RowCount --

	End IF
Next

dw_sub_sect_dtl.SetFocus()
dw_sub_sect_dtl.SelectRow(0,False)

This.SetRedraw( True )

wf_populate_sub_section_header()

wf_duplicate_prod_seq()

return lb_ret
end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_pas203) Then Destroy(iu_pas203)

If IsValid(iu_ws_pas3) Then Destroy(iu_ws_pas3)

If IsValid(iu_ws_pas5) Then Destroy(iu_ws_pas5)

end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

//If ib_reload Then
	wf_get_sub_desc()
//	ib_reload = False
//End If
end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_sub_sect_dtl.InsertRow(0)
dw_find_product.InsertRow(0)
dw_find_characteristic.InsertRow(0)
If dw_sub_section_copy.RowCount() = 0 Then
	dw_sub_section_copy.InsertRow(0)
End if

If dw_copy_plant.RowCount() = 0 Then
	dw_copy_plant.InsertRow(0)
End if


end event

event ue_postopen;call super::ue_postopen;String						ls_char

DataWindowChild			ldwc_type, &
								ldwc_char, &
								ldwc_state, &
								ldwc_char_copy, &
								ldwc_char_find
								
Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Schedule Section By Plant"
istr_error_info.se_user_id = sqlca.userid

idwc_sub_desc.Reset()
dw_sub_sect_dtl.GetChild("subsect_name_code", idwc_sub_desc)

idwc_sub_desc_copy.Reset()
dw_sub_section_copy.GetChild("sub_section", idwc_sub_desc_copy)

idwc_total_name_desc.Reset()
dw_sub_sect_dtl.GetChild("total_name", idwc_total_name_desc)

//populate the description drop down
wf_get_sub_desc()

//populate the type drop down
dw_sub_sect_dtl.GetChild('subsect_type', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PPSSTYPE")


//populate the product state drop down
dw_sub_sect_dtl.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()

//populate the display ind drop down
dw_sub_sect_dtl.GetChild('display_ind', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PPS DISP")

//populate the schedule miss ind drop down
dw_sub_sect_dtl.GetChild('schd_miss_ind', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PPS MISS")

//populate the mix pct ind drop down
dw_sub_sect_dtl.GetChild('mix_pct_ind', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PPS MPCT")


//populate the total ind drop down
dw_sub_sect_dtl.GetChild('total_ind', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PPS TOTL")

// populate the characteristics drop down

iu_pas203 = Create u_pas203
iu_ws_pas3 = Create u_ws_pas3
iu_ws_pas5 = Create u_ws_pas5

istr_error_info.se_event_name = "ue_postconstructor"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

//If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
If iu_ws_pas3.uf_pasp61fr(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_sub_sect_dtl.GetChild("characteristic", ldwc_char)
ldwc_char.ImportString(ls_char)
ldwc_char.Sort()

dw_sub_section_copy.GetChild("characteristics", ldwc_char_copy)
ldwc_char_copy.ImportString(ls_char)
ldwc_char_copy.Sort()

dw_find_characteristic.GetChild("characteristics", ldwc_char_find)
ldwc_char_find.ImportString(ls_char)
ldwc_char_find.Sort()

//move to above, so that wf_get_sub_desc is called only once.
//idwc_total_name_desc.Reset()
//dw_sub_sect_dtl.GetChild("total_name", idwc_total_name_desc)
//
////populate the description drop down
//wf_get_sub_desc()

This.PostEvent("ue_query")



end event

on w_prd_sub_sect.create
int iCurrent
call super::create
this.dw_find_characteristic=create dw_find_characteristic
this.cb_find_char=create cb_find_char
this.cb_find_product=create cb_find_product
this.cb_resequence=create cb_resequence
this.dw_sub_section_copy=create dw_sub_section_copy
this.cb_copy_to_subsection=create cb_copy_to_subsection
this.cb_copy_plant=create cb_copy_plant
this.rb_deselect=create rb_deselect
this.rb_select=create rb_select
this.dw_copy_plant=create dw_copy_plant
this.dw_find_product=create dw_find_product
this.dw_header=create dw_header
this.dw_sub_sect_dtl=create dw_sub_sect_dtl
this.gb_select=create gb_select
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_find_characteristic
this.Control[iCurrent+2]=this.cb_find_char
this.Control[iCurrent+3]=this.cb_find_product
this.Control[iCurrent+4]=this.cb_resequence
this.Control[iCurrent+5]=this.dw_sub_section_copy
this.Control[iCurrent+6]=this.cb_copy_to_subsection
this.Control[iCurrent+7]=this.cb_copy_plant
this.Control[iCurrent+8]=this.rb_deselect
this.Control[iCurrent+9]=this.rb_select
this.Control[iCurrent+10]=this.dw_copy_plant
this.Control[iCurrent+11]=this.dw_find_product
this.Control[iCurrent+12]=this.dw_header
this.Control[iCurrent+13]=this.dw_sub_sect_dtl
this.Control[iCurrent+14]=this.gb_select
end on

on w_prd_sub_sect.destroy
call super::destroy
destroy(this.dw_find_characteristic)
destroy(this.cb_find_char)
destroy(this.cb_find_product)
destroy(this.cb_resequence)
destroy(this.dw_sub_section_copy)
destroy(this.cb_copy_to_subsection)
destroy(this.cb_copy_plant)
destroy(this.rb_deselect)
destroy(this.rb_select)
destroy(this.dw_copy_plant)
destroy(this.dw_find_product)
destroy(this.dw_header)
destroy(this.dw_sub_sect_dtl)
destroy(this.gb_select)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Area Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'area_name_code')
	Case 'Sect Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'sect_name_code')
End Choose


end event

event ue_set_data;call super::ue_set_data;String		ls_name

Window		lw_temp

Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Area Name Code'
		dw_header.SetItem(1, 'area_name_code', as_value)
	Case 'Area Descr'
		dw_header.SetItem(1, 'area_descr', as_value)
	Case 'Sect Name Code'
		dw_header.SetItem(1, 'sect_name_code', as_value)
	Case 'Sect Descr'
		dw_header.SetItem(1, 'sect_descr', as_value)
	Case 'sched names'
		ls_name = 'SUBSECT'
		OpenSheetWithParm(lw_temp, ls_name, "w_prd_area_sect",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
End Choose


end event

event resize;call super::resize;integer li_x		
integer li_y	
integer li_height = 115

li_x = (dw_sub_sect_dtl.x * 2) + 30 
//li_y = dw_sub_sect_dtl.y + 115

if il_BorderPaddingHeight > li_height Then
                li_height = il_BorderPaddingHeight
End If

if il_BorderPaddingWidth > li_x Then
                li_x = il_BorderPaddingWidth
End If


li_y = dw_sub_sect_dtl.y + li_height

if width > li_x Then
	dw_sub_sect_dtl.width	= width - li_x
end if

if height > li_y then
	dw_sub_sect_dtl.height	= height - li_y
end if
end event

type dw_find_characteristic from u_base_dw_ext within w_prd_sub_sect
integer x = 503
integer y = 344
integer width = 704
integer height = 92
integer taborder = 110
string dataobject = "d_find_characteristic"
boolean border = false
end type

type cb_find_char from commandbutton within w_prd_sub_sect
integer x = 32
integer y = 352
integer width = 466
integer height = 92
integer taborder = 100
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Find Next Char"
end type

event clicked;Parent.PostEvent("ue_find_characteristic")
end event

type cb_find_product from commandbutton within w_prd_sub_sect
integer x = 32
integer y = 256
integer width = 466
integer height = 96
integer taborder = 70
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Find Next Product"
end type

event clicked;Parent.PostEvent("ue_find_product")
end event

type cb_resequence from commandbutton within w_prd_sub_sect
integer x = 1280
integer y = 256
integer width = 530
integer height = 92
integer taborder = 80
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Resequence Products"
end type

event clicked;
Parent.PostEvent("ue_resequence")
end event

type dw_sub_section_copy from u_base_dw_ext within w_prd_sub_sect
integer x = 1865
integer y = 128
integer width = 2341
integer height = 96
integer taborder = 90
string dataobject = "d_sub_section_copy"
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;This.ib_updateable = False
end event

event editchanged;call super::editchanged;//Long	ll_row, &
//		ll_first_row, &
//		ll_last_row
//
//ll_row = dw_sub_sect_dtl.Find("product_code = '" + data + "'",1,dw_sub_sect_dtl.RowCount()+1)
//
//If ll_row > 0 Then 
//	dw_sub_sect_dtl.ScrollToRow(ll_row)
//	dw_sub_sect_dtl.SetRow(ll_row + 1)
//End If
//
//ll_first_row = Long(dw_sub_sect_dtl.Object.DataWindow.FirstRowOnPage)
//ll_last_row = Long(dw_sub_sect_dtl.Object.DataWindow.LastRowOnPage)
//
//If ll_row > ll_first_row and ll_row <= ll_last_row Then 
//	dw_sub_sect_dtl.SetRedraw(False)
//	dw_sub_sect_dtl.ScrollToRow(ll_row + ll_last_row - ll_first_row)
//	dw_sub_sect_dtl.ScrollToRow(ll_row)
//	dw_sub_sect_dtl.SelectRow(ll_row, True)
//	dw_sub_sect_dtl.SetRow(ll_row + 1)
//	dw_sub_sect_dtl.SetRedraw(True)
//End If


end event

event losefocus;call super::losefocus;This.AcceptText()
end event

type cb_copy_to_subsection from commandbutton within w_prd_sub_sect
integer x = 1280
integer y = 128
integer width = 489
integer height = 92
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Copy to Sub Section"
end type

event clicked;String			ls_sub_sect_copy, &
					ls_char_copy, &
					ls_searchstring, &
					ls_name_code, &
					ls_chars, &
					ls_sub_sect_name
					
int				li_row_count, &
					li_counter, &
					li_char_copy, &
					li_char
					
long				ll_rtn, &
					ll_row_count, &
					ll_char_copy, &
					ll_char
				

//ls_sub_sect_copy = string(dw_sub_section_copy.GetItemNumber(1,"sub_section"))
//ls_char_copy = string(dw_sub_section_copy.GetItemNumber(1,"characteristics"))
//
//IF ( ( IsNull(ls_sub_sect_copy) or ls_sub_sect_copy = '         ' ) and &
//		 (IsNull (ls_char_copy) or ls_char_copy = '    ' ) ) Then
//		iw_frame.SetMicroHelp("You must select a sub section and characteristic to copy to")
//		Return 1
//end if
//
//
//li_counter = 1
//il_product_seq = 0
//
//ls_SearchString = "subsect_name_code = "
//ls_Searchstring += ls_sub_sect_copy 
//ls_SearchString += " and characteristic = "
//ls_Searchstring += ls_char_copy 
//
//ll_rtn = dw_sub_sect_dtl.Find  &
//			( ls_SearchString, 1, dw_sub_sect_dtl.RowCount())
//
//   
//If ll_rtn > 0 Then
//	il_product_seq = dw_sub_sect_dtl.GetItemNumber(ll_rtn, "product_seq")
//else
//	iw_Frame.SetMicroHelp( "Not a valid Subsection, Characteristic to copy to")
//	Return 1
//End if

wf_set_sub_copy()


//dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
//dw_sub_sect_dtl.Sort()

//if ib_copy_sub_sect then
//	wf_update()
//end if
	



end event

type cb_copy_plant from commandbutton within w_prd_sub_sect
integer x = 1280
integer width = 466
integer height = 92
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Copy to Plant"
end type

event clicked;string 	ls_copy_plant


ls_copy_plant = dw_copy_plant.GetItemString(1,"location_code")
if IsNull(ls_copy_plant) or ls_copy_plant = '   ' then
	iw_frame.SetMicroHelp("Please choose a plant to copy to.")
	dw_copy_plant.ScrollToRow(1)
	//dw_copy_plant.SetColumn("subsect_name_code")
	dw_copy_plant.SetFocus()
	Return 1
end if


wf_update()
end event

type rb_deselect from radiobutton within w_prd_sub_sect
integer x = 4279
integer y = 184
integer width = 343
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Deselect All"
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_sub_sect_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_sub_sect_dtl.SelectRow(li_Counter, False)
Next	

end event

type rb_select from radiobutton within w_prd_sub_sect
integer x = 4279
integer y = 88
integer width = 343
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Select All"
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_sub_sect_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_sub_sect_dtl.SelectRow(li_Counter, True)
Next	
end event

type dw_copy_plant from u_plant within w_prd_sub_sect
integer x = 1865
integer width = 1463
integer height = 64
integer taborder = 10
end type

event losefocus;call super::losefocus;This.AcceptText()
end event

event constructor;call super::constructor;This.ib_updateable = False
end event

type dw_find_product from u_base_dw_ext within w_prd_sub_sect
integer x = 512
integer y = 256
integer width = 585
integer height = 92
integer taborder = 60
string dataobject = "d_find_product"
boolean border = false
end type

event editchanged;call super::editchanged;//Long	ll_row, &
//		ll_first_row, &
//		ll_last_row
//
//ll_row = dw_sub_sect_dtl.Find("product_code = '" + data + "'",1,dw_sub_sect_dtl.RowCount()+1)
//
//If ll_row > 0 Then 
//	dw_sub_sect_dtl.ScrollToRow(ll_row)
//	dw_sub_sect_dtl.SetRow(ll_row + 1)
//End If
//
//ll_first_row = Long(dw_sub_sect_dtl.Object.DataWindow.FirstRowOnPage)
//ll_last_row = Long(dw_sub_sect_dtl.Object.DataWindow.LastRowOnPage)
//
//If ll_row > ll_first_row and ll_row <= ll_last_row Then 
//	dw_sub_sect_dtl.SetRedraw(False)
//	dw_sub_sect_dtl.ScrollToRow(ll_row + ll_last_row - ll_first_row)
//	dw_sub_sect_dtl.ScrollToRow(ll_row)
//	dw_sub_sect_dtl.SelectRow(ll_row, True)
//	dw_sub_sect_dtl.SetRow(ll_row + 1)
//	dw_sub_sect_dtl.SetRedraw(True)
//End If
//sap
Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
string	ls_prod_code, &
			ls_reset, &
			ls_update_flag
integer	li_counter
Boolean	lb_yes_found
//
//
is_data = ' '
ll_row_count = dw_sub_sect_dtl.RowCount()
//
li_counter = 1
//
if ll_row_count < li_counter then Return 
//
Do While li_counter <= ll_Row_count 
	dw_sub_sect_dtl.SelectRow(li_counter, False)
	li_counter = li_counter + 1
loop
//
li_counter = 1
lb_yes_found = False
//
Do While li_counter <= ll_Row_count 
		ls_prod_code = dw_sub_sect_dtl.GetItemString(li_counter, "product_code")
		If pos(ls_prod_code, data) > 0 and pos(ls_prod_code, data) < 2 then
			lb_yes_found=True
		else	
			lb_yes_found=False
		end if
		if lb_yes_found = True then
			ii_row_count = li_counter
			is_data = data
			dw_sub_sect_dtl.SetRedraw(False)
			dw_sub_sect_dtl.ScrollToRow(li_counter)
			dw_sub_sect_dtl.SelectRow(li_counter, True)
			dw_sub_sect_dtl.SetRow(li_counter)
			dw_sub_sect_dtl.SetRedraw(True)
			li_counter = ll_row_count + 1
		else	
			li_counter = li_counter + 1	
		end if
Loop
//
if lb_yes_found=False then
	iw_Frame.SetMicroHelp("No matching products found")
else
	iw_Frame.SetMicroHelp("Product found")
end if
//
this.resetUpdate()
//
//li_counter = 1
//ls_reset = 'YES'
//Do While li_counter <= ll_Row_count 
//	ls_update_flag = dw_detail.GetItemString(li_counter, "update_flag")
//	if ls_update_flag > ' ' then
//		ls_reset = 'NO' 
//		li_counter = ll_row_count + 1
//	end if
//		li_counter = li_counter + 1
//loop
//	 
//if ls_reset = 'YES' then
//	dw_detail.ResetUpdate()
//end if
////	
////	li_counter = li_counter + 1
////loop
//
//
//sap
end event

event constructor;call super::constructor;This.ib_updateable = False
end event

type dw_header from u_base_dw_ext within w_prd_sub_sect
integer y = 4
integer width = 1371
integer height = 240
integer taborder = 0
boolean enabled = false
string dataobject = "d_prd_area_sect_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False




end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_sub_sect_dtl from u_base_dw_ext within w_prd_sub_sect
integer y = 476
integer width = 5083
integer height = 1096
integer taborder = 20
string dataobject = "d_sched_sub_sect"
boolean minbox = true
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;
Long		ll_rtn

Int		li_colnbr, &
			li_row
Boolean 	lb_color

string ls_update_flag, &
		ls_column_name


If row = 0 Then return



If dwo.Type <> "column" Then 
	li_colnbr = dw_sub_sect_dtl.GetClickedColumn ( )
	li_row = dw_sub_sect_dtl.GetClickedRow ( )
	dw_sub_sect_dtl.SetRow(li_row)
	if li_colnbr = 0 then
		li_colnbr = 6
		dw_sub_sect_dtl.Setcolumn(li_colnbr)
		ii_row_count = li_row
	end if
	Return
end if

If dwo.Band <> "detail" Then Return



If Right(dwo.Name, 2) = '_t' Then return

IF row > 0 Then
	ls_update_flag = this.getItemString(row, "update_flag")
	if ls_update_flag = 'A' then
		This.SetRow(Row)
		return
	end if	
//	messagebox('Clicked Event',string(row))
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If
////
//// SAP ***** selection 3 handles KeyShift and KeyControl
////is_selection = '3'
////end if
//
//THIS.SelectRow(row, TRUE)

Call Super::Clicked

//ls_Column_Name = GetColumnName()

li_row = dw_sub_sect_dtl.GetClickedRow ( )
ii_row_count = li_row

//This.SetColumn(3)




end event

event constructor;call super::constructor;ib_updateable = True

end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

Integer		li_seq

long			ll_source_row, &
				ll_RowCount, &
				ll_rtn

String		ls_ColumnName, &
				ls_SearchString, &
				ls_type, &
				ls_name_code								

nvuo_pa_business_rules	u_rule


IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

ll_RowCount = This.RowCount()

// On a new row set the subsection type and sequence based
// upon the name code that is chosen.  All rows with the same
// name code must have the same type and sequence.
If ls_ColumnName = "subsect_name_code" Then
	ls_name_code = string(data)
	If This.GetItemString(row, "update_flag") = 'A' Then
		ls_Searchstring = "subsect_name_code = "
		ls_Searchstring +=  ls_name_code 
		ll_rtn = dw_sub_sect_dtl.Find  &
				( ls_SearchString, 1, ll_RowCount)
		If ll_rtn > 0 Then
			This.SetItem(row,"subsect_type",This.GetItemString(ll_rtn,'subsect_type'))
			This.SetItem(row,"subsect_seq",This.GetItemNumber(ll_rtn,'subsect_seq'))
		End If
	End If
	wf_populate_sub_section_header()
End If
	
If ls_ColumnName = "subsect_type" Then
	ls_type = data
	ls_name_code = string(This.GetItemNumber(row, "subsect_name_code"))
	ll_source_row = 1
	

	ls_Searchstring = "subsect_name_code = "
	ls_Searchstring += ls_name_code 
	ll_rtn = dw_sub_sect_dtl.Find  &
			( ls_SearchString, 1, ll_RowCount)
				
	Do While ll_source_row <= ll_RowCount
		ll_rtn = 0
		ll_rtn = dw_sub_sect_dtl.Find  &
			( ls_SearchString, ll_source_row, ll_RowCount)
		If ll_rtn > 0 Then
			This.SetItem(ll_rtn,"subsect_type",data)
			If data = 'OUTUNGRD' Then
				This.SetItem(ll_rtn,"characteristic", ' ')
			End If
			ll_source_row = ll_rtn + 1
		Else
			ll_source_row = ll_RowCount + 1
		End If
		
	Loop
End If

If ls_ColumnName = "subsect_seq" Then
	li_seq = Integer(data)
	ls_name_code = string(This.GetItemNumber(row, "subsect_name_code"))
	ll_source_row = 1
	
	ls_Searchstring = "subsect_name_code = "
	ls_Searchstring += ls_name_code 
	
	Do While ll_source_row <= ll_RowCount
		ll_rtn = 0
		ll_rtn = This.Find  &
				( ls_SearchString, ll_source_row, ll_RowCount)
		If ll_rtn > 0 Then
			This.SetItem(ll_rtn,"subsect_seq",li_seq)
			ll_source_row = ll_rtn + 1
		Else
			ll_source_row = ll_Rowcount + 1
		End If
	Loop
End If

ll_source_row	= GetRow()
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				If This.GetItemString(ll_source_row, "update_flag") = "A" Then
					//don't change
				Else
					This.SetItem(ll_source_row, "update_flag", "U")
				End If
			END CHOOSE		
		End if
	END IF
END IF

wf_duplicate_prod_seq()

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)


return 0
end event

event itemerror;call super::itemerror;return (1)
end event

event rbuttondown;call super::rbuttondown;m_prd_sched	lm_popup
alignment la_align

String 	ls_temp, &
			ls_curr_col_label

integer 	li_rtn, &
			li_width, &
			li_date_position
			
ls_curr_col_label = dw_sub_sect_dtl.GetColumnName()

IF ls_curr_col_label = "subsect_name_code" Then
	lm_popup = Create m_prd_sched
	lm_popup.m_prd_area.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

End If

Destroy lm_popup
end event

event ue_postconstructor;call super::ue_postconstructor;is_selection = '3'
end event

type gb_select from groupbox within w_prd_sub_sect
integer x = 4242
integer y = 48
integer width = 411
integer height = 224
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

