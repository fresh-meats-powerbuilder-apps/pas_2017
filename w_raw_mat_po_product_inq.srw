$PBExportHeader$w_raw_mat_po_product_inq.srw
forward
global type w_raw_mat_po_product_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_raw_mat_po_product_inq
end type
type dw_product from u_fab_product_code within w_raw_mat_po_product_inq
end type
type dw_sched_date from u_sched_date within w_raw_mat_po_product_inq
end type
type delivery_t from statictext within w_raw_mat_po_product_inq
end type
type fab_t from statictext within w_raw_mat_po_product_inq
end type
type cbx_prod from checkbox within w_raw_mat_po_product_inq
end type
type checkbox_t from statictext within w_raw_mat_po_product_inq
end type
end forward

global type w_raw_mat_po_product_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 2272
integer height = 720
string title = "Products on Purchase Orders Inquire"
long backcolor = 67108864
event begindatechanged ( )
dw_plant dw_plant
dw_product dw_product
dw_sched_date dw_sched_date
delivery_t delivery_t
fab_t fab_t
cbx_prod cbx_prod
checkbox_t checkbox_t
end type
global w_raw_mat_po_product_inq w_raw_mat_po_product_inq

type variables
Boolean		ib_valid_return
w_base_sheet	iw_parent

w_raw_mat_det_sched 	iw_parent_window
end variables

event ue_postopen;call super::ue_postopen;Date							ldt_temp

Int							li_pos, &
								li_ret

String						ls_header, &
								ls_temp, &
								ls_product_code, &
								ls_product_descr,ls_state, &
								ls_date

u_string_functions		lu_string


iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If

iw_parent.Event ue_Get_Data('Product')
ls_product_code = Message.StringParm
iw_parent.Event ue_Get_Data('Product_Descr')
ls_product_descr = Message.StringParm
iw_parent.Event ue_Get_Data('State')
ls_state = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_state) Then dw_product.uf_set_product_state(ls_state)
dw_product.uf_set_product_code( ls_product_code, ls_product_descr)

iw_parent.Event ue_get_data('sched_date')
ls_date = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_date) Then
	dw_sched_date.uf_set_sched_date(date(ls_date))
Else
	dw_sched_date.SetItem(1, "sched_date", Today())
End If


if Not lu_string.nf_IsEmpty(ls_product_code) Then
	cbx_prod.Checked = True
	dw_product.Enabled = True
	dw_product.Visible = True
	fab_t.Visible = True
Else	
	cbx_prod.Checked = False
	dw_product.Enabled = False
	dw_product.Visible = False
	fab_t.Visible = False
End If

If dw_plant.RowCount() < 1 Then 
	dw_plant.InsertRow(0)
ElseIf Not lu_string.nf_IsEmpty(dw_plant.GetItemString(1,1)) Then
	dw_product.setfocus( )
End if


end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

if dw_product.rowcount( ) = 0 Then
	dw_product.insertrow( 0)
End If

if dw_sched_date.rowcount( ) = 0 Then
	dw_sched_date.insertrow( 0)
End If

end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

on w_raw_mat_po_product_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_product=create dw_product
this.dw_sched_date=create dw_sched_date
this.delivery_t=create delivery_t
this.fab_t=create fab_t
this.cbx_prod=create cbx_prod
this.checkbox_t=create checkbox_t
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_product
this.Control[iCurrent+3]=this.dw_sched_date
this.Control[iCurrent+4]=this.delivery_t
this.Control[iCurrent+5]=this.fab_t
this.Control[iCurrent+6]=this.cbx_prod
this.Control[iCurrent+7]=this.checkbox_t
end on

on w_raw_mat_po_product_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_product)
destroy(this.dw_sched_date)
destroy(this.delivery_t)
destroy(this.fab_t)
destroy(this.cbx_prod)
destroy(this.checkbox_t)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date	ldt_temp

String	ls_temp,ls_temp1, ls_date
Long 		ll_rtn

u_string_functions		lu_strings

If dw_plant.AcceptText() = -1 or &
	dw_sched_date.AcceptText() = -1 Then 
	return
End if

If cbx_prod.checked = True Then
	If dw_product.AcceptText() = -1 Then
		return
	End If
End If

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End if

If cbx_prod.checked = True Then
	If not dw_product.uf_validate_rmt( ) Then
		dw_product.setfocus()
		return
	End If

	If iw_frame.iu_string.nf_IsEmpty(dw_product.GetItemString(1, "fab_product_code")) Then
		iw_frame.SetMicroHelp("Product Code is a required field")
		dw_product.SetFocus()
		return	
	End if


	If iw_frame.iu_string.nf_IsEmpty(dw_product.GetItemString(1, "product_state")) Then
		iw_frame.SetMicroHelp("Product State is a required field")
		dw_product.SetFocus()
		return
	End if
End If

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant_Desc', ls_temp)

If cbx_prod.checked = True Then
	ls_temp = dw_product.uf_get_product_code( )
	iw_parent.Event ue_Set_Data('Product', ls_temp)

	ls_temp = dw_product.uf_get_product_desc( )
	iw_parent.Event ue_Set_Data('Product_Desc', ls_temp)

	ls_temp = dw_product.uf_get_product_state( )
	iw_parent.Event ue_Set_Data('State', ls_temp)
Else
	ls_temp = ' '
	iw_parent.Event ue_Set_Data('Product', ls_temp)
	iw_parent.Event ue_Set_Data('Product_Desc', ls_temp)
	iw_parent.Event ue_Set_Data('State', ls_temp)
End If

ldt_temp = dw_sched_date.GetItemDate(1, "sched_date")

iw_parent.Event ue_Set_Data('sched_date', ls_temp)
iw_parentwindow.Event ue_set_data('sched_date', &
		String(ldt_temp, 'yyyy-mm-dd'))

ib_valid_return = True
Close(This)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_raw_mat_po_product_inq
boolean visible = false
integer x = 2254
integer y = 388
integer taborder = 0
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_raw_mat_po_product_inq
integer x = 891
integer y = 452
integer taborder = 60
fontcharset fontcharset = ansi!
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_raw_mat_po_product_inq
integer x = 498
integer y = 452
integer taborder = 50
end type

type dw_plant from u_plant within w_raw_mat_po_product_inq
integer x = 581
integer y = 12
integer taborder = 10
end type

type dw_product from u_fab_product_code within w_raw_mat_po_product_inq
integer x = 526
integer y = 200
integer width = 1627
integer taborder = 40
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;If not dw_product.uf_validate_rmt( ) Then
	dw_product.setfocus()
	return
End If

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

dw_product.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()
end event
type dw_sched_date from u_sched_date within w_raw_mat_po_product_inq
integer x = 521
integer y = 104
integer taborder = 20
boolean bringtotop = true
end type

type delivery_t from statictext within w_raw_mat_po_product_inq
integer x = 366
integer y = 116
integer width = 238
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Delivery"
alignment alignment = right!
boolean focusrectangle = false
end type

type fab_t from statictext within w_raw_mat_po_product_inq
integer x = 402
integer y = 228
integer width = 146
integer height = 68
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Fab"
alignment alignment = right!
boolean focusrectangle = false
end type

type cbx_prod from checkbox within w_raw_mat_po_product_inq
integer x = 137
integer y = 224
integer width = 96
integer height = 64
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean lefttext = true
end type

event clicked;If cbx_prod.Checked Then
	dw_product.Enabled = True
	dw_product.Visible = True
	fab_t.Visible = True
Else
	dw_product.Reset()
	dw_product.Enabled = False
	dw_product.Visible = False
	fab_t.Visible = False
End If
end event

type checkbox_t from statictext within w_raw_mat_po_product_inq
integer x = 46
integer y = 296
integer width = 407
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Inquire on Product"
alignment alignment = right!
boolean focusrectangle = false
end type

