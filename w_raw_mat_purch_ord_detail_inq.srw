$PBExportHeader$w_raw_mat_purch_ord_detail_inq.srw
forward
global type w_raw_mat_purch_ord_detail_inq from w_base_response_ext
end type
type st_1 from statictext within w_raw_mat_purch_ord_detail_inq
end type
type sle_purchase_order from u_base_singlelineedit_ext within w_raw_mat_purch_ord_detail_inq
end type
end forward

global type w_raw_mat_purch_ord_detail_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 1477
integer height = 548
string title = "Raw Material Purchase Order Detail Inquire"
long backcolor = 67108864
event begindatechanged ( )
st_1 st_1
sle_purchase_order sle_purchase_order
end type
global w_raw_mat_purch_ord_detail_inq w_raw_mat_purch_ord_detail_inq

type variables
Boolean		ib_valid_return
w_base_sheet	iw_parent

string	is_begin_date
end variables

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))
			 
iw_parent.Event ue_Get_Data('purchase_order')
sle_purchase_order.text = Message.StringParm			 

//

end event

on w_raw_mat_purch_ord_detail_inq.create
int iCurrent
call super::create
this.st_1=create st_1
this.sle_purchase_order=create sle_purchase_order
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.sle_purchase_order
end on

on w_raw_mat_purch_ord_detail_inq.destroy
call super::destroy
destroy(this.st_1)
destroy(this.sle_purchase_order)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_base_ok;call super::ue_base_ok;String	ls_temp

ls_temp = sle_purchase_order.text
CloseWithReturn(This,sle_purchase_order.text)


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_raw_mat_purch_ord_detail_inq
boolean visible = false
integer x = 1696
integer y = 392
integer taborder = 80
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_raw_mat_purch_ord_detail_inq
integer x = 773
integer y = 284
integer taborder = 70
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_raw_mat_purch_ord_detail_inq
integer x = 315
integer y = 284
integer taborder = 60
end type

type st_1 from statictext within w_raw_mat_purch_ord_detail_inq
integer x = 302
integer y = 112
integer width = 361
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Purchase Order:"
boolean focusrectangle = false
end type

type sle_purchase_order from u_base_singlelineedit_ext within w_raw_mat_purch_ord_detail_inq
integer x = 727
integer y = 68
integer width = 530
integer height = 88
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
fontcharset fontcharset = ansi!
string facename = "Microsoft Sans Serif"
textcase textcase = upper!
integer limit = 10
boolean hideselection = false
end type

