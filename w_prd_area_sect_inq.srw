$PBExportHeader$w_prd_area_sect_inq.srw
forward
global type w_prd_area_sect_inq from w_base_response
end type
type dw_sched_type from u_sched_type within w_prd_area_sect_inq
end type
end forward

global type w_prd_area_sect_inq from w_base_response
integer x = 1075
integer y = 485
integer width = 1024
integer height = 468
string title = "Schedule Section Names Inquire"
long backcolor = 67108864
dw_sched_type dw_sched_type
end type
global w_prd_area_sect_inq w_prd_area_sect_inq

type variables
Boolean		ib_valid_return
w_base_sheet	iw_parent
end variables
on w_prd_area_sect_inq.create
int iCurrent
call super::create
this.dw_sched_type=create dw_sched_type
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sched_type
end on

on w_prd_area_sect_inq.destroy
call super::destroy
destroy(this.dw_sched_type)
end on

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event
event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_sched_type.RowCount() = 0 Then
	dw_sched_type.InsertRow(0)
End if
end event
event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event
event ue_base_ok;call super::ue_base_ok;String	ls_temp
Long 		ll_rtn
Integer	li_rtn

If dw_sched_type.AcceptText() = -1 Then
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_sched_type.GetItemString(1, "sched_type")) Then
	iw_frame.SetMicroHelp("Type is a required field")
	dw_sched_type.SetFocus()
	return
End if

ls_temp = dw_sched_type.uf_get_type( )
iw_parent.Event ue_Set_Data('sched_type', ls_temp)

ib_valid_return = True
Close(This)
end event
event ue_postopen;call super::ue_postopen;Int							li_pos, &
								li_ret
String						ls_sched_type

u_string_functions		lu_string

iw_parent.Event ue_Get_Data('sched_type')
ls_sched_type = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_sched_type) Then dw_sched_type.uf_set_type(ls_sched_type)


end event
type cb_base_help from w_base_response`cb_base_help within w_prd_area_sect_inq
boolean visible = false
integer x = 1129
integer y = 596
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_prd_area_sect_inq
integer x = 631
end type

type cb_base_ok from w_base_response`cb_base_ok within w_prd_area_sect_inq
integer x = 183
integer y = 216
end type

type dw_sched_type from u_sched_type within w_prd_area_sect_inq
integer x = 32
integer y = 68
boolean bringtotop = true
end type

