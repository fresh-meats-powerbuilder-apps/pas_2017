$PBExportHeader$w_prd_sub_sect.srw
forward
global type w_prd_sub_sect from w_base_sheet_ext
end type
type dw_copy_plant from u_plant within w_prd_sub_sect
end type
type dw_find_product from u_base_dw_ext within w_prd_sub_sect
end type
type copy_st from statictext within w_prd_sub_sect
end type
type dw_header from u_base_dw_ext within w_prd_sub_sect
end type
type dw_sub_sect_dtl from u_base_dw_ext within w_prd_sub_sect
end type
end forward

global type w_prd_sub_sect from w_base_sheet_ext
integer width = 4160
integer height = 1612
string title = "Schedule Sub Section "
long backcolor = 67108864
dw_copy_plant dw_copy_plant
dw_find_product dw_find_product
copy_st copy_st
dw_header dw_header
dw_sub_sect_dtl dw_sub_sect_dtl
end type
global w_prd_sub_sect w_prd_sub_sect

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product	

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

u_pas201		iu_pas201
u_pas203		iu_pas203

String		is_input, &
				is_debug, &
				is_name_string
	
DataWindowChild		idwc_sub_desc

u_sect_functions		iu_sect_functions
end variables

forward prototypes
public subroutine wf_delete ()
public function boolean wf_deleterow ()
public function boolean wf_validate (long al_row)
public function boolean wf_get_sub_desc ()
public function boolean wf_check_dups ()
public function boolean wf_retrieve ()
public function boolean wf_set_plant_copy ()
public function boolean wf_addrow ()
public function boolean wf_validate_type ()
public function boolean wf_validate_display ()
public function boolean wf_update ()
end prototypes

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_deleterow ();Boolean		lb_ret

Long			ll_row

String		ls_ind, &
				ls_filter, &
				ls_name_code

This.SetRedraw( False )
ll_row = dw_sub_sect_dtl.GetRow()

If ll_row < 1 Then
	Return True
End If

if dw_sub_sect_dtl.GetItemString(ll_row, "update_flag") = 'A' Then
	ls_name_code = dw_sub_sect_dtl.GetItemString(ll_row, "subsect_name_code")
	idwc_sub_desc = iu_sect_functions.uf_set_visible_ind(idwc_sub_desc, ls_name_code,'Y') 
End if

dw_sub_sect_dtl.SetItem(ll_row, "update_flag", 'D')


dw_sub_sect_dtl.SetFocus()
lb_ret = super::wf_deleterow()

dw_sub_sect_dtl.SelectRow(0,False)

This.SetRedraw( True )
return lb_ret
end function

public function boolean wf_validate (long al_row);Boolean				lb_duplicate

Integer				li_temp
						
Long					ll_rtn, &
						ll_nbrrows, &
						ll_ModifiedRow, &
						ll_search_row
						
String				ls_searchstring, &
						ls_temp, &
						ls_update_flag, &
						ls_subsect, &
						ls_subsect_find, &
						ls_type, &
						ls_type_find, &
						ls_sequence
											
						
IF dw_sub_sect_dtl.AcceptText() = -1 THEN Return( False )

IF al_row < 0 THEN Return True

ll_nbrrows = dw_sub_sect_dtl.RowCount()

ls_subsect = dw_sub_sect_dtl.GetItemString(al_row, "subsect_name_code")
IF IsNull(ls_subsect) Then 
	MessageBox("subsect_name_code", "Please choose a sub section description.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("subsect_name_code")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_type = dw_sub_sect_dtl.GetItemString(al_row, "subsect_type")
IF IsNull(ls_type) Then 
	MessageBox("subsect_type", "Please choose a sub section type.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("subsect_type")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

li_temp = dw_sub_sect_dtl.GetItemNumber(al_row, "subsect_seq")
IF li_temp > 0 Then 
	//do nothing
Else
	MessageBox("subsect_seq", "Please enter a valid sub section sequence number.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("subsect_seq")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

li_temp = dw_sub_sect_dtl.GetItemNumber(al_row, "product_seq")
IF li_temp > 0 Then 
	//do nothing
Else
	MessageBox("product_seq", "Please enter a valid product sequence number.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("product_seq")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "product_code")
If not invuo_fab_product_code.uf_check_product(ls_temp) then
	If	invuo_fab_product_code.ib_error_occurred then
		iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
		Return False
	Else
		iw_frame.SetMicroHelp(ls_temp + " is an invalid Product Code")
		Return False
	End If
End If

ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "product_state")
IF IsNull(ls_temp) Then 
	MessageBox("product_state", "Please choose a valid product state.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("product_state")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "display_ind")
IF IsNull(ls_temp) Then 
	MessageBox("display_ind", "Display indicator must be Yes or No.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("display_ind")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "schd_miss_ind")
IF IsNull(ls_temp) Then 
	MessageBox("schd_miss_ind", "Schedule miss indicator must be Yes or No.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("schd_miss_ind")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_sub_sect_dtl.GetItemString(al_row, "characteristic")
IF IsNull(ls_temp) Then 
	MessageBox("characteristic", "Please enter a valid characteristic.")
	This.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(al_row)
	dw_sub_sect_dtl.SetColumn("characteristic")
	dw_sub_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If


Return True
end function

public function boolean wf_get_sub_desc ();Integer	li_ret, &
			li_row_Count, &
			li_rtn, &
			li_counter
			
String	ls_input, &
			ls_output_values, &
			ls_temp, &
			ls_name_string, &
			ls_ind, &
			ls_filter, &
			ls_name_code
			
Long		ll_value, &
			ll_rtn
	
u_string_functions u_string

DataWindowChild	ldwc_temp


idwc_sub_desc.Reset()

ls_input = 'SUBSECT~r~n'

ls_output_values = iu_sect_functions.uf_get_names(ls_input)

li_row_count = idwc_sub_desc.ImportString(ls_output_values)

Return True

end function

public function boolean wf_check_dups ();Boolean		lb_yes_found

Integer		li_counter				

Long			ll_row_count, &
				ll_rtn

String		ls_namecode, &
				ls_product_code, &
				ls_product_state, &
				ls_SearchString


//product code/state cannot occur more than once under the same
//sub section name code

dw_sub_sect_dtl.SetSort("subsect_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()

ll_row_count = dw_sub_sect_dtl.RowCount()

li_counter = 1

ls_namecode = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_name_code")
ls_product_code = dw_sub_sect_dtl.GetItemString(li_counter, "product_code")
ls_product_code = dw_sub_sect_dtl.GetItemString(li_counter, "product_state")

ls_SearchString = "subsect_name_code = "
ls_Searchstring += "'" + ls_namecode + "'"
ls_SearchString = "product_code = "
ls_Searchstring += "'" + ls_product_code + "'"
ls_SearchString = "product_state = "
ls_Searchstring += "'" + ls_product_state + "'"

For li_counter = 1 to ll_row_Count
	
	CHOOSE CASE li_counter
		CASE 1
			ll_rtn = dw_sub_sect_dtl.Find  &
				( ls_SearchString, li_counter + 1, ll_row_count)
		CASE 2 to (ll_row_count - 1)
			ll_rtn = dw_sub_sect_dtl.Find ( ls_SearchString, li_counter - 1, 1)
			If ll_rtn = 0 Then ll_rtn = dw_sub_sect_dtl.Find  &
					(ls_SearchString, li_counter + 1, ll_row_count)
		CASE ll_row_count
			ll_rtn = dw_sub_sect_dtl.Find ( ls_SearchString, li_counter - 1, 1)
	END CHOOSE

	If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "There are duplicate product codes within the same subsection")
		dw_sub_sect_dtl.SetRedraw(False)
		dw_sub_sect_dtl.ScrollToRow(li_counter)
		dw_sub_sect_dtl.SetColumn("product_code")
		dw_sub_sect_dtl.SetRow(li_counter)
		dw_sub_sect_dtl.SelectRow(ll_rtn, True)
		dw_sub_sect_dtl.SelectRow(li_counter, True)
		dw_sub_sect_dtl.SetRedraw(True)
		Return False
	End if
Next

Return True
end function

public function boolean wf_retrieve ();Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_output_values, ls_temp, &
			ls_name_code
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_prd_sub_sect_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")

//wf_get_sub_desc()

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			This.dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			This.dw_header.GetItemString(1, 'sect_name_code') + '~t'
			
is_input = ls_input

li_ret = iu_pas201.nf_pasp56cr_inq_sched_sub_sects(istr_error_info, &
									is_input, &
									ls_output_values) 
						

This.dw_sub_sect_dtl.Reset()
This.dw_copy_plant.Reset()
This.dw_copy_plant.InsertRow(0)

If li_ret = 0 Then
	This.dw_sub_sect_dtl.ImportString(ls_output_values)
End If

ll_value = dw_sub_sect_dtl.RowCount()
If ll_value < 0 Then ll_value = 0

li_row_count = dw_sub_sect_dtl.RowCount()
For li_counter = 1 to li_Row_count
	ls_name_code = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_name_code")
	idwc_sub_desc = iu_sect_functions.uf_set_visible_ind(idwc_sub_desc, ls_name_code, 'N') 
Next	
	
dw_sub_sect_dtl.ResetUpdate()

IF ll_value > 0 THEN
	dw_sub_sect_dtl.SetFocus()
	dw_sub_sect_dtl.ScrollToRow(1)
	dw_sub_sect_dtl.SetColumn( "subsect_name_code" )
	dw_sub_sect_dtl.TriggerEvent("RowFocusChanged")
END IF

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )

dw_sub_sect_dtl.ResetUpdate()
//dw_copy_plant.SetItem(1,"location_code", " ")
//dw_copy_plant.SetItem(1,"location_name", " ")
dw_copy_plant.AcceptText()

Return True

end function

public function boolean wf_set_plant_copy ();Long			ll_row

ll_row = 1
ll_row = dw_sub_sect_dtl.GetSelectedRow(0)
If ll_row = 0 Then
	iw_frame.SetMicroHelp("No rows have been selected to copy")
	Return False
End If

Do While ll_row <> 0
	IF ll_row <> 0 THEN 
		dw_sub_sect_dtl.SetItem(ll_row, 'update_flag', 'A')
	End If
	
	ll_row = dw_sub_sect_dtl.GetSelectedRow(ll_row)
Loop

Return True
end function

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row
				
String		ls_ind, &
				ls_filter

If dw_sub_sect_dtl.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_row = dw_sub_sect_dtl.InsertRow(0)	


dw_sub_sect_dtl.ScrollToRow(ll_row)
dw_sub_sect_dtl.SetItem(ll_row, "update_flag", 'A')
dw_sub_sect_dtl.SetItem(ll_row, "product_code", '0')
dw_sub_sect_dtl.SetColumn("product_code")
dw_sub_sect_dtl.SetFocus()
This.SetRedraw(True)

return true



end function

public function boolean wf_validate_type ();Boolean		lb_first_seq

Integer		li_counter, &
				li_subsect_seq 

Long			ll_row_count

String		ls_name_type, &
				ls_display_ind


//The 'input raw material' type must have the lowest sequence number

dw_sub_sect_dtl.SetSort("subsect_seq")
dw_sub_sect_dtl.Sort()

ll_row_count = dw_sub_sect_dtl.RowCount()

li_counter = 1

if ll_row_count < li_counter then Return True

ls_name_type = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_type")
li_subsect_seq = dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_seq")
lb_first_seq = True

Do While li_counter <= ll_Row_count 
	if lb_first_seq = True Then
		lb_first_seq = False
	Else
		if ls_name_type = 'INRAWMAT' Then
			iw_frame.SetMicroHelp("Input Raw Material must be before all other sequence numbers")
			dw_sub_sect_dtl.ScrollToRow(li_counter)
			dw_sub_sect_dtl.SetRow(li_counter)
			Return False
		End If
	End If
	li_counter = li_counter + 1
	
	If li_counter <= ll_Row_count Then
		
		If li_subsect_seq  = dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_seq") Then
			//do nothing
		Else
			ls_name_type = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_type")
			li_subsect_seq  = dw_sub_sect_dtl.GetItemNumber(li_counter, "subsect_seq")
			lb_first_seq = True
		End If
	End if
Loop
Return True
end function

public function boolean wf_validate_display ();Boolean		lb_yes_found

Integer		li_counter, &
				li_product_seq

Long			ll_row_count

String		ls_namecode, &
				ls_display_ind

DataStore	lds_detail


//at least one row must have a display ind = 'Y' for each
//subsect name code/product seq combination
lds_detail = Create DataStore
lds_detail.DataObject = "d_sched_sub_sect"

dw_sub_sect_dtl.SetSort("subsect_name_code, product_seq")
dw_sub_sect_dtl.Sort()

ll_row_count = dw_sub_sect_dtl.RowCount()

li_counter = 1

if ll_row_count < li_counter then Return True

ls_namecode = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_name_code")
li_product_seq = dw_sub_sect_dtl.GetItemNumber(li_counter, "product_seq")
lb_yes_found = False

Do While li_counter <= ll_Row_count 
	ls_display_ind = dw_sub_sect_dtl.GetItemString(li_counter, "display_ind")
	if ls_display_ind = 'Y' Then
		lb_yes_found = True
	End If
	li_counter = li_counter + 1
	
	If li_counter <= ll_Row_count Then
		If ls_namecode = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_name_code") &
			and li_product_seq = dw_sub_sect_dtl.GetItemNumber(li_counter, "product_seq") Then
			ls_display_ind = dw_sub_sect_dtl.GetItemString(li_counter, "display_ind")
			if ls_display_ind = 'Y' Then
				lb_yes_found = True
			End If
		Else
			If lb_yes_found = False Then
				iw_frame.SetMicroHelp("At least one product in a product seq group must be Display Yes")
				dw_sub_sect_dtl.ScrollToRow(li_counter - 1)
				dw_sub_sect_dtl.SetRow(li_counter - 1)
				Return False
			End If
			lb_yes_found = False
			ls_namecode = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_name_code")
			li_product_seq = dw_sub_sect_dtl.GetItemNumber(li_counter, "product_seq")
		End If
	End if
		
Loop
Return True
end function

public function boolean wf_update ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp

string			ls_Update_string, &
					ls_header_string, &
					ls_output_string, &
					ls_ind, &
					ls_name_code, &
					ls_copy_plant, &
					ls_input
					

IF dw_sub_sect_dtl.AcceptText() = -1 &
	And dw_copy_plant.AcceptText() = -1 THEN
	Return( False )
End If
ll_temp = dw_sub_sect_dtl.DeletedCount()

ls_copy_plant = dw_copy_plant.GetItemString(1,"location_code")
if IsNull(ls_copy_plant) or ls_copy_plant = '   ' or &
	ls_copy_plant = dw_header.GetItemString(1,"plant_code") Then
	IF dw_sub_sect_dtl.ModifiedCount() + dw_sub_sect_dtl.DeletedCount() <= 0 THEN 
		Return( False )
	End If
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_sub_sect_dtl.RowCount( )

dw_sub_sect_dtl.SetReDraw(False)
ll_row = 0

ls_copy_plant = dw_copy_plant.GetItemString(1,"location_code")
if IsNull(ls_copy_plant) or ls_copy_plant = '   ' or &
	ls_copy_plant = dw_header.GetItemString(1,"plant_code") Then
	is_input = is_input + 'N~t' + ls_copy_plant + '~t'
Else
	if not wf_set_plant_copy() Then
		dw_sub_sect_dtl.SetReDraw(True)
		Return False
	End If
	ls_input = ls_copy_plant + '~t' + &
			This.dw_header.GetItemString(1, 'area_name_code') + '~t' + &
			This.dw_header.GetItemString(1, 'sect_name_code') + '~t' + &
			'Y~t' + dw_header.GetItemString(1,"plant_code") + '~t'
	is_input = ls_input
End If

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_sub_sect_dtl.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then
			Return False
		End If
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP
// validate the input raw material type sequence
If not wf_validate_type() Then
	dw_sub_sect_dtl.SetReDraw(True)
	Return False
End If
//validate the display ind
If not wf_validate_display() Then
	dw_sub_sect_dtl.SetReDraw(True)
	Return False
End If



dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()

ls_header_string = is_input
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_sub_sect_dtl)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp57cr_upd_sched_sub_sects"
istr_error_info.se_message = Space(71)

If not iu_pas201.nf_pasp57cr_upd_sched_sub_sects(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then  Return False

iw_frame.SetMicroHelp("Update Successful")


ll_RowCount = dw_sub_sect_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
	ls_name_code = dw_sub_sect_dtl.GetItemString(li_counter, "subsect_name_code")
	idwc_sub_desc = iu_sect_functions.uf_set_visible_ind(idwc_sub_desc, ls_name_code, 'N')
	dw_sub_sect_dtl.SetItem(li_Counter, 'update_flag', ' ')
Next

dw_sub_sect_dtl.ResetUpdate()
dw_sub_sect_dtl.SetSort("subsect_seq, product_seq, product_code, product_state")
dw_sub_sect_dtl.Sort()
dw_sub_sect_dtl.SetFocus()
dw_sub_sect_dtl.SetReDraw(True)

Return( True )
end function
event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_pas203) Then Destroy(iu_pas203)


end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_sub_sect_dtl.InsertRow(0)
dw_copy_plant.InsertRow(0)
dw_find_product.InsertRow(0)

end event

event ue_postopen;call super::ue_postopen;String						ls_char

DataWindowChild			ldwc_type, &
								ldwc_char, &
								ldwc_state
								
Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Schedule Section By Plant"
istr_error_info.se_user_id = sqlca.userid

idwc_sub_desc.Reset()
dw_sub_sect_dtl.GetChild("subsect_name_code", idwc_sub_desc)

//populate the description drop down
wf_get_sub_desc()

//populate the type drop down
dw_sub_sect_dtl.GetChild('subsect_type', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PPSSTYPE")

//populate the product state drop down
dw_sub_sect_dtl.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()

// populate the characteristics drop down

iu_pas203 = Create u_pas203
//If Message.ReturnValue = -1 Then 
//	Close(This)
//	return
//End if
istr_error_info.se_event_name = "ue_postconstructor"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_sub_sect_dtl.GetChild("characteristic", ldwc_char)
ldwc_char.ImportString(ls_char)

This.PostEvent("ue_query")

end event

on w_prd_sub_sect.create
int iCurrent
call super::create
this.dw_copy_plant=create dw_copy_plant
this.dw_find_product=create dw_find_product
this.copy_st=create copy_st
this.dw_header=create dw_header
this.dw_sub_sect_dtl=create dw_sub_sect_dtl
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_copy_plant
this.Control[iCurrent+2]=this.dw_find_product
this.Control[iCurrent+3]=this.copy_st
this.Control[iCurrent+4]=this.dw_header
this.Control[iCurrent+5]=this.dw_sub_sect_dtl
end on

on w_prd_sub_sect.destroy
call super::destroy
destroy(this.dw_copy_plant)
destroy(this.dw_find_product)
destroy(this.copy_st)
destroy(this.dw_header)
destroy(this.dw_sub_sect_dtl)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Area Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'area_name_code')
	Case 'Sect Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'sect_name_code')
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Area Name Code'
		dw_header.SetItem(1, 'area_name_code', as_value)
	Case 'Area Descr'
		dw_header.SetItem(1, 'area_descr', as_value)
	Case 'Sect Name Code'
		dw_header.SetItem(1, 'sect_name_code', as_value)
	Case 'Sect Descr'
		dw_header.SetItem(1, 'sect_descr', as_value)
End Choose


end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_sub_sect_dtl.x * 2) + 30 
li_y = dw_sub_sect_dtl.y + 115

if width > li_x Then
	dw_sub_sect_dtl.width	= width - li_x
end if

if height > li_y then
	dw_sub_sect_dtl.height	= height - li_y
end if
end event

type dw_copy_plant from u_plant within w_prd_sub_sect
integer x = 1961
integer y = 96
integer width = 1481
integer taborder = 10
end type

type dw_find_product from u_base_dw_ext within w_prd_sub_sect
integer x = 1650
integer y = 232
integer width = 1166
integer height = 104
integer taborder = 30
string dataobject = "d_find_product"
boolean border = false
end type

event editchanged;call super::editchanged;Long	ll_row, &
		ll_first_row, &
		ll_last_row

ll_row = dw_sub_sect_dtl.Find("product_code = '" + data + "'",1,dw_sub_sect_dtl.RowCount()+1)

If ll_row > 0 Then 
	dw_sub_sect_dtl.ScrollToRow(ll_row)
	dw_sub_sect_dtl.SetRow(ll_row + 1)
End If

ll_first_row = Long(dw_sub_sect_dtl.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_sub_sect_dtl.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_sub_sect_dtl.SetRedraw(False)
	dw_sub_sect_dtl.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_sub_sect_dtl.ScrollToRow(ll_row)
	dw_sub_sect_dtl.SetRow(ll_row + 1)
	dw_sub_sect_dtl.SetRedraw(True)
End If
end event

type copy_st from statictext within w_prd_sub_sect
integer x = 1449
integer y = 108
integer width = 530
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Copy Selected Rows to "
boolean focusrectangle = false
end type

type dw_header from u_base_dw_ext within w_prd_sub_sect
integer y = 4
integer width = 1371
integer height = 240
integer taborder = 0
boolean enabled = false
string dataobject = "d_prd_area_sect_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False




end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_sub_sect_dtl from u_base_dw_ext within w_prd_sub_sect
integer y = 384
integer width = 4133
integer height = 1068
integer taborder = 10
string dataobject = "d_sched_sub_sect"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;Long		ll_rtn
String	ls_temp, &
			ls_ColumnName, &
			ls_avail_ind, &
			ls_SearchString, &
			ls_ind, &
			ls_filter
			
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return

IF row > 0 Then
//	messagebox('Clicked Event',string(row))
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If




end event

event constructor;call super::constructor;ib_updateable = True

end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

Integer		li_seq

long			ll_source_row, &
				ll_RowCount, &
				ll_rtn

String		ls_ColumnName, &
				ls_SearchString, &
				ls_type, &
				ls_name_code								

nvuo_pa_business_rules	u_rule

il_ChangedRow = 0

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

ll_RowCount = This.RowCount()

// On a new row set the subsection type and sequence based
// upon the name code that is chosen.  All rows with the same
// name code must have the same type and sequence.
If ls_ColumnName = "subsect_name_code" Then
	ls_name_code = data
	If This.GetItemString(row, "update_flag") = 'A' Then
		ls_Searchstring = "subsect_name_code = "
		ls_Searchstring += "'" + ls_name_code + "'"
		ll_rtn = dw_sub_sect_dtl.Find  &
				( ls_SearchString, 1, ll_RowCount)
		If ll_rtn > 0 Then
			This.SetItem(row,"subsect_type",This.GetItemString(ll_rtn,'subsect_type'))
			This.SetItem(row,"subsect_seq",This.GetItemNumber(ll_rtn,'subsect_seq'))
		End If
	End If
End If
	
If ls_ColumnName = "subsect_type" Then
	ls_type = data
	ls_name_code = This.GetItemString(row, "subsect_name_code")
	ll_source_row = 1
	
	ls_Searchstring = "subsect_name_code = "
	ls_Searchstring += "'" + ls_name_code + "'"
	ll_rtn = dw_sub_sect_dtl.Find  &
			( ls_SearchString, 1, ll_RowCount)
				
	Do While ll_source_row <= ll_RowCount
		ll_rtn = 0
		ll_rtn = dw_sub_sect_dtl.Find  &
			( ls_SearchString, ll_source_row, ll_RowCount)
		If ll_rtn > 0 Then
			This.SetItem(ll_rtn,"subsect_type",data)
			ll_source_row = ll_rtn + 1
		Else
			ll_source_row = ll_RowCount + 1
		End If
		
	Loop
End If

If ls_ColumnName = "subsect_seq" Then
	li_seq = Integer(data)
	ls_name_code = This.GetItemString(row, "subsect_name_code")
	ll_source_row = 1
	
	ls_Searchstring = "subsect_name_code = "
	ls_Searchstring += "'" + ls_name_code + "'"

	Do While ll_source_row <= ll_RowCount
		ll_rtn = 0
		ll_rtn = This.Find  &
				( ls_SearchString, ll_source_row, ll_RowCount)
		If ll_rtn > 0 Then
			This.SetItem(ll_rtn,"subsect_seq",li_seq)
			ll_source_row = ll_rtn + 1
		Else
			ll_source_row = ll_Rowcount + 1
		End If
	Loop
End If

ll_source_row	= GetRow()
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				If This.GetItemString(ll_source_row, "update_flag") = "A" Then
					//don't change
				Else
					This.SetItem(ll_source_row, "update_flag", "U")
				End If
			END CHOOSE		
		End if
	END IF
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;return (1)
end event

