$PBExportHeader$w_prd_carcass.srw
forward
global type w_prd_carcass from w_base_sheet_ext
end type
type dw_prd_carcass_speed from u_base_dw_ext within w_prd_carcass
end type
type dw_prd_carcass_chain from u_base_dw_ext within w_prd_carcass
end type
type dw_header from u_base_dw_ext within w_prd_carcass
end type
type dw_prd_carcass_dtl from u_base_dw_ext within w_prd_carcass
end type
end forward

global type w_prd_carcass from w_base_sheet_ext
integer width = 1906
integer height = 1744
string title = "Carcass Update"
long backcolor = 67108864
dw_prd_carcass_speed dw_prd_carcass_speed
dw_prd_carcass_chain dw_prd_carcass_chain
dw_header dw_header
dw_prd_carcass_dtl dw_prd_carcass_dtl
end type
global w_prd_carcass w_prd_carcass

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
DataStore	ids_tree

Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product	
				


nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

Boolean	ib_first_time, &
			ib_expand_all, &
			ib_set_redraw = False, &
			ib_update_successful, &
			ib_inquire_required, &
			ib_endoftree, &
			ib_compute_yields, &
			ib_first_compute

s_error		istr_error_info

u_pas201		iu_pas201
u_pas203		iu_pas203

String		is_input, &
				is_debug, &
				is_chain, &
				is_name_string
	
DataWindowChild		idwc_sect_desc

u_sect_functions		iu_sect_functions
end variables

forward prototypes
public subroutine wf_delete ()
public subroutine wf_set_filter (string as_ind)
public function boolean wf_addrow ()
public function boolean wf_deleterow ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_delete ();wf_deleterow()
end subroutine

public subroutine wf_set_filter (string as_ind);String 	ls_filter


ls_filter = "available_ind = '" + as_ind + "'"
idwc_sect_desc.SetFilter(ls_filter)
idwc_sect_desc.Filter()
end subroutine

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row
				
String		ls_ind, &
				ls_filter

If dw_prd_carcass_dtl.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_row = dw_prd_carcass_dtl.InsertRow(0)	

wf_set_filter('N')
//ls_ind = 'N'
//ls_filter = "available_ind = '" + ls_ind + "'"
//idwc_sect_desc.SetFilter(ls_filter)
//idwc_sect_desc.Filter()

dw_prd_carcass_dtl.ScrollToRow(ll_row)
dw_prd_carcass_dtl.SetItem(ll_row, "update_flag", 'A')
dw_prd_carcass_dtl.SetItem(ll_row, "sequence", '0')
dw_prd_carcass_dtl.SetColumn("sequence")
dw_prd_carcass_dtl.SetFocus()
This.SetRedraw(True)

return true



end function

public function boolean wf_deleterow ();Boolean		lb_ret

Long			ll_row

String		ls_ind, &
				ls_filter, &
				ls_name_code

This.SetRedraw( False )
ll_row = dw_prd_carcass_dtl.GetRow()

If ll_row < 1 Then
	Return True
End If

//if dw_prd_carcass_dtl.GetItemString(ll_row, "update_flag") = 'A' Then
//	ls_name_code = String(dw_prd_carcass_dtl.GetItemNumber(ll_row, "name_code"))
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code,'Y') 
//End if

dw_prd_carcass_dtl.SetItem(ll_row, "update_flag", 'D')

wf_set_filter('N')

dw_prd_carcass_dtl.SetFocus()
lb_ret = super::wf_deleterow()

dw_prd_carcass_dtl.SelectRow(0,False)

This.SetRedraw( True )
return lb_ret
end function

public function boolean wf_validate (long al_row);Integer				li_temp

Long					ll_rtn, &
						ll_nbrrows, &
						ll_ModifiedRow, &
						ll_find
						
String				ls_searchstring, &
						ls_temp, &
						ls_update_flag, &
						ls_sched_type, &
						ls_sequence, &
						ls_string, &
						ls_product, &
						ls_find_string, &
						ls_char_key, &
						ls_state
				
			
											
						
IF dw_prd_carcass_dtl.AcceptText() = -1 THEN Return( False )

ll_nbrrows = dw_prd_carcass_dtl.RowCount()

//ls_temp = String(dw_prd_carcass_dtl.GetItemNumber(al_row, "name_code"))
//IF IsNull(ls_temp) Then 
//	MessageBox("name_code", "Please choose a section description.")
//	This.SetRedraw(False)
//	dw_prd_carcass_dtl.ScrollToRow(al_row)
//	dw_prd_carcass_dtl.SetColumn("name_code")
//	dw_prd_carcass_dtl.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If

li_temp = dw_prd_carcass_dtl.GetItemNumber(al_row, "sequence")
ls_sequence = string(dw_prd_carcass_dtl.GetItemNumber(al_row, "sequence"))
IF li_temp > 0 Then 
	//do nothing
Else
	MessageBox("sequence", "Please enter a sequence number.")
	This.SetRedraw(False)
	dw_prd_carcass_dtl.ScrollToRow(al_row)
	dw_prd_carcass_dtl.SetColumn("sequence")
	dw_prd_carcass_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

ls_Searchstring = "sequence = "
ls_Searchstring += String(dw_prd_carcass_dtl.GetItemNumber(al_row, "sequence"))

CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_prd_carcass_dtl.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_prd_carcass_dtl.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_prd_carcass_dtl.Find  &
			(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_prd_carcass_dtl.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE

If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "There are duplicate sequence numbers")
		dw_prd_carcass_dtl.SetRedraw(False)
		dw_prd_carcass_dtl.ScrollToRow(al_row)
		dw_prd_carcass_dtl.SetColumn("sequence")
		dw_prd_carcass_dtl.SetRow(al_row)
		dw_prd_carcass_dtl.SelectRow(ll_rtn, True)
		dw_prd_carcass_dtl.SelectRow(al_row, True)
		dw_prd_carcass_dtl.SetRedraw(True)
		Return False
End if


ls_product = dw_prd_carcass_dtl.GetItemString(al_row, "product")
If IsNull(ls_product) or Len(trim(ls_product)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a product code.  Product Code cannot be left blank.")
	This.SetRedraw(False)
	dw_prd_carcass_dtl.ScrollToRow(al_row)
	dw_prd_carcass_dtl.SetColumn("product")
	dw_prd_carcass_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If not invuo_fab_product_code.uf_check_product(ls_product) then
		If	invuo_fab_product_code.ib_error_occurred then
			iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
			Return False
		Else
			iw_frame.SetMicroHelp(ls_product + " is an invalid Product Code")
			This.SetRedraw(False)
			dw_prd_carcass_dtl.ScrollToRow(al_row)
			dw_prd_carcass_dtl.SetColumn("product")
			dw_prd_carcass_dtl.SetFocus()
			This.SetRedraw(True)
			Return False
		End If
	End If
End If

ls_state = dw_prd_carcass_dtl.GetItemString(al_row, "state")
ls_char_key = string(dw_prd_carcass_dtl.GetItemNumber(al_row, "char_key"))
If al_row > 1 then
	ls_find_string = 	"product = '"+ ls_product +&
							"' and state = '" + ls_state + "'" +&
							" and char_key = " + ls_char_key 
	
//	ls_find_string = "sequence = "
//	ls_find_string += string(dw_prd_carcass_dtl.GetItemNumber(al_row, "sequence"))
//	ls_find_string += "product = "
//	ls_find_string += dw_prd_carcass_dtl.GetItemString(al_row, "product")
//	ls_find_string += " and state = "
//	ls_find_string += dw_prd_carcass_dtl.GetItemString(al_row, "state")
//	ls_find_string += " and char_key = "
//	ls_find_string += string(dw_prd_carcass_dtl.GetItemNumber(al_row, "char_key"))
////	ls_find_string += ""


CHOOSE CASE al_row 
		CASE 1
			ll_rtn = dw_prd_carcass_dtl.Find  &
					( ls_find_string, al_row + 1, ll_nbrrows)
		CASE 2 to (ll_nbrrows - 1)
			ll_rtn = dw_prd_carcass_dtl.Find ( ls_find_string, al_row - 1, 1)
			If ll_rtn = 0 Then ll_rtn = dw_prd_carcass_dtl.Find  &
				(ls_find_string, al_row + 1, ll_nbrrows)
		CASE ll_nbrrows 
			ll_rtn = dw_prd_carcass_dtl.Find ( ls_find_string, al_row - 1, 1)
	END CHOOSE

	If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "There are duplicate products with the" + &
  						" same Product State and Characteristics.")
		dw_prd_carcass_dtl.SetRedraw(False)
		dw_prd_carcass_dtl.ScrollToRow(al_row)
		dw_prd_carcass_dtl.SetColumn("product")
		dw_prd_carcass_dtl.SetRow(al_row)
		dw_prd_carcass_dtl.SelectRow(ll_rtn, True)
		dw_prd_carcass_dtl.SelectRow(al_row, True)
		dw_prd_carcass_dtl.SetRedraw(True)
		Return False
	End If
End If
//	ll_find = dw_prd_carcass_dtl.Find(ls_find_string, 1, al_row - 1)
//	If (ll_find > 0) Then
//		iw_Frame.SetMicroHelp( "There are duplicate products with the" + &
//				" same line number and production date.")
//		Return False		  
//	End If
//	If ll_nbrrows > al_row then
//		ll_find = dw_prd_carcass_dtl.Find(ls_find_string, al_row + 1, ll_nbrrows)
//		If (ll_find > 0) Then
//			iw_Frame.SetMicroHelp( "There are duplicate products with the" + &
//					" same product state and characteristics .")
//			Return False		  
//		End If
//	End If
//End If
dw_prd_carcass_dtl.SelectRow(0, False)
Return True
end function

public function boolean wf_update ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp

string			ls_Update_string, &
					ls_header_string, &
					ls_output_string, &
					ls_ind, &
					ls_filter, &
					ls_input, &
					ls_flag, &
					ls_chain
					

IF dw_prd_carcass_dtl.AcceptText() = -1 THEN Return( False )
ll_temp = dw_prd_carcass_dtl.DeletedCount()
//IF dw_prd_carcass_dtl.ModifiedCount() + dw_prd_carcass_dtl.DeletedCount() <= 0 THEN Return( False )

ll_NbrRows = dw_prd_carcass_dtl.RowCount( )
if ll_NbrRows <= 0 And ll_temp <=0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

//ll_NbrRows = dw_prd_carcass_dtl.RowCount( )

ll_row = 1

DO WHILE ll_Row <= ll_NbrRows 
//	ll_Row = dw_prd_carcass_dtl.GetNextModified(ll_Row, Primary!)
//	IF ll_Row > 0 THEN 
	if Not wf_validate(ll_row) Then
//		dw_prd_carcass_dtl.SetReDraw(False)
//		wf_set_filter('N')
//		dw_prd_carcass_dtl.SetReDraw(True)
		Return False
	End If
	ls_flag = dw_prd_carcass_dtl.GetItemString(ll_row, "update_flag")
	if ls_flag > ' ' then
	else
		dw_prd_carcass_dtl.SetItem(ll_row, "update_flag", "A")
	end if
	ll_row ++
LOOP
ls_chain =  string(dw_prd_carcass_speed.GetItemNumber(1, "chain_speed1"))

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			String(This.dw_header.GetItemString(1, 'area_name_code')) + '~t' + &
			String(dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + &
			This.dw_header.GetItemString(1, "Shift") + '~t' + &
			string(dw_prd_carcass_chain.GetItemNumber(1, "hours")) + '~t' + &
			string(dw_prd_carcass_speed.GetItemNumber(1, "chain_speed1")) + '~t' + &
			string(dw_prd_carcass_dtl.getItemNumber(1, "qty_compute")) + '~r~n'

is_input = ls_input
ls_header_string = is_input
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_prd_carcass_dtl)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp59cr_upd_sched_carcass"
istr_error_info.se_message = Space(71)

If not iu_pas201.nf_pasp59cr_upd_sched_carcass(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then 
	Return False
	end if

dw_prd_carcass_dtl.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = idwc_sect_desc.RowCount()
//For li_counter = 1 to ll_RowCount
//	idwc_sect_desc.SetItem(li_Counter, 'available_ind', 'Y')
//Next	
//
ll_RowCount = dw_prd_carcass_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
//	ls_name_code = String(dw_prd_carcass_dtl.GetItemNumber(li_counter, "name_code"))
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code, 'N') 
	dw_prd_carcass_dtl.SetItem(li_Counter, 'update_flag', ' ')
Next
//ib_ReInquire = True
//wf_set_filter('N')
dw_prd_carcass_dtl.ResetUpdate()
dw_prd_carcass_chain.ResetUpdate()
dw_prd_carcass_speed.ResetUpdate()
//dw_prd_carcass_dtl.Sort()
dw_prd_carcass_dtl.SetFocus()
dw_prd_carcass_dtl.SetReDraw(True)

Return( True )
end function
public function boolean wf_retrieve (); Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_chain, &
			ls_output_values, ls_temp, &
			ls_name_code
			
Long		ll_value, &
			ll_rtn, &
			ll_row, ll_qty, &
			ll_hours, ll_speed
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_prd_carcass_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")

//wf_get_sect_desc()

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
			String(This.dw_header.GetItemString(1, 'area_name_code')) + '~t' + &
			String(dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + &
			This.dw_header.GetItemString(1, "Shift") + '~r~n'

is_input = ls_input

li_ret = iu_pas201.nf_pasp58cr_inq_sched_carcass(istr_error_info, &
									is_input, &
									ls_output_values, &
									ls_chain) 
						

This.dw_prd_carcass_chain.Reset()
//This.dw_prd_carcass_speed.Reset()

If li_ret = 0 Then
	This.dw_prd_carcass_chain.ImportString(ls_chain)
End If
ll_hours = dw_prd_carcass_chain.GetItemNumber(1, "hours")

This.dw_prd_carcass_dtl.Reset()
If li_ret = 0 Then
	This.dw_prd_carcass_dtl.ImportString(ls_output_values)
End If

li_row_count = dw_prd_carcass_dtl.RowCount()
if li_row_count > 0 then
	ll_qty = dw_prd_carcass_dtl.getItemNumber(1, "qty_compute")
end if

if li_row_count > 0 then
	if ll_hours < 9 and ll_hours > 0 then
		ll_speed = ll_qty / (ll_hours - .25)
		dw_prd_carcass_speed.SetItem(1, "chain_speed1", ll_speed)
//		dw_prd_carcass_speed.Modify("chain_speed.Expression ='" + String(ll_qty) + "/(" + String(ll_hours) + "- .25)'")
	else
		ll_speed = ll_qty / (ll_hours - .75)
		dw_prd_carcass_speed.SetItem(1, "chain_speed1", ll_speed)
//		dw_prd_carcass_speed.Modify("chain_speed.Expression ='" + String(ll_qty) + "/(" + String(ll_hours) + "- .75)'")
	end if
end if	

ll_value = dw_prd_carcass_dtl.RowCount()
If ll_value < 0 Then ll_value = 0

//li_row_count = dw_prd_carcass_dtl.RowCount()
//For li_counter = 1 to li_Row_count
//	ls_name_code = String(dw_prd_carcass_dtl.GetItemNumber(li_counter, "name_code"))
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code, 'N') 
//Next	
	
dw_prd_carcass_dtl.ResetUpdate()

IF ll_value > 0 THEN
	dw_prd_carcass_dtl.SetFocus()
	dw_prd_carcass_dtl.ScrollToRow(1)
	dw_prd_carcass_dtl.SetColumn( "sequence" )
	dw_prd_carcass_dtl.TriggerEvent("RowFocusChanged")
END IF

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )

dw_prd_carcass_dtl.ResetUpdate()
dw_prd_carcass_chain.ResetUpdate()
dw_prd_carcass_speed.ResetUpdate()
//dw_prd_carcass_speed.ResetUpdate()

Return True

end function
event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)




end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;

wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
//dw_prd_carcass_chain.InsertRow(0)
dw_prd_carcass_speed.InsertRow(0)
dw_prd_carcass_dtl.InsertRow(0)


end event

event ue_postopen;call super::ue_postopen;Environment		le_env
String			ls_char, &
					ls_group_id, &
					ls_modify_auth

DataWindowChild	ldwc_char, &
						ldwc_temp


iu_pas203 = Create u_pas203
iu_pas201 = Create u_pas201

istr_error_info.se_event_name = "ue_postopen"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_prd_carcass_dtl.GetChild("characteristic", ldwc_char)
ldwc_char.ImportString(ls_char)

dw_prd_carcass_dtl.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)


If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Carcass Update"
istr_error_info.se_user_id = sqlca.userid


//idwc_sect_desc.Reset()
//dw_prd_carcass_dtl.GetChild("name_code", idwc_sect_desc)

This.PostEvent("ue_query")

end event

on w_prd_carcass.create
int iCurrent
call super::create
this.dw_prd_carcass_speed=create dw_prd_carcass_speed
this.dw_prd_carcass_chain=create dw_prd_carcass_chain
this.dw_header=create dw_header
this.dw_prd_carcass_dtl=create dw_prd_carcass_dtl
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prd_carcass_speed
this.Control[iCurrent+2]=this.dw_prd_carcass_chain
this.Control[iCurrent+3]=this.dw_header
this.Control[iCurrent+4]=this.dw_prd_carcass_dtl
end on

on w_prd_carcass.destroy
call super::destroy
destroy(this.dw_prd_carcass_speed)
destroy(this.dw_prd_carcass_chain)
destroy(this.dw_header)
destroy(this.dw_prd_carcass_dtl)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Name Type'
		Message.StringParm = dw_header.GetItemString(1, 'name_type')
	Case 'Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'area_name_code')
	Case 'Sched Date'
		Message.StringParm = string(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
	Case 'Shift'
		Message.StringParm = dw_header.GetItemString(1, 'Shift')	
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Name Type'
		dw_header.SetItem(1, 'name_type', as_value)
	Case 'Name Code'
		dw_header.SetItem(1, 'area_name_code', as_value)
	Case 'Type Descr'
		dw_header.SetItem(1, 'type_descr', as_value)	
	Case 'Sched Date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
	Case 'Shift'
		dw_header.SetItem(1, 'Shift', as_value)		
End Choose


end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_prd_carcass_dtl.x * 2) + 30 
li_y = dw_prd_carcass_dtl.y + 115

if width > li_x Then
	dw_prd_carcass_dtl.width	= width - li_x
end if

if height > li_y then
	dw_prd_carcass_dtl.height	= height - li_y
end if
end event

type dw_prd_carcass_speed from u_base_dw_ext within w_prd_carcass
integer x = 786
integer y = 236
integer width = 338
integer height = 244
integer taborder = 0
string dataobject = "d_prd_carcass_speed"
boolean border = false
end type

type dw_prd_carcass_chain from u_base_dw_ext within w_prd_carcass
integer x = 1115
integer y = 308
integer width = 389
integer height = 168
integer taborder = 10
string dataobject = "d_prd_carcass_chain"
boolean border = false
end type

event itemchanged;call super::itemchanged;Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_chain, &
			ls_output_values, ls_temp, &
			ls_name, ls_hours
			
			
Long		ll_value, &
			ll_rtn, &
			ll_row, ll_qty, &
			ll_hours, ll_speed

ls_name = dwo.name


CHOOSE CASE ls_name
	Case 'hours'
		IF Not IsNumber(data) Then
			iw_frame.SetMicroHelp("Hours must be numeric")
			This.SelectText(1, 100)
			Return 1
		End IF	
		If Dec(data) < 0 Then
			iw_frame.SetMicroHelp("Hours may not be less than zero")
			This.SelectText(1, 100)
			Return 1
		End IF	
//		This.AcceptText()
//		dw_prd_carcass_chain.ImportString(ls_chain)
//		ls_hours = string(data)
		ll_hours = dec(data)
		ll_qty = dw_prd_carcass_dtl.getItemNumber(1, "qty_compute")
		if dec(data) < 9 and dec(data) > 0 then
			ll_speed = ll_qty / (ll_hours - .25)
			dw_prd_carcass_speed.SetItem(1, "chain_speed1", ll_speed)
	//		ls_temp = dw_prd_carcass_speed.Modify("chain_speed.Expression ='" + String(ll_qty) + "/(" + String(data) + "- .25)'")
		else
			ll_speed = ll_qty / (ll_hours - .75)
			dw_prd_carcass_speed.SetItem(1, "chain_speed1", ll_speed)
	//		ls_temp = dw_prd_carcass_speed.Modify("chain_speed.Expression ='" + String(ll_qty) + "/(" + String(data) + "- .75)'")
		end if
//		dw_prd_carcass_chain.SetItem(1, "hours", data)

End Choose

end event
type dw_header from u_base_dw_ext within w_prd_carcass
integer y = 12
integer width = 1216
integer height = 324
integer taborder = 0
boolean enabled = false
string dataobject = "d_prd_carcass_header"
boolean border = false
end type

event constructor;call super::constructor;this.ib_updateable = False
DataWindowChild		ldwc_type


This.GetChild("product_state", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PRDSTATE")


end event
event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_prd_carcass_dtl from u_base_dw_ext within w_prd_carcass
integer x = 37
integer y = 460
integer width = 1806
integer height = 1112
integer taborder = 20
string dataobject = "d_prd_carcass_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;Long		ll_rtn
String	ls_temp, &
			ls_ColumnName, &
			ls_avail_ind, &
			ls_SearchString, &
			ls_ind, &
			ls_filter
			
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

//If ls_ColumnName = "name_code" Then
//	wf_set_filter('Y')
//End If
	





end event

event constructor;call super::constructor;ib_updateable = True

end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

long			ll_source_row, &
				ll_RowCount, &
				ll_rtn, ll_hours, ll_qty, ll_speed

String		ls_ColumnName, &
				ls_SearchString
	//			ls_avail_ind
 //				ls_name_code								

nvuo_pa_business_rules	u_rule

dw_prd_carcass_dtl.AcceptText()
ll_source_row	= GetRow()
il_ChangedRow = 0

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

//If ls_ColumnName = "name_code" Then
//	ls_name_code = String(data)
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code,'N') 
//	If This.GetItemString(row, "update_flag") = 'A' Then
//		ls_name_code = String(This.GetItemNumber(row, "name_code"))
//		idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code,'Y') 
//	End If
//End If


CHOOSE CASE ls_columnName
	Case 'quantity'
		ll_hours = dw_prd_carcass_chain.getItemNumber(1, "hours")
		ll_qty = dw_prd_carcass_dtl.getItemNumber(1, "qty_compute")
		if ll_hours < 9 and ll_hours > 0 then
			ll_speed = ll_qty / (ll_hours - .25)
			dw_prd_carcass_speed.SetItem(1, "chain_speed1", ll_speed)
		else
			ll_speed = ll_qty / (ll_hours - .75)
			dw_prd_carcass_speed.SetItem(1, "chain_speed1", ll_speed)
		end if
end choose
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event
event itemerror;call super::itemerror;return (1)
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

dw_prd_carcass_dtl.GetChild('state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()

end event

