HA$PBExportHeader$w_new_age_subs_parameters.srw
forward
global type w_new_age_subs_parameters from w_base_sheet_ext
end type
type dw_product_state from u_product_state_netwise within w_new_age_subs_parameters
end type
type dw_division_code from u_division within w_new_age_subs_parameters
end type
type dw_new_age_subst_parms_detail from u_base_dw_ext within w_new_age_subs_parameters
end type
end forward

global type w_new_age_subs_parameters from w_base_sheet_ext
integer x = 41
integer y = 268
integer width = 3323
integer height = 1476
string title = "Age Sub Parameters "
long backcolor = 12632256
event ue_postitemchanged pbm_custom01
dw_product_state dw_product_state
dw_division_code dw_division_code
dw_new_age_subst_parms_detail dw_new_age_subst_parms_detail
end type
global w_new_age_subs_parameters w_new_age_subs_parameters

type variables
Private:

Int		ii_async_commhandle
						
s_error     		istr_error_info
u_pas203          iu_pas203
nvuo_fab_product_code			invuo_fab_product_code			
w_new_age_subs_parameters		iw_parent


string 				is_product, &
						is_state, &
						is_start_date, &
						is_end_date, &
						is_ColName, &
						is_product_info

date        		id_date

Time					it_ChangedTime

Long					il_ChangedRow

String				is_ChangedColumnName, &
						is_inquire_flag

INTEGER				ii_Query_Retry_Count  

u_ws_pas5			iu_ws_pas5

end variables

forward prototypes
public subroutine wf_filenew ()
public function boolean wf_deleterow ()
public subroutine wf_print ()
public function boolean wf_addrow ()
public function boolean wf_retrieve ()
public subroutine wf_delete ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function string wf_buildupdatestring (datawindow adw_tobuildfrom)
end prototypes

event ue_postitemchanged;If il_ChangedRow > 0 And il_ChangedRow <= dw_new_age_subst_parms_detail.RowCount() &
		And Not iw_frame.iu_string.nf_IsEmpty(is_ChangedColumnName) Then
	dw_new_age_subst_parms_detail.SetItem(il_ChangedRow, is_ChangedColumnName, it_ChangedTime)
End if

end event

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_deleterow ();Boolean		lb_ret

dw_new_age_subst_parms_detail.SetFocus()
lb_ret = super::wf_deleterow()

dw_new_age_subst_parms_detail.SelectRow(0,False)
dw_new_age_subst_parms_detail.SelectRow(dw_new_age_subst_parms_detail.GetRow(),True)
return lb_ret

end function

public subroutine wf_print ();dw_new_age_subst_parms_detail.Print()
end subroutine

public function boolean wf_addrow ();Long 				ll_rownum, &
					ll_addrow
dwitemstatus	lis_temp
String			ls_column

This.setredraw(False)
//check to see if there is a row selected
ll_rownum = dw_new_age_subst_parms_detail.GetSelectedRow(0)

ls_column = 'product_code'	

IF ll_rownum = 0 Then // There is no row selected
	ll_rownum = dw_new_age_subst_parms_detail.RowCount()
	// If table is empty
	If ll_rownum = 0 Then
		Super:: wf_AddRow()
	Else
		If dw_new_age_subst_parms_detail.GetItemString(ll_rownum,'product_code') > '   ' Then
			Super:: wf_addrow()
		End If
	End If
	ll_addrow = dw_new_age_subst_parms_detail.GetSelectedRow(0)
// There is a selected row	
Else
	ll_addrow = dw_new_age_subst_parms_detail.insertrow(ll_rownum + 1)
	dw_new_age_subst_parms_detail.SetItem(ll_addrow,"start_date",Today())  // initialize dates
	dw_new_age_subst_parms_detail.SetItem(ll_addrow,"end_date",Date("12/31/2999"))
	dw_new_age_subst_parms_detail.SetRow(ll_addrow)
End if

// Shouldn't need these, in datawindow
//dw_new_age_subst_parms_detail.setitem(ll_addrow, "end_date", date('12/31/2999'))
//dw_new_age_subst_parms_detail.setitem(ll_addrow, "start_date", Today())


dw_new_age_subst_parms_detail.SetColumn (ls_column)
dw_new_age_subst_parms_detail.SetFocus()

This.SetRedraw(True)

Return True	


end function

public function boolean wf_retrieve ();Long				ll_value, &
					ll_count, &
					ll_row
					
Integer			li_ret

Boolean			lb_return

String			ls_input_string, &
					ls_product_state, &
					ls_division_code, &
					ls_output_string

u_string_functions	lu_string


Call w_base_sheet::closequery
IF Message.ReturnValue = 1 THEN Return False

OpenWithParm(w_new_age_subs_parameters_inq, This)
If is_inquire_flag = 'False' Then return False

dw_division_code.uf_ChangeRowStatus(1, NotModified!)

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")


ls_division_code = dw_division_code.uf_Get_Division()
ls_product_state = dw_product_state.uf_Get_product_State()

ls_input_string = ls_division_code + '~t' + ls_product_state + '~r~n'

istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_pasp93cr"
istr_error_info.se_message				= Space(70)

If NOT IsValid( iu_pas203 ) Then
	iu_pas203	=  CREATE u_pas203
End If

This.SetRedraw( False )

dw_new_age_subst_parms_detail.reset()

												 
//li_ret = iu_pas203.nf_pasp93cr_inq_age_sum_param( istr_Error_Info, ls_input_string, ls_output_string)
li_ret = iu_ws_pas5.nf_pasp93gr(istr_Error_Info, ls_input_string, ls_output_string)
 
If li_ret <> 0 Then
	This.SetRedraw(True)
	Return( False )
End If

dw_new_age_subst_parms_detail.importstring(ls_output_string)
li_ret = dw_new_age_subst_parms_detail.SetSort("product_code A, subst_product_code A, start_date A, end_date A")
li_ret = dw_new_age_subst_parms_detail.Sort()
ll_row = dw_new_age_subst_parms_detail.InsertRow(0)
dw_new_age_subst_parms_detail.SetItem(ll_row,"subst_product_code","") // get rid of null value
dw_new_age_subst_parms_detail.SetItem(ll_row,"start_date",Today())  // initialize dates
dw_new_age_subst_parms_detail.SetItem(ll_row,"end_date",Date("12/31/2999"))
For ll_row = 1 to dw_new_age_subst_parms_detail.RowCount() - 1
	dw_new_age_subst_parms_detail.SetItem(ll_row,"update_flag","O")
Next
dw_new_age_subst_parms_detail.ResetUpdate()
dw_new_age_subst_parms_detail.SetFocus()
dw_new_age_subst_parms_detail.ScrollToRow(1)
dw_new_age_subst_parms_detail.SetColumn( "product_code" )
dw_new_age_subst_parms_detail.TriggerEvent("RowFocusChanged")

SetMicroHelp(String(dw_new_age_subst_parms_detail.RowCount() - 1) + " rows retrieved")

This.SetRedraw( True )

SetPointer(Arrow!)



Return( True )

 
end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_validate (long al_row);Date					ldt_start_date, &
						ldt_end_date, &
						ldt_found_start_date, &
						ldt_found_end_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows

String				ls_division_code, &
						ls_fab_product, &
						ls_subst_product, &
						ls_product_state, &
						ls_product_descr, &
						ls_ship_plant, &
						ls_dest_plant, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag


ll_nbrrows = dw_new_age_subst_parms_detail.RowCount()
If ll_nbrrows < 1 Then Return True


// Check that the start date has a value and is > current date and < end date.
ls_update_flag = dw_new_age_subst_parms_detail.GetItemString(al_row, "update_flag")
ldt_temp = dw_new_age_subst_parms_detail.GetItemDate(al_row, "start_date")
IF IsNull(ldt_temp) or ldt_temp <= Date('00/00/0000') Then 
	MessageBox("Start Date", "Please enter a Start Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_new_age_subst_parms_detail.ScrollToRow(al_row)
	dw_new_age_subst_parms_detail.SetColumn("start_date")
	dw_new_age_subst_parms_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ldt_temp < Today() And ls_update_flag = 'A' Then
	MessageBox("Start Date", "Please enter a Start Date greater than Current Date")
	This.SetRedraw(False)
	dw_new_age_subst_parms_detail.ScrollToRow(al_row)
	dw_new_age_subst_parms_detail.SetColumn("start_date")
	dw_new_age_subst_parms_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If ldt_temp > dw_new_age_subst_parms_detail.GetItemDate(al_row, "end_date") And ls_temp = 'A' Then
		MessageBox("Start Date", "Please enter a Start Date less than End Date")
		This.SetRedraw(False)
		dw_new_age_subst_parms_detail.ScrollToRow(al_row)
		dw_new_age_subst_parms_detail.SetColumn("start_date")
		dw_new_age_subst_parms_detail.SetFocus()
		This.SetRedraw(True)
		Return False
	End IF
End If

// Check that the end date has a value and is > current date and start date.
ldt_temp = dw_new_age_subst_parms_detail.GetItemDate(al_row, "end_date")
IF IsNull(ldt_temp) or ldt_temp <= Date('00/00/0000') Then 
	MessageBox("End Date", "Please enter an Effective To Date Greater Than 00/00/0000")
	This.SetRedraw(False)
	dw_new_age_subst_parms_detail.ScrollToRow(al_row)
	dw_new_age_subst_parms_detail.SetColumn("end_date")
	dw_new_age_subst_parms_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ldt_temp < Today() Then
	MessageBox("End Date", "Please enter an Effective To Date Greater Than Current Date")
	This.SetRedraw(False)
	dw_new_age_subst_parms_detail.ScrollToRow(al_row)
	dw_new_age_subst_parms_detail.SetColumn("end_date")
	dw_new_age_subst_parms_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If ldt_temp < dw_new_age_subst_parms_detail.GetItemDate(al_row, "start_date") Then
		MessageBox("End Date", "Please enter an Effective To Date Greater Than From Date")
		This.SetRedraw(False)
		dw_new_age_subst_parms_detail.ScrollToRow(al_row)
		dw_new_age_subst_parms_detail.SetColumn("end_date")
		dw_new_age_subst_parms_detail.SetFocus()
		This.SetRedraw(True)
		Return False
	End IF
End If



IF al_row < 0 THEN Return True

ls_fab_product = dw_new_age_subst_parms_detail.GetItemString(al_row, "product_code")
ls_subst_product = dw_new_age_subst_parms_detail.GetItemString(al_row, "subst_product_code")
ldt_start_date = dw_new_age_subst_parms_detail.GetItemDate &
	(al_row, "start_date")
ldt_end_date = dw_new_age_subst_parms_detail.GetItemDate &
	(al_row, "end_date")


ls_SearchString	= "product_code = '" + ls_fab_product + "' and subst_product_code = '" + &
                     ls_subst_product + "'"
// Find a matching row excluding the current row.

CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_new_age_subst_parms_detail.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_new_age_subst_parms_detail.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_new_age_subst_parms_detail.Find  &
			(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_new_age_subst_parms_detail.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE

If ll_rtn > 0 Then
	ldt_found_start_date = dw_new_age_subst_parms_detail.GetItemDate &
	(ll_rtn, "start_date")
	ldt_found_end_date = dw_new_age_subst_parms_detail.GetItemDate &
	(ll_rtn, "end_date")
	
	If (ldt_found_start_date >= ldt_start_date And ldt_found_start_date <= ldt_end_date) Then
		MessageBox ("Date", "There are duplicate products with overlapping dates.") 
	ElseIf (ldt_found_end_date >= ldt_start_date And ldt_found_end_date <= ldt_end_date) Then
		MessageBox ("Date", "There are duplicate products with overlapping dates.")
	ElseIf (ldt_start_date >= ldt_found_start_date and ldt_start_date <= ldt_found_end_date) Then
		MessageBox ("Date", "There are duplicate products with overlapping dates.") 
	ElseIf(ldt_end_date >= ldt_found_start_date and ldt_end_date <= ldt_found_end_date) Then
		MessageBox ("Date", "There are duplicate products with overlapping dates.") 
	ElseIf (ldt_found_start_date = ldt_start_date) Then
		MessageBox ("Date", "There are duplicate products with overlapping dates.")
	Else
		ll_rtn = 0
	End if
End if
If ll_rtn > 0 Then
		dw_new_age_subst_parms_detail.SetRedraw(False)
		dw_new_age_subst_parms_detail.ScrollToRow(al_row)
		dw_new_age_subst_parms_detail.SetColumn("start_date")
		dw_new_age_subst_parms_detail.SetRow(al_row)
		dw_new_age_subst_parms_detail.SelectRow(ll_rtn, True)
		dw_new_age_subst_parms_detail.SelectRow(al_row, True)
		dw_new_age_subst_parms_detail.SetRedraw(True)
		Return False
End if

Return True
end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter, &
					li_commhandle

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdatePRPT, &
					ls_division_code, &
					ls_product_state, &
					ls_header

dwItemStatus	lis_status

IF dw_new_age_subst_parms_detail.AcceptText() = -1 THEN Return( False )
IF dw_new_age_subst_parms_detail.ModifiedCount() + dw_new_age_subst_parms_detail.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF

ll_NbrRows = dw_new_age_subst_parms_detail.RowCount( ) - 1 // pjm 09/15/2014 last row is blank

ll_row = 0

DO 
	ll_Row = dw_new_age_subst_parms_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN
		If ll_row > ll_NbrRows Then Exit
		IF iw_frame.iu_string.nf_IsEmpty(dw_new_age_subst_parms_detail.GetItemString(ll_row, "product_code")) Then 
			MessageBox("Product Code", "Please choose a Product Code.")
			This.SetRedraw(False)
			dw_new_age_subst_parms_detail.ScrollToRow(ll_row)
			dw_new_age_subst_parms_detail.SetColumn("product_code")
			dw_new_age_subst_parms_detail.SetFocus()
			This.SetRedraw(True)
			Return False
		End If
		If Not wf_validate(ll_row) Then Return False
	END IF
LOOP WHILE ll_Row > 0

ls_division_code = dw_division_code.uf_Get_Division()
ls_product_state = dw_product_state.uf_Get_product_State()

ls_header = ls_product_state  + '~t' + ls_division_code + '~r~n'

// This was changing the formats of the dates
//ls_UpdatePRPT = iw_frame.iu_string.nf_BuildUpdateString(dw_new_age_subst_parms_detail)
ls_UpdatePRPT = wf_BuildUpdateString(dw_new_age_subst_parms_detail)

//If not iu_pas203.pasp94cr_upd_age_sub_param(istr_error_info, ls_header, ls_UpdatePRPT, li_commhandle) Then  Return False
iu_ws_pas5.nf_pasp94gr(istr_error_info, ls_header, ls_UpdatePRPT)
dw_new_age_subst_parms_detail.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_new_age_subst_parms_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_new_age_subst_parms_detail.SetItem(li_Counter, 'update_flag', 'O')
Next
dw_new_age_subst_parms_detail.ResetUpdate()
//dw_new_age_subst_parms_detail.Sort()
//dw_new_age_subst_parms_detail.GroupCalc()
dw_new_age_subst_parms_detail.SetFocus()
dw_new_age_subst_parms_detail.SetReDraw(True)
is_start_date   = ""
is_end_date   = ""
is_product = ""

Return( True )
end function

public function string wf_buildupdatestring (datawindow adw_tobuildfrom);///////////////////////////////////////////////////////////////////////////////////////
//
// Modified Date		Author			Description
//
// 7/18/96				Tim Bornholtz is an idiot.
// 04/16/2002			Elwin McKernan	PB8 Migration. 
//							SetRedraw(True) when RowCount = 0 causes Memory Violation in w_pas_source
///////////////////////////////////////////////////////////////////////////////////////
Int		li_counter, &
			lia_SelectedRows[], &
			li_SelectedUpperBound,&
			li_MaskCounter

Long		ll_RowCount, &
			ll_ColumnCount, &
			ll_DeletedCount

String 	ls_update_flag, &
			ls_data, &
			ls_OldMask[]

Boolean  lb_ResetRedraw

 

ll_RowCount = adw_tobuildfrom.RowCount()

//** IBDKEEM ** 04/16/2002 ** PB8 Migration 
IF ll_RowCount > 0 THEN
	adw_tobuildfrom.SetRedraw(False)
	lb_ResetRedraw = true
else
	lb_ResetRedraw = false
END IF

For li_Counter = 1 to ll_RowCount
	If adw_ToBuildFrom.IsSelected(li_Counter) Then
		li_SelectedUpperBound ++
		lia_SelectedRows[li_SelectedUpperBound] = li_counter
	End if
Next

For li_counter = 1 to ll_RowCount
	ls_update_flag = adw_ToBuildFrom.GetItemString(li_counter, 'update_flag')
	If ls_update_flag = 'A' or ls_update_flag = 'U' Then
		adw_ToBuildFrom.RowsCopy(li_counter, li_counter, Primary!, adw_ToBuildFrom, 10000, Filter!)
	Else
		adw_ToBuildFrom.RowsMove(li_counter, li_counter, Primary!, adw_ToBuildFrom, 10000, Filter!)
		ll_RowCount --
		li_counter --
	End if
Next

ll_DeletedCount = adw_ToBuildFrom.DeletedCount()
If ll_DeletedCount > 0 Then
	adw_ToBuildFrom.RowsMove( 1, ll_DeletedCount, Delete!, adw_ToBuildFrom, 10000, Primary!)
	// Set update flag to 'D'
	// ll_RowCount is set to the OLD RowCount
	For li_Counter = ll_RowCount + 1 to ll_RowCount + ll_DeletedCount
		adw_ToBuildFrom.SetItem(li_Counter, 'update_flag', 'D')
	Next
End if

ll_ColumnCount = Long(adw_ToBuildFrom.Describe("DataWindow.Column.Count"))

ls_data = adw_ToBuildFrom.Describe("DataWindow.Data")
li_MaskCounter = 0

adw_ToBuildFrom.RowsDiscard(1, adw_ToBuildFrom.RowCount(), Primary!)
adw_ToBuildFrom.RowsMove(1, adw_ToBuildFrom.FilteredCount(), Filter!, adw_ToBuildFrom, 1, Primary!)

adw_ToBuildFrom.SelectRow(0, False)
For li_Counter = 1 to li_SelectedUpperBound
	adw_ToBuildFrom.SelectRow(lia_SelectedRows[li_counter], True)
Next

//** IBDKEEM ** 04/16/2002 ** PB8 Migration 
IF lb_ResetRedraw THEN
	adw_tobuildfrom.SetFocus()
	adw_tobuildfrom.SetRedraw(True)
END IF

return ls_data
end function

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

Destroy iu_ws_pas5




end event

event ue_postopen;call super::ue_postopen;iu_ws_pas5 = Create u_ws_pas5
This.PostEvent("ue_query")




end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_Disable('m_graph')

end on

event ue_query;call super::ue_query;istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "New Age Subst Parms"
istr_error_info.se_user_id 		= sqlca.userid

wf_retrieve()
end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_new_age_subst_parms_detail.x * 2) + 30 
li_y = dw_new_age_subst_parms_detail.y + 135


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If



if width > li_x Then
	dw_new_age_subst_parms_detail.width	= width - li_x
end if

if height > li_y then
	dw_new_age_subst_parms_detail.height	= height - li_y
end if
end event

on w_new_age_subs_parameters.create
int iCurrent
call super::create
this.dw_product_state=create dw_product_state
this.dw_division_code=create dw_division_code
this.dw_new_age_subst_parms_detail=create dw_new_age_subst_parms_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_state
this.Control[iCurrent+2]=this.dw_division_code
this.Control[iCurrent+3]=this.dw_new_age_subst_parms_detail
end on

on w_new_age_subs_parameters.destroy
call super::destroy
destroy(this.dw_product_state)
destroy(this.dw_division_code)
destroy(this.dw_new_age_subst_parms_detail)
end on

event activate;call super::activate;//iw_frame.im_menu.mf_Enable('m_graph')

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'division'
		Message.StringParm = dw_division_code.uf_get_division()
	Case 'product_state'
		Message.StringParm = dw_product_state.uf_get_product_state()
	Case 'title'
		Message.StringParm = this.title
End Choose

	
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'division'
		dw_division_code.uf_set_division(as_value)
	case 'product_state'
		dw_product_state.uf_set_product_state(as_value)
	case 'ib_inquire'
		is_inquire_flag = as_value
End Choose

end event

type dw_product_state from u_product_state_netwise within w_new_age_subs_parameters
integer x = 27
integer y = 60
integer taborder = 30
boolean enabled = false
end type

type dw_division_code from u_division within w_new_age_subs_parameters
integer x = 1518
integer y = 56
integer taborder = 20
boolean enabled = false
end type

type dw_new_age_subst_parms_detail from u_base_dw_ext within w_new_age_subs_parameters
event test pbm_dwntabout
event ue_set_defaults ( long al_next_row )
integer x = 123
integer y = 260
integer width = 2990
integer height = 1028
integer taborder = 30
string dataobject = "d_new_age_subst_parms_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_set_defaults(long al_next_row);this.SetItem(al_next_row, "product_code",this.GetItemString(al_next_row - 1,"product_code") )
this.SetItem(al_next_row, "subst_product_code",this.GetItemString(al_next_row - 1,"subst_product_code") )


end event

event itemchanged;call super::itemchanged;int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_prpt_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row, &
				ll_next_row

string		ls_GetText, &
				ls_temp, &
				ls_product, &
				ls_product_info 
Date			ldt_temp	

dwItemStatus	le_RowStatus
nvuo_pa_business_rules	u_rule
 
is_ColName    = GetColumnName()
ls_GetText    = data
il_ChangedRow = 0
ll_prpt_row = row

If row = This.RowCount() Then
	If is_ColName = "subst_product_code" Then
		ll_next_row = This.InsertRow(0)
		This.SetItem(ll_next_row,"start_date",Today())  // initialize dates
		This.SetItem(ll_next_row,"end_date",Date("12/31/2999"))
//		this.Post Event ue_set_defaults(ll_next_row)
	End If
End if

CHOOSE CASE is_ColName

		
	CASE "product_code"
		If not invuo_fab_product_code.uf_check_product(data) then
			If	invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
				Return 1
			Else
				iw_frame.SetMicroHelp(data + " is an invalid Product Code")
				Return 1
			End If
		End If
 
			// We know we have a valid product -- is it SKU or fab?
		
		ls_product_info =	invuo_fab_product_code.uf_get_product_data( )

		ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')	  // flag
		If ls_temp = 'F' Then
			iw_frame.SetMicroHelp(data + " is not a SKU Product Code")
			Return 1
		End If							
 
	CASE "days_old_from"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a number")
			This.selecttext(1,100)
			return 1
		End If
				
		li_temp = This.GetItemNumber( row, 'days_old_to')
		
		If integer(ls_gettext) >= li_temp Then
			iw_frame.SetMicroHelp("Days Old From must be less than Days Old To")
			This.selecttext(1,100)
			Return 1
		End If
	
	CASE "days_old_to"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a number")
			This.selecttext(1,100)
			return 1
		End If
				
		li_temp = This.GetItemNumber( row, 'days_old_from')
		
		If integer(ls_gettext) <= li_temp Then
			iw_frame.SetMicroHelp("Days Old From must be less than Days Old To")
			This.selecttext(1,100)
			Return 1
		End If
		
	CASE "subst_product_code"
		If not invuo_fab_product_code.uf_check_product(data) then
			If	invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
				Return 1
			Else
				iw_frame.SetMicroHelp(data + " is an invalid Product Code")
				Return 1
			End If
		End If
 
			// We know we have a valid product -- is it SKU or fab?
		
		ls_product_info =	invuo_fab_product_code.uf_get_product_data( )

		ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')	  // flag
		If ls_temp = 'F' Then
			iw_frame.SetMicroHelp(data + " is not a SKU Product Code")
			Return 1
		End If	
		
		CASE "sub_days_old_from"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a number")
			This.selecttext(1,100)
			return 1
		End If
				
		li_temp = This.GetItemNumber( row, 'sub_days_old_to')
		
		If integer(ls_gettext) >= li_temp Then
			iw_frame.SetMicroHelp("Sub Days Old From must be less than Sub Days Old To")
			This.selecttext(1,100)
			Return 1
		End If
	
	CASE "sub_days_old_to"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a number")
			This.selecttext(1,100)
			return 1
		End If
				
		li_temp = This.GetItemNumber( row, 'sub_days_old_from')
		
		If integer(ls_gettext) <= li_temp Then
			iw_frame.SetMicroHelp("Sub Days Old From must be less than Sub Days Old To")
			This.selecttext(1,100)
			Return 1
		End If
		
	CASE "start_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
				
		ldt_temp = This.GetItemDate( row, 'end_date')
		
		If date(ls_gettext) > ldt_temp Then
			iw_frame.SetMicroHelp("End Date cannot be less than the Start Date")
			This.selecttext(1,100)
			Return 1
		End If
		
	CASE "end_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
		
		ls_temp = ' '
		ldt_temp = This.GetItemDate( row, 'start_date')
		If date(ls_gettext) < ldt_temp Then
			iw_frame.SetMicroHelp("End Date cannot be less than the Start Date")
			This.selecttext(1,100)
			Return 1
		End If
END CHOOSE

	
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_prpt_row > 0 AND ll_prpt_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		String ls_update_flag
		ls_update_flag = This.GetItemString(ll_PRPT_row, "update_flag")
		If IsNull(ls_update_flag) Then ls_update_flag = " "
//			le_RowStatus = This.GetItemStatus(ll_PRPT_row, 0, Primary!)
//			CHOOSE CASE le_RowStatus
//			CASE NewModified!, New!
//				This.SetItem(ll_PRPT_row, "update_flag", "A")
//			CASE DataModified!, NotModified!
//				This.SetItem(ll_PRPT_row, "update_flag", "U")
//			END CHOOSE
		If ls_update_flag = 'O' Then
			This.SetItem(ll_PRPT_row, "update_flag", "U")
		ElseIf ls_update_flag <> 'U' Then
			This.SetItem(ll_PRPT_row, "update_flag", "A")
		End If
	END IF
END IF

parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;String	ls_column_name, &
			ls_GetText


ls_GetText = This.GetText()

Choose Case This.GetColumnName()
	
Case "product_code","subst_product_code", "days_old_from", "days_old_to", &
		"sub_days_old_from", "sub_days_old_to", "start_date", "end_date"  
	This.SelectText(1, Len(ls_gettext))
	return 1
End Choose
end event

event ue_graph;//w_graph		w_grp_child
//Window			lw_response

//Message.PowerObjectParm = This
//Message.StringParm = 'd_source_weekly_graph'
//OpenWithParm(lw_response ,  This, 'w_source_graph_select')
end event

on constructor;call u_base_dw_ext::constructor;is_selection = '1'
//ib_storedatawindowsyntax = TRUE


end on

event getfocus;call super::getfocus;If This.rowcount() > 0 then 
	This.SelectText(1, Len(GetText()))
end if
end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

event rowfocuschanged;call super::rowfocuschanged;iw_frame.SetMicroHelp("row focus changed")
end event

event ue_insertrow;call super::ue_insertrow;iw_frame.SetMicroHelp("row inserted")
end event

