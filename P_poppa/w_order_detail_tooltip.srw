HA$PBExportHeader$w_order_detail_tooltip.srw
forward
global type w_order_detail_tooltip from window
end type
type st_3 from statictext within w_order_detail_tooltip
end type
type st_2 from statictext within w_order_detail_tooltip
end type
type st_1 from statictext within w_order_detail_tooltip
end type
end forward

global type w_order_detail_tooltip from window
integer x = 832
integer y = 360
integer width = 379
integer height = 296
windowtype windowtype = child!
long backcolor = 15793151
st_3 st_3
st_2 st_2
st_1 st_1
end type
global w_order_detail_tooltip w_order_detail_tooltip

type variables
Window			iw_parent

String			is_data

Window	iw_ActiveSheet


end variables

forward prototypes
public function boolean wf_powerobjectparm ()
public function boolean wf_stringparm ()
end prototypes

public function boolean wf_powerobjectparm ();
Return True
end function

public function boolean wf_stringparm ();Return True
end function

event open;Long						ll_NewX, &
							ll_NewY, &
							ll_width
							
String					ls_data, &
							ls_xy_values


iw_parent = message.powerobjectparm

iw_ActiveSheet = iw_frame.GetActiveSheet()

iw_parent.Event Dynamic ue_get_data('data')
ls_data = RightTrim(message.stringparm)

iw_parent.Event Dynamic ue_get_data('xy_values')
ls_xy_values = message.stringparm

ll_NewX = Long(iw_frame.iu_string.nf_GetToken(ls_xy_values, '~t'))
ll_NewY = Long(iw_frame.iu_string.nf_GetToken(ls_xy_values, '~t'))

//This.Visible = False

Choose Case iw_frame.iu_string.nf_countOccurrences(ls_data, '~r~n') + 1
	Case 1
		st_1.Text = iw_frame.iu_string.nf_GetToken(ls_data, '~r~n')
		st_1.Width = Len(st_1.Text) * 40
		This.Resize(st_1.Width, st_1.Height)
	Case 2
		st_1.Text = iw_frame.iu_string.nf_GetToken(ls_data, '~r~n')
		st_2.Text = iw_frame.iu_string.nf_GetToken(ls_data, '~r~n')
		st_1.width = Len(st_1.Text) * 40
		st_2.width = Len(st_2.Text) * 40
		ll_width = Max(Long(st_1.width), Long(st_2.width))

		This.Resize(ll_width, st_2.Y + st_2.Height)
	Case 3
		st_1.Text = iw_frame.iu_string.nf_GetToken(ls_data, '~r~n')
		st_2.Text = iw_frame.iu_string.nf_GetToken(ls_data, '~r~n')
		st_3.Text = iw_frame.iu_string.nf_GetToken(ls_data, '~r~n')
		st_1.width = Len(st_1.Text) * 40
		st_2.width = Len(st_2.Text) * 40
		st_3.width = Len(st_3.Text) * 40
		ll_width = Max(Long(st_1.width), Long(st_2.width))
		ll_width = Max(ll_width, Long(st_3.Width))
		This.Resize(ll_width, st_3.Y + st_3.Height)
End Choose

If ll_NewX + This.Width > iw_Frame.Width Then
	ll_NewX = iw_Frame.Width - This.Width - 5
End if
If ll_NewY + This.Height > iw_Frame.Height Then
	ll_NewY = iw_Frame.Height - This.Height - 100
End if

This.Move(ll_NewX, ll_NewY)

//dmk prevent focus back to this window when another has focus
IF IsValid(iw_parent) Then
	if iw_parent = iw_ActiveSheet Then
		iw_parent.setfocus()
	end if
end if
//





end event

on w_order_detail_tooltip.create
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.st_3,&
this.st_2,&
this.st_1}
end on

on w_order_detail_tooltip.destroy
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
end on

event timer;This.Visible = True
Timer(0)
end event

type st_3 from statictext within w_order_detail_tooltip
integer x = 5
integer y = 112
integer width = 229
integer height = 64
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 15793151
boolean enabled = false
string text = "100.00 %"
boolean focusrectangle = false
end type

type st_2 from statictext within w_order_detail_tooltip
integer x = 5
integer y = 56
integer width = 229
integer height = 64
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 15793151
boolean enabled = false
string text = "100.00 %"
boolean focusrectangle = false
end type

type st_1 from statictext within w_order_detail_tooltip
integer x = 5
integer y = 4
integer width = 229
integer height = 64
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 15793151
boolean enabled = false
string text = "100.00 %"
boolean focusrectangle = false
end type

