HA$PBExportHeader$w_mcool_sub_overage.srw
$PBExportComments$MCOOL sub and overage window inquire
forward
global type w_mcool_sub_overage from w_base_sheet_ext
end type
type uo_products from u_group_list within w_mcool_sub_overage
end type
type uo_locations from u_group_list within w_mcool_sub_overage
end type
type st_or3 from statictext within w_mcool_sub_overage
end type
type st_or2 from statictext within w_mcool_sub_overage
end type
type cb_submit from u_base_commandbutton_ext within w_mcool_sub_overage
end type
type dw_end_date from u_base_dw_ext within w_mcool_sub_overage
end type
type dw_start_date from u_base_dw_ext within w_mcool_sub_overage
end type
type dw_division from u_division within w_mcool_sub_overage
end type
type dw_plant from u_plant within w_mcool_sub_overage
end type
type st_or from statictext within w_mcool_sub_overage
end type
type st_location_group from statictext within w_mcool_sub_overage
end type
type cbx_plant from u_base_checkbox_ext within w_mcool_sub_overage
end type
type cbx_location_group from u_base_checkbox_ext within w_mcool_sub_overage
end type
type cbx_mcool from u_base_checkbox_ext within w_mcool_sub_overage
end type
type st_product_group from statictext within w_mcool_sub_overage
end type
type cbx_product from u_base_checkbox_ext within w_mcool_sub_overage
end type
type cbx_product_group from u_base_checkbox_ext within w_mcool_sub_overage
end type
type dw_product from u_fab_product_code within w_mcool_sub_overage
end type
type dw_mcool from u_mcool_code_dropdown within w_mcool_sub_overage
end type
end forward

global type w_mcool_sub_overage from w_base_sheet_ext
integer x = 0
integer y = 0
integer width = 2144
integer height = 2388
long backcolor = 67108864
uo_products uo_products
uo_locations uo_locations
st_or3 st_or3
st_or2 st_or2
cb_submit cb_submit
dw_end_date dw_end_date
dw_start_date dw_start_date
dw_division dw_division
dw_plant dw_plant
st_or st_or
st_location_group st_location_group
cbx_plant cbx_plant
cbx_location_group cbx_location_group
cbx_mcool cbx_mcool
st_product_group st_product_group
cbx_product cbx_product
cbx_product_group cbx_product_group
dw_product dw_product
dw_mcool dw_mcool
end type
global w_mcool_sub_overage w_mcool_sub_overage

type variables
Boolean			ib_ok_to_close

w_age_avail		iw_parentwindow

String			is_option

u_pas203		iu_pas203

s_error		istr_error_info
u_ws_pas5		iu_ws_pas5
end variables

on w_mcool_sub_overage.create
int iCurrent
call super::create
this.uo_products=create uo_products
this.uo_locations=create uo_locations
this.st_or3=create st_or3
this.st_or2=create st_or2
this.cb_submit=create cb_submit
this.dw_end_date=create dw_end_date
this.dw_start_date=create dw_start_date
this.dw_division=create dw_division
this.dw_plant=create dw_plant
this.st_or=create st_or
this.st_location_group=create st_location_group
this.cbx_plant=create cbx_plant
this.cbx_location_group=create cbx_location_group
this.cbx_mcool=create cbx_mcool
this.st_product_group=create st_product_group
this.cbx_product=create cbx_product
this.cbx_product_group=create cbx_product_group
this.dw_product=create dw_product
this.dw_mcool=create dw_mcool
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_products
this.Control[iCurrent+2]=this.uo_locations
this.Control[iCurrent+3]=this.st_or3
this.Control[iCurrent+4]=this.st_or2
this.Control[iCurrent+5]=this.cb_submit
this.Control[iCurrent+6]=this.dw_end_date
this.Control[iCurrent+7]=this.dw_start_date
this.Control[iCurrent+8]=this.dw_division
this.Control[iCurrent+9]=this.dw_plant
this.Control[iCurrent+10]=this.st_or
this.Control[iCurrent+11]=this.st_location_group
this.Control[iCurrent+12]=this.cbx_plant
this.Control[iCurrent+13]=this.cbx_location_group
this.Control[iCurrent+14]=this.cbx_mcool
this.Control[iCurrent+15]=this.st_product_group
this.Control[iCurrent+16]=this.cbx_product
this.Control[iCurrent+17]=this.cbx_product_group
this.Control[iCurrent+18]=this.dw_product
this.Control[iCurrent+19]=this.dw_mcool
end on

on w_mcool_sub_overage.destroy
call super::destroy
destroy(this.uo_products)
destroy(this.uo_locations)
destroy(this.st_or3)
destroy(this.st_or2)
destroy(this.cb_submit)
destroy(this.dw_end_date)
destroy(this.dw_start_date)
destroy(this.dw_division)
destroy(this.dw_plant)
destroy(this.st_or)
destroy(this.st_location_group)
destroy(this.cbx_plant)
destroy(this.cbx_location_group)
destroy(this.cbx_mcool)
destroy(this.st_product_group)
destroy(this.cbx_product)
destroy(this.cbx_product_group)
destroy(this.dw_product)
destroy(this.dw_mcool)
end on

event close;String			ls_setvalue


If ib_ok_to_close Then
	ls_setvalue = 'True'
Else
	ls_setvalue = 'False'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('ib_inquire', ls_setvalue)
End IF

Destroy iu_pas203
Destroy iu_ws_pas5
end event

event open;call super::open;is_option = Message.StringParm

CHOOSE CASE is_option
		
	CASE "Sub"
		This.Title = "Automated MCOOL Sub"
		cb_submit.Text = "Substitute Shortages"
		
	CASE "Overage"
		This.Title = "Automated MCOOL Overage Allocation"
		cb_submit.Text = "Allocate Overages"
		
End Choose

end event

event ue_postopen;
string	ls_prod, &
			ls_checked_boxes, &
			ls_prod_desc
		
iu_ws_pas5 = Create u_ws_pas5		
iu_pas203 = create u_pas203		

dw_product.uf_enable(true)
dw_product.uf_enable_state(false)
dw_product.uf_enable_status(false)

If dw_product.RowCount() < 1 Then
	dw_product.InsertRow(0)
End If

//ole_location_group.object.GroupType(1)
//ole_location_group.object.LoadObject()
uo_locations.uf_load_groups('L')

//ole_product_group.object.GroupType(3)
//ole_product_group.object.LoadObject()
uo_products.uf_load_groups('P')

cbx_plant.Checked = True
cbx_mcool.Checked = True

dw_start_date.SetItem(1, "mcool_start_date", Today())
dw_end_date.SetItem(1, "mcool_end_date", Today())

dw_division.uf_set_division("11")





end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_inquire')
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_save')
end event

type uo_products from u_group_list within w_mcool_sub_overage
integer x = 581
integer y = 1228
integer taborder = 110
end type

on uo_products.destroy
call u_group_list::destroy
end on

type uo_locations from u_group_list within w_mcool_sub_overage
integer x = 599
integer y = 144
integer taborder = 40
end type

on uo_locations.destroy
call u_group_list::destroy
end on

type st_or3 from statictext within w_mcool_sub_overage
integer x = 219
integer y = 1120
integer width = 87
integer height = 52
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "or"
boolean focusrectangle = false
end type

type st_or2 from statictext within w_mcool_sub_overage
integer x = 219
integer y = 960
integer width = 87
integer height = 52
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "or"
boolean focusrectangle = false
end type

type cb_submit from u_base_commandbutton_ext within w_mcool_sub_overage
integer x = 622
integer y = 2080
integer width = 654
integer height = 108
integer taborder = 140
string text = "Substitute Shortages"
end type

event clicked;call super::clicked;String	ls_build_string, &
			ls_option, &
			ls_plant, &
			ls_location_group_system, &
			ls_location_group_groupid, &
			ls_mcool_code, &
			ls_division_code, &
			ls_product_code, &
			ls_product_group_system, &
			ls_product_group_groupid, &
			ls_start_date, &
			ls_end_date
			
Long		ll_ret			
		
dw_plant.AcceptText()
dw_mcool.AcceptText()
dw_division.AcceptText()
dw_product.AcceptText()
dw_end_date.AcceptText()
dw_start_date.AcceptText()

If cbx_plant.Checked = False and cbx_location_group.Checked = False Then
	MessageBox ("Error", "Either Plant or Location group must be selected")
	Return
End If

If cbx_mcool.Checked = False and cbx_product.Checked = False and cbx_product_group.Checked = False Then
	MessageBox ("Error", "Either MCOOL/Division, Product, or Product Group must be selected")
	Return
End If

If dw_start_date.GetItemDate(1, "mcool_start_date") > dw_end_date.GetItemDate(1, "mcool_end_date") Then
	MessageBox ("Error", "Start date cannot be greater than end date")
	Return
End If


If is_option = 'Sub' Then
	ls_option = 'S'
Else
	ls_option = 'O'
End If

If cbx_plant.Checked = True Then
	ls_plant = dw_plant.GetItemString(1, "location_code")
	ls_location_group_system = '     '
	ls_location_group_groupid = '    ' 
Else
	ls_plant = '   '
//	ls_location_group_system = string(ole_location_group.object.systemname())
//	ls_location_group_groupid = string(ole_location_group.object.groupID())	
	ls_location_group_system = uo_locations.uf_get_owner()
	ls_location_group_groupid = string(uo_locations.uf_get_sel_id(ls_location_group_groupid))
	
End If

If cbx_mcool.Checked = True Then
	ls_mcool_code = dw_mcool.GetItemString(1, "mcool_code")
	ls_division_code = dw_division.GetItemSTring(1, "division_code")
	ls_product_code = '          '
	ls_product_group_system = '     '
	ls_product_group_groupid = '    '
Else
	If cbx_product.Checked = True Then
		ls_mcool_code = ' '
		ls_division_code = '  '
		ls_product_code = dw_product.GetItemSTring(1, "fab_product_code")
		ls_product_group_system = '     '
		ls_product_group_groupid = '    '	
	Else
		ls_mcool_code = ' '
		ls_division_code = '  '
		ls_product_code = '          '
//		ls_product_group_system = string(ole_product_group.object.systemname())
//		ls_product_group_groupid = string(ole_product_group.object.groupID())	
		ls_product_group_system = uo_products.uf_get_owner()
		ls_product_group_groupid = string(uo_products.uf_get_sel_id(ls_product_group_groupid))
	End If
End If
		
ls_start_date = String(dw_start_date.GetItemDate(1, "mcool_start_date"), 'yyyy/mm/dd')
ls_end_date = String(dw_end_date.GetItemDate(1, "mcool_end_date"), 'yyyy/mm/dd')

ls_build_string  = ls_option + '~t'
ls_build_string += ls_plant + '~t' 
ls_build_string += ls_location_group_system + '~t'
ls_build_string += ls_location_group_groupid + '~t' 
ls_build_string += ls_mcool_code + '~t' 
ls_build_string += ls_division_code + '~t' 
ls_build_string += ls_product_code + '~t' 
ls_build_string += ls_product_group_system + '~t' 
ls_build_string += ls_product_group_groupid + '~t' 
ls_build_string += ls_start_date + '~t'
ls_build_string += ls_end_date + '~t'

//MessageBox ("ls_build_string", "ls_build_string = " + ls_build_string)

//ll_ret = iu_pas203.nf_pasp84cr_mcool_sub_overage(istr_error_info, &
//									ls_build_string) 

ll_ret = iu_ws_pas5.nf_pasp84gr(istr_error_info, ls_build_string)



end event

type dw_end_date from u_base_dw_ext within w_mcool_sub_overage
integer x = 914
integer y = 1856
integer width = 549
integer height = 96
integer taborder = 130
string dataobject = "d_mcool_end_date"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_start_date from u_base_dw_ext within w_mcool_sub_overage
integer x = 366
integer y = 1856
integer width = 549
integer height = 96
integer taborder = 120
string dataobject = "d_mcool_start_date"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_division from u_division within w_mcool_sub_overage
integer x = 375
integer y = 864
integer taborder = 70
boolean maxbox = true
end type

type dw_plant from u_plant within w_mcool_sub_overage
integer x = 169
integer y = 32
integer height = 92
integer taborder = 20
boolean bringtotop = true
end type

type st_or from statictext within w_mcool_sub_overage
integer x = 238
integer y = 108
integer width = 78
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "or"
boolean focusrectangle = false
end type

type st_location_group from statictext within w_mcool_sub_overage
integer x = 201
integer y = 176
integer width = 357
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Location Group"
boolean focusrectangle = false
end type

type cbx_plant from u_base_checkbox_ext within w_mcool_sub_overage
integer x = 64
integer y = 40
integer width = 82
integer taborder = 10
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;If cbx_plant.Checked = TRUE Then
	cbx_location_group.Checked = False
End If
	
end event

type cbx_location_group from u_base_checkbox_ext within w_mcool_sub_overage
integer x = 64
integer y = 172
integer width = 91
integer taborder = 30
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;If cbx_location_group.Checked = TRUE Then
	cbx_plant.Checked = False
End If
end event

type cbx_mcool from u_base_checkbox_ext within w_mcool_sub_overage
integer x = 73
integer y = 768
integer width = 91
integer taborder = 50
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;if cbx_mcool.Checked = True Then
	cbx_product.Checked = False
	cbx_product_group.Checked = False
End if
end event

type st_product_group from statictext within w_mcool_sub_overage
integer x = 210
integer y = 1212
integer width = 343
integer height = 52
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Product Group"
boolean focusrectangle = false
end type

type cbx_product from u_base_checkbox_ext within w_mcool_sub_overage
integer x = 64
integer y = 1032
integer width = 82
integer taborder = 80
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;If cbx_product.Checked = True Then
	cbx_mcool.checked = False
	cbx_product_group.Checked = False
End If
end event

type cbx_product_group from u_base_checkbox_ext within w_mcool_sub_overage
integer x = 64
integer y = 1196
integer width = 87
integer taborder = 100
boolean bringtotop = true
string text = ""
end type

event clicked;call super::clicked;If cbx_product_group.Checked = True Then
	cbx_mcool.Checked = False
	cbx_product.Checked = False
End If
end event

type dw_product from u_fab_product_code within w_mcool_sub_overage
integer x = 219
integer y = 1024
integer width = 1682
integer height = 96
integer taborder = 90
boolean bringtotop = true
end type

type dw_mcool from u_mcool_code_dropdown within w_mcool_sub_overage
integer x = 219
integer y = 768
integer width = 2048
integer height = 64
integer taborder = 60
boolean bringtotop = true
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
08w_mcool_sub_overage.bin 
2100000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000825a430001d2cb6300000003000001000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000825a430001d2cb63825a430001d2cb6300000000000000000000000000000001fffffffe00000003fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
2Cffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000200000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
24ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000825a430001d2cb6300000003000001000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000825a430001d2cb63825a430001d2cb6300000000000000000000000000000001fffffffe00000003fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000200000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000
2D0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
18w_mcool_sub_overage.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
