HA$PBExportHeader$w_new_age_subs_parameters_inq.srw
forward
global type w_new_age_subs_parameters_inq from w_base_response_ext
end type
type dw_division from u_division within w_new_age_subs_parameters_inq
end type
type dw_product_state from u_product_state within w_new_age_subs_parameters_inq
end type
end forward

global type w_new_age_subs_parameters_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer height = 832
string title = ""
long backcolor = 12632256
dw_division dw_division
dw_product_state dw_product_state
end type
global w_new_age_subs_parameters_inq w_new_age_subs_parameters_inq

on w_new_age_subs_parameters_inq.create
int iCurrent
call super::create
this.dw_division=create dw_division
this.dw_product_state=create dw_product_state
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
this.Control[iCurrent+2]=this.dw_product_state
end on

on w_new_age_subs_parameters_inq.destroy
call super::destroy
destroy(this.dw_division)
destroy(this.dw_product_state)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(this)
end event

event ue_base_ok;string ls_product_state, ls_division


ls_product_state = dw_product_state.uf_get_product_state()	 
If iw_frame.iu_string.nf_IsEmpty(ls_product_state) Then
	iw_frame.SetMicroHelp("Product State is a required field")
	dw_product_state.SetFocus() 
	return
End If

ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End If


If dw_product_state.AcceptText() = -1 Then return   
If dw_division.AcceptText() = -1 Then return

iw_parentwindow.Event ue_set_data('product_state',ls_product_state)
iw_parentwindow.Event ue_set_data('division',ls_division)

ib_ok_to_close = True

Close(This)
 
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('division')
dw_division.uf_set_division(Message.StringParm)
iw_parentwindow.Event ue_get_data('product_state')
// pjm 09/21/2014 - default to "Fresh"
If Len(Trim(message.StringParm)) > 0 Then
	dw_product_state.uf_set_product_state(Message.StringParm)
Else
 	dw_product_state.uf_set_product_state('1')
End If
iw_parentwindow.Event ue_set_data('close', "true")
 
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_new_age_subs_parameters_inq
boolean visible = false
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_new_age_subs_parameters_inq
integer x = 1376
integer y = 576
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_new_age_subs_parameters_inq
integer x = 891
integer y = 576
end type

type dw_division from u_division within w_new_age_subs_parameters_inq
integer x = 526
integer y = 220
integer taborder = 10
boolean bringtotop = true
end type

type dw_product_state from u_product_state within w_new_age_subs_parameters_inq
integer x = 398
integer y = 88
integer height = 68
boolean bringtotop = true
end type

