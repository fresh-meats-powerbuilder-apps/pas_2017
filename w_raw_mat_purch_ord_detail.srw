$PBExportHeader$w_raw_mat_purch_ord_detail.srw
forward
global type w_raw_mat_purch_ord_detail from w_base_sheet_ext
end type
type dw_rmt_header from u_base_dw_ext within w_raw_mat_purch_ord_detail
end type
type dw_rmt_detail from u_base_dw_ext within w_raw_mat_purch_ord_detail
end type
end forward

global type w_raw_mat_purch_ord_detail from w_base_sheet_ext
integer width = 3072
integer height = 1656
string title = "Raw Material Purchase Order Detail"
long backcolor = 67108864
dw_rmt_header dw_rmt_header
dw_rmt_detail dw_rmt_detail
end type
global w_raw_mat_purch_ord_detail w_raw_mat_purch_ord_detail

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables


s_error		istr_error_info

u_pas203		iu_pas203

nvuo_fab_product_code invuo_fab_product_code

end variables

forward prototypes
public function boolean wf_deleterow ()
public subroutine wf_new ()
public function boolean wf_addrow ()
public subroutine wf_delete ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_deleterow ();Boolean		lb_ret

dw_rmt_detail.SetFocus()
lb_ret = super::wf_deleterow()

dw_rmt_detail.SelectRow(0,False)
dw_rmt_detail.SelectRow(dw_rmt_detail.GetRow(),True)
return lb_ret
end function

public subroutine wf_new ();

dw_rmt_header.Reset()
dw_rmt_detail.Reset()

dw_rmt_header.InsertRow(0)
dw_rmt_detail.InsertRow(0)

dw_rmt_header.SetItem(1, "delivery_date", date("01/01/0001"))
dw_rmt_header.SetItem(1, "receive_date", date("01/01/0001"))

dw_rmt_header.SetItem(1, "action_ind", "A")
dw_rmt_detail.SetItem(1, "update_flag", "A")



end subroutine

public function boolean wf_addrow ();Long			ll_Old_Row, &
				ll_New_row

If dw_rmt_detail.accepttext( ) <> 1 then return false

This.SetRedraw(False)

// set old row equal to current row 
ll_Old_Row = dw_rmt_detail.GetRow()

// if current row is last row, insert row at end  
If ll_Old_Row = dw_rmt_detail.RowCount() Then
	ll_New_Row = dw_rmt_detail.InsertRow(0)	
Else
// insert new row after currrent row
	ll_New_Row = dw_rmt_detail.InsertRow(ll_Old_Row + 1)
End If

//dw_rmt_detail.SetItem(ll_New_Row, "line_number", dw_rmt_detail.GetItemNumber(ll_Old_Row, "line_number"))
//dw_rmt_detail.SetItem(ll_New_Row, "product", dw_rmt_detail.GetItemString(ll_Old_Row, "product"))
//dw_rmt_detail.SetItem(ll_New_Row, "product_state", dw_rmt_detail.GetItemString(ll_Old_Row, "product_state"))
dw_rmt_detail.SetItem(ll_New_Row, "update_flag", "A")

This.SetRedraw(True)

return true
end function

public subroutine wf_delete ();String	ls_header_string_in, &
			ls_header_string_out, &
			ls_UpdateRMDS
			
Long		ll_ret			



If MessageBox("Warning", "Are you sure you wish to delete the entire Purchase Order?", Exclamation!, YesNo!) <> 1 then return

dw_rmt_header.SetItem(1, "action_ind", "D")
ls_header_string_in = dw_rmt_header.Describe("DataWindow.Data")
ls_UpdateRMDS = iw_frame.iu_string.nf_BuildUpdateString(dw_rmt_detail)

ll_ret = iu_pas203.nf_pasp44cr_upd_rmt_po_detail(istr_error_info, ls_UpdateRMDS, ls_header_string_in, ls_header_string_out) 

end subroutine

public function boolean wf_validate (long al_row);Date					ldt_production_date, &
						ldt_delivery_date

Long					ll_number, &
						ll_nbrrows, &
						ll_find
						
Decimal{4}			ldc_price	
Decimal{1}			ldc_pounds

String				ls_product, &
						ls_product_state, &
						ls_price_uom, &
						ls_find_string
						
Integer				li_boxes						

ll_nbrrows = dw_rmt_detail.RowCount()

ll_number = dw_rmt_detail.GetItemNumber(al_row, "line_number")
IF IsNull(ll_number) or ll_number = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a line number.  Line number cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("line_number")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_product = dw_rmt_detail.GetItemString(al_row, "product")
IF IsNull(ls_product) or Len(trim(ls_product)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a product code.  Product Code cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("product")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If not invuo_fab_product_code.uf_check_product(ls_product) then
		If	invuo_fab_product_code.ib_error_occurred then
			iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
			Return False
		Else
			iw_frame.SetMicroHelp(ls_product + " is an invalid Product Code")
			This.SetRedraw(False)
			dw_rmt_detail.ScrollToRow(al_row)
			dw_rmt_detail.SetColumn("product")
			dw_rmt_detail.SetFocus()
			This.SetRedraw(True)
			Return False
		End If
	End If
End If

ls_product_state = dw_rmt_detail.GetItemString(al_row, "product_state")
IF IsNull(ls_product_state) or Len(trim(ls_product_state)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a product state.  Product state cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("product_state")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	ls_product = dw_rmt_detail.getitemstring(al_row, "product")
	if trim(ls_product) <> "1" then
		if not invuo_fab_product_code.uf_check_product_state_rmt(ls_product, Trim(ls_product_state)) then	
			iw_Frame.SetMicroHelp(ls_product_state + " is an invalid Product State for this Product")
			dw_rmt_detail.setfocus( )
			dw_rmt_detail.setcolumn("product_state") 
			dw_rmt_detail.SelectText(1, Len("product_state"))	
			Return False
		end if
	end if
End If

ldc_price = dw_rmt_detail.GetItemDecimal(al_row, "price")
if isnull(ldc_price) or ldc_price < 0 or ldc_price > 99999.9999 then
	iw_Frame.SetMicroHelp("Price must be a positive number less than or equal to 99999.9999")
	dw_rmt_detail.setfocus( )
	dw_rmt_detail.setcolumn("price") 
	dw_rmt_detail.SelectText(1, Len("price"))	
	Return False
End If

ls_price_uom = dw_rmt_detail.GetItemString(al_row, "price_uom")
IF IsNull(ls_price_uom) or Len(trim(ls_price_uom)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a price unit of measure.  Price UOM cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("price_uom")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ldt_production_date = dw_rmt_detail.GetItemDate(al_row, "production_date")
IF IsNull(ldt_production_date) or ldt_production_date <= Date('00/00/0000') Then 
	iw_Frame.SetMicroHelp("Please enter a production date greater than 00/00/0000.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("production date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	ldt_delivery_date = dw_rmt_header.GetItemDate(1, "delivery_date")
	if ldt_production_date > ldt_delivery_date Then
		iw_Frame.SetMicroHelp("Production date cannot be greater than delivery date.")
		This.SetRedraw(False)
		dw_rmt_detail.ScrollToRow(al_row)
		dw_rmt_detail.SetColumn("production_date")
		dw_rmt_detail.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
End If

li_boxes = dw_rmt_detail.GetItemNumber(al_row, "boxes")
if isnull(li_boxes) or li_boxes < 0 or li_boxes > 9999999 then
	iw_Frame.SetMicroHelp("Boxes must be a positive number less than or equal to 9999999.")
	dw_rmt_detail.setfocus( )
	dw_rmt_detail.setcolumn("boxes") 
	dw_rmt_detail.SelectText(1, Len("boxes"))	
	Return False
End If

ldc_pounds = dw_rmt_detail.GetItemDecimal(al_row, "pounds")
if isnull(ldc_pounds) or ldc_pounds < 0 or ldc_pounds > 999999999.9 then
	iw_Frame.SetMicroHelp("Pounds must be a positive number less than or equal to 999999999.9.")
	dw_rmt_detail.setfocus( )
	dw_rmt_detail.setcolumn("pounds") 
	dw_rmt_detail.SelectText(1, Len("pounds"))	
	Return False
End If

if al_row > 1 then
	ls_find_string = "line_number = "
	ls_find_string += string(dw_rmt_detail.GetItemNumber(al_row, "line_number"))
	ls_find_string += " and production_date = date("
	ls_find_string += '"' + string(dw_rmt_detail.GetItemDate(al_row, "production_date"), "mm/dd/yyyy") + '"'
	ls_find_string += ')'
	ll_find = dw_rmt_detail.Find(ls_find_string, 1, al_row - 1)
	if (ll_find > 0) Then
		iw_Frame.SetMicroHelp( "There are duplicate products with the" + &
				" same line number and production date.")
		Return False		  
	End If
End If

Return True
end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount, &
					ll_ret, ll_temp

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdateRMDS, &
					ls_header_string_in, &
					ls_header_string_out

dwItemStatus	lis_status

u_string_functions	lu_string_functions


If dw_rmt_header.AcceptText() = -1 Then Return False
If dw_rmt_detail.AcceptText() = -1 Then Return False

ll_temp = dw_rmt_detail.DeletedCount()

If dw_rmt_detail.ModifiedCount() + dw_rmt_detail.DeletedCount() <= 0 Then Return False

if dw_rmt_header.GetItemString(1, "action_ind") = 'A' Then
	If lu_string_functions.nf_isempty(dw_rmt_header.GetItemString(1,"purchase_order")) then
		iw_Frame.SetMicroHelp("Purchase order number is required.")
		Return False
	End If
	
	If dw_rmt_header.GetItemDate(1, "receive_date") <= today() and &
			dw_rmt_header.GetItemDate(1, "receive_date") > date("01/01/0001") Then
		iw_Frame.SetMicroHelp("Receive date must be greater than today.")
		Return False
	End If
End if	


ll_NbrRows = dw_rmt_detail.RowCount( )
ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_rmt_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

ls_header_string_in = dw_rmt_header.Describe("DataWindow.Data")
ls_UpdateRMDS = iw_frame.iu_string.nf_BuildUpdateString(dw_rmt_detail)

ll_ret = iu_pas203.nf_pasp44cr_upd_rmt_po_detail(istr_error_info, ls_UpdateRMDS, ls_header_string_in, ls_header_string_out) 

dw_rmt_detail.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_rmt_detail.SetItem(li_Counter, 'update_flag', ' ')
Next

dw_rmt_detail.ResetUpdate()

dw_rmt_detail.SetReDraw(True)

SetPointer(Arrow!)

Return True
end function

public function boolean wf_retrieve ();String	ls_input, &
			ls_header, &
			ls_output
		
			
Long		ll_value, &
			ll_rtn

u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

OpenWithParm(w_raw_mat_purch_ord_detail_inq, This)
ls_input = Message.StringParm

if ls_input = "Cancel" Then
	This.wf_new()
Else
	SetPointer(HourGlass!)
	ll_rtn = iu_pas203.nf_pasp43cr_inq_rmt_po_detail(istr_error_info, &
										ls_input, &
										ls_header, &
										ls_output) 
									
	dw_rmt_header.Reset()
	dw_rmt_detail.Reset()
	
	if ls_header > '' then
		dw_rmt_header.ImportString(ls_header)
	else
		dw_rmt_header.insertrow(0)
		iw_Frame.SetMicroHelp("Purchase order number not found.")
	end if	
	
	if ls_output > '' then
		dw_rmt_detail.ImportString(trim(ls_output))
	else
		dw_rmt_detail.insertrow(0)
	end if
	
	dw_rmt_detail.SetFocus() 
	dw_rmt_header.ResetUpdate()
	dw_rmt_detail.ResetUpdate()
	SetPointer(Arrow!)
End If

return true

end function

event close;call super::close;If IsValid(iu_pas203) Then Destroy(iu_pas203)



end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')
//iw_frame.im_menu.mf_enable('m_save')
//iw_frame.im_menu.mf_enable('m_delete')

//If not ib_good_product Then
//	iw_frame.im_menu.mf_disable('m_new')
//	iw_frame.im_menu.mf_disable('m_addrow')
//	iw_frame.im_menu.mf_disable('m_save')
//	iw_frame.im_menu.mf_disable('m_deleterow')
//End If
end event

event open;call super::open;iu_pas203 = Create u_pas203




end event

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Raw Material Purchase Order Detail"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")

end event

on w_raw_mat_purch_ord_detail.create
int iCurrent
call super::create
this.dw_rmt_header=create dw_rmt_header
this.dw_rmt_detail=create dw_rmt_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_rmt_header
this.Control[iCurrent+2]=this.dw_rmt_detail
end on

on w_raw_mat_purch_ord_detail.destroy
call super::destroy
destroy(this.dw_rmt_header)
destroy(this.dw_rmt_detail)
end on

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_rmt_detail.x * 2) + 30 
li_y = dw_rmt_detail.y + 115

if width > li_x Then
	dw_rmt_detail.width	= width - li_x
end if

if height > li_y then
	dw_rmt_detail.height	= height - li_y
end if
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'purchase_order' 
		Message.StringParm = dw_rmt_header.GetItemString(1, 'purchase_order')
End Choose

end event

type dw_rmt_header from u_base_dw_ext within w_raw_mat_purch_ord_detail
integer x = 5
integer width = 2875
integer height = 452
integer taborder = 10
string dataobject = "d_raw_mat_purch_detail_hdr"
boolean border = false
end type

event itemchanged;call super::itemchanged;String	ls_location_name

If dwo.name = "destination_plant" Then
		
	SELECT 	location_name
	INTO		:ls_location_name
	FROM     locations
	WHERE    (location_code = :data)
	ORDER BY location_code
	Using SQLCA;
	
	If SQLCA.SQLCode = 0 Then	
		this.SetItem(row,"destination_desc",ls_location_name)	
	End If
End If	
end event

event constructor;call super::constructor;This.InsertRow(0)
end event

type dw_rmt_detail from u_base_dw_ext within w_raw_mat_purch_ord_detail
integer x = 5
integer y = 452
integer width = 2862
integer height = 1032
integer taborder = 20
string dataobject = "d_raw_mat_purch_detail_dtl"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean ib_updateable = true
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return



end event

event constructor;call super::constructor;DataWindowChild	ldddw_child

ib_updateable = True

This.GetChild("product_state", ldddw_child)

ldddw_child.SetTransObject(SQLCA)
ldddw_child.Retrieve("PRDSTATE")

This.GetChild("price_uom", ldddw_child)

ldddw_child.SetTransObject(SQLCA)
ldddw_child.Retrieve("RMTPOUOM")

This.InsertRow(0)

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

dw_rmt_detail.GetChild('uom', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("RAWUOM")

dw_rmt_detail.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
end event

event itemchanged;call super::itemchanged;//dwItemStatus	le_RowStatus
//
//int			li_rc, &
//				li_seconds,li_temp
//
//long			ll_char_row, &
//				ll_source_row, &
//				ll_plant_row, &
//				ll_RowCount, &
//				ll_find_row
//
//string		ls_GetText, &
//				ls_temp
//Date			ldt_temp	
//nvuo_pa_business_rules	u_rule
//
//
//is_ColName = GetColumnName()
//ls_GetText = data
//ll_source_row	= GetRow()
//il_ChangedRow = 0
//
//CHOOSE CASE is_ColName
//	CASE "product_code"
//		If not invuo_fab_product_code.uf_check_product(data) then
//			If	invuo_fab_product_code.ib_error_occurred then
//				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
//				Return 1
//			Else
//				iw_frame.SetMicroHelp(data + " is an invalid Product Code")
//				Return 1
//			End If
//		End If
//	CASE "product_state"
//		string ls_product
//		ls_product = this.getitemstring(row, "product_code")
//		if trim(data) <> "1" then
//			if not invuo_fab_product_code.uf_check_product_state_rmt(ls_product, Trim(data)) then	
//				iw_Frame.SetMicroHelp(data + " is an invalid Product State for this Product")
//				this.setfocus( )
//				this.setcolumn("product_state") 
//				This.SelectText(1, Len(data))	
//				Return 1
//			end if
//		end if
//	CASE "start_date", "end_date"
//		If isnull(data) Then
//			iw_frame.SetMicroHelp("This is not a valid date")
//			This.selecttext(1,100)
//			return 1
//		End If
//	Case "days_old_min", "days_old_max"
//		If Not IsNumber(ls_GetText) Then
//			iw_frame.SetMicroHelp("Days must be a number")
//			This.selecttext(1,100)
//			return 1
//		End if
//		If Real(ls_GetText) < 0 Then
//			iw_frame.SetMicroHelp("Days cannot be negative")
//			This.selecttext(1,100)
//			return 1
//		End if
//	Case "quantity"
//		If Not IsNumber(ls_GetText) Then
//			iw_frame.SetMicroHelp("Quantity must be a number")
//			This.selecttext(1,100)
//			return 1
//		End if
//		If Real(ls_GetText) < 0 Then
//			iw_frame.SetMicroHelp("Quantity cannot be negative")
//			This.selecttext(1,100)
//			return 1
//		End if
//END CHOOSE
//
//// Update the Update_Flag column so the RPC will know how to update the row
//IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
//	IF Long(This.Describe("update_flag.id")) > 0 THEN
//		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
//			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
//			CHOOSE CASE le_RowStatus
//			CASE NewModified!, New!
//				This.SetItem(ll_source_row, "update_flag", "A")
//			CASE DataModified!, NotModified!
//				This.SetItem(ll_source_row, "update_flag", "U")
//			END CHOOSE		
//		End if
//	END IF
//END IF
//
////parent.PostEvent("ue_postitemchanged")
//iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
//
return 0
end event

event itemerror;call super::itemerror;return (1)
end event

