HA$PBExportHeader$w_pork_hot_sheet_report.srw
forward
global type w_pork_hot_sheet_report from w_netwise_sheet
end type
type cbx_all_plants from checkbox within w_pork_hot_sheet_report
end type
type dw_plant from u_plant within w_pork_hot_sheet_report
end type
end forward

global type w_pork_hot_sheet_report from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 1637
integer height = 328
string title = "Pork Hot Sheet Report"
long backcolor = 79741120
cbx_all_plants cbx_all_plants
dw_plant dw_plant
end type
global w_pork_hot_sheet_report w_pork_hot_sheet_report

type variables
s_error			istr_error_info
string			Is_inquire_parm

u_pas201		iu_pas201
u_ws_pas2      iu_ws_pas2
end variables

forward prototypes
public function boolean wf_update ()
end prototypes

public function boolean wf_update ();boolean lb_Return

string 	ls_update_string, &
			ls_tranid, &
			ls_type, &
			ls_view, &
			ls_userid, &
			ls_date, &
			ls_time, &
			ls_plant
	
IF dw_plant.AcceptText() = -1 THEN Return( False )	
	
				
SetPointer(HourGlass!)
ls_tranid = 'P215'  // run pas215xe

ls_type = '1'
If cbx_all_plants.checked Then
	ls_plant = '   '
ELSE
		ls_plant  = dw_plant.GetItemString(1, 'location_code')
end if

ls_view = 'V'
ls_userid = sqlca.userid
ls_update_string = ls_type  + &
 					ls_tranid + &
					 '    ' + &
					ls_userid + &
					' ' + &
					ls_view + &
					' 01    ' + &
					ls_plant 
					


istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_pasp72gr"
istr_error_info.se_message = Space(71)

//lb_return = iu_pas201.nf_pasp72cr_init_mainframe_report(istr_error_info, ls_Update_string)

lb_return = iu_ws_pas2.nf_pasp72gr(ls_Update_string,istr_error_info)

SetPointer(Arrow!)

iw_Frame.SetMicroHelp("Report initiated.")

return lb_Return



end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	


end event

event ue_postopen;call super::ue_postopen;iu_pas201 = CREATE u_pas201
iu_ws_pas2 = CREATE u_ws_pas2

end event

on w_pork_hot_sheet_report.create
int iCurrent
call super::create
this.cbx_all_plants=create cbx_all_plants
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_all_plants
this.Control[iCurrent+2]=this.dw_plant
end on

on w_pork_hot_sheet_report.destroy
call super::destroy
destroy(this.cbx_all_plants)
destroy(this.dw_plant)
end on

event resize;//if newwidth > 2694 then
//	ole_init_mainframe_reports.Width = newwidth - 25
//end if
//
//if (newheight - 100) > 500 then
//	ole_init_mainframe_reports.Height = newheight - 25
//end if

end event

event open;call super::open;//String ls_temp 
//ls_temp = Message.StringParm	
//
//If Len(ls_temp) > 0 Then
//	Is_inquire_parm = ls_temp
//End If
//

end event

event close;call super::close;If IsValid( iu_pas201) Then Destroy( iu_pas201)
If IsValid(iu_ws_pas2) Then Destroy(iu_ws_pas2)
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		Message.StringParm = dw_plant.uf_get_plant_code()
End choose

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
End choose
end event

type cbx_all_plants from checkbox within w_pork_hot_sheet_report
integer x = 73
integer width = 402
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "All Plants"
end type

event clicked;If cbx_all_plants.checked = True Then
	dw_plant.Hide()
Else
	dw_plant.Show()
End if
end event

type dw_plant from u_plant within w_pork_hot_sheet_report
integer x = 37
integer y = 64
integer width = 1097
integer height = 64
integer taborder = 10
end type

event constructor;call super::constructor;if this.rowcount() = 0 then this.insertrow(0)
end event

