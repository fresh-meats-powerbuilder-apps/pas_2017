HA$PBExportHeader$w_aged_c_f_product_report.srw
forward
global type w_aged_c_f_product_report from w_netwise_sheet
end type
type cbx_days from checkbox within w_aged_c_f_product_report
end type
end forward

global type w_aged_c_f_product_report from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 1591
integer height = 280
string title = "Aged C-F Product Report"
long backcolor = 79741120
cbx_days cbx_days
end type
global w_aged_c_f_product_report w_aged_c_f_product_report

type variables
s_error			istr_error_info
string			Is_inquire_parm

u_pas201	iu_pas201
u_ws_pas2  iu_ws_pas2
end variables

forward prototypes
public function boolean wf_update ()
end prototypes

public function boolean wf_update ();boolean lb_Return

string ls_update_string, &
			ls_tranid, &
			ls_type, &
			ls_plant_name, &
			ls_view, &
			ls_month, &
			ls_userid, &
			ls_date, &
			ls_time, &
			ls_opid, &
			ls_division
			
	
SetPointer(HourGlass!)
If cbx_days.checked = True Then
	ls_tranid = 'P393'   // run pas393xe
Else
	ls_tranid = 'P185'  // run pas185xe
End if
ls_type = '1'

ls_plant_name = '            '
ls_view = 'V'
ls_userid = sqlca.userid
ls_date = string(today(), "YYMMDD")
ls_time = string(Now(), "HHMMSS")
ls_opid = Right(ls_userid,3)
ls_update_string = ls_type  + &
 					ls_tranid + &
					 '    ' + &
					ls_userid + &
					' ' + &
					ls_view 				
					


istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_pasp72gr"
istr_error_info.se_message = Space(71)

//lb_return = iu_pas201.nf_pasp72cr_init_mainframe_report(istr_error_info, ls_Update_string)

lb_return = iu_ws_pas2.nf_pasp72gr(ls_Update_string,istr_error_info)

SetPointer(Arrow!)

iw_Frame.SetMicroHelp("Report initiated.")

return lb_Return



end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	


end event

event ue_postopen;call super::ue_postopen;string	ls_server_suffix, &
			ls_tran_id
			
iu_pas201 = CREATE u_pas201
iu_ws_pas2 = Create u_ws_pas2

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_aged_c_f_product_report"
istr_error_info.se_user_id 		= sqlca.userid


end event

on w_aged_c_f_product_report.create
int iCurrent
call super::create
this.cbx_days=create cbx_days
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_days
end on

on w_aged_c_f_product_report.destroy
call super::destroy
destroy(this.cbx_days)
end on

event resize;//if newwidth > 2694 then
//	ole_init_mainframe_reports.Width = newwidth - 25
//end if
//
//if (newheight - 100) > 500 then
//	ole_init_mainframe_reports.Height = newheight - 25
//end if

end event

event open;call super::open;//String ls_temp 
//ls_temp = Message.StringParm	
//
//If Len(ls_temp) > 0 Then
//	Is_inquire_parm = ls_temp
//End If
//
end event

event close;call super::close;If IsValid( iu_pas201) Then Destroy( iu_pas201)
If IsValid(iu_ws_pas2) Then Destroy(iu_ws_pas2)
end event

event ue_get_data;call super::ue_get_data;//Choose Case as_value
//	Case 'plant'
//		Message.StringParm = dw_plant.uf_get_plant_code()
//End choose
//
end event

event ue_set_data;call super::ue_set_data;//Choose Case as_data_item
//	Case 'plant'
//		dw_plant.uf_set_plant_code(as_value)
//End choose
end event

type cbx_days from checkbox within w_aged_c_f_product_report
integer x = 37
integer y = 32
integer width = 512
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "11-13 Days Old"
end type

