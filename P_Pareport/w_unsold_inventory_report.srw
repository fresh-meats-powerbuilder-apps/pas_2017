HA$PBExportHeader$w_unsold_inventory_report.srw
forward
global type w_unsold_inventory_report from w_netwise_sheet
end type
type dw_export_country from u_base_dw_ext within w_unsold_inventory_report
end type
type dw_division from u_division within w_unsold_inventory_report
end type
end forward

global type w_unsold_inventory_report from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 1664
integer height = 364
string title = "Unsold Inventory Report"
long backcolor = 79741120
dw_export_country dw_export_country
dw_division dw_division
end type
global w_unsold_inventory_report w_unsold_inventory_report

type variables
s_error			istr_error_info
string			Is_inquire_parm

u_pas201		iu_pas201

u_ws_pas2      iu_ws_pas2
end variables

forward prototypes
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
end prototypes

public function boolean wf_validate (long al_row);string ls_division

ls_division = dw_division.GetItemString(1, 'division_code')

IF IsNull(ls_division) or Len(trim(ls_division)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a division.  Division cannot be left blank.")
	This.SetRedraw(False)
	dw_division.ScrollToRow(1)
	dw_division.SetColumn("division_code")
	dw_division.SetFocus()
	This.SetRedraw(True)
	Return False
End If

Return True
end function

public function boolean wf_update ();boolean lb_Return

string ls_update_string, &
			ls_tranid, &
			ls_type, &
			ls_plant_name, &
			ls_view, &
			ls_month, &
			ls_userid, &
			ls_date, &
			ls_time, &
			ls_opid, &
			ls_country

IF dw_division.AcceptText() = -1 THEN Return( False )	
if dw_export_country.AcceptText() = -1 THEN Return( False )	
SetPointer(HourGlass!)

ls_country = dw_export_country.getitemstring(1,"export_country")
//values to pass to common report initiator for pas126xe

ls_type = '3'
ls_tranid = 'P167'
ls_plant_name = '            '
ls_view = 'V'
ls_userid = sqlca.userid
ls_date = string(today(), "YYMMDD")
ls_time = string(Now(), "HHMMSS")
ls_opid = Right(ls_userid,3)
ls_update_string = ls_type  + &
 					ls_tranid + &
					ls_opid + &
					'    ' + &
					ls_userid + &
					' ' + &
					ls_view + &
					trim(dw_division.GetItemString(1, 'division_code')) + &
					dw_export_country.getitemstring(1,"export_country")
//


istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_pasp72gr"
istr_error_info.se_message = Space(71)

//lb_return = iu_pas201.nf_pasp72cr_init_mainframe_report(istr_error_info, ls_Update_string)

lb_return = iu_ws_pas2.nf_pasp72gr(ls_Update_string,istr_error_info)

SetPointer(Arrow!)

iw_Frame.SetMicroHelp("Report initiated.")

return lb_Return
end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	


end event

event ue_postopen;call super::ue_postopen;string	ls_server_suffix

iu_pas201 = CREATE u_pas201

iu_ws_pas2 = Create u_ws_pas2

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_unsold_inventory_report"
istr_error_info.se_user_id 		= sqlca.userid


dw_division.Modify("DataWindow.Color = 78682240")


end event

on w_unsold_inventory_report.create
int iCurrent
call super::create
this.dw_export_country=create dw_export_country
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_export_country
this.Control[iCurrent+2]=this.dw_division
end on

on w_unsold_inventory_report.destroy
call super::destroy
destroy(this.dw_export_country)
destroy(this.dw_division)
end on

event resize;//if newwidth > 2694 then
//	ole_init_mainframe_reports.Width = newwidth - 25
//end if
//
//if (newheight - 100) > 500 then
//	ole_init_mainframe_reports.Height = newheight - 25
//end if

end event

event open;call super::open;//String ls_temp 
//ls_temp = Message.StringParm	
//
//If Len(ls_temp) > 0 Then
//	Is_inquire_parm = ls_temp
//End If
//
end event

event close;call super::close;If IsValid( iu_pas201) Then Destroy( iu_pas201)

If IsValid(iu_ws_pas2) Then Destroy(iu_ws_pas2)
end event

event ue_get_data;call super::ue_get_data;//Choose Case as_value
//	Case 'plant'
//		Message.StringParm = dw_plant.uf_get_plant_code()
//End choose
//
end event

event ue_set_data;call super::ue_set_data;//Choose Case as_data_item
//	Case 'plant'
//		dw_plant.uf_set_plant_code(as_value)
//End choose
end event

type dw_export_country from u_base_dw_ext within w_unsold_inventory_report
integer y = 128
integer width = 1024
integer height = 96
integer taborder = 20
string dataobject = "d_exp_country"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild			ldwc_temp

this.insertrow(0)
dw_export_country.GetChild('export_country', ldwc_temp)

IF ldwc_temp.RowCount()	= 0 THEN 
	ldwc_temp.SetTransObject(SQLCA)
	ldwc_temp.Retrieve()
end if

ib_updateable = False
end event

type dw_division from u_division within w_unsold_inventory_report
integer y = 32
integer taborder = 10
boolean controlmenu = true
end type

event constructor;call super::constructor;if this.rowcount() = 0 then this.insertrow(0)
end event

