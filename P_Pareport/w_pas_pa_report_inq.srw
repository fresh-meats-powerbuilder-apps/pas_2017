HA$PBExportHeader$w_pas_pa_report_inq.srw
$PBExportComments$Age Availability Inquire Window
forward
global type w_pas_pa_report_inq from w_base_response_ext
end type
type dw_product_state from u_product_state within w_pas_pa_report_inq
end type
type dw_plant from u_plant within w_pas_pa_report_inq
end type
type st_location_group from statictext within w_pas_pa_report_inq
end type
type dw_division from u_division within w_pas_pa_report_inq
end type
type st_product_group from statictext within w_pas_pa_report_inq
end type
type dw_display_option from u_loads_boxes within w_pas_pa_report_inq
end type
type dw_plant_option from u_base_dw_ext within w_pas_pa_report_inq
end type
type dw_plant_type from u_base_dw_ext within w_pas_pa_report_inq
end type
type dw_product_option from u_base_dw_ext within w_pas_pa_report_inq
end type
type dw_weekly_daily from u_base_dw_ext within w_pas_pa_report_inq
end type
type dw_product_file from u_base_dw_ext within w_pas_pa_report_inq
end type
type cbx_division from checkbox within w_pas_pa_report_inq
end type
type cbx_product_state from checkbox within w_pas_pa_report_inq
end type
type cbx_product_status from checkbox within w_pas_pa_report_inq
end type
type dw_product_status from u_base_dw_ext within w_pas_pa_report_inq
end type
type dw_begin_date from u_base_dw_ext within w_pas_pa_report_inq
end type
type uo_locations from u_group_list within w_pas_pa_report_inq
end type
type uo_products from u_group_list within w_pas_pa_report_inq
end type
type dw_userid_list from u_base_dw_ext within w_pas_pa_report_inq
end type
end forward

global type w_pas_pa_report_inq from w_base_response_ext
integer x = 4
integer y = 378
integer width = 2392
integer height = 2723
long backcolor = 67108864
dw_product_state dw_product_state
dw_plant dw_plant
st_location_group st_location_group
dw_division dw_division
st_product_group st_product_group
dw_display_option dw_display_option
dw_plant_option dw_plant_option
dw_plant_type dw_plant_type
dw_product_option dw_product_option
dw_weekly_daily dw_weekly_daily
dw_product_file dw_product_file
cbx_division cbx_division
cbx_product_state cbx_product_state
cbx_product_status cbx_product_status
dw_product_status dw_product_status
dw_begin_date dw_begin_date
uo_locations uo_locations
uo_products uo_products
dw_userid_list dw_userid_list
end type
global w_pas_pa_report_inq w_pas_pa_report_inq

type variables
u_orp204	iu_orp204

u_ws_pas3	iu_ws_pas3

u_ws_orp4	iu_ws_orp4

Long	il_selected_status_max = 8

Boolean	ib_open, &
			ib_modified = false

String         is_selected_status_array, & 
					is_product_status_table

Integer		ii_dddw_x, ii_dddw_y, first_time

w_pas_pa_report	iw_parentwindow2
end variables

forward prototypes
public subroutine wf_set_selected_status ()
public subroutine wf_get_selected_status (datawindow adw_datawindow)
public subroutine wf_load_userids ()
public subroutine wf_load_product_file (string as_userid)
end prototypes

public subroutine wf_set_selected_status ();DataStore			lds_selected_status
long					ll_rowcount, ll_count, ll_cnt, ll_rowcnt
string				ls_product_status, ls_stat_desc

//this function will take the status fields from the popup window and push them to the main datawindow

lds_selected_status = Create DataStore
lds_selected_status.DataObject = 'd_product_status_2'
ll_rowcount = lds_selected_status.importstring(is_selected_status_array)

if ib_modified then
	for ll_rowcnt = 1 to il_selected_status_max
		ls_product_status = lds_selected_status.getitemstring(1, "product_status"+ string(ll_rowcnt))
		dw_product_status.setitem(1, "product_status"+ string(ll_rowcnt), ls_product_status)
		ls_stat_desc = lds_selected_status.getitemstring(1, "status_desc"+ string(ll_rowcnt))
		dw_product_status.setitem(1, "status_desc"+ string(ll_rowcnt), ls_stat_desc)
	next

end if

dw_product_status.accepttext()
ib_open = false



end subroutine

public subroutine wf_get_selected_status (datawindow adw_datawindow);long							ll_count
string						ls_prod_status_string, ls_stat_desc
integer						li_windowx, li_windowy, li_height, li_height_H
Application					la_Application

//this function will get the list of product status values that exist in the window that were previously
//selected from the popup (dropdown) window
la_Application = GetApplication()
is_selected_status_array = ""
for ll_count = 1 to il_selected_status_max
    ls_prod_status_string = adw_datawindow.getitemstring(1, "product_status" + string(ll_count))
	 ls_stat_desc = adw_datawindow.getitemstring(1, "status_desc" + string(ll_count))
//	
   if ll_count = 1 then
	   if isnull(ls_prod_status_string) then
		  ls_prod_status_string = 'ALL'
		  ls_stat_desc = 'ALL'
	   END IF		
	else
   	if isnull(ls_prod_status_string) then
	   	ls_prod_status_string = ' '
		   ls_stat_desc = ' '
	   end if
	end if
	is_selected_status_array += ls_prod_status_string + "~t"
	 
	is_selected_status_array += ls_stat_desc + "~t"
	
next

if isnull(is_selected_status_array) Then
	//do nothing
else
   is_selected_status_array = is_selected_status_array + "~r~n"
end if

// find opening x/y positions for "Multi select drop down"
la_Application = GetApplication()
Choose Case iw_frame.ToolBarAlignment
	Case AlignAtLeft!
//		li_height = iw_frame.workspacey() + 52
		li_height_H = 0
		li_height = 0
	Case AlignAtTop!
		if iw_frame.ToolBarVisible then
//			li_height = iw_frame.workspacey() + 52 + 102
			if la_Application.ToolBarText then
				li_height = 50
				li_height_H = 50
			end if
		else
//			li_height = iw_frame.workspacey() + 52
			li_height_H = - 102
			li_height = -102
		end if
End Choose

//x = across and y = down positions
ii_dddw_x = integer(dw_product_status.X) + 35
ii_dddw_y = integer(dw_product_status.Y) + 0
end subroutine

public subroutine wf_load_userids ();String		ls_header_string, ls_detail_string

Boolean	lb_return

Integer	li_Rtn

DataWindowChild	ldwc_temp
		
s_Error	lstr_Error

lstr_Error.se_app_name  = "PAS"
lstr_Error.se_window_name = "w_pas_pa_report"
lstr_Error.se_function_name = "wf_load_userids"

ls_header_string = 'U' + '~t'

lb_return	= iu_ws_pas3.uf_pasp06hr( lstr_Error, ls_header_string, ls_detail_string)

dw_userid_list.Reset()
dw_userid_list.ImportString(ls_detail_string)
dw_userid_list.ResetUpdate()

dw_userid_list.GetChild('userid', ldwc_temp)
ldwc_temp.Reset()
ldwc_temp.ImportString(ls_detail_string)


end subroutine

public subroutine wf_load_product_file (string as_userid);Char	lc_ProcessOption, &
		lc_AvailableDate[10]			

DataWindowChild	ldwc_file_number, &
						ldwc_file_descr

Integer	li_Rtn

String	ls_description_out, &
			ls_plantdata, &
			ls_detaildata, &
			ls_descr_out, &
			ls_page_descr
			
s_Error	lstr_Error

lstr_Error.se_app_name  = "PAS"
lstr_Error.se_window_name = "w_PA-Summary"
lstr_Error.se_function_name = "wf_Retriev"
//lstr_Error.se_user_id = STRING(UPPER(Message.nf_getuserid()),"@@@@@@@@")

lc_ProcessOption = "Q"
lc_AvailableDate = ""
ls_page_descr = ""
ls_PlantData = ""
ls_DetailData = ""
ls_descr_out = ""

////call rpc 
//li_Rtn = iu_orp204.nf_orpo81br_pa_summary(lstr_Error, lc_ProcessOption, &
//										lc_AvailableDate, ls_page_descr, &
//										ls_PlantData, ls_DetailData, ls_descr_out)		

li_Rtn = iu_ws_orp4.nf_orpo81fr(lstr_Error, lc_ProcessOption, &
										lc_AvailableDate, ls_page_descr, &
										ls_descr_out, ls_PlantData, ls_DetailData, as_userid)	

dw_product_file.GetChild('file_no', ldwc_file_number)
IF NOT ISVAlid(ldwc_file_number)Then
	MessageBox("","seq_number not valid")
	return
END IF

dw_product_file.GetChild('file_desc', ldwc_file_descr)
IF NOT ISVAlid(ldwc_file_descr)Then
	MessageBox("","description not valid")
	return
END IF

ldwc_file_number.Reset()
ldwc_file_number.ImportString(ls_descr_out)
ldwc_file_number.ShareData(ldwc_file_descr)

dw_product_file.SetItem(1, "file_no", ldwc_file_number.GetItemString(1, "file_no"))
dw_product_file.SetItem(1, "file_desc", ldwc_file_number.GetItemString(1, "file_desc"))
dw_product_file.SetFocus()
dw_product_file.SelectText(1,2)


end subroutine

on w_pas_pa_report_inq.create
int iCurrent
call super::create
this.dw_product_state=create dw_product_state
this.dw_plant=create dw_plant
this.st_location_group=create st_location_group
this.dw_division=create dw_division
this.st_product_group=create st_product_group
this.dw_display_option=create dw_display_option
this.dw_plant_option=create dw_plant_option
this.dw_plant_type=create dw_plant_type
this.dw_product_option=create dw_product_option
this.dw_weekly_daily=create dw_weekly_daily
this.dw_product_file=create dw_product_file
this.cbx_division=create cbx_division
this.cbx_product_state=create cbx_product_state
this.cbx_product_status=create cbx_product_status
this.dw_product_status=create dw_product_status
this.dw_begin_date=create dw_begin_date
this.uo_locations=create uo_locations
this.uo_products=create uo_products
this.dw_userid_list=create dw_userid_list
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_product_state
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.st_location_group
this.Control[iCurrent+4]=this.dw_division
this.Control[iCurrent+5]=this.st_product_group
this.Control[iCurrent+6]=this.dw_display_option
this.Control[iCurrent+7]=this.dw_plant_option
this.Control[iCurrent+8]=this.dw_plant_type
this.Control[iCurrent+9]=this.dw_product_option
this.Control[iCurrent+10]=this.dw_weekly_daily
this.Control[iCurrent+11]=this.dw_product_file
this.Control[iCurrent+12]=this.cbx_division
this.Control[iCurrent+13]=this.cbx_product_state
this.Control[iCurrent+14]=this.cbx_product_status
this.Control[iCurrent+15]=this.dw_product_status
this.Control[iCurrent+16]=this.dw_begin_date
this.Control[iCurrent+17]=this.uo_locations
this.Control[iCurrent+18]=this.uo_products
this.Control[iCurrent+19]=this.dw_userid_list
end on

on w_pas_pa_report_inq.destroy
call super::destroy
destroy(this.dw_product_state)
destroy(this.dw_plant)
destroy(this.st_location_group)
destroy(this.dw_division)
destroy(this.st_product_group)
destroy(this.dw_display_option)
destroy(this.dw_plant_option)
destroy(this.dw_plant_type)
destroy(this.dw_product_option)
destroy(this.dw_weekly_daily)
destroy(this.dw_product_file)
destroy(this.cbx_division)
destroy(this.cbx_product_state)
destroy(this.cbx_product_status)
destroy(this.dw_product_status)
destroy(this.dw_begin_date)
destroy(this.uo_locations)
destroy(this.uo_products)
destroy(this.dw_userid_list)
end on

event close;String			ls_setvalue

If ib_ok_to_close Then
	ls_setvalue = 'True'
Else
	ls_setvalue = 'False'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('ib_inquire', ls_setvalue)
End IF

Destroy(iu_orp204)
Destroy(iu_ws_orp4)
Destroy(iu_ws_pas3)
end event

event open;call super::open;If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		iw_parentwindow = message.powerobjectparm
		iw_parentwindow2 = iw_parentwindow
		This.Title = iw_parentwindow.Title + " Inquire"
	End If
End If

iu_orp204 = Create u_orp204
iu_ws_orp4 = Create u_ws_orp4
iu_ws_pas3 = Create u_ws_pas3

end event

event ue_base_cancel;ib_ok_to_close = False

Close(This)
end event

event ue_base_ok;String	ls_weekly_daily_option, &
			ls_plant_option, &
			ls_plant_code, &
			ls_location_group_system, &
			ls_location_group_groupid, &
			ls_location_type, &
			ls_product_option, &
			ls_file_no, &
			ls_product_group_system, &
			ls_product_group_groupid, &
			ls_display_option, &
			ls_product_status, &
			ls_string, &
			ls_stat_desc, &
			ls_checked_boxes, &
			ls_location_group_desc, &
			ls_product_group_desc, &
			ls_userid

DataWindowChild	ldwc_temp		

Long ll_findrow, ll_count
			
dw_weekly_daily.AcceptText()
dw_display_option.AcceptText()
dw_plant_option.AcceptText()
dw_plant.AcceptText()
dw_plant_type.AcceptText()
dw_product_option.AcceptText()
dw_product_file.AcceptText()
dw_division.AcceptText()
dw_product_state.AcceptText()
dw_product_status.AcceptText()
dw_begin_date.AcceptText()

if IsValid(w_prod_status_popup) Then Close(w_prod_status_popup)

ls_weekly_daily_option = dw_weekly_daily.GetItemString(1, "weekly_daily_option") 
iw_parentwindow.Event ue_set_data('weekly_daily_option', ls_weekly_daily_option)

If ls_weekly_daily_option = 'W' Then
	iw_parentwindow.Event ue_set_data('weekly_daily_option_desc', "Weekly")
Else
	iw_parentwindow.Event ue_set_data('weekly_daily_option_desc', "Daily")
End If

ls_display_option = dw_display_option.GetItemString(1, "boxes_loads")

iw_parentwindow.Event ue_set_data('display_option', ls_display_option)

ls_plant_option = dw_plant_option.GetItemString(1, "plant_option")
iw_parentwindow.Event ue_set_data('plant_option', ls_plant_option)

Choose Case ls_plant_option
	Case 'A'
		ls_plant_code = '   '
		ls_location_group_system = '     '
		ls_location_group_groupid = '    '
		ls_location_type = '            '
		iw_parentwindow.Event ue_set_data('plant_option_desc', "Plant:")
		iw_parentwindow.Event ue_set_data('plant_option_text', "All")		
	Case 'C'
		ls_plant_code = dw_plant.GetItemString(1, "location_code")
		ls_location_group_system = '     '
		ls_location_group_groupid = '    '
		ls_location_type = '            '
		iw_parentwindow.Event ue_set_data('plant_option_desc', "Plant Code:")
		iw_parentwindow.Event ue_set_data('plant_option_text', dw_plant.GetItemString(1, "location_code"))		
	Case 'G'
		ls_plant_code = '   '
//		ls_location_group_system = string(ole_location_group.object.systemname())
//		ls_location_group_groupid = string(ole_location_group.object.groupID())
		ls_location_group_system = uo_locations.uf_get_owner()
		ls_location_group_groupid = string(uo_locations.uf_get_sel_id(ls_location_group_groupid))	
		ls_location_type = '            '
		iw_parentwindow.Event ue_set_data('plant_option_desc', "Plant Group:")
		iw_parentwindow.Event ue_set_data('plant_option_text', uo_locations.uf_get_desc_only())			
	Case 'T'	
		ls_plant_code = '   '
		ls_location_group_system = '     '
		ls_location_group_groupid = '    '
		ls_location_type = dw_plant_type.GetItemString(1, "plant_type")
		iw_parentwindow.Event ue_set_data('plant_option_desc', "Plant Type:")
		dw_plant_type.GetChild('plant_type', ldwc_temp)
		ll_findrow = ldwc_temp.Find('type_short_desc = "' + ls_location_type + '"', 1, ldwc_temp.RowCount())
		iw_parentwindow.Event ue_set_data('plant_option_text', ldwc_temp.GetItemString(ll_findrow, 'type_desc'))			
End Choose

iw_parentwindow.Event ue_set_data('plant_code', ls_plant_code)
iw_parentwindow.Event ue_set_data('location_group_system', ls_location_group_system)
iw_parentwindow.Event ue_set_data('location_group_groupid', ls_location_group_groupid)
iw_parentwindow.Event ue_set_data('location_type', ls_location_type)

ls_product_option = dw_product_option.GetItemString(1, "product_option")
iw_parentwindow.Event ue_set_data('product_option', ls_product_option)

Choose Case ls_product_option
	Case 'A'
		ls_file_no = '  '
		ls_product_group_system = '     '
		ls_product_group_groupid = '    '
		iw_parentwindow.Event ue_set_data('product_option_desc', "Product:")
		iw_parentwindow.Event ue_set_data('product_option_text', "All")		
	Case 'F'
		ls_file_no = dw_product_file.GetItemString(1, "file_no")		
		ls_userid = dw_userid_list.GetItemString(1, "userid")
		ls_product_group_system = '     '
		ls_product_group_groupid = '    '		
		iw_parentwindow.Event ue_set_data('product_option_desc', "Product File:")
		iw_parentwindow.Event ue_set_data('product_option_text', &
			trim(ls_userid) + ': ' +  &
			ls_file_no + ' ' + dw_product_file.GetItemString(1, "file_desc"))		
	Case 'G'
		ls_file_no = '  '		
//		ls_product_group_system = string(ole_product_group.object.systemname())
//		ls_product_group_groupid = string(ole_product_group.object.groupID())    
		ls_product_group_system = uo_products.uf_get_owner()
		ls_product_group_groupid = string(uo_products.uf_get_sel_id(ls_product_group_groupid))
		iw_parentwindow.Event ue_set_data('product_option_desc', "Product Group:")
		iw_parentwindow.Event ue_set_data('product_option_text', uo_products.uf_get_desc_only())	
End Choose

iw_parentwindow.Event ue_set_data('file_no', ls_file_no)
iw_parentwindow.Event ue_set_data('product_group_system', ls_product_group_system)
iw_parentwindow.Event ue_set_data('product_group_groupid', ls_product_group_groupid)

If cbx_division.Checked = True Then
	iw_parentwindow.Event ue_set_data('division_code', dw_division.GetItemString(1, "division_code"))
	iw_parentwindow.Event ue_set_data('division_description', dw_division.GetItemString(1, "division_description"))
Else	
	iw_parentwindow.Event ue_set_data('division_code', '  ')
	iw_parentwindow.Event ue_set_data('division_description', '')
End If

If cbx_product_state.Checked = True Then
	iw_parentwindow.Event ue_set_data('product_state', dw_product_state.uf_get_product_state())
	iw_parentwindow.Event ue_set_data('product_state_desc', dw_product_state.uf_get_product_state_desc())
Else
	iw_parentwindow.Event ue_set_data('product_state', ' ')
	iw_parentwindow.Event ue_set_data('product_state_desc', '')
End If
	
If cbx_product_status.Checked = True Then

	for ll_count = 1 to il_selected_status_max
		ls_product_status = dw_product_status.GetItemString(1, "product_status" + string(ll_count))
		ls_product_status = TRIM(ls_product_status)
		if isnull(ls_product_status) Then
			ls_product_status = " "
		end if
	//	if ls_product_status = 'ALL' then
	//		ls_product_status = 'A'
	//	end if 
		ls_string += ls_product_status + "~t"
		ls_stat_desc = dw_product_status.GetItemString(1, "status_desc" + string(ll_count))
		ls_stat_desc = TRIM( ls_stat_desc)
		if isnull( ls_stat_desc) Then
			ls_stat_desc = " "
		end if
		ls_string +=  ls_stat_desc + "~t"
	next
	
	dw_product_status.AcceptText()
	
	iw_parentwindow.Event ue_set_data('dw_product_status', ls_string)	
Else
	iw_parentwindow.Event ue_set_data('dw_product_status', 'A' + '~t' + 'ALL' + '~t' + &
		'' + + '~t' + '' + '~t' + &
		'' + + '~t' + '' + '~t' + &
		'' + + '~t' + '' + '~t' + &
		'' + + '~t' + '' + '~t' + &
		'' + + '~t' + '' + '~t' + &
		'' + + '~t' + '' + '~t' + &
		'' + + '~t' + '' + '~t')
		
End If

If dw_begin_date.GetItemDate(1, "begin_date") < Today() Then
	MessageBox("Begin Date Error", "Begin Date must be current date or greater")
	Return
End If

iw_parentwindow.Event ue_set_data('begin_date', String(dw_begin_date.GetItemDate(1, "begin_date")))

ls_checked_boxes = ''
If cbx_division.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

If cbx_product_state.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

If cbx_product_status.Checked = True Then
	ls_checked_boxes += 'Y'
Else
	ls_checked_boxes += 'N'
End If

iw_parentwindow.Event ue_set_data('check_boxes', ls_checked_boxes)

ib_ok_to_close = True

Close(This)
        

end event

event ue_postopen;String		ls_plant_option, &
			ls_product_option, &
			ls_file_no, &
			ls_product_status, &
			ls_check_boxes, &
			ls_LastPAReportuserid, &
			ls_find_string, &
			ls_username, &
			ls_plant_code
			
Integer	li_found			
			
Long		ll_row

DataWindowChild	ldwc_temp

This.Title = 'PA Report Inquire'

DataWindowChild	ldddw_child

iw_parentwindow.Event ue_get_data('weekly_daily_option')
dw_weekly_daily.SetItem(1, "weekly_daily_option", Message.StringParm)

iw_parentwindow.Event ue_get_data('display_option')
dw_display_option.SetItem(1, "boxes_loads", Message.StringParm)

iw_parentwindow.Event ue_get_data('plant_option')
ls_plant_option = Message.StringParm
dw_plant_option.SetItem(1, "plant_option", ls_plant_option)

iw_parentwindow.Event ue_get_data('plant_code')
ls_plant_code = Message.StringParm
if ls_plant_code <> '1'  then
    dw_plant.uf_set_plant_code(ls_plant_code)
end if
	

Choose Case ls_plant_option
	Case 'A'
		st_location_group.Visible = False
//		ole_location_group.Visible = False
		uo_locations.Visible = False
		dw_plant.Visible = False
		dw_plant_type.Visible = False	
	Case 'C'
		st_location_group.Visible = False
//		ole_location_group.Visible = False
		uo_locations.Visible = False
		dw_plant.Visible = True
		dw_plant_type.Visible = False		
	Case 'G'
		st_location_group.Visible = True
//		ole_location_group.Visible = True
		uo_locations.Visible = True
		dw_plant.Visible = False
		dw_plant_type.Visible = False		
	Case 'T'
		st_location_group.Visible = False
//		ole_location_group.Visible = False
		uo_locations.Visible = False
		dw_plant.Visible = False
		dw_plant_type.Visible = True	
End Choose

iw_parentwindow.Event ue_get_data('location_type')
dw_plant_type.SetItem(1,"plant_type", Message.StringParm)

//ole_location_group.object.GroupType(1)
//ole_location_group.object.LoadObject()
uo_locations.uf_load_groups('L')

//ole_product_group.object.GroupType(3)
//ole_product_group.object.LoadObject()
uo_products.uf_load_groups('P')

iw_parentwindow.Event ue_get_data('product_option')
ls_product_option = Message.StringParm
dw_product_option.SetItem(1, "product_option", ls_product_option)

Choose Case ls_product_option
	Case 'A'
		st_product_group.Visible = False
//		ole_product_group.Visible = False
		uo_products.Visible = False
		dw_product_file.Visible = False
		dw_userid_list.Visible = False
	Case 'F'
		st_product_group.Visible = False
//		ole_product_group.Visible = False
		uo_products.Visible = False
		dw_product_file.Visible = True		
		dw_userid_list.Visible = True
	Case 'G'
		st_product_group.Visible = True
//		ole_product_group.Visible = True
		uo_products.Visible = True
		dw_product_file.Visible = False		
		dw_userid_list.Visible = False
End Choose	

wf_load_userids()
ls_LastPAReportuserid = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastPAReportuserid", "") 

If ls_LastPAReportuserid > ' ' Then
	dw_userid_list.SetItem(1, "userid", ls_LastPAReportuserid)
Else
	dw_userid_list.SetItem(1, "userid", SQLCA.userid)
End If

dw_userid_list.GetChild('userid', ldwc_temp)
ls_find_string = "userid = '"  + dw_userid_list.GetItemString(1, "userid") + "'"
li_found = ldwc_temp.Find(ls_find_string, 1, ldwc_temp.RowCount())
ls_username = ldwc_temp.GetItemString(li_found, "userid_name")
dw_userid_list.SetItem(1, "userid_name", ls_username)

wf_load_product_file(dw_userid_list.GetItemString(1, "userid"))

iw_parentwindow.Event ue_get_data('product_state')
dw_product_state.SetItem(1, "product_state", Message.StringParm)

iw_parentwindow.Event ue_get_data('file_no')
ls_file_no = Message.StringParm
If ls_file_no > '  ' Then
	dw_product_file.SetItem(1,"file_no", ls_file_no)
	dw_product_file.GetChild("file_no", ldddw_child)
	ll_row = ldddw_child.Find('file_no = "' + ls_file_no + '"', 1, ldddw_child.RowCount())
	dw_product_file.SetItem(1, 'file_desc', ldddw_child.GetItemString(ll_row, 'file_desc'))	
Else
	dw_product_file.GetChild("file_no", ldddw_child)
	If ldddw_child.RowCount() > 0 Then
		dw_product_file.SetItem(1, "file_no", ldddw_child.GetItemString(1, "file_no"))
		dw_product_file.SetItem(1, "file_desc", ldddw_child.GetItemString(1, "file_desc"))		
	End If
End If

ls_product_status = iw_parentwindow2.wf_get_selected_status()
is_selected_status_array = iw_parentwindow2.wf_get_selected_status()

if is_selected_status_array = ' ' Then
   ls_product_status = 'ALL'
   dw_product_status.SetItem(1,'status_desc1',ls_product_status)
   dw_product_status.InsertRow(0)
else 
	ls_product_status = is_selected_status_array
	dw_product_status.ImportString(is_selected_status_array)
	dw_product_Status.Accepttext()
end if 

iw_parentwindow.Event ue_get_data('check_boxes')
ls_check_boxes = Message.StringParm

If mid(ls_check_boxes,1,1) = 'Y' Then
	cbx_division.Checked = True
Else
	cbx_division.Checked = False
End If

If mid(ls_check_boxes,2,1) = 'Y' Then
	cbx_product_state.Checked = True
Else
	cbx_product_state.Checked = False
End If

If mid(ls_check_boxes,3,1) = 'Y' Then
	cbx_product_status.Checked = True
Else
	cbx_product_status.Checked = False
End If

iw_parentwindow.Event ue_get_data('begin_date')
dw_begin_date.SetItem(1, "begin_date", Date(Message.StringParm))






end event

event ue_get_data;call super::ue_get_data;//1-13  jac	
STRING	ls_return_value

Choose Case as_value
   case "product_status"
		ls_return_value = ''
	case "selected"
		ls_return_value = is_selected_status_array
	case "dddwxpos"
		ls_return_value = string(ii_dddw_x)
	case "dddwypos"
		ls_return_value = string(ii_dddw_y)
end choose

return ls_return_value
//
end event

event ue_set_data;call super::ue_set_data;//1-13 jac

choose case as_data_item
	case "selected"
		is_selected_status_array = as_value
		////test
//messagebox("ue_set_data", as_value)
	case "modified"
		if as_value = 'true' then
			ib_modified = true
		else
			ib_modified = false
		end if
end choose
//
end event

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_pa_report_inq
integer x = 2392
integer y = 2288
integer taborder = 180
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_pa_report_inq
integer x = 1189
integer y = 2496
integer taborder = 170
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_pa_report_inq
integer x = 903
integer y = 2496
integer taborder = 160
fontcharset fontcharset = defaultcharset!
end type

type dw_product_state from u_product_state within w_pas_pa_report_inq
event ue_postconstructor ( )
integer x = 311
integer y = 2093
integer width = 929
integer height = 74
integer taborder = 150
boolean bringtotop = true
end type

event ue_postconstructor();This.uf_Enable(True)
This.SetItem(1,'product_state', '1')
end event

event constructor;call super::constructor;This.PostEvent('ue_postconstructor')
end event

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type dw_plant from u_plant within w_pas_pa_report_inq
integer x = 622
integer y = 320
integer width = 1503
integer height = 93
integer taborder = 60
boolean bringtotop = true
end type

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type st_location_group from statictext within w_pas_pa_report_inq
integer x = 644
integer y = 448
integer width = 358
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Location Group"
boolean focusrectangle = false
end type

type dw_division from u_division within w_pas_pa_report_inq
integer x = 439
integer y = 2000
integer height = 90
integer taborder = 140
boolean bringtotop = true
end type

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type st_product_group from statictext within w_pas_pa_report_inq
integer x = 614
integer y = 1421
integer width = 344
integer height = 51
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Product Group"
boolean focusrectangle = false
end type

type dw_display_option from u_loads_boxes within w_pas_pa_report_inq
integer x = 622
integer width = 402
integer height = 288
integer taborder = 40
boolean bringtotop = true
end type

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type dw_plant_option from u_base_dw_ext within w_pas_pa_report_inq
integer y = 288
integer width = 475
integer height = 416
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_pa_rpt_plant_option"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;Choose Case data
	Case 'A'
		st_location_group.Visible = False
//		ole_location_group.Visible = False
		uo_locations.Visible = False
		dw_plant.Visible = False
		dw_plant_type.Visible = False	
	Case 'C'
		st_location_group.Visible = False
//		ole_location_group.Visible = False
		uo_locations.Visible = False
		dw_plant.Visible = True
		dw_plant_type.Visible = False		
	Case 'G'
		st_location_group.Visible = True
//		ole_location_group.Visible = True
		uo_locations.Visible = True
		dw_plant.Visible = False
		dw_plant_type.Visible = False		
	Case 'T'
		st_location_group.Visible = False
//		ole_location_group.Visible = False
		uo_locations.Visible = False
		dw_plant.Visible = False
		dw_plant_type.Visible = True		
End Choose	

end event

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type dw_plant_type from u_base_dw_ext within w_pas_pa_report_inq
integer x = 731
integer y = 992
integer width = 1317
integer height = 64
integer taborder = 90
boolean bringtotop = true
string dataobject = "d_pa_rpt_plant_type"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild		ldwc_type

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild("plant_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PLTGROUP")

end event

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type dw_product_option from u_base_dw_ext within w_pas_pa_report_inq
integer y = 1085
integer width = 512
integer height = 384
integer taborder = 100
boolean bringtotop = true
string dataobject = "d_pa_rpt_product_option"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;Choose Case data
	Case 'A'
		st_product_group.Visible = False
//		ole_product_group.Visible = False
		uo_products.Visible = False
		dw_product_file.Visible = False
		dw_userid_list.Visible = False
	Case 'F'
		st_product_group.Visible = False
//		ole_product_group.Visible = False
		uo_products.Visible = False
		dw_product_file.Visible = True	
		dw_userid_list.Visible = True
	Case 'G'
		st_product_group.Visible = True
//		ole_product_group.Visible = True
		uo_products.Visible = True
		dw_product_file.Visible = False	
		dw_userid_list.Visible = False
End Choose	

end event

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type dw_weekly_daily from u_base_dw_ext within w_pas_pa_report_inq
integer width = 475
integer height = 256
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_pa_rpt_weekly_daily"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type dw_product_file from u_base_dw_ext within w_pas_pa_report_inq
integer x = 658
integer y = 1264
integer width = 1390
integer height = 96
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_pa_rpt_product_file"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;Long	ll_row

DataWindowChild	ldddw_child

This.GetChild("file_no", ldddw_child)

// Find the entered file number in the table -- 
ll_row = ldddw_child.Find('file_no = "' + Trim(data) + '"', 1, ldddw_child.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(data + " is an Invalid File Number")
	This.SetFocus()
	This.SelectText(1, Len(data))
	return 1
Else
	This.SetItem(1, 'file_desc', ldddw_child.GetItemString(ll_row, 'file_desc'))
	This.SetFocus()
	This.SelectText(1, Len(data))
	//SetProfileString( iw_frame.is_UserINI, "Pas", "Lastplant",Trim(data))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

return 
end event

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event

type cbx_division from checkbox within w_pas_pa_report_inq
integer x = 73
integer y = 2000
integer width = 88
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

event clicked;Close(w_prod_status_popup)
ib_open = false
end event

type cbx_product_state from checkbox within w_pas_pa_report_inq
integer x = 73
integer y = 2096
integer width = 88
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

event clicked;Close(w_prod_status_popup)
ib_open = false
end event

type cbx_product_status from checkbox within w_pas_pa_report_inq
integer x = 73
integer y = 2192
integer width = 88
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

event clicked;Close(w_prod_status_popup)
ib_open = false
end event

type dw_product_status from u_base_dw_ext within w_pas_pa_report_inq
integer x = 241
integer y = 2176
integer width = 1024
integer height = 64
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_product_status_2"
richtexttoolbaractivation richtexttoolbaractivation = richtexttoolbaractivationalways!
boolean border = false
boolean ib_scrollsetscurrentrow = true
end type

event constructor;call super::constructor;//If This.RowCount() = 0 Then This.InsertRow(0)
end event

event clicked;call super::clicked;is_columnname = dwo.name



choose case dwo.name
	case 'status_desc1'
//		if (first_time=1) then
		if ib_open = true then
			Close(w_prod_status_popup)
			first_time=0
			ib_open = false
		else
			ib_open = true
			wf_get_selected_status(This)
			OpenWithParm( w_prod_status_popup, parent)
			first_time=1
		end if
end choose

end event

type dw_begin_date from u_base_dw_ext within w_pas_pa_report_inq
integer x = 307
integer y = 2320
integer width = 731
integer height = 96
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_pa_rpt_begin_date"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
This.SetItem(1, "begin_date", Today())
end event

type uo_locations from u_group_list within w_pas_pa_report_inq
event destroy ( )
integer x = 1046
integer y = 413
integer width = 1306
integer height = 573
integer taborder = 80
boolean bringtotop = true
end type

on uo_locations.destroy
call u_group_list::destroy
end on

type uo_products from u_group_list within w_pas_pa_report_inq
event destroy ( )
integer x = 1039
integer y = 1418
integer width = 1306
integer height = 573
integer taborder = 130
boolean bringtotop = true
end type

on uo_products.destroy
call u_group_list::destroy
end on

type dw_userid_list from u_base_dw_ext within w_pas_pa_report_inq
integer x = 589
integer y = 1149
integer width = 1503
integer height = 90
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_pa_userid_list"
boolean border = false
end type

event itemchanged;call super::itemchanged;String					ls_find_string, ls_username

Integer				li_found

DataWindowChild	ldwc_temp

dw_userid_list.GetChild('userid', ldwc_temp)
ls_find_string = "userid = '"  + data + "'"
li_found = ldwc_temp.Find(ls_find_string, 1, ldwc_temp.RowCount())
ls_username = ldwc_temp.GetItemString(li_found, "userid_name")
dw_userid_list.SetItem(1, "userid_name", ls_username)

wf_load_product_file(data)

SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastPAReportuserid", data)



end event

event getfocus;call super::getfocus;If This.GetColumnName() = "userid" Then
	This.SelectText(1, Len(This.GetText()))
End if
end event

event clicked;call super::clicked;Close(w_prod_status_popup)
ib_open = false
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
03w_pas_pa_report_inq.bin 
2A00000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000dd05481001d2bd0600000003000001000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000dd05481001d2bd06dd05481001d2bd0600000000000000000000000000000001fffffffe00000003fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
2Cffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000200000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
2Dffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000dd05481001d2bd0600000003000001000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000dd05481001d2bd06dd05481001d2bd0600000000000000000000000000000001fffffffe00000003fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000200000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff000000000000000000000000
2D0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
13w_pas_pa_report_inq.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
