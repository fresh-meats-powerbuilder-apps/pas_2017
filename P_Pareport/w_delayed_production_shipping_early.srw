HA$PBExportHeader$w_delayed_production_shipping_early.srw
forward
global type w_delayed_production_shipping_early from w_base_sheet_ext
end type
type dw_prod_group_info_1 from u_owner_group_info within w_delayed_production_shipping_early
end type
type dw_division from u_division within w_delayed_production_shipping_early
end type
type dw_detail from u_base_dw_ext within w_delayed_production_shipping_early
end type
type dw_plant from u_plant within w_delayed_production_shipping_early
end type
type dw_begin_end_dates from u_begin_end_dates within w_delayed_production_shipping_early
end type
end forward

global type w_delayed_production_shipping_early from w_base_sheet_ext
integer width = 2958
integer height = 1876
string title = "Delayed Production Shipping Early"
long backcolor = 67108864
dw_prod_group_info_1 dw_prod_group_info_1
dw_division dw_division
dw_detail dw_detail
dw_plant dw_plant
dw_begin_end_dates dw_begin_end_dates
end type
global w_delayed_production_shipping_early w_delayed_production_shipping_early

type variables
s_error                  istr_error_info

u_pas203		iu_pas203
u_ws_pas3		iu_ws_pas3

DataWindowChild	idwc_plant_code

DataStore	ids_print

integer		ii_PARange

String		is_header_string, &
				is_product_group_id, &
				is_product_group_desc, &
				is_product_owner, &
				is_inquire_options

end variables

forward prototypes
public subroutine wf_print ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_print ();

end subroutine

public function boolean wf_retrieve ();Long	ll_rec_count

String	ls_header, &
			ls_detail, &
			ls_options, &
			ls_division, &  
			ls_division_desc, &
			ls_date, &
			ls_product_owner,&
			ls_product_groups

Date	ld_temp1, &
		ld_temp2
		
u_string_functions	lu_string

IF Not Super::wf_retrieve() Then Return False

SetPointer(HourGlass!)

dw_detail.Reset()

iw_frame.SetMicroHelp("Wait.. Inquiring Database")
ls_division 		= dw_division.uf_get_division()
ls_division_desc 	= dw_division.uf_get_division_descr()

//Somehow occasionally getting a date in division, so verify.
//If not a valid division, set division to '11'.

// Establish the connection if not already connected
CONNECT USING SQLCA;	
SELECT tutltypes.type_code  
  INTO :ls_division 
  FROM tutltypes  
  WHERE ( tutltypes.record_type = 'DIVCODE ' ) AND  
         ( tutltypes.type_code  = :ls_division)   ;

If SQLCA.SQLCode = 100 Then
	dw_division.uf_set_division('11')
	ls_division 		= dw_division.uf_get_division()
	ls_division_desc 	= dw_division.uf_get_division_descr()
End If

ls_product_owner 	= is_product_owner
ls_product_groups = Trim(lu_string.nf_globalreplace(is_product_group_id,'~r~n',' '))
ls_options		  	= is_inquire_options


ls_header = dw_plant.uf_get_plant_code() + '~t' + &
				String(dw_begin_end_dates.uf_get_begin_date(), 'yyyy-mm-dd') + '~t' + &
				String(dw_begin_end_dates.uf_get_end_date(), 'yyyy-mm-dd')+ '~t' + &
				ls_division + '~t' + &
				ls_options + '~t' + &
				ls_product_groups + '~r~n'
				
				
istr_error_info.se_event_name = "wf_retrieve"

//If iu_pas203.nf_pasp70br_inq_delay_prd_shp_early(istr_error_info, &
//													ls_header, &
//													ls_detail) <> 0 Then 
If iu_ws_pas3.uf_pasp70fr(istr_error_info, &
									ls_header, &
									ls_detail) <> 0 Then 
	SetRedraw(True)
	Return False
End If

ll_rec_count = dw_detail.ImportString(ls_detail)

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

		
ld_temp1 = dw_begin_end_dates.uf_get_begin_date()
ld_temp2 = dw_begin_end_dates.uf_get_end_date()

ids_print.Modify("plant_t.Text = 'Plant:  " + dw_plant.uf_get_plant_code() + " " + &
			dw_plant.uf_get_plant_descr() + "' " + &
			"begin_date_t.Text = 'Begin Date:  " + &
			String(ld_temp1) + "' " + &
			"end_date_t.Text = 'End Date:  " + &
			String(ld_temp2) + "'")

return True
end function

event ue_postopen;call super::ue_postopen;String ls_PARange

iu_pas203 = create u_pas203
iu_ws_pas3 = create u_ws_pas3

istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "dlayprod"
istr_error_info.se_user_id 		= sqlca.userid

is_inquire_window_name = 'w_plant_date_inq'

ids_print = Create u_print_datastore

ids_print.DataObject = 'd_delayed_production_shipping_early_prt'

dw_detail.ShareData(ids_print)

This.PostEvent("ue_query")
end event

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

IF IsValid( iu_ws_pas3 ) THEN
	DESTROY iu_ws_pas3
END IF
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

on w_delayed_production_shipping_early.create
int iCurrent
call super::create
this.dw_prod_group_info_1=create dw_prod_group_info_1
this.dw_division=create dw_division
this.dw_detail=create dw_detail
this.dw_plant=create dw_plant
this.dw_begin_end_dates=create dw_begin_end_dates
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prod_group_info_1
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_detail
this.Control[iCurrent+4]=this.dw_plant
this.Control[iCurrent+5]=this.dw_begin_end_dates
end on

on w_delayed_production_shipping_early.destroy
call super::destroy
destroy(this.dw_prod_group_info_1)
destroy(this.dw_division)
destroy(this.dw_detail)
destroy(this.dw_plant)
destroy(this.dw_begin_end_dates)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
end event

event ue_get_data;call super::ue_get_data;Date		ldt_begin_date, &
			ldt_end_date

Choose Case as_value
	Case 'plant'
		Message.StringParm = dw_plant.uf_get_plant_code()
	Case 'begin_date'
		ldt_begin_date = dw_begin_end_dates.uf_get_begin_date()
		Message.StringParm = String(ldt_begin_date, 'yyyy-mm-dd')
	Case 'end_date'
		ldt_end_date = dw_begin_end_dates.uf_get_end_date()
		Message.StringParm = String(ldt_end_date, 'yyyy-mm-dd')
	Case 'division'
		message.StringParm = dw_division.uf_get_division()
	Case 'div_visible'
		message.StringParm = dw_division.uf_get_division()
	Case 'product_group_id'
		message.StringParm = is_product_group_id
	Case 'product_owner'
		message.StringParm = is_product_owner
	Case 'inquire_options'
		message.StringParm = is_inquire_options	
End Choose
end event

event ue_set_data;call super::ue_set_data;u_conversion_functions		lu_conv

Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'begin_date'
		dw_begin_end_dates.uf_set_begin_date(Date(as_value))
	Case 'end_date'
		dw_begin_end_dates.uf_set_end_date(Date(as_value))
	Case 'division'
		dw_division.uf_set_division(as_value)
	Case 'div_visible'
		If as_value = 'False' Then
			dw_division.Visible = False
		else	
			dw_division.Visible = True
		End If
	Case 'prod_group_visible'
		If as_value = 'False' Then
			dw_prod_group_info_1.Visible = False
		else	
			dw_prod_group_info_1.Visible = True
		End If
	Case 'product_group_desc'
		is_product_group_desc = as_value
		dw_prod_group_info_1.SetItem(1,'prod_group_info',as_value)
	Case 'product_group_id'
		is_product_group_id = as_value
	Case 'product_owner'
		is_product_owner = as_value
		dw_prod_group_info_1.Object.owner_t.Text = is_product_owner
	Case 'inquire_options'
		is_inquire_options = as_value
	Case 'ib_inquire'
		ib_inquire = lu_conv.nf_Boolean(as_value)	
	End Choose
end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return

ids_print.Print()
end event

event resize;call super::resize;integer li_x		= 28
integer li_y		= 277

dw_detail.width	= width - (50 + li_x)
dw_detail.height	= height - (125 + li_y)


end event

type dw_prod_group_info_1 from u_owner_group_info within w_delayed_production_shipping_early
integer x = 9
integer y = 336
integer width = 2587
integer taborder = 30
boolean controlmenu = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

type dw_division from u_division within w_delayed_production_shipping_early
integer x = 78
integer y = 252
integer taborder = 10
end type

type dw_detail from u_base_dw_ext within w_delayed_production_shipping_early
integer y = 504
integer width = 1829
integer height = 1100
integer taborder = 50
string dataobject = "d_delayed_production_shipping_early"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;ib_Updateable = False

end event

type dw_plant from u_plant within w_delayed_production_shipping_early
integer x = 114
integer y = 8
integer taborder = 40
end type

event constructor;call super::constructor;This.Disable()
end event

type dw_begin_end_dates from u_begin_end_dates within w_delayed_production_shipping_early
integer x = 5
integer y = 84
integer height = 164
integer taborder = 20
end type

event constructor;call super::constructor;Date				ldt_end_date
Integer			li_PARange

This.InsertRow(0)
This.uf_Enable(False)

This.uf_set_date_type('PARANGE')

This.uf_set_begin_date(Today())

li_PARange = This.uf_get_pa_range()
ldt_end_date = RelativeDate(Today(),li_PARange)
This.uf_set_end_date(ldt_end_date)

end event

