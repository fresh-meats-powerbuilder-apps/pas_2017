HA$PBExportHeader$w_auto_transfer_inq.srw
forward
global type w_auto_transfer_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_auto_transfer_inq
end type
end forward

global type w_auto_transfer_inq from w_base_response_ext
integer width = 1595
integer height = 436
string title = "Auto Transfer Inquire"
long backcolor = 67108864
dw_plant dw_plant
end type
global w_auto_transfer_inq w_auto_transfer_inq

type variables
w_auto_transfer	iw_Parent_Window

end variables

event open;call super::open;iw_parent_window = Message.PowerObjectParm
If Not IsValid(iw_parent_window) Then 
	Close(This)
	return
End if

This.Title = iw_parent_window.Title + " Inquire"
end event

event ue_postopen;call super::ue_postopen;String	ls_temp


ls_temp = iw_parent_window.dw_plant.uf_Get_Plant_Code()
If Not iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.Reset()
	dw_plant.ImportString(ls_temp + '~t' + iw_parent_window.dw_plant.uf_Get_Plant_Descr() )
End if
end event

on w_auto_transfer_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
end on

on w_auto_transfer_inq.destroy
call super::destroy
destroy(this.dw_plant)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "N")
end event

event ue_base_ok;call super::ue_base_ok;IF (dw_plant.AcceptText() = -1) THEN return

If (iw_frame.iu_string.nf_IsEmpty(dw_plant.uf_get_plant_code())) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	Return
End If

IF iw_parent_window.dw_plant.uf_Set_Plant_Code( &
		dw_plant.uf_get_plant_code()) <> 0 THEN Return
		
CloseWithReturn(This, "Y")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_auto_transfer_inq
integer x = 1065
integer y = 172
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_auto_transfer_inq
integer x = 663
integer y = 172
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_auto_transfer_inq
integer x = 261
integer y = 172
integer taborder = 20
end type

type dw_plant from u_plant within w_auto_transfer_inq
integer x = 82
integer y = 48
integer width = 1463
integer taborder = 10
end type

