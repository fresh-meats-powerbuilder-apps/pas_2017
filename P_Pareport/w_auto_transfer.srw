HA$PBExportHeader$w_auto_transfer.srw
forward
global type w_auto_transfer from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_auto_transfer
end type
type dw_plant from u_plant within w_auto_transfer
end type
end forward

global type w_auto_transfer from w_base_sheet_ext
integer x = 165
integer y = 288
integer width = 2428
integer height = 1309
string title = "Auto Transfer List"
long backcolor = 67108864
dw_detail dw_detail
dw_plant dw_plant
end type
global w_auto_transfer w_auto_transfer

type variables
Private:
s_error     istr_error_info
u_pas203		iu_pas203
u_ws_pas3		iu_ws_pas3
end variables

forward prototypes
public subroutine wf_print ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_print ();
end subroutine

public function boolean wf_retrieve ();Long	ll_rec_count

String	ls_plant_code

Call w_base_sheet::closequery
IF Message.ReturnValue = 1 THEN Return False

OpenWithParm(w_auto_transfer_inq, This)
If Message.StringParm <> 'Y' Then return False

ls_plant_code = dw_plant.uf_Get_plant_code()
dw_detail.Reset()

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait.. Inquiring Database")
istr_error_info.se_event_name = "wf_retrieve"
dw_detail.SetRedraw(False)

//If Not iu_pas203.nf_inq_auto_tran(istr_error_info, ls_plant_code, dw_detail) Then 
If Not iu_ws_pas3.uf_pasp50fr(istr_error_info, ls_plant_code, dw_detail) Then 
	dw_detail.ResetUpdate()
	dw_detail.SetRedraw(True)
	Return False
End If

dw_detail.ResetUpdate()

ll_rec_count = dw_detail.RowCount()

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + " Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

dw_detail.SetRedraw(True)
return True
end function

event ue_postopen;call super::ue_postopen;iu_pas203 = create u_pas203
iu_ws_pas3 = create u_ws_pas3

istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "AutoTran"
istr_error_info.se_user_id 		= sqlca.userid

This.PostEvent("ue_query")
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

on w_auto_transfer.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_plant
end on

on w_auto_transfer.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_plant)
end on

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

IF IsValid( iu_ws_pas3 ) THEN
	DESTROY iu_ws_pas3
END IF
end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
end event

event ue_fileprint;call super::ue_fileprint;DataWindowChild	ldwc_print

DataStore			lds_print


lds_print = Create u_print_datastore
lds_print.dataobject = 'd_auto_transfer_print'

lds_print.GetChild("dw_auto_transfer",ldwc_print)

ldwc_print.Modify("product_code.Border = 0 " + &
						"shift.Border = 0 " + &
						"start_date.Border = 0 " + &
						"end_date.Border = 0 " + &
						"dest_plant_code.Border = 0 " + &
						"transfer.Border = 0");

lds_print.Modify("plant_t.Text = 'Plant:  " + &
	dw_plant.uf_Get_plant_code() + "  " + &
	dw_plant.uf_Get_Plant_Descr() + "'")

dw_detail.ShareData(ldwc_print)

lds_print.Print()

dw_detail.ShareDataOff()
end event

event resize;call super::resize;integer li_width, li_height
integer ly_height = 10

li_width = newwidth - (dw_detail.x * 2)


//li_height = newheight - (dw_detail.y + 10)
//

li_height = newheight - (dw_detail.y + ly_height)

if li_width > 0 then
                dw_detail.width = li_width
end if


if li_height > 0 then
                dw_detail.height = li_height
end if

end event

type dw_detail from u_base_dw_ext within w_auto_transfer
integer x = 37
integer y = 128
integer width = 2319
integer height = 1056
integer taborder = 20
string dataobject = "d_auto_transfer"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_plant from u_plant within w_auto_transfer
integer x = 135
integer y = 29
integer width = 1562
integer height = 90
integer taborder = 10
end type

event constructor;call super::constructor;ib_Updateable = False
ib_protected = True
TriggerEvent( "ue_insertrow" )
end event

