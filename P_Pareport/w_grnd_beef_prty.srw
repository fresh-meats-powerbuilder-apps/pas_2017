HA$PBExportHeader$w_grnd_beef_prty.srw
forward
global type w_grnd_beef_prty from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_grnd_beef_prty
end type
type dw_plant from u_plant within w_grnd_beef_prty
end type
type dw_product_print from u_base_dw_ext within w_grnd_beef_prty
end type
type dw_order_print from u_base_dw_ext within w_grnd_beef_prty
end type
type dw_load_print from u_base_dw_ext within w_grnd_beef_prty
end type
type tab_1 from tab within w_grnd_beef_prty
end type
type tabpage_1 from userobject within tab_1
end type
type dw_by_product from u_base_dw_ext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_by_product dw_by_product
end type
type tabpage_2 from userobject within tab_1
end type
type dw_by_order from u_base_dw_ext within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_by_order dw_by_order
end type
type tabpage_3 from userobject within tab_1
end type
type dw_by_load from u_base_dw_ext within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_by_load dw_by_load
end type
type tab_1 from tab within w_grnd_beef_prty
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type
end forward

global type w_grnd_beef_prty from w_base_sheet_ext
string title = "Ground Beef Priority Report"
long backcolor = 12632256
dw_header dw_header
dw_plant dw_plant
dw_product_print dw_product_print
dw_order_print dw_order_print
dw_load_print dw_load_print
tab_1 tab_1
end type
global w_grnd_beef_prty w_grnd_beef_prty

type variables
u_pas203		iu_pas203

u_ws_pas2      iu_ws_pas2

s_error		istr_error_info

DataStore	ids_grnd_beef_prty

String		is_output_data
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_set_other_products (ref datawindow adw_input)
end prototypes

public function boolean wf_retrieve ();String	ls_input, &
			ls_output, &
			ls_filter, &
			ls_exclude

Integer	li_ret, &
			li_counter, &
			li_tab
			
Long		ll_rowcount


IF Not Super::wf_retrieve() Then Return False

SetPointer(HourGlass!)

ls_input  = dw_plant.uf_get_plant_code() + "~t"
ls_input += String(dw_header.GetItemDate(1,"inquire_date"),"yyyy-mm-dd")

//li_ret = iu_pas203.nf_pasp62br_inq_grnd_beef_prty(istr_error_info, &
//														ls_input, &
//														is_output_data)
														
li_ret = iu_ws_pas2.nf_pasp62fr(ls_input, &
														is_output_data, istr_error_info)

//is_output_data = 'D0211BH	1997-06-21	03:00	70472	77171	470.000~r~n' + &
//				'D0271BH	1997-06-21	03:00	77171		35.000' + '~r~n' + &
//				'D0371BHL	1997-06-21	12:00	18268	18268	99.000' + '~r~n' + &
//				'D0371BHL	1997-06-21	03:00	73282	81977	15.000' + '~r~n' + &
//				'D0336BHL	1997-06-21	03:00	81077	81977	108.000' + '~r~n' + &
//				'D0371BHL	1997-06-20	07:00	84040	84040	140.000' + '~r~n' + &
//				'D0371BHL	1997-06-20	22:00	81826	81881	162.000' + '~r~n' + &
//				'D0331BHL	1997-06-21	03:00	73282	81977	175.000' + '~r~n' + &
//				'D0351BHL	1997-06-21	03:00	73282	81977	27.000' + '~r~n' + &
//				'D0381BHL	1997-06-21	12:00	42270	42270	530.000' + '~r~n' + &
//				'D0386BHL	1997-06-21	00:00	M7571	80993	120.000' + '~r~n' + &
//				'D0386BHL	1997-06-21	06:00	37802	79672	270.000' + '~r~n' + &
//				'D0371BHL	1997-06-20	13:00	29892	80219	108.000' + '~r~n' + &
//				'D0371BHL	1997-06-20	22:00	82812	81881	27.000' + '~r~n' + &
//				'D0336BHL	1997-06-20	07:00	84076	84040	20.000' + '~r~n' + &
//				'D0371BHL	1997-06-20	23:00	77160	83777	32.000' + '~r~n' + &
//				'D0371BHL	1997-06-21	12:00	75457	76427	60.000' + '~r~n' + &
//				'D0271BH	1997-06-21	12:00	75457	76427	5.000' + '~r~n' + &
//				'D0386BHL	1997-06-21	03:00	81077	81977	108.000' + '~r~n' + &
//				'D0331BHL	1997-06-22	00:00	83729	83729	530.000' + '~r~n' + &
//				'D0336BHL	1997-06-21	06:00	37802	79672	0.000' + '~r~n' + &
//				'D0331BHL	1997-06-20	07:00	65694	80996	17.000' + '~r~n' + &
//				'D0756BHL	1997-06-20	07:00	84076	84040	30.000' + '~r~n' 

If li_ret <> 0 Then
	return False
End If

This.SetRedraw(False)
li_tab = tab_1.selectedtab

tab_1.tabpage_1.Event ue_retrieve()
tab_1.tabpage_2.Event ue_retrieve()
tab_1.tabpage_3.Event ue_retrieve()

tab_1.selectedtab = li_tab

This.SetRedraw(True)

return true
end function

public subroutine wf_set_other_products (ref datawindow adw_input);Long			ll_rowcount, &
				ll_findrow, &
				ll_count


ll_rowcount = adw_input.RowCount()

IF ll_rowcount = 1 then return
If adw_input.Find('order_number = "' + adw_input.GetItemString &
			(1, 'order_number') + '"', 2 , ll_rowcount + 1) < 1 Then
	adw_input.SetItem(1, 'other_on_orders', '')
Else
	adw_input.SetItem(1, 'other_on_orders', '*')
End IF
If adw_input.Find('primary_number = "' + adw_input.GetItemString &
			(1, 'primary_number') + '"', 2 , ll_rowcount + 1) < 1 Then
	adw_input.SetItem(1, 'other_on_loads', '')
Else
	adw_input.SetItem(1, 'other_on_loads', '*')
End IF

For ll_count = 2 to ll_rowcount
	ll_findrow = adw_input.Find('order_number = "' + adw_input.GetItemString &
			(ll_count, 'order_number') + '"', 0 , ll_count - 1)
	If ll_findrow < 1 Then
			ll_findrow = adw_input.Find('order_number = "' + adw_input.GetItemString &
			(ll_count, 'order_number') + '"', ll_count + 1, ll_rowcount + 1)
		If ll_findrow > 0 Then
			adw_input.SetItem(ll_count, 'other_on_orders', '*')
		Else
			adw_input.SetItem(ll_count, 'other_on_orders', '')
		End IF
	Else
			adw_input.SetItem(ll_count, 'other_on_orders', '*')
	End If
	ll_findrow = adw_input.Find('primary_number = "' + adw_input.GetItemString &
			(ll_count, 'primary_number') + '"', 0 , ll_count - 1)
	If ll_findrow < 1 Then
			ll_findrow = adw_input.Find('primary_number = "' + adw_input.GetItemString &
			(ll_count, 'primary_number') + '"', ll_count + 1, ll_rowcount + 1)
		If ll_findrow > 0 Then
			adw_input.SetItem(ll_count, 'other_on_loads', '*')
		Else
			adw_input.SetItem(ll_count, 'other_on_loads', '')
		End IF
	Else
			adw_input.SetItem(ll_count, 'other_on_loads', '*')
	End If
Next

end subroutine

on w_grnd_beef_prty.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_plant=create dw_plant
this.dw_product_print=create dw_product_print
this.dw_order_print=create dw_order_print
this.dw_load_print=create dw_load_print
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_product_print
this.Control[iCurrent+4]=this.dw_order_print
this.Control[iCurrent+5]=this.dw_load_print
this.Control[iCurrent+6]=this.tab_1
end on

on w_grnd_beef_prty.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_plant)
destroy(this.dw_product_print)
destroy(this.dw_order_print)
destroy(this.dw_load_print)
destroy(this.tab_1)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_save')

end event

event close;call super::close;If IsValid(iu_pas203) Then
	Destroy iu_pas203
End If

If IsValid(iu_ws_pas2) Then
	Destroy iu_ws_pas2
End If
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'title'
		message.StringParm = This.Title
	Case 'inquire_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'inquire_date'), 'yyyy-mm-dd')
	Case 'plant_code'
		Message.StringParm = dw_plant.uf_get_plant_code()
End Choose



end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'plant_code'
		dw_plant.uf_set_plant_code(as_value)
	Case 'inquire_date'
		dw_header.SetItem(1, 'inquire_date', Date(as_value))
End Choose
end event

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_grnd_beef_prty"
istr_error_info.se_user_id 		= sqlca.userid


iu_pas203 = Create u_pas203
iu_ws_pas2 = Create u_ws_pas2
is_inquire_window_name = 'w_grnd_beef_prty_inq'

wf_retrieve()
end event

event ue_fileprint;call super::ue_fileprint;Integer		li_tab


li_tab = tab_1.selectedtab


Choose Case li_tab
	Case 1
		tab_1.tabpage_1.dw_by_product.ShareData(dw_product_print)
		dw_product_print.object.scheduled_ship_date_t.text = &
				String(dw_header.GetItemDate(1, 'inquire_date'), 'mm/dd/yyyy')
		dw_product_print.object.plant_code_t.text = &
				dw_plant.uf_get_plant_code()
		dw_product_print.object.plant_desc_t.text = &
				dw_plant.uf_get_plant_descr()
		dw_product_print.Print()
		tab_1.tabpage_1.dw_by_product.ShareDataOff()
		
	Case 2
		tab_1.tabpage_2.dw_by_order.ShareData(dw_order_print)
		dw_order_print.object.scheduled_ship_date_t.text = &
				String(dw_header.GetItemDate(1, 'inquire_date'), 'mm/dd/yyyy')
		dw_order_print.object.plant_code_t.text = &
				dw_plant.uf_get_plant_code()
		dw_order_print.object.plant_desc_t.text = &
				dw_plant.uf_get_plant_descr()
		dw_order_print.Print()
		tab_1.tabpage_2.dw_by_order.ShareDataOff()
	Case 3
		tab_1.tabpage_3.dw_by_load.ShareData(dw_load_print)
		dw_load_print.object.scheduled_ship_date_t.text = &
				String(dw_header.GetItemDate(1, 'inquire_date'), 'mm/dd/yyyy')
		dw_load_print.object.plant_code_t.text = &
				dw_plant.uf_get_plant_code()
		dw_load_print.object.plant_desc_t.text = &
				dw_plant.uf_get_plant_descr()
		dw_load_print.Print()
		tab_1.tabpage_3.dw_by_load.ShareDataOff()
End Choose


end event

event resize;call super::resize;constant integer li_tab_x		= 10
constant integer li_tab_y		= 230
  
constant integer li_dw_x		= 30
constant integer li_dw_y		= 350

  
  
tab_1.width	= newwidth - li_tab_x
tab_1.height = newheight - li_tab_y

tab_1.tabpage_1.dw_by_product.width = newwidth - li_dw_x
tab_1.tabpage_1.dw_by_product.height = newheight - li_dw_y

tab_1.tabpage_2.dw_by_order.width = newwidth - li_dw_x
tab_1.tabpage_2.dw_by_order.height = newheight - li_dw_y

tab_1.tabpage_3.dw_by_load.width = newwidth - li_dw_x
tab_1.tabpage_3.dw_by_load.height = newheight - li_dw_y


end event

type dw_header from u_base_dw_ext within w_grnd_beef_prty
integer y = 128
integer width = 859
integer height = 92
integer taborder = 10
string dataobject = "d_grnd_beef_prty_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.InsertRow(0)
This.SetItem(1, 'inquire_date', Today())

end event

event itemchanged;call super::itemchanged;Choose Case dwo.name
	Case 'report_selection'
		Choose Case data
			Case 'P' 
//				dw_grnd_beef_prty.Group( 'Product_code') Level=1))
		End Choose
End Choose
end event

type dw_plant from u_plant within w_grnd_beef_prty
integer x = 315
integer y = 16
integer width = 1472
integer taborder = 0
end type

event constructor;call super::constructor;ib_updateable = False
This.Disable()

end event

type dw_product_print from u_base_dw_ext within w_grnd_beef_prty
boolean visible = false
integer x = 654
integer y = 1092
integer width = 485
integer height = 272
boolean enabled = false
string dataobject = "d_grnd_beef_prty_by_product_prt"
boolean border = false
end type

type dw_order_print from u_base_dw_ext within w_grnd_beef_prty
boolean visible = false
integer x = 1193
integer y = 1092
integer width = 485
integer height = 272
integer taborder = 0
boolean bringtotop = true
boolean enabled = false
string dataobject = "d_grnd_beef_prty_by_order_prt"
boolean border = false
end type

type dw_load_print from u_base_dw_ext within w_grnd_beef_prty
boolean visible = false
integer x = 1801
integer y = 1092
integer width = 485
integer height = 272
integer taborder = 0
boolean bringtotop = true
boolean enabled = false
string dataobject = "d_grnd_beef_prty_by_load_prt"
boolean border = false
end type

type tab_1 from tab within w_grnd_beef_prty
integer y = 224
integer width = 2821
integer height = 1228
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
end on

event selectionchanged;Choose Case newindex
	Case 1
		This.tabpage_1.dw_by_product.Sort()
		This.tabpage_1.dw_by_product.GroupCalc()
	Case 2
		This.tabpage_2.dw_by_order.Sort()
		This.tabpage_2.dw_by_order.GroupCalc()
	Case 3
		This.tabpage_3.dw_by_load.Sort()
		This.tabpage_3.dw_by_load.GroupCalc()
End Choose
end event

type tabpage_1 from userobject within tab_1
event ue_retrieve ( )
integer x = 18
integer y = 100
integer width = 2784
integer height = 1112
long backcolor = 12632256
string text = "Order By Product"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 12632256
dw_by_product dw_by_product
end type

event ue_retrieve;Long			ll_rowcount, &
				ll_findrow, &
				ll_count


This.dw_by_product.Reset()
ll_rowcount = This.dw_by_product.ImportString(is_output_data)
If ll_rowcount > 0 Then
	wf_set_other_products(This.dw_by_product)
	This.dw_by_product.Sort()
	This.dw_by_product.GroupCalc()
	iw_frame.SetMicroHelp(String(String(ll_rowcount) + " records retrieved"))
Else
	iw_frame.SetMicroHelp(String(String(0) + " records retrieved"))
End If


end event

on tabpage_1.create
this.dw_by_product=create dw_by_product
this.Control[]={this.dw_by_product}
end on

on tabpage_1.destroy
destroy(this.dw_by_product)
end on

type dw_by_product from u_base_dw_ext within tabpage_1
integer x = -18
integer y = -4
integer width = 2784
integer height = 1108
integer taborder = 3
string dataobject = "d_grnd_beef_prty_by_product"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;ib_updateable = false

end event

type tabpage_2 from userobject within tab_1
event ue_retrieve ( )
integer x = 18
integer y = 100
integer width = 2784
integer height = 1112
long backcolor = 12632256
string text = "Order By Order Number"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_by_order dw_by_order
end type

event ue_retrieve;Long			ll_rowcount, &
				ll_findrow, &
				ll_count


This.dw_by_order.Reset()

ll_rowcount = This.dw_by_order.ImportString(is_output_data)

If ll_rowcount > 0 Then
	wf_set_other_products(This.dw_by_order)
	This.dw_by_order.Sort()
	This.dw_by_order.GroupCalc()
	iw_frame.SetMicroHelp(String(String(ll_rowcount) + " records retrieved"))
Else
	iw_frame.SetMicroHelp(String(String(0) + " records retrieved"))
End If


end event

on tabpage_2.create
this.dw_by_order=create dw_by_order
this.Control[]={this.dw_by_order}
end on

on tabpage_2.destroy
destroy(this.dw_by_order)
end on

type dw_by_order from u_base_dw_ext within tabpage_2
integer x = -18
integer y = -4
integer width = 2784
integer height = 1108
integer taborder = 2
string dataobject = "d_grnd_beef_prty_by_order"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;ib_updateable = false

end event

type tabpage_3 from userobject within tab_1
event ue_retrieve ( )
integer x = 18
integer y = 100
integer width = 2784
integer height = 1112
long backcolor = 12632256
string text = "Order By Load"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_by_load dw_by_load
end type

event ue_retrieve;Long			ll_rowcount, &
				ll_findrow, &
				ll_count


This.dw_by_load.Reset()

ll_rowcount = This.dw_by_load.ImportString(is_output_data)

If ll_rowcount > 0 Then
	wf_set_other_products(dw_by_load)
	This.dw_by_load.Sort()
	This.dw_by_load.GroupCalc()
	iw_frame.SetMicroHelp(String(String(ll_rowcount) + " records retrieved"))
Else
	iw_frame.SetMicroHelp(String(String(0) + " records retrieved"))
End If


end event

on tabpage_3.create
this.dw_by_load=create dw_by_load
this.Control[]={this.dw_by_load}
end on

on tabpage_3.destroy
destroy(this.dw_by_load)
end on

type dw_by_load from u_base_dw_ext within tabpage_3
integer width = 2784
integer height = 1108
integer taborder = 3
string dataobject = "d_grnd_beef_prty_by_load"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;ib_updateable = false

end event

