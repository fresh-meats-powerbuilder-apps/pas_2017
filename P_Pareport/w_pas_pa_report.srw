HA$PBExportHeader$w_pas_pa_report.srw
$PBExportComments$Weekly PA Window
forward
global type w_pas_pa_report from w_base_sheet_ext
end type
type cb_find_next from commandbutton within w_pas_pa_report
end type
type cbx_grade_sort from checkbox within w_pas_pa_report
end type
type dw_scroll_product from datawindow within w_pas_pa_report
end type
type dw_pa_detail from u_base_dw_ext within w_pas_pa_report
end type
type cbx_expand_all from u_base_checkbox_ext within w_pas_pa_report
end type
type dw_product_status_window from u_base_dw_ext within w_pas_pa_report
end type
type dw_header from u_base_dw_ext within w_pas_pa_report
end type
type dw_loads_boxes from u_loads_boxes within w_pas_pa_report
end type
type cbx_neg_selection from checkbox within w_pas_pa_report
end type
end forward

global type w_pas_pa_report from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 3215
integer height = 1725
string title = "Weekly PA "
long backcolor = 67108864
event ue_find_product ( )
cb_find_next cb_find_next
cbx_grade_sort cbx_grade_sort
dw_scroll_product dw_scroll_product
dw_pa_detail dw_pa_detail
cbx_expand_all cbx_expand_all
dw_product_status_window dw_product_status_window
dw_header dw_header
dw_loads_boxes dw_loads_boxes
cbx_neg_selection cbx_neg_selection
end type
global w_pas_pa_report w_pas_pa_report

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
end prototypes

type variables
//u_pas203		iu_pas203
u_ws_pas2       iu_ws_pas2

s_error		istr_error_info

Long		il_color

Long		il_SelectedColor

Long		il_SelectedTextColor, &
         il_selected_status_max = 8

Boolean	ib_reinquire, &
			ib_enable_save

Integer		ii_max_page_number
//1-13 jac
Integer	ii_dddw_x, ii_dddw_y, ii_row_count
//
string		is_selected_status_array, &
				is_check_boxes, &
				is_data, &
				is_save_file

Datastore		ids_report

w_netwise_response	iw_parentwindow

//u_orp204  	iu_orp204

u_ws_orp4      iu_ws_orp4
end variables

forward prototypes
public subroutine wf_format ()
public function string wf_get_selected_status ()
public subroutine wf_filter ()
public function boolean wf_update ()
public function integer wf_updat ()
public function boolean wf_deleterow ()
public function boolean wf_retrieve ()
public function boolean wf_find_product ()
public subroutine wf_calc_totals ()
public function integer wf_getdetaildata (ref string as_detaildata)
end prototypes

event ue_find_product();wf_find_product()
end event

public subroutine wf_format ();string	ls_temp

If  dw_header.GetItemString(1, "weekly_daily_option") = 'W' Then

	If dw_loads_boxes.GetItemString(1,"boxes_loads") = 'B' Then
		 
		ls_temp = dw_pa_detail.Modify("weekly_sum_1_comp.Format='##,##0' " + &
				"weekly_sum_2_comp.Format='##,##0' " + &
				"weekly_sum_3_comp.Format='##,##0' " + &
				"weekly_sum_4_comp.Format='##,##0' " + &
				"weekly_sum_5_comp.Format='##,##0' " + &
				"weekly_sum_6_comp.Format='##,##0' " + &
				"weekly_sum_7_comp.Format='##,##0' " + &
				"weekly_sum_8_comp.Format='##,##0' " + &
				"weekly_sum_9_comp.Format='##,##0' " + &
				"weekly_sum_10_comp.Format='##,##0' " + &
				"weekly_sum_11_comp.Format='##,##0' " + &
				"weekly_sum_12_comp.Format='##,##0' " + &
				"weekly_sum_13_comp.Format='##,##0' " + &
				"weekly_sum_14_comp.Format='##,##0' " + &
				"weekly_sum_15_comp.Format='##,##0' " + &
				"weekly_sum_16_comp.Format='##,##0' " + &
				"weekly_sum_17_comp.Format='##,##0' " + &
				"weekly_sum_18_comp.Format='##,##0' " + &
				"weekly_sum_19_comp.Format='##,##0' " + &
				"weekly_sum_20_comp.Format='##,##0' " + &
				"weekly_sum_21_comp.Format='##,##0' " + &
				"weekly_sum_22_comp.Format='##,##0' " + &
				"compute_1.Format = '##,##0' " + &
				"compute_2.Format = '##,##0' " + &
				"compute_3.Format = '##,##0' " + &
				"compute_4.Format = '##,##0' " + &
				"compute_5.Format = '##,##0' " + &
				"compute_6.Format = '##,##0' " + &
				"compute_7.Format = '##,##0' " + &
				"compute_8.Format = '##,##0' " + &
				"compute_9.Format = '##,##0' " + &
				"compute_10.Format = '##,##0' " + &
				"compute_11.Format = '##,##0' " + &
				"compute_12.Format = '##,##0' " + &
				"compute_13.Format = '##,##0' " + &
				"compute_14.Format = '##,##0' " + &
				"compute_15.Format = '##,##0' " + &
				"compute_16.Format = '##,##0' " + &
				"compute_17.Format = '##,##0' " + &
				"compute_18.Format = '##,##0' " + &
				"compute_19.Format = '##,##0' " + &
				"compute_20.Format = '##,##0' " + &
				"compute_21.Format = '##,##0' ")
	Else
		dw_pa_detail.Modify("weekly_sum_1_comp.Format='##0.00' " + &
			"weekly_sum_2_comp.Format='##0.00' " + &			
			"weekly_sum_3_comp.Format='##0.00' " + &			
			"weekly_sum_4_comp.Format='##0.00' " + &			
			"weekly_sum_5_comp.Format='##0.00' " + &			
			"weekly_sum_6_comp.Format='##0.00' " + &			
			"weekly_sum_7_comp.Format='##0.00' " + &			
			"weekly_sum_8_comp.Format='##0.00' " + &			
			"weekly_sum_9_comp.Format='##0.00' " + &			
			"weekly_sum_10_comp.Format='##0.00' " + &			
			"weekly_sum_11_comp.Format='##0.00' " + &			
			"weekly_sum_12_comp.Format='##0.00' " + &			
			"weekly_sum_13_comp.Format='##0.00' " + &			
			"weekly_sum_14_comp.Format='##0.00' " + &			
			"weekly_sum_15_comp.Format='##0.00' " + &			
			"weekly_sum_16_comp.Format='##0.00' " + &			
			"weekly_sum_17_comp.Format='##0.00' " + &			
			"weekly_sum_18_comp.Format='##0.00' " + &			
			"weekly_sum_19_comp.Format='##0.00' " + &			
			"weekly_sum_20_comp.Format='##0.00' " + &			
			"weekly_sum_21_comp.Format='##0.00' " + &
			"weekly_sum_22_comp.Format='##0.00' " + &
			"compute_1.Format = '##0.00' " + &
			"compute_2.Format = '##0.00' " + &
			"compute_3.Format = '##0.00' " + &
			"compute_4.Format = '##0.00' " + &
			"compute_5.Format = '##0.00' " + &
			"compute_6.Format = '##0.00' " + &
			"compute_7.Format = '##0.00' " + &
			"compute_8.Format = '##0.00' " + &
			"compute_9.Format = '##0.00' " + &
			"compute_10.Format = '##0.00' " + &
			"compute_11.Format = '##0.00' " + &
			"compute_12.Format = '##0.00' " + &
			"compute_13.Format = '##0.00' " + &
			"compute_14.Format = '##0.00' " + &
			"compute_15.Format = '##0.00' " + &
			"compute_16.Format = '##0.00' " + &
			"compute_17.Format = '##0.00' " + &
			"compute_18.Format = '##0.00' " + &
			"compute_19.Format = '##0.00' " + &
			"compute_20.Format = '##0.00' " + &
			"compute_21.Format = '##0.00' ")
	End If
Else		
	If dw_loads_boxes.GetItemString(1,"boxes_loads") = 'B' Then
		ls_temp = dw_pa_detail.Modify("daily_sum_1_comp.Format='##,##0' " + &
				"daily_sum_2_comp.Format='##,##0' " + &
				"daily_sum_3_comp.Format='##,##0' " + &
				"daily_sum_4_comp.Format='##,##0' " + &
				"daily_sum_5_comp.Format='##,##0' " + &
				"daily_sum_6_comp.Format='##,##0' " + &
				"daily_sum_7_comp.Format='##,##0' " + &
				"daily_sum_8_comp.Format='##,##0' " + &
				"daily_sum_9_comp.Format='##,##0' " + &
				"daily_sum_10_comp.Format='##,##0' " + &
				"daily_sum_11_comp.Format='##,##0' " + &
				"daily_sum_12_comp.Format='##,##0' " + &
				"daily_sum_13_comp.Format='##,##0' " + &
				"daily_sum_14_comp.Format='##,##0' " + &
				"daily_sum_15_comp.Format='##,##0' " + &
				"daily_sum_16_comp.Format='##,##0' " + &
				"daily_sum_17_comp.Format='##,##0' " + &
				"daily_sum_18_comp.Format='##,##0' " + &
				"daily_sum_19_comp.Format='##,##0' " + &
				"daily_sum_20_comp.Format='##,##0' " + &
				"daily_sum_21_comp.Format='##,##0' " + &
				"daily_sum_22_comp.Format='##,##0' ")
	Else
		dw_pa_detail.Modify("daily_sum_1_comp.Format='##0.00' " + &
				"daily_sum_2_comp.Format='##0.00' " + &			
				"daily_sum_3_comp.Format='##0.00' " + &			
				"daily_sum_4_comp.Format='##0.00' " + &			
				"daily_sum_5_comp.Format='##0.00' " + &			
				"daily_sum_6_comp.Format='##0.00' " + &			
				"daily_sum_7_comp.Format='##0.00' " + &			
				"daily_sum_8_comp.Format='##0.00' " + &			
				"daily_sum_9_comp.Format='##0.00' " + &			
				"daily_sum_10_comp.Format='##0.00' " + &			
				"daily_sum_11_comp.Format='##0.00' " + &			
				"daily_sum_12_comp.Format='##0.00' " + &			
				"daily_sum_13_comp.Format='##0.00' " + &			
				"daily_sum_14_comp.Format='##0.00' " + &			
				"daily_sum_15_comp.Format='##0.00' " + &			
				"daily_sum_16_comp.Format='##0.00' " + &			
				"daily_sum_17_comp.Format='##0.00' " + &			
				"daily_sum_18_comp.Format='##0.00' " + &			
				"daily_sum_19_comp.Format='##0.00' " + &			
				"daily_sum_20_comp.Format='##0.00' " + &			
				"daily_sum_21_comp.Format='##0.00' " + &
				"daily_sum_22_comp.Format='##0.00' ")
	End If
End If
	
end subroutine

public function string wf_get_selected_status ();//1-13 jac
long  ll_count
string ls_product_status, & 
		ls_stat_desc, &
       ls_input 

is_selected_status_array = ''
for ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	if ll_count = 1 then
		If isnull(ls_product_status) THEN
		   ls_product_status = 'ALL'
		end if 
	ELSE 
	  If isnull(ls_product_status) Then
	     ls_product_status = " "
	  end if
	end if
	is_selected_status_array += ls_product_status + "~t"
   ls_stat_desc = dw_product_status_window.GetItemString(1, "status_desc" + string(ll_count))
   ls_stat_desc = TRIM(ls_stat_desc)
	if ll_count = 1 then
		If isnull(ls_stat_desc) Then
		   ls_stat_desc = 'ALL'
	   end if
	ELSE
	  If isnull(ls_stat_desc) Then
		ls_stat_desc = " "
	  end if
   END IF
	is_selected_status_array += ls_stat_desc + "~t"
next


Return is_selected_status_array





end function

public subroutine wf_filter ();Integer		li_counter

Long			ll_nbrrows

String		ls_filter, &
				ls_row_message, &
				ls_daily_weekly_ind

ls_daily_weekly_ind  = dw_header.GetItemString(1, "weekly_daily_option")

dw_pa_detail.SetRedraw(False)
If cbx_neg_selection.checked  Then
	If ls_daily_weekly_ind = 'D' Then
		ls_filter = "daily_sum1 < 0 or daily_sum2 < 0 or daily_sum3 < 0 or " + &
						"daily_sum4 < 0 or daily_sum5 < 0 or daily_sum6 < 0 or " + &
						"daily_sum7 < 0 or daily_sum8 < 0 or daily_sum9 < 0 or " + &	
						"daily_sum10 < 0 or daily_sum11 < 0 or daily_sum12 < 0 or " + &
						"daily_sum13 < 0 or daily_sum14 < 0 or daily_sum15 < 0 or " + &
						"daily_sum16 < 0 or daily_sum17 < 0 or daily_sum18 < 0 or " + &
						"daily_sum19 < 0 or daily_sum20 < 0 or daily_sum21 < 0 or " + &
						"daily_sum22 < 0 "
	Else
		ls_filter = "weekly_sum1 < 0 or weekly_sum2 < 0 or weekly_sum3 < 0 or " + &
						"weekly_sum4 < 0 or weekly_sum5 < 0 or weekly_sum6 < 0 or " + &
						"weekly_sum7 < 0 or weekly_sum8 < 0 or weekly_sum9 < 0 or " + &	
						"weekly_sum10 < 0 or weekly_sum11 < 0 or weekly_sum12 < 0 or " + &
						"weekly_sum13 < 0 or weekly_sum14 < 0 or weekly_sum15 < 0 or " + &
						"weekly_sum16 < 0 or weekly_sum17 < 0 or weekly_sum18 < 0 or " + &
						"weekly_sum19 < 0 or weekly_sum20 < 0 or weekly_sum21 < 0 or " + &
						"weekly_sum22 < 0 "
	End If
	ls_row_message = " Rows with negative values"
Else
	ls_filter = "visible_ind = 'Y'"
	ls_row_message = " Rows Total"
End IF

dw_pa_detail.SetFilter(ls_filter)	
dw_pa_detail.Filter()
dw_pa_detail.sort()

wf_calc_totals()


// Set the highlighting again.
dw_pa_detail.SetItem(1,"back_color", il_SelectedColor)
dw_pa_detail.SetItem(1,"text_color", il_SelectedTextColor)

iw_frame.SetMicroHelp(String(ll_nbrrows) + ls_row_message)
dw_pa_detail.SetRedraw(True)

Return
end subroutine

public function boolean wf_update ();STRING	ls_FileName

if SQLCA.userid = mid(dw_header.GetItemString(1, "product_description"),1,7) Then
	Openwithparm(w_pa_summary_file_save_as, This)
	If is_save_file = "Y" Then
		ls_FileName = Message.StringParm
		IF ls_FileName = "A" THEN RETURN FALSE	
		//dw_pa-summary_header.SetItem(1, "seq_num", ls_FileName)
		//dw_pa-summary_header.SetItem(1, "last_seq_num", ls_FileName)
		wf_updat()
	Else
		iw_frame.SetMicroHelp("Ready")
	End If
	RETURN TRUE
ELSE
	RETURN TRUE
END IF
end function

public function integer wf_updat ();INT		li_rtn, &
			li_counter

CHAR		lc_AvailableDate[10], &
			lc_ProcessOption

STRING	ls_PlantData, &
			ls_DetailData, &
			ls_descr_out, &
			ls_page_descr_in, &
			ls_save_prod
			
long     ll_RowCount

s_Error	lstr_Error

lstr_Error.se_app_name  = "PAS"
lstr_Error.se_window_name = "w_pa_summary_file_save_as"
lstr_Error.se_function_name = "wf_Updat"
lstr_Error.se_user_id = sqlca.userid

dw_pa_detail.AcceptText()

ls_page_descr_in = dw_header.GetItemString(1, "file_no" + '~t' + "description")

lc_AvailableDate = STRING(today(), "mm/dd/yyyy")

IF	ls_page_descr_in = "00" THEN
	MESSAGEBOX("Operator Error","You must select a sequence number.")
	RETURN -1
ELSE
	lc_ProcessOption = "S"

	IF wf_GetDetailData(ls_DetailData) <> 0 THEN 
		RETURN -1
	END IF
END IF

//IF NOt IsValid(iu_orp204) Then iu_orp204 = Create u_orp204
//
//li_Rtn = iu_orp204.nf_orpo81br_pa_summary(lstr_Error, lc_ProcessOption, &
//										lc_AvailableDate, ls_page_descr_in, &
//										ls_PlantData, ls_DetailData, ls_descr_out)
										
IF NOT IsValid(u_ws_orp4) Then iu_ws_orp4 = Create u_ws_orp4
									
li_rtn = iu_ws_orp4.nf_orpo81fr(lstr_Error, lc_ProcessOption, &
										lc_AvailableDate, ls_page_descr_in, &
										ls_descr_out, ls_PlantData, ls_DetailData, SQLCA.userid)										
																
//  somehow these get turned off after the save
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')										
										

RETURN li_Rtn
end function

public function boolean wf_deleterow ();String	ls_product_code

Long		ll_RowCount, &
			ll_sub, &
			ll_line_no, &
			ll_current_row

ll_current_row = dw_pa_detail.GetRow()
ll_line_no = dw_pa_detail.GetItemNumber(ll_current_row, "line_no")
ls_product_code = dw_pa_detail.GetItemString(ll_current_row, "product_code")

ll_RowCount = dw_pa_detail.RowCount()

ll_sub = 1

Do until ll_sub > ll_RowCount
	If (dw_pa_detail.GetItemString(ll_sub, "product_code") = ls_product_code) and &
			(dw_pa_detail.GetItemNumber(ll_sub, "line_no") = ll_line_no) Then
		dw_pa_detail.DeleteRow(ll_sub)	
		ll_RowCount --
	Else
		ll_sub ++
	End If
Loop

Return True
end function

public function boolean wf_retrieve ();long			ll_rec_count, &
				ll_row, &
				ll_ret, &
				ll_count

String		ls_string, &
				ls_string2, &
				ls_header, &
				ls_product_data, &
				ls_plant_option, &
				ls_plant_code, &
				ls_location_group_system, &
				ls_location_group_groupid, &
				ls_location_type, &
				ls_product_option, &
				ls_file_no, &
				ls_product_group_system, &
				ls_product_group_groupid, &
				ls_week_end_date, &
				ls_boxes_loads, &
				ls_daily_weekly_ind, &
				ls_begin_date, &
				ls_product_status, &
				ls_product_statuses, &
				ls_description, &
				ls_temp, &
				ls_userid

u_string_functions	lu_string

Date			ldt_begin_date

Integer		li_pa_range

iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

ib_reinquire = true

ls_string2 = dw_product_status_window.Describe("DataWindow.Data")

ll_ret = dw_product_status_window.ImportString(ls_string2)
//dw_product_status_window.AcceptText()

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
//istr_error_info.se_procedure_name = "nf_pasp85cr_inq_new_pa_report"
istr_error_info.se_procedure_name = "nf_pasp85gr"
istr_error_info.se_message = Space(71)


ls_daily_weekly_ind  = dw_header.GetItemString(1, "weekly_daily_option")
ls_header = ls_daily_weekly_ind + '~t'

If ls_daily_weekly_ind = 'D' Then
	This.Title = "Daily PA Report" 
	dw_pa_detail.dataobject = "d_pas_pa_report_dly_dtl"
Else
	This.Title = "Weekly PA Report" 
	dw_pa_detail.dataobject = "d_pas_pa_report_wk_dtl"
End If

ls_boxes_loads = dw_loads_boxes.GetItemString(1, "boxes_loads")
ls_header += ls_boxes_loads + '~t' 

ls_plant_option = dw_header.GetItemString(1, "plant_option")

Choose Case ls_plant_option
	Case 'A'
		ls_plant_code = '   '
		ls_location_group_system = '     '
		ls_location_group_groupid = '    '
		ls_location_type = '            '
	Case 'C'
		ls_plant_code = dw_header.GetItemString(1, "plant_code")
		ls_location_group_system = '     '
		ls_location_group_groupid = '    '
		ls_location_type = '            '
	Case 'G'
		ls_plant_code = '   '
		ls_location_group_system = dw_header.GetItemString(1, "location_group_system")
		ls_location_group_groupid = dw_header.GetItemString(1, "location_group_groupid")
		ls_location_type = '            '
	Case 'T'	
		ls_plant_code = '   '
		ls_location_group_system = '     '
		ls_location_group_groupid = '    '
		ls_location_type = dw_header.GetItemString(1, "plant_type")
End Choose

ls_header += ls_plant_option + '~t'
ls_header += ls_plant_code + '~t'
ls_header += ls_location_group_system + '~t'
ls_header += ls_location_group_groupid + '~t'
ls_header += ls_location_type + '~t'

ls_product_option = dw_header.GetItemString(1, "product_option")

Choose Case ls_product_option
	Case 'A'
		ls_file_no = '  '
		ls_product_group_system = '     '
		ls_product_group_groupid = '    '
		ls_userid = '        '
	Case 'F'
		ls_file_no = dw_header.GetItemString(1, "file_no")		
		ls_product_group_system = '     '
		ls_product_group_groupid = '    '	
		ls_userid = mid(dw_header.GetItemString(1, "product_description"),1,7) 
	Case 'G'
		ls_file_no = '  '		
		ls_product_group_system = dw_header.GetItemString(1, "product_group_system")
		ls_product_group_groupid = dw_header.GetItemString(1, "product_group_groupid")      
		ls_userid = '        '
End Choose

If ls_product_option = 'F' Then
	ib_enable_save = True
	iw_frame.im_menu.mf_enable('m_save')
	iw_frame.im_menu.mf_enable('m_addrow')
	iw_frame.im_menu.mf_enable('m_deleterow')
Else
	ib_enable_save = False	
	iw_frame.im_menu.mf_disable('m_save')
	iw_frame.im_menu.mf_disable('m_addrow')
	iw_frame.im_menu.mf_disable('m_deleterow')	
End If

ls_header += ls_product_option + '~t'
ls_header += ls_userid + '~t' 
ls_header += ls_file_no + '~t'
ls_header += ls_product_group_system + '~t'
ls_header += ls_product_group_groupid + '~t'

ls_header += dw_header.GetItemString(1, "division_code") + '~t'
ls_header += dw_header.GetItemString(1, "product_state") + '~t'
ls_header += String(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd") + '~t'

For ll_count = 1 to il_selected_status_max
	ls_product_status = dw_product_status_window.GetItemString(1, "product_status" + string(ll_count))
   ls_product_status = TRIM(ls_product_status)
	if isnull(ls_product_status) then
		ls_product_status = " " + '~t'
	end if
   ls_product_statuses += ls_product_status + '~t'
Next

ls_header += ls_product_statuses + '~t'

//MessageBox ("ls_header", "ls_header = " + ls_header) 

dw_pa_detail.reset()
ls_begin_date =  String(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd")
ldt_begin_date = dw_header.GetItemDate(1, "begin_date")

If ls_daily_weekly_ind = 'W' Then
	If DayNumber(date(ls_begin_date)) = 1 Then  // Sunday
		ls_week_end_date = String(RelativeDate(ldt_begin_date, 0), "yyyy-mm-dd")
	Else 
		If DayNumber(date(ls_begin_date)) = 2 Then  // Monday
			ls_week_end_date = String(RelativeDate(ldt_begin_date, 6), "yyyy-mm-dd")
		Else
			If DayNumber(date(ls_begin_date)) = 3 Then  // Tuesday
				ls_week_end_date = String(RelativeDate(ldt_begin_date, 5), "yyyy-mm-dd")
			Else	
				If DayNumber(date(ls_begin_date)) = 4 Then  // Wedndsday
					ls_week_end_date = String(RelativeDate(ldt_begin_date, 4), "yyyy-mm-dd")
				Else	
					If DayNumber(date(ls_begin_date)) = 5 Then  // Thursday
						ls_week_end_date = String(RelativeDate(ldt_begin_date, 3), "yyyy-mm-dd")
					Else
						If DayNumber(date(ls_begin_date)) = 6 Then  // Friday
							ls_week_end_date = String(RelativeDate(ldt_begin_date, 2), "yyyy-mm-dd")	
						Else	
							If DayNumber(date(ls_begin_date)) = 7 Then  // Saturday
								ls_week_end_date = String(RelativeDate(ldt_begin_date, 1), "yyyy-mm-dd")
							End If
						End If
					End If
				End If
			End If
		End If
	End If
	dw_pa_detail.object.weekly_sum1_date.expression = ls_week_end_date
Else
	dw_pa_detail.object.daily_sum1_date.expression = ls_begin_date
End If

	
//If iu_pas203.nf_pasp85cr_inq_new_pa_report(istr_error_info, & 
//										ls_header, &
//										ls_product_data) < 0 Then
//										This.SetRedraw(True) 
//										Return False
//End If										

If iu_ws_pas2.nf_pasp85gr(ls_header,ls_product_data,istr_error_info) < 0 Then
	This.SetRedraw(True)
	Return False
End If

////ls_product_data = 'd0101dm~ttrim~t80~t-500~t2~t3~t4~t-55555~t6~t7~t8~t9~t10~t11~t12~t13~t14~t15~t16~t17~t18~t19~t20~t21~t22~r~n' + &
////					  'D01014DMCE*~tMORETRIMMING~t80~t1~t2~t3~t99999~t5~t6~t7~t8~t9~t10~t11~t12~t13~t14~t15~t16~t17~t18~t19~t20~t21~t22~r~n' //+ &
//					  
////MessageBox('Return Data',string(ls_product_data))
//
If Not lu_string.nf_IsEmpty(ls_product_data) Then
	ll_rec_count = dw_pa_detail.ImportString(ls_product_data)
	If ll_rec_count > 0 Then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

              
FOR ll_row = 1 TO ll_rec_count
	dw_pa_detail.SetItem ( ll_row, "loads_boxes", ls_boxes_loads)
	dw_pa_detail.SetItem ( ll_row, "back_color", il_selectedcolor)
	dw_pa_detail.SetItem ( ll_row, "text_color", il_selectedtextcolor)
	dw_pa_detail.SetItem ( ll_row, "grade", mid(dw_pa_detail.GetItemString( ll_row, "product_code"), 5, 1))
	If cbx_expand_all.Checked = True Then
		dw_pa_detail.SetItem(ll_row, "visible_ind", 'Y')
		If dw_pa_detail.GetItemString (ll_row, "plant_code") = '   ' Then
			dw_pa_detail.SetItem(ll_row, "expand_ind", '-')
		End If
	Else
		If dw_pa_detail.GetItemString (ll_row, "plant_code") = '   ' Then
			dw_pa_detail.SetItem(ll_row, "expand_ind", '+')
			dw_pa_detail.SetItem(ll_row, "visible_ind", 'Y')
		Else	
			dw_pa_detail.SetItem(ll_row, "visible_ind", 'N')
		End If
	End If
//	If dw_pa_detail.GetItemString (ll_row, "plant_code") = '   ' Then
//		dw_pa_detail.SetItem(ll_row,"back_color", 8421504)
//	Else
//		dw_pa_detail.SetItem(ll_row,"back_color", il_SelectedColor)
//	End If
NEXT 

wf_format()

wf_filter()

If cbx_grade_sort.Checked = True Then
	dw_pa_detail.SetRedraw(False)
	dw_pa_detail.SetSort("line_no A, grade A, product_code A, product_state A, product_status A, plant_code A, seq_no A")
	dw_pa_detail.Sort()
	dw_pa_detail.SetRedraw(True)
Else
	dw_pa_detail.SetRedraw(False)
	dw_pa_detail.SetSort("line_no A, product_code A, product_state A, product_status A, plant_code A, seq_no A")
	dw_pa_detail.Sort()
	dw_pa_detail.SetRedraw(True)
End If


//dw_weekly_pa_detail.object.last_week_end_date.expression =  &
//				string(dw_week_end_date.uf_get_last_week_end_date(), "yyyy-mm-dd")

If ls_daily_weekly_ind = 'W' Then
	dw_pa_detail.object.last_week_end_date.expression = String(RelativeDate(Today(), 196), "yyyy-mm-dd")
Else
	CONNECT USING SQLCA;	
	SELECT tutltypes.type_short_desc  
	  INTO :ls_description  
	  FROM tutltypes  
	  WHERE ( tutltypes.record_type = 'PA RANGE' ) AND  
				( tutltypes.type_code = 'PA RANGE' )   ;
	
	li_pa_range = Integer(ls_description) - 1	
	ls_temp = String(RelativeDate(Today(), li_pa_range), "yyyy-mm-dd")
	
	
	dw_pa_detail.object.last_week_end_date.expression = String(RelativeDate(Today(), li_pa_range), "yyyy-mm-dd")
End If

// Must do because of the reinquire to set the scroll bar
dw_pa_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'
//1-13 jac 
//dw_pa_detail.Object.DataWindow.HorizontalScrollSplit = '966'
//dw_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '966'
dw_pa_detail.Object.DataWindow.HorizontalScrollSplit = '1250'
dw_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '1250'
//
This.SetRedraw(True) 

dw_header.ResetUpdate()
dw_loads_boxes.ResetUpdate()

dw_pa_detail.ResetUpdate()
dw_pa_detail.SetFocus()
 
Return True

end function

public function boolean wf_find_product ();Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
string	ls_prod_code, & 
			ls_data, &
			ls_update_flag, &
			ls_reset
integer	li_counter
Boolean	lb_yes_found


ll_row_count = dw_pa_detail.RowCount()
li_counter = 1
Do While li_counter <= ll_Row_count 
	dw_pa_detail.SelectRow(li_counter, False)
	li_counter = li_counter + 1
loop

li_counter = ii_row_count + 1

if ii_row_count > ll_row_count then
	li_counter = 1
end if

if ll_row_count < li_counter then Return True
ls_data = is_data

lb_yes_found = False

Do While li_counter <= ll_Row_count 
		ls_prod_code = dw_pa_detail.GetItemString(li_counter, "product_code")
		If pos(ls_prod_code, ls_data) > 0 and pos(ls_prod_code, ls_data) < 2 then
			lb_yes_found=True
		else	
			lb_yes_found=False
		end if
		if lb_yes_found = True then
			ii_row_count = li_counter
			dw_pa_detail.SetRedraw(False)
 			dw_pa_detail.ScrollToRow(li_counter)
			dw_pa_detail.SelectRow(li_counter, True)
			dw_pa_detail.SetRow(li_counter + 1)
			dw_pa_detail.SetRedraw(True)
			li_counter = ll_row_count + 1
		else	
			dw_pa_detail.SelectRow(li_counter, False)
			li_counter = li_counter + 1	
		end if
Loop

if lb_yes_found=False then
	iw_Frame.SetMicroHelp("No matching products found")
	ii_row_count = ll_row_count + 1
else
	iw_Frame.SetMicroHelp("Product found")	
end if

Return True

end function

public subroutine wf_calc_totals ();Long	ll_rowcount, ll_sub

Decimal{2}	ld_total_1, ld_total_2, ld_total_3, ld_total_4, ld_total_5, &
				ld_total_6, ld_total_7, ld_total_8, ld_total_9, ld_total_10, &
				ld_total_11, ld_total_12, ld_total_13, ld_total_14, ld_total_15, &
				ld_total_16, ld_total_17, ld_total_18, ld_total_19, ld_total_20, &
				ld_total_21
				
Long			ll_total_1, ll_total_2, ll_total_3, ll_total_4, ll_total_5, &
				ll_total_6, ll_total_7, ll_total_8, ll_total_9, ll_total_10, &
				ll_total_11, ll_total_12, ll_total_13, ll_total_14, ll_total_15, &
				ll_total_16, ll_total_17, ll_total_18, ll_total_19, ll_total_20, &
				ll_total_21		
				
String		ls_temp				
				
				
ll_rowcount = dw_pa_detail.Rowcount()

For ll_sub = 1 to ll_rowcount
	If dw_header.GetItemString(1, "weekly_daily_option") = 'W' Then
		If dw_pa_detail.GetItemString(ll_sub, "plant_code") = '   ' Then
			If dw_loads_boxes.GetItemString(1, "boxes_loads") = 'L' Then
				ld_total_1 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum1") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_2 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum2") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_3 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum3") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_4 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum4") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_5 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum5") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_6 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum6") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_7 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum7") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_8 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum8") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_9 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum9") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_10 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum10") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_11 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum11") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_12 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum12") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_13 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum13") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_14 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum14") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_15 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum15") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_16 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum16") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_17 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum17") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_18 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum18") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_19 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum19") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_20 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum20") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_21 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum21") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250	
			Else
				ll_total_1 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum1")
				ll_total_2 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum2")
				ll_total_3 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum3")
				ll_total_4 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum4")
				ll_total_5 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum5")
				ll_total_6 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum6")
				ll_total_7 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum7")
				ll_total_8 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum8")
				ll_total_9 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum9")
				ll_total_10 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum10")
				ll_total_11 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum11")
				ll_total_12 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum12")
				ll_total_13 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum13")
				ll_total_14 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum14")
				ll_total_15 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum15")
				ll_total_16 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum16")
				ll_total_17 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum17")
				ll_total_18 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum18")
				ll_total_19 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum19")
				ll_total_20 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum20")
				ll_total_21 += dw_pa_detail.GetItemNumber(ll_sub, "weekly_sum21")
			End If
		End If
	Else	
		If dw_pa_detail.GetItemString(ll_sub, "plant_code") = '   ' Then
			If dw_loads_boxes.GetItemString(1, "boxes_loads") = 'L' Then
				ld_total_1 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum1") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_2 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum2") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_3 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum3") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_4 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum4") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_5 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum5") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_6 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum6") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_7 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum7") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_8 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum8") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_9 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum9") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_10 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum10") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_11 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum11") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_12 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum12") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_13 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum13") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_14 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum14") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_15 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum15") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_16 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum16") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_17 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum17") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_18 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum18") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_19 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum19") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_20 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum20") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250
				ld_total_21 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum21") * dw_pa_detail.GetItemNumber(ll_sub, "product_avg_weight")  / 42250	
			Else
				ll_total_1 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum1")
				ll_total_2 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum2")
				ll_total_3 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum3")
				ll_total_4 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum4")
				ll_total_5 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum5")
				ll_total_6 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum6")
				ll_total_7 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum7")
				ll_total_8 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum8")
				ll_total_9 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum9")
				ll_total_10 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum10")
				ll_total_11 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum11")
				ll_total_12 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum12")
				ll_total_13 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum13")
				ll_total_14 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum14")
				ll_total_15 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum15")
				ll_total_16 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum16")
				ll_total_17 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum17")
				ll_total_18 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum18")
				ll_total_19 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum19")
				ll_total_20 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum20")
				ll_total_21 += dw_pa_detail.GetItemNumber(ll_sub, "daily_sum21")		
			End If
		End If
	End If
Next

If dw_loads_boxes.GetItemString(1, "boxes_loads") = 'L' Then
	dw_pa_detail.Object.compute1_t.text = string(ld_total_1)
	dw_pa_detail.Object.compute2_t.text = string(ld_total_2)
	dw_pa_detail.Object.compute3_t.Text = string(ld_total_3)
	dw_pa_detail.Object.compute4_t.Text = string(ld_total_4)
	dw_pa_detail.Object.compute5_t.Text = string(ld_total_5)
	dw_pa_detail.Object.compute6_t.Text = string(ld_total_6)
	dw_pa_detail.Object.compute7_t.Text = string(ld_total_7)
	dw_pa_detail.Object.compute8_t.Text = string(ld_total_8)
	dw_pa_detail.Object.compute9_t.Text = string(ld_total_9)
	dw_pa_detail.Object.compute10_t.Text = string(ld_total_10)
	dw_pa_detail.Object.compute11_t.Text = string(ld_total_11)
	dw_pa_detail.Object.compute12_t.Text = string(ld_total_12)
	dw_pa_detail.Object.compute13_t.Text = string(ld_total_13)
	dw_pa_detail.Object.compute14_t.Text = string(ld_total_14)
	dw_pa_detail.Object.compute15_t.Text = string(ld_total_15)
	dw_pa_detail.Object.compute16_t.Text = string(ld_total_16)
	dw_pa_detail.Object.compute17_t.Text = string(ld_total_17)
	dw_pa_detail.Object.compute18_t.Text = string(ld_total_18)
	dw_pa_detail.Object.compute19_t.Text = string(ld_total_19)
	dw_pa_detail.Object.compute20_t.Text = string(ld_total_20)
	dw_pa_detail.Object.compute21_t.Text = string(ld_total_21)
Else
	dw_pa_detail.Object.compute1_t.text = string(ll_total_1)
	dw_pa_detail.Object.compute2_t.text = string(ll_total_2)
	dw_pa_detail.Object.compute3_t.Text = string(ll_total_3)
	dw_pa_detail.Object.compute4_t.Text = string(ll_total_4)
	dw_pa_detail.Object.compute5_t.Text = string(ll_total_5)
	dw_pa_detail.Object.compute6_t.Text = string(ll_total_6)
	dw_pa_detail.Object.compute7_t.Text = string(ll_total_7)
	dw_pa_detail.Object.compute8_t.Text = string(ll_total_8)
	dw_pa_detail.Object.compute9_t.Text = string(ll_total_9)
	dw_pa_detail.Object.compute10_t.Text = string(ll_total_10)
	dw_pa_detail.Object.compute11_t.Text = string(ll_total_11)
	dw_pa_detail.Object.compute12_t.Text = string(ll_total_12)
	dw_pa_detail.Object.compute13_t.Text = string(ll_total_13)
	dw_pa_detail.Object.compute14_t.Text = string(ll_total_14)
	dw_pa_detail.Object.compute15_t.Text = string(ll_total_15)
	dw_pa_detail.Object.compute16_t.Text = string(ll_total_16)
	dw_pa_detail.Object.compute17_t.Text = string(ll_total_17)
	dw_pa_detail.Object.compute18_t.Text = string(ll_total_18)
	dw_pa_detail.Object.compute19_t.Text = string(ll_total_19)
	dw_pa_detail.Object.compute20_t.Text = string(ll_total_20)
	dw_pa_detail.Object.compute21_t.Text = string(ll_total_21)	
End If
end subroutine

public function integer wf_getdetaildata (ref string as_detaildata);Long	ll_sub, &
		ll_line_no
		
String	ls_fortyone_tabs, &
			ls_product, &
			ls_line_number, &
			ls_age_code, ls_plantcode, ls_temp
			
			
ls_fortyone_tabs = '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + &
					'~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + &
					'~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + &
					'~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t' + '~t'

ll_line_no = 0
For ll_sub = 1 to dw_pa_detail.RowCount()
	If dw_pa_detail.GetItemNumber(ll_sub, "line_no") > ll_line_no Then
		ls_product = dw_pa_detail.GetItemString(ll_sub, "product_code")
		ls_line_number = String(dw_pa_detail.GetItemNumber(ll_sub, "line_no"))		
		If dw_pa_detail.GetItemString(ll_sub, "age_code") > '  ' Then
			If ls_product > '          ' Then
				ls_age_code = dw_pa_detail.GetItemString(ll_sub, "age_code")
			Else
				ls_age_code = '  '
			End If
		Else
			If ls_product > '          ' Then
				ls_age_code = 'A '
			Else	
				ls_age_code = '  '
			End If
		End If
		
		as_detaildata += ls_product +  '~t' + ls_age_code + ls_fortyone_tabs + ls_line_number + '~r~n'
		ll_line_no = dw_pa_detail.GetItemNumber(ll_sub, "line_no")
	End If
Next

//For ll_sub = 1 to dw_pa_detail.Rowcount()
//	ls_plantcode = dw_pa_detail.GetItemString(ll_sub, "plant_code")
//	ls_temp += ls_plantcode + '~t'
//Next

Return 0
end function

on w_pas_pa_report.create
int iCurrent
call super::create
this.cb_find_next=create cb_find_next
this.cbx_grade_sort=create cbx_grade_sort
this.dw_scroll_product=create dw_scroll_product
this.dw_pa_detail=create dw_pa_detail
this.cbx_expand_all=create cbx_expand_all
this.dw_product_status_window=create dw_product_status_window
this.dw_header=create dw_header
this.dw_loads_boxes=create dw_loads_boxes
this.cbx_neg_selection=create cbx_neg_selection
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_find_next
this.Control[iCurrent+2]=this.cbx_grade_sort
this.Control[iCurrent+3]=this.dw_scroll_product
this.Control[iCurrent+4]=this.dw_pa_detail
this.Control[iCurrent+5]=this.cbx_expand_all
this.Control[iCurrent+6]=this.dw_product_status_window
this.Control[iCurrent+7]=this.dw_header
this.Control[iCurrent+8]=this.dw_loads_boxes
this.Control[iCurrent+9]=this.cbx_neg_selection
end on

on w_pas_pa_report.destroy
call super::destroy
destroy(this.cb_find_next)
destroy(this.cbx_grade_sort)
destroy(this.dw_scroll_product)
destroy(this.dw_pa_detail)
destroy(this.cbx_expand_all)
destroy(this.dw_product_status_window)
destroy(this.dw_header)
destroy(this.dw_loads_boxes)
destroy(this.cbx_neg_selection)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_generatesales')
iw_frame.im_menu.mf_enable('m_print')

If Isnull(ib_enable_save) or (ib_enable_save = False) Then
	iw_frame.im_menu.mf_disable('m_save')
	iw_frame.im_menu.mf_disable('m_addrow')
	iw_frame.im_menu.mf_disable('m_deleterow')
Else	
	iw_frame.im_menu.mf_enable('m_save')
	iw_frame.im_menu.mf_enable('m_addrow')
	iw_frame.im_menu.mf_enable('m_deleterow')
End If



end event

event deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')

//iw_frame.im_menu.mf_disable('m_print')




end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288
// 
//dw_weekly_pa_detail.width	= newwidth - (30 + li_x)
//dw_weekly_pa_detail.height	= newheight - (30 + li_y)

integer li_x		= 30
integer li_y		= 450


if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

dw_pa_detail.width  = newwidth - (li_x)
dw_pa_detail.height = newheight - (li_y)

// Must do because of the split horizonal bar 
dw_pa_detail.Object.DataWindow.HorizontalScrollSplit = '0'
dw_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '0'
//1-13 jac
//dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollSplit = '966'
//dw_weekly_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '966'
dw_pa_detail.Object.DataWindow.HorizontalScrollSplit = '1250'
dw_pa_detail.Object.DataWindow.HorizontalScrollPosition2 =  '1250'
//

end event

event ue_fileprint;call super::ue_fileprint;String						ls_mod, &
								ls_temp, &
								ls_week_end_date

DataStore					lds_print

u_string_functions		lu_string_functions

lds_print = create u_print_datastore

If  dw_header.GetItemString(1, "weekly_daily_option") = 'W' Then
	lds_print.DataObject = 'd_pas_pa_report_wkly_print'
	lds_print.object.weekly_sum1_date.expression = dw_pa_detail.object.weekly_sum1_date.expression	
Else
	lds_print.DataObject = 'd_pas_pa_report_dly_print'
	lds_print.object.daily_sum1_date.expression = dw_pa_detail.object.daily_sum1_date.expression	
End If

lds_print.object.type_label.text = dw_header.Object.option_description_t.Text
lds_print.object.type_description.text = dw_header.GetItemString(1, "option_description")
lds_print.object.plant_label.text = dw_header.Object.plant_description_t.Text
lds_print.object.product_label.text = dw_header.Object.product_description_t.Text

ls_temp = dw_header.GetItemString(1, "plant_description")
ls_temp = lu_string_functions.nf_globalreplace(ls_temp,'"', ' ', True)
lds_print.object.plant_description.text = ls_temp

ls_temp = dw_header.GetItemString(1, "product_description")
ls_temp = lu_string_functions.nf_globalreplace(ls_temp,'"', ' ', True) 
lds_print.object.product_description.text = ls_temp

lds_print.object.last_week_end_date.expression =  dw_pa_detail.object.last_week_end_date.expression

lds_print.object.compute1_t.text = dw_pa_detail.Object.compute1_t.text
lds_print.object.compute2_t.text = dw_pa_detail.Object.compute2_t.text
lds_print.object.compute3_t.text = dw_pa_detail.Object.compute3_t.text
lds_print.object.compute4_t.text = dw_pa_detail.Object.compute4_t.text
lds_print.object.compute5_t.text = dw_pa_detail.Object.compute5_t.text
lds_print.object.compute6_t.text = dw_pa_detail.Object.compute6_t.text
lds_print.object.compute7_t.text = dw_pa_detail.Object.compute7_t.text
lds_print.object.compute8_t.text = dw_pa_detail.Object.compute8_t.text
lds_print.object.compute9_t.text = dw_pa_detail.Object.compute9_t.text
lds_print.object.compute10_t.text = dw_pa_detail.Object.compute10_t.text
lds_print.object.compute11_t.text = dw_pa_detail.Object.compute11_t.text
lds_print.object.compute12_t.text = dw_pa_detail.Object.compute12_t.text
lds_print.object.compute13_t.text = dw_pa_detail.Object.compute13_t.text
lds_print.object.compute14_t.text = dw_pa_detail.Object.compute14_t.text
lds_print.object.compute15_t.text = dw_pa_detail.Object.compute15_t.text
lds_print.object.compute16_t.text = dw_pa_detail.Object.compute16_t.text
lds_print.object.compute17_t.text = dw_pa_detail.Object.compute17_t.text
lds_print.object.compute18_t.text = dw_pa_detail.Object.compute18_t.text
lds_print.object.compute19_t.text = dw_pa_detail.Object.compute19_t.text
lds_print.object.compute20_t.text = dw_pa_detail.Object.compute20_t.text
lds_print.object.compute21_t.text = dw_pa_detail.Object.compute21_t.text


dw_pa_detail.sharedata(lds_print)

If  dw_header.GetItemString(1, "weekly_daily_option") = 'W' Then
	If dw_loads_boxes.GetItemString(1,"boxes_loads") = 'B' Then
		ls_mod ="weekly_sum_1_comp.Format='##,##0' " + &
				"weekly_sum_2_comp.Format='##,##0' " + &
				"weekly_sum_3_comp.Format='##,##0' " + &
				"weekly_sum_4_comp.Format='##,##0' " + &
				"weekly_sum_5_comp.Format='##,##0' " + &
				"weekly_sum_6_comp.Format='##,##0' " + &
				"weekly_sum_7_comp.Format='##,##0' " + &
				"weekly_sum_8_comp.Format='##,##0' " + &
				"weekly_sum_9_comp.Format='##,##0' " + &
				"weekly_sum_10_comp.Format='##,##0' " + &
				"weekly_sum_11_comp.Format='##,##0' " + &
				"weekly_sum_12_comp.Format='##,##0' " + &
				"weekly_sum_13_comp.Format='##,##0' " + &
				"weekly_sum_14_comp.Format='##,##0' " + &
				"weekly_sum_15_comp.Format='##,##0' " + &
				"weekly_sum_16_comp.Format='##,##0' " + &
				"weekly_sum_17_comp.Format='##,##0' " + &
				"weekly_sum_18_comp.Format='##,##0' " + &
				"weekly_sum_19_comp.Format='##,##0' " + &
				"weekly_sum_20_comp.Format='##,##0' " + &
				"weekly_sum_21_comp.Format='##,##0' " + &
				"weekly_sum_22_comp.Format='##,##0' "
	Else
			ls_mod ="weekly_sum_1_comp.Format='##0.00' " + &
				"weekly_sum_2_comp.Format='##0.00' " + &			
				"weekly_sum_3_comp.Format='##0.00' " + &			
				"weekly_sum_4_comp.Format='##0.00' " + &			
				"weekly_sum_5_comp.Format='##0.00' " + &			
				"weekly_sum_6_comp.Format='##0.00' " + &			
				"weekly_sum_7_comp.Format='##0.00' " + &			
				"weekly_sum_8_comp.Format='##0.00' " + &			
				"weekly_sum_9_comp.Format='##0.00' " + &			
				"weekly_sum_10_comp.Format='##0.00' " + &			
				"weekly_sum_11_comp.Format='##0.00' " + &			
				"weekly_sum_12_comp.Format='##0.00' " + &			
				"weekly_sum_13_comp.Format='##0.00' " + &			
				"weekly_sum_14_comp.Format='##0.00' " + &			
				"weekly_sum_15_comp.Format='##0.00' " + &			
				"weekly_sum_16_comp.Format='##0.00' " + &			
				"weekly_sum_17_comp.Format='##0.00' " + &			
				"weekly_sum_18_comp.Format='##0.00' " + &			
				"weekly_sum_19_comp.Format='##0.00' " + &			
				"weekly_sum_20_comp.Format='##0.00' " + &			
				"weekly_sum_21_comp.Format='##0.00' " + &
				"weekly_sum_22_comp.Format='##0.00' " 
	End If
Else
	If dw_loads_boxes.GetItemString(1,"boxes_loads") = 'B' Then
		ls_mod = "daily_sum_1_comp.Format='##,##0' " + &
				"daily_sum_2_comp.Format='##,##0' " + &
				"daily_sum_3_comp.Format='##,##0' " + &
				"daily_sum_4_comp.Format='##,##0' " + &
				"daily_sum_5_comp.Format='##,##0' " + &
				"daily_sum_6_comp.Format='##,##0' " + &
				"daily_sum_7_comp.Format='##,##0' " + &
				"daily_sum_8_comp.Format='##,##0' " + &
				"daily_sum_9_comp.Format='##,##0' " + &
				"daily_sum_10_comp.Format='##,##0' " + &
				"daily_sum_11_comp.Format='##,##0' " + &
				"daily_sum_12_comp.Format='##,##0' " + &
				"daily_sum_13_comp.Format='##,##0' " + &
				"daily_sum_14_comp.Format='##,##0' " + &
				"daily_sum_15_comp.Format='##,##0' " + &
				"daily_sum_16_comp.Format='##,##0' " + &
				"daily_sum_17_comp.Format='##,##0' " + &
				"daily_sum_18_comp.Format='##,##0' " + &
				"daily_sum_19_comp.Format='##,##0' " + &
				"daily_sum_20_comp.Format='##,##0' " + &
				"daily_sum_21_comp.Format='##,##0' " + &
				"daily_sum_22_comp.Format='##,##0' "
	Else
		ls_mod = "daily_sum_1_comp.Format='##0.00' " + &
				"daily_sum_2_comp.Format='##0.00' " + &			
				"daily_sum_3_comp.Format='##0.00' " + &			
				"daily_sum_4_comp.Format='##0.00' " + &			
				"daily_sum_5_comp.Format='##0.00' " + &			
				"daily_sum_6_comp.Format='##0.00' " + &			
				"daily_sum_7_comp.Format='##0.00' " + &			
				"daily_sum_8_comp.Format='##0.00' " + &			
				"daily_sum_9_comp.Format='##0.00' " + &			
				"daily_sum_10_comp.Format='##0.00' " + &			
				"daily_sum_11_comp.Format='##0.00' " + &			
				"daily_sum_12_comp.Format='##0.00' " + &			
				"daily_sum_13_comp.Format='##0.00' " + &			
				"daily_sum_14_comp.Format='##0.00' " + &			
				"daily_sum_15_comp.Format='##0.00' " + &			
				"daily_sum_16_comp.Format='##0.00' " + &			
				"daily_sum_17_comp.Format='##0.00' " + &			
				"daily_sum_18_comp.Format='##0.00' " + &			
				"daily_sum_19_comp.Format='##0.00' " + &			
				"daily_sum_20_comp.Format='##0.00' " + &			
				"daily_sum_21_comp.Format='##0.00' " + &
				"daily_sum_22_comp.Format='##0.00' "
	End If	
End If

ls_temp = lds_print.modify(ls_mod)

If cbx_grade_sort.Checked = True Then
	lds_print.SetSort("line_no A, grade A, product_code A, product_state A, product_status A, plant_code A, seq_no A")
	lds_print.Sort()
End If

lds_print.print()




end event

event ue_get_data;//date		ldt_date	
//
Choose Case as_value
	Case 'weekly_daily_option'
		If IsNull(dw_header.GetItemString(1, "weekly_daily_option")) Then 
			message.StringParm = 'W'
		Else
			message.StringParm = dw_header.GetItemString(1, "weekly_daily_option")
		End If
		
	Case 'display_option'
		If IsNull(dw_loads_boxes.GetItemString(1, "boxes_loads")) Then 
			//message.StringParm = 'L'
			// pjm 09/18/2014 changed default to 'Boxes'
			message.StringParm = 'B'
		Else
			message.StringParm = dw_loads_boxes.GetItemString(1, "boxes_loads")
		End If	
		
	Case 'plant_option'
		If IsNull(dw_header.GetItemString(1, "plant_option")) Then 
			message.StringParm = 'A'
		Else
			message.StringParm = dw_header.GetItemString(1, "plant_option")
		End If		
		
	Case 'location_type'
		If IsNull(dw_header.GetItemString(1, "plant_type")) Then 
			message.StringParm = 'PMWXYSHNTFZ '
		Else
			message.StringParm = dw_header.GetItemString(1, "plant_type")
		End If		
		
	Case 'file_no'
		If IsNull(dw_header.GetItemString(1, "file_no")) or (dw_header.GetItemString(1, "file_no") = '  ') Then 
			message.StringParm = '  '
		Else
			message.StringParm = dw_header.GetItemString(1, "file_no")
		End If	
		
	Case 'file_desc'
		message.StringParm = Mid(dw_header.GetItemString(1, "product_description"), 13)
		
		
	Case 'product_option'
		If IsNull(dw_header.GetItemString(1, "product_option")) Then 
			message.StringParm = 'A'
		Else
			message.StringParm = dw_header.GetItemString(1, "product_option")
		End If	
		
	Case 'begin_date'
		If IsNull(dw_header.GetItemDate(1, "begin_date")) Then 
			message.StringParm = String(Today())
		Else
			message.StringParm = String(dw_header.GetItemDate(1, "begin_date"))
		End If
		
	Case 'begin_date'
		If IsNull(dw_header.GetItemDate(1, "begin_date")) Then 
			message.StringParm = String(Today())
		Else
			message.StringParm = String(dw_header.GetItemDate(1, "begin_date"))
		End If		
		
	Case 'check_boxes'
		If isnull(is_check_boxes) Then
			message.StringParm = 'NNN'
		Else
			message.StringParm = is_check_boxes
		End If
		
	Case 'product_state'
		If IsNull(dw_header.GetItemString(1, "product_state")) Then 
			message.StringParm = '1'
		Else
			message.StringParm = dw_header.GetItemString(1, "product_state")
		End If		
		
	Case 'plant_code'
		If IsNull(dw_header.GetItemString(1, "plant_code")) Then 
			message.StringParm = '1'
		Else
			message.StringParm = dw_header.GetItemString(1, "plant_code")
		End If		
		
End choose

end event

event ue_postopen;call super::ue_postopen;Environment	le_env


GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

ib_reinquire = FALSE

This.PostEvent('ue_query')//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Weekpa"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_pas_pa_report_inq'

//iu_pas203 = Create u_pas203
iu_ws_pas2 = Create u_ws_pas2
iu_ws_orp4 = Create u_ws_orp4
wf_retrieve()

end event

event ue_set_data;//u_conversion_functions		lu_conv
//
//
Choose Case as_data_item
		
	Case 'weekly_daily_option'
		dw_header.SetItem(1, "weekly_daily_option", as_value)
		
	Case 'weekly_daily_option_desc'
		dw_header.SetItem(1, "option_description", as_value)

	Case 'display_option'
		dw_loads_boxes.SetItem(1, "boxes_loads", as_value)		
		
	Case 'plant_option'
		dw_header.SetItem(1, "plant_option", as_value)
		
	Case 'plant_option_desc'
		dw_header.object.plant_description_t.Text = as_value

	Case 'plant_option_text'
		dw_header.SetItem(1, "plant_description", as_value)
		
	Case 'product_option'
		dw_header.SetItem(1, "product_option", as_value)		
		
	Case 'product_option_desc'
		dw_header.object.product_description_t.Text = as_value

	Case 'product_option_text'
		dw_header.SetItem(1, "product_description", as_value)		
		
	Case 'location_type'
		dw_header.SetItem(1, "plant_type", as_value)		
		
	Case 'file_no'
		dw_header.SetItem(1, "file_no", as_value)		
		
	Case 'division_code'
		dw_header.SetItem(1, "division_code", as_value)		
		
	Case 'division_description'
		dw_header.SetItem(1, "division_description", as_value)	
		
	Case 'product_state'
		dw_header.SetItem(1, "product_state", as_value)	
		
	Case 'product_state_desc'
		dw_header.SetItem(1, "product_state_desc", as_value)		
		
	Case 'dw_product_status'
		dw_product_status_window.Reset()
		dw_product_status_window.ImportString(as_value)
		
	Case 'begin_date'
		dw_header.SetItem(1, "begin_date", Date(as_value))		
		
	Case 'plant_code'
		dw_header.SetItem(1, "plant_code", as_value)	
		
	Case 'location_group_system'
		dw_header.SetItem(1, "location_group_system", as_value)
		
	Case 'location_group_groupid'
		dw_header.SetItem(1, "location_group_groupid", as_value)
		
	Case 'product_group_system'
		dw_header.SetItem(1, "product_group_system", as_value)
		
	Case 'product_group_groupid'
		dw_header.SetItem(1, "product_group_groupid", as_value)
		
	Case 'ib_inquire'
		If as_value = 'True' Then
			ib_inquire = True
		Else
			ib_inquire = False
		End If
		
	Case 'check_boxes'
		is_check_boxes = as_value	
	Case 'save'
		is_save_file = as_value
End Choose
end event

event ue_addrow;call super::ue_addrow;String	ls_product_option	

Integer	li_line_no

Long		ll_RowCount, ll_sub


li_line_no = 0
ll_RowCount = dw_pa_detail.RowCount()
For ll_sub = 1 to ll_RowCount
	If dw_pa_detail.GetItemNumber(ll_sub, "line_no") > li_line_no Then
		li_line_no = dw_pa_detail.GetItemNumber(ll_sub, "line_no")
	End If
Next	
dw_pa_detail.SetItem(ll_RowCount, "line_no", li_line_no + 1)
dw_pa_detail.SetItem(ll_RowCount, "age_code", '  ')

dw_pa_detail.SetFocus()
dw_pa_detail.SetRow(ll_RowCount)
dw_pa_detail.SetColumn("product_code")
end event

event closequery;// changed to override ancester script
end event

event open;call super::open;// pjm 09/18/2014 - Want initial display option to default to 'B' for boxes - see ue_get_data
string ls_null

SetNull(ls_null)
dw_loads_boxes.SetItem(1,"boxes_loads",ls_null)

end event

type cb_find_next from commandbutton within w_pas_pa_report
event ue_find_product ( )
integer x = 37
integer y = 256
integer width = 435
integer height = 93
integer taborder = 70
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Find Next Product"
end type

event clicked;Parent.PostEvent("ue_find_product")
end event

type cbx_grade_sort from checkbox within w_pas_pa_report
integer x = 1276
integer y = 288
integer width = 603
integer height = 67
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Sort by Grade, Product"
boolean lefttext = true
end type

event clicked;If This.Checked = True Then
	dw_pa_detail.SetRedraw(False)
	dw_pa_detail.SetSort("line_no A, grade A, product_code A, product_state A, product_status A, plant_code A, seq_no A")
	dw_pa_detail.Sort()
	dw_pa_detail.SetRedraw(True)
Else
	dw_pa_detail.SetRedraw(False)
	dw_pa_detail.SetSort("line_no A, product_code A, product_state A, product_status A, plant_code A, seq_no A")
	dw_pa_detail.Sort()
	dw_pa_detail.SetRedraw(True)
End If
end event

type dw_scroll_product from datawindow within w_pas_pa_report
integer x = 512
integer y = 272
integer width = 585
integer height = 93
integer taborder = 60
string title = "none"
string dataobject = "d_scroll_product_pa_report"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;This.InsertRow(0)
 
end event

event editchanged;Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
string	ls_prod_code, &
			ls_reset, &
			ls_update_flag
integer	li_counter
Boolean	lb_yes_found
//
//
is_data = ' '
ll_row_count = dw_pa_detail.RowCount()
//
li_counter = 1
//
if ll_row_count < li_counter then Return 
//
Do While li_counter <= ll_Row_count 
	dw_pa_detail.SelectRow(li_counter, False)
	li_counter = li_counter + 1
loop
//
li_counter = 1
lb_yes_found = False
//
Do While li_counter <= ll_Row_count 
		ls_prod_code = dw_pa_detail.GetItemString(li_counter, "product_code")
		If pos(ls_prod_code, data) > 0 and pos(ls_prod_code, data) < 2 then
			lb_yes_found=True
		else	
			lb_yes_found=False
		end if
		if lb_yes_found = True then
			ii_row_count = li_counter
			is_data = data
			dw_pa_detail.SetRedraw(False)
			dw_pa_detail.ScrollToRow(li_counter)
			dw_pa_detail.SelectRow(li_counter, True)
			dw_pa_detail.SetRow(li_counter)
			dw_pa_detail.SetRedraw(True)
			li_counter = ll_row_count + 1
		else	
			li_counter = li_counter + 1	
		end if
Loop
//
if lb_yes_found=False then
	iw_Frame.SetMicroHelp("No matching products found")
else
	iw_Frame.SetMicroHelp("Product found")
end if
//
This.ResetUpdate()
end event

type dw_pa_detail from u_base_dw_ext within w_pas_pa_report
integer x = 37
integer y = 448
integer width = 3072
integer height = 1088
integer taborder = 60
string dataobject = "d_pas_pa_report_wk_dtl"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
end type

event clicked;call super::clicked;String	ls_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_expand, &
			ls_scroll_position 
			
Long		ll_sub, &
			ll_RowCount, &
			ll_line_no
			
//MessageBox("Clicked Event", "Got to Clicked Event")

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return

If dwo.Name = 'expand_ind' Then 

	ls_scroll_position = This.Object.DataWindow.VerticalScrollPosition
	ls_product_code = This.GetItemString(row, "product_code")
	ls_product_state = This.GetItemString(row, "product_state")
	ls_product_status = This.GetItemString(row, "product_status")	
	ls_expand = This.GetItemString(row, "expand_ind")
	ll_line_no = This.GetItemNumber(row, "line_no")

	
	dw_pa_detail.SetRedraw(False)
	This.SetFilter("")	
	This.Filter()
	
	ll_RowCount = This.RowCount()

	ll_sub = 1

	Do Until (ll_sub > ll_RowCount)
		
		If (This.GetItemString(ll_sub, "product_code") = ls_product_code) and &
			(This.GetItemString(ll_sub, "product_state") = ls_product_state) and &
			(This.GetItemString(ll_sub, "product_status") = ls_product_status) and &
			(This.GetItemNumber(ll_sub, "line_no") = ll_line_no) Then
			If ls_expand = "+" Then
				This.SetItem(ll_sub, "visible_ind", 'Y')
				If This.GetItemString (ll_sub, "plant_code") = '   ' Then
					This.SetItem(ll_sub, "expand_ind", '-')
				End If
			Else
				If ls_expand = '-' Then
					If This.GetItemString (ll_sub, "plant_code") = '   ' Then
						This.SetItem(ll_sub, "visible_ind", 'Y')
						This.SetItem(ll_sub, "expand_ind", '+')
					Else
						This.SetItem(ll_sub, "visible_ind", 'N')
					End If
				End If
			End If
		End if
		ll_sub ++
	Loop	
		

	wf_filter()
	This.Object.DataWindow.VerticalScrollPosition = ls_scroll_position 
End If
end event

event constructor;call super::constructor;ib_updateable = True
end event

type cbx_expand_all from u_base_checkbox_ext within w_pas_pa_report
integer x = 2392
integer y = 128
integer width = 519
integer height = 74
integer textsize = -8
fontcharset fontcharset = ansi!
string facename = "MS Sans Serif"
string text = "Expand All Plants:  "
boolean lefttext = true
end type

event clicked;call super::clicked;Long	ll_sub, &
		ll_RowCount

dw_pa_detail.SetRedraw(False)
dw_pa_detail.SetFilter("")	
dw_pa_detail.Filter()

ll_RowCount = dw_pa_detail.RowCount()

For ll_sub = 1 to ll_RowCount
	
	If This.Checked = True Then
		dw_pa_detail.SetItem(ll_sub, "visible_ind", 'Y')
		If dw_pa_detail.GetItemString (ll_sub, "plant_code") = '   ' Then
			dw_pa_detail.SetItem(ll_sub, "expand_ind", '-')
		End If
	Else
		If dw_pa_detail.GetItemString (ll_sub, "plant_code") = '   ' Then
			dw_pa_detail.SetItem(ll_sub, "expand_ind", '+')
			dw_pa_detail.SetItem(ll_sub, "visible_ind", 'Y')
		Else	
			dw_pa_detail.SetItem(ll_sub, "visible_ind", 'N')
		End If
	End If	
Next

wf_filter()

dw_pa_detail.SetRedraw(True)
end event

type dw_product_status_window from u_base_dw_ext within w_pas_pa_report
boolean visible = false
integer x = 3291
integer y = 64
integer width = 1938
integer height = 96
integer taborder = 50
string dataobject = "d_product_status_2"
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_header from u_base_dw_ext within w_pas_pa_report
integer width = 1975
integer height = 224
integer taborder = 50
string dataobject = "d_pas_pa_report_hdr"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_loads_boxes from u_loads_boxes within w_pas_pa_report
integer x = 1935
integer height = 275
integer taborder = 40
boolean bringtotop = true
end type

event itemchanged;Long					ll_count


dw_pa_detail.SetRedraw(False)
dw_pa_detail.SetFilter("")	
dw_pa_detail.Filter()

For ll_count = 1 to dw_pa_detail.RowCount()
	dw_pa_detail.SetItem(ll_count,"loads_boxes", data)
Next

Parent.Post wf_format()

wf_filter()

dw_pa_detail.SetRedraw(True)



end event

type cbx_neg_selection from checkbox within w_pas_pa_report
integer x = 2194
integer y = 282
integer width = 720
integer height = 77
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Display Negative Rows Only:  "
boolean lefttext = true
end type

event clicked;wf_filter()
end event

