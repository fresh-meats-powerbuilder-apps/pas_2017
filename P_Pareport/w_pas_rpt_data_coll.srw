HA$PBExportHeader$w_pas_rpt_data_coll.srw
forward
global type w_pas_rpt_data_coll from w_base_sheet_ext
end type
type dw_pas_test_upd from u_base_dw_ext within w_pas_rpt_data_coll
end type
type dw_division from u_division within w_pas_rpt_data_coll
end type
end forward

global type w_pas_rpt_data_coll from w_base_sheet_ext
integer x = 910
integer y = 420
integer width = 1723
string title = "PA Report Data Collection"
long backcolor = 12632256
dw_pas_test_upd dw_pas_test_upd
dw_division dw_division
end type
global w_pas_rpt_data_coll w_pas_rpt_data_coll

type variables
u_pas203		iu_pas203

u_ws_pas2       iu_ws_pas2

s_error		istr_error_info

long		il_last_clicked_row


end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_print ()
end prototypes

public function boolean wf_retrieve ();String	ls_paspdav, &
			ls_input, &
			ls_header

Long		ll_rec_count

IF Not Super::wf_retrieve() Then Return False

SetPointer(HourGlass!)

ls_header  = dw_division.uf_get_division() + "~r~n"

istr_error_info.se_event_name = "wf_retrieve"

dw_pas_test_upd.Reset()

//If iu_pas203.nf_inq_paspdav(istr_error_info, &
//								ls_header, &
//								"   ",&							
//								ls_paspdav) < 0 then return false

If iu_ws_pas2.nf_pasp31fr(ls_header, &
								"   ",&							
								ls_paspdav, &
								istr_error_info) < 0 then return false	

If Not iw_frame.iu_string.nf_IsEmpty(ls_paspdav) Then
	ll_rec_count = dw_pas_test_upd.ImportString(ls_paspdav)
	If ll_rec_count > 0 then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
	dw_pas_test_upd.ResetUpdate()
End if

return True
end function

public function boolean wf_update ();Long		ll_row_count, &
			ll_ind

String 	ls_update_string, &
			ls_header

ls_update_string = ""

SetRedraw(False)
dw_pas_test_upd.SetFilter("IsSelected()")
dw_pas_test_upd.Filter()

ll_row_count = dw_pas_test_upd.RowCount()

FOR ll_ind = 1 TO ll_row_count
	ls_update_string += dw_pas_test_upd.GetItemString(ll_ind, "plant_code")+"~r~n"
NEXT

dw_pas_test_upd.SetFilter("")
dw_pas_test_upd.Filter()
SetRedraw(True)

ls_header  = dw_division.uf_get_division() + "~r~n"

istr_error_info.se_event_name = "wf_update"

//if iu_pas203.nf_upd_paspdav(istr_error_info, &
//											ls_update_string, &
//											ls_header) < 0 Then Return False
											
if iu_ws_pas2.nf_pasp32fr(ls_header, &
								   ls_update_string, &
								   istr_error_info) < 0 Then Return False											
											
if ls_update_string >"" then SetMicroHelp("Update Started")		

dw_pas_test_upd.SelectRow(0, FALSE)

									

Return TRUE
end function

public subroutine wf_print ();
end subroutine

event close;call super::close;Destroy iu_pas203
Destroy iu_ws_pas2
end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
end on

event resize;call super::resize;//integer li_x		= 1
//integer li_y		= 1

//dw_pas_test_upd.width	= width - (50 + li_x)
//dw_pas_test_upd.height	= height - (226 + li_y)
//

integer li_x		= 51
integer li_y		= 227


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


dw_pas_test_upd.width	= width - (li_x)
dw_pas_test_upd.height	= height - (li_y)

end event

event ue_postopen;call super::ue_postopen;iu_pas203 = create u_pas203
iu_ws_pas2 = create u_ws_pas2

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "DatColl"
istr_error_info.se_user_id				= sqlca.userid

is_inquire_window_name = 'w_division_inq'

This.PostEvent('ue_query')
end event

on activate;call w_base_sheet_ext::activate;iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
end on

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

on w_pas_rpt_data_coll.create
int iCurrent
call super::create
this.dw_pas_test_upd=create dw_pas_test_upd
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_pas_test_upd
this.Control[iCurrent+2]=this.dw_division
end on

on w_pas_rpt_data_coll.destroy
call super::destroy
destroy(this.dw_pas_test_upd)
destroy(this.dw_division)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'division'
		Message.StringParm = dw_division.uf_get_division()
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'division'
		dw_division.uf_set_division(as_value)
End Choose

end event

event ue_fileprint;call super::ue_fileprint;DataStore				lds_print
DataWindowChild		ldwc_division, &
							ldwc_detail


lds_print = Create DataStore

lds_print.DataObject = 'd_pas_rpt_data_coll_prt'

lds_print.GetChild('dw_division', ldwc_division)
lds_print.GetChild('dw_detail', ldwc_detail)

dw_division.ShareData(ldwc_division)
dw_pas_test_upd.ShareData(ldwc_detail)


lds_print.print()

destroy lds_print


end event

type dw_pas_test_upd from u_base_dw_ext within w_pas_rpt_data_coll
integer x = 9
integer y = 100
integer width = 1097
integer height = 1304
string dataobject = "d_pas_rpt_data_coll"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event clicked;call super::clicked;Long	ll_clicked_row
int	li_idx

ll_clicked_row = row

IF ll_clicked_row = 0 THEN RETURN

IF KeyDown(KeyShift!) THEN
	SetRedraw(FALSE)
	IF il_last_clicked_row > ll_clicked_row THEN
 		FOR li_idx = il_last_clicked_row TO ll_clicked_row STEP -1
			SelectRow(li_idx, TRUE)
		END FOR
	ELSE	
		FOR li_idx = il_last_clicked_row TO ll_clicked_row 
			SelectRow(li_idx, TRUE)
		END FOR
	END IF
	SetRedraw (TRUE)
	Return
ELSE
	IF IsSelected(ll_clicked_row) THEN
		This.SelectRow(ll_clicked_row, FALSE)
	ELSE
		This.SelectRow(ll_clicked_row, TRUE)
	END IF
END IF

il_last_clicked_row = ll_clicked_row
end event

type dw_division from u_division within w_pas_rpt_data_coll
integer x = 5
integer y = 8
end type

event constructor;call super::constructor;This.Disable()
end event

