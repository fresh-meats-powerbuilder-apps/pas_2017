HA$PBExportHeader$w_weekly_source_summary_inq.srw
forward
global type w_weekly_source_summary_inq from w_base_response_ext
end type
type dw_plant_type from u_plant_type within w_weekly_source_summary_inq
end type
type dw_week_end_date from u_week_end_date within w_weekly_source_summary_inq
end type
type dw_header from u_base_dw_ext within w_weekly_source_summary_inq
end type
type dw_fab_product_code from u_fab_product_code within w_weekly_source_summary_inq
end type
end forward

global type w_weekly_source_summary_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1883
integer height = 868
string title = ""
long backcolor = 67108864
dw_plant_type dw_plant_type
dw_week_end_date dw_week_end_date
dw_header dw_header
dw_fab_product_code dw_fab_product_code
end type
global w_weekly_source_summary_inq w_weekly_source_summary_inq

type variables

end variables

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event close;call super::close;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String	ls_plant_type, &
			ls_fab_product, &
			ls_week_end_date, &
			ls_product_status
			
Date		ldt_week_end_date

u_string_functions		lu_strings


IF dw_week_end_date.AcceptText() = -1 Then Return

ls_plant_type = dw_plant_type.uf_get_plant_type()
//12-12 jac
//ls_product_status = dw_product_status.uf_get_product_status()
//
If lu_strings.nf_IsEmpty(ls_plant_type) Then
	iw_frame.SetMicroHelp('Plant Type is a required field')
	dw_plant_type.SetFocus()
	return
End if

If Not dw_fab_product_code.uf_validate( ) Then Return
	
ls_fab_product = dw_fab_product_code.uf_exportstring( )

ldt_week_end_date = dw_week_end_date.uf_get_week_end_date()
ls_week_end_date = String(ldt_week_end_date, 'yyyy-mm-dd')
	
iw_parentwindow.Event ue_set_data('plant_type',ls_plant_type)
iw_parentwindow.Event ue_set_data('fab_product',ls_fab_product)
iw_parentwindow.Event ue_set_data('week_end_date',ls_week_end_date)
iw_parentwindow.Event ue_set_data('actual_projected_ind', &
		dw_header.GetItemString(1, 'actual_projected_ind'))
//12-12 jac
//iw_parentwindow.Event ue_set_data('product_status',ls_product_status)
//
ib_ok_to_close = True

Close(This)

end event

event ue_postopen;call super::ue_postopen;DataWindowChild			ldwc_child, &
								ldwc_parent_child
String						ls_product
								

iw_parentwindow.Event ue_get_data('plant_group')
dw_plant_type.uf_set_plant_type(message.StringParm)

iw_parentwindow.Event ue_get_data('fab_product')
ls_product = message.StringParm

dw_fab_product_code.uf_importstring( ls_product, true)

iw_parentwindow.Event ue_get_data('week_end_date')
dw_week_end_date.uf_set_week_end_date(Date(message.StringParm))

iw_parentwindow.Event ue_get_data('actual_projected_ind')
dw_header.SetItem(1, 'actual_projected_ind', message.StringParm)

dw_plant_type.SetFocus()
end event

on w_weekly_source_summary_inq.create
int iCurrent
call super::create
this.dw_plant_type=create dw_plant_type
this.dw_week_end_date=create dw_week_end_date
this.dw_header=create dw_header
this.dw_fab_product_code=create dw_fab_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant_type
this.Control[iCurrent+2]=this.dw_week_end_date
this.Control[iCurrent+3]=this.dw_header
this.Control[iCurrent+4]=this.dw_fab_product_code
end on

on w_weekly_source_summary_inq.destroy
call super::destroy
destroy(this.dw_plant_type)
destroy(this.dw_week_end_date)
destroy(this.dw_header)
destroy(this.dw_fab_product_code)
end on

type cb_base_help from w_base_response_ext`cb_base_help within w_weekly_source_summary_inq
integer x = 1093
integer y = 576
integer taborder = 70
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_weekly_source_summary_inq
integer x = 809
integer y = 576
integer taborder = 60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_weekly_source_summary_inq
integer x = 530
integer y = 576
integer taborder = 50
end type

type dw_plant_type from u_plant_type within w_weekly_source_summary_inq
integer x = 183
integer y = 36
integer taborder = 10
end type

event constructor;call super::constructor;This.InsertRow(0)
This.uf_Enable(True)
end event

type dw_week_end_date from u_week_end_date within w_weekly_source_summary_inq
event ue_post_constructor ( )
integer x = 27
integer y = 448
integer width = 923
integer height = 92
integer taborder = 30
end type

event ue_post_constructor;call super::ue_post_constructor;This.InsertRow(0)
This.uf_enable(True)

end event

event constructor;call super::constructor;This.PostEvent('ue_post_constructor')
end event

type dw_header from u_base_dw_ext within w_weekly_source_summary_inq
integer x = 1010
integer y = 448
integer width = 439
integer height = 76
integer taborder = 40
string dataobject = "d_source_rpt_header"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
This.object.actual_projected_ind.TabSequence = 10
end event

type dw_fab_product_code from u_fab_product_code within w_weekly_source_summary_inq
integer x = 210
integer y = 124
integer taborder = 11
boolean bringtotop = true
end type

