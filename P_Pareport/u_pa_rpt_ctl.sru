HA$PBExportHeader$u_pa_rpt_ctl.sru
forward
global type u_pa_rpt_ctl from datawindow
end type
end forward

global type u_pa_rpt_ctl from datawindow
integer width = 1609
integer height = 100
integer taborder = 10
string dataobject = "d_pa_rpt_ctl"
boolean border = false
boolean livescroll = true
end type
global u_pa_rpt_ctl u_pa_rpt_ctl

forward prototypes
public function string uf_get_shift ()
public subroutine uf_set_shift (string as_shift)
public function integer uf_enable (boolean ab_enable)
public function integer uf_modified ()
end prototypes

public function string uf_get_shift ();Return This.GetItemString(1, "shift")

end function

public subroutine uf_set_shift (string as_shift);	This.SetItem(1,"shift",as_shift)
  
end subroutine

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.shift.Background.Color = 16777215
	This.object.shift.Protect = 0
Else
	This.object.shift.Background.Color = 12632256
	This.object.shift.Protect = 1
End If

Return 1
end function

public function integer uf_modified ();return 0
end function

event constructor;DataWindowChild		ldwc_type


This.GetChild("parptctl", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("parptctl")
This.InsertRow(0)


end event

on u_pa_rpt_ctl.create
end on

on u_pa_rpt_ctl.destroy
end on

