$PBExportHeader$w_prd_comments_inq.srw
forward
global type w_prd_comments_inq from w_base_response
end type
type dw_plant from u_plant within w_prd_comments_inq
end type
type dw_shift from u_shift within w_prd_comments_inq
end type
type dw_area from u_sched_area_sect within w_prd_comments_inq
end type
type dw_sched_date from u_sched_date within w_prd_comments_inq
end type
end forward

global type w_prd_comments_inq from w_base_response
integer x = 1075
integer y = 485
integer width = 1961
integer height = 960
string title = "Schedule Comments Inquire "
long backcolor = 67108864
event ue_query pbm_custom70
dw_plant dw_plant
dw_shift dw_shift
dw_area dw_area
dw_sched_date dw_sched_date
end type
global w_prd_comments_inq w_prd_comments_inq

type variables
Boolean		ib_valid_return, &
				ib_newplant
w_base_sheet	iw_parent

u_pas201		iu_pas201

s_error		istr_error_info

DataWindowChild	idwc_temp

u_sect_functions	iu_sect_functions
end variables

on w_prd_comments_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_shift=create dw_shift
this.dw_area=create dw_area
this.dw_sched_date=create dw_sched_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_shift
this.Control[iCurrent+3]=this.dw_area
this.Control[iCurrent+4]=this.dw_sched_date
end on

on w_prd_comments_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_shift)
destroy(this.dw_area)
destroy(this.dw_sched_date)
end on

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

If dw_area.RowCount() = 0 Then
	dw_area.InsertRow(0)
End If

If dw_sched_date.RowCount() = 0 Then
	dw_sched_date.InsertRow(0)
End If

If dw_shift.RowCount() = 0 Then
	dw_shift.InsertRow(0)
End If

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;String	ls_temp, &
			ls_SearchString
Long 		ll_rtn
Integer	li_rtn, &
			li_temp

If dw_plant.AcceptText() = -1 or &
	dw_area.AcceptText() = -1  or &
	dw_sched_date.AcceptText() = -1  or &
	dw_shift.AcceptText() = -1  then
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End if
//String(This.dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd')

If iw_frame.iu_string.nf_IsEmpty(dw_area.GetItemString(1, "area_name_code")) Then
	iw_frame.SetMicroHelp("Area is a required field")
	dw_area.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(String(dw_sched_date.GetItemDate(1, "sched_date"),'yyyy-mm-dd')) Then
	iw_frame.SetMicroHelp("Date is a required field")
	dw_sched_date.SetFocus()
	return
End if

ls_temp = dw_shift.uf_get_shift( )
iw_parent.Event ue_Set_Data('Shift', ls_temp)


If iw_frame.iu_string.nf_IsEmpty(dw_shift.GetItemString(1, "shift")) Then
	iw_frame.SetMicroHelp("Shift is a required field")
	dw_shift.SetFocus()
	return
End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant Desc', ls_temp)

ls_temp = 'AREA'
iw_parent.Event ue_Set_Data('Name Type', ls_temp)

ls_temp = dw_area.GetItemString(1, "area_name_code")
iw_parent.Event ue_Set_Data('Name Code', ls_temp)


ls_temp = String(dw_sched_date.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd')
iw_parent.Event ue_Set_Data('Sched Date', string(ls_temp))

ls_temp = dw_shift.GetItemString(1, "shift")
iw_parent.Event ue_Set_Data('Shift', ls_temp)

ls_Searchstring = "area_name_code = "
ls_Searchstring += dw_area.GetItemString(1, "area_name_code")
								
ll_rtn = idwc_temp.Find  &
				( ls_SearchString, 1, idwc_temp.RowCount())
				
ls_temp = dw_area.uf_get_area_descr()
iw_parent.Event ue_Set_Data('Type Descr', ls_temp)				

ib_valid_return = True
Close(This)
end event

event ue_postopen;call super::ue_postopen;Int							li_pos, &
								li_ret
								
String						ls_sect_name, &
								ls_temp

u_string_functions		lu_string

Environment					le_env


iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
	dw_area.SetItem(1,"plant_code", ls_temp)
	dw_area.uf_get_plt_codes(ls_temp)
	dw_area.AcceptText()
End If
iw_parent.Event ue_Get_Data('Name Code')
ls_sect_name = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_sect_name) Then 
	dw_area.SetItem(1,"area_name_code",ls_sect_name)
End If

iw_parent.Event ue_Get_Data('Sched Date')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_sched_date.SetItem(1,"sched_date",ls_temp)
End If

iw_parent.Event ue_Get_Data('Shift')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_shift.SetItem(1,"shift",ls_temp)
End If

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Schedule Comments"
istr_error_info.se_user_id = sqlca.userid

//This.PostEvent("ue_query")



end event
type cb_base_help from w_base_response`cb_base_help within w_prd_comments_inq
boolean visible = false
integer x = 1129
integer y = 596
integer taborder = 0
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_prd_comments_inq
integer x = 539
integer y = 432
integer taborder = 70
end type

type cb_base_ok from w_base_response`cb_base_ok within w_prd_comments_inq
integer x = 165
integer y = 428
integer taborder = 60
end type

type dw_plant from u_plant within w_prd_comments_inq
integer x = 82
integer y = 48
integer taborder = 10
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;String		ls_ColName

ls_ColName = GetColumnName()

CHOOSE CASE ls_ColName
	CASE "location_code"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid location")
			This.selecttext(1,100)
			return 1
		Else
			ib_NewPlant = True
			dw_area.Reset()
			dw_area.InsertRow(0)
		End If
End Choose
end event

event losefocus;call super::losefocus;String	ls_PlantCode

ls_PlantCode = This.GetItemString(1,"location_code")

//set the hidden plant code on the next window to 
//be used to retrieve the area name codes.  
dw_area.SetItem(1,"plant_code", ls_PlantCode)
dw_area.AcceptText()

//set this to true for the clicked event on the area window
ib_NewPlant = True
end event

type dw_shift from u_shift within w_prd_comments_inq
integer x = 91
integer y = 288
integer height = 92
integer taborder = 40
boolean bringtotop = true
end type

type dw_area from u_sched_area_sect within w_prd_comments_inq
integer y = 120
integer width = 923
integer height = 84
integer taborder = 20
boolean bringtotop = true
boolean maxbox = false
end type

event clicked;call super::clicked;String		ls_ColName, &
				ls_plant_code

ls_ColName = GetColumnName()


//ib_NewPlant is set to true when the plant window loses focus
If ib_NewPlant Then
	CHOOSE CASE ls_ColName
		CASE "area_name_code"
			ls_plant_code = This.GetItemString(1,"plant_code")
			If isnull(ls_plant_code) Then
				iw_frame.SetMicroHelp("This is not a valid plant")
				return 1
			Else
				This.SetRedraw( False )
				super::uf_get_plt_codes(ls_plant_code)
				This.AcceptText()
				This.SetRedraw( True )
			End If
	End Choose
	ib_NewPlant = False
End If
return 0

end event

event constructor;call super::constructor;This.SetTabOrder("sect_name_code", 0)
end event

type dw_sched_date from u_sched_date within w_prd_comments_inq
integer x = 18
integer y = 204
integer taborder = 30
boolean bringtotop = true
end type

