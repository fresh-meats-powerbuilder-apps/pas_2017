$PBExportHeader$w_prd_not_sched_inq.srw
forward
global type w_prd_not_sched_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_prd_not_sched_inq
end type
type dw_choice from datawindow within w_prd_not_sched_inq
end type
type dw_sched_date from u_sched_date within w_prd_not_sched_inq
end type
type dw_shift from u_shift within w_prd_not_sched_inq
end type
type gb_1 from groupbox within w_prd_not_sched_inq
end type
end forward

global type w_prd_not_sched_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 1481
integer height = 904
string title = "Product Not Scheduled Inquire"
long backcolor = 67108864
event begindatechanged ( )
event ue_query pbm_custom01
dw_plant dw_plant
dw_choice dw_choice
dw_sched_date dw_sched_date
dw_shift dw_shift
gb_1 gb_1
end type
global w_prd_not_sched_inq w_prd_not_sched_inq

type variables
Boolean		ib_valid_return

String		is_input_string
w_base_sheet	iw_parent

w_raw_mat_sched		iw_parent_window

s_error		istr_error_info

u_pas201		iu_pas201

DataWindowChild		idwc_exist_prod, &
							idddw_child
							

end variables

event ue_postopen;call super::ue_postopen;DataWindowChild			ldwc_header

Int							li_pos, &
								li_ret, &
								li_null

String						ls_header, &
								ls_temp, &
								ls_option, &
								ls_shift, &
								ls_date

u_string_functions		lu_string

DataWindowChild			ldwc_state

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If

iw_parent.Event ue_Get_Data('Row Option')
ls_option = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_option) Then 
	dw_choice.SetItem(1,'choice', ls_option)
Else
	dw_choice.SetItem(1,'choice', 'A')
End If

If ls_option = 'N' Then
	iw_parent.Event ue_Get_Data('shift')
	ls_shift = Message.StringParm
	if Not lu_string.nf_IsEmpty(ls_shift) Then dw_shift.uf_set_shift(ls_shift)
	
	iw_parent.Event ue_get_data('sched_date')
	ls_date = Message.StringParm
	if Not lu_string.nf_IsEmpty(ls_date) Then dw_sched_date.uf_set_sched_date(date(ls_date))
Else
	dw_sched_date.Enabled = False
	dw_sched_date.visible = False
	dw_shift.Enabled = False
	dw_shift.visible = False
End If

If dw_plant.RowCount() < 1 Then 
	dw_plant.InsertRow(0)
End if

If dw_shift.RowCount() < 1 Then 
	dw_shift.InsertRow(0)
End if

If dw_sched_date.RowCount() < 1 Then 
	dw_sched_date.InsertRow(0)
End if



end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

if dw_shift.rowcount( ) = 0 Then
	dw_shift.insertrow( 0)
End If

if dw_sched_date.rowcount( ) = 0 Then
	dw_sched_date.insertrow( 0)
End If

if dw_choice.rowcount( ) = 0 Then
	dw_choice.insertrow( 0)
End If
end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

on w_prd_not_sched_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_choice=create dw_choice
this.dw_sched_date=create dw_sched_date
this.dw_shift=create dw_shift
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_choice
this.Control[iCurrent+3]=this.dw_sched_date
this.Control[iCurrent+4]=this.dw_shift
this.Control[iCurrent+5]=this.gb_1
end on

on w_prd_not_sched_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_choice)
destroy(this.dw_sched_date)
destroy(this.dw_shift)
destroy(this.gb_1)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date	ldt_temp

String	ls_temp, &
			ls_temp1, &
			ls_choice, &
			ls_data, &
			ls_SearchString, &
			ls_PrdPrdstate
			
Long 		ll_rtn, &
			ll_len

If dw_plant.AcceptText() = -1 Then 
	dw_plant.SetFocus()
	return
End if

dw_choice.AcceptText()
dw_sched_date.AcceptText()
dw_shift.AcceptText()

ls_choice = dw_choice.GetItemString(1,'choice')

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End if


If ls_choice = 'N' Then
	If iw_frame.iu_string.nf_IsEmpty(dw_shift.GetItemString(1, "Shift")) Then
		iw_frame.SetMicroHelp("Shift is a required field")
		dw_shift.SetFocus()
		return
	End if

//	If iw_frame.iu_string.nf_IsEmpty(dw_sched_date.GetItemDate(1, "sched_date")) Then
//		iw_frame.SetMicroHelp("Date is a required field")
//		dw_sched_date .SetFocus()
//		return
//	End if
End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant Desc', ls_temp)

ls_temp = dw_choice.GetItemString(1, 'choice')
iw_parent.Event ue_Set_Data('Row Option', ls_temp)

If ls_choice = 'N' Then
	ls_temp = dw_shift.uf_get_shift( )
	iw_parent.Event ue_Set_Data('shift', ls_temp)
	
	ldt_temp = dw_sched_date.GetItemDate(1, "sched_date")

	iw_parent.Event ue_set_data('sched_date', &
		String(ldt_temp, 'yyyy-mm-dd'))
End If


ib_valid_return = True
Close(This)
end event
type cb_base_help from w_base_response_ext`cb_base_help within w_prd_not_sched_inq
boolean visible = false
integer x = 1431
integer y = 636
integer taborder = 0
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_prd_not_sched_inq
integer x = 558
integer y = 644
integer taborder = 60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_prd_not_sched_inq
integer x = 55
integer y = 644
integer taborder = 50
end type

type dw_plant from u_plant within w_prd_not_sched_inq
integer x = 37
integer y = 12
integer taborder = 10
end type

type dw_choice from datawindow within w_prd_not_sched_inq
integer x = 110
integer y = 132
integer width = 1070
integer height = 208
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_not_sched_choice"
boolean border = false
boolean livescroll = true
end type

event itemchanged;String			ls_choice

ls_choice = data

If ls_choice = 'N' Then
	dw_sched_date.Enabled = True
	dw_sched_date.visible = True
	dw_shift.Enabled = True
	dw_shift.visible = True
Else
	dw_sched_date.Enabled = False
	dw_sched_date.visible = False
	dw_shift.Enabled = False
	dw_shift.visible = False
End If
end event

type dw_sched_date from u_sched_date within w_prd_not_sched_inq
integer y = 500
integer taborder = 40
boolean bringtotop = true
end type

type dw_shift from u_shift within w_prd_not_sched_inq
integer x = 78
integer y = 412
integer taborder = 30
boolean bringtotop = true
end type

type gb_1 from groupbox within w_prd_not_sched_inq
integer x = 69
integer y = 104
integer width = 1381
integer height = 244
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

