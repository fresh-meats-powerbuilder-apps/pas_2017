﻿$PBExportHeader$u_excel.sru
$PBExportComments$IBDKDLD
forward
global type u_excel from nonvisualobject
end type
end forward

global type u_excel from nonvisualobject
end type
global u_excel u_excel

forward prototypes
public function long uf_openworkbook (string as_filename, ref oleobject ao_workbook, string as_plants)
public function integer uf_add_sheets ()
public function long uf_connect_excel ()
end prototypes

public function long uf_openworkbook (string as_filename, ref oleobject ao_workbook, string as_plants);Long 						ll_result
Boolean 					lb_IsOpen 
Integer 					li_count
String 					ls_filename,ls_plants,ls_file

oleobject 				ole_Excel
u_string_functions 	lu_string


// First obtain the filename without the path information
ls_filename = as_filename
Do While Pos(ls_filename, "\") > 0
	ls_filename = Mid(ls_filename, Pos(ls_filename, "\") + 1)
Loop
ls_file = ls_filename

// Connect to Excel; use existing instance if possible
ole_Excel = CREATE oleobject
ll_result = ole_Excel.ConnectToObject("", "Excel.Application")
If ll_result = -5 Then 
	// Excel isn't running, so start it
	ll_result = ole_Excel.ConnectToNewObject("Excel.Application")
End If


If ll_result = 0 Then
	// Make sure excel is visable
	ole_Excel.Application.Visible = True
	/////////////////////////////////////

	// Try to see if the workbook is open already
	For li_count = 1 to ole_Excel.Application.Workbooks.Count
		If Lower(ole_Excel.Application.Workbooks(li_count).Name) = Lower(ls_file)  Then
			// The particular workbook is already open
			ao_Workbook = ole_Excel.Application.Workbooks(li_count)
			lb_IsOpen = True
			Exit
		End If
	Next
	
	If Not lb_IsOpen Then
		If FileExists(as_filename) Then
			// Open the workbook that already exists
			ole_Excel.Application.Workbooks.Open(as_filename)
			// Set the name of the current sheet by default you must have a sheet on the spreadsheet
			
			ls_plants = lu_string.nf_gettoken(as_plants,'~t')
			ole_Excel.Worksheets(1).name = ls_Plants 
			// copy the data from the default sheet to the clipboard
			ole_Excel.Worksheets(1).UsedRange.Copy
			
			DO UNTIL as_plants = ''
				ls_plants = lu_string.nf_gettoken(as_plants,'~t')
				//Add a new worksheet
				//This.uf_add_sheets()
				ole_Excel.Worksheets.Add
				// Paste the contents of the clipboard
				ole_Excel.worksheets(1).Range("A1").PasteSpecial
				// Name the new sheet just created
				ole_Excel.Worksheets(1).name = ls_plants
			LOOP
			// clear the Clipboard of data
			Clipboard ( '' )
		
			

//			// Create worksheets and populate them
//				ole_Excel.Worksheets.Add
//				ole_Excel.worksheets(1).Range("A1").PasteSpecial
//				ole_Excel.worksheets(1).cells(1,1).value = 14
//				ole_Excel.worksheets(1).cells(2,1).value = 24
//				ole_Excel.worksheets(1).cells(1,2).value = 24
//				ole_Excel.worksheets(1).cells(1,3).value = 5
//				ole_Excel.worksheets(1).cells(5,5).value = "=A2-A1"
//			// Name the worksheet just added
//				ole_Excel.Worksheets(1).name = 'dave ' + string(ll_count)
//			NEXT
		Else
			// Create an empty workbook file and save it in the location specified
			ole_Excel.Application.Workbooks.Add()
			ole_Excel.Application.Workbooks(1).SaveAs(as_filename)
		End If
		ao_Workbook = ole_Excel.Application.Workbooks(ls_file)
	End If
End If

DESTROY ole_Excel
Return ll_result
end function

public function integer uf_add_sheets ();oleobject 	ole_Excel
Long 		 	ll_result
ole_Excel = CREATE oleobject

// I am 
ll_result = ole_Excel.ConnectToObject("", "Excel.Application")
ole_Excel.Worksheets.Add

Return ll_result
end function

public function long uf_connect_excel ();Long 						ll_result
oleobject 				ole_Excel

// Connect to Excel; use existing instance if possible
ole_Excel = CREATE oleobject
ll_result = ole_Excel.ConnectToObject("", "Excel.Application")
If ll_result = -5 Then 
	// Excel isn't running, so start it
	ll_result = ole_Excel.ConnectToNewObject("Excel.Application")
End If

Return ll_result
end function

on u_excel.create
TriggerEvent( this, "constructor" )
end on

on u_excel.destroy
TriggerEvent( this, "destructor" )
end on

