﻿$PBExportHeader$w_grading_forecast_inq.srw
$PBExportComments$IBDKDLD
forward
global type w_grading_forecast_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_grading_forecast_inq
end type
type dw_begin_date from u_effective_date within w_grading_forecast_inq
end type
end forward

global type w_grading_forecast_inq from w_base_response_ext
integer width = 1801
integer height = 472
long backcolor = 67108864
dw_plant dw_plant
dw_begin_date dw_begin_date
end type
global w_grading_forecast_inq w_grading_forecast_inq

on w_grading_forecast_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_begin_date=create dw_begin_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_begin_date
end on

on w_grading_forecast_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_begin_date)
end on

event ue_base_ok();call super::ue_base_ok;String		ls_plant, &
 				ls_plant_desc, &
				ls_date
				
Int			li_pa_range				

u_string_functions		lu_strings


If dw_plant.AcceptText() = -1 or dw_begin_date.AcceptText() = -1 Then return 
	
ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End if

//dw_begin_date.uf_validate('Begin Date:','PA')
ls_date = string(dw_begin_date.uf_get_effective_date())

iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('begin_date',ls_date)

ib_ok_to_close = True

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen(unsignedlong wparam, long lparam);call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('begin_date')
if Message.StringParm > ' ' Then
	dw_begin_date.uf_set_effective_date(date(Message.StringParm))
Else
	dw_begin_date.SetItem(1,"effective_date", Today())
end if

dw_begin_date.uf_initilize('Begin Date:','PA_7')








end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_grading_forecast_inq
integer x = 1147
integer y = 248
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_grading_forecast_inq
integer x = 859
integer y = 248
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_grading_forecast_inq
integer x = 571
integer y = 248
integer taborder = 20
end type

type dw_plant from u_plant within w_grading_forecast_inq
integer x = 270
integer y = 12
integer taborder = 10
boolean bringtotop = true
end type

type dw_begin_date from u_effective_date within w_grading_forecast_inq
integer x = 50
integer y = 112
integer taborder = 11
boolean bringtotop = true
end type

