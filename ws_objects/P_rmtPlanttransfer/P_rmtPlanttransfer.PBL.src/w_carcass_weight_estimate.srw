﻿$PBExportHeader$w_carcass_weight_estimate.srw
forward
global type w_carcass_weight_estimate from w_base_sheet_ext
end type
type dw_satellite_cattle from u_base_dw_ext within w_carcass_weight_estimate
end type
type dw_plant from u_plant within w_carcass_weight_estimate
end type
type dw_last_updated from u_last_updated_combined within w_carcass_weight_estimate
end type
type dw_effective_date from u_effective_date within w_carcass_weight_estimate
end type
type dw_shift from u_shift within w_carcass_weight_estimate
end type
type cbx_update_b from checkbox within w_carcass_weight_estimate
end type
end forward

global type w_carcass_weight_estimate from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 1485
integer height = 1565
long backcolor = 67108864
dw_satellite_cattle dw_satellite_cattle
dw_plant dw_plant
dw_last_updated dw_last_updated
dw_effective_date dw_effective_date
dw_shift dw_shift
cbx_update_b cbx_update_b
end type
global w_carcass_weight_estimate w_carcass_weight_estimate

type variables
u_rmt001		iu_rmt001
u_ws_pas4	iu_ws_pas4

s_error		istr_error_info

String		is_update_string, &
		is_update_string1

Long		il_rec_count

Window		iw_parentwindow
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_validate ()
public subroutine wf_delete ()
public subroutine wf_filenew ()
public function boolean wf_addrow ()
public function boolean wf_update_modify (long al_row, string as_buffer, boolean ab_value)
end prototypes

public function boolean wf_retrieve ();Long						ll_rec_count, &
							ll_row, &
							ll_message, &
							ll_count, &
							ll_column = 1,&
							ll_column1 = 2
							
String					ls_plant, &
							ls_column, & 
							ls_header, &
							ls_column_headings, &
							ls_detail, &
							ls_date, &
							ls_inquire_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_time, &
							ls_describe
							
u_string_functions 	lu_string	
Date						ld_date
DateTime 				ldt_datetime
	
If dw_plant.AcceptText() = -1 or &
	dw_shift.AcceptText() = -1 or &
	dw_effective_date.AcceptText() = -1 Then Return False
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait.. Inquiring Database")
This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr16mr_inq_car_wgt_estimate"

//Populate header-string
ls_plant	  = dw_plant.uf_get_plant_code()
ls_header  = ls_plant + '~t'
ls_header += String(dw_effective_date.uf_get_effective_date(),'YYYY-mm-DD') + '~t'
ls_header += dw_shift.uf_get_shift() + '~t'
//MessageBox("header",ls_header)

dw_satellite_cattle.reset()
/*If iu_rmt001.uf_rmtr16mr_inq_car_wgt_estimate(istr_error_info, & 
										ls_header, &
										ls_detail) = -1 Then 
										This.SetRedraw(True) 
										Return False
End If			
*/
//Changed old function by netwise convertion new function
If iu_ws_pas4.NF_RMTR16NR(istr_error_info, & 
										ls_header, &
										ls_detail) = -1 Then 
										This.SetRedraw(True) 
										Return False
End If


//ls_detail = &
//'525.0~t710.1~t81~r~n' + &
//'710.2~t735~t81~r~n' + &
//'735.1~t800.9~t720~r~n' + &
//'800.1~t950~t118~r~n' 
//MessageBox("ls_detail",ls_detail)

ll_rec_count = dw_satellite_cattle.ImportString(ls_detail)

If ll_rec_count > 0 Then
	//ls_header = '1999/09/09 12:48 AM IBDKDLD'
	//MessageBox("Return header",ls_header)
	dw_last_updated.uf_set_last_updated(ls_header)
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	dw_last_updated.uf_set_last_updated(' ')
	SetMicroHelp("0 Rows Retrieved")
End If

//messageBox('resup',dw_satellite_cattle.ResetUpdate())
dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.SetFocus()
This.SetRedraw(True) 
Return True

end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_moddate, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name,ls_column_text,ls_temp
dwItemStatus	ldwis_status			
u_string_functions u_string

IF dw_satellite_cattle.AcceptText() = -1 Then
	Return False
End If

If Not wf_validate() Then
	Return False
End If

// Loop through all rows checking if the actual data displayed has changed
For ll_count = 1 To dw_satellite_cattle.RowCount()
	// all data passed validate so turn the red colums off
	dw_satellite_cattle.SetItem(ll_count,'color_from','N')
	dw_satellite_cattle.SetItem(ll_count,'color_to','N')
	If dw_satellite_cattle.GetItemStatus (ll_count, 'from_wgt',Primary!) = DataModified! &
			or dw_satellite_cattle.GetItemStatus (ll_count, 'to_wgt',Primary!) = DataModified! &
			or dw_satellite_cattle.GetItemStatus (ll_count, 'head',Primary!) = DataModified! Then
		// No Not
	Else
// None of the values on the screen has changed so make the row NotModified 
		dw_satellite_cattle.uf_ChangeRowStatus(ll_count, NotModified!)
	End If	
Next


ll_modrows = dw_satellite_cattle.ModifiedCount()
ll_delrows = dw_satellite_cattle.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0 Then 
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

ls_header = ''
ls_header += dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_effective_date.uf_get_effective_date(),'yyyy-mm-dd') + '~t'
//ls_header += dw_shift.uf_get_shift() + '~t'
ls_temp = dw_shift.uf_get_shift()
ls_header += ls_temp + '~t'
If ls_temp = "A" Then
	If cbx_update_b.checked then
		ls_header += "C" + '~t'
	Else
		ls_header += " " + '~t'
	End If
Else
	ls_header += " " + '~t'
End If

SetMicroHelp("Wait, Updating ...")
SetPointer(HourGlass!)
This.SetRedraw(False)
is_update_string = ""

For ll_count = 1 To ll_delrows 
	lc_status_ind = 'D'
	If Not This.wf_update_modify(ll_count,lc_status_ind,False) Then 
		This.SetRedraw(True)
		Return False
	End if
Next

ll_row = 0
dw_satellite_cattle.SelectRow(0,False)

lc_status_ind = ' '
For ll_count = 1 To ll_modrows
	ll_Row = dw_satellite_cattle.GetNextModified(ll_Row, Primary!) 
	// If DataModified I need to delete the original row  	
	Choose Case dw_satellite_cattle.GetItemStatus(ll_row, 0, Primary!)
		CASE NewModified!
			If Not This.wf_update_modify(ll_row, lc_status_ind,False) Then 
				This.SetRedraw(True)
				Return False
			End If
		CASE DataModified!
			lc_status_ind = 'M'
			If Not This.wf_update_modify(ll_row, lc_status_ind,True) Then 
				This.SetRedraw(True)
				Return False
			End If
			lc_status_ind = ' '
			If Not This.wf_update_modify(ll_row, lc_status_ind,False) Then 
				This.SetRedraw(True)
				Return False
			End If
	END CHOOSE	
Next

//MessageBox('header',ls_header)
//MessageBox('Update String',is_update_string)

/*IF Not iu_rmt001.uf_rmtr17mr_upd_car_wgt_estimate(istr_error_info, &
								ls_header, is_update_string) THEN 
	This.SetRedraw(True)
	Return False
End if
*/
IF Not iu_ws_pas4.NF_RMTR17NR(istr_error_info, &
								ls_header, is_update_string) THEN 
	This.SetRedraw(True)
	Return False
End if



SetMicroHelp("Modification Successful")
dw_satellite_cattle.ResetUpdate()
This.SetRedraw(True)

Return( True )
end function

public function boolean wf_validate ();Long		ll_row,ll_row_count,ll_red
Decimal	ld_100_percent = 100.00,ld_temp,ld_temp1,ld_temp2
Boolean	lb_error


ll_row_count = dw_satellite_cattle.RowCount()
dw_satellite_cattle.SetRedraw(False)
FOR ll_row  = 1 TO ll_row_count
	ld_temp  = dw_satellite_cattle.GetItemDecimal(ll_row,'from_wgt')
	ld_temp1 = dw_satellite_cattle.GetItemDecimal(ll_row,'to_wgt')
	If ld_temp >= ld_temp1 Then
		iw_frame.SetMicroHelp("The From Weight of " + string(ld_temp) + &
		" can't be greater then or equal to the To Weight of " + String(ld_temp1))
		dw_satellite_cattle.SetItem(ll_row,'color_from','Y')
		lb_error = True
		Exit
	End If
	If Not ll_row = ll_row_count Then
		ld_temp2 = dw_satellite_cattle.GetItemDecimal(ll_row + 1,'from_wgt')
		If ld_temp1 >= ld_temp2 Then
			iw_frame.SetMicroHelp("The To Weight of " + string(ld_temp1) + &
				" can't be less then or equal to the From Weight of " + String(ld_temp2) + &
				" from the next row")
			dw_satellite_cattle.SetItem(ll_row,'color_to','Y')
			lb_error = True
			Exit
		End If
	End If
NEXT
dw_satellite_cattle.SetRedraw(True)

If lb_error Then
	Return False
Else
	Return True
End If
end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_addrow ();Super:: wf_addrow()

dw_satellite_cattle.SetColumn('from_wgt')
dw_satellite_cattle.SetFocus()


Return True
end function

public function boolean wf_update_modify (long al_row, string as_buffer, boolean ab_value);DWBuffer						ldwb_buffer

CHOOSE CASE as_buffer
	CASE 'D'
		ldwb_buffer = Delete!
	CASE ELSE
		ldwb_buffer = Primary!
END CHOOSE
//messagebox('mod',string(al_row) + ' ' + as_buffer + ' ' + String(ab_value))
is_update_string += &
	String(dw_satellite_cattle.GetItemDecimal(al_row,"from_wgt",ldwb_buffer, ab_value)) + "~t" 
is_update_string += &
	String(dw_satellite_cattle.GetItemDecimal(al_row,"to_wgt",ldwb_buffer, ab_value)) + "~t" 
is_update_string += &
	String(dw_satellite_cattle.GetItemNumber(al_row,"head",ldwb_buffer, ab_value)) + "~t"	 
// If = M Then I need to send this row down as a row to delete
If as_buffer = 'M' Then
	is_update_string += 'D' + "~r~n"	 
Else
	is_update_string += as_buffer + "~r~n"	 
End If
Return True
end function

on w_carcass_weight_estimate.create
int iCurrent
call super::create
this.dw_satellite_cattle=create dw_satellite_cattle
this.dw_plant=create dw_plant
this.dw_last_updated=create dw_last_updated
this.dw_effective_date=create dw_effective_date
this.dw_shift=create dw_shift
this.cbx_update_b=create cbx_update_b
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_satellite_cattle
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_last_updated
this.Control[iCurrent+4]=this.dw_effective_date
this.Control[iCurrent+5]=this.dw_shift
this.Control[iCurrent+6]=this.cbx_update_b
end on

on w_carcass_weight_estimate.destroy
call super::destroy
destroy(this.dw_satellite_cattle)
destroy(this.dw_plant)
destroy(this.dw_last_updated)
destroy(this.dw_effective_date)
destroy(this.dw_shift)
destroy(this.cbx_update_b)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

end event

event open;call super::open;This.Title = 'Carcass Weight Estimate'

dw_plant.disable()
dw_effective_date.uf_enable(false)
dw_effective_date.uf_set_text('Transfer Date:')
dw_shift.uf_enable(False)
dw_shift.uf_set_shift('A')

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'shift'
		message.StringParm = dw_shift.uf_get_shift()
	Case 'effective_date'
		message.StringParm = String(dw_effective_date.uf_get_effective_date())
	Case 'date_object_text'
		message.StringParm = 'Transfer Date:' + '~t' + 'PA' + '~t'
End choose

end event

event ue_postopen;call super::ue_postopen;//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "CarwgEst"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_carcass_estimated_inq'

iu_rmt001 = Create u_rmt001
iu_ws_pas4	=	Create	u_ws_pas4

wf_retrieve()


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'shift'
		dw_shift.uf_set_shift(as_value)
		Choose Case as_value
			Case 'A'
				cbx_update_b.Show()
			Case Else
				cbx_update_b.Hide()
		End Choose	
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date(Date(as_value))
End Choose


end event

type dw_satellite_cattle from u_base_dw_ext within w_carcass_weight_estimate
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
integer y = 362
integer width = 1243
integer height = 1075
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_carcass_weight_estimate"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;Long 		ll_row_count,ll_dump,ll_row
String	ls_column,ls_column_name,ls_temp
Decimal	ld_column_total,ld_100_percent= 100.00,ld_compute,ld_temp,ld_temp1,ld_temp2

This.SetRedraw(False) 
// Insure data is valid
If Not IsNumber(data) Then
	iw_frame.SetMicroHelp("All Columns must be numeric")
	This.SelectText(1, 100)
	This.SetRedraw(True) 
	Return 1
End If
IF Sign(Integer(data)) = -1 Then
	iw_frame.SetMicroHelp("The Number must be Positive")
	This.SelectText(1, 100)
	This.SetRedraw(True) 
	Return 1
End If

ll_row_count = This.RowCount()
//// Reset the values before proceeding to clean up previous colors
FOR ll_row = 1 TO ll_row_count
	This.SetItem(ll_row,'color_from','N')
	This.SetItem(ll_row,'color_to','N')
NEXT

CHOOSE CASE dwo.name
	CASE 'from_wgt'
		ld_temp  = Dec(data)
		If This.getItemStatus(row,'to_wgt',Primary!) = DataModified! Then
			ld_temp1 = This.GetItemDecimal(row,'to_wgt')
			If ld_temp >= ld_temp1 Then
				iw_frame.SetMicroHelp("The From Weight of " + string(ld_temp) + &
					" can't be greater than or equal to the To Weight of " + String(ld_temp1))
				This.SetItem(row,'color_from','Y')
				This.SetRedraw(True)
			//	messagebox('from wgt','1')
			   Return 0
			End If
		End If
		If Not row = 1 Then
			ld_temp2 = This.GetItemDecimal(row - 1,'to_wgt')
			If ld_temp < ld_temp2 Then
				iw_frame.SetMicroHelp("The To Weight of " + string(ld_temp2) + &
					" from the previous row can't be greater than or equal to the From Weight of " + String(ld_temp))// + &
				This.SetItem(row,'color_from','Y')
				This.SetRedraw(True) 
	//			messagebox('from wgt','3')
				Return 0
			End If
		End If
	CASE 'to_wgt'
		ld_temp  = Dec(data)
		If This.getItemStatus(row,'from_wgt',Primary!) = DataModified! Then
			ld_temp1 = This.GetItemDecimal(row,'from_wgt')
			If ld_temp <= ld_temp1 Then
				iw_frame.SetMicroHelp("The To Weight of " + string(ld_temp) + &
					" can't be less than or equal to the From Weight of " + String(ld_temp1))
				This.SetItem(row,'color_from','Y')
				This.SetRedraw(True) 
				//messagebox('to wgt','1')
				Return 0
			End If
		End If
		If Not row = ll_row_count Then
			ld_temp2 = This.GetItemDecimal(row + 1,'from_wgt')
			If ld_temp >= ld_temp2 Then
				iw_frame.SetMicroHelp("The To Weight of " + string(ld_temp) + &
					" can't be greater than or equal to the From Weight of " + String(ld_temp2) + &
					" from the next row")
				This.SetItem(row,'color_to','Y')
				This.SetRedraw(True) 
				//messagebox('to wgt','2')
				Return 0
			End If
		End If
END CHOOSE

This.SetRedraw(True) 
Return 0

end event

event constructor;call super::constructor; iw_parentwindow = Parent
 is_selection = '1'
end event

event itemerror;call super::itemerror;Return 2
end event

type dw_plant from u_plant within w_carcass_weight_estimate
integer width = 1448
integer height = 90
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()

end event

type dw_last_updated from u_last_updated_combined within w_carcass_weight_estimate
integer x = 80
integer y = 173
integer width = 1185
integer height = 90
integer taborder = 0
boolean bringtotop = true
end type

type dw_effective_date from u_effective_date within w_carcass_weight_estimate
integer x = 26
integer y = 83
integer width = 720
integer height = 93
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;uf_set_effective_date(Today())
uf_enable(False)
end event

type dw_shift from u_shift within w_carcass_weight_estimate
integer x = 764
integer y = 90
integer width = 333
integer height = 74
integer taborder = 0
boolean bringtotop = true
end type

type cbx_update_b from checkbox within w_carcass_weight_estimate
integer y = 272
integer width = 673
integer height = 77
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Update B Shift with A Shift:"
boolean checked = true
boolean lefttext = true
end type

