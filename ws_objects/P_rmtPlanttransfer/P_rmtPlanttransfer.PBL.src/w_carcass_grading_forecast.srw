﻿$PBExportHeader$w_carcass_grading_forecast.srw
forward
global type w_carcass_grading_forecast from w_base_sheet_ext
end type
type dw_begin_date from u_effective_date within w_carcass_grading_forecast
end type
type dw_plant from u_plant within w_carcass_grading_forecast
end type
type dw_satellite_cattle from u_base_dw_ext within w_carcass_grading_forecast
end type
end forward

global type w_carcass_grading_forecast from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 4041
integer height = 2253
long backcolor = 67108864
dw_begin_date dw_begin_date
dw_plant dw_plant
dw_satellite_cattle dw_satellite_cattle
end type
global w_carcass_grading_forecast w_carcass_grading_forecast

type variables
u_rmt001		iu_rmt001
u_ws_pas4	iu_ws_pas4

s_error		istr_error_info

Boolean		ib_successful_selection

String		is_update_string, &
		is_update_string1, &
		is_column_headings, &
		is_formula_string, &
		is_char_name
		
Long		il_rec_count, &
			il_row

Window		iw_parentwindow

DataStore	ids_grade_formula

				
end variables

forward prototypes
public function boolean wf_update_modify (long al_row)
public function boolean wf_update_formula (string as_column_name, string as_category, long al_row)
public function integer wf_recalc_with_def ()
public function boolean wf_update_modify_comp (long al_row)
public function boolean wf_retrieve ()
public function boolean wf_calc_pct (string as_column_num, string as_category, long al_row, boolean ab_update_all)
public function boolean wf_validate ()
public function integer wf_get_formula (string as_char, string as_category, string as_form_type, ref string as_form_char)
public function boolean wf_update ()
end prototypes

public function boolean wf_update_modify (long al_row);String						ls_describe, &
								ls_date, &
								ls_desc, &
								ls_category, &
								ls_form_type, &
								ls_def_pct, &
								ls_def_head, &
								ls_num, &
								ls_column_name, &
								ls_column_heading
								
long							ll_count


ls_date = String(dw_satellite_cattle.GetItemDate(al_row,"row_date"),'yyyy-mm-dd') 
ls_desc = dw_satellite_cattle.GetItemString(al_row, "text_2")
ls_category = dw_satellite_cattle.GetItemString(al_row, "category")
ls_def_pct = String(dw_satellite_cattle.GetItemDecimal(al_row, "def_pct"))
ls_def_head = String(dw_satellite_cattle.GetItemDecimal(al_row, "def_head"))

is_update_string += ls_date + "~t" + ls_desc + "~t" + ls_category + "~t" + ls_def_pct + "~t" +&
						 ls_def_head 
ll_count = 1	
do while ll_count < 100 
	
	ls_column_name = "col_" + String(ll_count) 
	ls_describe = "col_" + String(ll_count) + "_t.Visible"
	if dw_satellite_cattle.Describe(ls_describe) > '0' Then
		ls_describe = ls_column_name + "_t.text"
		ls_column_heading = dw_satellite_cattle.Describe(ls_describe)
		ls_form_type = dw_satellite_cattle.GetItemString(al_row, ls_column_name + "_ind")
		if Len(ls_form_type) <= 0 then
			ls_form_type = ' '
		End if
		ls_num = string(dw_satellite_cattle.GetItemDecimal(al_row, ls_column_name))
	Else
		ls_column_heading = ' '
		ls_form_type = ' '
		ls_num = '0'
	end If

	is_update_string += "~t" + ls_column_heading 
	is_update_string += "~t" + ls_form_type + "~t" + ls_num
	
	ll_count +=2
loop

is_update_string += "~r~n"
//is_update_string += &
//	String(dw_satellite_cattle.GetItemDecimal(al_row,as_column_name,ldwb_buffer, False)) + "~r~n"	 


Return True
end function

public function boolean wf_update_formula (string as_column_name, string as_category, long al_row);boolean						lb_found_row, &
								lb_continue_search, &
								lb_update_all

long							ll_row, &
								ll_return, &
								ll_rowcount, &
								ll_rowcounter, &
								ll_counter, &
								ll_find, &
								ll_column_num, &
								ll_formula_count

String						ls_formula_type, &
								ls_SearchString, &
								ls_form_char_name, &
								ls_describe, &
								ls_char, &
								ls_char_in, &
								ls_prev_char
								

/* this function will update the column if it has a formula.  If it does not have a formula
	it will find where that characteristic is used in other formulas and then update those */

dw_satellite_cattle.SetRedraw(False)
//initialize the ws_field for each daily row
ll_rowcount = dw_satellite_cattle.RowCount()
ll_column_num = long(RightTrim(mid(as_column_name,5,2)))
ls_formula_type = dw_satellite_cattle.GetItemString(dw_satellite_cattle.GetRow(), as_column_name +'_ind') 

//if the field that was changed has a formula update just that column for every daily row
if RightTrim(ls_formula_type) = 'BASE' or RightTrim(ls_formula_type) = 'REM' Then
	lb_update_all = True
	for ll_rowcounter = 1 to ll_rowcount
		if dw_satellite_cattle.GetItemString(ll_rowcounter, "date_type_ind") = 'D' then
			wf_calc_pct(string(ll_column_num), as_category, ll_rowcounter, lb_update_all)
			dw_satellite_cattle.SetItem(ll_rowcounter, 'update_flag', 'U')
		end if
	next
	lb_continue_search = False
else
//if the field that was changed does not have a formula check to see that it is part of a formula
	ls_describe = as_column_name + "_t.text"	
	ls_form_char_name = dw_satellite_cattle.Describe(ls_describe)
	
	ls_SearchString = 	"category = '"+ as_category +&
								"' and formula_char_name = '" + ls_form_char_name + "'" 
	lb_continue_search = True
	
end if

//search for the rows that use the field with no formula, if the update flag is (D)elete don't use it
lb_update_all = False
ll_formula_count = ids_grade_formula.RowCount()
ll_row = 1
Do While lb_continue_search
	ll_find = ids_grade_formula.Find(ls_SearchString, ll_row, ll_formula_count)
	if ll_find > 0 Then /*1*/
		if	ids_grade_formula.GetItemString(ll_find, 'update_flag') = 'D' Then
			//do nothing
		else
			ls_char = ids_grade_formula.GetItemString(ll_find, 'char_name')
			//find the char name in the headings
			ll_counter = 1
			lb_found_row = False
			Do While lb_found_row = False
			//if the column counter = passed column bypass it
				if ll_counter = ll_column_num then /*3*/
					//do nothing
				else
			//if the char from the datastore matches the heading call the function to recalc the pct
					ls_describe	= "col_"+ string(ll_counter) + "_t.text"
					ls_char_in = dw_satellite_cattle.Describe(ls_describe)
					if RightTrim(ls_char_in) = RightTrim(ls_char) Then /*4*/
						lb_found_row = True
						wf_calc_pct(string(ll_counter), as_category, al_row, lb_update_all)
					end if /*4*/
				end if /*3*/
				ll_counter += 2
			Loop
		
		end if
	else
		lb_continue_search = False
	end if /*1*/
	ll_row = ll_find + 1
	if ll_row > ll_formula_count Then lb_continue_search = False
Loop

//dw_satellite_cattle.AcceptText()
//dw_satellite_cattle.GroupCalc()
dw_satellite_cattle.SetItem(al_row, 'update_flag', 'U')
dw_satellite_cattle.SetRedraw(True)

Return True
end function

public function integer wf_recalc_with_def ();boolean						lb_update_all
long							ll_column, &
								ll_count, &
								ll_find, &
								ll_row_count
																
String						ls_formula_type, &
								ls_column_name, &
								ls_category, &
								ls_describe


dw_satellite_cattle.SetRedraw(false)

ll_row_count = dw_satellite_cattle.RowCount()
lb_update_all = True
//call the function to calculate the percents where the folmula type is BASE or REM - daily rows only.
if ll_row_count > 0 Then
	For ll_count = 1 to ll_row_count
		ll_column = 1
	if dw_satellite_cattle.GetItemString(ll_count, "date_type_ind") = 'D' then
		ls_category = dw_satellite_cattle.GetItemString(ll_count, 'category')
		ls_describe = "col_" + String(ll_column) + ".Visible"
		Do while dw_satellite_cattle.Describe(ls_describe) > '0' 
			ls_column_name = "col_" + string(ll_column) + '_ind'
			ls_formula_type = dw_satellite_cattle.GetItemString(ll_count, ls_column_name)
			if RightTrim(ls_formula_type) = 'BASE' or RightTrim(ls_formula_type) = 'REM' Then 
				dw_satellite_cattle.SetItem(ll_count, 'update_flag', 'U')
				wf_calc_pct(string(ll_column), ls_category, ll_count, lb_update_all)
			end if
			ll_column += 2
			ls_describe = "col_" + String(ll_column) + "_t.Visible"
		loop
	end if
	Next
end if
//dw_satellite_cattle.AcceptText()
//dw_satellite_cattle.GroupCalc()

dw_satellite_cattle.SetRedraw(True)
Return 1
end function

public function boolean wf_update_modify_comp (long al_row);String						ls_describe, &
								ls_date, &
								ls_desc, &
								ls_category, &
								ls_form_type, &
								ls_def_pct, &
								ls_def_head, &
								ls_temp1, &
								ls_num, &
								ls_column_heading, &
								ls_column_name
							
long							ll_count


//this function will populate the input string for the RPC with the computed fields.

ls_date = String(dw_satellite_cattle.GetItemDate(al_row,"row_date"),'yyyy-mm-dd') 
ls_desc = 'Plant Est.'
ls_category = ' '
ls_def_pct = ''
ls_def_head = string(dw_satellite_cattle.Object.sum_def_head[al_row])

is_update_string += ls_date + "~t" + ls_desc + "~t" + ls_category + "~t" + ls_def_pct + "~t" +&
						 ls_def_head 
	

ll_count = 1
do while ll_count < 100 
	ls_column_name = "col_" + string(ll_count)
	ls_describe = "col_" + String(ll_count) + "_t.Visible"
	if dw_satellite_cattle.Describe(ls_describe) > '0' Then
		ls_column_heading = dw_satellite_cattle.Describe(ls_column_name + "_t.text")
		ls_temp1 = "sum_col_" + String(ll_count) + "_compute"
		ls_num = String(dw_satellite_cattle.GetItemNumber(al_row, ls_temp1)) 
	Else
		ls_column_heading = ' '
		ls_form_type = ' '
		ls_num = '0'
	end If
	is_update_string +=  "~t" + ls_column_heading
	is_update_string += "~t" + "~t" + ls_num

	
	
	
	ll_count += 2
Loop

is_update_string += "~r~n"

Return True
end function

public function boolean wf_retrieve ();Boolean					lb_update_all
Long						ll_rec_count, &
							ll_row, &
							ll_message, &
							ll_count, &
							ll_column = 1
							
String					ls_plant, &
							ls_column, & 
							ls_header, &
							ls_column_headings, &
							ls_detail, &
							ls_date, &
							ls_inquire_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_time, &
							ls_describe, &
							ls_formula, &
							ls_formula_type, & 
							ls_column_name, &
							ls_category
							
u_string_functions 	lu_string	
Date						ld_date
DateTime 				ldt_datetime

	
ids_grade_formula = Create DataStore
ids_grade_formula.DataObject = "d_grade_formula"

If dw_plant.AcceptText() = -1 Then Return False
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait.. Inquiring Database")
This.SetRedraw(False) 

// clear out prev values reset the datawindow
ls_describe = "col_" + String(ll_column) + ".Visible"
Do While dw_satellite_cattle.Describe(ls_describe) > '0'
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 0 " + &
		" col_" + String(ll_column) + ".Visible = 0 " 
//dmk	" col_" + String(ll_column + 1) + ".Visible = 0 " 
//	If ll_column = 1 Then
//		ls_TitleModify += " co_col_" + String(ll_column) + ".Visible = 0 "
//		ls_TitleModify += " total_pct" + String(ll_column) + ".Visible = 0 "
//	End If	
	ll_column += 2
	ls_describe = "col_" + String(ll_column) + ".Visible"
Loop

dw_satellite_cattle.Modify (ls_TitleModify)
ll_column = 1	
ls_TitleModify = ''

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr14mr_inq_car_grade_forecast"
//Populate header-string
ls_plant	  = dw_plant.uf_get_plant_code()
ls_header  = ls_plant + '~t'
ls_header += String(dw_begin_date.uf_get_effective_date(),'YYYY-mm-DD') + '~t'
//MessageBox("header",ls_header)

dw_satellite_cattle.reset()

If Not iu_ws_pas4.nf_rmtr14nr(istr_error_info, & 
										ls_header, &
										ls_column_headings, &
										ls_detail, &
										ls_formula) Then 
										This.SetRedraw(True) 
										Return False
End If	
	

//MessageBox("Return header",ls_header)
//MessageBox("Column headings",ls_column_headings)
//MessageBox("Detail",ls_detail)

//ls_header = '1999/09/09 12:48 AM IBDKDLD'
//dw_last_updated.uf_set_last_updated(ls_header)

// If nothing sent down they need to set up plant characteristics
If lu_string.nf_amiempty(ls_column_headings) Then
	ll_message = 1
End If

//dmk comment out - changed to loop below this one.
// Build a modify string based on the column_headeings returned
//DO WHILE Not lu_string.nf_amiempty(ls_column_headings) or ll_column > 100
//	ls_temp = lu_string.nf_gettoken(ls_column_headings,"~t")
//	If ll_column =  1 Then
//		ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + &
//		/* right trim to remove spaces*/RightTrim(ls_temp) + &
//		"' col_" + String(ll_column) + "_t.Visible = 1 " + &
//		" co_col_" + String(ll_column) + ".Visible = 1 "
//	Else
//		ls_TitleModify += 'col_' + String(Round(ll_column/2,0)) + "_t.text = '" +&
//		/* right trim to remove spaces*/RightTrim(ls_temp) + &
//		"' col_" + String(Round(ll_column/2,0)) + "_t.Visible = 1 " 
//	End If
//	ls_TitleModify += " col_" + String(ll_column) + ".Visible = 1 " + &
//		" col_" + String(ll_column1) + ".Visible = 1 " 
//	ll_column 	+= 2
//	ll_column1 	+= 2
//LOOP

// Build a modify string based on the column_headings returned
DO WHILE Not lu_string.nf_amiempty(ls_column_headings) or ll_column > 100
	ls_temp = lu_string.nf_gettoken(ls_column_headings,"~t")
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + &
	/* right trim to remove spaces*/RightTrim(ls_temp) + &
	"' col_" + String(ll_column) + "_t.Visible = 1 " 
	ls_TitleModify += " col_" + String(ll_column) + ".Visible = 1 " 
		
	ll_column 	+= 2

LOOP


is_column_headings = ls_column_headings
//MessageBox('Modify',ls_titlemodify)
dw_satellite_cattle.Modify ( ls_TitleModify ) 

ll_rec_count = ids_grade_formula.ImportString(ls_formula)
is_formula_string = ls_formula

ll_rec_count = dw_satellite_cattle.ImportString(ls_detail)

If ll_rec_count > 0 Then

	IF ll_message = 0 Then
		ll_message = 4
	Else
		ll_message = 1
	End If
Else
	IF ll_message = 0 Then
		ll_message = 3
	Else		
		ll_message = 2 
	End If
End if


lb_update_all = True
//call the function to calculate the percents where the folmula type is BASE or REM - daily rows only.
if ll_rec_count > 0 Then
	For ll_count = 1 to ll_rec_count
	dw_satellite_cattle.SetItem(ll_count,"update_flag", ' ')
	ll_column = 1
	if dw_satellite_cattle.GetItemString(ll_count, "date_type_ind") = 'D' then
		ls_category = dw_satellite_cattle.GetItemString(ll_count, 'category')
		ls_describe = "col_" + String(ll_column) + ".Visible"
		Do while dw_satellite_cattle.Describe(ls_describe) > '0' 
			ls_column_name = "col_" + string(ll_column) + '_ind'
			ls_formula_type = dw_satellite_cattle.GetItemString(ll_count, ls_column_name)
			if RightTrim(ls_formula_type) = 'BASE' or RightTrim(ls_formula_type) = 'REM' Then 
				wf_calc_pct(string(ll_column), ls_category, ll_count, lb_update_all)
			end if
			ll_column += 2
			ls_describe = "col_" + String(ll_column) + "_t.Visible"
		loop
	end if
Next
end if
// Must do because of the reinquire 
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '1400'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '1400'

CHOOSE CASE ll_message
	CASE 1
		Beep(1)
		SetMicroHelp('There are no Characteristics '+ &
		'associated with Ship Plant ' + ls_plant)
	CASE 2
		SetMicroHelp("0 Rows Retrieved")
	CASE 3
		Beep(1)
		SetMicroHelp('There are no Destination Plants ' + &
		'associated with Ship Plant ' + ls_plant)
	CASE 4
		SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
END CHOOSE


If ll_rec_count > 0 Then
	//	continue
Else
	IF ll_message = 0 Then
		Beep(1)
		SetMicroHelp('There are no Grade Formulas '+ &
		'associated with Ship Plant ' + ls_plant)
	End If
End if

dw_satellite_cattle.AcceptText()
dw_satellite_cattle.GroupCalc()
dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.SetFocus()
This.SetRedraw(True) 
Return True

end function

public function boolean wf_calc_pct (string as_column_num, string as_category, long al_row, boolean ab_update_all);boolean						lb_found_row, &
								lb_negative

decimal						ld_pct, &
								ld_ws_field, &
								ld_def_pct
								
long							ll_row, &
								ll_return, &
								ll_counter, &
								ll_rowcount, &
								ll_rowcounter
								

String						ls_describe, &
								ls_form_describe, &
								ls_form_char_in, &
								ls_form_char_out, &
								ls_column_name, &
								ls_formula_type, &
								ls_char



//initialize the ws_field for each daily row
ll_rowcount = dw_satellite_cattle.RowCount()

for ll_rowcounter = 1 to ll_rowcount
	if dw_satellite_cattle.GetItemString(ll_rowcounter, "date_type_ind") = 'D' then
		dw_satellite_cattle.SetItem(ll_rowcounter, 'ws_field', 0)
	end if
next 

//get the formula type first, this exists in the ind column
ls_column_name = "col_" + as_column_num + '_ind'
ls_formula_type = dw_satellite_cattle.GetItemString(al_row, ls_column_name)
il_row = 1

//get the value in the heading field, this is the characteristic
ls_describe = "col_"+ as_column_num + "_t.text"	
ls_char = dw_satellite_cattle.Describe(ls_describe)
//call the function for the first characteristic in the formula.
ll_return = wf_get_formula(ls_char, as_category, ls_formula_type, ls_form_char_out)

Do While ll_return = 0
	//find the column number that matches the found formula characteristic from the function call
	// to get the pct value
	
	ll_counter = 1
	lb_found_row = False
	
	Do While lb_found_row = False and ll_counter < 100
	//check to see if the heading is visible, if it isn't stop looking but setting the counter to 100
		ls_describe = "col_" + String(ll_counter) + "_t.Visible"
		if dw_satellite_cattle.Describe(ls_describe) > '0' Then
	//if the column counter = passed column bypass it
			if	string(ll_counter) = as_column_num then
				//do nothing
			else
				ls_form_describe	= "col_"+ string(ll_counter) + "_t.text"
				ls_form_char_in = dw_satellite_cattle.Describe(ls_form_describe)
				if RightTrim(ls_form_char_in) = RightTrim(ls_form_char_out) Then
					lb_found_row = True
					ls_column_name = "col_" + string(ll_counter)
	//retrieve the value in the corresponding column for each daily row and add it to the sum (ws_field)
					if ab_update_all Then
						for ll_rowcounter = 1 to ll_rowcount
							if dw_satellite_cattle.GetItemString(ll_rowcounter, "date_type_ind") = 'D' then
								ld_pct = dw_satellite_cattle.GetItemDecimal(ll_rowcounter, ls_column_name)
								ld_ws_field = dw_satellite_cattle.GetItemDecimal(ll_rowcounter, 'ws_field')
								if ld_ws_field < 1 Then	lb_negative = True
								dw_satellite_cattle.SetItem(ll_rowcounter, 'ws_field', ld_pct + ld_ws_field)
							end if
						next 
					else
						ld_pct = dw_satellite_cattle.GetItemDecimal(al_row, ls_column_name)
						ld_ws_field = dw_satellite_cattle.GetItemDecimal(al_row, 'ws_field')
						if ld_ws_field < 1 Then	lb_negative = True
						dw_satellite_cattle.SetItem(al_row, 'ws_field', ld_pct + ld_ws_field)
					end if
				end if
			end if
			ll_counter += 2
		else
			ll_counter = 100
		end if
	Loop
	//call the function again for the next formula characteristic		
	ll_return = wf_get_formula(ls_char, as_category, ls_formula_type, ls_form_char_out)	
Loop

if Not lb_found_row then return true
//calculate the new pct for the column passed

ls_column_name = "col_" + as_column_num
if ab_update_all Then
	for ll_rowcounter = 1 to ll_rowcount
		
		if dw_satellite_cattle.GetItemString(ll_rowcounter, "date_type_ind") = 'D' and &
		   dw_satellite_cattle.GetItemString(ll_rowcounter, "category") = as_category then
			ld_ws_field = dw_satellite_cattle.GetItemDecimal(ll_rowcounter, 'ws_field')
			ld_def_pct = dw_satellite_cattle.GetItemDecimal(ll_rowcounter, 'def_pct')
			
			if RightTrim(ls_formula_type) = 'REM' then
				ld_ws_field = 100 - ld_def_pct - ld_ws_field
			else 
				ld_ws_field = ld_def_pct - ld_ws_field
			end if
			if ld_ws_field < 1 Then	lb_negative = True
			dw_satellite_cattle.SetItem(ll_rowcounter, ls_column_name, ld_ws_field)
		end if
	next 
else
//set the column name back to the one passed so that it can be udpated with the new pct
	ld_ws_field = dw_satellite_cattle.GetItemDecimal(al_row, 'ws_field')
	ld_def_pct = dw_satellite_cattle.GetItemDecimal(al_row, 'def_pct')
		
	if RightTrim(ls_formula_type) = 'REM' then
		ld_ws_field = 100 - ld_def_pct - ld_ws_field
	else 
		ld_ws_field = ld_def_pct - ld_ws_field
	end if
	if ld_ws_field < 1 Then	lb_negative = True
	dw_satellite_cattle.SetItem(al_row, ls_column_name, ld_ws_field)
end if

//dw_satellite_cattle.AcceptText()
//dw_satellite_cattle.GroupCalc()

dw_satellite_cattle.SetRedraw(true)


Return True
end function

public function boolean wf_validate ();Long		ll_row,ll_row_count, ll_col_count
Decimal	ld_100_percent = 100.00,ld_temp
Boolean	lb_error
String	ls_col_name


ll_row_count = dw_satellite_cattle.RowCount()
// check only daily rows - dmk
FOR ll_row = 1 TO ll_row_count
	If dw_satellite_cattle.GetItemString(ll_row, 'date_type_ind') = 'D' Then
		ld_temp = Round(dw_satellite_cattle.GetItemDecimal(ll_row,'total_pct'),2)
		If Not ld_temp = ld_100_percent Then
			If Not ld_temp = 0 Then
				iw_frame.SetMicroHelp('The total for Transfer Est. must equal 100 percent') 
				dw_satellite_cattle.ScrollToRow(ll_row)
				dw_satellite_cattle.SetColumn('col_1')
				Beep(1)
				lb_error = True
				Exit
			End IF
		End If
		ll_col_count = 1
		Do While ll_col_count < 100
			ls_col_name = 'col_' + string(ll_col_count)
			ld_temp = dw_satellite_cattle.GetItemDecimal(ll_row, ls_col_name)
			if ld_temp < 0 Then
				iw_frame.SetMicroHelp('Percent must be greater than 0') 
				dw_satellite_cattle.ScrollToRow(ll_row)
				dw_satellite_cattle.SetColumn(ls_col_name)
				Beep(1)
				lb_error = True
				Exit
			end if
			ll_col_count +=2
		Loop
	End if
NEXT

If lb_error Then
	Return False
Else
	Return True
End If
end function

public function integer wf_get_formula (string as_char, string as_category, string as_form_type, ref string as_form_char);boolean						lb_continue_search
long							ll_row, &
								ll_return, &
								ll_find, &
								ll_rowcount
																
String						ls_SearchString




//initialize the ws_field for each daily row
ll_rowcount = ids_grade_formula.RowCount()

lb_continue_search = True

ls_SearchString = 	"formula_type = '"+ as_form_type +&
							"' and char_name = '" + as_char + "'" +&
							" and category = '" + as_category + "'" 

//search for the row that matches, if the update flag is (D)elete don't use it
Do While lb_continue_search
	IF il_row > ll_rowcount THEN 
		return 1
	ELSE	
		ll_find = ids_grade_formula.Find(ls_SearchString, il_row, ll_rowcount)
		if ll_find > 0 Then
			if	ids_grade_formula.GetItemString(ll_find, 'update_flag') = 'D' Then
				//do nothing
			else
				as_form_char = ids_grade_formula.GetItemString(ll_find, 'formula_char_name')	
				lb_continue_search = False
			end if
		
			il_row = ll_find + 1
		else
			lb_continue_search = False
			return 1	
		end if
	END IF
Loop
Return 0
end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_moddate, &
					ll_count,ll_count1,ll_count2,ll_column_num, &
					ll_rowcount, &
					ll_modformula, &
					ll_temp, &
					ll_16th_row

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name,ls_column_text, &
					ls_update_string, &
					ls_date, ls_prev_date, &
					ls_RPC_detail, ls_temp
					
dwItemStatus	ldwis_status			

u_string_functions u_string
datawindow		ldw_temp


IF dw_satellite_cattle.AcceptText() = -1 and ids_grade_formula.AcceptText() = -1 Then
	Return False
End If

ll_rowcount = dw_satellite_cattle.RowCount()

//check to see if even one row has been updated before continuing.  Use the update flag instead of status
ll_modrows = 0
ll_count = 1

do 
	if dw_satellite_cattle.GetItemString(ll_count, "update_flag") > ' ' Then
		ll_modrows =1 
	end if
	ll_count ++
LOOP WHILE ll_modrows <= 0 and ll_count < ll_rowcount
//ll_modrows = dw_satellite_cattle.ModifiedCount()

//check to see if any formulas need to be updated first
ll_rowcount = ids_grade_formula.RowCount()
for ll_count = 1 to ll_rowcount
	ls_temp = ids_grade_formula.GetItemString(ll_count, "update_flag")
	if ls_temp > ' ' Then
		ll_modformula =1 
		ll_count = ll_rowcount + 1
		ll_modrows =1 
	end if
Next
IF ll_modrows <= 0 Then 
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

If Not wf_validate() Then
	Return False
End If


ls_header = ''
ls_header += dw_plant.uf_get_plant_code() + '~t'

SetMicroHelp("Wait, Updating ...")
SetPointer(HourGlass!)
This.SetRedraw(False)

if ll_modformula > 0 Then
	ids_grade_formula.SetSort('update_flag A')
	ids_grade_formula.Sort()
	ls_update_string = ids_grade_formula.object.datawindow.data
	//ldw_temp.ImportString(ls_update_string)
//	ldw_temp.AcceptText() 
//	ls_update_string = iw_frame.iu_string.nf_BuildUpdateString(ldw_temp)
/*	IF Not iu_rmt001.uf_rmtr31mr_grade_formula_upd(istr_error_info, &
							ls_header, ls_update_string) THEN 
		This.SetRedraw(True)
		Return False
	End if
*/
IF Not iu_ws_pas4.NF_RMTR31NR(istr_error_info, &
							ls_header, ls_update_string) THEN 
		This.SetRedraw(True)
		Return False
	End if
	for ll_count = 1 to ids_grade_formula.RowCount()
		ids_grade_formula.SetItem(ll_count, "update_flag", ' ')
	Next
	ids_grade_formula.ResetUpdate()
End if

is_update_string = ""
ll_row = 0
dw_satellite_cattle.SelectRow(0,False)
lc_status_ind = ' '

//remove this logic - check update flag on row instead
//For ll_count = 1 To ll_modrows
//	ll_Row = dw_satellite_cattle.GetNextModified(ll_Row, Primary!) 
//	ll_column_num = 1
//	ls_describe = "col_" + String(ll_column_num) + ".Visible"
//	/* loop through all visable columns that are unprotected(odd columns) 
//	checking status*/ 
//	Do While dw_satellite_cattle.Describe(ls_describe) > '0'
//		ls_column_name = "col_" + string(ll_column_num)
//		ls_column_text = "col_" + String(Round(ll_column_num / 2,0))
//		If dw_satellite_cattle.GetItemStatus(ll_row,ls_column_name, Primary!) = DataModified! Then
//			wf_update_modify(ll_row,ls_column_name,ls_column_name,lc_status_ind)  
//		End If
//		ll_column_num += 2
//		ls_describe = "col_" + String(ll_column_num) + ".Visible"
//	Loop
//Next
//
//MessageBox('header',ls_header)
//MessageBox('Update String',is_update_string)

ll_rowcount = dw_satellite_cattle.RowCount()

//build the string of updated rows
for ll_count = 1 to ll_rowcount
	if dw_satellite_cattle.GetItemString(ll_count, "update_flag") > ' ' Then
		if dw_satellite_cattle.GetItemString(ll_count, "date_type_ind") = 'D' Then
			ls_date = String(dw_satellite_cattle.GetItemDate(ll_count,"row_date"),'yyyy-mm-dd') 
//add the plant est only once for the date - computed fields
			if ls_date <> ls_prev_date Then
				wf_update_modify_comp(ll_count)	
				ls_prev_date = ls_date
			end if
			wf_update_modify(ll_count)
		end if
	end if
Next


ls_update_string = is_update_string
DO 
	ll_16th_Row = u_string.nf_nPos( ls_update_string, "~r~n",1,16)
	IF ll_16th_Row > 0 Then
		ls_RPC_Detail = Left( ls_Update_string, ll_16th_Row + 1)
		ls_Update_string = Mid( is_Update_string, ll_16th_Row + 2)
	ELSE
		ls_Rpc_detail = ls_Update_string
		ls_Update_string = ''
	END IF

	/*if Not iu_rmt001.uf_rmtr15mr_upd_car_grade_forecast(istr_error_info, &
											ls_header, &
											ls_RPC_detail ) Then 
		This.SetRedraw(True)
		Return False 
	End if
	*/
	if Not iu_ws_pas4.NF_RMTR15NR(istr_error_info, &
											ls_header, &
											ls_RPC_detail ) Then 
		This.SetRedraw(True)
		Return False 
	End if
Loop While Not u_string.nf_IsEmpty( ls_Update_string)


SetMicroHelp("Modification Successful")
dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.AcceptText()
This.SetRedraw(True)

Return( True )
end function

on w_carcass_grading_forecast.create
int iCurrent
call super::create
this.dw_begin_date=create dw_begin_date
this.dw_plant=create dw_plant
this.dw_satellite_cattle=create dw_satellite_cattle
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_begin_date
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_satellite_cattle
end on

on w_carcass_grading_forecast.destroy
call super::destroy
destroy(this.dw_begin_date)
destroy(this.dw_plant)
destroy(this.dw_satellite_cattle)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;String 					ls_temp
u_string_functions 	lu_string


ls_temp = Message.StringParm	

This.Title = 'Carcass Grading Forecast'
dw_begin_date.uf_set_text('Begin Date')

dw_plant.disable()


end event

event ue_get_data;call super::ue_get_data;string	ls_column_name, &
			ls_describe, &
			ls_formula_type, &
			ls_object

Long		ll_row


Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'begin_date'
		message.StringParm = String(dw_begin_date.uf_get_effective_date(), 'yyyy-mm-dd')
	Case 'GRD'
		message.StringParm = ids_grade_formula.object.datawindow.data
	Case 'Characteristic'
		message.StringParm = is_char_name
	Case	'category'
		ls_object = dw_satellite_cattle.is_ObjectAtPointer
		ls_column_name = iw_frame.iu_string.nf_gettoken(ls_object, '~t')
		ll_row = Long(ls_object)
		message.StringParm = dw_satellite_cattle.GetItemString(ll_row, 'category')
	Case	'formula_type'
		ls_object = dw_satellite_cattle.is_ObjectAtPointer
		ls_column_name = iw_frame.iu_string.nf_gettoken(ls_object, '~t')
		ll_row = Long(ls_object)
		ls_formula_type = dw_satellite_cattle.GetItemString(ll_row, ls_column_name + "_ind")
		if ls_formula_type = 'UPDT' Then
			ls_formula_type = '    '
		end if
		message.StringParm = ls_formula_type
	Case 'column_headings'
		message.StringParm = is_column_headings
End choose

end event

event ue_postopen;call super::ue_postopen;This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "CarGrFor"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_grading_forecast_inq'

iu_rmt001 = Create u_rmt001
iu_ws_pas4 = Create u_ws_pas4

wf_retrieve()


end event

event ue_set_data;call super::ue_set_data;String		ls_column_name, &
				ls_formula_char, &
				ls_object
	
Long			ll_row

Boolean		lb_successful


Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'begin_date'
		dw_begin_date.uf_set_effective_date( Date(as_value ))
	Case 'GRD'
		ids_grade_formula.Reset()
		ids_grade_formula.ImportString(as_value)
	Case 'formula_type'
		ls_formula_char = as_value
		if ls_formula_char = '    ' Then
			ls_formula_char = 'UPDT'
		end if
		ls_object = dw_satellite_cattle.is_ObjectAtPointer
		ls_column_name = iw_frame.iu_string.nf_gettoken(ls_object, '~t')
		ll_row = Long(ls_object)
		dw_satellite_cattle.SetItem(ll_row, ls_column_name + "_ind", ls_formula_char)
	Case 'selection'
		if as_value = 'true' then
			ib_successful_selection = true
//			wf_calc_pct()
		Else
			ib_successful_selection = false
		End if
End Choose


end event

event resize;call super::resize;//constant integer li_x		= 0 
//constant integer li_y		= 96
//
integer li_x		= 30 
integer li_y		= 126


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

  
//dw_satellite_cattle.width	= newwidth - (30 + li_x)
//dw_satellite_cattle.height	= newheight - (30 + li_y)

dw_satellite_cattle.width	= newwidth - (li_x)
dw_satellite_cattle.height	= newheight - (li_y)


// Must do because of the split horizonal bar 
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '1400'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '1400'


end event

type dw_begin_date from u_effective_date within w_carcass_grading_forecast
integer x = 1448
integer width = 881
integer height = 93
integer taborder = 10
end type

type dw_plant from u_plant within w_carcass_grading_forecast
integer x = 4
integer width = 1448
integer height = 90
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()

end event

type dw_satellite_cattle from u_base_dw_ext within w_carcass_grading_forecast
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
integer y = 96
integer width = 3785
integer height = 2000
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_carcass_grading_forecast"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event ue_post_itemchanged(long al_row, dwobject adwo_dwo);String 			ls_column_name, &
					ls_category, &
					ls_column
					

ls_column_name = adwo_dwo.name
ls_column = left(adwo_dwo.name,4)

if ls_column = 'col_' Then
	ls_category = this.GetItemString(al_row, "category")
	
	wf_update_formula(ls_column_name, ls_category, al_row)
else
	wf_recalc_with_def()
end if




end event

event itemchanged;call super::itemchanged;boolean	lb_update_all
Long 		ll_row_count,ll_dump,ll_row, ll_count, ll_column, ll_column_num
String	ls_column,ls_column_name,ls_temp, ls_category, ls_formula_type, ls_describe
Decimal	ld_column_total,ld_100_percent= 100.00,ld_compute,ld_temp


// I did this to make the code more generic
ls_column_name = dwo.name
ls_column = left(dwo.name,4)

This.SetRedraw(false)
CHOOSE CASE ls_column
	CASE 'col_'
		// Insure data is valid
		If (data) = ' ' Then
			This.SetItem(row, ls_column_name, 0)
		elseif Not IsNumber(data) or isnull(data) Then
			iw_frame.SetMicroHelp("All Columns must be numeric")
			This.SelectText(1, 100)
			This.SetRedraw(True)
			Return 1
		Elseif Sign(Integer(data)) = -1 Then
			iw_frame.SetMicroHelp("The Number must be Positive")
			This.SelectText(1, 100)
			This.SetRedraw(True)
			Return 1
		Elseif Long(data) > 100 Then
			iw_frame.SetMicroHelp("The Number must be less than 100")
			This.SelectText(1, 100)
			This.SetRedraw(True)
			Return 1
		End If
		This.event Post ue_post_itemchanged(row,dwo)
	
END CHOOSE

CHOOSE CASE ls_column_name
	CASE 'def_pct'
		If Not IsNumber(data) or isnull(data) Then
			iw_frame.SetMicroHelp("All Columns must be numeric")
			This.SelectText(1, 100)
			This.SetRedraw(True)
			Return 1
		End If
		IF Sign(Integer(data)) = -1 Then
			iw_frame.SetMicroHelp("The Number must be Positive")
			This.SelectText(1, 100)
			This.SetRedraw(True)
			Return 1
		End If
		
		This.event Post ue_post_itemchanged(row,dwo)		

END CHOOSE

This.SetItem(row, 'update_flag', 'U')
This.SetRedraw(True)
Return 0

end event

event constructor;call super::constructor; iw_parentwindow = Parent
end event

event itemerror;call super::itemerror;Return 2
end event

event doubleclicked;/* I had to override this it was causing a datawindow error when
you double clicked on the check box any ideas let me know. ibdkdld */ 
end event

event rbuttondown;call super::rbuttondown;Boolean	lb_good_column, &
			lb_update_all 
Long		ll_rowcount, &
			ll_find, &
			ll_row
String	ls_SearchString, &
			ls_char_name, & 
			ls_column_num, &
			ls_category, &
			ls_formula_type_ind, &
			ls_column_name, &
			ls_formula_type
			
Window	lw_temp


If Right(dwo.Name, 2) = '_t' Then return

If Left(dwo.name, 4) = 'col_' Then
	//continue
else
	Return
end if


if This.GetItemString(row, "date_type_ind") <> 'D' Then
	SetMicroHelp("Must right click on a cell on the Transfer Est. row.")
	Return 1
end if 

lb_good_column = False
If Left(dwo.name, 4) = 'col_' Then 
	if This.GetItemString(row, "date_type_ind") <> 'D' Then
		SetMicroHelp("Must right click on a cell on the Transfer Est. row.")
		Return 1
	end if 
	ll_rowcount = ids_grade_formula.RowCount()
	is_char_name = dw_satellite_cattle.describe(dwo.name + '_t.text')
	ls_SearchString = 	"formula_char_name = '"+ is_char_name
	ll_find = ids_grade_formula.Find(ls_SearchString, ll_row, ll_rowcount)
	
	if ll_find = 0 Then
		SetMicroHelp("Characteristic is already used in a formula.")
		Return 1
	else
		lb_good_column = True
	End if
end if

If Not lb_good_column Then return


OpenWithParm(w_carcass_grading_forecast_selection, iw_parent)

if ib_successful_selection Then
	ls_column_name = dwo.name
	ls_formula_type = dw_satellite_cattle.GetItemString(row, ls_column_name + "_ind")
	ls_category = This.GetItemString(row, 'category') 
	ll_rowcount = this.RowCount()
	for ll_row = 1 to ll_rowcount
		if This.GetItemString(ll_row, "date_type_ind") = 'D' and &
						This.GetItemString(ll_row, 'category') = ls_category  Then
			if ls_formula_type = 'UPDT' then
				dw_satellite_cattle.SetItem(ll_row, ls_column_name, 0)
			end if
			dw_satellite_cattle.SetItem(ll_row, ls_column_name + '_ind', ls_formula_type)
			dw_satellite_cattle.SetItem(ll_row, 'update_flag', 'U')
		End if
	next 
	if Not ls_formula_type = 'UPDT' Then
		lb_update_all = True
		ls_column_num = RightTrim(Right(ls_column_name, (Len(ls_column_name) - Pos(ls_column_name, '_', 1))))
		wf_calc_pct(ls_column_num, ls_category, row, lb_update_all)
	end if
end if

return
end event

