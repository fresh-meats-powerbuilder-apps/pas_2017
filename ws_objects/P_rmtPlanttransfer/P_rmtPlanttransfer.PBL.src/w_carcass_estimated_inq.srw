﻿$PBExportHeader$w_carcass_estimated_inq.srw
$PBExportComments$ibdkdld
forward
global type w_carcass_estimated_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_carcass_estimated_inq
end type
type dw_date from u_effective_date within w_carcass_estimated_inq
end type
type dw_shift from u_shift within w_carcass_estimated_inq
end type
end forward

global type w_carcass_estimated_inq from w_base_response_ext
integer width = 1682
integer height = 416
long backcolor = 67108864
dw_plant dw_plant
dw_date dw_date
dw_shift dw_shift
end type
global w_carcass_estimated_inq w_carcass_estimated_inq

on w_carcass_estimated_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_date=create dw_date
this.dw_shift=create dw_shift
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_date
this.Control[iCurrent+3]=this.dw_shift
end on

on w_carcass_estimated_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_date)
destroy(this.dw_shift)
end on

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
 				ls_shift, &
				ls_date
				
Int			li_pa_range				

u_string_functions		lu_strings


If dw_plant.AcceptText() = -1 or &
	dw_shift.AcceptText() = -1 or &
	dw_date.AcceptText() = -1 Then return 
	
ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End If

ls_date = String(dw_date.uf_get_effective_date(), 'YYYY-mm-dd')
If lu_strings.nf_IsEmpty(ls_date) Then
	iw_frame.SetMicroHelp("Date is a required field")
	dw_date.SetFocus()
	Return
End If

ls_shift = dw_shift.uf_get_shift()
If lu_strings.nf_IsEmpty(ls_shift) Then
	iw_frame.SetMicroHelp("Shift is a required field")
	dw_shift.SetFocus()
	Return
End If

iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('effective_date',ls_date)
iw_parentwindow.Event ue_set_data('shift',ls_shift)


ib_ok_to_close = True

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel; Close(This)
end event

event ue_postopen;call super::ue_postopen;String ls_text,ls_type,ls_message

u_string_functions lu_string

iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('effective_date')
dw_date.uf_set_effective_date(date(Message.StringParm))

iw_parentwindow.Event ue_get_data('shift')
dw_shift.uf_set_shift(Message.StringParm)

iw_parentwindow.Event ue_get_data('date_object_text')
ls_message = Message.StringParm
If Not lu_string.nf_isempty(ls_message) Then
	ls_text = lu_string.nf_gettoken(ls_message,"~t")
	ls_type = lu_string.nf_gettoken(ls_message,"~t")
	dw_date.uf_initilize(ls_text,ls_type)
End If




end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_carcass_estimated_inq
integer x = 1376
integer y = 208
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_carcass_estimated_inq
integer x = 1088
integer y = 208
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_carcass_estimated_inq
integer x = 800
integer y = 208
integer taborder = 30
end type

type dw_plant from u_plant within w_carcass_estimated_inq
integer x = 219
integer y = 12
integer taborder = 60
boolean bringtotop = true
end type

type dw_date from u_effective_date within w_carcass_estimated_inq
integer y = 104
integer height = 84
integer taborder = 10
boolean bringtotop = true
end type

type dw_shift from u_shift within w_carcass_estimated_inq
integer x = 233
integer y = 196
integer height = 68
integer taborder = 20
boolean bringtotop = true
end type

