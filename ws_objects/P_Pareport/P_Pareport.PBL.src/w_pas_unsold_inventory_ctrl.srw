﻿$PBExportHeader$w_pas_unsold_inventory_ctrl.srw
forward
global type w_pas_unsold_inventory_ctrl from w_base_sheet_ext
end type
type dw_division from u_division within w_pas_unsold_inventory_ctrl
end type
type dw_unsold_inv_print from u_base_dw_ext within w_pas_unsold_inventory_ctrl
end type
type tab_1 from tab within w_pas_unsold_inventory_ctrl
end type
type tp_unsold_inv from userobject within tab_1
end type
type dw_unsold_inv from u_base_dw_ext within tp_unsold_inv
end type
type tp_unsold_inv from userobject within tab_1
dw_unsold_inv dw_unsold_inv
end type
type tp_pct_of_prod from userobject within tab_1
end type
type dw_pct_of_prod from u_base_dw_ext within tp_pct_of_prod
end type
type tp_pct_of_prod from userobject within tab_1
dw_pct_of_prod dw_pct_of_prod
end type
type tp_prod_buffer from userobject within tab_1
end type
type dw_prod_buffer from u_base_dw_ext within tp_prod_buffer
end type
type tp_prod_buffer from userobject within tab_1
dw_prod_buffer dw_prod_buffer
end type
type tab_1 from tab within w_pas_unsold_inventory_ctrl
tp_unsold_inv tp_unsold_inv
tp_pct_of_prod tp_pct_of_prod
tp_prod_buffer tp_prod_buffer
end type
type dw_scroll_product from u_base_dw_ext within w_pas_unsold_inventory_ctrl
end type
end forward

global type w_pas_unsold_inventory_ctrl from w_base_sheet_ext
integer x = 338
integer y = 400
integer width = 2062
integer height = 1616
string title = "Product Parameters"
long backcolor = 12632256
dw_division dw_division
dw_unsold_inv_print dw_unsold_inv_print
tab_1 tab_1
dw_scroll_product dw_scroll_product
end type
global w_pas_unsold_inventory_ctrl w_pas_unsold_inventory_ctrl

type variables
DataWindowChild	idwc_section, &
		idwc_section_exp

Double		id_task_number, &
		id_Last_record, &
		id_Max_Record

u_pas203		iu_pas203
u_ws_pas4	iu_ws_pas4
s_error		istr_error_info

DataWindow	idw_top_dw
end variables

forward prototypes
public function string wf_get_division ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_print ()
end prototypes

public function string wf_get_division ();If dw_division.RowCount() > 0 Then
	return dw_division.uf_get_division()
End if
return ""
end function

public function boolean wf_retrieve ();Long			ll_row_count, &
				ll_row

String		ls_division, &
				ls_unsold

IF Not Super::wf_retrieve() Then Return False


SetPointer(HourGlass!)

ls_division  = dw_division.uf_get_division() + "~r~n"

tab_1.tp_unsold_inv.dw_unsold_inv.Reset()

istr_error_info.se_event_name = "wf_retrieve"
SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

/*If id_last_record <> id_max_record Then
	iu_pas203.nf_inq_unsold_inv_ctrl(istr_error_info, &
										ls_Division + '~tD~r~n', &
										id_task_number, &
										id_last_record, &
										id_max_record, &
										ls_unsold)
End if
*/
//If id_last_record <> id_max_record Then
	iu_ws_pas4.NF_PASP46FR(istr_error_info, &
										ls_Division, &
										ls_unsold)
//End if


id_task_number = 0
id_last_record = 0
id_max_record  = 0

This.SetRedraw(False)

// Changed it so it loops here and retrieves all of them --
//   there's code elsewhere (scrollvertical) that retrieves the next
//   page.
/*Do
	If Not iu_pas203.nf_inq_unsold_inv_ctrl(istr_error_info, &
										ls_Division, &
										id_task_number, &
										id_last_record, &
										id_max_record, &
										ls_unsold) Then 
		This.SetRedraw(True)
		Return False
	End If
	
	
Loop While id_last_record <> id_max_record
*/
ll_row_count = tab_1.tp_unsold_inv.dw_unsold_inv.ImportString(ls_unsold)
ll_row_count = tab_1.tp_unsold_inv.dw_unsold_inv.RowCount()

ll_row = 0
Do
	ll_row = tab_1.tp_unsold_inv.dw_unsold_inv.Find("cut_style = 'X'", ll_row + 1, ll_row_count + 1)
	If ll_row > 0 Then	
		tab_1.tp_unsold_inv.dw_unsold_inv.SetItem(ll_row, 'grade', '')
	End if
Loop While ll_row > 0

SetMicroHelp("Inquire Successful")

tab_1.tp_unsold_inv.dw_unsold_inv.ResetUpdate()
tab_1.tp_unsold_inv.dw_unsold_inv.Sort()

This.SetRedraw(True)

Return True
end function

public function boolean wf_update ();Int		li_counter, &
			li_column

Long		ll_ModifiedCount, &
			ll_row

String	ls_division, &
			ls_update, &
			ls_temp, &
			ls_ColumnName


If tab_1.tp_unsold_inv.dw_unsold_inv.AcceptText() = -1 Then return False

ll_ModifiedCount = tab_1.tp_unsold_inv.dw_unsold_inv.ModifiedCount()
If ll_ModifiedCount < 1 Then 
	iw_frame.SetMicroHelp("No Update Necessary")
	return True
End if

ll_row = 1
li_column = 1

tab_1.tp_unsold_inv.dw_unsold_inv.SetRedraw(False)

//f_required(dw_unsold_inv, True)
tab_1.tp_unsold_inv.dw_unsold_inv.Modify("grade." + tab_1.tp_unsold_inv.dw_unsold_inv.Describe("grade.Edit.Style") + &
		".Required=Yes")
tab_1.tp_unsold_inv.dw_unsold_inv.FindRequired(Primary!, ll_row, li_column, ls_ColumnName, False)
If ll_row > 0 Then
	f_required(tab_1.tp_unsold_inv.dw_unsold_inv, False)
	tab_1.tp_unsold_inv.dw_unsold_inv.SetRedraw(True)
	MessageBox("Required Field", tab_1.tp_unsold_inv.dw_unsold_inv.Describe(ls_ColumnName + '_t.Text') + &
					" is a required field for product code " + &
					Trim(tab_1.tp_unsold_inv.dw_unsold_inv.GetItemString(ll_row,"sku_product_code")) + ".")
	tab_1.tp_unsold_inv.dw_unsold_inv.SetRow(ll_row)
	tab_1.tp_unsold_inv.dw_unsold_inv.SetColumn(li_column)
	tab_1.tp_unsold_inv.dw_unsold_inv.ScrollToRow(ll_row)
	tab_1.tp_unsold_inv.dw_unsold_inv.SetFocus()
	return False
End if
//f_required(dw_unsold_inv, False)
tab_1.tp_unsold_inv.dw_unsold_inv.Modify("grade." + tab_1.tp_unsold_inv.dw_unsold_inv.Describe("grade.Edit.Style") + &
		".Required=No")


// If this isn't done, the text disappears
// I can assume row 1 is valid if we got to this point
ll_row = tab_1.tp_unsold_inv.dw_unsold_inv.GetRow()
tab_1.tp_unsold_inv.dw_unsold_inv.SetRow(1)
tab_1.tp_unsold_inv.dw_unsold_inv.SetRow(ll_row)
// Force a redraw
tab_1.tp_unsold_inv.dw_unsold_inv.SetRedraw(True)

iw_frame.SetMicroHelp("Updating the Database ...")
SetPointer(HourGlass!)

// the update doesn't really use the division code
ls_division = '  '
ll_row = 0
ls_update = ''

For li_counter = 1 to ll_ModifiedCount
	ll_row = tab_1.tp_unsold_inv.dw_unsold_inv.GetNextModified(ll_row, Primary!)

	ls_update += tab_1.tp_unsold_inv.dw_unsold_inv.GetItemString(ll_row, 'sku_product_code') + '~t'
	ls_temp = tab_1.tp_unsold_inv.dw_unsold_inv.GetItemString(ll_row, 'cut_style')
	If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
		MessageBox("Cut Style", "Cut Style is a required field for product code " + &
			Trim(tab_1.tp_unsold_inv.dw_unsold_inv.GetItemString(ll_row,"sku_product_code")) + ".")
		iw_frame.SetMicroHelp("Cut Style is a required field")
		tab_1.tp_unsold_inv.dw_unsold_inv.SetColumn('cut_style')
		tab_1.tp_unsold_inv.dw_unsold_inv.SetRow(ll_row)
		tab_1.tp_unsold_inv.dw_unsold_inv.SetFocus()
		return False
	End if

	ls_update += ls_temp + '~t'
	ls_update += tab_1.tp_unsold_inv.dw_unsold_inv.GetItemString(ll_row, 'grade') + "~t" 
	ls_update += tab_1.tp_unsold_inv.dw_unsold_inv.GetItemString(ll_row,'pct_of_prod') + "~t"
	ls_update += tab_1.tp_prod_buffer.dw_prod_buffer.GetItemString(ll_row,'prod_buffer') + "~r~n"

	If Mod(li_counter, 550) = 0 Then
//		If Not iu_pas203.nf_upd_uns_inv_ctrl(istr_error_info, &
//												ls_division, &
//												ls_update) Then return False
				If Not iu_ws_pas4.nf_pasp47fr(istr_error_info, &
												ls_division, &
												ls_update) Then return False
		
		
		ls_update = ''
	End if
Next

//If Not iu_pas203.nf_upd_uns_inv_ctrl(istr_error_info, &
//											ls_division, &
//											ls_update) Then return False
	If Not iu_ws_pas4.nf_pasp47fr(istr_error_info, &
												ls_division, &
												ls_update) Then return False
tab_1.tp_unsold_inv.dw_unsold_inv.ResetUpdate()

iw_frame.SetMicroHelp("Update Successful")
return True

end function

public subroutine wf_print ();Long		ll_FirstRow
String	ls_Division, &
			ls_Unsold

If Not wf_update() then return

Do While id_last_record <> id_max_record

	SetMicroHelp("Retrieving additional rows ...")
	SetPointer(HourGlass!)
//	If Not iu_pas203.nf_inq_unsold_inv_ctrl(istr_error_info, &
//											ls_Division, &
//											id_task_number, &
//											id_last_record, &
//											id_max_record, &
//											ls_unsold) Then Return
	This.SetRedraw(False)
	ll_FirstRow = Integer(tab_1.tp_unsold_inv.dw_unsold_inv.Describe("DataWindow.FirstRowOnPage"))
	tab_1.tp_unsold_inv.dw_unsold_inv.RowsMove(1, tab_1.tp_unsold_inv.dw_unsold_inv.RowCount(), Primary!, tab_1.tp_unsold_inv.dw_unsold_inv, 1, Filter!)
	tab_1.tp_unsold_inv.dw_unsold_inv.ImportString(ls_unsold)
	tab_1.tp_unsold_inv.dw_unsold_inv.ResetUpdate()
	tab_1.tp_unsold_inv.dw_unsold_inv.RowsMove(1, tab_1.tp_unsold_inv.dw_unsold_inv.FilteredCount(), Filter!, tab_1.tp_unsold_inv.dw_unsold_inv, 1, Primary!)
	tab_1.tp_unsold_inv.dw_unsold_inv.ScrollToRow(ll_FirstRow)
	This.SetRedraw(True)
	iw_frame.SetMicroHelp("Additional rows retrieved successfully")
Loop

tab_1.tp_unsold_inv.dw_unsold_inv.ShareData(dw_unsold_inv_print)

dw_unsold_inv_print.Modify("Division_code.Text = '" + dw_division.uf_Get_Division() + &
									"' Division_description.Text = '" + &
									dw_division.uf_Get_Division_Descr() + "'")

dw_unsold_inv_print.Print()
return 
end subroutine

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_cut_style, &
						ldwc_cut_style_print, &
						ldwc_section_print, &
						ldwc_pct_of_prod_print, &
						ldwc_prod_buffer_print, &
						ldwc_pct_of_prod, &
						ldwc_prod_buffer

Int					li_counter

Long					ll_row_count

String				ls_text


iu_pas203 = Create u_pas203
iu_ws_pas4	=	Create	u_ws_pas4

If Message.ReturnValue = -1 Then
	Close(This)
	return
End if

is_inquire_window_name = 'w_division_inq'

istr_error_info.se_user_id = sqlca.userid
istr_error_info.se_Window_name = "UnsInvCt"
 
tab_1.tp_unsold_inv.dw_unsold_inv.GetChild( "cut_style", ldwc_cut_style )
tab_1.tp_unsold_inv.dw_unsold_inv.GetChild( "grade", idwc_section )
tab_1.tp_unsold_inv.dw_unsold_inv.GetChild( "grade_exp", idwc_section_exp )
tab_1.tp_pct_of_prod.dw_pct_of_prod.GetChild( "pct_of_prod", ldwc_pct_of_prod )
tab_1.tp_prod_buffer.dw_prod_buffer.GetChild( "prod_buffer", ldwc_prod_buffer )

ldwc_cut_style.SetTransObject(SQLCA)
ldwc_cut_style.Retrieve("CUTSTYLE")
idwc_section.SetTransObject(SQLCA)
idwc_section.Retrieve("DOM SECT")
idwc_section_exp.SetTransObject(SQLCA)
idwc_section_exp.Retrieve("EXP SECT")
ldwc_pct_of_prod.SetTransObject(SQLCA)
ldwc_pct_of_prod.Retrieve("PCTOFPRD")
ldwc_prod_buffer.SetTransObject(SQLCA)
ldwc_prod_buffer.Retrieve("PRODBUFR")

ll_row_count = ldwc_cut_style.RowCount()
For li_counter = 1 to ll_row_count
	ls_text = ldwc_cut_style.GetItemString(li_counter, 'type_code')
	If iw_frame.iu_string.nf_IsEmpty(ls_text) Then
		ldwc_cut_style.SetItem(li_counter, 'type_code', Space(1))
	Else
		ldwc_cut_style.SetItem(li_counter, 'type_code', Trim(ls_text))
	End if
Next

ll_row_count = idwc_section.RowCount()
For li_counter = 1 to ll_row_count
	ls_text = idwc_section.GetItemString(li_counter, 'type_code')
	If iw_frame.iu_string.nf_IsEmpty(ls_text) Then
		idwc_section.SetItem(li_counter, 'type_code', Space(1))
	Else
		idwc_section.SetItem(li_counter, 'type_code', Trim(ls_text))
	End if
Next

ll_row_count = idwc_section_exp.RowCount()
For li_counter = 1 to ll_row_count
	ls_text = idwc_section_exp.GetItemString(li_counter, 'type_code')
	If iw_frame.iu_string.nf_IsEmpty(ls_text) Then
		idwc_section_exp.SetItem(li_counter, 'type_code', Space(1))
	Else
		idwc_section_exp.SetItem(li_counter, 'type_code', Trim(ls_text))
	End if
Next

ll_row_count = ldwc_pct_of_prod.RowCount()
For li_counter = 1 to ll_row_count
	ls_text = ldwc_pct_of_prod.GetItemString(li_counter, 'type_code')
	If iw_frame.iu_string.nf_IsEmpty(ls_text) Then
		ldwc_pct_of_prod.SetItem(li_counter, 'type_code', Space(1))
	Else
		ldwc_pct_of_prod.SetItem(li_counter, 'type_code', Trim(ls_text))
	End if
Next

dw_unsold_inv_print.GetChild( "cut_style", ldwc_cut_style_print )
dw_unsold_inv_print.GetChild( "grade", ldwc_section_print )
dw_unsold_inv_print.GetChild( "pct_of_prod", ldwc_pct_of_prod_print )
dw_unsold_inv_print.GetChild( "prod_buffer", ldwc_prod_buffer_print )


ldwc_section_print.ImportString(idwc_section.Describe("DataWindow.Data"))
ldwc_section_print.ImportString(idwc_section_exp.Describe("DataWindow.Data"))
ldwc_pct_of_prod_print.ImportString(ldwc_pct_of_prod.Describe("DataWindow.Data"))
ldwc_prod_buffer_print.ImportString(ldwc_prod_buffer.Describe("DataWindow.Data"))

ldwc_cut_style.ShareData(ldwc_cut_style_print)
tab_1.tp_unsold_inv.dw_unsold_inv.ShareData(dw_unsold_inv_print)
tab_1.tp_unsold_inv.dw_unsold_inv.ShareData(tab_1.tp_pct_of_prod.dw_pct_of_prod)
tab_1.tp_unsold_inv.dw_unsold_inv.ShareData(tab_1.tp_prod_buffer.dw_prod_buffer)

Choose Case tab_1.SelectedTab
	Case 1
		idw_top_dw = tab_1.tp_unsold_inv.dw_unsold_inv
	Case 2
		idw_top_dw = tab_1.tp_pct_of_prod.dw_pct_of_prod
	Case 3
		idw_top_dw = tab_1.tp_prod_buffer.dw_prod_buffer
	Case Else
		idw_top_dw = tab_1.tp_unsold_inv.dw_unsold_inv
		tab_1.SelectedTab = 1
End Choose
 
iw_frame.im_menu.m_file.m_inquire.TriggerEvent(Clicked!)
end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

end on

event resize;call super::resize;
 
 integer li_dw_x		= 50
 integer li_dw_y		= 125


if il_BorderPaddingWidth > li_dw_x Then
   li_dw_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_dw_y Then
   li_dw_y = il_BorderPaddingHeight
End If

//tab_1.width = width - (50 + tab_1.x)
//tab_1.height = height - (125 + tab_1.y)
//
//tab_1.tp_unsold_inv.dw_unsold_inv.width	= tab_1.width - (50 + tab_1.tp_unsold_inv.dw_unsold_inv.x)
//tab_1.tp_unsold_inv.dw_unsold_inv.height	= tab_1.height - (125 + tab_1.tp_unsold_inv.dw_unsold_inv.y)
//
//tab_1.tp_pct_of_prod.dw_pct_of_prod.width	= tab_1.width - (50 + tab_1.tp_pct_of_prod.dw_pct_of_prod.x)
//tab_1.tp_pct_of_prod.dw_pct_of_prod.height	= tab_1.height - (125 + tab_1.tp_pct_of_prod.dw_pct_of_prod.y)
//
//tab_1.tp_prod_buffer.dw_prod_buffer.width	= tab_1.width - (50 + tab_1.tp_prod_buffer.dw_prod_buffer.x)
//tab_1.tp_prod_buffer.dw_prod_buffer.height	= tab_1.height - (125 + tab_1.tp_prod_buffer.dw_prod_buffer.y)
//


tab_1.width = width - (li_dw_x + tab_1.x)
tab_1.height = height - (li_dw_y + tab_1.y)

tab_1.tp_unsold_inv.dw_unsold_inv.width	= tab_1.width - (li_dw_x + tab_1.tp_unsold_inv.dw_unsold_inv.x)
tab_1.tp_unsold_inv.dw_unsold_inv.height	= tab_1.height - (li_dw_y  + tab_1.tp_unsold_inv.dw_unsold_inv.y)

tab_1.tp_pct_of_prod.dw_pct_of_prod.width	= tab_1.width - (li_dw_x + tab_1.tp_pct_of_prod.dw_pct_of_prod.x)
tab_1.tp_pct_of_prod.dw_pct_of_prod.height	= tab_1.height - (li_dw_y  + tab_1.tp_pct_of_prod.dw_pct_of_prod.y)

tab_1.tp_prod_buffer.dw_prod_buffer.width	= tab_1.width - (li_dw_x + tab_1.tp_prod_buffer.dw_prod_buffer.x)
tab_1.tp_prod_buffer.dw_prod_buffer.height	= tab_1.height - (li_dw_y  + tab_1.tp_prod_buffer.dw_prod_buffer.y)

end event

event close;call super::close;String	ls_division, &
			ls_Unsold


If id_last_record <> id_max_record Then
	ls_division = dw_division.Uf_Get_division() + '~tD~r~n'
	istr_error_info.se_event_name = "close"
	SetPointer(HourGlass!)
//	iu_pas203.nf_inq_unsold_inv_ctrl(istr_error_info, &
//										ls_Division, &
//										id_task_number, &
//										id_last_record, &
//										id_max_record, &
//										ls_unsold)
End if

If IsValid(iu_pas203) Then destroy iu_pas203
end event

on w_pas_unsold_inventory_ctrl.create
int iCurrent
call super::create
this.dw_division=create dw_division
this.dw_unsold_inv_print=create dw_unsold_inv_print
this.tab_1=create tab_1
this.dw_scroll_product=create dw_scroll_product
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
this.Control[iCurrent+2]=this.dw_unsold_inv_print
this.Control[iCurrent+3]=this.tab_1
this.Control[iCurrent+4]=this.dw_scroll_product
end on

on w_pas_unsold_inventory_ctrl.destroy
call super::destroy
destroy(this.dw_division)
destroy(this.dw_unsold_inv_print)
destroy(this.tab_1)
destroy(this.dw_scroll_product)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'division'
		dw_division.uf_set_division(as_value)
End Choose

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'division'
		Message.StringParm = dw_division.uf_get_division()
End Choose


end event

type dw_division from u_division within w_pas_unsold_inventory_ctrl
integer x = 37
integer y = 12
integer height = 92
integer taborder = 30
end type

on constructor;call u_division::constructor;This.Disable()
end on

type dw_unsold_inv_print from u_base_dw_ext within w_pas_unsold_inventory_ctrl
boolean visible = false
integer x = 27
integer y = 564
integer width = 1856
integer taborder = 20
string dataobject = "d_unsold_inv_print"
end type

type tab_1 from tab within w_pas_unsold_inventory_ctrl
integer y = 192
integer width = 1993
integer height = 1284
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
integer selectedtab = 1
tp_unsold_inv tp_unsold_inv
tp_pct_of_prod tp_pct_of_prod
tp_prod_buffer tp_prod_buffer
end type

on tab_1.create
this.tp_unsold_inv=create tp_unsold_inv
this.tp_pct_of_prod=create tp_pct_of_prod
this.tp_prod_buffer=create tp_prod_buffer
this.Control[]={this.tp_unsold_inv,&
this.tp_pct_of_prod,&
this.tp_prod_buffer}
end on

on tab_1.destroy
destroy(this.tp_unsold_inv)
destroy(this.tp_pct_of_prod)
destroy(this.tp_prod_buffer)
end on

event selectionchanging;String ls_firstrow

If IsValid(idw_top_dw) Then
	ls_firstrow = idw_top_dw.Describe("DataWindow.VerticalScrollPosition")

	// This should synchronize the two datawindows' scrolling
	Choose Case newindex
	 	Case 1   // Switched to Unsold Inventory
			idw_top_dw = This.tp_unsold_inv.dw_unsold_inv
		Case 2   // Switched to Pct of Prod
			idw_top_dw = This.tp_pct_of_prod.dw_pct_of_prod
		Case 3   // Switched to Prod Buffer
			idw_top_dw = This.tp_prod_buffer.dw_prod_buffer
	End Choose
	idw_top_dw.Modify("DataWindow.VerticalScrollPosition = '" + ls_firstrow + "'")	
End If
return 0  // Allow the tabpage selection to change
end event

type tp_unsold_inv from userobject within tab_1
integer x = 18
integer y = 100
integer width = 1957
integer height = 1168
long backcolor = 12632256
string text = "Unsold Inventory"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_unsold_inv dw_unsold_inv
end type

on tp_unsold_inv.create
this.dw_unsold_inv=create dw_unsold_inv
this.Control[]={this.dw_unsold_inv}
end on

on tp_unsold_inv.destroy
destroy(this.dw_unsold_inv)
end on

type dw_unsold_inv from u_base_dw_ext within tp_unsold_inv
integer x = 14
integer y = 16
integer width = 1934
integer height = 1148
integer taborder = 2
string dataobject = "d_unsold_inv"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;String	ls_Null, &
			ls_GetText


ls_GetText = This.GetText()

SetNull(ls_Null)

Choose Case This.GetColumnName()
Case 'cut_style'
	If ls_GetText = 'X' Then
		This.SetItem(This.GetRow(), 'grade', '')
	Else
		This.SetItem(This.GetRow(), 'grade', ls_Null)
	End if
End Choose
end event

event scrollvertical;call super::scrollvertical;Long		ll_FirstRow, &
			ll_Row_Count, &
			ll_Row

String	ls_division, &
			ls_Unsold


If id_last_record <> id_max_record And &
	Integer(This.Describe("DataWindow.LastRowOnPage")) >= This.RowCount() - 20 Then

	ls_division = dw_division.Uf_Get_division()

	istr_error_info.se_event_name = "scrollvertical"
	SetMicroHelp("Retrieving additional rows ...")
	SetPointer(HourGlass!)
//	If Not iu_pas203.nf_inq_unsold_inv_ctrl(istr_error_info, &
//											ls_Division, &
//											id_task_number, &
//											id_last_record, &
//											id_max_record, &
//											ls_unsold) Then Return
	This.SetRedraw(False)
	ll_FirstRow = Integer(This.Describe("DataWindow.FirstRowOnPage"))
	This.RowsMove(1, This.RowCount(), Primary!, This, 1, Filter!)

	ll_row_count = This.ImportString(ls_unsold)

	ll_row = 0
	Do
		ll_row = This.Find("cut_style = 'X'", ll_row + 1, ll_row_count)
		If ll_row > 0 Then	
			This.SetItem(ll_row, 'grade', '')
		End if
	Loop While ll_row > 0


	This.ResetUpdate()
	This.RowsMove(1, This.FilteredCount(), Filter!, This, 1, Primary!)
	This.ScrollToRow(ll_FirstRow)
	This.SetRedraw(True)
	iw_frame.SetMicroHelp("Additional rows retrieved successfully")

End if

end event

type tp_pct_of_prod from userobject within tab_1
integer x = 18
integer y = 100
integer width = 1957
integer height = 1168
long backcolor = 12632256
string text = "PA Cutoff Time"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_pct_of_prod dw_pct_of_prod
end type

on tp_pct_of_prod.create
this.dw_pct_of_prod=create dw_pct_of_prod
this.Control[]={this.dw_pct_of_prod}
end on

on tp_pct_of_prod.destroy
destroy(this.dw_pct_of_prod)
end on

type dw_pct_of_prod from u_base_dw_ext within tp_pct_of_prod
integer x = 14
integer y = 16
integer width = 1833
integer height = 1148
integer taborder = 2
string dataobject = "d_unsold_inv_pct"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event scrollvertical;call super::scrollvertical;Long		ll_FirstRow, &
			ll_Row_Count, &
			ll_Row

String	ls_division, &
			ls_Unsold


If id_last_record <> id_max_record And &
	Integer(This.Describe("DataWindow.LastRowOnPage")) >= This.RowCount() - 20 Then

	ls_division = dw_division.Uf_Get_division()

	istr_error_info.se_event_name = "scrollvertical"
	SetMicroHelp("Retrieving additional rows ...")
	SetPointer(HourGlass!)
//	If Not iu_pas203.nf_inq_unsold_inv_ctrl(istr_error_info, &
//											ls_Division, &
//											id_task_number, &
//											id_last_record, &
//											id_max_record, &
//											ls_unsold) Then Return
	This.SetRedraw(False)
	ll_FirstRow = Integer(This.Describe("DataWindow.FirstRowOnPage"))
	This.RowsMove(1, This.RowCount(), Primary!, This, 1, Filter!)

	ll_row_count = This.ImportString(ls_unsold)

	Long ll_temp
	ll_temp = This.RowCount()
	
	ll_row = 0
	Do
		ll_row = This.Find("cut_style = 'X'", ll_row + 1, ll_row_count)
		If ll_row > 0 Then	
//			This.SetItem(ll_row, 'grade', '')
		End if
	Loop While ll_row > 0


	This.ResetUpdate()
	This.RowsMove(1, This.FilteredCount(), Filter!, This, 1, Primary!)
	This.ScrollToRow(ll_FirstRow)
	This.SetRedraw(True)
	iw_frame.SetMicroHelp("Additional rows retrieved successfully")

End if

end event

type tp_prod_buffer from userobject within tab_1
integer x = 18
integer y = 100
integer width = 1957
integer height = 1168
long backcolor = 12632256
string text = "Production Buffer"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_prod_buffer dw_prod_buffer
end type

on tp_prod_buffer.create
this.dw_prod_buffer=create dw_prod_buffer
this.Control[]={this.dw_prod_buffer}
end on

on tp_prod_buffer.destroy
destroy(this.dw_prod_buffer)
end on

type dw_prod_buffer from u_base_dw_ext within tp_prod_buffer
integer x = 14
integer y = 16
integer width = 1842
integer height = 1148
integer taborder = 2
string dataobject = "d_unsold_inv_prod_buffer"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_scroll_product from u_base_dw_ext within w_pas_unsold_inventory_ctrl
integer x = 41
integer y = 108
integer width = 1431
integer height = 80
integer taborder = 10
string dataobject = "d_scroll_product"
boolean border = false
end type

event editchanged;call super::editchanged;Long	ll_row, &
		ll_first_row, &
		ll_last_row

ll_row = idw_top_dw.Find("sku_product_code >= '" + &
		data + "'",1,idw_top_dw.RowCount()+1)

If ll_row > 0 Then 
	idw_top_dw.ScrollToRow(ll_row)
	idw_top_dw.SetRow(ll_row + 1)
End If

ll_first_row = Long(idw_top_dw.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(idw_top_dw.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	idw_top_dw.SetRedraw(False)
	idw_top_dw.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	idw_top_dw.ScrollToRow(ll_row)
	idw_top_dw.SetRow(ll_row + 1)
	idw_top_dw.SetRedraw(True)
End If
end event

event constructor;call super::constructor;InsertRow(0)
ib_updateable = False
end event

