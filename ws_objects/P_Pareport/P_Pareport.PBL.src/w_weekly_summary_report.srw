﻿$PBExportHeader$w_weekly_summary_report.srw
forward
global type w_weekly_summary_report from w_netwise_sheet
end type
type cbx_load from checkbox within w_weekly_summary_report
end type
type dw_division from u_division within w_weekly_summary_report
end type
end forward

global type w_weekly_summary_report from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 1641
integer height = 388
string title = "Weekly Summary Report (3 Week)"
long backcolor = 79741120
cbx_load cbx_load
dw_division dw_division
end type
global w_weekly_summary_report w_weekly_summary_report

type variables
s_error			istr_error_info
string			Is_inquire_parm

u_pas201		iu_pas201

u_ws_pas2      iu_ws_pas2
end variables

forward prototypes
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
end prototypes

public function boolean wf_validate (long al_row);string ls_division

ls_division = dw_division.GetItemString(1, 'division_code')

IF IsNull(ls_division) or Len(trim(ls_division)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a division code.  Division cannot be left blank.")
	This.SetRedraw(False)
	dw_division.ScrollToRow(1)
	dw_division.SetColumn("division_code")
	dw_division.SetFocus()
	This.SetRedraw(True)
	Return False
End If

Return True
end function

public function boolean wf_update ();boolean lb_Return

string ls_update_string, &
			ls_tranid, &
			ls_type, &
			ls_plant_name, &
			ls_view, &
			ls_month, &
			ls_userid, &
			ls_date, &
			ls_time, &
			ls_opid, &
			ls_division, &
			ls_loads
			
			
SetPointer(HourGlass!)

IF dw_division.AcceptText() = -1 THEN Return( False )

If Not wf_validate(1) Then
			dw_division.SetReDraw(True)
	Return False
End If
////
//values to pass to common report initiator for pas126xe
ls_division = trim(dw_division.GetItemString(1, 'division_code'))
ls_type = '2'

if ls_division = '05' then
	ls_tranid = 'P047'
else
	if ls_division = '07' then
		ls_tranid = 'P047'
	else
		if ls_division = '06' then
			ls_tranid = 'P065'
		else
			if ls_division = '31' then
				ls_tranid = 'P065'
			else
				if ls_division = '32' then
					ls_tranid = 'P065'
				else
					if ls_division = '33' then
						ls_tranid = 'P065'
					else	
						ls_tranid = 'P107'
					end if
				end if
			end if
		end if
	end if
end if

If cbx_load.checked Then
	ls_loads = 'PAS166XL'
ELSE
		ls_loads = '        '
end if
	
ls_plant_name = '            '
ls_view = 'V'
ls_userid = sqlca.userid
ls_date = string(today(), "YYMMDD")
ls_time = string(Now(), "HHMMSS")
ls_opid = Right(ls_userid,3)
ls_update_string = ls_type  + &
 					ls_tranid + &
					ls_opid + &
					ls_date + &
					ls_time + &
					ls_loads + &
					'     ' + &					
					trim(dw_division.GetItemString(1, 'division_code')) + &
					ls_view + &
					ls_userid + ' '
//


istr_error_info.se_event_name = "wf_update"
//istr_error_info.se_procedure_name = "u_pas201.nf_pasp72cr_init_mainframe_report"
istr_error_info.se_procedure_name = "nf_pasp72fr"
istr_error_info.se_message = Space(71)

//lb_return = iu_pas201.nf_pasp72cr_init_mainframe_report(istr_error_info, ls_Update_string)

lb_return = iu_ws_pas2.nf_pasp72gr(ls_Update_string,istr_error_info)

SetPointer(Arrow!)

iw_Frame.SetMicroHelp("Report initiated.")


return lb_Return
end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	


end event

event ue_postopen;call super::ue_postopen;string	ls_server_suffix, &
			ls_division

iu_pas201 = CREATE u_pas201
iu_ws_pas2 = Create u_ws_pas2

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_weekly_sales_report_by_plant"
istr_error_info.se_user_id 		= sqlca.userid


dw_division.Modify("DataWindow.Color = 78682240")
ls_division = dw_division.GetItemString(1, 'division_code')

if ls_division = '06' or ls_division = '31' or ls_division = '33' then
   				cbx_load.Show()
Else
		cbx_load.Hide()
End if
//		End Choose	
//	Case 'effective_date'
//		dw_effective_date.uf_set_effective_date(Date(as_value))
//End Choose



//ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

//    InitializeControl("IBDKDLD ", _
//                           "HEADER6", _
//                           "t", _
//                           "PAS", _
//                           "TEST", _
//                           "OD16", _
//                           "V", _
//                           "????")
//

//ole_init_mainframe_reports.object.initializecontrol(sqlca.userid,sqlca.DBpass,ls_server_suffix,Message.nf_Get_App_ID(),"Initiate Product Special Handling Report","OD16","V","????") 
//ole_init_mainframe_reports.Height = This.Height - 100


end event

on w_weekly_summary_report.create
int iCurrent
call super::create
this.cbx_load=create cbx_load
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_load
this.Control[iCurrent+2]=this.dw_division
end on

on w_weekly_summary_report.destroy
call super::destroy
destroy(this.cbx_load)
destroy(this.dw_division)
end on

event resize;//if newwidth > 2694 then
//	ole_init_mainframe_reports.Width = newwidth - 25
//end if
//
//if (newheight - 100) > 500 then
//	ole_init_mainframe_reports.Height = newheight - 25
//end if

end event

event open;call super::open;String ls_division


cbx_load.Hide()
ls_division = dw_division.GetItemString(1, 'division_code')

if ls_division = '06' then
  	cbx_load.Show()
end if
end event

event close;call super::close;If IsValid( iu_pas201) Then Destroy( iu_pas201)
end event

event ue_get_data;call super::ue_get_data;//Choose Case as_value
//	Case 'plant'
//		Message.StringParm = dw_plant.uf_get_plant_code()
//End choose
//
end event

event ue_set_data;call super::ue_set_data;//Choose Case as_data_item
//	Case 'plant'
//		dw_plant.uf_set_plant_code(as_value)
//End choose
end event

event clicked;call super::clicked;String ls_division


cbx_load.Hide()
ls_division = trim(dw_division.GetItemString(1, 'division_code'))

if ls_division = '06' or ls_division = '31' or ls_division = '33' then
  	cbx_load.Show()
Else
	cbx_load.Hide()
end if
end event

type cbx_load from checkbox within w_weekly_summary_report
integer x = 219
integer y = 160
integer width = 366
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Loads"
end type

type dw_division from u_division within w_weekly_summary_report
integer y = 32
integer taborder = 10
boolean controlmenu = true
boolean hscrollbar = true
end type

event constructor;call super::constructor;if this.rowcount() = 0 then this.insertrow(0)
end event

event itemchanged;call super::itemchanged;String ls_division

AcceptText()
cbx_load.Hide()
ls_division = trim(dw_division.GetItemString(1, 'division_code'))

if ls_division = '06' then
  	cbx_load.Show()
end if

if ls_division = '31' then
  	cbx_load.Show()
end if

if ls_division = '33' then
  	cbx_load.Show()
end if




end event

