﻿$PBExportHeader$w_export_booking_report.srw
forward
global type w_export_booking_report from w_base_sheet_ext
end type
type dw_daily_weekly from u_base_dw_ext within w_export_booking_report
end type
type dw_metric_weight from u_base_dw_ext within w_export_booking_report
end type
type cbx_plant from checkbox within w_export_booking_report
end type
type cbx_customer from checkbox within w_export_booking_report
end type
type dw_export_options from u_base_dw_ext within w_export_booking_report
end type
type dw_date from u_base_dw_ext within w_export_booking_report
end type
type dw_plant from u_plant within w_export_booking_report
end type
type gb_page_break from groupbox within w_export_booking_report
end type
type gb_sort from groupbox within w_export_booking_report
end type
end forward

global type w_export_booking_report from w_base_sheet_ext
integer width = 1719
integer height = 896
string title = "Export Booking Report"
long backcolor = 67108864
dw_daily_weekly dw_daily_weekly
dw_metric_weight dw_metric_weight
cbx_plant cbx_plant
cbx_customer cbx_customer
dw_export_options dw_export_options
dw_date dw_date
dw_plant dw_plant
gb_page_break gb_page_break
gb_sort gb_sort
end type
global w_export_booking_report w_export_booking_report

type variables
u_pas201		iu_pas201

u_ws_pas2      iu_ws_pas2

s_error		istr_error_info

//DataStore	ids_grnd_beef_prty

String		is_output_data
end variables

forward prototypes
public subroutine wf_set_other_products (ref datawindow adw_input)
public function boolean wf_update ()
public function boolean wf_validate (long al_row)
end prototypes

public subroutine wf_set_other_products (ref datawindow adw_input);
end subroutine

public function boolean wf_update ();boolean lb_Return

string ls_update_string, &
			ls_tranid, &
			ls_type, &
			ls_view, &
			ls_userid, &
			ls_date, &
			ls_plant_checked, &
			ls_customer_checked, &
			ls_min_tons, &
			ls_max_tons, &
			ls_min_tons_format, &
			ls_max_tons_format
long		ll_min_tons, &
			ll_max_tons
dec{3}	ld_min_tons, &
			ld_max_tons

IF dw_date.AcceptText() = -1 THEN Return( False )	
IF dw_daily_weekly.AcceptText() = -1 THEN Return( False )
IF dw_export_options.AcceptText() = -1 THEN Return( False )
IF dw_metric_weight.AcceptText() = -1 THEN Return( False )
IF dw_plant.AcceptText() = -1 THEN Return( False )

			
SetPointer(HourGlass!)
If Not wf_validate(1) Then
			dw_plant.SetReDraw(True)
	Return False
End If
ls_tranid = 'O196' 
ls_type = '1'
If cbx_plant.checked = True Then
	ls_plant_checked = 'Y'
else
	ls_plant_checked = ' '
end if
If cbx_customer.checked = True Then
	ls_customer_checked = 'Y'
else
	ls_customer_checked = ' '
end if
//ls_temp = dw_metric_weight.Modify("min_metric_tons.Format='00000' " )
ld_min_tons = dw_metric_weight.GetItemNumber(1, 'min_metric_tons')
ld_max_tons = dw_metric_weight.GetItemNumber(1, 'max_metric_tons')
//ld_min_tons = dw_metric_weight.GetItemNumber(1, 'min_metric_tons')
//ls_min_tons = Right(string(ld_min_tons,2))
//ls_min_tons = ls_min_tons + left(ld_min_tons,3)
ls_min_tons = string(ld_min_tons, "00.000")
ls_max_tons = string(ld_max_tons, "00.000")
ls_min_tons_format = left(ls_min_tons,2)
ls_min_tons_format = ls_min_tons_format + right(ls_min_tons,3)
ls_max_tons_format = left(ls_max_tons,2)
ls_max_tons_format = ls_max_tons_format + right(ls_max_tons,3)

//ls_min_tons = string(dw_metric_weight.GetItemNumber(1, 'min_metric_tons'))
//ls_max_tons = string(dw_metric_weight.GetItemNumber(1, 'max_metric_tons'))
//ls_max_tons = string(ll_max_tons, "00000")
ls_view = 'V'
ls_userid = sqlca.userid
ls_date = string(dw_date.GetItemDate(1, 'sched_date'), "YYMMDD")
ls_update_string = ls_type  + &
 					ls_tranid + &
					 '    ' + &
					ls_userid + &
					' ' + &
					ls_view + &
					'N01    ' + &
					ls_date + &
					dw_plant.GetItemString(1, 'location_code') + &
					'   ' + &
					dw_daily_weekly.GetItemString(1, 'report_options') + &
					ls_plant_checked + &
					ls_customer_checked + &
					dw_export_options.GetItemString(1, 'page_break_options') + &
					ls_min_tons_format + &
					ls_max_tons_format
					
					


istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_pasp72gr"
istr_error_info.se_message = Space(71)

//lb_return = iu_pas201.nf_pasp72cr_init_mainframe_report(istr_error_info, ls_Update_string)

lb_return = iu_ws_pas2.nf_pasp72gr(ls_Update_string, istr_error_info)

SetPointer(Arrow!)

iw_Frame.SetMicroHelp("Report initiated.")

return lb_Return



end function

public function boolean wf_validate (long al_row);string ls_plant
long   ll_min_tons, &
		 ll_max_tons
		 
dec{3}	ld_min_tons, &
			ld_max_tons
		 
ls_plant = dw_plant.GetItemString(1, 'location_code')

IF IsNull(ls_plant) or Len(trim(ls_plant)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a plant.  Plant cannot be left blank.")
	This.SetRedraw(False)
	dw_plant.ScrollToRow(1)
	dw_plant.SetColumn("location_code")
	dw_plant.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ld_min_tons = dw_metric_weight.GetItemNumber(1, 'min_metric_tons')
if isNull(ld_min_tons) or ld_min_tons = 0 then
	iw_Frame.SetMicroHelp("Please enter a Minimum Tons.  Minimum Tons cannot be zero.")
	This.SetRedraw(False)
	dw_metric_weight.ScrollToRow(1)
	dw_metric_weight.SetColumn("min_metric_tons")
	dw_metric_weight.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ld_max_tons = dw_metric_weight.GetItemNumber(1, 'max_metric_tons')
if isNull(ld_max_tons) or ld_max_tons = 0 then
	iw_Frame.SetMicroHelp("Please enter a Maximum Tons.  Maximum Tons cannot be zero.")
	This.SetRedraw(False)
	dw_metric_weight.ScrollToRow(1)
	dw_metric_weight.SetColumn("max_metric_tons")
	dw_metric_weight.SetFocus()
	This.SetRedraw(True)
	Return False
End If

if ld_min_tons > ld_max_tons then
	iw_Frame.SetMicroHelp("Minimum Tons must be less than Maximum tons.")
	This.SetRedraw(False)
	dw_metric_weight.ScrollToRow(1)
	dw_metric_weight.SetColumn("max_metric_tons")
	dw_metric_weight.SetFocus()
	This.SetRedraw(True)
	Return False
End If



Return True
end function

on w_export_booking_report.create
int iCurrent
call super::create
this.dw_daily_weekly=create dw_daily_weekly
this.dw_metric_weight=create dw_metric_weight
this.cbx_plant=create cbx_plant
this.cbx_customer=create cbx_customer
this.dw_export_options=create dw_export_options
this.dw_date=create dw_date
this.dw_plant=create dw_plant
this.gb_page_break=create gb_page_break
this.gb_sort=create gb_sort
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_daily_weekly
this.Control[iCurrent+2]=this.dw_metric_weight
this.Control[iCurrent+3]=this.cbx_plant
this.Control[iCurrent+4]=this.cbx_customer
this.Control[iCurrent+5]=this.dw_export_options
this.Control[iCurrent+6]=this.dw_date
this.Control[iCurrent+7]=this.dw_plant
this.Control[iCurrent+8]=this.gb_page_break
this.Control[iCurrent+9]=this.gb_sort
end on

on w_export_booking_report.destroy
call super::destroy
destroy(this.dw_daily_weekly)
destroy(this.dw_metric_weight)
destroy(this.cbx_plant)
destroy(this.cbx_customer)
destroy(this.dw_export_options)
destroy(this.dw_date)
destroy(this.dw_plant)
destroy(this.gb_page_break)
destroy(this.gb_sort)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
end event

event close;call super::close;If IsValid(iu_pas201) Then
	Destroy iu_pas201
End If

If IsValid(iu_ws_pas2) Then
	Destroy iu_ws_pas2
End If
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'sched_date'
		Message.StringParm = String(dw_date.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd')
	Case 'plant_code'
		Message.StringParm = dw_plant.uf_get_plant_code()
End Choose



end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'plant_code'
		dw_plant.uf_set_plant_code(as_value)
	Case 'inquire_date'
		dw_date.SetItem(1, 'sched_date', Date(as_value))
End Choose
end event

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_export_booking_report"
istr_error_info.se_user_id 		= sqlca.userid


iu_pas201 = Create u_pas201
iu_ws_pas2 = Create u_ws_pas2
end event

type dw_daily_weekly from u_base_dw_ext within w_export_booking_report
integer x = 146
integer y = 352
integer width = 805
integer height = 128
integer taborder = 40
string dataobject = "d_daily_weekly"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.InsertRow(0) 

end event

type dw_metric_weight from u_base_dw_ext within w_export_booking_report
integer x = 146
integer y = 192
integer width = 1097
integer height = 192
integer taborder = 30
string dataobject = "d_metric_tons"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.InsertRow(0)


end event

type cbx_plant from checkbox within w_export_booking_report
integer x = 878
integer y = 624
integer width = 343
integer height = 64
integer taborder = 70
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Plant"
end type

type cbx_customer from checkbox within w_export_booking_report
integer x = 878
integer y = 536
integer width = 366
integer height = 96
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Customer"
end type

type dw_export_options from u_base_dw_ext within w_export_booking_report
integer x = 183
integer y = 544
integer width = 549
integer height = 96
integer taborder = 50
string dataobject = "d_export_booking_options"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.InsertRow(0)
//This.SetItem(1, 'sched_date', Today())

end event

type dw_date from u_base_dw_ext within w_export_booking_report
integer x = 37
integer y = 120
integer width = 695
integer height = 80
integer taborder = 20
string dataobject = "d_sched_date"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.InsertRow(0)
This.SetItem(1, 'sched_date', Today())

end event

type dw_plant from u_plant within w_export_booking_report
integer x = 101
integer y = 32
integer width = 1472
integer taborder = 10
end type

event constructor;call super::constructor;ib_updateable = False
if this.rowcount() = 0 then this.insertrow(0)
end event

type gb_page_break from groupbox within w_export_booking_report
integer x = 146
integer y = 476
integer width = 599
integer height = 232
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Page Break Option"
end type

type gb_sort from groupbox within w_export_booking_report
integer x = 800
integer y = 476
integer width = 512
integer height = 232
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Sort Options"
end type

