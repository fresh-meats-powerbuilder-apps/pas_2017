﻿$PBExportHeader$w_shift_duration.srw
forward
global type w_shift_duration from w_base_sheet_ext
end type
type dw_scroll_product from u_base_dw_ext within w_shift_duration
end type
type dw_detail from u_base_dw_ext within w_shift_duration
end type
type dw_plant from u_plant within w_shift_duration
end type
type dw_effective_date from u_effective_date within w_shift_duration
end type
end forward

global type w_shift_duration from w_base_sheet_ext
integer width = 3223
string title = "Shift Duration List"
long backcolor = 67108864
dw_scroll_product dw_scroll_product
dw_detail dw_detail
dw_plant dw_plant
dw_effective_date dw_effective_date
end type
global w_shift_duration w_shift_duration

type variables
DataStore	ids_print

u_pas201		iu_pas201

u_ws_pas3		iu_ws_pas3

s_error		istr_error_info
end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();Long	ll_rec_count

String	ls_input_string, &
			ls_output_string, &
			ls_plant_code, &
			ls_plant_desc, &
			ls_effective_date

Date	ld_temp1, &
		ld_temp2, &
		ldt_effective_date
		

IF Not Super::wf_retrieve() Then Return False

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

dw_detail.Reset()
dw_scroll_product.Reset()
dw_scroll_product.InsertRow(0)

ls_plant_code = dw_plant.uf_get_plant_code()
ls_plant_desc = dw_plant.uf_get_plant_descr()
ldt_effective_date = dw_effective_date.uf_get_effective_date()

ids_print.object.plant_code_t.Text = ls_plant_code
ids_print.object.plant_code_t.Text = ls_plant_desc
ids_print.object.effective_date_t.Text = String(ldt_effective_date, 'mm/dd/yyyy')

ls_input_string = ls_plant_code + '~t' + &
				String(ldt_effective_date, 'yyyy-mm-dd') + '~r~n'
				

istr_error_info.se_event_name = "wf_retrieve"

//If iu_pas201.nf_pasp72br_inq_shift_duration(istr_error_info, &
//													ls_input_string, &
//													ls_output_string) <> 0 Then 
If iu_ws_pas3.uf_pasp72fr(istr_error_info, &
									ls_input_string, &
									ls_output_string) <> 0 Then 													
	SetRedraw(True)
	Return False
End If

ll_rec_count = dw_detail.ImportString(ls_output_string)

if ll_rec_count > 0 then
	dw_detail.SetRedraw(false)
	dw_detail.SetSort("fab_product_code A, product_state A")
	dw_detail.Sort()
	dw_detail.SetRedraw(true)
end if

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

return True
end function

on w_shift_duration.create
int iCurrent
call super::create
this.dw_scroll_product=create dw_scroll_product
this.dw_detail=create dw_detail
this.dw_plant=create dw_plant
this.dw_effective_date=create dw_effective_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_scroll_product
this.Control[iCurrent+2]=this.dw_detail
this.Control[iCurrent+3]=this.dw_plant
this.Control[iCurrent+4]=this.dw_effective_date
end on

on w_shift_duration.destroy
call super::destroy
destroy(this.dw_scroll_product)
destroy(this.dw_detail)
destroy(this.dw_plant)
destroy(this.dw_effective_date)
end on

event ue_postopen;call super::ue_postopen;String				ls_group_id, &
						ls_add_auth

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "ShftDur"
istr_error_info.se_user_id 		= sqlca.userid

is_inquire_window_name = 'w_plant_eff_date_inq'

iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3

ids_print = Create u_print_datastore
ids_print.dataobject = 'd_shift_duration_prt'

dw_detail.ShareData(ids_print)

wf_retrieve()
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_save')

end event

event close;call super::close;Destroy iu_pas201
Destroy iu_ws_pas3

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

end event

event ue_get_data;call super::ue_get_data;Date			ldt_effective_date

Choose Case as_value
	Case 'plant'
		Message.StringParm = dw_plant.uf_get_plant_code()
	Case 'effective_date'
		ldt_effective_date = dw_effective_date.uf_get_effective_date()
		Message.StringParm = String(ldt_effective_date, 'yyyy-mm-dd')
End choose

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date(Date(as_value))
End choose

end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return

ids_print.Print()
end event

event resize;call super::resize;
integer li_y		= 125


if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If




if width > (50 + dw_detail.x) then
	dw_detail.width	= width - (50 + dw_detail.x)
end if

//if height > (125 + dw_detail.y) then
//	dw_detail.height	= height - (125 + dw_detail.y)
//end if


if height > (li_y + dw_detail.y) then
	dw_detail.height	= height - (li_y + dw_detail.y)
end if





end event

type dw_scroll_product from u_base_dw_ext within w_shift_duration
integer y = 196
integer width = 1125
integer height = 84
integer taborder = 12
string dataobject = "d_scroll_product"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
ib_updateable = False
end event

event editchanged;call super::editchanged;Long	ll_row, &
		ll_first_row, &
		ll_last_row

ll_row = dw_detail.Find("fab_product_code >= '" + data + "'",1,dw_detail.RowCount()+1)

If ll_row > 0 Then 
	dw_detail.ScrollToRow(ll_row)
	dw_detail.SetRow(ll_row + 1)
End If

ll_first_row = Long(dw_detail.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_detail.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_detail.SetRedraw(False)
	dw_detail.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_detail.ScrollToRow(ll_row)
	dw_detail.SetRow(ll_row + 1)
	dw_detail.SetRedraw(True)
End If
end event

type dw_detail from u_base_dw_ext within w_shift_duration
integer x = 37
integer y = 288
integer width = 2779
integer height = 1088
integer taborder = 20
string dataobject = "d_shift_duration"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_plant from u_plant within w_shift_duration
integer x = 219
integer y = 16
integer width = 1472
integer height = 80
integer taborder = 10
end type

event constructor;call super::constructor;This.Disable()
end event

type dw_effective_date from u_effective_date within w_shift_duration
integer y = 104
integer width = 695
integer height = 80
integer taborder = 2
end type

event constructor;call super::constructor;This.InsertRow(0)
This.uf_Enable(False)

This.uf_set_effective_date(Today())
end event

