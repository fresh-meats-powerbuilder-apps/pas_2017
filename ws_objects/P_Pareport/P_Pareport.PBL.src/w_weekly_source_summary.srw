﻿$PBExportHeader$w_weekly_source_summary.srw
forward
global type w_weekly_source_summary from w_base_sheet_ext
end type
type dw_fab_product_code from u_fab_product_code within w_weekly_source_summary
end type
type dw_detail from u_base_dw_ext within w_weekly_source_summary
end type
type dw_plant_type from u_plant_type within w_weekly_source_summary
end type
type dw_week_end_date from u_week_end_date within w_weekly_source_summary
end type
type dw_header from u_base_dw_ext within w_weekly_source_summary
end type
end forward

global type w_weekly_source_summary from w_base_sheet_ext
integer width = 2574
integer height = 1792
string title = "Weekly Source Summary"
long backcolor = 67108864
dw_fab_product_code dw_fab_product_code
dw_detail dw_detail
dw_plant_type dw_plant_type
dw_week_end_date dw_week_end_date
dw_header dw_header
end type
global w_weekly_source_summary w_weekly_source_summary

type variables
u_pas201		iu_pas201
u_ws_pas3		iu_ws_pas3
DataStore	ids_print
s_error		istr_error_info
end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();Integer	li_count

Long	ll_rec_count

String	ls_header, &
			ls_detail, &
			ls_week_end_date, &
			ls_plant_type, &
			ls_product_code, &
			ls_product_desc, &
			ls_product_state, &
			ls_product_state_desc, &
			ls_actual_projected_ind, &
			ls_product_status, &
			ls_product_status_desc

Date	ld_temp1, &
		ld_temp2
		

IF Not Super::wf_retrieve() Then Return False

SetPointer(HourGlass!)

This.SetRedraw(False)

dw_detail.Reset()

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

ls_plant_type = dw_plant_type.uf_get_plant_type()

ls_week_end_date = String(dw_week_end_date.uf_get_week_end_date(), 'yyyy-mm-dd')
ls_product_code = dw_fab_product_code.uf_get_product_code()
ls_product_desc = dw_fab_product_code.uf_get_product_desc()
ls_product_state = dw_fab_product_code.uf_get_product_state( )
ls_product_state_desc = dw_fab_product_code.uf_get_product_state_desc( )
//12-12 jac
ls_product_status = dw_fab_product_code.uf_get_product_status( )
ls_product_status_desc = dw_fab_product_code.uf_get_product_status_desc( )
//
ls_actual_projected_ind = dw_header.GetItemString(1, 'actual_projected_ind')

dw_detail.object.week_end_date_1.expression =  ls_week_end_date

ids_print.object.week_end_date_1.expression =  ls_week_end_date
ids_print.object.plant_type_t.Text = dw_plant_type.uf_get_desc()
ids_print.object.product_code_t.Text = ls_product_code
ids_print.object.product_desc_t.Text = ls_product_desc
ids_print.object.product_state_t.Text = ls_product_state
ids_print.object.product_state_desc_t.Text = ls_product_state_desc

//ids_print.object.actual_projected_ind_t.Text = ls_actual_projected_ind
//12-12 jac added product status
ls_header = ls_plant_type + '~t' + &
				ls_product_code + '~t' + &
				ls_product_state + '~t' + &
            ls_product_status + '~t' + & 
				ls_week_end_date + '~t' + &
				ls_actual_projected_ind + '~r~n'
				

istr_error_info.se_event_name = "wf_retrieve"

//If iu_pas201.nf_pasp71br_inq_weekly_sources(istr_error_info, &
//													ls_header, &
//													ls_detail) <> 0 Then 
If iu_ws_pas3.uf_pasp71fr(istr_error_info, &
									ls_header, &
									ls_detail) <> 0 Then 
	SetRedraw(True)
	Return False
End If

//ls_detail = '005	Dakota City	A	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'005	Dakota City	B	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'003	Dakota City	A	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'003	Dakota City	B	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'004	Dakota City	A	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'004	Dakota City	B	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'018	Dakota City	A	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'018	Dakota City	B	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'026	Dakota City	A	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'026	Dakota City	B	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'031	Dakota City	A	1000	2000	1000	20000	3000	1000	0~r~n' + &
//				'031	Dakota City	B	1000	2000	1000	20000	3000	1000	0~r~n'
				

ll_rec_count = dw_detail.ImportString(ls_detail)

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

dw_detail.Sort()
dw_detail.GroupCalc()

This.SetRedraw(True)

return True
end function

on w_weekly_source_summary.create
int iCurrent
call super::create
this.dw_fab_product_code=create dw_fab_product_code
this.dw_detail=create dw_detail
this.dw_plant_type=create dw_plant_type
this.dw_week_end_date=create dw_week_end_date
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fab_product_code
this.Control[iCurrent+2]=this.dw_detail
this.Control[iCurrent+3]=this.dw_plant_type
this.Control[iCurrent+4]=this.dw_week_end_date
this.Control[iCurrent+5]=this.dw_header
end on

on w_weekly_source_summary.destroy
call super::destroy
destroy(this.dw_fab_product_code)
destroy(this.dw_detail)
destroy(this.dw_plant_type)
destroy(this.dw_week_end_date)
destroy(this.dw_header)
end on

event ue_postopen;call super::ue_postopen;String				ls_group_id, &
						ls_add_auth

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "WklySrcs"
istr_error_info.se_user_id 		= sqlca.userid

is_inquire_window_name = 'w_weekly_source_summary_inq'

iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3

ids_print = Create u_print_datastore
ids_print.dataobject = 'd_source_rpt_prt'

dw_detail.ShareData(ids_print)

wf_retrieve()
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_save')

end event

event close;call super::close;Destroy iu_pas201
Destroy iu_ws_pas3

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

end event

event ue_get_data;call super::ue_get_data;String	ls_fab_product

Choose Case as_value
	Case 'plant_group'
		Message.StringParm = dw_plant_type.uf_get_plant_type()
	Case 'fab_product'
		ls_fab_product = dw_fab_product_code.uf_exportstring( )
		Message.StringParm = ls_fab_product
	Case 'week_end_date'
		Message.StringParm = String(dw_week_end_date.uf_get_week_end_date() , 'yyyy-mm-dd')
	Case 'actual_projected_ind'
		Message.StringParm = dw_header.GetItemString(1, 'actual_projected_ind')
End choose

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant_type'
		dw_plant_type.uf_set_plant_type(as_value)
	Case 'fab_product'
		dw_fab_product_code.uf_importstring(as_value, true)
	Case 'week_end_date'
		dw_week_end_date.uf_set_week_end_date(Date(as_value))
	Case 'actual_projected_ind'
		dw_header.SetItem(1, 'actual_projected_ind',as_value)

End choose

end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return

ids_print.Print()
end event

event resize;call super::resize;//integer li_x		= 14
//integer li_y		= 285
//
//dw_detail.width	= width - (50 + li_x)
//dw_detail.height	= height - (125 + li_y)

integer li_x		= 64
integer li_y		= 410

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


dw_detail.width	= width - (li_x)
dw_detail.height	= height - (li_y)


end event

type dw_fab_product_code from u_fab_product_code within w_weekly_source_summary
integer x = 192
integer y = 108
integer taborder = 20
end type

event constructor;call super::constructor;This.uf_enable(False)

end event

type dw_detail from u_base_dw_ext within w_weekly_source_summary
integer x = 5
integer y = 548
integer width = 2510
integer height = 1404
integer taborder = 40
string dataobject = "d_source_rpt"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_plant_type from u_plant_type within w_weekly_source_summary
integer x = 165
integer y = 20
integer taborder = 30
end type

event constructor;call super::constructor;This.InsertRow(0)
This.uf_Enable(False)
end event

type dw_week_end_date from u_week_end_date within w_weekly_source_summary
integer x = 27
integer y = 412
integer taborder = 20
end type

event constructor;call super::constructor;This.InsertRow(0)
This.uf_Enable(False)


This.SetItem(1, 'week_end_date', RelativeDate(Today(), 1 - DayNumber(Today())))


end event

type dw_header from u_base_dw_ext within w_weekly_source_summary
integer x = 1925
integer y = 12
integer width = 402
integer height = 84
integer taborder = 2
string dataobject = "d_source_rpt_header"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
ib_updateable = False
end event

