﻿$PBExportHeader$w_percentage_sold.srw
$PBExportComments$Window to host Pecentage Sold Report .net page
forward
global type w_percentage_sold from w_base_web_sheet_ext
end type
end forward

global type w_percentage_sold from w_base_web_sheet_ext
string tag = "w_percentage_sold"
integer width = 2894
integer height = 1548
string title = "Percentage Sold Report"
long backcolor = 67108864
boolean ib_print_ok = true
end type
global w_percentage_sold w_percentage_sold

type variables
end variables

forward prototypes
end prototypes

on w_percentage_sold.create
call super::create
end on

on w_percentage_sold.destroy
call super::destroy
end on

event ue_revisions;call super::ue_revisions;// author: David Deal
// New window 10/10/02

end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
end event

event open;call super::open;IF Not FileExists("C:\Program Files\Adobe") THEN 
	MessageBox("Dependent Program Missing","Please contact the Help Desk!" + "~r~n" + "Ask them to have on-site install Acrobat Reader on your system!")
	close(this)
	return
elseif isnull(is_url) or is_url = 'about:blank' Then
	messagebox("Percentage Sold Report", "The url was not supplied for this window in the Ibp002.ini file!" )
	close(this)
	return
End IF
end event

event ue_postopen;call super::ue_postopen;this.title = "Percentage Sold Report Inquire"
end event

type ole_web from w_base_web_sheet_ext`ole_web within w_percentage_sold
end type

event ole_web::titlechange;call super::titlechange;if upper(text) = upper("Percentage Sold Report Inquire") Then
	parent.title = text
else
	parent.title = "Percentage Sold Report"
end if
end event

type stlabel from w_base_web_sheet_ext`stlabel within w_percentage_sold
end type

