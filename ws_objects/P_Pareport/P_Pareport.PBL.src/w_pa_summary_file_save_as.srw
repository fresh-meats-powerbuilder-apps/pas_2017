﻿$PBExportHeader$w_pa_summary_file_save_as.srw
forward
global type w_pa_summary_file_save_as from w_base_response_ext
end type
type st_1 from statictext within w_pa_summary_file_save_as
end type
type dw_filenumber from u_base_dw_ext within w_pa_summary_file_save_as
end type
end forward

global type w_pa_summary_file_save_as from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1961
integer height = 412
string title = "Save As"
long backcolor = 12632256
st_1 st_1
dw_filenumber dw_filenumber
end type
global w_pa_summary_file_save_as w_pa_summary_file_save_as

type variables
s_error		istr_error_info

u_orp204		iu_orp204

s_error			istr_error

w_base_sheet	iw_parent
end variables

on w_pa_summary_file_save_as.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_filenumber=create dw_filenumber
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_filenumber
end on

on w_pa_summary_file_save_as.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_filenumber)
end on

event open;call super::open;iw_parent = Message.PowerObjectParm
end event

event ue_postopen;call super::ue_postopen;iw_parent.event ue_get_data('file_no')
dw_filenumber.SetItem(1, 'seq_num', Message.stringparm)
iw_parent.event ue_get_data('file_desc')
if Message.stringparm = 'Current' then
	dw_filenumber.SetItem(1, 'description', '       ')
else
	dw_filenumber.SetItem(1, 'description', Message.stringparm)
end if
dw_filenumber.SetColumn('seq_num')

This.Title = 'Save As'
end event

event close;call super::close;Destroy(iu_orp204)

end event

event ue_base_cancel;call super::ue_base_cancel;iw_parentwindow.Event ue_set_data('save', 'N')
Close(This)
end event

event ue_base_ok;call super::ue_base_ok;iw_parentwindow.Event ue_set_data('save', 'Y')
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pa_summary_file_save_as
integer x = 2085
integer y = 1152
integer taborder = 0
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pa_summary_file_save_as
integer x = 1650
integer y = 184
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pa_summary_file_save_as
integer x = 1650
integer y = 60
integer taborder = 20
end type

event cb_base_ok::clicked;call super::clicked;DataWindowChild	ldwc_temp

string 				ls_info_out, &
						ls_temp, &
						ls_seq_num, &
						ls_desc

Integer	li_message

Long 	ll_rowcount

u_string_functions	lu_string

dw_filenumber.AcceptText()
ls_seq_num = dw_filenumber.GetItemString(1, 'seq_num')

dw_filenumber.GetChild('seq_num', ldwc_temp) 
ll_rowcount = ldwc_temp.RowCount()

ls_temp = "file_number = '" + ls_seq_num + "'"
IF ldwc_temp.Find(ls_temp, 1, ldwc_temp.rowcount()) > 0 THEN
	li_message = MessageBox('File Override', 'Do you want to override this file', question!, yesno!)
	IF li_message = 2 THEN 
		dw_filenumber.SetFocus()
		Return
	END IF
END IF

ls_desc = dw_filenumber.GetItemString(1, 'description')
IF lu_string.nf_IsEmpty(ls_desc) Then
	dw_filenumber.SetItem(1,'description', "")
END IF
ls_info_out = ls_seq_num + '~t' + dw_filenumber.GetItemString(1, 'description')
CloseWithReturn(Parent, ls_info_out)
end event

type st_1 from statictext within w_pa_summary_file_save_as
boolean visible = false
integer x = 1097
integer y = 32
integer width = 453
integer height = 84
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "@Arial Unicode MS"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = " ** Unused File"
boolean focusrectangle = false
end type

type dw_filenumber from u_base_dw_ext within w_pa_summary_file_save_as
integer x = 37
integer y = 128
integer width = 1609
integer height = 128
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_pa_summary_saveas"
boolean border = false
boolean ib_updateable = true
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event ue_postconstructor;call super::ue_postconstructor;Char	lc_ProcessOption, &
		lc_AvailableDate[10]			

DataWindowChild	ldwc_file_number, &
						ldwc_file_descr

Integer	li_Rtn

String	ls_description_out, &
			ls_plantdata, &
			ls_detaildata, &
			ls_descr_out, &
			ls_page_descr
			
s_Error	lstr_Error

lstr_Error.se_app_name  = "pas"
lstr_Error.se_window_name = "w_PA-Summary"
lstr_Error.se_function_name = "wf_Retriev"
lstr_Error.se_user_id = sqlca.userid		

lc_ProcessOption = "Q"
lc_AvailableDate = ""
ls_page_descr = ""
ls_PlantData = ""
ls_DetailData = ""
ls_descr_out = ""

IF NOt IsValid(iu_orp204) Then iu_orp204 = Create u_orp204
////call rpc 
li_Rtn = iu_orp204.nf_orpo81br_pa_summary(istr_Error, lc_ProcessOption, &
										lc_AvailableDate, ls_page_descr, &
										ls_PlantData, ls_DetailData, ls_descr_out)			

This.GetChild('seq_num', ldwc_file_number)
IF NOT ISVAlid(ldwc_file_number)Then
	MessageBox("","seq_number not valid")
	return
END IF
This.SetFocus()

This.GetChild('description', ldwc_file_descr)
IF NOT ISVAlid(ldwc_file_descr)Then
	MessageBox("","description not valid")
	return
END IF

ldwc_file_number.ImportString(ls_descr_out)
ldwc_file_number.ShareData(ldwc_file_descr)
end event

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_Temp

Long	ll_Row

This.GetChild("seq_num", ldwc_Temp)
ll_Row = ldwc_Temp.Find("file_number='" +Data+ "'", 1, ldwc_temp.RowCount())
if ll_Row > 0 Then
	This.SetItem(1, "description", ldwc_temp.GetItemString(ll_row, "file_description"))
End if
end event

