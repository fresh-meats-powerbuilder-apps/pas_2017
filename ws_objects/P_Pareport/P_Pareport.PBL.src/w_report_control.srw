﻿$PBExportHeader$w_report_control.srw
$PBExportComments$Defauld Sched Parameters
forward
global type w_report_control from w_base_sheet_ext
end type
type cb_find_next_product from commandbutton within w_report_control
end type
type dw_find_next_product from u_base_dw_ext within w_report_control
end type
type cb_resequence from commandbutton within w_report_control
end type
type dw_header from u_base_dw_ext within w_report_control
end type
type dw_detail from u_base_dw_ext within w_report_control
end type
end forward

global type w_report_control from w_base_sheet_ext
integer width = 2619
integer height = 2172
string title = "Report Control"
boolean maxbox = false
long backcolor = 67108864
event ue_resequence pbm_custom70
event ue_find_next_product pbm_custom70
cb_find_next_product cb_find_next_product
dw_find_next_product dw_find_next_product
cb_resequence cb_resequence
dw_header dw_header
dw_detail dw_detail
end type
global w_report_control w_report_control

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product, & 
				ib_delete_performed
				
integer		ii_row_count
string		is_data

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

//u_pas201		iu_pas201

u_ws_pas2   iu_ws_pas2

String		is_colname, &
				is_input, &
				is_ChangedColumnName, &
				is_debug
				
w_base_sheet	iw_order_detail
end variables

forward prototypes
public subroutine wf_delete ()
public function string wf_getheaderinfo ()
public function boolean wf_addrow ()
public function boolean wf_validate (long al_row)
public function boolean wf_resequence ()
public function boolean wf_deleterow ()
public function boolean wf_retrieve ()
public function boolean wf_find_next_product ()
public function boolean wf_update ()
end prototypes

event ue_resequence;
wf_resequence()
end event

event ue_find_next_product;
wf_find_next_product()
end event

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function string wf_getheaderinfo ();String		ls_string


ls_string = dw_header.GetItemString(1, 'parptctl') + '~t' + &
				dw_header.GetItemString(1, 'division_code') + '~t' 

return ls_string


end function

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row, &
				ll_seq
String 		ls_description
 
 
 
If dw_detail.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_current_row = dw_detail.GetRow()

If ll_current_row > 0 Then
	ll_row = dw_detail.InsertRow(ll_current_row + 1)
	ll_seq = dw_detail.GetItemNumber(ll_current_row, "sequence")
	ls_description = dw_detail.GetItemString(ll_current_row, "description")
	dw_detail.setItem(ll_row, "sequence", ll_seq)
	dw_detail.setItem(ll_row, "description", ls_description)
Else
	ll_row = dw_detail.InsertRow(0)
End If
//
dw_detail.ScrollToRow(ll_row)
dw_detail.SetItem(ll_row, "update_flag", 'A')
dw_detail.SetColumn("sequence")
dw_detail.SetFocus()
This.SetRedraw(True)

return true



end function

public function boolean wf_validate (long al_row);Date					ldt_temp
						
Long					ll_rtn, &
						ll_nbrrows, &
						ll_ModifiedRow, &
						ll_prev_sequence, & 
						ll_sequence
						

String				ls_prev_description, &
						ls_fab_product, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag, &
						ls_prev_prod_code, &						
						ls_sequence, &
						ls_prod_code, &
						ls_description
						
IF dw_detail.AcceptText() = -1 THEN Return( False )

ll_nbrrows = dw_detail.RowCount()

ls_update_flag = dw_detail.GetItemString(al_row, "update_flag")
If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

ll_sequence = dw_detail.GetItemNumber(al_row, "sequence")
if ll_sequence < 0 then
	iw_Frame.SetMicroHelp("Please enter a Sequence greater than 0")
		This.SetRedraw(False)
		dw_detail.ScrollToRow(al_row)
		dw_detail.SetFocus()
		This.SetRedraw(True)
		Return False
End if

if isNull(ll_sequence) then
	iw_Frame.SetMicroHelp("Please enter a Sequence greater than 0")
		This.SetRedraw(False)
		dw_detail.ScrollToRow(al_row)
		dw_detail.SetFocus()
		This.SetRedraw(True)
		Return False
End if


ls_description = dw_detail.getitemstring(al_row, "description")
if isNull(ls_description) then
		iw_Frame.SetMicroHelp("Please enter a Description")
		This.SetRedraw(False)
		dw_detail.ScrollToRow(al_row)
		dw_detail.SetFocus()
		This.SetRedraw(True)
		Return False
end if

ls_prev_prod_code = dw_detail.getitemstring(al_row, "prev_prod_code")
if isNull(ls_prev_prod_code) then
	dw_detail.setitem(al_row, "prev_prod_code", ' ')		
end if

ls_prev_description = dw_detail.getitemstring(al_row, "prev_description")
if isNull(ls_prev_description) then
	dw_detail.setitem(al_row, "prev_description", ' ')		
end if

ll_prev_sequence = dw_detail.GetItemNumber(al_row, "prev_sequence")
if isNull(ll_prev_sequence) then
	dw_detail.setitem(al_row, "prev_sequence", 00000)
end if

ls_sequence = String(dw_detail.GetItemNumber(al_row, "sequence"))
ls_prod_code = dw_detail.getitemstring(al_row, "prod_code")

ls_SearchString	= "sequence = " + ls_sequence + &
						" and prod_code = '" + ls_prod_code + "'" 
// Find a matching row excluding the current row.
If al_row > 1 then
	CHOOSE CASE al_row 
		CASE 1
			ll_rtn = dw_detail.Find  &
					( ls_SearchString, al_row + 1, ll_nbrrows)
		CASE 2 to (ll_nbrrows - 1)
			ll_rtn = dw_detail.Find ( ls_SearchString, al_row - 1, 1)
			If ll_rtn = 0 Then ll_rtn = dw_detail.Find  &
				(ls_SearchString, al_row + 1, ll_nbrrows)
		CASE ll_nbrrows 
			ll_rtn = dw_detail.Find ( ls_SearchString, al_row - 1, 1)
	END CHOOSE
//
	If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "There are duplicate product codes with the" + &
  					" same sequence number.")
		dw_detail.SetRedraw(False)
		dw_detail.ScrollToRow(al_row)
		dw_detail.SetColumn("sequence")
		dw_detail.SetRow(al_row)
		dw_detail.SelectRow(ll_rtn, True)
		dw_detail.SelectRow(al_row, True)
		dw_detail.SetRedraw(True)
		Return False
	End if
end if

Return True
end function

public function boolean wf_resequence ();
Long			ll_row, &				
				ll_row_cnt, &
				ll_hold_seq, &
				ll_skip_seq, &
				ll_new_seq, &
				ll_seq, &
				ll_seq_count
				


//This logic will recalculate the sequence number

ll_row_cnt = dw_detail.RowCount()

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Resequencing Rows")

ll_row = 1
ll_seq_count = 0
dw_detail.SetRedraw(False)
do
	ll_seq = dw_detail.GetItemNumber(ll_row, "sequence")
	if ll_hold_seq = ll_seq then
	else 
		ll_seq_count = ll_seq_count + 1
		ll_hold_seq = ll_seq
	end if
	ll_row = ll_row + 1
Loop while ll_row <=  ll_row_cnt

ll_seq_count = ll_seq_count + 2
ll_skip_seq = 950000 / ll_seq_count
ll_new_seq = 0

ll_row = 1
dw_detail.SetRedraw(False)
do
	ll_seq = dw_detail.GetItemNumber(ll_row, "sequence")
	if ll_hold_seq = ll_seq then
	else
		ll_new_seq = ll_new_seq + ll_skip_seq
		ll_hold_seq = ll_seq
	end if
	dw_detail.SetItem(ll_row, "prev_sequence", ll_seq)
	dw_detail.SetItem(ll_row, "sequence", ll_new_seq)
	dw_detail.SetItem(ll_row, "update_flag", 'U')
	ll_row = ll_row + 1
Loop while ll_row <=  ll_row_cnt


dw_detail.SetRedraw(True)
dw_detail.accepttext()
iw_frame.SetMicroHelp("Ready")
Return True

end function

public function boolean wf_deleterow ();Boolean		lb_ret

Long			ll_row

String		ls_ind, &
				ls_filter, &
				ls_name_code

This.SetRedraw( False )
ll_row = dw_detail.GetRow()

If ll_row < 1 Then
	Return True
End If

dw_detail.SetItem(ll_row, "update_flag", 'D')
ib_delete_performed = True


dw_detail.SetFocus()
lb_ret = super::wf_deleterow()

dw_detail.SelectRow(0,False)

This.SetRedraw( True )
return lb_ret
end function

public function boolean wf_retrieve ();Boolean	lb_ret
Integer	li_ret, &
			li_row_Count,li_loop
String	ls_input, &
			ls_output_values, ls_temp
		
			
Long		ll_value, &
			ll_rtn, &
			ll_row_count, ll_row, &
			ll_weight, ll_boxes
			
Date		ll_prod_date

u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_report_control_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")
is_debug = This.dw_header.GetItemString(1, 'parptctl')



ls_input = This.dw_header.GetItemString(1, "parptctl") + '~t' + &
				dw_header.GetItemString(1, 'division_code') + '~r~n'
							
							
is_input = ls_input

//li_ret = iu_pas201.nf_pasp70cr_rpt_ctrl_file_inq(istr_error_info, &
//									is_input, &
//									ls_output_values) 

li_ret = iu_ws_pas2.nf_pasp70gr(is_input, &
									ls_output_values, istr_error_info) 
						

This.dw_detail.Reset()
//
If li_ret = 0 Then
This.dw_detail.ImportString(ls_output_values)

	ll_value = dw_detail.RowCount()
	If ll_value < 0 Then ll_value = 0

	dw_detail.ResetUpdate()

	IF ll_value > 0 THEN
 		dw_detail.SetFocus()
		dw_detail.ScrollToRow(1)
		dw_detail.SetColumn( "sequence" )
		dw_detail.TriggerEvent("RowFocusChanged")
	END IF

	SetMicroHelp(String(ll_value) + " rows retrieved")
	This.SetRedraw( True )
	li_Row_count = dw_detail.RowCount()

end if	
dw_detail.ResetUpdate()

ib_delete_performed = False

Return True

end function

public function boolean wf_find_next_product ();Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
string	ls_prod_code, & 
			ls_data, &
			ls_update_flag, &
			ls_reset
integer	li_counter
Boolean	lb_yes_found


ll_row_count = dw_detail.RowCount()
li_counter = 1
Do While li_counter <= ll_Row_count 
	dw_detail.SetItem(li_counter, "hilite_prod", 'N')
	li_counter = li_counter + 1
loop

li_counter = ii_row_count + 1

if ll_row_count < li_counter then Return True
ls_data = is_data

lb_yes_found = False

Do While li_counter <= ll_Row_count 
		ls_prod_code = dw_detail.GetItemString(li_counter, "prod_code")
		If pos(ls_prod_code, ls_data) > 0 and pos(ls_prod_code, ls_data) < 2 then
			lb_yes_found=True
		else	
			lb_yes_found=False
		end if
		if lb_yes_found = True then
			ii_row_count = li_counter
			dw_detail.SetRedraw(False)
			dw_detail.SetItem(li_counter, "hilite_prod", 'Y')
 			dw_detail.ScrollToRow(li_counter)
			dw_detail.SetRow(li_counter + 1)
			dw_detail.SetRedraw(True)
			li_counter = ll_row_count + 1
		else	
			dw_detail.SetItem(li_counter, "hilite_prod", 'N')
			li_counter = li_counter + 1	
		end if
Loop

if lb_yes_found=False then
	iw_Frame.SetMicroHelp("No matching products found")
else
	iw_Frame.SetMicroHelp("Product found")
end if

li_counter = 1
ls_reset = 'YES'
Do While li_counter <= ll_Row_count 
	ls_update_flag = dw_detail.GetItemString(li_counter, "update_flag")
	if ls_update_flag > ' ' then
		ls_reset = 'NO' 
		li_counter = ll_row_count + 1
	end if
	li_counter = li_counter + 1
loop

If (ib_delete_performed = True) then
	ls_reset = 'NO' 
End If
	 
if ls_reset = 'YES' then
	dw_detail.ResetUpdate()
end if 
Return True

end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter
					
long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount, &
					ll_ModifiedRow, &
					ll_100th_row, &
					ll_csr_row
					

string			ls_ColName, &
					ls_TextName, &
					ls_UpdateRMDS, &
					ls_header_string, &
					ls_RPC_detail

u_string_functions		lu_string_functions					
dwItemStatus	lis_status

ll_csr_row	= dw_detail.GetRow()
IF dw_detail.AcceptText() = -1 THEN Return( False )
IF dw_detail.ModifiedCount() + dw_detail.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

//IF Not IsValid( iu_pas201 ) THEN
//	iu_pas201	=  CREATE u_pas201
//END IF

IF Not IsValid(iu_ws_pas2) THEN
	iu_ws_pas2	=  CREATE u_ws_pas2
END IF

ll_NbrRows = dw_detail.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_header_string = is_input
ls_UpdateRMDS = iw_frame.iu_string.nf_BuildUpdateString(dw_detail)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp71cr_rpt_ctrl_file_maint"
istr_error_info.se_message = Space(71)

DO
	 ll_100th_row = lu_string_functions.nf_nPos( ls_UpdateRMDS, "~r~n",1,100)
	 IF ll_100th_Row > 0 Then
		ls_RPC_Detail = Left( ls_UpdateRMDS, ll_100th_Row + 1)
		ls_UpdateRMDS = Mid( ls_UpdateRMDS, ll_100th_Row + 2)
	ELSE
		ls_Rpc_detail = ls_UpdateRMDS
		ls_UpdateRMDS = ''
	END IF

//If iu_pas201.nf_pasp71cr_rpt_ctrl_file_maint(istr_error_info, ls_header_string, &
//	ls_RPC_detail) = false Then
//	Return False
//end if

If iu_ws_pas2.nf_pasp71gr(ls_header_string, &
	ls_RPC_detail, istr_error_info) = false Then
	Return False
end if

Loop While Not lu_string_functions.nf_IsEmpty( ls_UpdateRMDS)

dw_detail.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")
ll_RowCount = dw_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_detail.SetItem(li_Counter, 'update_flag', ' ')
Next
dw_detail.ResetUpdate()
dw_detail.SetFocus()

dw_detail.SetReDraw(True)
dw_find_next_product.ResetUpdate()

ib_reinquire = True

wf_retrieve()

dw_detail.ScrollToRow(ll_csr_row)

Return( True )
end function

event close;call super::close;//If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_ws_pas2) Then Destroy(iu_ws_pas2)




end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

	

end event

event ue_query;call super::ue_query;
wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')
 
iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')


end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_detail.InsertRow(0)
dw_find_next_product.InsertRow(0)


end event

event ue_postopen;call super::ue_postopen;Environment		le_env

//iu_pas201 = Create u_pas201
//If Message.ReturnValue = -1 Then 
//	Close(This)
//	return
//End if

iu_ws_pas2 = Create u_ws_pas2
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Report Control File"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")

end event

on w_report_control.create
int iCurrent
call super::create
this.cb_find_next_product=create cb_find_next_product
this.dw_find_next_product=create dw_find_next_product
this.cb_resequence=create cb_resequence
this.dw_header=create dw_header
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_find_next_product
this.Control[iCurrent+2]=this.dw_find_next_product
this.Control[iCurrent+3]=this.cb_resequence
this.Control[iCurrent+4]=this.dw_header
this.Control[iCurrent+5]=this.dw_detail
end on

on w_report_control.destroy
call super::destroy
destroy(this.cb_find_next_product)
destroy(this.dw_find_next_product)
destroy(this.cb_resequence)
destroy(this.dw_header)
destroy(this.dw_detail)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'parptctl' 
		Message.StringParm = dw_header.GetItemString(1, 'parptctl')
	Case 'division_code' 
		Message.StringParm = dw_header.GetItemString(1, 'division_code')	
	Case 'division_description'
		Message.StringParm = dw_header.GetItemString(1, 'division_description')	
End Choose



end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'parptctl' 
		dw_header.SetItem(1, 'parptctl', as_value)
	Case 'division_code' 
		dw_header.SetItem(1, 'division_code', as_value)
	Case 'division_description'
		dw_header.SetItem(1, 'division_description', as_value)	
End Choose

end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_detail.x * 2) + 30 
li_y = dw_detail.y + 90


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If



if width > li_x Then
	dw_detail.width	= width - li_x
end if

if height > li_y then
	dw_detail.height	= height - li_y
end if
end event

type cb_find_next_product from commandbutton within w_report_control
integer x = 1390
integer y = 192
integer width = 434
integer height = 92
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Find Next Product"
end type

event clicked;Parent.PostEvent("ue_find_next_product")
end event

type dw_find_next_product from u_base_dw_ext within w_report_control
integer x = 1829
integer y = 200
integer width = 512
integer height = 96
integer taborder = 20
string dataobject = "d_find_next_product"
boolean border = false
end type

event editchanged;call super::editchanged;Long		ll_row, &
			ll_first_row, &
			ll_last_row, &
			ll_row_count
string	ls_prod_code, &
			ls_reset, &
			ls_update_flag
integer	li_counter
Boolean	lb_yes_found


ll_row_count = dw_detail.RowCount()

li_counter = 1

if ll_row_count < li_counter then Return 

Do While li_counter <= ll_Row_count 
	dw_detail.SetItem(li_counter, "hilite_prod", 'N')
	li_counter = li_counter + 1
loop

li_counter = 1
lb_yes_found = False

Do While li_counter <= ll_Row_count 
		ls_prod_code = dw_detail.GetItemString(li_counter, "prod_code")
		If pos(ls_prod_code, data) > 0 and pos(ls_prod_code, data) < 2 then
			lb_yes_found=True
		else	
			lb_yes_found=False
		end if
		if lb_yes_found = True then
			ii_row_count = li_counter
			is_data = data
			dw_detail.SetItem(li_counter, "hilite_prod", 'Y')
			dw_detail.SetRedraw(False)
			dw_detail.ScrollToRow(li_counter)
			dw_detail.SetRedraw(True)
			li_counter = ll_row_count + 1
		else	
			dw_detail.SetItem(li_counter, "hilite_prod", 'N')
			li_counter = li_counter + 1	
		end if
Loop

if lb_yes_found=False then
	iw_Frame.SetMicroHelp("No matching products found")
else
	iw_Frame.SetMicroHelp("Product found")
end if

this.resetUpdate()

li_counter = 1
ls_reset = 'YES'
Do While li_counter <= ll_Row_count 
	ls_update_flag = dw_detail.GetItemString(li_counter, "update_flag")
	if ls_update_flag > ' ' then
		ls_reset = 'NO' 
		li_counter = ll_row_count + 1
	end if
		li_counter = li_counter + 1
loop
	 
If (ib_delete_performed = True) then
	ls_reset = 'NO' 
End If

if ls_reset = 'YES' then
	dw_detail.ResetUpdate()
end if
//	
//	li_counter = li_counter + 1
//loop


end event

event constructor;call super::constructor;This.ib_updateable = False
end event

type cb_resequence from commandbutton within w_report_control
integer x = 1865
integer y = 32
integer width = 343
integer height = 92
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Resequence"
end type

event clicked;Parent.PostEvent("ue_resequence")
end event

type dw_header from u_base_dw_ext within w_report_control
integer x = 37
integer width = 1829
integer height = 192
integer taborder = 0
string dataobject = "d_report_control_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
DataWindowChild		ldwc_type


This.GetChild("parptctl", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("parptctl")


end event

type dw_detail from u_base_dw_ext within w_report_control
integer x = 37
integer y = 284
integer width = 2523
integer height = 1164
integer taborder = 30
string dataobject = "d_report_control_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return



end event

event constructor;call super::constructor;ib_updateable = True
//This.SetItem(1, 'from_eff_date', Today())
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state
//
//dw_detail.GetChild('parptctl', ldwc_state)
//ldwc_state.SetTransObject(SQLCA)
//ldwc_state.Retrieve("parptctl")
//
//dw_rmt_detail.GetChild('product_state', ldwc_state)
//ldwc_state.SetTransObject(SQLCA)
//ldwc_state.Retrieve("PRDSTATE")
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row, &
				ll_boxes, ll_wgt, &
				ll_prod_avg_wgt, & 
			 	ll_sales_avg_wgt, &
				ll_avg_wgt, ll_prev_avg_wgt, &
				ll_prev_weight, ll_prev_boxes, &
				ll_weight

string		ls_GetText, &
				ls_tem, ls_temp, &
				ls_prod_descr, &
				ls_description_old, &
				ls_description_new, &
				ls_description
				
Boolean		lb_changed
				
Date			ldt_temp	


Long			ll_seq_new, &
				ll_seq_old, &
				ll_seq

long			ll_report_row, &
				ll_rtn

String		ls_ColumnName, &
				ls_SearchString, &
				ls_type, &
				ls_name_code, &
				ls_update_flag


nvuo_pa_business_rules	u_rule

is_ColName = GetColumnName()
ls_GetText = data

ll_source_row	= GetRow()
il_ChangedRow = 0


ll_RowCount = This.RowCount()

If is_ColName = "sequence" Then
	ll_seq_new = Real(data)
	if ll_seq_new > 0 then
	else	
		iw_Frame.SetMicroHelp("Please enter a Sequence greater than 0")
		this.setfocus( )
		this.setcolumn("sequence") 
		This.SelectText(1, Len(data))	
		return 1		
	End if
	ls_update_flag = This.GetItemString(ll_source_row, "update_flag")
	if ls_update_flag = 'A' then
	else
		ll_report_row = 1
		ll_seq_old =  This.GetItemNumber(row, "sequence")
	   Do While ll_report_row <= ll_RowCount
			ll_seq =  This.GetItemNumber(ll_report_row, "sequence")
			if ll_seq = ll_seq_old then
				This.SetItem(ll_report_row,"sequence",ll_seq_new)
				This.SetItem(ll_report_row, "update_flag", "U")
			end if
			ll_report_row = ll_report_row + 1
		loop	
	end if
end if

If is_ColName = "description" Then
	ls_description_new = data
	ll_seq_old =  This.GetItemNumber(row, "sequence")
	ls_description_old = This.GetItemString(row, "description")
	ll_report_row = 1
	
	Do While ll_report_row <= ll_RowCount
		ll_seq =  This.GetItemNumber(ll_report_row, "sequence")
		if ll_seq = ll_seq_old then
			ls_description =  This.GetItemString(ll_report_row, "description")
			If ls_description = ls_description_old Then
				This.SetItem(ll_report_row,"description",ls_description_new)
				This.SetItem(ll_report_row, "update_flag", "U")
			End If
		end if
		ll_report_row = ll_report_row + 1
	Loop
End If


// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

//parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;return (1)
end event

