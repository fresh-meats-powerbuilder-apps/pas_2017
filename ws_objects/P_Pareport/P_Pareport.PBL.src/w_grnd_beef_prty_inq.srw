﻿$PBExportHeader$w_grnd_beef_prty_inq.srw
forward
global type w_grnd_beef_prty_inq from w_base_response_ext
end type
type dw_inquire from u_base_dw_ext within w_grnd_beef_prty_inq
end type
type dw_plant from u_plant within w_grnd_beef_prty_inq
end type
end forward

global type w_grnd_beef_prty_inq from w_base_response_ext
integer width = 1957
integer height = 548
long backcolor = 12632256
dw_inquire dw_inquire
dw_plant dw_plant
end type
global w_grnd_beef_prty_inq w_grnd_beef_prty_inq

type variables

end variables

on w_grnd_beef_prty_inq.create
int iCurrent
call super::create
this.dw_inquire=create dw_inquire
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_inquire
this.Control[iCurrent+2]=this.dw_plant
end on

on w_grnd_beef_prty_inq.destroy
call super::destroy
destroy(this.dw_inquire)
destroy(this.dw_plant)
end on

event ue_base_ok;call super::ue_base_ok;string ls_PaDateRange
nvuo_pa_business_rules luo_pa

If dw_plant.AcceptText() = -1 Then return
If dw_inquire.AcceptText() = -1 Then return

If iw_frame.iu_string.nf_IsEmpty(dw_plant.uf_get_plant_code()) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
	
elseIf iw_frame.iu_string.nf_IsEmpty(String(dw_inquire.GetItemDate(1,"inquire_date"))) Then
	iw_frame.SetMicroHelp("Departure Date is a required field")
	dw_inquire.SetFocus()
	dw_inquire.SetColumn("inquire_date")
	return
	
elseif not luo_pa.uf_check_pa_date(dw_inquire.GetItemDate(1,"inquire_date"), ls_PaDateRange)then
	iw_frame.SetMicroHelp("Departure Date cannot be later than " + ls_PaDateRange)
	dw_inquire.SetFocus()
	dw_inquire.SetColumn("inquire_date")
	return

End If

iw_parentwindow.Event ue_set_data('plant_code', dw_plant.uf_get_plant_code())
iw_parentwindow.Event ue_set_data('inquire_date', String(dw_inquire.GetItemDate(1, &
		'inquire_date'), 'mm-dd-yyyy'))

Close(This)

end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('inquire_date')
dw_inquire.SetItem(1, 'inquire_date', Date(Message.StringParm))

iw_parentwindow.Event ue_get_data('plant_code')
dw_plant.uf_set_plant_code(Message.StringParm)
end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_grnd_beef_prty_inq
integer x = 978
integer y = 288
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_grnd_beef_prty_inq
integer x = 686
integer y = 288
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_grnd_beef_prty_inq
integer x = 402
integer y = 288
integer taborder = 30
end type

type dw_inquire from u_base_dw_ext within w_grnd_beef_prty_inq
integer x = 37
integer y = 120
integer width = 791
integer height = 104
integer taborder = 20
string dataobject = "d_grnd_beef_prty_header"
boolean border = false
end type

event constructor;call super::constructor;dw_inquire.InsertRow(0)
dw_inquire.object.inquire_date.background.color = '16777215'
dw_inquire.object.inquire_date.protect = 0


end event

event itemchanged;call super::itemchanged;Choose Case String(dwo.Name)
	Case "inquire_date"
		If Date(data) < Date(String(Today(), 'yyyy-mm-dd')) Then
			iw_frame.SetMicroHelp("Scheduled Ship Date must be greater than or equal to today")
			This.SetFocus()
			This.SelectText(1, 100)
			return 1
		End If
End Choose
end event

event itemerror;call super::itemerror;Return 1
end event

type dw_plant from u_plant within w_grnd_beef_prty_inq
integer x = 352
integer y = 24
integer taborder = 10
end type

