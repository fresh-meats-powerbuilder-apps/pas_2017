﻿$PBExportHeader$w_report_control_inq.srw
forward
global type w_report_control_inq from w_base_response_ext
end type
type dw_type from u_pa_rpt_ctl within w_report_control_inq
end type
type dw_division from u_division within w_report_control_inq
end type
end forward

global type w_report_control_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 1701
integer height = 544
string title = "Report Control Inquire"
long backcolor = 67108864
event begindatechanged ( )
dw_type dw_type
dw_division dw_division
end type
global w_report_control_inq w_report_control_inq

type variables
Boolean		ib_valid_return
w_base_sheet	iw_parent
w_base_sheet lw_parent

string	is_begin_date
end variables

event ue_postopen;call super::ue_postopen;Int							li_pos, &
								li_ret

String						ls_header, &
								ls_temp, &
								ls_parptctl, &
								ls_product_descr,ls_state, &
								ls_shift, &
								ls_division_code, &
								ls_division_description

u_string_functions		lu_string


iw_parent.Event ue_Get_Data('parptctl')
ls_parptctl = Message.StringParm


If Not lu_string.nf_IsEmpty(ls_parptctl) Then
	dw_type.SetItem(1, 'parptctl', ls_parptctl)
End If


dw_division.Modify("DataWindow.Color = 78682240")

end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

lw_parent = this.parentwindow()

This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
	This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2) 


If dw_type.RowCount() = 0 Then
	dw_type.InsertRow(0)
End if

if dw_division.rowcount( ) = 0 Then
	dw_division.insertrow( 0)
End If


end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

on w_report_control_inq.create
int iCurrent
call super::create
this.dw_type=create dw_type
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_type
this.Control[iCurrent+2]=this.dw_division
end on

on w_report_control_inq.destroy
call super::destroy
destroy(this.dw_type)
destroy(this.dw_division)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date	ldt_temp

String	ls_temp,ls_temp1
Long 		ll_rtn

If dw_type.AcceptText() = -1 Then 
	dw_type.SetFocus()
	return
End if

dw_division.AcceptText()
dw_type.AcceptText()

If iw_frame.iu_string.nf_IsEmpty(dw_type.GetItemString(1, "parptctl")) Then
	iw_frame.SetMicroHelp("Type is a required field")
	dw_type.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_division.GetItemString(1, "division_code")) Then
	iw_frame.SetMicroHelp("Type is a required field")
	dw_division.SetFocus()
	return
End if

ls_temp = dw_type.GetItemString(1, 'parptctl')
iw_parent.Event ue_Set_Data('parptctl', ls_temp)

ls_temp = dw_division.GetItemString(1, 'division_code')
iw_parent.Event ue_Set_Data('division_code', ls_temp)

ls_temp = dw_division.GetItemString(1, 'division_description')
iw_parent.Event ue_Set_Data('division_description', ls_temp)

ib_valid_return = true
Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_report_control_inq
boolean visible = false
integer x = 1696
integer y = 392
integer taborder = 50
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_report_control_inq
integer x = 823
integer y = 272
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_report_control_inq
integer x = 320
integer y = 272
integer taborder = 30
end type

type dw_type from u_pa_rpt_ctl within w_report_control_inq
integer y = 32
boolean bringtotop = true
end type

type dw_division from u_division within w_report_control_inq
integer y = 128
integer taborder = 11
boolean bringtotop = true
end type

