﻿$PBExportHeader$w_transfer_report.srw
forward
global type w_transfer_report from w_base_sheet_ext
end type
type dw_report_option from u_base_dw_ext within w_transfer_report
end type
type dw_date from u_base_dw_ext within w_transfer_report
end type
type dw_plant from u_plant within w_transfer_report
end type
end forward

global type w_transfer_report from w_base_sheet_ext
integer width = 1687
integer height = 644
string title = "Transfer Report"
long backcolor = 67108864
dw_report_option dw_report_option
dw_date dw_date
dw_plant dw_plant
end type
global w_transfer_report w_transfer_report

type variables
u_pas201		iu_pas201

u_ws_pas2     iu_ws_pas2

s_error		istr_error_info


String		is_output_data
end variables

forward prototypes
public subroutine wf_set_other_products (ref datawindow adw_input)
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
end prototypes

public subroutine wf_set_other_products (ref datawindow adw_input);
end subroutine

public function boolean wf_validate (long al_row);string ls_plant

ls_plant = dw_plant.GetItemString(1, 'location_code')

IF IsNull(ls_plant) or Len(trim(ls_plant)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a plant code.  Plant cannot be left blank.")
	This.SetRedraw(False)
	dw_plant.ScrollToRow(1)
	dw_plant.SetColumn("plant_code")
	dw_plant.SetFocus()
	This.SetRedraw(True)
	Return False
End If

Return True
end function

public function boolean wf_update ();boolean lb_Return

string ls_update_string, &
			ls_tranid, &
			ls_type, &
			ls_plant_name, &
			ls_view, &
			ls_month, &
			ls_userid, &
			ls_date, &
			ls_time, &
			ls_opid, &
			ls_plant, &
			ls_incl_pa, &
			ls_rpt_opt
			
			
IF dw_date.AcceptText() = -1 THEN Return( False )	
IF dw_plant.AcceptText() = -1 THEN Return( False )	
IF dw_report_option.AcceptText() = -1 THEN Return( False )	

SetPointer(HourGlass!)

If Not wf_validate(1) Then
			dw_plant.SetReDraw(True)
	Return False
End If
////
//values to pass to common report initiator for pas216xe
ls_tranid = 'P216'
ls_plant = dw_plant.GetItemString(1, 'location_code')
ls_type = '2'

ls_plant_name = '            '
ls_view = 'V'
ls_userid = sqlca.userid
ls_date = string(dw_date.GetItemDate(1, 'sched_date'), "MMDDYY")
//ls_date_form = string(today(), "YYMMDD")
ls_time = string(Now(), "HHMMSS")
ls_opid = Right(ls_userid,3)
ls_incl_pa = dw_report_option.GetItemString(1, 'include_pa')
ls_rpt_opt = dw_report_option.GetItemString(1, 'report_options')

ls_update_string = ls_type  + &
 					ls_tranid + &
					ls_opid + &
					ls_date + &
					ls_time + &
					'             ' + &			
					ls_date + &
					dw_plant.GetItemString(1, 'location_code') + &
					ls_rpt_opt + &
					ls_incl_pa + &
					ls_view + &
					ls_userid
//


istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_pasp72gr"
istr_error_info.se_message = Space(71)

//lb_return = iu_pas201.nf_pasp72cr_init_mainframe_report(istr_error_info, ls_Update_string)

lb_return = iu_ws_pas2.nf_pasp72gr(ls_Update_string, istr_error_info)

SetPointer(Arrow!)

iw_Frame.SetMicroHelp("Report initiated.")

return lb_Return
end function

on w_transfer_report.create
int iCurrent
call super::create
this.dw_report_option=create dw_report_option
this.dw_date=create dw_date
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_option
this.Control[iCurrent+2]=this.dw_date
this.Control[iCurrent+3]=this.dw_plant
end on

on w_transfer_report.destroy
call super::destroy
destroy(this.dw_report_option)
destroy(this.dw_date)
destroy(this.dw_plant)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
end event

event close;call super::close;If IsValid(iu_pas201) Then
	Destroy iu_pas201
End If

If IsValid(iu_ws_pas2) Then
	Destroy iu_ws_pas2
End If
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'sched_date'
		Message.StringParm = String(dw_date.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd')
	Case 'plant_code'
		Message.StringParm = dw_plant.uf_get_plant_code()
End Choose



end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'plant_code'
		dw_plant.uf_set_plant_code(as_value)
	Case 'sched_date'
		dw_date.SetItem(1, 'sched_date', Date(as_value))
End Choose
end event

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_transfer_report"
istr_error_info.se_user_id 		= sqlca.userid


iu_pas201 = Create u_pas201
iu_ws_pas2 = Create u_ws_pas2

end event

type dw_report_option from u_base_dw_ext within w_transfer_report
integer x = 110
integer y = 224
integer width = 1024
integer height = 320
integer taborder = 20
string dataobject = "d_select_report_option"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.InsertRow(0)
//This.SetItem(1, 'inquire_date', Today())

end event

event itemchanged;call super::itemchanged;Choose Case dwo.name
	Case 'report_selection'
		Choose Case data
			Case 'P' 
//				dw_grnd_beef_prty.Group( 'Product_code') Level=1))
		End Choose
End Choose
end event

type dw_date from u_base_dw_ext within w_transfer_report
integer x = 37
integer y = 128
integer width = 859
integer height = 92
integer taborder = 10
string dataobject = "d_sched_date"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.InsertRow(0)
This.SetItem(1, 'sched_date', Today())
 
end event

type dw_plant from u_plant within w_transfer_report
integer x = 101
integer y = 32
integer width = 1472
integer taborder = 0
end type

event constructor;call super::constructor;if this.rowcount() = 0 then this.insertrow(0)
end event

