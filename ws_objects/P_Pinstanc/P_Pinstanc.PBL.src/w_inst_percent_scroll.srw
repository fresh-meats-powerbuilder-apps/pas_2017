﻿$PBExportHeader$w_inst_percent_scroll.srw
$PBExportComments$Popup Datawindow with 1 to 100 percent and corresponding Piece ratios
forward
global type w_inst_percent_scroll from Window
end type
type dw_1 from datawindow within w_inst_percent_scroll
end type
end forward

global type w_inst_percent_scroll from Window
int X=833
int Y=357
int Width=677
int Height=537
WindowType WindowType=child!
dw_1 dw_1
end type
global w_inst_percent_scroll w_inst_percent_scroll

forward prototypes
public function long wf_scrollnext ()
public function long wf_scrollprevious ()
end prototypes

public function long wf_scrollnext ();long		ll_row


ll_row = dw_1.GetSelectedRow(0)
// always 100 rows here
If ll_row < 100 Then
	dw_1.SelectRow(0, False)
	dw_1.SelectRow(ll_row + 1, True)
	return dw_1.ScrollNextRow()
Else
	return ll_row
End if
end function

public function long wf_scrollprevious ();long		ll_row


ll_row = dw_1.GetSelectedRow(0)
// always 100 rows here
If ll_row > 1 Then
	dw_1.SelectRow(0, False)
	dw_1.SelectRow(ll_row - 1, True)
	return dw_1.ScrollPriorRow()
Else
	return ll_row
End if
end function

event mousemove;String	ls_left, &
			ls_right

Long		ll_row


iw_frame.iu_string.nf_parseLeftRight(dw_1.GetObjectAtPointer(), '~t', ls_left, ls_Right)
If Long(ls_Right) <= 0 Then return

ll_row = dw_1.GetSelectedRow(0)
If Long(ls_Right) <> ll_row Then
	dw_1.SelectRow(0, False)
	dw_1.SelectRow(Long(ls_Right), True)
End if

end event

event open;Decimal		ldc_number

Int			li_counter

Long			ll_row

String		ls_text, &
				ls_Left, &
				ls_Right, &
				ls_string


ls_text = Message.StringParm

This.Move(PointerX(), PointerY())

// Left String is current Percent, Right String is Piece Count
iw_frame.iu_string.nf_ParseLeftRight(ls_text, '~t', ls_Left, ls_Right)
ldc_number = Dec(ls_Right) / 100

ls_string = ''

For li_counter = 1 to 100
	ls_string += String(li_counter) + '~t' + String((Dec(ls_Right) / 100) * li_counter, "0") + '~r~n'
Next

dw_1.ImportString(ls_string)

ll_row = dw_1.Find('percent = ' + ls_left, 1, dw_1.RowCount())
If ll_row > 0 Then
	dw_1.ScrolltoRow(ll_row)
	dw_1.SelectRow(ll_row, True)
End If
end event

on w_inst_percent_scroll.create
this.dw_1=create dw_1
this.Control[]={ this.dw_1}
end on

on w_inst_percent_scroll.destroy
destroy(this.dw_1)
end on

type dw_1 from datawindow within w_inst_percent_scroll
event mousemove pbm_mousemove
int Y=5
int Width=677
int Height=521
int TabOrder=1
string DataObject="d_pct_pcs_scroll"
boolean Border=false
boolean VScrollBar=true
boolean LiveScroll=true
end type

event mousemove;String	ls_left, &
			ls_right

Long		ll_row


iw_frame.iu_string.nf_parseLeftRight(This.GetObjectAtPointer(), '~t', ls_left, ls_Right)
If Long(ls_Right) <= 0 Then return
ll_row = This.GetSelectedRow(0)
If Long(ls_Right) <> ll_row Then
	SelectRow(0, False)
	This.SelectRow(Long(ls_Right), True)
End if

end event

on rowfocuschanged;This.SelectRow(0, False)
This.SelectRow(This.GetRow(), True)
end on

