﻿$PBExportHeader$w_pas_instances.srw
$PBExportComments$Instance Window
forward
global type w_pas_instances from w_base_sheet_ext
end type
type cbx_shade_rows from u_base_checkbox_ext within w_pas_instances
end type
type uo_next_prev from u_spin within w_pas_instances
end type
type st_next from statictext within w_pas_instances
end type
type st_previous from statictext within w_pas_instances
end type
type dw_header from u_base_dw_ext within w_pas_instances
end type
type tab_1 from tab within w_pas_instances
end type
type tabpage_inst from userobject within tab_1
end type
type dw_instances from u_base_dw_ext within tabpage_inst
end type
type tabpage_inst from userobject within tab_1
dw_instances dw_instances
end type
type tabpage_inst_parm from userobject within tab_1
end type
type dw_parameters from u_base_dw_ext within tabpage_inst_parm
end type
type tabpage_inst_parm from userobject within tab_1
dw_parameters dw_parameters
end type
type tabpage_auto_conv from userobject within tab_1
end type
type dw_auto_conv from u_base_dw_ext within tabpage_auto_conv
end type
type tabpage_auto_conv from userobject within tab_1
dw_auto_conv dw_auto_conv
end type
type tab_1 from tab within w_pas_instances
tabpage_inst tabpage_inst
tabpage_inst_parm tabpage_inst_parm
tabpage_auto_conv tabpage_auto_conv
end type
type cbx_tab_code from checkbox within w_pas_instances
end type
type cb_copy_button from u_base_commandbutton_ext within w_pas_instances
end type
type str_date_range from structure within w_pas_instances
end type
type str_quantity from structure within w_pas_instances
end type
end forward

type str_date_range from structure
	date		begin_date
	date		end_date
	boolean		new_column
	integer		week_day
end type

type str_quantity from structure
	decimal {2}	ld_quantity
end type

global type w_pas_instances from w_base_sheet_ext
integer width = 4005
integer height = 1584
string title = "Manufacturing Instances"
long backcolor = 67108864
event type integer ue_closeuserobject ( userobject au_userobject )
cbx_shade_rows cbx_shade_rows
uo_next_prev uo_next_prev
st_next st_next
st_previous st_previous
dw_header dw_header
tab_1 tab_1
cbx_tab_code cbx_tab_code
cb_copy_button cb_copy_button
end type
global w_pas_instances w_pas_instances

type prototypes
function long SendMessage ( ulong whnd, uint wmsg, ulong wParam, ulong lParam ) library "user32.dll" alias for "SendMessageA"
end prototypes

type variables
Private:
Boolean		ib_optimize_done, &
		ib_reinquire_necessary, &
		ib_changes_made, &
		ib_no_instance_data, &
		ib_no_parameter_data, &
		ib_no_auto_conv_data, &
		ib_instances_redraw, &
		ib_parameters_redraw, &
		ib_protection_status, &
		ib_date_protection, &
		ib_save_instances, &
		ib_save_parm, &
		ib_save_auto, &
		ib_initial_inquire, &
		ib_bypass_check_for_changes

// This is definitely a PowerBuilder 8.0.1.90???? hack.  This needs to be fixed.  REM.
	boolean ib_hs
//

Date		idt_inquire_date
			
Int		ii_column_number, &
		ii_column_number_parm, &
		ii_column_number_auto_conv, &
		ii_start_column, &
		ii_start_column_parm, &
		ii_start_column_auto_conv, &
		ii_PaPast
		
str_date_range	istr_date_range[35], &
					istr_date_range_parm[35], &
					istr_date_range_auto_conv[35]
					
String		is_addrow_text, &
		is_deleterow_text, &
		is_dest_plants, &
		is_column_modify, &
		is_timestamp_string, &
		is_tab_index
Window		iw_dest_plants
w_pas_instances	iw_ThisWindow

INTEGER 	ii_Query_Retry_Count

Public:
s_error		istr_error_info

u_pas202		iu_pas202
u_pas203		iu_pas203
u_ws_pas1		iu_ws_pas1
u_ws_pas3		iu_ws_pas3

// The reason these are named starting with "dnv"
//    is historical, so don't bother asking why.
DataStore	dnv_instances, &
		dnv_parameters, &
		dnv_ratios, &
		dnv_update_instances, &
		dnv_update_parameters, &
		dnv_update_auto_conv, &
		ids_piece_count, &
		dnv_auto_conv, &
		ids_original_instances, &
		ids_original_instances_dw_data
		
LONG il_horz_scroll_pos1, il_horz_scroll_pos2		

Date 	idt_instances_end_date

end variables

forward prototypes
public subroutine wf_unselect_rows ()
public subroutine wf_import_plants (string as_plants)
public function boolean wf_check_for_changes ()
public subroutine wf_print ()
public subroutine wf_reset_title_text ()
public function boolean wf_clear_values (datawindow adw_clear_dw)
public function boolean wf_daily_parm ()
public subroutine wf_reset_title_text_parm ()
public function integer wf_check_parms_daily (readonly date adt_instance_date, readonly long al_row, readonly double ad_instance_value)
public function string wf_duplicate_step (readonly datawindow adw_datawindow, readonly long al_row)
public function boolean wf_validate_instance_daily (readonly date adt_parm_date, readonly long al_row, readonly string as_parm, readonly double ad_min_value, readonly double ad_max_value)
public function boolean wf_daily ()
public function boolean wf_long_term ()
public function boolean wf_long_term_parm ()
public function integer wf_check_parms_long (readonly date adt_inst_begin_date, readonly date adt_inst_end_date, readonly long al_row, readonly double ad_inst_value)
public function boolean wf_validate_instance_long (date adt_parm_begin_date, date adt_parm_end_date, readonly long al_row, readonly string as_parm, readonly double ad_min_value, readonly double ad_max_value)
public subroutine wf_reset_title_text_auto_conv ()
public function boolean wf_long_term_auto_conv ()
public function boolean wf_daily_auto_conv ()
public function boolean wf_next ()
public function boolean wf_previous ()
public function boolean wf_fill_values_auto_conv ()
public function boolean wf_fill_values_parm ()
public function boolean wf_addrow ()
public function boolean wf_deleterow ()
public function integer wf_add_date_range_parm (date adt_begin, date adt_end)
public function integer wf_add_date_range_auto_conv (date adt_begin, date adt_end)
public subroutine wf_protect (string as_display, long al_tab_selected)
public subroutine wf_check_auth ()
public function boolean wf_update ()
public subroutine wf_copy_parm ()
public function integer wf_add_date_range (date adt_begin, date adt_end)
public function boolean wf_retrieve ()
public function boolean wf_get_step (ref string as_product_code, ref integer ai_step_sequence, ref string as_product_state, ref string as_product_status)
public function integer wf_update_instances ()
public function boolean wf_update_parameters ()
public function boolean wf_update_auto_conv ()
public subroutine wf_rightsideclicked ()
public subroutine wf_rightsideclicked (long al_xpos, long al_ypos, string as_dwname)
public subroutine wf_shade_rows ()
public function boolean wf_two_week_daily ()
public function boolean wf_two_week_parm ()
public function boolean wf_two_week_auto_conv ()
public function boolean wf_three_week_daily ()
public function boolean wf_three_week_parm ()
public function boolean wf_three_week_auto_conv ()
public function boolean wf_long_term_day_of_week ()
public function integer wf_add_date_range_pattern (date adt_begin, date adt_end)
public function integer wf_add_date_range_overflow (date adt_begin, date adt_end)
public function boolean wf_fill_values (string as_display_status)
public subroutine wf_instances_columns_exceeded (integer ai_column_number, date adt_start_date, date adt_end_date)
end prototypes

event ue_closeuserobject;call super::ue_closeuserobject;Return CloseUserObject(au_userobject)
end event

public subroutine wf_unselect_rows ();tab_1.tabpage_inst.dw_instances.SelectRow(0, False)
end subroutine

public subroutine wf_import_plants (string as_plants);//DataWindowChild		ldwc_plants


If Len(Trim(as_plants)) = 0 Then 
	is_dest_plants = ''
	return
End If

as_plants = "location_code = '" + as_plants + "'"
as_plants = iw_frame.iu_string.nf_globalreplace("~r~n", "' or location_code = '", as_plants, 1)

DataStore lds_locations
lds_locations = Create DataStore
lds_locations.DataObject = 'd_locations'
lds_locations.SetTransObject(SQLCA)
lds_locations.Retrieve()
lds_locations.SetFilter(as_plants)
lds_locations.Filter()
is_dest_plants = lds_locations.Describe("DataWindow.Data")
Destroy lds_locations

return

end subroutine

public function boolean wf_check_for_changes ();Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return false

return True
end function

public subroutine wf_print ();If IsValid(w_pas_output_products) Then
	w_pas_output_products.wf_print()
Else
	tab_1.tabpage_inst.dw_instances.Print()
End If



end subroutine

public subroutine wf_reset_title_text ();Int	li_counter, &
		li_upper_Bound

String	ls_Modify


str_date_range	lstr_date_range_reset[35]


ls_Modify = ''
li_upper_bound = UpperBound(istr_date_range)


For li_counter = 1 to 30
	// Build the string to reset text
	ls_Modify += "quantity_" + String(li_counter) + "_t.Text = '' " + &
					"quantity_" + String(li_counter) + "_t.Visible = 0 " + &
					"quantity_" + String(li_counter) + "_t.BackGround.Color = 78682240 " + &
					"quantity_" + String(li_counter) + ".Visible = 0 "

Next

tab_1.tabpage_inst.dw_instances.Modify(ls_Modify)
ii_column_number = 0
istr_date_range = lstr_date_range_reset

return
end subroutine

public function boolean wf_clear_values (datawindow adw_clear_dw);// NO SETREDRAW DONE IN THIS FUNCTION
Int	li_counter, &
		li_tab, &
		li_step_Seq

Long	ll_row, &
		ll_row_count

String	ls_temp, &
			ls_data


ls_data = adw_clear_dw.object.DataWindow.Data
If Len(ls_data) <= 0 Then return false
ls_data += '~r~n'
ls_temp = ""
li_tab = 0
Do While Len(ls_data) > 0
	For li_counter = 1 to 7
		li_tab = Pos(ls_data, '~t', li_tab + 1)	
	Next
	ls_temp += Left(ls_data, li_tab -1) + "~r~n"
	li_tab = Pos(ls_data, '~r~n', li_tab)
	// the - 1 is for the ~n
	ls_data = Right(ls_data, Len(ls_data) - li_tab - 1)
	li_tab = 0
Loop

adw_clear_dw.Reset()
adw_clear_dw.ImportString(ls_temp)

// now go delete any duplicate rows
ll_row_count = adw_clear_dw.RowCount()
For li_counter = 1 to ll_row_count - 1
	ll_row = 1
	li_step_seq = adw_clear_dw.GetItemNumber(li_counter, "mfg_step_Sequence")
	Do while ll_row > 0 and ll_row < ll_row_count
		ll_row = adw_clear_dw.Find("mfg_step_Sequence = " + &
							String(li_step_seq), li_counter + 1, ll_row_count)
		If ll_row > 0 Then
			adw_clear_dw.DeleteRow(ll_row)
			ll_row_count = adw_clear_dw.RowCount()
		End if
	Loop
Next

ls_data = adw_clear_dw.object.DataWindow.Data

return true
end function

public function boolean wf_daily_parm ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp

Int	li_counter, &
		li_days_after

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_Parm_TitleModify


SetPointer(HourGlass!)

ii_start_column_parm = 1
ii_column_number_parm = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_parm_TitleModify = ''

For li_counter = 0 to 6
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number_parm ++
	istr_date_range_parm[ii_column_number_parm].begin_date = ldt_temp 
	istr_date_range_parm[ii_column_number_parm].end_date   = ldt_temp 

	ls_Parm_TitleModify += "parm_" + String(ii_column_number_parm) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yyyy") + "~~r~~n" + DayName(ldt_temp) + "' " + &
						"parm_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"max_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_parm) + ".Visible = 1 " + &
						"max_" + String(ii_column_number_parm) + ".Visible = 1 " 
Next	

tab_1.tabpage_inst_parm.dw_parameters.Modify(ls_Parm_TitleModify)

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')

return True
end function

public subroutine wf_reset_title_text_parm ();Int	li_counter, &
		li_upper_Bound

String	ls_parm_Modify

str_date_range	lstr_date_range_reset[30]


ls_parm_Modify = ''
li_upper_bound = UpperBound(istr_date_range_parm)


For li_counter = 1 to li_upper_bound
	// Build the string to reset text
	ls_parm_Modify += "parm_" + String(li_counter) + "_t.Text = '' " + &
					"parm_" + String(li_counter) + "_t.Visible = 0 " + &
					"min_" + String(li_counter) + "_t.Visible = 0 " + &
					"max_" + String(li_counter) + "_t.Visible = 0 " + &
					"min_" + String(li_counter) + ".Visible = 0 " + &
					"max_" + String(li_counter) + ".Visible = 0 " 
Next

tab_1.tabpage_inst_parm.dw_parameters.Modify(ls_parm_Modify)
ii_column_number_parm = 0
istr_date_range_parm = lstr_date_range_reset

return
end subroutine

public function integer wf_check_parms_daily (readonly date adt_instance_date, readonly long al_row, readonly double ad_instance_value);Integer			li_counter
Date				ldt_parm_date
Double			ld_min_value, &
					ld_max_value
String			ls_temp


SetNull(ldt_parm_date)

For li_counter = 1 to ii_column_number_parm
	ldt_parm_date = Date(mid(tab_1.tabpage_inst_parm.dw_parameters.Describe('parm_' + &
			String(li_counter) + '_t.text'), 2, 10))
If ldt_parm_date = adt_instance_date Then Exit
next 

If IsNull(ldt_parm_date) Then Return 0

IF IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(al_row, 'min_' + &
	String(li_counter))) Then
	ld_min_value = 0.00
Else
	ld_min_value = tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(al_row, 'min_' + &
		String(li_counter)) 
End If

If IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(al_row, 'max_' + &
	String(li_counter))) Then
	ld_max_value = 100.00
Else
	ld_max_value = tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(al_row, 'max_' + &
		String(li_counter)) 
End If

If ad_instance_value < ld_min_value Then Return -1
If ad_instance_value > ld_max_value Then Return 1

Return 0
end function

public function string wf_duplicate_step (readonly datawindow adw_datawindow, readonly long al_row);String		ls_temp, &
				ls_data

ls_Temp = String(adw_datawindow.GetItemNumber(al_row, "mfg_step_sequence"))
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Error Getting Step Information")
	return ''
End IF
ls_data = ls_temp + '~t'

ls_temp = adw_datawindow.GetItemString(al_row, "mfg_step_descr")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Error Getting Step Information")
	return ''
End if
ls_data += ls_temp + '~t'

ls_temp = adw_datawindow.GetItemString(al_row, "step_type")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Error Getting Step Information")
	return ''
End if
ls_data += ls_temp + '~t'

ls_temp = String(adw_datawindow.GetItemNumber(al_row, "step_duration"))
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Error Getting Step Information")
	return ''
End if
ls_data += ls_temp + '~t'

ls_temp = adw_datawindow.GetItemString(al_row, "dump_product_ind")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Error Getting Step Information")
	return ''
End if
ls_data += ls_temp + '~t'

ls_temp = String(adw_datawindow.GetItemNumber(al_row, "key_pieces_per_box"))
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Error Getting Pieces Per Box Information")
	return ''
End if
ls_data += ls_temp + '~t'


//Ratio has been ommitted from this string.
//ls_temp = String(adw_datawindow.GetItemNumber(al_row, "ratio"))
//If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
//	SetMicroHelp("Error Getting Ratio Information")
//	return ''
//End if
//ls_data += ls_temp + '~t'

ls_temp = adw_datawindow.GetItemString(al_row, "fab_uom")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	SetMicroHelp("Error Getting Fab UOM Information")
	return ''
End if
ls_data += ls_temp + '~r~n'

Return ls_data
end function

public function boolean wf_validate_instance_daily (readonly date adt_parm_date, readonly long al_row, readonly string as_parm, readonly double ad_min_value, readonly double ad_max_value);Integer			li_counter
Date				ldt_instance_date
Double			ld_instance_value
String			ls_temp, &
					ls_detail_errors



For li_counter = 1 to ii_column_number
	ldt_instance_date = Date(mid(tab_1.tabpage_inst.dw_instances.Describe('quantity_' + &
			String(li_counter) + '_t.text'), 2, 10))
If ldt_instance_date = adt_parm_date Then Exit
next 

IF IsNull(tab_1.tabpage_inst.dw_instances.GetItemNumber(al_row, 'quantity_' + &
		String(li_counter))) Then
	ld_instance_value = 0.00
Else
	ld_instance_value = tab_1.tabpage_inst.dw_instances.GetItemDecimal(al_row, 'quantity_' + &
		String(li_counter)) 
End If
If as_parm = 'min' Then
	If ld_instance_value < ad_min_value or (ad_min_value > 0 And IsNull(ld_instance_value)) Then 
		ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
		ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, 'E')
		tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
		Return False
	Else
		IF ld_instance_value <= ad_max_value Then
			ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
			ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, ' ')
			tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
			iw_frame.SetMicroHelp('Ready')
		End IF
	End IF
Else
	If ld_instance_value > ad_max_value Then 
		ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
		ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, 'E')
		tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
		Return False
	Else
		IF ld_instance_value >= ad_min_value Then
			ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
			ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, ' ')
			tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
			iw_frame.SetMicroHelp('Ready')
		End IF
	End IF
End IF


Return True

end function

public function boolean wf_daily ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp, &
		ldt_duration_begin_date, &
		ldt_duration_end_date, &
		ldt_instance_date, &
		ldt_header_start_date, &
		ldt_first_monday, &
		ldt_first_sunday, &
		ldt_prev_monday, &
		ldt_new_begin_date

Int	li_counter, &
		li_days_after, &
		li_instances_rowcount, &
		li_row, li_col, li_duration_days, &
		li_start_day_number, li_new_row

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_TitleModify, &
			ls_detail_errors, &
			ls_detail_errors_new
			

SetPointer(HourGlass!)

ii_start_column = 1
ii_column_number = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_TitleModify = ''

For li_counter = 0 to 6
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number ++
	istr_date_range[ii_column_number].begin_date = ldt_temp 
	istr_date_range[ii_column_number].end_date   = ldt_temp 

	ls_TitleModify += "quantity_" + String(ii_column_number) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yy") + "~~r~~n" + left(DayName(ldt_temp),3) + "' " + &
							"quantity_" + String(ii_column_number) + "_t.Visible = 1 " + &
							"quantity_" + String(ii_column_number) + "_t.BackGround.Color = 78682240 " + &
							"quantity_" + String(ii_column_number) + ".Visible = 1 "

Next	

tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)

// Clear Values for Piece Count
tab_1.tabpage_inst.dw_instances.Object.total_1.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_2.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_3.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_4.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_5.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_6.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_7.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_8.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_9.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_10.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_11.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_12.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_13.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_14.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_15.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_16.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_17.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_18.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_19.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_20.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_21.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_8.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_9.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_10.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_11.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_12.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_13.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_14.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_15.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_16.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_17.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_18.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_19.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_20.Visible = False
tab_1.tabpage_inst.dw_instances.Object.total_21.Visible = False

li_days_after = DaysAfter(idt_inquire_date, ldt_start)
ll_row_count = ids_piece_count.RowCount()

If li_days_after >= 0 Then
	For li_counter = 1 to 7
		If li_counter + li_days_after <= ll_row_count Then
			ls_temp = "total_" + String(li_counter) + ".Text = '" + String( &
					ids_piece_count.GetItemNumber(li_counter + li_days_after, 'piece_count')) + "'"
			ls_ret = tab_1.tabpage_inst.dw_instances.Modify(ls_temp)
			If Len(ls_ret) > 0 Then MessageBox("Modify", ls_ret + '~r~n' + ls_temp)
		End if
	Next
End if

// new routine to handle patterns

ldt_header_start_date = dw_header.GetItemDate(1, "begin_date")

ls_temp = dnv_instances.Object.DataWindow.Data

li_start_day_number = DayNumber(dw_header.GetItemDate(1, "begin_date"))
Choose Case li_start_day_number
	Case 1  
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 1)
	Case 2
		ldt_first_monday = dw_header.GetItemDate(1, "begin_date")
	Case 3
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 6)
	Case 4
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 5)
	Case 5
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 4)
	Case 6
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 3)
	Case 7
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 2)
End Choose
ldt_first_sunday = RelativeDate(ldt_first_monday, -1)
ldt_prev_monday = RelativeDate(ldt_first_monday, -7)

ll_row_count = dnv_instances.RowCount()
For li_counter = 1 to ll_row_count
	If (dnv_instances.GetItemString(li_counter, "pattern_type") = "W") Then
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") - 1)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,7)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") +6)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,7)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +6))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +6))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If			

		dnv_instances.DeleteRow(li_counter)
		ll_row_count --
		li_counter --		
	End If
Next

//end pattern routine

If dw_header.GetItemDate(1, "begin_date") < Today() Then
	li_instances_rowcount =  tab_1.tabpage_inst.dw_instances.RowCount()
	For li_row = 1 to li_instances_rowcount
		If tab_1.tabpage_inst.dw_instances.GetItemString (li_row, "step_type") <> 'F' Then
			li_duration_days = Int(tab_1.tabpage_inst.dw_instances.GetItemNumber(li_row, "step_duration") / 3)
			If (li_duration_days >= 1) OR (tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "dump_product_ind") = "Y") Then
				ldt_duration_end_date = RelativeDate(Today(), -1)	
				ldt_duration_begin_date =  RelativeDate(Today(), 0 - li_duration_days)	
				For li_col = 1 to 30
					ldt_instance_date = Date(mid(tab_1.tabpage_inst.dw_instances.Describe('quantity_' + &
							String(li_col) + '_t.text'), 2, 10))
					If ((ldt_instance_date >= ldt_duration_begin_date) and (ldt_instance_date <= ldt_duration_end_date)) OR &
							((tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "dump_product_ind") = "Y") and (ldt_instance_date < Today())) Then
//						MessageBox("Unprotect Row Column", "Unprotect row = " + string(li_row) + " column= " + string(li_col) + " instance date= " + string(ldt_instance_date,"mm/dd/yyyy")	)
						 ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "detail_errors")
						 if li_col = 1 then
							ls_detail_errors_new = 'U' + mid(ls_detail_errors,2,29)
						else	
							ls_detail_errors_new = mid(ls_detail_errors,1,li_col - 1) + 'U' + mid(ls_detail_errors, li_col + 1, 30 - li_col)
						End If
						tab_1.tabpage_inst.dw_instances.SetItem(li_row, "detail_errors", ls_detail_errors_new)
					End If
				Next
			End If
		End if
	Next
End If

tab_1.tabpage_inst.dw_instances.Object.DataWindow.Header.Height = 209

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')


return True
end function

public function boolean wf_long_term ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

//u_base_dw_ext	ldw_date_sort
DataStore			ldw_date_sort

Date		ldt_begin, &
			ldt_end, &
			ldt_header_start_date, &
			ldt_begin_date, &
			ldt_end_date, &
			ldt_new_start, &
			ldt_new_end

Int		li_counter, &
			li_sub_counter, &
			li_ret, &
			li_temp, &
			li_new_row, &
			li_week_day, &
			li_counter_2

Long		ll_row_count, &
			ll_sort_row_Count

String	ls_temp, &
			ls_temp2, &
			ls_titleModify, &
			ls_ret

Boolean	lb_interval_found

SetPointer(HourGlass!)

ii_start_column = 1
dnv_instances.SetSort("begin_date A, end_date A")
dnv_instances.Sort()

/******************************************************
**	Create dynamic datawindow to hold all unique dates
** Fill in all dates from dnv_instances
******************************************************/

//This.OpenUserObject(ldw_date_sort, 'u_base_dw_ext')
//ldw_date_sort.Visible = False
ldw_date_sort = create DataStore
ldw_date_sort.DataObject = 'd_pas_instances_date_sort'

dnv_instances.SetSort("mfg_step_sequence A, dest_plant A, begin_date A, end_date A ")
dnv_instances.Sort()

ls_temp = dnv_instances.Object.DataWindow.Data

ll_sort_row_count = 0

ldt_header_start_date = dw_header.GetItemDate(1, "begin_date")

ll_row_count = dnv_instances.RowCount()
For li_counter = 1 to ll_row_count
	If (dnv_instances.GetItemString(li_counter, "pattern_type") = "W") Then
		ldt_begin_date = dnv_instances.GetItemDate(li_counter, "begin_date")
		ldt_end_date = dnv_instances.GetItemDate(li_counter, "end_date")
		DO WHILE ldt_begin_date <= ldt_end_date
			li_week_day = dnv_instances.GetItemNumber(li_counter, "pattern_day")
			ldt_new_start = RelativeDate(ldt_begin_date, li_week_day - 1)
			ldt_new_end = RelativeDate(ldt_begin_date, li_week_day - 1)
			if ldt_new_start >= ldt_header_start_date then
				li_new_row = dnv_instances.InsertRow(0)
				dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
				dnv_instances.SetItem(li_new_row, "begin_date", ldt_new_start)
				dnv_instances.SetItem(li_new_row, "end_date", ldt_new_end)
				dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
				dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
				dnv_instances.SetItem(li_new_row, "pattern_type", " ")
				dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			End If	
			ldt_begin_date = RelativeDate(ldt_begin_date,7)
		LOOP
		dnv_instances.DeleteRow(li_counter)
		ll_row_count --
		li_counter --
	End If	
Next

dnv_instances.SetSort("mfg_step_sequence A, dest_plant A, begin_date A, end_date A ")
dnv_instances.Sort()

ls_temp = dnv_instances.Object.DataWindow.Data

//merge date ranges together for an instance step if the amounts are the same
ll_row_count = dnv_instances.RowCount()
For li_counter = 1 to ll_row_count
	For li_counter_2 = li_counter +1 to ll_row_count
		If dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence") = dnv_instances.GetItemNumber(li_counter_2, "mfg_step_sequence") Then
			If dnv_instances.GetItemString(li_counter, "dest_plant") = dnv_instances.GetItemString(li_counter_2, "dest_plant") Then
				IF 	dnv_instances.GetItemNumber(li_counter, "amount") = dnv_instances.GetItemNumber(li_counter_2, "amount") Then
					IF 	dnv_instances.GetItemDate(li_counter, "end_date") = RelativeDate(dnv_instances.GetItemDate(li_counter_2, "begin_date"),-1) Then 
						dnv_instances.SetItem(li_counter, "end_date", dnv_instances.GetItemDate(li_counter_2, "end_date"))
						dnv_instances.DeleteRow(li_counter_2)
						li_counter_2 --
						ll_row_count --
					End IF
				End If
			End If
		End If
	Next	
Next

ls_temp = dnv_instances.Object.DataWindow.Data


dnv_instances.SetSort("begin_date A, pattern_day A, end_date A")
dnv_instances.Sort()

ls_temp = dnv_instances.Object.DataWindow.Data

ll_row_count = dnv_instances.RowCount()
For li_counter = 1 to ll_row_count
	// check end date first.  If End date is less than begin date, then discard row
	ls_temp = String(dnv_instances.GetItemDate(li_counter, "end_date"), "yyyy-mm-dd")

	If Date(ls_temp) < ldt_header_start_date Then Continue

	If ldw_date_sort.Find("Left(date_col, 10) = '" + ls_temp + &
								"' And Right(date_col, 1) = 'E'", 1, ll_sort_row_count) = 0 Then
		ldw_date_sort.ImportString(ls_temp + 'E~r~n')
		ll_sort_row_count ++
	End if

	ls_temp = String(dnv_instances.GetItemDate(li_counter, "begin_date"), "yyyy-mm-dd")
	If Date(ls_temp) < ldt_header_start_Date Then 
		ls_temp = String(ldt_header_start_date, "yyyy-mm-dd")
	End if

	If ldw_date_sort.Find("Left(date_col, 10) = '" + ls_temp + &
								"' And Right(date_col, 1) = 'B'", 1, ll_sort_row_count) = 0 Then
		ldw_date_sort.ImportString(ls_temp + 'B~r~n')
		ll_sort_row_count ++
	End if
Next

li_ret = ldw_date_sort.Sort()
ls_temp = ldw_date_sort.Object.DataWindow.Data

/**********************************************
**  Get date range pairs and create columns
**********************************************/
ll_row_count = ldw_date_sort.RowCount()
ls_TitleModify = ''

for li_counter = 1 to ll_row_count
	ls_temp = ldw_date_sort.GetItemString(li_counter, "date_col")
	If Right(ls_temp, 1) = 'B' Then
		ldt_begin = Date(Left(ls_temp, 10))
		If li_counter < ll_row_count Then
			ls_temp2 = ldw_date_sort.GetItemString(li_counter + 1, "date_col")
			If Right(ls_temp2, 1) = 'B' Then
				ldt_end = RelativeDate(Date(Left( ls_temp2, 10)), -1)
			Else
				ldt_end = Date(Left(ls_temp2, 10))
			End if
		Else
			Continue
		End if
	Else
		ldt_end = Date(Left(ls_temp, 10))
		If li_counter > 1 Then
			ls_temp2 = ldw_date_sort.GetItemString(li_counter - 1, "date_col")
			If Right(ls_temp2, 1) = 'E' Then
				ldt_begin = RelativeDate(Date(Left(ls_temp2, 10)), 1)
			Else
				// picked up this date range on last loop
				Continue
			End if
		Else
			Continue
		End if
	End if
	ii_column_number ++
//	If ii_column_number > UpperBound(istr_date_range) Then
//		ii_column_number --
//		li_temp = UpperBound(istr_date_range)
//		MessageBox("Date Range", "Maximum Number of Date Ranges exceeded.  " + &
//				"Only the first " + String(li_temp) + " Date Ranges will be displayed.  " + &
//				"Contact Applications Programming.")
//		Exit
//	End if		
	If ii_column_number > 30 Then
		ii_column_number --
		li_temp = UpperBound(istr_date_range)
		MessageBox("Date Range", "Maximum Number of Date Ranges exceeded.  " + &
				"Only the first 30 Date Ranges will be displayed.  " + &
				"Contact Applications Programming.")
		Exit
	End if			
	istr_date_range[ii_column_number].begin_date = ldt_begin 
	istr_date_range[ii_column_number].end_date   = ldt_end 

	ls_TitleModify += "quantity_" + String(ii_column_number) + "_t.Text = '" + &
							String(ldt_begin, "mm/dd/yy") + "~~r~~n" + &
							String(ldt_end, "mm/dd/yy") + "' " + &
							"quantity_" + String(ii_column_number) + "_t.Visible = 1 " + &
							"quantity_" + String(ii_column_number) + "_t.BackGround.Color = 78682240 " + &
							"quantity_" + String(ii_column_number) + ".Visible = 1 " 
Next

ls_ret = tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)
If Len(ls_ret) > 0 Then
	MessageBox("Modify", ls_ret)
End if

// this might miss date ranges for instances that are very long term
for li_counter = 1 to ii_column_number - 1
	If DaysAfter(istr_date_range[li_counter].end_date, &
			istr_date_range[li_counter + 1].begin_date) <> 1 Then
		// these are not successive date ranges, check to see if anything needs it
		ll_row_count = dnv_instances.RowCount()
		for li_sub_counter = 1 to ll_row_count
			If dnv_instances.GetItemDate(li_sub_counter, "begin_date") < &
					istr_date_range[li_counter].end_date And &
					dnv_instances.GetItemDate(li_sub_counter, "end_date") > &
					istr_date_range[li_counter + 1].begin_date Then
				wf_add_date_range(RelativeDate(istr_date_range[li_counter].end_date, 1), &
											RelativeDate(istr_date_range[li_counter + 1].begin_date, -1))
				Exit
			End if
		Next
	End if
Next

Destroy ldw_date_sort
//This.PostEvent("ue_CloseUserObject")

tab_1.tabpage_inst.dw_instances.Object.DataWindow.Header.Height = 125

If tab_1.SelectedTab = 1 Then iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

return True
end function

public function boolean wf_long_term_parm ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

//u_base_dw_ext	ldw_date_sort
DataStore			ldw_date_sort

Date		ldt_begin, &
			ldt_end, &
			ldt_header_start_date

Int		li_counter, &
			li_sub_counter, &
			li_ret, &
			li_temp

Long		ll_row_count, &
			ll_sort_row_Count

String	ls_temp, &
			ls_temp2, &
			ls_ret, &
			ls_parm_TitleModify


SetPointer(HourGlass!)

ii_start_column_parm = 1
dnv_parameters.SetSort("begin_date A, end_date A")
dnv_parameters.Sort()

/******************************************************
**	Create dynamic datawindow to hold all unique dates
** Fill in all dates from dnv_parameters
******************************************************/

//This.OpenUserObject(ldw_date_sort, 'u_base_dw_ext')
//ldw_date_sort.Visible = False

ldw_date_sort = Create DataStore
ldw_date_sort.DataObject = 'd_pas_instances_date_sort'

ll_row_count = dnv_parameters.RowCount()
ll_sort_row_count = 0

ldt_header_start_date = dw_header.GetItemDate(1, "begin_date")

For li_counter = 1 to ll_row_count
	// check end date first.  If End date is less than begin date, then discard row
	ls_temp = String(dnv_parameters.GetItemDate(li_counter, "end_date"), "yyyy-mm-dd")

	If Date(ls_temp) < ldt_header_start_date Then Continue

	If ldw_date_sort.Find("Left(date_col, 10) = '" + ls_temp + &
								"' And Right(date_col, 1) = 'E'", 1, ll_sort_row_count) = 0 Then
		ldw_date_sort.ImportString(ls_temp + 'E~r~n')
		ll_sort_row_count ++
	End if

	ls_temp = String(dnv_parameters.GetItemDate(li_counter, "begin_date"), "yyyy-mm-dd")
	If Date(ls_temp) < ldt_header_start_Date Then 
		ls_temp = String(ldt_header_start_date, "yyyy-mm-dd")
	End if

	If ldw_date_sort.Find("Left(date_col, 10) = '" + ls_temp + &
								"' And Right(date_col, 1) = 'B'", 1, ll_sort_row_count) = 0 Then
		ldw_date_sort.ImportString(ls_temp + 'B~r~n')
		ll_sort_row_count ++
	End if
Next

li_ret = ldw_date_sort.Sort()
ls_temp = ldw_date_sort.Object.DataWindow.Data

/**********************************************
**  Get date range pairs and create columns
**********************************************/
ll_row_count = ldw_date_sort.RowCount()
ls_parm_TitleModify = ''

for li_counter = 1 to ll_row_count
	ls_temp = ldw_date_sort.GetItemString(li_counter, "date_col")
	If Right(ls_temp, 1) = 'B' Then
		ldt_begin = Date(Left(ls_temp, 10))
		If li_counter < ll_row_count Then
			ls_temp2 = ldw_date_sort.GetItemString(li_counter + 1, "date_col")
			If Right(ls_temp2, 1) = 'B' Then
				ldt_end = RelativeDate(Date(Left( ls_temp2, 10)), -1)
			Else
				ldt_end = Date(Left(ls_temp2, 10))
			End if
		Else
			Continue
		End if
	Else
		ldt_end = Date(Left(ls_temp, 10))
		If li_counter > 1 Then
			ls_temp2 = ldw_date_sort.GetItemString(li_counter - 1, "date_col")
			If Right(ls_temp2, 1) = 'E' Then
				ldt_begin = RelativeDate(Date(Left(ls_temp2, 10)), 1)
			Else
				// picked up this date range on last loop
				Continue
			End if
		Else
			Continue
		End if
	End if
	ii_column_number_parm ++
	If ii_column_number_parm > UpperBound(istr_date_range_parm) Then
		ii_column_number_parm --
		li_temp = UpperBound(istr_date_range_parm)
		MessageBox("Date Range", "Maximum Number of Date Ranges exceeded.  " + &
				"Only the first " + String(li_temp) + " Date Ranges will be displayed.  " + &
				"Contact Applications Programming.")
		Exit
	End if		
	istr_date_range_parm[ii_column_number_parm].begin_date = ldt_begin 
	istr_date_range_parm[ii_column_number_parm].end_date   = ldt_end 
	
	If ii_column_number_parm <= 30 Then

		ls_parm_TitleModify += "parm_" + String(ii_column_number_parm) + "_t.Text = '" + &
								String(ldt_begin, "mm/dd/yy") + "~~r~~n" + &
								String(ldt_end, "mm/dd/yy") + "' " + &
								"parm_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
								"min_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
								"max_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
								"min_" + String(ii_column_number_parm) + ".Visible = 1 " + &
								"max_" + String(ii_column_number_parm) + ".Visible = 1 "
	End If
Next

ls_ret = tab_1.tabpage_inst_parm.dw_parameters.Modify(ls_parm_TitleModify)
If Len(ls_ret) > 0 Then
	MessageBox("Modify", ls_ret)
End if

// this might miss date ranges for instances that are very long term
for li_counter = 1 to ii_column_number_parm - 1
	If DaysAfter(istr_date_range_parm[li_counter].end_date, &
			istr_date_range_parm[li_counter + 1].begin_date) <> 1 Then
		// these are not successive date ranges, check to see if anything needs it
		ll_row_count = dnv_parameters.RowCount()
		for li_sub_counter = 1 to ll_row_count
			If dnv_parameters.GetItemDate(li_sub_counter, "begin_date") < &
					istr_date_range_parm[li_counter].end_date And &
					dnv_parameters.GetItemDate(li_sub_counter, "end_date") > &
					istr_date_range_parm[li_counter + 1].begin_date Then
				wf_add_date_range_parm(RelativeDate(istr_date_range_parm[li_counter].end_date, 1), &
											RelativeDate(istr_date_range_parm[li_counter + 1].begin_date, -1))
				Exit
			End if
		Next
	End if
Next

//This.PostEvent("ue_CloseUserObject")
Destroy ldw_date_sort

If tab_1.SelectedTab = 2 Then
	If Not ib_protection_status then
		iw_frame.im_menu.mf_Enable('m_addrow')
	Else
		iw_frame.im_menu.mf_disable('m_addrow')
	End If
End If

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

return True
end function

public function integer wf_check_parms_long (readonly date adt_inst_begin_date, readonly date adt_inst_end_date, readonly long al_row, readonly double ad_inst_value);Date					ldt_parm_begin_date, &
						ldt_parm_end_date
						
Double			ld_min_value, &
					ld_max_value

Integer			li_counter

String				ls_temp


For li_counter = 1 to ii_column_number_parm
	ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Describe('parm_' + String(li_counter) + '_t.text')
	ldt_parm_begin_date = date(Mid(ls_temp, 2, 10))
	ls_temp = Right(ls_temp, 11)
	ldt_parm_end_date = Date(Left(ls_temp, len(ls_temp) - 1))
//Hack because we are displaying only two digit year in the dates
//for instances.  And if the date includes a year greater than 50
//Powerbuilder converts the century to be '19'.  When we use 12-31-2999
//as the default end of time, it gets converted to 12-31-1999 which has
//already expired.

			if year(ldt_parm_begin_date) < 2000 then
				ldt_parm_begin_date = date("20" + Mid(string(ldt_parm_begin_date, "yyyy-mm-dd"),3,8))
			end if
			if year(ldt_parm_end_date) < 2000 then
				ldt_parm_end_date = date("20" + Mid(string(ldt_parm_end_date, "yyyy-mm-dd"),3,8))
			end if

	If adt_inst_begin_date <= ldt_parm_end_date And adt_inst_end_date >= ldt_parm_begin_date Then
		If IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(al_row, 'min_' + &
				String(li_counter))) Then
			ld_min_value = 0.00
		Else
			ld_min_value = tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(al_row, 'min_' + &
				String(li_counter)) 
		End If
		IF IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(al_row, 'max_' + &
			String(li_counter))) Then
			ld_max_value = 100.00
		Else
			ld_max_value = tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(al_row, 'max_' + &
				String(li_counter)) 
		End If		
		
		If ad_inst_value < ld_min_value Then Return -1
		If ad_inst_value > ld_max_value Then Return 1
	End If
next 

Return 0

end function

public function boolean wf_validate_instance_long (date adt_parm_begin_date, date adt_parm_end_date, readonly long al_row, readonly string as_parm, readonly double ad_min_value, readonly double ad_max_value);Boolean			lb_error, &
					lb_date_range_found

Integer			li_counter, &
					li_date_offset

Date				ldt_inst_begin_date, &
					ldt_inst_end_date
					
Double			ld_instance_value

String			ls_temp, &
					ls_detail_errors



lb_error = False
lb_date_range_found = False

For li_counter = 1 to ii_column_number
//	ls_temp = tab_1.tabpage_inst.dw_instances.Describe('quantity_' + String(li_counter) + '_t.text')
//	ldt_inst_begin_date = date(Mid(ls_temp, 2, 10))
//	ls_temp = Right(ls_temp, 11)
//	ldt_inst_end_date = Date(Left(ls_temp, len(ls_temp) - 1))

//Hack because we are displaying only two digit year in the dates
//for instances.  And if the date includes a year greater than 50
//Powerbuilder converts the century to be '19'.  When we use 12-31-2999
//as the default end of time, it gets converted to 12-31-1999 which has
//already expired.

//	if year(ldt_inst_begin_date) < 2000 then
//		ldt_inst_begin_date = date("20" + Mid(string(ldt_inst_begin_date, "yyyy-mm-dd"),3,8))
//	end if
//	if year(ldt_inst_end_date) < 2000 then
//		ldt_inst_end_date = date("20" + Mid(string(ldt_inst_end_date, "yyyy-mm-dd"),3,8))
//	end if
//
//	if year(adt_parm_begin_date) < 2000 then
//		adt_parm_begin_date = date("20" + Mid(string(adt_parm_begin_date, "yyyy-mm-dd"),3,8))
//	end if
//	if year(adt_parm_end_date) < 2000 then
//		adt_parm_end_date = date("20" + Mid(string(adt_parm_end_date, "yyyy-mm-dd"),3,8))
//	end if
//

	If istr_date_range[li_counter].week_day > 0 then
		
			ldt_inst_begin_date = RelativeDate(istr_date_range[li_counter].begin_date,istr_date_range[li_counter].week_day - 1) 
			ldt_inst_end_date = ldt_inst_begin_date
			li_date_offset = 0
			DO WHILE ldt_inst_begin_date <= istr_date_range[li_counter].end_date
				
				If adt_parm_begin_date <= ldt_inst_end_date And adt_parm_end_date >= ldt_inst_begin_date Then
					lb_date_range_found = True
					If IsNull(ld_instance_value = tab_1.tabpage_inst.dw_instances.GetItemNumber(al_row, 'quantity_' + &
							String(li_counter))) Then
						ld_instance_value = 0.00
					Else
						ld_instance_value = tab_1.tabpage_inst.dw_instances.GetItemDecimal(al_row, 'quantity_' + &
								String(li_counter)) 
					End If
					If as_parm = 'min' Then
						If ld_instance_value < ad_min_value or (ad_min_value > 0 And IsNull(ld_instance_value)) Then 
							ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
							ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, 'E')
							tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
							lb_error = True
						Else
							If ld_instance_value <= ad_max_value Then 
								ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
								ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, ' ')
								tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
								iw_frame.SetMicroHelp('Ready')
							End IF
						End IF
					Else
						If ld_instance_value > ad_max_value Then 
							ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
							ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, 'E')
							tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
							lb_error = True
						Else
							If ld_instance_value >= ad_min_value Then 
								ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
								ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, ' ')
								tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
								iw_frame.SetMicroHelp('Ready')
							End IF
						End IF
					End IF
				End IF				

				li_date_offset = li_date_offset + 7
				ldt_inst_begin_date = RelativeDate(ldt_inst_begin_date, li_date_offset)
				ldt_inst_end_date = ldt_inst_begin_date
			LOOP		
		
	Else
		ldt_inst_begin_date = istr_date_range[li_counter].begin_date
		ldt_inst_end_date = istr_date_range[li_counter].end_date	
		
		If adt_parm_begin_date <= ldt_inst_end_date And adt_parm_end_date >= ldt_inst_begin_date Then
			lb_date_range_found = True
			If IsNull(ld_instance_value = tab_1.tabpage_inst.dw_instances.GetItemNumber(al_row, 'quantity_' + &
					String(li_counter))) Then
				ld_instance_value = 0.00
			Else
				ld_instance_value = tab_1.tabpage_inst.dw_instances.GetItemDecimal(al_row, 'quantity_' + &
						String(li_counter)) 
			End If
			If as_parm = 'min' Then
				If ld_instance_value < ad_min_value or (ad_min_value > 0 And IsNull(ld_instance_value)) Then 
					ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
					ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, 'E')
					tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
					lb_error = True
				Else
					If ld_instance_value <= ad_max_value Then 
						ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
						ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, ' ')
						tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
						iw_frame.SetMicroHelp('Ready')
					End IF
				End IF
			Else
				If ld_instance_value > ad_max_value Then 
					ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
					ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, 'E')
					tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
					lb_error = True
				Else
					If ld_instance_value >= ad_min_value Then 
						ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
						ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, ' ')
						tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
						iw_frame.SetMicroHelp('Ready')
					End IF
				End IF
			End IF
		End IF
	End If
next 

If Not lb_date_range_found and as_parm = 'min' Then
	li_counter = This.wf_add_date_range(adt_parm_begin_date, adt_parm_end_date)
	ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(al_row, 'detail_errors')
	ls_detail_errors = Replace(ls_detail_errors, li_counter, 1, 'E')
	tab_1.tabpage_inst.dw_instances.SetItem(al_row, 'detail_errors', ls_detail_errors)
	lb_error = True
End if
	
If lb_error Then
	Return False
Else
	Return True
End If

end function

public subroutine wf_reset_title_text_auto_conv ();Int	li_counter, &
		li_upper_Bound

String	ls_auto_conv_Modify

str_date_range	lstr_date_range_reset[30]


ls_auto_conv_Modify = ''
li_upper_bound = UpperBound(istr_date_range_auto_conv)


For li_counter = 1 to li_upper_bound
	// Build the string to reset text
	ls_auto_conv_Modify += "parm_" + String(li_counter) + "_t.Text = '' " + &
					"parm_" + String(li_counter) + "_t.Visible = 0 " + &
					"min_" + String(li_counter) + "_t.Visible = 0 " + &
					"max_" + String(li_counter) + "_t.Visible = 0 " + &
					"min_" + String(li_counter) + ".Visible = 0 " + &
					"max_" + String(li_counter) + ".Visible = 0 " 
Next

tab_1.tabpage_auto_conv.dw_auto_conv.Modify(ls_auto_conv_Modify)
ii_column_number_auto_conv = 0
istr_date_range_auto_conv = lstr_date_range_reset

return
end subroutine

public function boolean wf_long_term_auto_conv ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

//u_base_dw_ext	ldw_date_sort
DataStore			ldw_date_sort

Date		ldt_begin, &
			ldt_end, &
			ldt_header_start_date

Int		li_counter, &
			li_sub_counter, &
			li_ret, &
			li_temp

Long		ll_row_count, &
			ll_sort_row_Count

String	ls_temp, &
			ls_temp2, &
			ls_ret, &
			ls_auto_conv_TitleModify


SetPointer(HourGlass!)

ii_start_column_auto_conv = 1
dnv_auto_conv.SetSort("begin_date A, end_date A")
dnv_auto_conv.Sort()

/******************************************************
**	Create dynamic datawindow to hold all unique dates
** Fill in all dates from dnv_auto_conv
******************************************************/

//This.OpenUserObject(ldw_date_sort, 'u_base_dw_ext')
//ldw_date_sort.Visible = False

ldw_date_sort = Create DataStore
ldw_date_sort.DataObject = 'd_pas_instances_date_sort'

ll_row_count = dnv_auto_conv.RowCount()
ll_sort_row_count = 0

ldt_header_start_date = dw_header.GetItemDate(1, "begin_date")

For li_counter = 1 to ll_row_count
	// check end date first.  If End date is less than begin date, then discard row
	ls_temp = String(dnv_auto_conv.GetItemDate(li_counter, "end_date"), "yyyy-mm-dd")

	If Date(ls_temp) < ldt_header_start_date Then Continue

	If ldw_date_sort.Find("Left(date_col, 10) = '" + ls_temp + &
								"' And Right(date_col, 1) = 'E'", 1, ll_sort_row_count) = 0 Then
		ldw_date_sort.ImportString(ls_temp + 'E~r~n')
		ll_sort_row_count ++
	End if

	ls_temp = String(dnv_auto_conv.GetItemDate(li_counter, "begin_date"), "yyyy-mm-dd")
	If Date(ls_temp) < ldt_header_start_Date Then 
		ls_temp = String(ldt_header_start_date, "yyyy-mm-dd")
	End if

	If ldw_date_sort.Find("Left(date_col, 10) = '" + ls_temp + &
								"' And Right(date_col, 1) = 'B'", 1, ll_sort_row_count) = 0 Then
		ldw_date_sort.ImportString(ls_temp + 'B~r~n')
		ll_sort_row_count ++
	End if
Next

li_ret = ldw_date_sort.Sort()
ls_temp = ldw_date_sort.Object.DataWindow.Data

/**********************************************
**  Get date range pairs and create columns
**********************************************/
ll_row_count = ldw_date_sort.RowCount()
ls_auto_conv_TitleModify = ''

for li_counter = 1 to ll_row_count
	ls_temp = ldw_date_sort.GetItemString(li_counter, "date_col")
	If Right(ls_temp, 1) = 'B' Then
		ldt_begin = Date(Left(ls_temp, 10))
		If li_counter < ll_row_count Then
			ls_temp2 = ldw_date_sort.GetItemString(li_counter + 1, "date_col")
			If Right(ls_temp2, 1) = 'B' Then
				ldt_end = RelativeDate(Date(Left( ls_temp2, 10)), -1)
			Else
				ldt_end = Date(Left(ls_temp2, 10))
			End if
		Else
			Continue
		End if
	Else
		ldt_end = Date(Left(ls_temp, 10))
		If li_counter > 1 Then
			ls_temp2 = ldw_date_sort.GetItemString(li_counter - 1, "date_col")
			If Right(ls_temp2, 1) = 'E' Then
				ldt_begin = RelativeDate(Date(Left(ls_temp2, 10)), 1)
			Else
				// picked up this date range on last loop
				Continue
			End if
		Else
			Continue
		End if
	End if
	ii_column_number_auto_conv ++
	If ii_column_number_auto_conv > UpperBound(istr_date_range_auto_conv) Then
		ii_column_number_auto_conv --
		li_temp = UpperBound(istr_date_range_auto_conv)
		MessageBox("Date Range", "Maximum Number of Date Ranges exceeded.  " + &
				"Only the first " + String(li_temp) + " Date Ranges will be displayed.  " + &
				"Contact Applications Programming.")
		Exit
	End if		
	istr_date_range_auto_conv[ii_column_number_auto_conv].begin_date = ldt_begin 
	istr_date_range_auto_conv[ii_column_number_auto_conv].end_date   = ldt_end 

	ls_auto_conv_TitleModify += "parm_" + String(ii_column_number_auto_conv) + "_t.Text = '" + &
							String(ldt_begin, "mm/dd/yy") + "~~r~~n" + &
							String(ldt_end, "mm/dd/yy") + "' " + &
							"parm_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
							"min_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
							"max_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
							"min_" + String(ii_column_number_auto_conv) + ".Visible = 1 " + &
							"max_" + String(ii_column_number_auto_conv) + ".Visible = 1 "
Next

ls_ret = tab_1.tabpage_auto_conv.dw_auto_conv.Modify(ls_auto_conv_TitleModify)
If Len(ls_ret) > 0 Then
	MessageBox("Modify", ls_ret)
End if

// this might miss date ranges for instances that are very long term
for li_counter = 1 to ii_column_number_auto_conv - 1
	If DaysAfter(istr_date_range_auto_conv[li_counter].end_date, &
			istr_date_range_auto_conv[li_counter + 1].begin_date) <> 1 Then
		// these are not successive date ranges, check to see if anything needs it
		ll_row_count = dnv_auto_conv.RowCount()
		for li_sub_counter = 1 to ll_row_count
			If dnv_auto_conv.GetItemDate(li_sub_counter, "begin_date") < &
					istr_date_range_auto_conv[li_counter].end_date And &
					dnv_auto_conv.GetItemDate(li_sub_counter, "end_date") > &
					istr_date_range_auto_conv[li_counter + 1].begin_date Then
				wf_add_date_range_parm(RelativeDate(istr_date_range_auto_conv[li_counter].end_date, 1), &
											RelativeDate(istr_date_range_auto_conv[li_counter + 1].begin_date, -1))
				Exit
			End if
		Next
	End if
Next

//This.PostEvent("ue_CloseUserObject")
Destroy ldw_date_sort

If tab_1.SelectedTab = 2 Then
	If Not ib_protection_status then
		iw_frame.im_menu.mf_Enable('m_addrow')
	Else
		iw_frame.im_menu.mf_disable('m_addrow')
	End If
End If

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

return True
end function

public function boolean wf_daily_auto_conv ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp

Int	li_counter, &
		li_days_after

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_Auto_Conv_TitleModify


SetPointer(HourGlass!)

ii_start_column_auto_conv = 1
ii_column_number_auto_conv = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_Auto_Conv_TitleModify = ''

For li_counter = 0 to 6
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number_auto_conv ++
	istr_date_range_auto_conv[ii_column_number_auto_conv].begin_date = ldt_temp 
	istr_date_range_auto_conv[ii_column_number_auto_conv].end_date   = ldt_temp 

	ls_Auto_Conv_TitleModify += "parm_" + String(ii_column_number_auto_conv) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yyyy") + "~~r~~n" + DayName(ldt_temp) + "' " + &
						"parm_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"max_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_auto_conv) + ".Visible = 1 " + &
						"max_" + String(ii_column_number_auto_conv) + ".Visible = 1 " 
Next	

tab_1.tabpage_auto_conv.dw_auto_conv.Modify(ls_Auto_Conv_TitleModify)

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')

return True
end function

public function boolean wf_next ();Integer	li_instance_row, li_instance_col, li_parameters_row, li_parameters_col, li_auto_conv_row, li_auto_conv_col

Long		ll_xpos, ll_ypos

If dw_header.GetItemString(1, "display_status") = "D" Then
	iw_frame.SetMicroHelp("Inquiring on Next Week")
	dw_header.SetItem(1, "begin_date", RelativeDate(dw_header.GetItemDate(1, "begin_date"), 7))
Else	
	If dw_header.GetItemString(1, "display_status") = "T" Then
		iw_frame.SetMicroHelp("Inquiring on Next 2 Weeks")
		dw_header.SetItem(1, "begin_date", RelativeDate(dw_header.GetItemDate(1, "begin_date"), 14))
	Else
		If dw_header.GetItemString(1, "display_status") = "H" Then
			iw_frame.SetMicroHelp("Inquiring on Next 3 Weeks")
			dw_header.SetItem(1, "begin_date", RelativeDate(dw_header.GetItemDate(1, "begin_date"), 21))		
		End If
	End If
End If

li_instance_row = This.Tab_1.tabpage_inst.dw_instances.GetRow() 
li_instance_col = This.Tab_1.tabpage_inst.dw_instances.GetColumn() 
li_parameters_row = This.Tab_1.tabpage_inst_parm.dw_parameters.GetRow()
li_parameters_col = This.Tab_1.tabpage_inst_parm.dw_parameters.GetColumn()
li_auto_conv_row = This.Tab_1.tabpage_auto_conv.dw_auto_conv.GetRow()
li_auto_conv_col = This.Tab_1.tabpage_auto_conv.dw_auto_conv.GetColumn()


If ib_Changes_Made Then
	ib_reinquire_necessary = True
//	This.post wf_retrieve()
	This.wf_retrieve()
End If

dnv_instances.Reset()
dnv_instances.ImportString(ids_original_instances.Object.DataWindow.Data)

If dw_header.GetItemString(1, "display_status") = "D" Then
	This.Tab_1.tabpage_inst.dw_instances.Event ue_next_week()
	This.Tab_1.tabpage_inst_parm.dw_parameters.Event ue_next_week()
	This.Tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_next_week()
Else
	If dw_header.GetItemString(1, "display_status") = "T" Then
		This.Tab_1.tabpage_inst.dw_instances.Event ue_next_two_weeks()
		This.Tab_1.tabpage_inst_parm.dw_parameters.Event ue_next_two_weeks()
		This.Tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_next_two_weeks()
	Else	
		If dw_header.GetItemString(1, "display_status") = "H" Then
			This.Tab_1.tabpage_inst.dw_instances.Event ue_next_three_weeks()
			This.Tab_1.tabpage_inst_parm.dw_parameters.Event ue_next_three_weeks()
			This.Tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_next_three_weeks()
		End If
	End If
End If

tab_1.tabpage_inst.dw_instances.Event ue_set_hspscroll()

if (tab_1.SelectedTab = 1) and (li_instance_row > 0) and (li_instance_col > 0) Then
	If tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", 1, 10000) > 0 then
		ll_xpos = ((li_instance_col - 6) * 55) + 231
	else
		ll_xpos = ((li_instance_col - 6) * 55) + 200
	end if
	ll_ypos = (li_instance_row * 19) + 42	
	wf_rightsideclicked(ll_xpos, ll_ypos, "dw_instances")
end if

tab_1.tabpage_inst_parm.dw_parameters.Event ue_set_hspscroll()

if (tab_1.SelectedTab = 2) and (li_parameters_row > 0) and (li_parameters_col > 0) Then
	if (li_parameters_col = 6) or (li_parameters_col = 68) or (li_parameters_col = 69) then
		tab_1.tabpage_inst_parm.dw_parameters.SetFocus()
		tab_1.tabpage_inst_parm.dw_parameters.SetRow(li_parameters_row)
		tab_1.tabpage_inst_parm.dw_parameters.SetColumn(li_parameters_col)
	else
		If tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", 1, 10000) > 0 then
			ll_xpos = ((li_parameters_col - 6) * 45) + 348
			ll_ypos = (li_parameters_row * 18) + 37	
			wf_rightsideclicked(ll_xpos, ll_ypos, "dw_parameters")
		else
			ll_xpos = ((li_parameters_col - 6) * 45) + 303
			ll_ypos = (li_parameters_row * 18) + 37	
			wf_rightsideclicked(ll_xpos, ll_ypos, "dw_parameters")				
		end if
	end if
end if		

tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_set_hspscroll()

if (tab_1.SelectedTab = 3) and (li_auto_conv_row > 0) and (li_auto_conv_col > 0) Then
	if (li_auto_conv_col = 6) or (li_auto_conv_col = 68) or (li_auto_conv_col = 69) then
		tab_1.tabpage_auto_conv.dw_auto_conv.SetFocus()
		tab_1.tabpage_auto_conv.dw_auto_conv.SetRow(li_auto_conv_row)
		tab_1.tabpage_auto_conv.dw_auto_conv.SetColumn(li_auto_conv_col)
	else
		If tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", 1, 10000) > 0 then
			ll_xpos = ((li_auto_conv_col - 6) * 45) + 348
			ll_ypos = (li_auto_conv_row * 18) + 37	
			wf_rightsideclicked(ll_xpos, ll_ypos, "dw_auto_conv")
		else
			ll_xpos = ((li_auto_conv_col - 6) * 45) + 303
			ll_ypos = (li_auto_conv_row * 18) + 37	
			wf_rightsideclicked(ll_xpos, ll_ypos, "dw_auto_conv")				
		end if
	end if
end if		

wf_shade_rows()	
	
return True
end function

public function boolean wf_previous ();Integer	li_instance_row, li_instance_col, li_parameters_row, li_parameters_col, li_auto_conv_row, li_auto_conv_col

Long		ll_xpos, ll_ypos

Date	ldt_begin, &
		ldt_PastDate


ldt_PastDate = RelativeDate(Today(), 0 - ii_PaPast)
ldt_begin = dw_header.GetItemDate(1, "begin_date")

If ldt_begin <= ldt_PastDate Then
	iw_frame.SetMicroHelp("Begin Date cannot be less than " + &
				String(ldt_PastDate, "mm/dd/yyyy"))
	return False
End if

If DaysAfter(ldt_PastDate, ldt_begin) < 7 Then
	If	MessageBox("Begin Date", "Begin Date cannot be less than " + &
						String(ldt_PastDate, "mm/dd/yyyy") + ".  " + &
					"Would you like to continue using this date as begin date?", Question!, &
					YesNo!) = 2 Then 
		return False
	End if
	ldt_begin = ldt_PastDate
Else
	If dw_header.GetItemString(1, "display_status") = "D" Then
		ldt_begin = RelativeDate(ldt_begin, -7)
	Else
		If dw_header.GetItemString(1, "display_status") = "T" Then
			ldt_begin = RelativeDate(ldt_begin, -14)
		Else
			ldt_begin = RelativeDate(ldt_begin, -21)
		End If
	End if
End if

If ldt_PastDate < idt_inquire_date Then
	// Need to reinquire to get older data
	ib_changes_made = True
End if

iw_frame.SetMicroHelp("Inquiring on Last Week")

dw_header.SetItem(1, "begin_date", ldt_begin)

li_instance_row = This.Tab_1.tabpage_inst.dw_instances.GetRow() 
li_instance_col = This.Tab_1.tabpage_inst.dw_instances.GetColumn() 
li_parameters_row = This.Tab_1.tabpage_inst_parm.dw_parameters.GetRow()
li_parameters_col = This.Tab_1.tabpage_inst_parm.dw_parameters.GetColumn()
li_auto_conv_row = This.Tab_1.tabpage_auto_conv.dw_auto_conv.GetRow()
li_auto_conv_col = This.Tab_1.tabpage_auto_conv.dw_auto_conv.GetColumn()

If ib_Changes_Made Then
	ib_reinquire_necessary = True
//	This.post wf_retrieve()
	This.wf_retrieve()
End If

dnv_instances.Reset()
dnv_instances.ImportString(ids_original_instances.Object.DataWindow.Data)

If dw_header.GetItemString(1, "display_status") = "D" Then
	This.Tab_1.tabpage_inst.dw_instances.Event ue_prev_week()
	This.Tab_1.tabpage_inst_parm.dw_parameters.Event ue_prev_week()
	This.Tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_prev_week()
Else
	If dw_header.GetItemString(1, "display_status") = "T" Then
		This.Tab_1.tabpage_inst.dw_instances.Event ue_prev_two_weeks()
		This.Tab_1.tabpage_inst_parm.dw_parameters.Event ue_prev_two_weeks()
		This.Tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_prev_two_weeks()	
	Else	
		This.Tab_1.tabpage_inst.dw_instances.Event ue_prev_three_weeks()
		This.Tab_1.tabpage_inst_parm.dw_parameters.Event ue_prev_three_weeks()
		This.Tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_prev_three_weeks()			
	End If
End If

tab_1.tabpage_inst.dw_instances.Event ue_set_hspscroll()

if (tab_1.SelectedTab = 1) and (li_instance_row > 0) and (li_instance_col > 0) Then
	If tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", 1, 10000) > 0 then
		ll_xpos = ((li_instance_col - 6) * 55) + 231
	else
		ll_xpos = ((li_instance_col - 6) * 55) + 200
	end if
	ll_ypos = (li_instance_row * 19) + 42	
	wf_rightsideclicked(ll_xpos, ll_ypos, "dw_instances")
end if

tab_1.tabpage_inst_parm.dw_parameters.Event ue_set_hspscroll()

if (tab_1.SelectedTab = 2) and (li_parameters_row > 0) and (li_parameters_col > 0) Then
	if (li_parameters_col = 6) or (li_parameters_col = 68) or (li_parameters_col = 69) then
		tab_1.tabpage_inst_parm.dw_parameters.SetFocus()
		tab_1.tabpage_inst_parm.dw_parameters.SetRow(li_parameters_row)
		tab_1.tabpage_inst_parm.dw_parameters.SetColumn(li_parameters_col)
	else
		If tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", 1, 10000) > 0 then
			ll_xpos = ((li_parameters_col - 6) * 45) + 348
			ll_ypos = (li_parameters_row * 18) + 37	
			wf_rightsideclicked(ll_xpos, ll_ypos, "dw_parameters")
		else
			ll_xpos = ((li_parameters_col - 6) * 45) + 303
			ll_ypos = (li_parameters_row * 18) + 37	
			wf_rightsideclicked(ll_xpos, ll_ypos, "dw_parameters")				
		end if
	end if
end if		

tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_set_hspscroll()

if (tab_1.SelectedTab = 3) and (li_auto_conv_row > 0) and (li_auto_conv_col > 0) Then
	if (li_auto_conv_col = 6) or (li_auto_conv_col = 68) or (li_auto_conv_col = 69) then
		tab_1.tabpage_auto_conv.dw_auto_conv.SetFocus()
		tab_1.tabpage_auto_conv.dw_auto_conv.SetRow(li_auto_conv_row)
		tab_1.tabpage_auto_conv.dw_auto_conv.SetColumn(li_auto_conv_col)
	else
		If tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", 1, 10000) > 0 then
			ll_xpos = ((li_auto_conv_col - 6) * 45) + 348
			ll_ypos = (li_auto_conv_row * 18) + 37	
			wf_rightsideclicked(ll_xpos, ll_ypos, "dw_auto_conv")
		else
			ll_xpos = ((li_auto_conv_col - 6) * 45) + 303
			ll_ypos = (li_auto_conv_row * 18) + 37	
			wf_rightsideclicked(ll_xpos, ll_ypos, "dw_auto_conv")				
		end if
	end if
end if	

wf_shade_rows()	

return True


end function

public function boolean wf_fill_values_auto_conv ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THIS IS FASTER

Date	ldt_begin, &
		ldt_end

Decimal	ld_min, &
			ld_max

Int	li_counter, &
		li_temp_step_seq, &
		li_sub_counter

Long	ll_row_count, &
		ll_nv_row_count, &
		ll_row, &
		ll_priority

String	ls_dest_plant, &
			ls_inst_dest_plant, &
			ls_temp, &
			ls_group, &
			ls_value[]


/*********************************************************
**  Set Values	
*********************************************************/
// this sort should make finding the instances a little quicker

dnv_auto_conv.SetSort("mfg_step_sequence A, dest_plant A")
dnv_auto_conv.Sort()
ll_nv_row_count = dnv_auto_conv.RowCount()
ll_row_count = tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()

// Find all of the unique step/dest_plant combinations and put rows in for them
For li_counter = 1 to ll_nv_row_count
	li_temp_step_seq = dnv_auto_conv.GetItemNumber(li_counter, "mfg_step_Sequence")
	ls_dest_plant = dnv_auto_conv.GetItemString(li_counter, "dest_plant")
	
	If IsNull(dnv_auto_conv.GetItemString(li_counter, "group")) Then
		ls_group = ''
	Else
		ls_group = dnv_auto_conv.GetItemString(li_counter, "group")
	End If
		
	If IsNull(dnv_auto_conv.GetItemNumber(li_counter, "priority")) Then
		ll_priority = 0
	Else
		ll_priority = dnv_auto_conv.GetItemNumber(li_counter, "priority")
	End If
	
	If iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then Continue
	ls_temp = "mfg_step_sequence = " + String(li_temp_step_seq) + &
								" And destination_plant = '" + ls_dest_plant + "'"
	If tab_1.tabpage_auto_conv.dw_auto_conv.Find(ls_temp, 1, ll_row_count) = 0 Then
		// not found, make sure there is not a row there with a blank dest plant
		ll_row = tab_1.tabpage_auto_conv.dw_auto_conv.Find("mfg_step_sequence = " + String(li_temp_step_seq), &
											1, ll_row_count)
		If ll_row = 0 Then
			// serious error here
			return false
		End if
		If iw_frame.iu_string.nf_IsEmpty(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemString(ll_row, "destination_plant")) Then
			tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(ll_row, "destination_plant", ls_dest_plant)
			tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(ll_row, "group", ls_group)
			tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(ll_row, "priority", ll_priority)
		Else
			// Need to duplicate the row
			tab_1.tabpage_auto_conv.dw_auto_conv.RowsCopy(ll_row, ll_row, Primary!, tab_1.tabpage_auto_conv.dw_auto_conv, ll_row, Primary!)
			tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(ll_row, "destination_plant", ls_dest_plant)
			tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(ll_row, "group", ls_group)
			tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(ll_row, "priority", ll_priority)
			ll_row_count = tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()
		End if
	End if
Next

tab_1.tabpage_auto_conv.dw_auto_conv.Sort()

For li_counter = 1 to ll_row_Count
	li_temp_step_seq = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(li_counter, &
			"mfg_step_Sequence")
	ls_dest_plant = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemString(li_counter, &
			"destination_plant")
	If IsNull(ls_dest_plant) Then 
		ls_inst_dest_plant = tab_1.tabpage_inst.dw_instances.GetItemString(li_counter, &
				'destination_plant')
		If iw_frame.iu_string.nf_IsEmpty(ls_inst_dest_plant) Then 
			ls_dest_plant = ""
		Else
			tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_counter, &
					"destination_plant", ls_inst_dest_plant)
		End IF
	End If			
	ll_row = 0
	Do
		ls_temp = "mfg_step_sequence = " + String(li_temp_step_seq)
		If Not iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then
			ls_temp += " And dest_plant = '" + ls_dest_plant + "'"
		End if
		ll_row = dnv_auto_conv.Find(ls_temp, ll_row + 1, ll_nv_row_count)
		If ll_row = 0 Then Continue
		ldt_begin = dnv_auto_conv.GetItemDate(ll_row, "begin_date")
		ldt_end =   dnv_auto_conv.GetItemDate(ll_row, "end_date")
		
		If IsNull(dnv_auto_conv.GetItemString(ll_row, "group")) Then
			ls_group = ''
		Else
			ls_group = dnv_auto_conv.GetItemString(ll_row, "group")
		End If
		
		If IsNull(dnv_auto_conv.GetItemNumber(ll_row, "priority")) Then
			ll_priority = 0
		Else
			ll_priority = dnv_auto_conv.GetItemNumber(ll_row, "priority")
		End If
		
		// This is to correct a bug in PB 5.0.1 doing GetItemDecimal
		// remove this "if" when the bug is corrected
		If IsNull(dnv_auto_conv.GetItemNumber(ll_row, "min")) Then 
			ld_min = 0.00
		Else
			ld_min =  dnv_auto_conv.GetItemDecimal(ll_row, "min")
		End if
		If IsNull(dnv_auto_conv.GetItemNumber(ll_row, "max")) Then 
			ld_max = 0.00
		Else
			ld_max =  dnv_auto_conv.GetItemDecimal(ll_row, "max")
		End if

		For li_sub_counter = 1 to ii_column_number_auto_conv
			If li_sub_counter > UpperBound(istr_date_range_auto_conv) Then Exit
			If ldt_begin <= istr_date_range_auto_conv[li_sub_counter].end_date AND &
						ldt_end >= istr_date_range_auto_conv[li_sub_counter].begin_date Then
					tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_counter, 2 * li_sub_counter + 7, ld_min)
					tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_counter, 2 * li_sub_counter + 8, ld_max)
			End if
		Next
		
		tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_counter, "group", ls_group)
		tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_counter, "priority", ll_priority)

	Loop While ll_row < ll_nv_row_count and ll_row > 0
Next

For li_counter = 1 to tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()
	If ib_protection_status or ib_date_protection Then
		tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_counter, 'protection_status', &
				'Y')
	Else
		tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_counter, 'protection_status', &
				'N')
	End If
Next		
	
tab_1.tabpage_auto_conv.dw_auto_conv.ResetUpdate()

return True
end function

public function boolean wf_fill_values_parm ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THIS IS FASTER

Date	ldt_begin, &
		ldt_end

Decimal	ld_min, &
			ld_max

Int	li_counter, &
		li_temp_step_seq, &
		li_sub_counter

Long	ll_row_count, &
		ll_nv_row_count, &
		ll_row, &
		ll_priority

String	ls_dest_plant, &
			ls_inst_dest_plant, &
			ls_temp, &
			ls_group, &
			ls_value[]


/*********************************************************
**  Set Values	
*********************************************************/
// this sort should make finding the instances a little quicker

dnv_parameters.SetSort("mfg_step_sequence A, dest_plant A")
dnv_parameters.Sort()
ll_nv_row_count = dnv_parameters.RowCount()
ll_row_count = tab_1.tabpage_inst_parm.dw_parameters.RowCount()

ls_temp = dnv_parameters.Object.DataWindow.Data

ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

// Find all of the unique step/dest_plant combinations and put rows in for them
For li_counter = 1 to ll_nv_row_count
	li_temp_step_seq = dnv_parameters.GetItemNumber(li_counter, "mfg_step_Sequence")
	ls_dest_plant = dnv_parameters.GetItemString(li_counter, "dest_plant")
	
	If IsNull(dnv_parameters.GetItemString(li_counter, "group")) Then
		ls_group = ''
	Else
		ls_group = dnv_parameters.GetItemString(li_counter, "group")
	End If
		
	If IsNull(dnv_parameters.GetItemNumber(li_counter, "priority")) Then
		ll_priority = 0
	Else
		ll_priority = dnv_parameters.GetItemNumber(li_counter, "priority")
	End If	
	
	If iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then Continue
	ls_temp = "mfg_step_sequence = " + String(li_temp_step_seq) + &
								" And destination_plant = '" + ls_dest_plant + "'"
	If tab_1.tabpage_inst_parm.dw_parameters.Find(ls_temp, 1, ll_row_count) = 0 Then
		// not found, make sure there is not a row there with a blank dest plant
		ll_row = tab_1.tabpage_inst_parm.dw_parameters.Find("mfg_step_sequence = " + String(li_temp_step_seq), &
											1, ll_row_count)
		If ll_row = 0 Then
			// serious error here
			return false
		End if
		If iw_frame.iu_string.nf_IsEmpty(tab_1.tabpage_inst_parm.dw_parameters.GetItemString(ll_row, "destination_plant")) Then
			tab_1.tabpage_inst_parm.dw_parameters.SetItem(ll_row, "destination_plant", ls_dest_plant)
			tab_1.tabpage_inst_parm.dw_parameters.SetItem(ll_row, "group", ls_group)
			tab_1.tabpage_inst_parm.dw_parameters.SetItem(ll_row, "priority", ll_priority)
		Else
			// Need to duplicate the row
			tab_1.tabpage_inst_parm.dw_parameters.RowsCopy(ll_row, ll_row, Primary!, tab_1.tabpage_inst_parm.dw_parameters, ll_row, Primary!)
			tab_1.tabpage_inst_parm.dw_parameters.SetItem(ll_row, "destination_plant", ls_dest_plant)
			tab_1.tabpage_inst_parm.dw_parameters.SetItem(ll_row, "group", ls_group)
			tab_1.tabpage_inst_parm.dw_parameters.SetItem(ll_row, "priority", ll_priority)
			ll_row_count = tab_1.tabpage_inst_parm.dw_parameters.RowCount()
		End if
	End if
Next

ls_temp = dnv_parameters.Object.DataWindow.Data

tab_1.tabpage_inst_parm.dw_parameters.Sort()

For li_counter = 1 to ll_row_Count
	li_temp_step_seq = tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(li_counter, &
			"mfg_step_Sequence")
	ls_dest_plant = tab_1.tabpage_inst_parm.dw_parameters.GetItemString(li_counter, &
			"destination_plant")
	If IsNull(ls_dest_plant) Then 
		ls_inst_dest_plant = tab_1.tabpage_inst.dw_instances.GetItemString(li_counter, &
				'destination_plant')
		If iw_frame.iu_string.nf_IsEmpty(ls_inst_dest_plant) Then 
			ls_dest_plant = ""
		Else
			tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_counter, &
					"destination_plant", ls_inst_dest_plant)
		End IF
	End If			
	ll_row = 0
	Do
		ls_temp = "mfg_step_sequence = " + String(li_temp_step_seq)
		If Not iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then
			ls_temp += " And dest_plant = '" + ls_dest_plant + "'"
		End if
		ll_row = dnv_parameters.Find(ls_temp, ll_row + 1, ll_nv_row_count)
		If ll_row = 0 Then Continue
		ldt_begin = dnv_parameters.GetItemDate(ll_row, "begin_date")
		ldt_end =   dnv_parameters.GetItemDate(ll_row, "end_date")
		
		If IsNull(dnv_parameters.GetItemString(ll_row, "group")) Then
			ls_group = ''
		Else
			ls_group = dnv_parameters.GetItemString(ll_row, "group")
		End If
		
		If IsNull(dnv_parameters.GetItemNumber(ll_row, "priority")) Then
			ll_priority = 0
		Else
			ll_priority = dnv_parameters.GetItemNumber(ll_row, "priority")
		End If
		
		// This is to correct a bug in PB 5.0.1 doing GetItemDecimal
		// remove this "if" when the bug is corrected
		If IsNull(dnv_parameters.GetItemNumber(ll_row, "min")) Then 
			ld_min = 0.00
		Else
			ld_min =  dnv_parameters.GetItemDecimal(ll_row, "min")
		End if
		If IsNull(dnv_parameters.GetItemNumber(ll_row, "max")) Then 
			ld_max = 0.00
		Else
			ld_max =  dnv_parameters.GetItemDecimal(ll_row, "max")
		End if

		For li_sub_counter = 1 to ii_column_number_parm
			If li_sub_counter > UpperBound(istr_date_range_parm) Then Exit
			If ldt_begin <= istr_date_range_parm[li_sub_counter].end_date AND &
						ldt_end >= istr_date_range_parm[li_sub_counter].begin_date Then
					tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_counter, 2 * li_sub_counter + 7, ld_min)
					tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_counter, 2 * li_sub_counter + 8, ld_max)
			End if
		Next
		
		tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_counter, "group", ls_group)
		tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_counter, "priority", ll_priority)
	
	Loop While ll_row < ll_nv_row_count and ll_row > 0
Next

For li_counter = 1 to tab_1.tabpage_inst_parm.dw_parameters.RowCount()
	If ib_protection_status or ib_date_protection Then
		tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_counter, 'protection_status', &
				'Y')
	Else
		tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_counter, 'protection_status', &
				'N')
	End If
Next		
	
tab_1.tabpage_inst_parm.dw_parameters.ResetUpdate()

return True
end function

public function boolean wf_addrow ();If tab_1.SelectedTab = 1 Then
	This.tab_1.tabpage_inst.dw_instances.Event ue_addrow()
Else
	If tab_1.SelectedTab = 2 Then
		This.tab_1.tabpage_inst_parm.dw_parameters.Event ue_addrow()
	Else
		This.tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_addrow()
	End If
End If

return True



end function

public function boolean wf_deleterow ();// This really duplicates a transfer step, not deletes a row
Long	ll_current_row

String	ls_data, &
			ls_temp


If This.tab_1.SelectedTab = 1 Then
	This.Tab_1.TabPage_inst.dw_instances.Event ue_duplicate_transfer()
Else
	If This.tab_1.SelectedTab = 2 Then
		This.Tab_1.TabPage_inst_parm.dw_parameters.Event ue_duplicate_transfer()
	Else
		This.Tab_1.TabPage_auto_conv.dw_auto_conv.Event ue_duplicate_transfer()
	End IF
End If

return false
end function

public function integer wf_add_date_range_parm (date adt_begin, date adt_end);Boolean	lb_new_column

Date		ldt_temp

Int	li_counter, &
		li_sub_counter, &
		li_new_column_number, &
		li_row_counter, &
		li_num_tabs

Long	ll_row_count, &
		ll_pos1, &
		ll_pos2, ll_temp

str_date_range	lstr_temp_range[]

String	ls_data, &
			ls_TitleModify, &
			ls_qty_field, &
			ls_value1, &
			ls_value2, &
			ls_xx, &
			ls_xx2, &
			ls_orig_col_name1, &
			ls_orig_col_name2, &
			ls_new_col_name1, &
			ls_new_col_name2

Decimal	ld_value1, &
			ld_value2

DataStore 			lds_original_data
str_date_range		lstr_original_date_range[]

lds_original_data = Create DataStore
lds_original_data.DataObject = "d_pas_inst_parms"

lstr_original_date_range = istr_date_range_parm

If ii_column_number_parm > UpperBound(istr_date_range_parm) - 1 Then return -1

If adt_Begin < idt_inquire_date Then
	MessageBox("Inquire Date", "New date range cannot be before the inquired Begin Date.  " + &
										"Please reinquire with an earlier Begin Date.")
	return -1
End if

SetPointer(HourGlass!)
This.SetRedraw(False)
ii_start_column_parm = ii_column_number_parm

ls_data = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data
If Right(ls_data, 2) <> '~r~n' Then
	ls_data += '~r~n'
End if
ll_row_count = tab_1.tabpage_inst_parm.dw_parameters.RowCount()

lds_original_data.Reset()
lds_original_data.ImportString(ls_data)

//  Set li_num_tabs to the number of tabs before the 1st quantity column
//  If column 9 is the 1st quantity column, set li_num_tabs = 8
li_num_tabs = 8

For li_counter = 1 to ii_column_number_parm
	If adt_begin <= istr_date_range_parm[li_counter].begin_date And &
			adt_end >= istr_date_range_parm[li_counter].end_date Then
		If Not lb_new_column Then
			istr_date_range_parm[li_counter].begin_date = adt_begin
			istr_date_range_parm[li_counter].end_date = adt_end
			istr_date_range_parm[li_counter].new_column = True
			lb_new_column = True
			li_new_column_number = li_counter

			// need to loop through the whole string to remove values in that column
			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					// li_counter * 2) + 6 is the first quantity_? column
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
//					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1 - 1, '~t')
//					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
				
					ls_qty_field = "min_" + string(li_counter + 1)
					ld_value1 = tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(li_row_counter, ls_qty_field)
					ls_qty_field = "max_" + string(li_counter + 1)
					ld_value2 = tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(li_row_counter, ls_qty_field)					
					if isnull(ld_value1) then
						ls_value1 = ''
					else
						ls_value1 = string(ld_value1)
					end if
					if isnull(ld_value2) then
						ls_value2 = ''
					else
						ls_value2 = string(ld_value2)
					end if					
					
					ls_xx = mid(ls_data, 1, ll_pos1) + ls_value1 + '~t' + ls_value2
					ls_xx2 = mid(ls_data, ll_pos2) 	
					ls_data = ls_xx + ls_xx2					
					
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)					
			Next
			
			Continue
		Else
			// this column is not to be used anymore
			// loop through the string and replace those values
			ii_column_number_parm --
			for li_sub_counter = li_counter to ii_column_number_parm
				istr_date_range_parm[li_sub_counter] = istr_date_range_parm[li_sub_counter + 1]
				
			Next
			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
					
					ls_qty_field = "min_" + string(li_counter + 1)
					ld_value1 = tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(li_row_counter, ls_qty_field)
					ls_qty_field = "max_" + string(li_counter + 1)
					ld_value2 = tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(li_row_counter, ls_qty_field)					
					if isnull(ld_value1) then
						ls_value1 = ''
					else
						ls_value1 = string(ld_value1)
					end if
					if isnull(ld_value2) then
						ls_value2 = ''
					else
						ls_value2 = string(ld_value2)
					end if					
					
					ls_xx = mid(ls_data, 1, ll_pos1) + ls_value1 + '~t' + ls_value2
					ls_xx2 = mid(ls_data, ll_pos2) 	
					ls_data = ls_xx + ls_xx2					
					
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			// I just cleared out the data for this loop, now need to decrement the counter to
			// check the new data in that column
			li_counter --

			Continue
		End if
	End if
	// If the column goes in the middle of this, then it will turn into three cols
	If adt_begin > istr_date_range_parm[li_counter].begin_date And &
			adt_end < istr_date_range_parm[li_counter].end_date Then
		If Not lb_new_column Then
			// This should always be, but just in case...
			ldt_temp = istr_date_range_parm[li_counter].end_date
			istr_date_range_parm[li_counter].end_date = RelativeDate(adt_begin, -1)
			istr_date_range_parm[li_counter].new_column = True

			// about to insert two columns
			for li_sub_counter = ii_column_number_parm + 2 to li_counter + 2 step -1
				istr_date_range_parm[li_sub_counter] = istr_date_range_parm[li_sub_counter - 2]
			Next
			istr_date_range_parm[li_counter + 1].begin_date = adt_begin
			istr_date_range_parm[li_counter + 1].end_date = adt_end
			istr_date_range_parm[li_counter + 1].new_column = True

			istr_date_range_parm[li_counter + 2].begin_date = RelativeDate(adt_end, 1)
			istr_date_range_parm[li_counter + 2].end_date = ldt_temp
			istr_date_range_parm[li_counter + 2].new_column = True

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1, &
									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1) + '~t~t' + &
									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1))
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			li_new_column_number = li_counter + 1
			li_counter += 2 
			ii_column_number_parm += 2
			lb_new_column = True	

		End if
	End if

	If adt_begin <= istr_date_range_parm[li_counter].end_date And &
			adt_begin > istr_date_range_parm[li_counter].begin_date Then
		istr_date_range_parm[li_counter].end_date = RelativeDate(adt_begin, -1)
		istr_date_range_parm[li_counter].new_column = True

		If Not lb_new_column Then
			for li_sub_counter = ii_column_number_parm to li_counter step -1
				istr_date_range_parm[li_sub_counter + 1] = istr_date_range_parm[li_sub_counter]
			Next
			istr_date_range_parm[li_counter + 1].begin_date = adt_begin
			istr_date_range_parm[li_counter + 1].end_date = adt_end
			istr_date_range_parm[li_counter + 1].new_column = True

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1, &
									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1))
//									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1) + '~t~t')
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			lb_new_column = True
			li_counter ++
			ii_column_number_parm ++
			li_new_column_number = li_counter
		End if
		Continue
	End if
	If adt_end >= istr_date_range_parm[li_counter].begin_date And &
			adt_end <  istr_date_range_parm[li_counter].end_date Then
		istr_date_range_parm[li_counter].begin_date = RelativeDate(adt_end, 1)
		istr_date_range_parm[li_counter].new_column = True

		If Not lb_new_column Then
			for li_sub_counter = ii_column_number_parm + 1 to li_counter + 1 step -1
				istr_date_range_parm[li_sub_counter] = istr_date_range_parm[li_sub_counter - 1]
			Next
			istr_date_range_parm[li_counter].begin_date = adt_begin
			istr_date_range_parm[li_counter].end_date = adt_end

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1, &
									 + Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1))
//									'~t~t' + Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1))
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			lb_new_column = True
			li_new_column_number = li_counter
			ii_column_number_parm ++
		End if
	End if
Next

If Not lb_new_column Then
	// this date range is for some hole in the dates
	for li_counter = 1 to ii_column_number_parm - 1
		if istr_date_range_parm[li_counter].end_date < adt_begin And &
				istr_date_range_parm[li_counter + 1].begin_date > adt_end Then
			ii_column_number_parm ++
			For li_sub_counter = ii_column_number_parm to li_counter + 1 step -1
				// Shift all dates 1 to the right
				istr_date_range_parm[li_sub_counter] = istr_date_range_parm[li_sub_counter - 1]
			Next

			istr_date_range_parm[li_counter + 1].begin_date = adt_begin
			istr_date_range_parm[li_counter + 1].end_date = adt_end
			istr_date_range_parm[li_counter + 1].new_column = True

			lb_new_column = True
			li_new_column_number = li_counter
			Exit
		End if
	Next
End if

If Not lb_new_column Then
	If adt_end < istr_date_range_parm[1].begin_date Then
		// goes before all other columns, Shift all one to the right
		ii_column_number_parm ++
		For li_counter = ii_column_number_parm to 2 Step -1
			istr_date_range_parm[li_counter] = istr_date_range_parm[li_counter - 1]
		Next
		istr_date_range_parm[1].begin_date = adt_begin
		istr_date_range_parm[1].end_date = adt_end
		istr_date_range_parm[1].new_column = True

		ll_pos1 = 0
		for li_row_counter = 1 to ll_row_count
			// insert a tab in the place of the first column
			ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
			ls_data = Replace(ls_data, ll_pos1 + 1, 0, '~t~t')
			ll_pos1 = Pos(ls_data, '~r~n', ll_pos1)
		Next
		lb_new_column = True
		li_new_column_number = 1
		
			
	Else
		// must go at the end
		ii_column_number_parm ++
		istr_date_range_parm[ii_column_number_parm].begin_date = adt_begin
		istr_date_range_parm[ii_column_number_parm].end_date = adt_end
		istr_date_range_parm[ii_column_number_parm].new_column = True
		lb_new_column = True
		li_new_column_number = ii_column_number_parm
	End if
End if

ls_TitleModify = ''
For li_Counter = 1 to ii_column_number_parm
	ls_TitleModify += "parm_" + String(li_counter) + "_t.Text = '" + &
			String(istr_date_range_parm[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
			String(istr_date_range_parm[li_Counter].end_date, "mm/dd/yy") + "' " + &
			"parm_" + String(li_Counter) + "_t.Visible = 1 " + &
			"min_" + String(li_counter) + "_t.Visible = 1 " + &
			"max_" + String(li_counter) + "_t.Visible = 1 " + &
			"min_" + String(li_counter) + ".Visible = 1 " + &
			"max_" + String(li_counter) + ".Visible = 1 " 

Next

// wf_reset_Title_Text_parm will clear out ii_column_number_parm and istr_date_range_parm
li_counter = ii_column_number_parm
lstr_temp_range = istr_date_range_parm
wf_Reset_Title_Text_parm()
ii_column_number_parm = li_counter
istr_date_range_parm = lstr_temp_range

tab_1.tabpage_inst_parm.dw_parameters.Modify(ls_TitleModify)

tab_1.tabpage_inst_parm.dw_parameters.Reset()
If ll_row_count > 0 Then
	// If there are no rows, ls_data just turned to garbage, so don't re import it
	tab_1.tabpage_inst_parm.dw_parameters.ImportString(ls_data)
End if

For li_counter = 1 to 30
	If istr_date_range_parm[li_counter].end_date >=adt_end Then
		For li_sub_counter = 1 to 30
			If istr_date_range_parm[li_counter].end_date = lstr_original_date_range[li_sub_counter].end_date Then
				ls_orig_col_name1 = 'min_' + string(li_sub_counter)
				ls_orig_col_name2 = 'max_' + string(li_sub_counter)				
				ls_new_col_name1 = 'min_' + string(li_counter)
				ls_new_col_name2 = 'max_' + string(li_counter)				
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_row_counter, ls_new_col_name1, lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name1))
					tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_row_counter, ls_new_col_name2, lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name2))					
				Next
			End If
		Next
	End if
Next


This.tab_1.tabpage_inst_parm.dw_parameters.Event ue_Set_HSpScroll()

This.SetRedraw(True)
tab_1.tabpage_inst_parm.dw_parameters.SetColumn(li_new_column_number + 8)
return li_new_column_number


end function

public function integer wf_add_date_range_auto_conv (date adt_begin, date adt_end);Boolean	lb_new_column

Date		ldt_temp

Int	li_counter, &
		li_sub_counter, &
		li_new_column_number, &
		li_row_counter, &
		li_num_tabs

Long	ll_row_count, &
		ll_pos1, &
		ll_pos2, ll_temp

str_date_range	lstr_temp_range[]

String	ls_data, &
			ls_TitleModify, &
			ls_qty_field, &
			ls_value1, &
			ls_value2, &
			ls_xx, &
			ls_xx2, &
			ls_orig_col_name1, &
			ls_orig_col_name2, &
			ls_new_col_name1, &
			ls_new_col_name2
			

Decimal	ld_value1, &
			ld_value2			


DataStore 			lds_original_data
str_date_range		lstr_original_date_range[]

lds_original_data = Create DataStore
lds_original_data.DataObject = "d_pas_auto_conv_parms"

lstr_original_date_range = istr_date_range_auto_conv

If ii_column_number_auto_conv > UpperBound(istr_date_range_auto_conv) - 1 Then return -1

If adt_Begin < idt_inquire_date Then
	MessageBox("Inquire Date", "New date range cannot be before the inquired Begin Date.  " + &
										"Please reinquire with an earlier Begin Date.")
	return -1
End if

SetPointer(HourGlass!)
This.SetRedraw(False)
ii_start_column_auto_conv = ii_column_number_auto_conv

ls_data = tab_1.tabpage_auto_conv.dw_auto_conv.Object.DataWindow.Data
If Right(ls_data, 2) <> '~r~n' Then
	ls_data += '~r~n'
End if
ll_row_count = tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()

lds_original_data.Reset()
lds_original_data.ImportString(ls_data)

//  Set li_num_tabs to the number of tabs before the 1st quantity column
//  If column 9 is the 1st quantity column, set li_num_tabs = 8
li_num_tabs = 8

For li_counter = 1 to ii_column_number_auto_conv
	If adt_begin <= istr_date_range_auto_conv[li_counter].begin_date And &
			adt_end >= istr_date_range_auto_conv[li_counter].end_date Then
		If Not lb_new_column Then
			istr_date_range_auto_conv[li_counter].begin_date = adt_begin
			istr_date_range_auto_conv[li_counter].end_date = adt_end
			istr_date_range_auto_conv[li_counter].new_column = True
			lb_new_column = True
			li_new_column_number = li_counter

			// need to loop through the whole string to remove values in that column
			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					// li_counter * 2) + 6 is the first quantity_? column
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
//					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1 - 1, '~t')
//					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
				
					ls_qty_field = "min_" + string(li_counter + 1)
					ld_value1 = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemDecimal(li_row_counter, ls_qty_field)
					ls_qty_field = "max_" + string(li_counter + 1)
					ld_value2 = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemDecimal(li_row_counter, ls_qty_field)					
					if isnull(ld_value1) then
						ls_value1 = ''
					else
						ls_value1 = string(ld_value1)
					end if
					if isnull(ld_value2) then
						ls_value2 = ''
					else
						ls_value2 = string(ld_value2)
					end if					
					
					ls_xx = mid(ls_data, 1, ll_pos1) + ls_value1 + '~t' + ls_value2
					ls_xx2 = mid(ls_data, ll_pos2) 	
					ls_data = ls_xx + ls_xx2					
					
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)					
			Next
			
			Continue
		Else
			// this column is not to be used anymore
			// loop through the string and replace those values
			ii_column_number_auto_conv --
			for li_sub_counter = li_counter to ii_column_number_auto_conv
				istr_date_range_auto_conv[li_sub_counter] = istr_date_range_auto_conv[li_sub_counter + 1]
				
			Next
			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
					
					ls_qty_field = "min_" + string(li_counter + 1)
					ld_value1 = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemDecimal(li_row_counter, ls_qty_field)
					ls_qty_field = "max_" + string(li_counter + 1)
					ld_value2 = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemDecimal(li_row_counter, ls_qty_field)					
					if isnull(ld_value1) then
						ls_value1 = ''
					else
						ls_value1 = string(ld_value1)
					end if
					if isnull(ld_value2) then
						ls_value2 = ''
					else
						ls_value2 = string(ld_value2)
					end if					
					
					ls_xx = mid(ls_data, 1, ll_pos1) + ls_value1 + '~t' + ls_value2
					ls_xx2 = mid(ls_data, ll_pos2) 	
					ls_data = ls_xx + ls_xx2					
					
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			// I just cleared out the data for this loop, now need to decrement the counter to
			// check the new data in that column
			li_counter --

			Continue
		End if
	End if
	// If the column goes in the middle of this, then it will turn into three cols
	If adt_begin > istr_date_range_auto_conv[li_counter].begin_date And &
			adt_end < istr_date_range_auto_conv[li_counter].end_date Then
		If Not lb_new_column Then
			// This should always be, but just in case...
			ldt_temp = istr_date_range_auto_conv[li_counter].end_date
			istr_date_range_auto_conv[li_counter].end_date = RelativeDate(adt_begin, -1)
			istr_date_range_auto_conv[li_counter].new_column = True

			// about to insert two columns
			for li_sub_counter = ii_column_number_auto_conv + 2 to li_counter + 2 step -1
				istr_date_range_auto_conv[li_sub_counter] = istr_date_range_auto_conv[li_sub_counter + 1]
			Next
			istr_date_range_auto_conv[li_counter + 1].begin_date = adt_begin
			istr_date_range_auto_conv[li_counter + 1].end_date = adt_end
			istr_date_range_auto_conv[li_counter + 1].new_column = True

			istr_date_range_auto_conv[li_counter + 2].begin_date = RelativeDate(adt_end, 1)
			istr_date_range_auto_conv[li_counter + 2].end_date = ldt_temp
			istr_date_range_auto_conv[li_counter + 2].new_column = True

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1, &
									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1) + '~t~t' + &
									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1))
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			li_new_column_number = li_counter + 1
			li_counter += 2 
			ii_column_number_auto_conv += 2
			lb_new_column = True	

		End if
	End if

	If adt_begin <= istr_date_range_auto_conv[li_counter].end_date And &
			adt_begin > istr_date_range_auto_conv[li_counter].begin_date Then
		istr_date_range_auto_conv[li_counter].end_date = RelativeDate(adt_begin, -1)
		istr_date_range_auto_conv[li_counter].new_column = True

		If Not lb_new_column Then
			for li_sub_counter = ii_column_number_auto_conv to li_counter step -1
				istr_date_range_auto_conv[li_sub_counter + 1] = istr_date_range_auto_conv[li_sub_counter]
			Next
			istr_date_range_auto_conv[li_counter + 1].begin_date = adt_begin
			istr_date_range_auto_conv[li_counter + 1].end_date = adt_end
			istr_date_range_auto_conv[li_counter + 1].new_column = True

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1, &
									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1) )
//									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1) + '~t~t')
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			lb_new_column = True
			li_counter ++
			ii_column_number_auto_conv ++
			li_new_column_number = li_counter
		End if
		Continue
	End if
	If adt_end >= istr_date_range_auto_conv[li_counter].begin_date And &
			adt_end <  istr_date_range_auto_conv[li_counter].end_date Then
		istr_date_range_auto_conv[li_counter].begin_date = RelativeDate(adt_end, 1)
		istr_date_range_auto_conv[li_counter].new_column = True

		If Not lb_new_column Then
			for li_sub_counter = ii_column_number_auto_conv + 1 to li_counter + 1 step -1
				istr_date_range_auto_conv[li_sub_counter] = istr_date_range_auto_conv[li_sub_counter - 1]
			Next
			istr_date_range_auto_conv[li_counter].begin_date = adt_begin
			istr_date_range_auto_conv[li_counter].end_date = adt_end

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
					ll_pos2 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, 2)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1, &
									 + Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1))
//									 '~t~t' + Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1))
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			lb_new_column = True
			li_new_column_number = li_counter
			ii_column_number_auto_conv ++
		End if
	End if
Next

If Not lb_new_column Then
	// this date range is for some hole in the dates
	for li_counter = 1 to ii_column_number_auto_conv - 1
		if istr_date_range_auto_conv[li_counter].end_date < adt_begin And &
				istr_date_range_auto_conv[li_counter + 1].begin_date > adt_end Then
			ii_column_number_auto_conv ++
			For li_sub_counter = ii_column_number_auto_conv to li_counter + 1 step -1
				// Shift all dates 1 to the right
				istr_date_range_auto_conv[li_sub_counter] = istr_date_range_auto_conv[li_sub_counter - 2]
			Next

			istr_date_range_auto_conv[li_counter + 1].begin_date = adt_begin
			istr_date_range_auto_conv[li_counter + 1].end_date = adt_end
			istr_date_range_auto_conv[li_counter + 1].new_column = True

			lb_new_column = True
			li_new_column_number = li_counter
			Exit
		End if
	Next
End if

If Not lb_new_column Then
	If adt_end < istr_date_range_auto_conv[1].begin_date Then
		// goes before all other columns, Shift all one to the right
		ii_column_number_auto_conv ++
		For li_counter = ii_column_number_auto_conv to 2 Step -1
			istr_date_range_auto_conv[li_counter] = istr_date_range_auto_conv[li_counter - 1]
		Next
		istr_date_range_auto_conv[1].begin_date = adt_begin
		istr_date_range_auto_conv[1].end_date = adt_end
		istr_date_range_auto_conv[1].new_column = True

		ll_pos1 = 0
		for li_row_counter = 1 to ll_row_count
			// insert a tab in the place of the first column
			ll_pos1 = iw_frame.iu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter * 2) + li_num_tabs - 2)
			ls_data = Replace(ls_data, ll_pos1 + 1, 0, '~t~t')
			ll_pos1 = Pos(ls_data, '~r~n', ll_pos1)
		Next
		lb_new_column = True
		li_new_column_number = 1
		
			
	Else
		// must go at the end
		ii_column_number_auto_conv ++
		istr_date_range_auto_conv[ii_column_number_auto_conv].begin_date = adt_begin
		istr_date_range_auto_conv[ii_column_number_auto_conv].end_date = adt_end
		istr_date_range_auto_conv[ii_column_number_auto_conv].new_column = True
		lb_new_column = True
		li_new_column_number = ii_column_number_auto_conv
	End if
End if

ls_TitleModify = ''
For li_Counter = 1 to ii_column_number_auto_conv
	ls_TitleModify += "parm_" + String(li_counter) + "_t.Text = '" + &
			String(istr_date_range_auto_conv[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
			String(istr_date_range_auto_conv[li_Counter].end_date, "mm/dd/yy") + "' " + &
			"parm_" + String(li_Counter) + "_t.Visible = 1 " + &
			"min_" + String(li_counter) + "_t.Visible = 1 " + &
			"max_" + String(li_counter) + "_t.Visible = 1 " + &
			"min_" + String(li_counter) + ".Visible = 1 " + &
			"max_" + String(li_counter) + ".Visible = 1 " 

Next

// wf_reset_Title_Text_auto_conv will clear out ii_column_number_auto_conv and istr_date_range_auto_conv
li_counter = ii_column_number_auto_conv
lstr_temp_range = istr_date_range_auto_conv
wf_Reset_Title_Text_auto_conv()
ii_column_number_auto_conv = li_counter
istr_date_range_auto_conv = lstr_temp_range

tab_1.tabpage_auto_conv.dw_auto_conv.Modify(ls_TitleModify)

tab_1.tabpage_auto_conv.dw_auto_conv.Reset()
If ll_row_count > 0 Then
	// If there are no rows, ls_data just turned to garbage, so don't re import it
	tab_1.tabpage_auto_conv.dw_auto_conv.ImportString(ls_data)
End if


For li_counter = 1 to 30
	If istr_date_range_auto_conv[li_counter].end_date >=adt_end Then
		For li_sub_counter = 1 to 30
			If istr_date_range_auto_conv[li_counter].end_date = lstr_original_date_range[li_sub_counter].end_date Then
				ls_orig_col_name1 = 'min_' + string(li_sub_counter)
				ls_orig_col_name2 = 'max_' + string(li_sub_counter)				
				ls_new_col_name1 = 'min_' + string(li_counter)
				ls_new_col_name2 = 'max_' + string(li_counter)				
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_row_counter, ls_new_col_name1, lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name1))
					tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_row_counter, ls_new_col_name2, lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name2))					
				Next
			End If
		Next
	End if
Next

This.tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_Set_HSpScroll()

This.SetRedraw(True)
tab_1.tabpage_auto_conv.dw_auto_conv.SetColumn(li_new_column_number + 8)
return li_new_column_number

end function

public subroutine wf_protect (string as_display, long al_tab_selected);String	ls_group_id, &
			ls_add_auth, &
			ls_delete_auth, &
			ls_modify_auth, &
			ls_menu_name, &
			ls_window_name

iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_save')


Choose Case al_tab_selected
	Case 1
		ls_window_name= 'W_PAS_INSTANCES_INST'
		ls_menu_name = 'M_PAS_INSTANCES_INST'
	Case 2
		ls_window_name = 'W_PAS_INSTANCES_PARM'
		ls_menu_name = 'M_PAS_INSTANCES_PARM'
	Case 3
		ls_window_name = 'W_PAS_INSTANCES_AUTO'
		ls_menu_name = 'M_PAS_INSTANCES_AUTO'
End Choose

ls_group_id = iw_frame.iu_netwise_data.is_groupid

SELECT group_profile.add_auth, group_profile.delete_auth, group_profile.modify_auth
  INTO :ls_add_auth, :ls_delete_auth, :ls_modify_auth
  FROM group_profile  
  WHERE ( group_profile.appid = 'PRD' ) AND  
        ( group_profile.group_id = :ls_group_id ) AND  
        ( group_profile.window_name = :ls_window_name ) AND  
        ( group_profile.menuid = :ls_menu_name)  ;

If ls_add_auth = 'Y' Then
	ib_protection_status = False
Else
	ib_protection_status = True
End If  
 
If Not ib_date_protection Then
	If Not ib_protection_status Then
		If (as_display = 'L') or (as_display = 'W') Then 
			iw_frame.im_menu.mf_Enable('m_addrow')
		End If
	End If
	If ls_delete_auth = 'Y' Then
		iw_frame.im_menu.mf_Enable('m_deleterow')
	End If
	If ls_modify_auth = 'Y' Then
		iw_frame.im_menu.mf_Enable('m_save')
	End If
Else
	If ls_modify_auth = 'Y' Then
		iw_frame.im_menu.mf_Enable('m_save')
	End If	
End If

wf_check_auth()


end subroutine

public subroutine wf_check_auth ();String	ls_group_id, &
			ls_add_auth, &
			ls_delete_auth, &
			ls_modify_auth, &
			ls_menu_name, &
			ls_window_name
			
Boolean	lb_protection_status			

ib_save_instances = False
ib_save_parm = False
ib_save_auto = False

ls_group_id = iw_frame.iu_netwise_data.is_groupid

ls_window_name= 'W_PAS_INSTANCES_INST'
ls_menu_name = 'M_PAS_INSTANCES_INST'

SELECT group_profile.add_auth, group_profile.delete_auth, group_profile.modify_auth
  INTO :ls_add_auth, :ls_delete_auth, :ls_modify_auth
  FROM group_profile  
  WHERE ( group_profile.appid = 'PRD' ) AND  
        ( group_profile.group_id = :ls_group_id ) AND  
        ( group_profile.window_name = :ls_window_name ) AND  
        ( group_profile.menuid = :ls_menu_name)  ;

If ls_modify_auth = 'Y' Then
	ib_save_instances = True
Else
	ib_save_instances = False
End If

ls_window_name = 'W_PAS_INSTANCES_PARM'
ls_menu_name = 'M_PAS_INSTANCES_PARM'

SELECT group_profile.add_auth, group_profile.delete_auth, group_profile.modify_auth
  INTO :ls_add_auth, :ls_delete_auth, :ls_modify_auth
  FROM group_profile  
  WHERE ( group_profile.appid = 'PRD' ) AND  
        ( group_profile.group_id = :ls_group_id ) AND  
        ( group_profile.window_name = :ls_window_name ) AND  
        ( group_profile.menuid = :ls_menu_name)  ;

If ls_modify_auth = 'Y' Then
	ib_save_parm = True
Else
	ib_save_parm = False
End If
		
ls_window_name = 'W_PAS_INSTANCES_AUTO'
ls_menu_name = 'M_PAS_INSTANCES_AUTO'

SELECT group_profile.add_auth, group_profile.delete_auth, group_profile.modify_auth
  INTO :ls_add_auth, :ls_delete_auth, :ls_modify_auth
  FROM group_profile  
  WHERE ( group_profile.appid = 'PRD' ) AND  
        ( group_profile.group_id = :ls_group_id ) AND  
        ( group_profile.window_name = :ls_window_name ) AND  
        ( group_profile.menuid = :ls_menu_name)  ;

If ls_modify_auth = 'Y' Then
	ib_save_auto = True
Else
	ib_save_auto = False
End If



end subroutine

public function boolean wf_update ();Long	ll_RowCount, ll_sub, ll_sub2

Boolean	lb_99_found, &
			lb_instance_found, &
			lb_instance_group_found	


If ib_save_instances Then
	If tab_1.tabpage_inst.dw_instances.AcceptText() = -1 Then 
		tab_1.Selectedtab = 1
		return false
	End If
End If

If ib_save_parm Then
	If tab_1.tabpage_inst_parm.dw_parameters.AcceptText() = -1 Then 
		tab_1.Selectedtab = 2
		return false
	End If
End If

If ib_save_auto Then
	If tab_1.tabpage_auto_conv.dw_auto_conv.AcceptText() = -1 Then 
		tab_1.Selectedtab = 3
		return false
	End If
End If

ib_no_parameter_data = False
ib_no_instance_data = False
ib_no_auto_conv_data = False

If ib_save_parm Then
	lb_99_found = False
	lb_instance_found = False
	lb_instance_group_found = False
	ll_RowCount = tab_1.tabpage_inst_parm.dw_parameters.RowCount()
	For ll_sub = 1 to ll_RowCount
		If (Not IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemString(ll_sub, "group"))) Then
			If tab_1.tabpage_inst_parm.dw_parameters.GetItemString(ll_sub, "group") > '   ' Then
				lb_instance_group_found = True
			End If
		End If
		For ll_sub2 = 1 to 30
			If (Not IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(ll_sub, "min_"+ String(ll_sub2)))) OR &
				(Not IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(ll_sub, "max_"+ String(ll_sub2)))) Then
				lb_instance_found = True
				If  tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(ll_sub, "priority") = 99 Then
					lb_99_found = True
				End If
				If  tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(ll_sub, "priority") > 99 Then
					MessageBox ('Priority Error', "Priority can not be greater than 99 on Instance Parameters tab")
					Return False 
				End If
			End If	
		Next
	Next
	If lb_instance_found and lb_instance_group_found and Not lb_99_found Then
		MessageBox ('Priority Error', "No priority 99 found on Instance Parameters tab")
		Return False 
	End If
End If

If ib_save_auto Then
	lb_99_found = False
	lb_instance_found = False
	ll_RowCount = tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()
	For ll_sub = 1 to ll_RowCount
		For ll_sub2 = 1 to 30
			If (Not IsNull(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(ll_sub, "min_"+ String(ll_sub2)))) OR &
				(Not IsNull(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(ll_sub, "max_"+ String(ll_sub2)))) Then
				lb_instance_found = True
				If  tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(ll_sub, "priority") = 99 Then
					lb_99_found = True
				End If
				If  tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(ll_sub, "priority") > 99 Then
					MessageBox ('Priority Error', "Priority can not be greater than 99 on Auto Conversion tab")
					Return False 
				End If
			End If	
		Next
	Next
	If lb_instance_found and Not lb_99_found Then
		MessageBox ('Priority Error', "No priority 99 found on Auto Conversion Parameters tab")
		Return False 
	End If
End If

SetPointer(HourGlass!)

If ib_save_instances Then	
	If tab_1.tabpage_inst.dw_instances.RowCount() < 1 Then
		ib_no_instance_data = true
	Else
		If wf_update_instances() <> 0 Then Return False
	End if
End If

If ib_save_parm Then
	If tab_1.tabpage_inst_parm.dw_parameters.RowCount() < 1 Then
		ib_no_parameter_data = true
	Else
		If Not wf_update_parameters() Then Return False
	End if
End If

If ib_save_auto Then
	If tab_1.tabpage_auto_conv.dw_auto_conv.RowCount() < 1 Then
		ib_no_auto_conv_data = true
	Else
		If Not wf_update_auto_conv() Then Return False
	End if
End If

If ib_no_instance_data and ib_no_parameter_data and ib_no_auto_conv_data Then
	iw_frame.SetMicroHelp("Update not performed; no data to update")
Else
	iw_frame.SetMicroHelp("Modification Successful")
End If

// Don't know why this is sometimes necessary...
SetPointer(Arrow!)

return true
end function

public subroutine wf_copy_parm ();Long 	ll_RowCount, &
		ll_sub
		
String	ls_parm_text, &
			ls_parm_visible, &
			ls_min_text, &
			ls_min_text_visible, &
			ls_max_text, &
			ls_max_text_visible, &
			ls_min_visible, &
			ls_max_visible

Integer	li_counter, &
			li_label_visible

If cb_copy_button.Text = 'Copy to Auto Conv Parm' Then
	tab_1.tabpage_inst_parm.dw_parameters.AcceptText()
	tab_1.tabpage_auto_conv.dw_auto_conv.Reset()
	tab_1.tabpage_auto_conv.dw_auto_conv.ImportString(tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data)
	
	dnv_auto_conv.Reset()
	dnv_auto_conv.ImportString(dnv_parameters.Object.DataWindow.Data)
	dnv_update_auto_conv.Reset()
	dnv_update_auto_conv.ImportString(dnv_update_parameters.Object.DataWindow.Data)
		
	
	ll_RowCount = tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()
	For ll_sub = 1 to ll_RowCount  /* Set at least 1 column to modified */
		tab_1.tabpage_auto_conv.dw_auto_conv.SetItemStatus(ll_sub, "destination_plant", Primary!, DataModified!)
	Next
	
	For li_Counter = 1 to 30

		ls_parm_text = tab_1.tabpage_inst_parm.dw_parameters.Describe ("parm_" + String(li_counter) + "_t.Text")
		ls_parm_visible = tab_1.tabpage_inst_parm.dw_parameters.Describe ("parm_" + String(li_counter) + "_t.Visible")
		ls_min_text = tab_1.tabpage_inst_parm.dw_parameters.Describe ("min_" + String(li_counter) + "_t.Text")
		ls_min_text_visible = tab_1.tabpage_inst_parm.dw_parameters.Describe ("min_" + String(li_counter) + "_t.Visible")
		ls_max_text = tab_1.tabpage_inst_parm.dw_parameters.Describe ("max_" + String(li_counter) + "_t.Text")
		ls_max_text_visible = tab_1.tabpage_inst_parm.dw_parameters.Describe ("max_" + String(li_counter) + "_t.Visible")
		ls_min_visible = tab_1.tabpage_inst_parm.dw_parameters.Describe ("min_" + String(li_counter) + ".Visible")
		ls_max_visible = tab_1.tabpage_inst_parm.dw_parameters.Describe ("max_" + String(li_counter) + ".Visible")
	
		tab_1.tabpage_auto_conv.dw_auto_conv.Modify ("parm_" + String(li_counter) + "_t.Text = " + ls_parm_text)
		tab_1.tabpage_auto_conv.dw_auto_conv.Modify ("parm_" + String(li_counter) + "_t.Visible = " + ls_parm_visible)
		tab_1.tabpage_auto_conv.dw_auto_conv.Modify ("min_" + String(li_counter) + "_t.Text = " + ls_min_text)
		tab_1.tabpage_auto_conv.dw_auto_conv.Modify ("min_" + String(li_counter) + "_t.Visible = " + ls_min_text_visible)
		tab_1.tabpage_auto_conv.dw_auto_conv.Modify ("max_" + String(li_counter) + "_t.Text = " + ls_max_text)
		tab_1.tabpage_auto_conv.dw_auto_conv.Modify ("max_" + String(li_counter) + "_t.Visible = " + ls_max_text_visible)
		tab_1.tabpage_auto_conv.dw_auto_conv.Modify ("min_" + String(li_counter) + ".Visible = " + ls_min_visible)
		tab_1.tabpage_auto_conv.dw_auto_conv.Modify ("max_" + String(li_counter) + ".Visible = " + ls_max_visible)
		istr_date_range_auto_conv[li_counter] = istr_date_range_parm[li_counter]

	Next
	
	
	istr_date_range_auto_conv = istr_date_range_parm
	ii_column_number_auto_conv = ii_column_number_parm
//	
//	wf_reset_title_text_auto_conv()
//	
//	ls_TitleModify = ''
//	For li_Counter = 1 to ii_column_number_auto_conv
//		ls_TitleModify += "parm_" + String(li_counter) + "_t.Text = '" + &
//			String(istr_date_range_auto_conv[li_Counter].begin_date, "mm/dd/yyyy") + "~~r~~n" + &
//			String(istr_date_range_auto_conv[li_Counter].end_date, "mm/dd/yyyy") + "' " + &
//			"parm_" + String(li_Counter) + "_t.Visible = 1 " + &
//			"min_" + String(li_counter) + "_t.Visible = 1 " + &
//			"max_" + String(li_counter) + "_t.Visible = 1 " + &
//			"min_" + String(li_counter) + ".Visible = 1 " + &
//			"max_" + String(li_counter) + ".Visible = 1 " 
//
//	Next
//
//
//	tab_1.tabpage_auto_conv.dw_auto_conv.Modify(ls_TitleModify)
	

Else	
	tab_1.tabpage_auto_conv.dw_auto_conv.AcceptText()
	tab_1.tabpage_inst_parm.dw_parameters.Reset()
	tab_1.tabpage_inst_parm.dw_parameters.ImportString(tab_1.tabpage_auto_conv.dw_auto_conv.Object.DataWindow.Data)

	dnv_parameters.Reset()
	dnv_parameters.ImportString(dnv_auto_conv.Object.DataWindow.Data)
	dnv_update_parameters.Reset()
	dnv_update_parameters.ImportString(dnv_update_auto_conv.Object.DataWindow.Data)


	ll_RowCount = tab_1.tabpage_inst_parm.dw_parameters.RowCount()
	For ll_sub = 1 to ll_RowCount  /* Set at least 1 column to modified */
		tab_1.tabpage_inst_parm.dw_parameters.SetItemStatus(ll_sub, "destination_plant", Primary!, DataModified!)
	Next
	
	For li_Counter = 1 to 30

		ls_parm_text = tab_1.tabpage_auto_conv.dw_auto_conv.Describe ("parm_" + String(li_counter) + "_t.Text")
		ls_parm_visible = tab_1.tabpage_auto_conv.dw_auto_conv.Describe ("parm_" + String(li_counter) + "_t.Visible")
		ls_min_text = tab_1.tabpage_auto_conv.dw_auto_conv.Describe ("min_" + String(li_counter) + "_t.Text")
		ls_min_text_visible = tab_1.tabpage_auto_conv.dw_auto_conv.Describe ("min_" + String(li_counter) + "_t.Visible")
		ls_max_text = tab_1.tabpage_auto_conv.dw_auto_conv.Describe ("max_" + String(li_counter) + "_t.Text")
		ls_max_text_visible = tab_1.tabpage_auto_conv.dw_auto_conv.Describe ("max_" + String(li_counter) + "_t.Visible")
		ls_min_visible = tab_1.tabpage_auto_conv.dw_auto_conv.Describe ("min_" + String(li_counter) + ".Visible")
		ls_max_visible = tab_1.tabpage_auto_conv.dw_auto_conv.Describe ("max_" + String(li_counter) + ".Visible")
	
		tab_1.tabpage_inst_parm.dw_parameters.Modify ("parm_" + String(li_counter) + "_t.Text = " + ls_parm_text)
		tab_1.tabpage_inst_parm.dw_parameters.Modify ("parm_" + String(li_counter) + "_t.Visible = " + ls_parm_visible)
		tab_1.tabpage_inst_parm.dw_parameters.Modify ("min_" + String(li_counter) + "_t.Text = " + ls_min_text)
		tab_1.tabpage_inst_parm.dw_parameters.Modify ("min_" + String(li_counter) + "_t.Visible = " + ls_min_text_visible)
		tab_1.tabpage_inst_parm.dw_parameters.Modify ("max_" + String(li_counter) + "_t.Text = " + ls_max_text)
		tab_1.tabpage_inst_parm.dw_parameters.Modify ("max_" + String(li_counter) + "_t.Visible = " + ls_max_text_visible)
		tab_1.tabpage_inst_parm.dw_parameters.Modify ("min_" + String(li_counter) + ".Visible = " + ls_min_visible)
		tab_1.tabpage_inst_parm.dw_parameters.Modify ("max_" + String(li_counter) + ".Visible = " + ls_max_visible)
		istr_date_range_parm[li_counter] = istr_date_range_auto_conv[li_counter]

	Next
	
	istr_date_range_parm = istr_date_range_auto_conv 
	ii_column_number_parm = ii_column_number_auto_conv
//	
//	wf_reset_title_text_parm()
//	
//	ls_TitleModify = ''
//	For li_Counter = 1 to ii_column_number_parm
//		ls_TitleModify += "parm_" + String(li_counter) + "_t.Text = '" + &
//			String(istr_date_range_parm[li_Counter].begin_date, "mm/dd/yyyy") + "~~r~~n" + &
//			String(istr_date_range_parm[li_Counter].end_date, "mm/dd/yyyy") + "' " + &
//			"parm_" + String(li_Counter) + "_t.Visible = 1 " + &
//			"min_" + String(li_counter) + "_t.Visible = 1 " + &
//			"max_" + String(li_counter) + "_t.Visible = 1 " + &
//			"min_" + String(li_counter) + ".Visible = 1 " + &
//			"max_" + String(li_counter) + ".Visible = 1 " 
//
//	Next
//
//	tab_1.tabpage_inst_parm.dw_parameters.Modify(ls_TitleModify)
	
End If

Return
end subroutine

public function integer wf_add_date_range (date adt_begin, date adt_end);Boolean	lb_new_column

Date		ldt_temp
Decimal	ld_value, &
			ld_value2

Int	li_counter, &
		li_sub_counter, &
		li_new_column_number, &
		li_row_counter, &
		li_num_tabs

Long	ll_row_count, &
		ll_pos1, &
		ll_pos2, &
		ll_pos3, &
		ll_pos4, &
		ll_pos5, &
		ll_end_of_row, &
		lla_column_amounts[], &
		ll_temp

str_date_range	lstr_temp_range[]
str_quantity	lstr_quantity[], &
					lstr_zero_quantity[]
String	ls_data, &
			ls_TitleModify, &
			ls_columnAmounts, &
			ls_columnName, ls_temp, &
			ls_qty_field, &
			ls_mid1, &
			ls_mid2, &
			ls_xx, &
			ls_xx2, &
			ls_orig_col_name, &
			ls_new_col_name

u_string_functions		lu_string

DataStore 			lds_original_data
str_date_range		lstr_original_date_range[]

lds_original_data = Create DataStore
lds_original_data.DataObject = "d_pas_instances"

lstr_original_date_range = istr_date_range

If ii_column_number > UpperBound(istr_date_range) - 1 Then Return -1

If adt_Begin < idt_inquire_date Then
	MessageBox("Inquire Date", "New date range cannot be before the inquired Begin Date.  " + &
										"Please reinquire with an earlier Begin Date.")
	return -1
End if

SetPointer(HourGlass!)
This.SetRedraw(False)
ii_start_column = ii_column_number

ls_data = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data

lds_original_data.Reset()
lds_original_data.ImportString(ls_data)

If Right(ls_data, 2) <> '~r~n' Then
	ls_data += '~r~n'
End if
ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()

//  Set li_num_tabs to the number of tabs before the 1st quantity column
//  If column 9 is the 1st quantity column, set li_num_tabs = 8
li_num_tabs = 8

For li_counter = 1 to ii_column_number
	If adt_begin <= istr_date_range[li_counter].begin_date And &
			adt_end >= istr_date_range[li_counter].end_date Then
		If Not lb_new_column Then
			istr_date_range[li_counter].begin_date = adt_begin
			istr_date_range[li_counter].end_date = adt_end
			istr_date_range[li_counter].new_column = True
			lb_new_column = True
			li_new_column_number = li_counter
			
			// need to loop through the whole string to remove values in that column
			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					// 9 is the first quantity_? column, 
					
					ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter) + li_num_tabs - 1)
					ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1 - 1, '~t')
					ll_end_of_row = Pos(ls_data, '~r~n', ll_pos2 + 1)
					ll_pos3 = lu_string.nf_npos(ls_data, '~t', ll_end_of_row, -2)
					ll_pos4 = pos(ls_data, '~t', ll_pos3 + 1)
					ls_data = Replace(ls_data, ll_pos3, ll_pos4 - ll_pos3, '')
				
//move the qty to the new column					
					ls_qty_field = "quantity_" + String(li_Counter)
					ld_value  = tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_qty_field)		
					if isnull(ld_value) Then
						ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
						ls_xx = mid(ls_data, 1, ll_pos2 - 1) + ''
						ls_xx2 = mid(ls_data, ll_pos2) 	
						ls_data = ls_xx + ls_xx2
					else
						ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
						ls_xx = mid(ls_data, 1, ll_pos2 - 1) + string(ld_value)
						ls_xx2 = mid(ls_data, ll_pos2) 	
						ls_data = ls_xx + ls_xx2
					end if
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			Continue
		Else
			// this column is not to be used anymore
			// loop through the string and replace those values
			ii_column_number --
			for li_sub_counter = li_counter to ii_column_number
				istr_date_range[li_sub_counter] = istr_date_range[li_sub_counter + 1]
				
			Next
			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, li_counter + li_num_tabs - 1)
					ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)

					ls_qty_field = "quantity_" + String(li_Counter)
					ld_value  = tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_qty_field)	
					
					if isnull(ld_value) Then
						ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
						ls_xx = mid(ls_data, 1, ll_pos1) + ''
						ls_xx2 = mid(ls_data, ll_pos2) 	
						ls_data = ls_xx + ls_xx2
					else
						ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
						ls_xx = mid(ls_data, 1, ll_pos1) + string(ld_value)
						ls_xx2 = mid(ls_data, ll_pos2) 	
						ls_data = ls_xx + ls_xx2
					end if
					
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
					// put that tabs back at the end of the line
//					ls_data = Replace(ls_data, ll_pos1 - 30, 32, '~t' + Right(ls_data, 32))
			Next
			// I just cleared out the data for this loop, now need to decrement the counter to
			// check the new data in that column
			li_counter --

			Continue
		End if
	End if
	// If the column goes in the middle of this, then it will turn into three cols
	If adt_begin > istr_date_range[li_counter].begin_date And &
			adt_end < istr_date_range[li_counter].end_date Then
		If Not lb_new_column Then
			// This should always be, but just in case...
			ldt_temp = istr_date_range[li_counter].end_date
			istr_date_range[li_counter].end_date = RelativeDate(adt_begin, -1)
			istr_date_range[li_counter].new_column = True

			// about to insert two columns
			for li_sub_counter = ii_column_number + 2 to li_counter + 2 step -1
				istr_date_range[li_sub_counter] = istr_date_range[li_sub_counter - 2]
			Next
			istr_date_range[li_counter + 1].begin_date = adt_begin
			istr_date_range[li_counter + 1].end_date = adt_end
			istr_date_range[li_counter + 1].new_column = True

			istr_date_range[li_counter + 2].begin_date = RelativeDate(adt_end, 1)
			istr_date_range[li_counter + 2].end_date = ldt_temp
			istr_date_range[li_counter + 2].new_column = True

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, li_counter + li_num_tabs - 1)
					ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
					ll_pos3 = pos(ls_data, '~t', ll_pos2 + 1)
					ll_end_of_row = Pos(ls_data, '~r~n', ll_pos2 + 1) + 1
					ll_pos4 = lu_string.nf_npos(ls_data, '~t', ll_end_of_row, -3)
					ll_pos5 = pos(ls_data, '~t', ll_pos4 + 1)
//dmk
					ls_mid1 = mid(ls_data, ll_pos1 + 1, (ll_pos4 - 1) - (ll_pos1))
					ls_mid2 = mid(ls_data, ll_pos5 + 1, ll_end_of_row - (ll_pos5))
//					ls_data = Replace(ls_data, ll_pos3 + 1, (ll_end_of_row - (ll_pos3)), &
//							mid(ls_data, ll_pos1 + 1, (ll_pos4 - 1) - (ll_pos1)) +&
//							mid(ls_data, ll_pos5 + 1, ll_end_of_row - (ll_pos5)))
					
					ls_data = Replace(ls_data, ll_pos3 + 1, (ll_end_of_row - (ll_pos3)), &
							ls_mid1 + ls_mid2)
//dmk - replacing the new columns with delimiters so that the amount can be populated below.		
					ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
					ll_pos3 = pos(ls_data, '~t', ll_pos2 + 1)	
					ls_data = Replace(ls_data, ll_pos1 + 1, (ll_pos3  - (ll_pos1)), '~t~t')
					
					
												
//dmk - getting the quantity from the existing column so that it can be put on the new columns added, column
//      was not being changed and sent to the rpc and updated incorrectly on the mainframe 
					ls_qty_field = "quantity_" + String(li_Counter)
					ld_value  = tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_qty_field)	

					if isnull(ld_value) Then
						//do nothing
					else
						ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
						ls_xx = mid(ls_data, 1, ll_pos2 - 1) + string(ld_value)
						ls_xx2 = mid(ls_data, ll_pos2) 	
						ls_data = ls_xx + ls_xx2
						
						ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
						ll_pos3 = pos(ls_data, '~t', ll_pos2 + 1)
						ls_xx = mid(ls_data, 1, ll_pos3 - 1) + string(ld_value)
						ls_xx2 = mid(ls_data, ll_pos3) 	
						ls_data = ls_xx + ls_xx2
						
						
					end if
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
				
			Next

			li_new_column_number = li_counter + 1
			li_counter += 2 
			ii_column_number += 2
			lb_new_column = True	

		End if
	End if

	If adt_begin <= istr_date_range[li_counter].end_date And &
			adt_begin > istr_date_range[li_counter].begin_date Then
		istr_date_range[li_counter].end_date = RelativeDate(adt_begin, -1)
		istr_date_range[li_counter].new_column = True

		If Not lb_new_column Then
			for li_sub_counter = ii_column_number to li_counter step -1
				istr_date_range[li_sub_counter + 1] = istr_date_range[li_sub_counter]
			Next
			istr_date_range[li_counter + 1].begin_date = adt_begin
			istr_date_range[li_counter + 1].end_date = adt_end
			istr_date_range[li_counter + 1].new_column = True

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
//					ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, li_counter + li_num_tabs - 1)
//					ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
//					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1, &
//									Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1) + '~t')
//					ll_end_of_row = Pos(ls_data, '~r~n', ll_pos2 + 1)
//					ll_pos3 = lu_string.nf_npos(ls_data, '~t', ll_end_of_row, -2)
//					ll_pos4 = pos(ls_data, '~t', ll_pos3 + 1)
//					ls_data = Replace(ls_data, ll_pos3, ll_pos4 - ll_pos3, '')
//			//dmk - populate the quantity fields for the new columns
//					ls_qty_field = "quantity_" + String(li_Counter)
//					ld_value  = tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_qty_field)		
//					if isnull(ld_value) Then
//						//do nothing
//					else
//						ls_xx = mid(ls_data, 1, ll_pos2) + string(ld_value)
//						ls_xx2 = mid(ls_data, ll_pos2 + 1) 	
//						ls_data = ls_xx + ls_xx2
//					end if
//					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)

					ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter) + li_num_tabs - 1)
					ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1 - 1, '~t')
					ll_end_of_row = Pos(ls_data, '~r~n', ll_pos2 + 1)
					ll_pos3 = lu_string.nf_npos(ls_data, '~t', ll_end_of_row, -2)
					ll_pos4 = pos(ls_data, '~t', ll_pos3 + 1)
					ls_data = Replace(ls_data, ll_pos3, ll_pos4 - ll_pos3, '~t~t~t')
				
//move the qty to the new column					
					ls_qty_field = "quantity_" + String(li_Counter)
					ld_value  = tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_qty_field)		
					if isnull(ld_value) Then
						//do nothing
					else
						ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
						ls_xx = mid(ls_data, 1, ll_pos2 - 1) + string(ld_value) 
//						ls_xx = mid(ls_data, 1, ll_pos2 - 1) + ''
						ls_xx2 = mid(ls_data, ll_pos2) 	
						ls_data = ls_xx + ls_xx2
					end if
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)

			Next

			lb_new_column = True
			li_counter ++
			ii_column_number ++
			li_new_column_number = li_counter
		End if
		Continue
	End if
	If adt_end >= istr_date_range[li_counter].begin_date And &
			adt_end <  istr_date_range[li_counter].end_date Then
		istr_date_range[li_counter].begin_date = RelativeDate(adt_end, 1)
		istr_date_range[li_counter].new_column = True

		If Not lb_new_column Then
			for li_sub_counter = ii_column_number + 1 to li_counter + 1 step -1
				istr_date_range[li_sub_counter] = istr_date_range[li_sub_counter - 1]
			Next
			istr_date_range[li_counter].begin_date = adt_begin
			istr_date_range[li_counter].end_date = adt_end

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
					ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, li_counter + li_num_tabs - 1)
					ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
					ls_data = Replace(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1, &
									'~t' + Mid(ls_data, ll_pos1 + 1, ll_pos2 - ll_pos1))
					ll_end_of_row = Pos(ls_data, '~r~n', ll_pos2 + 1)
					ll_pos3 = lu_string.nf_npos(ls_data, '~t', ll_end_of_row, -2)
					ll_pos4 = pos(ls_data, '~t', ll_pos3 + 1)
			 		ls_data = Replace(ls_data, ll_pos3, ll_pos4 - ll_pos3, '')
				//dmk - populate the quantity fields for the new columns
					ls_qty_field = "quantity_" + String(li_Counter)
					ld_value  = tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_qty_field)		
					if isnull(ld_value) Then
					//	do nothing
					else
						ls_xx = mid(ls_data, 1, ll_pos1) + string(ld_value)
						ls_xx2 = mid(ls_data, ll_pos1 + 1) 	
						ls_data = ls_xx + ls_xx2
					end if 
					ll_pos1 = Pos(ls_data, '~r~n', ll_pos2)
			Next
			lb_new_column = True
			li_new_column_number = li_counter
			ii_column_number ++
		End if
	End if
Next

If Not lb_new_column Then
	// this date range is for some hole in the dates
	for li_counter = 1 to ii_column_number - 1
		if istr_date_range[li_counter].end_date < adt_begin And &
				istr_date_range[li_counter + 1].begin_date > adt_end Then
			ii_column_number ++
			For li_sub_counter = ii_column_number to li_counter + 1 step -1
				// Shift all dates 1 to the right
				istr_date_range[li_sub_counter] = istr_date_range[li_sub_counter - 1]
			Next

			istr_date_range[li_counter + 1].begin_date = adt_begin
			istr_date_range[li_counter + 1].end_date = adt_end
			istr_date_range[li_counter + 1].new_column = True

			ll_pos1 = 0
			for li_row_counter = 1 to ll_row_count
				// insert a tab in the place of the first column
				ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, (li_counter + 1) + li_num_tabs - 1)
				ls_data = Replace(ls_data, ll_pos1 + 1, 0, '~t')
				ll_end_of_row = Pos(ls_data, '~r~n', ll_pos1 + 1)
				ll_pos3 = lu_string.nf_npos(ls_data, '~t', ll_end_of_row, -2)
				ll_pos4 = pos(ls_data, '~t', ll_pos3 + 1)
				ls_data = Replace(ls_data, ll_pos3, ll_pos4 - ll_pos3, '')
				
		//dmk - populate the quantity fields for the new columns
				ls_qty_field = "quantity_" + String(li_Counter)
				ld_value  = tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_qty_field)		
				if isnull(ld_value) Then
				//	do nothing
				else
					ls_xx = mid(ls_data, 1, ll_pos1) + string(ld_value)
					ls_xx2 = mid(ls_data, ll_pos1 + 1) 	
					ls_data = ls_xx + ls_xx2
				end if
				ll_pos1 = Pos(ls_data, '~r~n', ll_pos1)
			Next
			lb_new_column = True
			li_new_column_number = li_counter
			Exit
		End if
	Next
End if

If Not lb_new_column Then
	If adt_end < istr_date_range[1].begin_date Then
		// goes before all other columns, Shift all one to the right
		ii_column_number ++
		For li_counter = ii_column_number to 2 Step -1
			istr_date_range[li_counter] = istr_date_range[li_counter - 1]
		Next
		istr_date_range[1].begin_date = adt_begin
		istr_date_range[1].end_date = adt_end
		istr_date_range[1].new_column = True


		ll_pos1 = 0
		for li_row_counter = 1 to ll_row_count
			// insert a tab in the place of the first column
			ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, li_counter + li_num_tabs - 2)
			ls_data = Replace(ls_data, ll_pos1 + 1, 0, '~t')
			ll_end_of_row = Pos(ls_data, '~r~n', ll_pos1 + 1)
			ll_pos3 = lu_string.nf_npos(ls_data, '~t', ll_end_of_row, -2)
			ll_pos4 = pos(ls_data, '~t', ll_pos3 + 1)
			ls_data = Replace(ls_data, ll_pos3, ll_pos4 - ll_pos3, '')
		//dmk - populate the quantity fields for the new columns
			ls_qty_field = "quantity_" + String(li_Counter)
			ld_value  = tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_qty_field)		
			if isnull(ld_value) Then
				//	do nothing
			else
				//ll_pos1 = lu_string.nf_npos(ls_data, '~t', ll_pos1 + 1, li_counter + li_num_tabs - 2)
				ll_pos2 = pos(ls_data, '~t', ll_pos1 + 1)
				ll_pos3 = pos(ls_data, '~t', ll_pos2 + 1)
				ls_xx = mid(ls_data, 1, ll_pos1) + string(ld_value)
				ls_xx2 = mid(ls_data, ll_pos1 + 1) 	
				ls_data = ls_xx + ls_xx2
			end if
			
			ll_pos1 = Pos(ls_data, '~r~n', ll_pos1)
		Next

		lb_new_column = True
		li_new_column_number = 1
			
	Else
		// must go at the end
		ii_column_number ++
		istr_date_range[ii_column_number].begin_date = adt_begin
		istr_date_range[ii_column_number].end_date = adt_end
		istr_date_range[ii_column_number].new_column = True
		lb_new_column = True
		li_new_column_number = ii_column_number
	End if
End if

ls_TitleModify = ''
For li_Counter = 1 to ii_column_number
	If li_Counter <= 30 Then
		ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
				String(istr_date_range[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
				String(istr_date_range[li_Counter].end_date, "mm/dd/yy") + "' " + &
				"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
				"quantity_" + String(li_counter) + ".Visible = 1 " 
	End If
Next

// wf_reset_Title_Text will clear out ii_column number and istr_date_range
li_counter = ii_column_number
lstr_temp_range = istr_date_range
wf_Reset_Title_Text()
ii_column_number = li_counter
istr_date_range = lstr_temp_range

tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)

tab_1.tabpage_inst.dw_instances.Reset()
If ll_row_count > 0 Then
	// If there are no rows, ls_data just turned to garbage, so don't re import it
	tab_1.tabpage_inst.dw_instances.ImportString(ls_data)
End if

ls_data = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data
//tab_1.tabpage_inst.dw_instances.SetFocus()

For li_counter = 1 to ii_column_number
	if li_counter <= 30 Then
		If istr_date_range[li_counter].end_date >=adt_end Then
			For li_sub_counter = 1 to 30
				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
				ls_new_col_name = 'quantity_' + string(li_counter)					
				If istr_date_range[li_counter].end_date = lstr_original_date_range[li_sub_counter].end_date Then
					For li_row_counter = 1 to ll_row_count
						tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
					Next
				End If
			Next
		End if
	End If
Next

This.tab_1.tabpage_inst.dw_instances.Event ue_Set_HSpScroll()

This.SetRedraw(True)

tab_1.tabpage_inst.dw_instances.SetColumn(li_new_column_number + li_num_tabs - 1)

return li_new_column_number


end function

public function boolean wf_retrieve ();Date		ldt_begin

Int		li_counter, &
			li_ret

Long		ll_row_count, &
			ll_row

String	ls_header, &
			ls_input, &
			ls_steps, &
			ls_instances, &
			ls_valid_plants, &
			ls_sort, &
			ls_piece_count, &
			ls_parameters, &
			ls_ratios, &
			ls_temp, &
			ls_auto_conv, &
			ls_instances_end_date

if not ib_save_instances Then
	tab_1.tabpage_inst.dw_instances.ResetUpdate()
End If

if not ib_save_parm Then
	tab_1.tabpage_inst_parm.dw_parameters.ResetUpdate()
End If

if not ib_save_auto Then
	tab_1.tabpage_auto_conv.dw_auto_conv.ResetUpdate()
End If

SetPointer(HourGlass!)
If ib_bypass_check_for_changes = True Then
	// do nothing, the boolean value gets turned on in the header itemchanged event which alredy does a check for changes
	// this prevents the window from sending a double "do you want to save changes" message
Else
	If Not wf_Check_for_changes() Then return false
End If
ib_bypass_check_for_changes = False

If Not ib_reinquire_necessary Then
	OpenWithParm(w_pas_instances_inq, This)
	ls_header = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_header) Then return false

	This.SetRedraw(False)
	dw_header.Reset()
	dw_header.ImportString(ls_header)
	This.SetRedraw(True)
	ib_initial_inquire = true
Else
	ib_reinquire_necessary = False
	ib_initial_inquire = false
End if

// can we assume that plant et al are valid???

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

ls_input = dw_header.GetItemString(1, "plant_code")  + '~t'
ls_input += dw_header.GetItemString(1, "fab_product_code") + '~t'
ls_input += dw_header.GetItemString(1, "product_state") + '~t'
ls_input += dw_header.GetItemString(1, "product_status") + '~t'
ls_input += String(dw_header.GetItemNumber(1, "char_key"), '0000') + '~t'
ls_input += dw_header.GetItemString(1, "shift") + '~t'

idt_inquire_date = dw_header.GetItemDate(1, 'begin_date')

//if idt_inquire_date < Today() then
//	MessageBox("Instances Inquire Previous Date", "Warning - you are inquiring on a date prior to current date")
//end if

ls_input += String(idt_inquire_date, 'yyyy-mm-dd') + '~r~n'

IF idt_inquire_date < Today() Then
	ib_date_protection = True
Else
	ib_date_protection = False
End If

wf_protect(dw_header.GetItemString(1, "display_status"), tab_1.selectedtab)


istr_error_info.se_event_name = "wf_retrieve"

This.SetRedraw(False)

// This is definitely a PowerBuilder 8.0.1.90???? hack.  This needs to be fixed.  REM.
	ib_hs  = Profileint(gw_base_frame.is_userini, "InstanceHScroll", "ib_hs",  1) = 1
		
	if tab_1.tabpage_inst.dw_instances.hsplitscroll <> ib_hs then
		tab_1.tabpage_inst.dw_instances.dataobject = ""
		tab_1.tabpage_inst_parm.dw_parameters.dataobject = ""	
		tab_1.tabpage_inst.dw_instances.dataobject = "d_pas_instances"
		tab_1.tabpage_inst_parm.dw_parameters.dataobject = "d_pas_inst_parms"
	
		tab_1.tabpage_inst.dw_instances.hsplitscroll = ib_hs 
		tab_1.tabpage_inst_parm.dw_parameters.hsplitscroll = ib_hs 
	end if
//

tab_1.tabpage_inst.dw_instances.Reset()
dnv_instances.Reset()
ids_original_instances.Reset()

tab_1.tabpage_inst_parm.dw_parameters.Reset()
dnv_parameters.Reset()

tab_1.tabpage_auto_conv.dw_auto_conv.Reset()
dnv_auto_conv.Reset()

dnv_ratios.Reset()

//
//If Not iu_pas202.nf_inq_instances(istr_error_info, &
//												ls_input, &
//												ls_steps, &
//												ls_instances, &
//												ls_parameters, &
//												ls_auto_conv, &
//												ls_valid_plants, &
//												ls_piece_count, &
//												is_timestamp_string, &
//												ls_ratios) Then 

If Not iu_ws_pas1.nf_pasp15fr(istr_error_info, &
												ls_input, &
												ls_steps, &
												ls_instances, &
												ls_parameters, &
												ls_auto_conv, &
												ls_valid_plants, &
												ls_piece_count, &
												is_timestamp_string, &
												ls_ratios, &
												ls_instances_end_date) Then 

	This.SetRedraw(True)
	return False
End if

// Clear out everything

li_ret = tab_1.tabpage_inst.dw_instances.ImportString(ls_steps)
tab_1.tabpage_inst.dw_instances.Sort()

li_ret = tab_1.tabpage_inst_parm.dw_parameters.ImportString(ls_steps)

tab_1.tabpage_inst_parm.dw_parameters.Sort()

ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

li_ret = tab_1.tabpage_auto_conv.dw_auto_conv.ImportString(ls_steps)
tab_1.tabpage_auto_conv.dw_auto_conv.Sort()

ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

wf_import_plants(ls_valid_plants)

ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

//ls_parameters = 	'0003	1997-10-01	2999-12-31	   	015.00	015.00~r~n' + &
//						'0011	1997-10-01	2999-12-31	   	015.00	015.00~r~n' + &
//						'0033	1997-10-01	2999-12-31	   	011.00	011.00~r~n' + &
//						'0046	1997-10-01	2999-12-31	   	005.00	005.00~r~n' + &
//						'0047	1997-10-01	2999-12-31	   	054.00	054.00'


dnv_instances.ImportString(ls_instances)
ids_original_instances.ImportString(ls_instances)

dnv_parameters.ImportString(ls_parameters)
dnv_auto_conv.ImportString(ls_auto_conv)
dnv_ratios.ImportString(ls_ratios)

ids_piece_count.Reset()
ids_piece_count.ImportString(ls_piece_count)

idt_instances_end_date = Date(ls_instances_end_date)

/*********************************************************
**  Set Column Headings
*********************************************************/

ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

This.wf_Reset_Title_Text()
If dw_header.GetItemString(1, "display_status") = 'W' Then
	wf_long_term_day_of_week()
Else
	If dw_header.GetItemString(1, "display_status") = 'L' Then
		wf_Long_Term()
	Else
		If dw_header.GetItemString(1, "display_status") = 'T' Then
			wf_two_week_daily()
		Else
			If dw_header.GetItemString(1, "display_status") = 'H' Then
				wf_three_week_daily()
			Else
				wf_daily()
			End If
		End if
	End if
End If

//ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

wf_fill_values(dw_header.GetItemString(1, "display_status"))

ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

ids_original_instances_dw_data.Reset()
ids_original_instances_dw_data.ImportString(ls_temp)

This.tab_1.tabpage_inst.dw_instances.Event ue_set_hspscroll()

ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

This.wf_Reset_Title_Text_parm()
If dw_header.GetItemString(1, "display_status") = 'W' Then
	wf_Long_Term_parm()
Else	
	If dw_header.GetItemString(1, "display_status") = 'L' Then
		wf_Long_Term_parm()
	Else
		If dw_header.GetItemString(1, "display_status") = 'T' Then
			wf_two_week_parm()
		Else
			If dw_header.GetItemString(1, "display_status") = 'H' Then
				wf_three_week_parm()
			Else		
				wf_daily_parm()
			End If
		End If
	End if
End If

wf_fill_values_parm()

This.tab_1.tabpage_inst_parm.dw_parameters.Event ue_set_hspscroll()

ls_temp = tab_1.tabpage_inst_parm.dw_parameters.Object.DataWindow.Data

This.wf_Reset_Title_Text_auto_conv()

If dw_header.GetItemString(1, "display_status") = 'W' Then
	wf_Long_Term_auto_conv()
Else
	If dw_header.GetItemString(1, "display_status") = 'L' Then
		wf_Long_Term_auto_conv()
	Else
		If dw_header.GetItemString(1, "display_status") = 'T' Then
			wf_two_week_auto_conv()	
		Else
			If dw_header.GetItemString(1, "display_status") = 'H' Then
				wf_three_week_auto_conv()
			Else
				wf_daily_auto_conv()
			End If
		End If
	End if
End if

wf_fill_values_auto_conv()

This.tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_set_hspscroll()

wf_shade_rows()	
	
tab_1.tabpage_inst.dw_instances.ResetUpdate()
tab_1.tabpage_inst_parm.dw_parameters.ResetUpdate()
tab_1.tabpage_auto_conv.dw_auto_conv.ResetUpdate()
This.SetRedraw(True)

if (tab_1.SelectedTab = 1) and ib_initial_inquire then
	wf_rightsideclicked()
end if

ib_initial_inquire = false

SetMicroHelp("Inquire Successful")
return true

end function

public function boolean wf_get_step (ref string as_product_code, ref integer ai_step_sequence, ref string as_product_state, ref string as_product_status);as_product_code = dw_header.GetItemString(1, "fab_product_code")
as_product_state = dw_header.GetItemString(1, "product_state")
as_product_status = dw_header.GetItemString(1, "product_status")
If iw_frame.iu_string.nf_IsEmpty(as_product_code) Then
	return false
End If

If is_tab_index = '1' then
    ai_step_sequence = tab_1.tabpage_inst.dw_instances.GetItemNumber(tab_1.tabpage_inst.dw_instances.GetRow(), "mfg_step_Sequence")
else 
    ai_step_sequence = tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(tab_1.tabpage_inst_parm.dw_parameters.GetRow(), "mfg_step_Sequence")
end if

return true



end function

public function integer wf_update_instances ();Boolean	lb_plant_ok

Decimal	ld_value

dwItemStatus	lst_status

Int	li_counter, &
		li_mfg_step_sequence, &
		li_sub_counter, &
		li_rtn

Long	ll_row, &
		ll_row_count, &
		ll_UpdateRowCount, &
		ll_find1, &
		ll_find2

String	ls_header, &
			ls_quantifier, &
			ls_temp, &
			ls_detail, &
			ls_dest_plant, &
			ls_update, &
			ls_detail_errors, &
			ls_timestamp_string, &
			ls_mfg_step_sequence, &
			ls_find_string

For li_counter = 1 to tab_1.tabpage_inst.dw_instances.Rowcount()
	ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(li_counter, 'detail_errors')
	If pos(ls_detail_errors, 'E') > 0 Then
		tab_1.SelectedTab = 1
		MessageBox('Invalid Instance', 'You must fix all invalid instances before saving.')
		Return -1
	End If
Next
	
if ii_column_number > 30 then
	ii_column_number = 30
end if

For li_counter = 1 to ii_column_number
	If IsNull(tab_1.tabpage_inst.dw_instances.GetItemNumber(1, "sum_" + String(li_counter))) Then
		ld_value = 0.00
	Else
		// Updated 08/21/96 tjb -- had rounding problem with some decimal numbers
		ld_value = Round(tab_1.tabpage_inst.dw_instances.GetItemDecimal(1, "sum_" + String(li_counter)),3)
		If IsNull(ld_value) Then ld_value = 0.00
	End if
	If ld_value <> 100.00 and ld_value <> 0.00 Then
		If dw_header.GetItemString(1, "product_type") = 'S' Then
			// product is an sku
			If ld_value < 100 and ld_value > 0 Then Continue
		End if
		ls_header = tab_1.tabpage_inst.dw_instances.Describe("quantity_" + String(li_counter) + "_t.Text")
		// remove quotes
		ls_header = Left(ls_header, Len(ls_header) - 1)
		ls_header = Right(ls_header, Len(ls_header) - 1)
		If IsDate(Right(ls_header, 10)) Then
			ls_header = Left(ls_header, 10) + " - " + Right(ls_header, 10)
		Else
			ls_header = Left(ls_header, 10)
		End if
		If ld_value > 100 then
			ls_quantifier = "more"
		Else
			ls_quantifier = "less"
		End if
		MessageBox(ls_header, "Instances are " + String(Abs(ld_value - 100)) + " % " + &
						ls_quantifier + " than 100 %.")
		tab_1.SelectedTab = 1
		tab_1.tabpage_inst.dw_instances.SetColumn("quantity_" + String(li_counter))
		tab_1.tabpage_inst.dw_instances.SetFocus()
		return -1
	End if
Next

// validate that all transfer steps have destination plants there
ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
ll_row = tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", 1, ll_row_count)
Do While ll_row > 0 
	lb_plant_ok = False
	If Len(Trim(tab_1.tabpage_inst.dw_instances.GetItemString(ll_row, "destination_plant"))) <> 0 Then
		lb_plant_ok = True
	End if

	// need to loop through to see if there is a value there
	// if there is no values, then blank plant is ok
	If Not lb_plant_ok Then
		// no plant code entered if here
		For li_counter = 1 to ii_column_number 
			If IsNull(tab_1.tabpage_inst.dw_instances.GetItemNumber( ll_row, "quantity_" + String(li_Counter))) Then
				Continue
			End if
			If tab_1.tabpage_inst.dw_instances.GetItemDecimal(ll_row, "quantity_" + String(li_counter)) > 0 Then
				lb_plant_ok = true
				Exit
			End if
		Next
		If lb_plant_ok Then 
			lb_plant_ok = False
		Else
			lb_plant_ok = True
		End If
		
	End if

	If Not lb_plant_ok Then
		MessageBox("Transfer Step", "Transfer Steps must have a valid destination plant.")
		tab_1.tabpage_inst.dw_instances.SetRow(ll_row)
		return -1
	End if

	If ll_row = ll_row_Count Then
		ll_row = 0
	Else
		ll_row = tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", ll_row + 1, ll_row_count)
	End if
Loop

ls_header = dw_header.GetItemString(1, "fab_product_code") + '~t' + &
				dw_header.GetItemString(1, "product_state") + '~t' + &
				dw_header.GetItemString(1, "product_status") + '~t' + &				
				dw_header.GetItemString(1, "plant_code") + '~t' + &
				dw_header.GetItemString(1, "shift") + '~t' + &
				String(dw_header.GetItemNumber(1, "char_key")) + '~t' + &
				String(idt_instances_end_date, "yyyy-mm-dd")

ls_timestamp_string = is_timestamp_string

istr_error_info.se_event_name = "wf_update"

// put all rows into dnv_update_instances
dnv_update_instances.Reset()
ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
For li_counter = 1 to ll_row_count
	li_mfg_step_sequence = tab_1.tabpage_inst.dw_instances.GetItemNumber(li_counter, "mfg_Step_sequence")
	ls_dest_plant = tab_1.tabpage_inst.dw_instances.GetItemString(li_counter, "destination_plant")
	If IsNull(ls_dest_plant) Then ls_dest_plant = ""
	for li_sub_counter = 1 to ii_column_number
		If istr_date_range[li_sub_counter].new_column = False And &
				tab_1.tabpage_inst.dw_instances.GetItemStatus(li_counter, "quantity_" + &
				String(li_sub_counter), Primary!) = NotModified! And &
				tab_1.tabpage_inst.dw_instances.GetItemStatus(li_counter,"destination_plant",Primary!) = &
				NotModified! Then Continue


		If IsNull(li_mfg_step_Sequence) Then li_mfg_step_sequence = 0
		ls_update = String(li_mfg_step_sequence) + '~t'
		ls_temp = String(istr_date_range[li_sub_counter].begin_date, "yyyy-mm-dd")
		If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = '1900-01-01'
		ls_update += ls_temp + '~t'
		ls_temp = String(istr_date_range[li_sub_counter].end_date, "yyyy-mm-dd")
		If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = '1900-01-01'
		ls_update += ls_temp + '~t' + ls_dest_plant + '~t'
		If IsNull(tab_1.tabpage_inst.dw_instances.GetItemNumber(li_counter, "quantity_" + &
				String(li_sub_counter))) Then 
			ls_temp = "0.00" 
		Else
			ls_temp = String(tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_counter, "quantity_" + &
							String(li_sub_counter)), "0.00") 
			If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = "0.00" 
		End if	
		ls_update += ls_temp + '~t' 
		If istr_date_range[li_sub_counter].week_day > 0 Then
			ls_temp = "W" + '~t' + String (istr_date_range[li_sub_counter].week_day)
		Else
			ls_temp = " " + "~t"  + "0"
		End if
		ls_update += ls_temp 
		dnv_update_instances.ImportString(ls_update)
	Next
Next

// make sure that destination plants were not changed
// if a step/plant combination exists in original stuff and not in updated 
// stuff then add a record of zero quantity
ls_update = ""
ll_row_count = dnv_instances.RowCount()
//ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()

For li_counter = 1 to ll_row_Count
	ls_dest_plant = dnv_instances.GetItemString(li_counter, "dest_plant")
	If Not iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then
		li_mfg_step_sequence = dnv_instances.GetItemNumber(li_counter, "mfg_step_Sequence")
		If tab_1.tabpage_inst.dw_instances.Find("mfg_step_sequence = " + String(li_mfg_step_sequence) + &
				" And destination_plant = '" + ls_dest_plant + "'", 1, tab_1.tabpage_inst.dw_instances.RowCount()) &
				= 0 AND ls_update = "" Then
			ls_update += String(li_mfg_step_sequence) + '~t'
			ls_update += String(idt_inquire_date, "yyyy-mm-dd") + '~t'
			ls_update += '2999-12-31~t' + ls_dest_plant + '~t'
			ls_update += '0.00'
			ls_update += '~t'
		End if
	End if
Next

// Have to make a call to remove the old dest plant first
//dnv_update_instances.ImportString(ls_update)
If Not iw_frame.iu_string.nf_isempty(ls_update) Then
//	li_rtn = iu_pas202.nf_upd_instances(istr_error_info, ls_header, ls_update, ls_timestamp_string)
	li_rtn = iu_ws_pas1.nf_pasp16fr(istr_error_info, ls_header+'~t', ls_update, ls_timestamp_string)
	If li_rtn = 0 Then
		tab_1.tabpage_inst.dw_instances.ResetUpdate()
		is_timestamp_string = ls_timestamp_string
	End if
End If


ll_UpdateRowCount = dnv_update_instances.RowCount()

If ll_UpdateRowCount < 1 Then
	ib_no_instance_data = True
	tab_1.tabpage_inst.dw_instances.ResetUpdate()
	Return 0
End if

//If ll_UpdateRowCount > 450 Then
//	MessageBox("Row Count Exceeded", "Row Count was exceeded for update." + &
//					"  Data will NOT be updated. Contact Applications.", StopSign!, OK!)
//	return -1
//End if

ls_detail = dnv_update_instances.object.DataWindow.Data 

For li_counter = 1 to ll_UpdateRowCount
	If dnv_update_instances.GetItemDecimal(li_counter, "amount") = 0 Then
		ls_mfg_step_sequence = String(dnv_update_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
//		ls_dest_plant = dnv_update_instances.GetItemString(li_counter, "dest_plant")
		ls_find_string = "mfg_step_sequence = " + ls_mfg_step_sequence +  " AND (quantity_1 > 0 or quantity_2 > 0 or quantity_3 > 0 or quantity_4 > 0 or " + &
			"quantity_5 > 0 or quantity_6 > 0 or quantity_7 > 0 or quantity_8 > 0 or quantity_9 > 0 or quantity_10 > 0 or quantity_11 > 0 or quantity_12 > 0 or " + &
			"quantity_13 > 0 or quantity_14 > 0 or quantity_15 > 0 or quantity_16 > 0 or quantity_17 > 0 or quantity_18 > 0 or quantity_19 > 0 or quantity_20 > 0 or " + &
			"quantity_21 > 0 or quantity_22 > 0 or quantity_23 > 0 or quantity_24 > 0 or quantity_25 > 0 or quantity_26 > 0 or quantity_27 > 0 or quantity_28 > 0 or " + &
			"quantity_29 > 0 or quantity_30 > 0)" 	
		ll_Find1 = tab_1.tabpage_inst.dw_instances.Find(ls_find_string, 1,tab_1.tabpage_inst.dw_instances.RowCount())
		ll_Find2 = ids_original_instances_dw_data.Find(ls_find_string, 1,tab_1.tabpage_inst.dw_instances.RowCount())
		If ll_Find1 = 0 and ll_Find2 = 0 Then
			dnv_update_instances.DeleteRow(li_counter)
			li_counter --
			ll_UpdateRowCount --
		End If
	End If
Next

ls_detail = dnv_update_instances.object.DataWindow.Data 

//li_rtn = iu_pas202.nf_upd_instances(istr_error_info, ls_header, ls_detail, ls_timestamp_string)
li_rtn = iu_ws_pas1.nf_pasp16fr(istr_error_info, ls_header+'~t', ls_detail, ls_timestamp_string)
If li_rtn >= 0 Then
//	dnv_instances.Reset()
	dnv_instances.ImportString(ls_detail)
	tab_1.tabpage_inst.dw_instances.ResetUpdate()
	is_timestamp_string = ls_timestamp_string
	ids_original_instances_dw_data.Reset()
	ls_temp = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data
	ids_original_instances_dw_data.ImportString(ls_temp)
Else
	tab_1.selectedtab = 1
	Return li_rtn
End if

//dnv_instances.PostEvent("ue_optimize")
// if they switch modes, reinquire now, or it gets kinda goofy
ib_Changes_Made = True

//revgll//
if istr_error_info.se_message > Space(1) and  li_rtn = 0 then 
	iw_frame.SetMicrohelp(" ")
	messagebox("PA Message", "An instance has been modified after the production schedule was generated for " + istr_error_info.se_message + ", shift A.")
end if	

return li_rtn

end function

public function boolean wf_update_parameters ();Boolean	lb_plant_ok

Decimal	ld_value

dwItemStatus	lst_status

Int	li_counter, &
		li_mfg_step_sequence, &
		li_sub_counter

Long	ll_row, &
		ll_row_count, &
		ll_UpdateRowCount, ll_temp

String	ls_header, &
			ls_quantifier, &
			ls_temp, &
			ls_detail, &
			ls_dest_plant, &
			ls_update


// validate that all transfer steps have destination plants there
ll_row_count = tab_1.tabpage_inst_parm.dw_parameters.RowCount()
ll_row = tab_1.tabpage_inst_parm.dw_parameters.Find("step_type = 'T'", 1, ll_row_count)
Do While ll_row > 0 
	lb_plant_ok = False
	If Len(Trim(tab_1.tabpage_inst_parm.dw_parameters.GetItemString(ll_row, "destination_plant"))) <> 0 Then
		lb_plant_ok = True
	End if

	// need to loop through to see if there is a value there
	// if there is no values, then blank plant is ok
	If Not lb_plant_ok Then
		// no plant code entered if here
		For li_counter = 1 to ii_column_number 
			If IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber( ll_row, "min_" + String(li_Counter))) Then
				Continue
			End if
			If tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(ll_row, "min_" + String(li_counter)) > 0 Then
				lb_plant_ok = true
				Exit
			End if
		Next
		If lb_plant_ok Then 
			lb_plant_ok = False
		Else
			lb_plant_ok = True
		End If
	End if

	If Not lb_plant_ok Then
		MessageBox("Transfer Step", "Transfer Steps must have a valid destination plant.")
		tab_1.tabpage_inst_parm.dw_parameters.SetRow(ll_row)
		return false
	End if

	If ll_row = ll_row_Count Then
		ll_row = 0
	Else
		ll_row = tab_1.tabpage_inst_parm.dw_parameters.Find("step_type = 'T'", ll_row + 1, ll_row_count)
	End if
Loop

ls_header = dw_header.GetItemString(1, "fab_product_code") + '~t' + &
				dw_header.GetItemString(1, "product_state") + '~t' + &
				dw_header.GetItemString(1, "product_status") + '~t' + &				
				dw_header.GetItemString(1, "plant_code") + '~t' + &
				dw_header.GetItemString(1, "shift") + '~t' + &
				String(dw_header.GetItemNumber(1, "char_key")) + '~t' + 'I' + '~r~n'

istr_error_info.se_event_name = "wf_update"

// put all rows into dnv_update_parameters
dnv_update_parameters.Reset()
ll_row_count = tab_1.tabpage_inst_parm.dw_parameters.RowCount()
For li_counter = 1 to ll_row_count
	li_mfg_step_sequence = tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(li_counter, "mfg_Step_sequence")
	ls_dest_plant = tab_1.tabpage_inst_parm.dw_parameters.GetItemString(li_counter, "destination_plant")
	If IsNull(ls_dest_plant) Then ls_dest_plant = ""
	for li_sub_counter = 1 to ii_column_number_parm
		If istr_date_range_parm[li_sub_counter].new_column = False And &
				tab_1.tabpage_inst_parm.dw_parameters.GetItemStatus(li_counter, "min_" + &
				String(li_sub_counter), Primary!) = NotModified! And &
				tab_1.tabpage_inst_parm.dw_parameters.GetItemStatus(li_counter, "max_" + &
				String(li_sub_counter), Primary!) = NotModified! And &
				tab_1.tabpage_inst_parm.dw_parameters.GetItemStatus(li_counter,"destination_plant",Primary!) = NotModified! And &
				tab_1.tabpage_inst_parm.dw_parameters.GetItemStatus(li_counter,"group",Primary!) = NotModified! And &
				tab_1.tabpage_inst_parm.dw_parameters.GetItemStatus(li_counter,"priority",Primary!) = NotModified! Then &
					Continue
				
		If IsNull(li_mfg_step_Sequence) Then li_mfg_step_sequence = 0
		ls_update = String(li_mfg_step_sequence) + '~t'
		ls_temp = String(istr_date_range_parm[li_sub_counter].begin_date, "yyyy-mm-dd")
		If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = '1900-01-01'
		ls_update += ls_temp + '~t'
		ls_temp = String(istr_date_range_parm[li_sub_counter].end_date, "yyyy-mm-dd")
		If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = '1900-01-01'
		ls_update += ls_temp + '~t' + ls_dest_plant + '~t'
		
		If IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(li_counter, "min_" + &
				String(li_sub_counter))) Then 
			ls_temp = "0.00"
		Else
			ls_temp = String(tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(li_counter, "min_" + &
							String(li_sub_counter)), "0.00")
			If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = "0.00"
		End if	
		ls_update += ls_temp + '~t'
		
		If IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(li_counter, "max_" + &
				String(li_sub_counter))) Then 
			ls_temp = "0.00"
		Else
			ls_temp = String(tab_1.tabpage_inst_parm.dw_parameters.GetItemDecimal(li_counter, "max_" + &
								String(li_sub_counter)), "0.00")
		End If
		ls_update += ls_temp + '~t'
		
		If IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemString(li_counter, "group")) Then
			ls_temp = ''
		Else
			ls_temp = tab_1.tabpage_inst_parm.dw_parameters.GetItemString(li_counter, "group")
		End If
		
		ls_update += ls_temp + '~t'
		
		If IsNull(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(li_counter, "priority")) Then
			ls_temp = '0'
		Else
			ls_temp = String(tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(li_counter, "priority"))
		End If
		
		ls_update += ls_temp + '~r~n'
		
		dnv_update_parameters.ImportString(ls_update)
	Next
Next

// make sure that destination plants were not changed
// if a step/plant combination exists in original stuff and not in updated 
// stuff then add a record of zero quantity
ls_update = ""
ll_row_count = dnv_parameters.RowCount()
For li_counter = 1 to ll_row_Count
	ls_dest_plant = dnv_parameters.GetItemString(li_counter, "dest_plant")
	If Not iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then 
		li_mfg_step_sequence = dnv_parameters.GetItemNumber(li_counter, "mfg_step_Sequence")
		If tab_1.tabpage_inst_parm.dw_parameters.Find("mfg_step_sequence = " + String(li_mfg_step_sequence) + &
				" And destination_plant = '" + ls_dest_plant + "'", 1, tab_1.tabpage_inst_parm.dw_parameters.RowCount()) &
				= 0 Then
			ls_update += String(li_mfg_step_sequence) + '~t'
			ls_update += String(idt_inquire_date, "yyyy-mm-dd") + '~t'
			ls_update += '2999-12-31~t' + ls_dest_plant + '~t'
			ls_update += '0.00~t0.00~t'
			ls_update += dnv_parameters.GetItemString(li_counter, "group") + '~t'
			ls_update += string(dnv_parameters.GetItemNumber(li_counter, "priority")) + '~r~n'
		End if
	End if
Next

// Have to make a call to remove the old dest plant first
//dnv_update_parameters.ImportString(ls_update)
If Not iw_frame.iu_string.nf_isempty(ls_update) Then
	//If iu_pas203.nf_pasp66br_upd_instance_parms(istr_error_info, ls_header, ls_update) = 0 Then
	If iu_ws_pas3.uf_pasp66fr(istr_error_info, ls_header, ls_update) = 0 Then
	//	dnv_parameters.Reset()
	//	dnv_parameters.ImportString(ls_update)
		tab_1.tabpage_inst_parm.dw_parameters.ResetUpdate()
	End if
End If


ll_UpdateRowCount = dnv_update_parameters.RowCount()

If ll_UpdateRowCount < 1 Then
	ib_no_parameter_data = True
	Return True
End if

If ll_UpdateRowCount > 450 Then
	MessageBox("Row Count Exceeded", "Row Count was exceeded for update." + &
					"  Data will NOT be updated. Contact Applications.", StopSign!, OK!)
	return False
End if
ls_detail = dnv_update_parameters.object.DataWindow.Data + '~r~n'

//If iu_pas203.nf_pasp66br_upd_instance_parms(istr_error_info, ls_header, ls_detail) = 0 Then
If iu_ws_pas3.uf_pasp66fr(istr_error_info, ls_header, ls_detail) = 0 Then
	dnv_parameters.ImportString(ls_detail)
	ll_temp = tab_1.tabpage_inst_parm.dw_parameters.ResetUpdate()
Else
	tab_1.selectedtab = 2
	Return False
End if

//dnv_parameters.PostEvent("ue_optimize")
// if they switch modes, reinquire now, or it gets kinda goofy
ib_Changes_Made = True

return true
end function

public function boolean wf_update_auto_conv ();Boolean	lb_plant_ok

Decimal	ld_value

dwItemStatus	lst_status

Int	li_counter, &
		li_mfg_step_sequence, &
		li_sub_counter

Long	ll_row, &
		ll_row_count, &
		ll_UpdateRowCount, ll_temp

String	ls_header, &
			ls_quantifier, &
			ls_temp, &
			ls_detail, &
			ls_dest_plant, &
			ls_update


// validate that all transfer steps have destination plants there
ll_row_count = tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()
ll_row = tab_1.tabpage_auto_conv.dw_auto_conv.Find("step_type = 'T'", 1, ll_row_count)
Do While ll_row > 0 
	lb_plant_ok = False
	If Len(Trim(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemString(ll_row, "destination_plant"))) <> 0 Then
		lb_plant_ok = True
	End if

	// need to loop through to see if there is a value there
	// if there is no values, then blank plant is ok
	If Not lb_plant_ok Then
		// no plant code entered if here
		For li_counter = 1 to ii_column_number 
			If IsNull(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber( ll_row, "min_" + String(li_Counter))) Then
				Continue
			End if
			If tab_1.tabpage_auto_conv.dw_auto_conv.GetItemDecimal(ll_row, "min_" + String(li_counter)) > 0 Then
				lb_plant_ok = true
				Exit
			End if
		Next
		If lb_plant_ok Then 
			lb_plant_ok = False
		Else
			lb_plant_ok = True
		End If
	End if

	If Not lb_plant_ok Then
		MessageBox("Transfer Step", "Transfer Steps must have a valid destination plant.")
		tab_1.tabpage_auto_conv.dw_auto_conv.SetRow(ll_row)
		return false
	End if

	If ll_row = ll_row_Count Then
		ll_row = 0
	Else
		ll_row = tab_1.tabpage_auto_conv.dw_auto_conv.Find("step_type = 'T'", ll_row + 1, ll_row_count)
	End if
Loop

ls_header = dw_header.GetItemString(1, "fab_product_code") + '~t' + &
				dw_header.GetItemString(1, "product_state") + '~t' + &
				dw_header.GetItemString(1, "product_status") + '~t' + &				
				dw_header.GetItemString(1, "plant_code") + '~t' + &
				dw_header.GetItemString(1, "shift") + '~t' + &
				String(dw_header.GetItemNumber(1, "char_key")) + '~t' + 'A' + '~r~n'

istr_error_info.se_event_name = "wf_update"

// put all rows into dnv_update_auto_conv
dnv_update_auto_conv.Reset()
ll_row_count = tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()
For li_counter = 1 to ll_row_count
	li_mfg_step_sequence = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(li_counter, "mfg_Step_sequence")
	ls_dest_plant = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemString(li_counter, "destination_plant")
	If IsNull(ls_dest_plant) Then ls_dest_plant = ""
	for li_sub_counter = 1 to ii_column_number_auto_conv
		If istr_date_range_auto_conv[li_sub_counter].new_column = False And &
				tab_1.tabpage_auto_conv.dw_auto_conv.GetItemStatus(li_counter, "min_" + &
				String(li_sub_counter), Primary!) = NotModified! And &
				tab_1.tabpage_auto_conv.dw_auto_conv.GetItemStatus(li_counter, "max_" + &
				String(li_sub_counter), Primary!) = NotModified! And &
				tab_1.tabpage_auto_conv.dw_auto_conv.GetItemStatus(li_counter,"destination_plant",Primary!) = NotModified! And &
				tab_1.tabpage_auto_conv.dw_auto_conv.GetItemStatus(li_counter,"group",Primary!) = NotModified! And &
				tab_1.tabpage_auto_conv.dw_auto_conv.GetItemStatus(li_counter,"priority",Primary!) = NotModified! Then &
					Continue

		If IsNull(li_mfg_step_Sequence) Then li_mfg_step_sequence = 0
		ls_update = String(li_mfg_step_sequence) + '~t'
		ls_temp = String(istr_date_range_auto_conv[li_sub_counter].begin_date, "yyyy-mm-dd")
		If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = '1900-01-01'
		ls_update += ls_temp + '~t'
		ls_temp = String(istr_date_range_auto_conv[li_sub_counter].end_date, "yyyy-mm-dd")
		If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = '1900-01-01'
		ls_update += ls_temp + '~t' + ls_dest_plant + '~t'
		
		If IsNull(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(li_counter, "min_" + &
				String(li_sub_counter))) Then 
			ls_temp = "0.00"
		Else
			ls_temp = String(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemDecimal(li_counter, "min_" + &
							String(li_sub_counter)), "0.00")
			If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then ls_temp = "0.00"
		End if	
		ls_update += ls_temp + '~t'
		
		If IsNull(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(li_counter, "max_" + &
				String(li_sub_counter))) Then 
			ls_temp = "0.00"
		Else
			ls_temp = String(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemDecimal(li_counter, "max_" + &
								String(li_sub_counter)), "0.00")
		End If
		
		ls_update += ls_temp + '~t'
		
		If IsNull(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemString(li_counter, "group")) Then
			ls_temp = ''
		Else
			ls_temp = tab_1.tabpage_auto_conv.dw_auto_conv.GetItemString(li_counter, "group")
		End If
		
		ls_update += ls_temp + '~t'
		
		If IsNull(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(li_counter, "priority")) Then
			ls_temp = '0'
		Else
			ls_temp = String(tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(li_counter, "priority"))
		End If
		
		ls_update += ls_temp + '~r~n'
		
		dnv_update_auto_conv.ImportString(ls_update)
	Next
Next

// make sure that destination plants were not changed
// if a step/plant combination exists in original stuff and not in updated 
// stuff then add a record of zero quantity
ls_update = ""
ll_row_count = dnv_auto_conv.RowCount()
For li_counter = 1 to ll_row_Count
	ls_dest_plant = dnv_auto_conv.GetItemString(li_counter, "dest_plant")
	If Not iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then
		li_mfg_step_sequence = dnv_auto_conv.GetItemNumber(li_counter, "mfg_step_Sequence")
		If tab_1.tabpage_auto_conv.dw_auto_conv.Find("mfg_step_sequence = " + String(li_mfg_step_sequence) + &
				" And destination_plant = '" + ls_dest_plant + "'", 1, tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()) &
				= 0 Then
			ls_update += String(li_mfg_step_sequence) + '~t'
			ls_update += String(idt_inquire_date, "yyyy-mm-dd") + '~t'
			ls_update += '2999-12-31~t' + ls_dest_plant + '~t'
			ls_update += '0.00~t0.00~t'
			ls_update += dnv_auto_conv.GetItemString(li_counter, "group") + '~t'
			ls_update += string(dnv_auto_conv.GetItemString(li_counter, "priority")) + '~r~n'
		End if
	End if
Next

// Have to make a call to remove the old dest plant first
//dnv_update_auto_conv.ImportString(ls_update)
If Not iw_frame.iu_string.nf_isempty(ls_update) Then
	//If iu_pas203.nf_pasp66br_upd_instance_parms(istr_error_info, ls_header, ls_update) = 0 Then
	If iu_ws_pas3.uf_pasp66fr(istr_error_info, ls_header, ls_update) = 0 Then
	//	dnv_auto_conv.Reset()
	//	dnv_auto_conv.ImportString(ls_update)
		tab_1.tabpage_auto_conv.dw_auto_conv.ResetUpdate()
	End if
End If


ll_UpdateRowCount = dnv_update_auto_conv.RowCount()

If ll_UpdateRowCount < 1 Then
	ib_no_auto_conv_data = True
	Return True
End if

If ll_UpdateRowCount > 450 Then
	MessageBox("Row Count Exceeded", "Row Count was exceeded for update." + &
					"  Data will NOT be updated. Contact Applications.", StopSign!, OK!)
	return False
End if
ls_detail = dnv_update_auto_conv.object.DataWindow.Data + '~r~n'

//If iu_pas203.nf_pasp66br_upd_instance_parms(istr_error_info, ls_header, ls_detail) = 0 Then
If iu_ws_pas3.uf_pasp66fr(istr_error_info, ls_header, ls_detail) = 0 Then
	dnv_auto_conv.ImportString(ls_detail)
	ll_temp = tab_1.tabpage_auto_conv.dw_auto_conv.ResetUpdate()
Else
	tab_1.selectedtab = 3
	Return False
End if

//dnv_auto_conv.PostEvent("ue_optimize")
// if they switch modes, reinquire now, or it gets kinda goofy
ib_Changes_Made = True

return true
end function

public subroutine wf_rightsideclicked ();constant long MK_LBUTTON  = 1
constant long WM_LBUTTONDOWN = 513
constant long WM_LBUTTONUP  = 514
constant long HEX$10000   = 65536

long ll_handle, ll_xpos, ll_ypos, ll_packedpos

// get handle to datawindow
ll_handle = Handle ( tab_1.tabpage_inst.dw_instances )

// pack coordinates 
If tab_1.tabpage_inst.dw_instances.Find("step_type = 'T'", 1, 10000) > 0 Then 
	ll_xpos =286
else
	ll_xpos =255
end if

ll_ypos = 63
ll_packedpos = ( ll_ypos * HEX$10000 ) + ll_xpos

// emulate mouse click on right side of splitbar
SendMessage ( ll_handle, WM_LBUTTONDOWN, MK_LBUTTON, ll_packedpos )


end subroutine

public subroutine wf_rightsideclicked (long al_xpos, long al_ypos, string as_dwname);constant long MK_LBUTTON  = 1
constant long WM_LBUTTONDOWN = 513
constant long WM_LBUTTONUP  = 514
constant long HEX$10000   = 65536

long ll_handle, ll_packedpos

// get handle to datawindow
if as_dwname = "dw_instances" then
	ll_handle = Handle ( tab_1.tabpage_inst.dw_instances )
else
	if as_dwname = "dw_parameters" then
		ll_handle = Handle (tab_1.tabpage_inst_parm.dw_parameters)
	else
		if as_dwname = "dw_auto_conv" then
			ll_handle = Handle (tab_1.tabpage_auto_conv.dw_auto_conv)
		end if
	end if
end if

ll_packedpos = ( al_ypos * HEX$10000 ) + al_xpos

// emulate mouse click on right side of splitbar
SendMessage ( ll_handle, WM_LBUTTONDOWN, MK_LBUTTON, ll_packedpos )


end subroutine

public subroutine wf_shade_rows ();String		ls_modify, ls_detail_errors, ls_detail_errors_new, ls_new_val, ls_old_val, ls_new_protection_status

Integer	li_row, li_col, li_instances_rowcount, li_parameters_rowcount, li_auto_conv_rowcount

If cbx_shade_rows.Checked = True Then
	ls_modify = "datawindow.detail.color='536870912~tif(mod(getrow(),2) =0, 536870912, 16777190)' "
Else
	ls_modify = "datawindow.detail.Color= 536870912" 
End If

tab_1.tabpage_inst.dw_instances.Modify(ls_modify)
tab_1.tabpage_inst_parm.dw_parameters.Modify(ls_modify)
tab_1.tabpage_auto_conv.dw_auto_conv.Modify(ls_modify)

li_instances_rowcount =  tab_1.tabpage_inst.dw_instances.RowCount()
For li_row = 1 to li_instances_rowcount
	If mod(li_row,2) = 1 Then
		For li_col = 1 to 30
			 ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "detail_errors")
			 ls_old_val = mid(ls_detail_errors, li_col, 1)
			 If ls_old_val > ' ' Then
				If ls_old_val = 'S' Then
					ls_new_val = ' '	
				Else	
					ls_new_val = ls_old_val
				End If
			Else
				 If cbx_shade_rows.Checked = True Then 
					ls_new_val = 'S'
				else
					ls_new_val = ' '
				end if
			end if
			
			If li_col = 1 Then
				ls_detail_errors_new = ls_new_val + mid(ls_detail_errors,2,29)
			Else	
				ls_detail_errors_new = mid(ls_detail_errors,1,li_col - 1) + ls_new_val + mid(ls_detail_errors, li_col + 1, 30 - li_col)
			End If
			tab_1.tabpage_inst.dw_instances.SetItem(li_row, "detail_errors", ls_detail_errors_new)
		Next
	End If	
Next

li_parameters_rowcount =  tab_1.tabpage_inst_parm.dw_parameters.RowCount()
For li_row = 1 to li_parameters_rowcount
	If mod(li_row,2) = 1 Then
		If cbx_shade_rows.Checked = True Then	
			ls_new_protection_status = 'S'
		Else	
			ls_new_protection_status = ' '
		End If
		tab_1.tabpage_inst_parm.dw_parameters.SetItem(li_row, "protection_status", ls_new_protection_status)
	End If	
Next

li_auto_conv_rowcount =  tab_1.tabpage_auto_conv.dw_auto_conv.RowCount()
For li_row = 1 to li_auto_conv_rowcount
	If mod(li_row,2) = 1 Then
		If cbx_shade_rows.Checked = True Then	
			ls_new_protection_status = 'S'
		Else	
			ls_new_protection_status = ' '
		End If
		tab_1.tabpage_auto_conv.dw_auto_conv.SetItem(li_row, "protection_status", ls_new_protection_status)
	End If	
Next

tab_1.tabpage_inst.dw_instances.ResetUpdate()
tab_1.tabpage_inst_parm.dw_parameters.ResetUpdate()
tab_1.tabpage_auto_conv.dw_auto_conv.ResetUpdate()
	
end subroutine

public function boolean wf_two_week_daily ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp, &
		ldt_duration_begin_date, &
		ldt_duration_end_date, &
		ldt_instance_date, &
		ldt_header_start_date, &
		ldt_first_monday, &
		ldt_first_sunday, &
		ldt_prev_monday, &
		ldt_new_begin_date

Int	li_counter, &
		li_days_after, &
		li_instances_rowcount, &
		li_row, li_col, li_duration_days, &
		li_start_day_number, li_new_row

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_TitleModify, &
			ls_detail_errors, &
			ls_detail_errors_new
			

SetPointer(HourGlass!)

ii_start_column = 1
ii_column_number = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_TitleModify = ''

For li_counter = 0 to 13
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number ++
	istr_date_range[ii_column_number].begin_date = ldt_temp 
	istr_date_range[ii_column_number].end_date   = ldt_temp 

	ls_TitleModify += "quantity_" + String(ii_column_number) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yy") + "~~r~~n" + left(DayName(ldt_temp),3) + "' " + &
							"quantity_" + String(ii_column_number) + "_t.Visible = 1 " + &
							"quantity_" + String(ii_column_number) + "_t.BackGround.Color = 78682240 " + &
							"quantity_" + String(ii_column_number) + ".Visible = 1 "

Next	

tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)

// Clear Values for Piece Count
tab_1.tabpage_inst.dw_instances.Object.total_1.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_2.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_3.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_4.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_5.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_6.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_7.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_8.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_9.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_10.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_11.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_12.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_13.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_14.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_8.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_9.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_10.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_11.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_12.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_13.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_14.Visible = True
tab_1.tabpage_inst.dw_instances.Object.total_15.Visible = False 
tab_1.tabpage_inst.dw_instances.Object.total_16.Visible = False 
tab_1.tabpage_inst.dw_instances.Object.total_17.Visible = False 
tab_1.tabpage_inst.dw_instances.Object.total_18.Visible = False 
tab_1.tabpage_inst.dw_instances.Object.total_19.Visible = False 
tab_1.tabpage_inst.dw_instances.Object.total_20.Visible = False 
tab_1.tabpage_inst.dw_instances.Object.total_21.Visible = False 

li_days_after = DaysAfter(idt_inquire_date, ldt_start)
ll_row_count = ids_piece_count.RowCount()

If li_days_after >= 0 Then
	For li_counter = 1 to 14
		If li_counter + li_days_after <= ll_row_count Then
			ls_temp = "total_" + String(li_counter) + ".Text = '" + String( &
					ids_piece_count.GetItemNumber(li_counter + li_days_after, 'piece_count')) + "'"
			ls_ret = tab_1.tabpage_inst.dw_instances.Modify(ls_temp)
			If Len(ls_ret) > 0 Then MessageBox("Modify", ls_ret + '~r~n' + ls_temp)
		End if
	Next
End if

// new routine to handle patterns

ldt_header_start_date = dw_header.GetItemDate(1, "begin_date")

ls_temp = dnv_instances.Object.DataWindow.Data

li_start_day_number = DayNumber(dw_header.GetItemDate(1, "begin_date"))
Choose Case li_start_day_number
	Case 1  
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 1)
	Case 2
		ldt_first_monday = dw_header.GetItemDate(1, "begin_date")
	Case 3
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 6)
	Case 4
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 5)
	Case 5
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 4)
	Case 6
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 3)
	Case 7
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 2)
End Choose
ldt_first_sunday = RelativeDate(ldt_first_monday, -1)
ldt_prev_monday = RelativeDate(ldt_first_monday, -7)

ll_row_count = dnv_instances.RowCount()
For li_counter = 1 to ll_row_count
	If (dnv_instances.GetItemString(li_counter, "pattern_type") = "W") Then
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") - 1)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,14)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") + 6)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,14)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +6))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +6))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If		
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") + 13)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,14)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +13))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +13))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If		

		dnv_instances.DeleteRow(li_counter)
		ll_row_count --
		li_counter --		
	End If
Next

//end pattern routine

If dw_header.GetItemDate(1, "begin_date") < Today() Then
	li_instances_rowcount =  tab_1.tabpage_inst.dw_instances.RowCount()
	For li_row = 1 to li_instances_rowcount
		If tab_1.tabpage_inst.dw_instances.GetItemString (li_row, "step_type") <> 'F' Then
			li_duration_days = Int(tab_1.tabpage_inst.dw_instances.GetItemNumber(li_row, "step_duration") / 3)
			If (li_duration_days >= 1) OR (tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "dump_product_ind") = "Y") Then
				ldt_duration_end_date = RelativeDate(Today(), -1)	
				ldt_duration_begin_date =  RelativeDate(Today(), 0 - li_duration_days)	
				For li_col = 1 to 30
					ldt_instance_date = Date(mid(tab_1.tabpage_inst.dw_instances.Describe('quantity_' + &
							String(li_col) + '_t.text'), 2, 10))
					If ((ldt_instance_date >= ldt_duration_begin_date) and (ldt_instance_date <= ldt_duration_end_date)) OR &
							((tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "dump_product_ind") = "Y") and (ldt_instance_date < Today())) Then
//						MessageBox("Unprotect Row Column", "Unprotect row = " + string(li_row) + " column= " + string(li_col) + " instance date= " + string(ldt_instance_date,"mm/dd/yyyy")	)
						 ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "detail_errors")
						 if li_col = 1 then
							ls_detail_errors_new = 'U' + mid(ls_detail_errors,2,29)
						else	
							ls_detail_errors_new = mid(ls_detail_errors,1,li_col - 1) + 'U' + mid(ls_detail_errors, li_col + 1, 30 - li_col)
						End If
						tab_1.tabpage_inst.dw_instances.SetItem(li_row, "detail_errors", ls_detail_errors_new)
					End If
				Next
			End If
		End if
	Next
End If

tab_1.tabpage_inst.dw_instances.Object.DataWindow.Header.Height = 209

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')


return True
end function

public function boolean wf_two_week_parm ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp

Int	li_counter, &
		li_days_after

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_Parm_TitleModify


SetPointer(HourGlass!)

ii_start_column_parm = 1
ii_column_number_parm = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_parm_TitleModify = ''

For li_counter = 0 to 13
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number_parm ++
	istr_date_range_parm[ii_column_number_parm].begin_date = ldt_temp 
	istr_date_range_parm[ii_column_number_parm].end_date   = ldt_temp 

	ls_Parm_TitleModify += "parm_" + String(ii_column_number_parm) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yyyy") + "~~r~~n" + DayName(ldt_temp) + "' " + &
						"parm_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"max_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_parm) + ".Visible = 1 " + &
						"max_" + String(ii_column_number_parm) + ".Visible = 1 " 
Next	

tab_1.tabpage_inst_parm.dw_parameters.Modify(ls_Parm_TitleModify)

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')

return True
end function

public function boolean wf_two_week_auto_conv ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp

Int	li_counter, &
		li_days_after

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_Auto_Conv_TitleModify


SetPointer(HourGlass!)

ii_start_column_auto_conv = 1
ii_column_number_auto_conv = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_Auto_Conv_TitleModify = ''

For li_counter = 0 to 13
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number_auto_conv ++
	istr_date_range_auto_conv[ii_column_number_auto_conv].begin_date = ldt_temp 
	istr_date_range_auto_conv[ii_column_number_auto_conv].end_date   = ldt_temp 

	ls_Auto_Conv_TitleModify += "parm_" + String(ii_column_number_auto_conv) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yyyy") + "~~r~~n" + DayName(ldt_temp) + "' " + &
						"parm_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"max_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_auto_conv) + ".Visible = 1 " + &
						"max_" + String(ii_column_number_auto_conv) + ".Visible = 1 " 
Next	

tab_1.tabpage_auto_conv.dw_auto_conv.Modify(ls_Auto_Conv_TitleModify)

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')

return True
end function

public function boolean wf_three_week_daily ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp, &
		ldt_duration_begin_date, &
		ldt_duration_end_date, &
		ldt_instance_date, &
		ldt_header_start_date, &
		ldt_first_monday, &
		ldt_first_sunday, &
		ldt_prev_monday, &
		ldt_new_begin_date

Int	li_counter, &
		li_days_after, &
		li_instances_rowcount, &
		li_row, li_col, li_duration_days, &
		li_start_day_number, li_new_row

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_TitleModify, &
			ls_detail_errors, &
			ls_detail_errors_new
			

SetPointer(HourGlass!)

ii_start_column = 1
ii_column_number = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_TitleModify = ''

For li_counter = 0 to 20
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number ++
	istr_date_range[ii_column_number].begin_date = ldt_temp 
	istr_date_range[ii_column_number].end_date   = ldt_temp 

	ls_TitleModify += "quantity_" + String(ii_column_number) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yy") + "~~r~~n" + left(DayName(ldt_temp),3) + "' " + &
							"quantity_" + String(ii_column_number) + "_t.Visible = 1 " + &
							"quantity_" + String(ii_column_number) + "_t.BackGround.Color = 78682240 " + &
							"quantity_" + String(ii_column_number) + ".Visible = 1 "

Next	

tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)

// Clear Values for Piece Count
tab_1.tabpage_inst.dw_instances.Object.total_1.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_2.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_3.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_4.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_5.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_6.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_7.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_8.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_9.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_10.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_11.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_12.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_13.Text = '' 
tab_1.tabpage_inst.dw_instances.Object.total_14.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_15.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_16.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_17.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_18.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_19.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_20.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_21.Text = ''
tab_1.tabpage_inst.dw_instances.Object.total_8.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_9.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_10.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_11.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_12.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_13.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_14.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_15.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_16.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_17.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_18.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_19.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_20.Visible = True 
tab_1.tabpage_inst.dw_instances.Object.total_21.Visible = True 

li_days_after = DaysAfter(idt_inquire_date, ldt_start)
ll_row_count = ids_piece_count.RowCount()

If li_days_after >= 0 Then
	For li_counter = 1 to 21
		If li_counter + li_days_after <= ll_row_count Then
			ls_temp = "total_" + String(li_counter) + ".Text = '" + String( &
					ids_piece_count.GetItemNumber(li_counter + li_days_after, 'piece_count')) + "'"
			ls_ret = tab_1.tabpage_inst.dw_instances.Modify(ls_temp)
			If Len(ls_ret) > 0 Then MessageBox("Modify", ls_ret + '~r~n' + ls_temp)
		End if
	Next
End if

// new routine to handle patterns

ldt_header_start_date = dw_header.GetItemDate(1, "begin_date")

ls_temp = dnv_instances.Object.DataWindow.Data

li_start_day_number = DayNumber(dw_header.GetItemDate(1, "begin_date"))
Choose Case li_start_day_number
	Case 1  
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 1)
	Case 2
		ldt_first_monday = dw_header.GetItemDate(1, "begin_date")
	Case 3
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 6)
	Case 4
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 5)
	Case 5
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 4)
	Case 6
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 3)
	Case 7
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 2)
End Choose
ldt_first_sunday = RelativeDate(ldt_first_monday, -1)
ldt_prev_monday = RelativeDate(ldt_first_monday, -7)

ll_row_count = dnv_instances.RowCount()
For li_counter = 1 to ll_row_count
	If (dnv_instances.GetItemString(li_counter, "pattern_type") = "W") Then
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") - 1)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,21)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") + 6)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,21)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +6))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +6))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If		
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") + 13)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,21)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +13))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +13))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If		
		ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") + 20)
		If (ldt_new_begin_date >= ldt_header_start_date) AND (ldt_new_begin_date <= RelativeDate(ldt_header_start_date,21)) AND &
			(ldt_new_begin_date >= dnv_instances.GetItemDate(li_counter, "begin_date")) AND &
			(ldt_new_begin_date <= dnv_instances.GetItemDate(li_counter, "end_date")) Then
			li_new_row = dnv_instances.InsertRow(0)
			dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
			dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +20))
			dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") +20))
			dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
			dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
			dnv_instances.SetItem(li_new_row, "pattern_type", " ")
			dnv_instances.SetItem(li_new_row, "pattern_day", 0)
			ll_row_count ++				
		End If			

		dnv_instances.DeleteRow(li_counter)
		ll_row_count --
		li_counter --		
	End If
	ls_temp = dnv_instances.Object.DataWindow.Data
Next


//end pattern routine

If dw_header.GetItemDate(1, "begin_date") < Today() Then
	li_instances_rowcount =  tab_1.tabpage_inst.dw_instances.RowCount()
	For li_row = 1 to li_instances_rowcount
		If tab_1.tabpage_inst.dw_instances.GetItemString (li_row, "step_type") <> 'F' Then
			li_duration_days = Int(tab_1.tabpage_inst.dw_instances.GetItemNumber(li_row, "step_duration") / 3)
			If (li_duration_days >= 1) OR (tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "dump_product_ind") = "Y") Then
				ldt_duration_end_date = RelativeDate(Today(), -1)	
				ldt_duration_begin_date =  RelativeDate(Today(), 0 - li_duration_days)	
				For li_col = 1 to 30
					ldt_instance_date = Date(mid(tab_1.tabpage_inst.dw_instances.Describe('quantity_' + &
							String(li_col) + '_t.text'), 2, 10))
					If ((ldt_instance_date >= ldt_duration_begin_date) and (ldt_instance_date <= ldt_duration_end_date)) OR &
							((tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "dump_product_ind") = "Y") and (ldt_instance_date < Today())) Then
//						MessageBox("Unprotect Row Column", "Unprotect row = " + string(li_row) + " column= " + string(li_col) + " instance date= " + string(ldt_instance_date,"mm/dd/yyyy")	)
						 ls_detail_errors = tab_1.tabpage_inst.dw_instances.GetItemString(li_row, "detail_errors")
						 if li_col = 1 then
							ls_detail_errors_new = 'U' + mid(ls_detail_errors,2,29)
						else	
							ls_detail_errors_new = mid(ls_detail_errors,1,li_col - 1) + 'U' + mid(ls_detail_errors, li_col + 1, 30 - li_col)
						End If
						tab_1.tabpage_inst.dw_instances.SetItem(li_row, "detail_errors", ls_detail_errors_new)
					End If
				Next
			End If
		End if
	Next
End If

tab_1.tabpage_inst.dw_instances.Object.DataWindow.Header.Height = 209

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')


return True
end function

public function boolean wf_three_week_parm ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp

Int	li_counter, &
		li_days_after

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_Parm_TitleModify


SetPointer(HourGlass!)

ii_start_column_parm = 1
ii_column_number_parm = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_parm_TitleModify = ''

For li_counter = 0 to 20
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number_parm ++
	istr_date_range_parm[ii_column_number_parm].begin_date = ldt_temp 
	istr_date_range_parm[ii_column_number_parm].end_date   = ldt_temp 

	ls_Parm_TitleModify += "parm_" + String(ii_column_number_parm) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yyyy") + "~~r~~n" + DayName(ldt_temp) + "' " + &
						"parm_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"max_" + String(ii_column_number_parm) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_parm) + ".Visible = 1 " + &
						"max_" + String(ii_column_number_parm) + ".Visible = 1 " 
Next	

tab_1.tabpage_inst_parm.dw_parameters.Modify(ls_Parm_TitleModify)

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')

return True
end function

public function boolean wf_three_week_auto_conv ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

Date	ldt_start, &
		ldt_temp

Int	li_counter, &
		li_days_after

Long	ll_row_Count

String	ls_temp, &
			ls_ret, &
			ls_Auto_Conv_TitleModify


SetPointer(HourGlass!)

ii_start_column_auto_conv = 1
ii_column_number_auto_conv = 0

ldt_start = dw_header.GetItemDate(1, "begin_date")
If IsNull(ldt_start) Then
	ldt_start = Today()
End if

ls_Auto_Conv_TitleModify = ''

For li_counter = 0 to 13
	ldt_temp = RelativeDate(ldt_start, li_counter)
	ii_column_number_auto_conv ++
	istr_date_range_auto_conv[ii_column_number_auto_conv].begin_date = ldt_temp 
	istr_date_range_auto_conv[ii_column_number_auto_conv].end_date   = ldt_temp 

	ls_Auto_Conv_TitleModify += "parm_" + String(ii_column_number_auto_conv) + "_t.Text = '" + &
							String(ldt_temp, "mm/dd/yyyy") + "~~r~~n" + DayName(ldt_temp) + "' " + &
						"parm_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"max_" + String(ii_column_number_auto_conv) + "_t.Visible = 1 " + &
						"min_" + String(ii_column_number_auto_conv) + ".Visible = 1 " + &
						"max_" + String(ii_column_number_auto_conv) + ".Visible = 1 " 
Next	

tab_1.tabpage_auto_conv.dw_auto_conv.Modify(ls_Auto_Conv_TitleModify)

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')

return True
end function

public function boolean wf_long_term_day_of_week ();// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THAT IS FASTER

//u_base_dw_ext	ldw_date_sort
DataStore			ldw_date_sort

Date		ldt_begin, &
			ldt_end, &
			ldt_header_start_date, &
			ldt_new_begin_date, &
			ldt_first_monday, &
			ldt_first_sunday, &
			ldt_prev_monday

Int		li_counter, &
			li_sub_counter, &
			li_ret, &
			li_temp, &
			li_counter2, &
			li_row_count, &
			li_start_day_number, &
			li_sub, &
			li_new_row, &
			li_counter_2, &
			li_col, &
			li_day

Long		ll_row_count, &
			ll_sort_row_Count, &
			ll_found_row

String	ls_temp, &
			ls_temp2, &
			ls_titleModify, &
			ls_ret, &
			ls_find_string, &
			ls_pattern_type, &
			ls_week_day


SetPointer(HourGlass!)

ldt_header_start_date = dw_header.GetItemDate(1, "begin_date")

ls_temp = dnv_instances.Object.DataWindow.Data

li_start_day_number = DayNumber(dw_header.GetItemDate(1, "begin_date"))
Choose Case li_start_day_number
	Case 1  
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 1)
	Case 2
		ldt_first_monday = dw_header.GetItemDate(1, "begin_date")
	Case 3
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 6)
	Case 4
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 5)
	Case 5
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 4)
	Case 6
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 3)
	Case 7
		ldt_first_monday = RelativeDate(dw_header.GetItemDate(1, "begin_date"), 2)
End Choose
ldt_first_sunday = RelativeDate(ldt_first_monday, -1)
ldt_prev_monday = RelativeDate(ldt_first_monday, -7)
	
ls_temp = dnv_instances.Object.DataWindow.Data

ll_row_count = dnv_instances.RowCount()
For li_counter = 1 to ll_row_count
	If (dnv_instances.GetItemString(li_counter, "pattern_type") = "W") and (dnv_instances.GetItemDate(li_counter, "begin_date") < ldt_header_start_date) Then
		If (dnv_instances.GetItemDate(li_counter, "end_date") = ldt_first_sunday) Then
			If (RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") - 1) < ldt_header_start_date)  Then
				dnv_instances.DeleteRow(li_counter)
				ll_row_count --
				li_counter --
			Else
				ldt_new_begin_date = RelativeDate(ldt_prev_monday,  dnv_instances.GetItemNumber(li_counter, "pattern_day") - 1)
				dnv_instances.SetItem(li_counter, "pattern_type", " ")
				dnv_instances.SetItem(li_counter, "pattern_day", 0)
				dnv_instances.SetItem(li_counter, "begin_date", ldt_new_begin_date)
				dnv_instances.SetItem(li_counter, "end_date", ldt_new_begin_date)
			End If
		Else
			If RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1) >= ldt_header_start_date Then
				li_new_row = dnv_instances.InsertRow(0)
				dnv_instances.SetItem(li_new_row, "mfg_step_sequence", dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence"))
				dnv_instances.SetItem(li_new_row, "begin_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1))
				dnv_instances.SetItem(li_new_row, "end_date", RelativeDate(ldt_prev_monday,dnv_instances.GetItemNumber(li_counter, "pattern_day") -1))
				dnv_instances.SetItem(li_new_row, "dest_plant", dnv_instances.GetItemString(li_counter, "dest_plant"))
				dnv_instances.SetItem(li_new_row, "amount", dnv_instances.GetItemDecimal(li_counter, "amount"))
				dnv_instances.SetItem(li_new_row, "pattern_type", " ")
				dnv_instances.SetItem(li_new_row, "pattern_day", 0)
				ll_row_count ++
			End If
			If dnv_instances.GetItemDate(li_counter, "begin_date") < ldt_header_start_date  Then
				dnv_instances.SetItem(li_counter, "begin_date", ldt_first_monday)
			End If
		End If	
	End If	
Next

ls_temp = dnv_instances.Object.DataWindow.Data

//merge date ranges together for an instance step if the amounts are the same
ll_row_count = dnv_instances.RowCount()
For li_counter = 1 to ll_row_count
	If dnv_instances.GetItemString(li_counter, "pattern_type") <= ' ' Then
		For li_counter_2 = li_counter +1 to ll_row_count
			If dnv_instances.GetItemNumber(li_counter, "mfg_step_sequence") = dnv_instances.GetItemNumber(li_counter_2, "mfg_step_sequence") AND & 
					dnv_instances.GetItemString(li_counter, "dest_plant") = dnv_instances.GetItemString(li_counter_2, "dest_plant") AND &
					dnv_instances.GetItemNumber(li_counter, "amount") = dnv_instances.GetItemNumber(li_counter_2, "amount") AND &
					dnv_instances.GetItemString(li_counter, "pattern_type") = dnv_instances.GetItemString(li_counter_2, "pattern_type") AND &
					dnv_instances.GetItemDate(li_counter, "end_date") = RelativeDate(dnv_instances.GetItemDate(li_counter_2, "begin_date"),-1) Then 
				dnv_instances.SetItem(li_counter, "end_date", dnv_instances.GetItemDate(li_counter_2, "end_date")) 
				dnv_instances.DeleteRow(li_counter_2)
				li_counter_2 --
				ll_row_count --
			End If
		Next	
	End If
Next


ii_start_column = 1

dnv_instances.SetSort("begin_date A, pattern_day, end_date A")
dnv_instances.Sort()

ls_temp = dnv_instances.Object.DataWindow.Data

/******************************************************
**	Create dynamic datawindow to hold all unique dates
** Fill in all dates from dnv_instances
******************************************************/

//This.OpenUserObject(ldw_date_sort, 'u_base_dw_ext')
//ldw_date_sort.Visible = False
ldw_date_sort = create DataStore
ldw_date_sort.DataObject = 'd_pas_instances_date_sort'

ll_row_count = dnv_instances.RowCount()
ll_sort_row_count = 0

For li_counter = 1 to ll_row_count
	If  dnv_instances.GetItemString(li_counter, "pattern_type") = 'W' Then
		ls_pattern_type = 'W'
	Else
		ls_pattern_type = ' '
	End If

	// check end date first.  If End date is less than begin date, then discard row
	ls_temp = String(dnv_instances.GetItemDate(li_counter, "end_date"), "yyyy-mm-dd")

	If Date(ls_temp) < ldt_header_start_date Then Continue

	If ldw_date_sort.Find("Left(date_col, 10) = '" + ls_temp + &
								"' And mid(date_col, 11, 1) = 'E'", 1, ll_sort_row_count) = 0 Then
		ldw_date_sort.ImportString(ls_temp + 'E' + ls_pattern_type + '~r~n')
		ll_sort_row_count ++
	End if

	ls_temp = String(dnv_instances.GetItemDate(li_counter, "begin_date"), "yyyy-mm-dd")
	If Date(ls_temp) < ldt_header_start_Date Then 
		ls_temp = String(ldt_header_start_date, "yyyy-mm-dd")
	End if

	If ldw_date_sort.Find("Left(date_col, 10) = '" + ls_temp + &
								"' And mid(date_col, 11, 1) = 'B'", 1, ll_sort_row_count) = 0 Then
		ldw_date_sort.ImportString(ls_temp + 'B'+ ls_pattern_type + '~r~n')
		ll_sort_row_count ++
	End if

Next

li_ret = ldw_date_sort.Sort()
ls_temp = ''

For li_counter = 1 to ldw_date_sort.RowCount()
	ls_temp = ls_temp + ldw_date_sort.GetItemString(li_counter, "date_col") + "~r~n"
Next


/**********************************************
**  Get date range pairs and create columns
**********************************************/
ll_row_count = ldw_date_sort.RowCount()
ls_TitleModify = ''

for li_counter = 1 to ll_row_count
	ls_temp = ldw_date_sort.GetItemString(li_counter, "date_col")
	If mid(ls_temp, 11, 1) = 'B' Then
		ldt_begin = Date(Left(ls_temp, 10))
		If li_counter < ll_row_count Then
			ls_temp2 = ldw_date_sort.GetItemString(li_counter + 1, "date_col")
			If mid(ls_temp2, 11, 1) = 'B' Then
				ldt_end = RelativeDate(Date(Left( ls_temp2, 10)), -1)
			Else
				ldt_end = Date(Left(ls_temp2, 10))
			End if
		Else
			Continue
		End if
	Else
		ldt_end = Date(Left(ls_temp, 10))
		If li_counter > 1 Then
			ls_temp2 = ldw_date_sort.GetItemString(li_counter - 1, "date_col")
			If mid(ls_temp2, 11, 1) = 'E' Then
				ldt_begin = RelativeDate(Date(Left(ls_temp2, 10)), 1)
			Else
				// picked up this date range on last loop
				Continue
			End if
		Else
			Continue
		End if
	End if
	
	If mid(ls_temp, 12, 1) = 'W' Then
		If ii_column_number + 7 > 30 Then
			MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
			Exit
		End if		
		li_day = 1
		For li_col = ii_column_number +1 to ii_column_number + 7 
			istr_date_range[li_col].begin_date = ldt_begin 
			istr_date_range[li_col].end_date   = ldt_end 
			istr_date_range[li_col].week_day = li_day
			
			Choose Case li_day
				Case 1
					ls_week_day = 'Mon'
				Case 2
					ls_week_day = 'Tue'
				Case 3
					ls_week_day = 'Wed'
				Case 4
					ls_week_day = 'Thu'
				Case 5
					ls_week_day = 'Fri'
				Case 6
					ls_week_day = 'Sat'
				Case 7
					ls_week_day = 'Sun'
			End Choose		
			If li_day = 1 Then
				ls_TitleModify += "quantity_" + String(li_col) + "_t.Text = '" + &
					String(istr_date_range[li_col].begin_date, "mm/dd/yy") + "~~r~~n" + &
					ls_week_day + "' " + &
					"quantity_" + String(li_col) + "_t.Visible = 1 " + &
					"quantity_" + String(li_col) + "_t.BackGround.Color = 13434879 " + &
					"quantity_" + String(li_col) + ".Visible = 1 " 
			Else		
				If li_day = 7 Then
					ls_TitleModify += "quantity_" + String(li_col) + "_t.Text = '" + &
						String(istr_date_range[li_col].end_date, "mm/dd/yy") + "~~r~~n" + &
						ls_week_day + "' " + &
						"quantity_" + String(li_col) + "_t.Visible = 1 " + &
						"quantity_" + String(li_col) + "_t.BackGround.Color = 13434879 " + &
						"quantity_" + String(li_col) + ".Visible = 1 "
				Else
					ls_TitleModify += "quantity_" + String(li_col) + "_t.Text = '" + &
						"~~r~~n" + &
						ls_week_day + "' " + &
						"quantity_" + String(li_col) + "_t.Visible = 1 " + &
						"quantity_" + String(li_col) + "_t.BackGround.Color = 13434879 " + &
						"quantity_" + String(li_col) + ".Visible = 1 " 	
				End If	
			End If		
			li_day ++
			
		Next		
		ii_column_number = ii_column_number + 7
	Else	
		ii_column_number ++
		If ii_column_number > 30 Then
			ii_column_number --
			li_temp = UpperBound(istr_date_range)
			MessageBox("Date Range", "Maximum Number of Date Ranges exceeded.  " + &
					"Only the first 30 Date Ranges will be displayed.  " + &
					"Contact Applications Programming.")
			Exit
		End if		
		istr_date_range[ii_column_number].begin_date = ldt_begin 
		istr_date_range[ii_column_number].end_date   = ldt_end 
	
		ls_TitleModify += "quantity_" + String(ii_column_number) + "_t.Text = '" + &
								String(ldt_begin, "mm/dd/yy") + "~~r~~n" + &
								String(ldt_end, "mm/dd/yy") + "' " + &
								"quantity_" + String(ii_column_number) + "_t.Visible = 1 " + &
								"quantity_" + String(ii_column_number) + ".Visible = 1 " 
	End If
Next

ls_ret = tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)
If Len(ls_ret) > 0 Then
	MessageBox("Modify", ls_ret)
End if

ls_temp = dnv_instances.Object.DataWindow.Data

// this might miss date ranges for instances that are very long term
// don't think this is needed IBDKSCS 03/2019
//for li_counter = 1 to ii_column_number - 1
//	If DaysAfter(istr_date_range[li_counter].end_date, &
//			istr_date_range[li_counter + 1].begin_date) <> 1 Then
//		// these are not successive date ranges, check to see if anything needs it
//		ll_row_count = dnv_instances.RowCount()
//		for li_sub_counter = 1 to ll_row_count
//			If dnv_instances.GetItemDate(li_sub_counter, "begin_date") < &
//					istr_date_range[li_counter].end_date And &
//					dnv_instances.GetItemDate(li_sub_counter, "end_date") > &
//					istr_date_range[li_counter + 1].begin_date Then
//				If dnv_instances.GetItemNumber(li_sub_counter, "pattern_day") = 0 then					
//					wf_add_date_range(RelativeDate(istr_date_range[li_counter].end_date, 1), &
//												RelativeDate(istr_date_range[li_counter + 1].begin_date, -1))
//					Exit
//				Else
//					wf_add_date_range_pattern(RelativeDate(istr_date_range[li_counter].end_date, 1), &
//												RelativeDate(istr_date_range[li_counter + 1].begin_date, -1))
//					Exit	
//				End If
//			End If
//		Next
//	End if
//Next

ls_temp = dnv_instances.Object.DataWindow.Data

/////  new code IBDKSCS
//li_row_count = dnv_instances.RowCount()
//For li_counter = 1 to li_row_count
//	If (dnv_instances.GetItemString(li_counter, "pattern_type") = "W") Then
//		li_ret = wf_add_date_range_pattern(dnv_instances.GetItemDate(li_counter, "begin_date"), dnv_instances.GetItemDate(li_counter, "end_date"))
//		If li_ret = -1 Then
//			Exit
//		End if
//	End If
//Next

///// end new code IBDKSCS

Destroy ldw_date_sort
//This.PostEvent("ue_CloseUserObject")

tab_1.tabpage_inst.dw_instances.Object.DataWindow.Header.Height = 125

If tab_1.SelectedTab = 1 Then iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

return True
end function

public function integer wf_add_date_range_pattern (date adt_begin, date adt_end);Boolean	lb_new_column

Date		ldt_temp
Decimal	ld_value, &
			ld_value2, &
			ldc_null_value

Int	li_counter, &
		li_sub_counter, &
		li_new_column_number, &
		li_row_counter, &
		li_num_tabs, &
		li_week_number, &
		li_week_day, &
		li_sub_counter2

Long	ll_row_count, &
		ll_pos1, &
		ll_pos2, &
		ll_pos3, &
		ll_pos4, &
		ll_pos5, &
		ll_end_of_row, &
		lla_column_amounts[], &
		ll_temp

str_date_range	lstr_temp_range[]
str_quantity	lstr_quantity[], &
					lstr_zero_quantity[]
String	ls_data, &
			ls_TitleModify, &
			ls_columnAmounts, &
			ls_columnName, ls_temp, &
			ls_qty_field, &
			ls_mid1, &
			ls_mid2, &
			ls_xx, &
			ls_xx2, &
			ls_orig_col_name, &
			ls_new_col_name, &
			ls_week_day

u_string_functions		lu_string

DataStore 			lds_original_data
str_date_range		lstr_original_date_range[]

lds_original_data = Create DataStore
lds_original_data.DataObject = "d_pas_instances"

lstr_original_date_range = istr_date_range

If ii_column_number > UpperBound(istr_date_range) - 1 Then Return -1

If adt_Begin < idt_inquire_date Then
	MessageBox("Inquire Date", "New date range cannot be before the inquired Begin Date.  " + &
										"Please reinquire with an earlier Begin Date.")
	return -1
End if

SetPointer(HourGlass!)
This.SetRedraw(False)
ii_start_column = ii_column_number

ls_data = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data

lds_original_data.Reset()
lds_original_data.ImportString(ls_data)

If Right(ls_data, 2) <> '~r~n' Then
	ls_data += '~r~n'
End if
ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()

//  Set li_num_tabs to the number of tabs before the 1st quantity column
//  If column 9 is the 1st quantity column, set li_num_tabs = 8
li_num_tabs = 8

For li_counter = 1 to ii_column_number
	
	// insert a pattern in the middle of a bigger date range
	If adt_begin > istr_date_range[li_counter].begin_date And &
			adt_end < istr_date_range[li_counter].end_date Then

		// create a new pattern within existing pattern
		if istr_date_range[li_counter].week_day > 0 Then
			If ii_column_number + 14 > 30 Then
				MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
				wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
				This.SetRedraw(True)
				Return -1				
			Else	
				For li_sub_counter = ii_column_number to li_counter  step -1
					istr_date_range[li_sub_counter + 14] = istr_date_range[li_sub_counter]
					ls_orig_col_name = 'quantity_' + string(li_sub_counter)
					ls_new_col_name = 'quantity_' + string(li_sub_counter + 14)		
					For li_row_counter = 1 to ll_row_count
						tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
					Next		
				Next
				// move li_counter to 1st column of new date range
				li_counter = li_counter + 7
				ii_column_number = ii_column_number + 14
			End If
		Else
		// break up previous non-pattern date range	
			If ii_column_number + 8 > 30 Then
				MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
				wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
				This.SetRedraw(True)
				Return -1				
			Else		
				For li_sub_counter = ii_column_number to li_counter step -1
					istr_date_range[li_sub_counter + 8] = istr_date_range[li_sub_counter]
					ls_orig_col_name = 'quantity_' + string(li_sub_counter)
					ls_new_col_name = 'quantity_' + string(li_sub_counter +8)	
					For li_row_counter = 1 to ll_row_count
						tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
					Next		
				Next	
				li_counter = li_counter + 1
				ii_column_number = ii_column_number + 8
			End If
		End If	
		
		//set values for new pattern 
		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter to li_counter + 6
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			istr_date_range[li_sub_counter].new_column = True
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next
		Next
		
		//adjust begin dates after the pattern 
		if li_counter + 7 <= ii_column_number then
			If istr_date_range[li_counter + 7].week_day  = 0 Then
				istr_date_range[li_counter + 7].begin_date = RelativeDate(adt_end, 1)
			Else
				For li_sub_counter = li_counter + 7  to li_counter + 13
					istr_date_range[li_sub_counter].begin_date = RelativeDate(adt_end, 1)
				Next
			End if
		End If
		
		// adjust the end dates prior to the pattern
		If istr_date_range[li_counter - 1].week_day  = 0 Then
			istr_date_range[li_counter - 1].end_date = RelativeDate(adt_begin, -1)
		Else
			For li_sub_counter = li_counter - 1  to li_counter - 7 step -1
				istr_date_range[li_sub_counter].end_date = RelativeDate(adt_begin, -1)
			Next
		End if		
		
	End If	
	

	// break interval into a smaller interval, same ending date
	If (adt_begin >  istr_date_range[li_counter].begin_date) AND &
		(adt_end = istr_date_range[li_counter].end_date)  Then
		
		If ii_column_number + 7 > 30 Then
			MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
			wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
			This.SetRedraw(True)
			Return -1			
		End If
		
		IF istr_date_range[li_counter].week_day > 0 Then
			For li_sub_counter = ii_column_number to li_counter step -1
				istr_date_range[li_sub_counter + 7] = istr_date_range[li_sub_counter]
				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
				ls_new_col_name = 'quantity_' + string(li_sub_counter + 7)		
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next			
			Next					
		Else	
			For li_sub_counter = ii_column_number to li_counter +1 step -1
				istr_date_range[li_sub_counter + 7] = istr_date_range[li_sub_counter]
				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
				ls_new_col_name = 'quantity_' + string(li_sub_counter + 7)		
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			Next		
		End If
		
		if istr_date_range[li_counter].week_day > 0 Then
			li_counter = li_counter + 7
		End If		

		if istr_date_range[li_counter].week_day > 0 Then
			li_week_day = 1
			SetNull(ldc_null_value)		
			For li_sub_counter = li_counter  to li_counter + 6
				istr_date_range[li_sub_counter].begin_date = adt_begin
				istr_date_range[li_sub_counter].end_date = adt_end
				istr_date_range[li_sub_counter].week_day = li_week_day
				istr_date_range[li_sub_counter].new_column = True			
				li_week_day ++
				ls_new_col_name = 'quantity_' + string(li_sub_counter)
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
				Next
			Next
		Else
			li_week_day = 1
			SetNull(ldc_null_value)		
			For li_sub_counter = li_counter + 1 to li_counter + 7
				if li_sub_counter <= 30 Then
					istr_date_range[li_sub_counter].begin_date = adt_begin
					istr_date_range[li_sub_counter].end_date = adt_end
					istr_date_range[li_sub_counter].week_day = li_week_day
					istr_date_range[li_sub_counter].new_column = True			
					li_week_day ++
					ls_new_col_name = 'quantity_' + string(li_sub_counter)
					For li_row_counter = 1 to ll_row_count
						tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
					Next
				Else
					MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
					This.SetRedraw(True)
					Return -1					
				End If
			Next			
		End If	

//		If (li_counter -1) > 1 Then
//			If istr_date_range[li_counter - 1].week_day  = 0 Then
//				istr_date_range[li_counter - 1].end_date = RelativeDate(adt_begin, -1)
//			Else
//				For li_sub_counter = li_counter - 1 to li_counter - 7 step -1
//					istr_date_range[li_sub_counter].end_date = RelativeDate(adt_begin, -1)
//				Next
//			End if			
//		End If
		
//		Fixed above loop 03/22
		If (li_counter ) > 1 Then
			If istr_date_range[li_counter].week_day  = 0 Then
				istr_date_range[li_counter].end_date = RelativeDate(adt_begin, -1)
			Else
				For li_sub_counter = li_counter to li_counter - 6 step -1
					istr_date_range[li_sub_counter].end_date = RelativeDate(adt_begin, -1)
				Next
			End if			
		End If

		ii_column_number = ii_column_number + 7
		
	End if		
	
	// break interval into a smaller interval, same begin date
	If (adt_begin = istr_date_range[li_counter].begin_date) AND &
			(adt_end <  istr_date_range[li_counter].end_date) Then
			
		If ii_column_number + 7 > 30 Then
			MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
			wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
			This.SetRedraw(True)
			Return -1			
		End If			
	
		For li_sub_counter = ii_column_number to li_counter step -1
			istr_date_range[li_sub_counter + 7] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter + 7)		
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
			Next	
		Next					
	
		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter to li_counter + 6
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next			
		Next
		
		li_new_column_number = li_counter
		ii_column_number = ii_column_number + 7
		
		If istr_date_range[li_counter + 7].week_day  = 0 Then
			istr_date_range[li_counter + 7].begin_date = RelativeDate(adt_end, 1)
		Else
			For li_sub_counter = li_counter + 7  to li_counter + 13
				istr_date_range[li_sub_counter].begin_date = RelativeDate(adt_end, 1)
			Next
		End if
		
	End If	
	
	// expand an existing date range on the beginning side
	If (adt_begin <  istr_date_range[li_counter].begin_date) AND &
			(adt_end = istr_date_range[li_counter].end_date)  Then

		// expand the existing date range if not a pattern
		If istr_date_range[li_counter].week_day = 0 Then
			
			If ii_column_number + 6 > 30 Then
				MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
				wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
				This.SetRedraw(True)
				Return -1			
			End If		
			
			For li_sub_counter = ii_column_number to li_counter + 1 step -1
				istr_date_range[li_sub_counter + 6] = istr_date_range[li_sub_counter]
				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
				ls_new_col_name = 'quantity_' + string(li_sub_counter + 6)		
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			Next	
			ii_column_number = ii_column_number + 6
		End If

		//eliminate previous columns with begin date greater than the new begin date
		If li_counter > 1 Then
			For li_sub_counter = li_counter - 1 to 1 step -1
				if (istr_date_range[li_sub_counter].begin_date >= adt_begin) Then
					For li_sub_counter2 = li_sub_counter to ii_column_number
						istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
						ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
						ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
						For li_row_counter = 1 to ll_row_count
							tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
						Next
					Next
					ii_column_number --
					li_counter --
				End If	
			Next
		End If
		
		// set values for the current pattern
		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter  to li_counter + 6
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			istr_date_range[li_sub_counter].new_column = True			
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next			
		Next		
		
		//if necessary, adjust the end date of the previous column
		if (li_counter > 1) Then
			If (istr_date_range[li_counter - 1].end_date >= adt_begin) Then
				If istr_date_range[li_counter - 1].week_day  = 0 Then
					istr_date_range[li_counter - 1].end_date = RelativeDate(adt_begin, -1)
				Else
					For li_sub_counter = li_counter -1 to li_counter - 7 step -1
						istr_date_range[li_sub_counter].end_date = RelativeDate(adt_begin, -1)
					Next
				End if		
			End If
		End If
		
	End if		
	
	// expand an existing date range on the ending side	
	If (adt_begin = istr_date_range[li_counter].begin_date) AND &
			(adt_end >  istr_date_range[li_counter].end_date) Then
	
		istr_date_range[li_counter].new_column = True	
		
		// if date range was non-pattern, add 6 more days	
		If istr_date_range[li_counter].week_day = 0 Then
			
			If ii_column_number + 6 > 30 Then
				MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
				wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
				This.SetRedraw(True)
				Return -1			
			End If			
			
			For li_sub_counter = ii_column_number to li_counter + 1 step -1
				istr_date_range[li_sub_counter + 6] = istr_date_range[li_sub_counter]
				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
				ls_new_col_name = 'quantity_' + string(li_sub_counter + 6)		
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			Next		
			ii_column_number = ii_column_number + 6
		End If

		li_new_column_number = li_counter
		
		// remove any columns not needed because of the new ending date
		For li_sub_counter = li_counter + 7 to ii_column_number 
			if (adt_end >= istr_date_range[li_sub_counter].end_date) Then
				For li_sub_counter2 = li_sub_counter to ii_column_number
					istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
					ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
					ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
					For li_row_counter = 1 to ll_row_count
						tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
					Next
				Next
				ii_column_number --
				li_sub_counter --
			End If	
		Next
		
		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter to li_counter + 6
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next			
		Next		
		
		If li_counter + 7 > ii_column_number Then
			// no nothing, no more columns exist
		Else
			If (istr_date_range[li_counter+7].week_day  = 0)  Then
				istr_date_range[li_counter + 7].begin_date = RelativeDate(adt_end, 1)
			Else
				For li_sub_counter = li_counter + 7 to li_counter + 13
					istr_date_range[li_sub_counter].begin_date = RelativeDate(adt_end, 1)
				Next
			End if
		End If
		
	End if		
	
	// expand an existing date range both side of the date range	
	If (adt_begin < istr_date_range[li_counter].begin_date) AND &
			(adt_end >  istr_date_range[li_counter].end_date) Then
	
		istr_date_range[li_counter].new_column = True	
		
		// if date range was non-pattern, add 6 more days	
		If istr_date_range[li_counter].week_day = 0 Then
			
			If ii_column_number + 6 > 30 Then
				MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
				wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
				This.SetRedraw(True)
				Return -1			
			End If			
			
			For li_sub_counter = ii_column_number to li_counter + 1 step -1
				istr_date_range[li_sub_counter + 6] = istr_date_range[li_sub_counter]
				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
				ls_new_col_name = 'quantity_' + string(li_sub_counter + 6)		
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			Next		
			ii_column_number = ii_column_number + 6
		End If

		li_new_column_number = li_counter
		
		//eliminate previous columns with begin date greater than the new begin date
		If li_counter > 1 Then
			For li_sub_counter = li_counter - 1 to 1 step -1
				if (istr_date_range[li_sub_counter].begin_date >= adt_begin) Then
					For li_sub_counter2 = li_sub_counter to ii_column_number
						istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
						ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
						ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
						For li_row_counter = 1 to ll_row_count
							tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
						Next
					Next
					ii_column_number --
					li_counter --
				End If	
			Next
		End If		
		
		// remove any columns not needed because of the new ending date
		For li_sub_counter = li_counter + 7 to ii_column_number 
			if (adt_end >= istr_date_range[li_sub_counter].end_date) Then
				For li_sub_counter2 = li_sub_counter to ii_column_number
					istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
					ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
					ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
					For li_row_counter = 1 to ll_row_count
						tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
					Next
				Next
				ii_column_number --
				li_sub_counter --
			End If	
		Next
		
		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter to li_counter + 6
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next			
		Next		
		
		If li_counter + 7 > ii_column_number Then
			// no nothing, no more columns exist
		Else
			If (istr_date_range[li_counter+7].week_day  = 0)  Then
				istr_date_range[li_counter + 7].begin_date = RelativeDate(adt_end, 1)
			Else
				For li_sub_counter = li_counter + 7 to li_counter + 13
					istr_date_range[li_sub_counter].begin_date = RelativeDate(adt_end, 1)
				Next
			End if
		End If
		
		//if necessary, adjust the end date of the previous column
		if (li_counter > 1) Then
			If (istr_date_range[li_counter - 1].end_date >= adt_begin) Then
				If istr_date_range[li_counter - 1].week_day  = 0 Then
					istr_date_range[li_counter - 1].end_date = RelativeDate(adt_begin, -1)
				Else
					For li_sub_counter = li_counter -1 to li_counter - 7 step -1
						istr_date_range[li_sub_counter].end_date = RelativeDate(adt_begin, -1)
					Next
				End if		
			End If
		End If		
		
	End if		
	
	// the interval has already been set, but not as a pattern, expand with the pattern info
	If (adt_begin = istr_date_range[li_counter].begin_date) AND &
			(adt_end =  istr_date_range[li_counter].end_date) AND &
			(istr_date_range[li_counter].week_day = 0) Then

		If ii_column_number + 6 > 30 Then
			MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
			wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
			This.SetRedraw(True)
			Return -1			
		End If
		
		For li_sub_counter = ii_column_number to li_counter step -1
			istr_date_range[li_sub_counter + 6] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter + 6)		
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
			Next	
		Next
		
		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter to li_counter + 6
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			istr_date_range[li_sub_counter].new_column = True			
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next
		Next

		ii_column_number = ii_column_number + 6
		li_new_column_number = li_counter

	End if		
	

		
	If (adt_begin > istr_date_range[li_counter].begin_date) AND &
			(adt_end >  istr_date_range[li_counter].end_date) AND &
			(istr_date_range[li_counter+1].begin_date = date('1900-01-01')) Then
		// 	insert at the end,			

		If li_counter + 7 > 30 Then
			MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
			wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
			This.SetRedraw(True)
			Return -1			
		End If			

		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter + 1 to li_counter + 7
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next		
		Next

//		If istr_date_range[li_counter].week_day  = 0 Then
//			istr_date_range[li_counter].end_date = RelativeDate(adt_begin, -1)
//		Else
//			For li_sub_counter = li_counter to li_counter - 6 step -1
//				istr_date_range[li_sub_counter].end_date = RelativeDate(adt_begin, -1)
//			Next
//		End if		
		
		li_new_column_number = li_counter + 1
		ii_column_number = ii_column_number + 7 		
		
	End if		
	
	If (adt_begin < istr_date_range[li_counter].begin_date) AND &
			(adt_end <  istr_date_range[li_counter].end_date) AND &
			(li_counter = 1) Then
		// 	insert at the beginning		

		If ii_column_number + 7 > 30 Then
			MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
			wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
			This.SetRedraw(True)
			Return -1			
		End If

		For li_sub_counter = ii_column_number to li_counter step -1
			istr_date_range[li_sub_counter + 7] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter + 7)		
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
			Next		
		Next
		
		ii_column_number = ii_column_number + 7

		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter  to li_counter + 6
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next			
		Next


		li_new_column_number = li_counter + 1
		
	End if			
	
	If (adt_begin > istr_date_range[li_counter].begin_date) AND &
			(adt_end > istr_date_range[li_counter].end_date) AND &
			(adt_end <  istr_date_range[li_counter+1].begin_date) Then
		// 	insert between 2 ranges

		If ii_column_number + 7 > 30 Then
			MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
			wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
			This.SetRedraw(True)
			Return -1			
		End If		
		
		For li_sub_counter = ii_column_number to li_counter + 1 step -1
			istr_date_range[li_sub_counter + 7] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter + 7)		
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
			Next		
		Next
		ii_column_number = ii_column_number + 7

		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter +1  to li_counter + 7
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next			
		Next

		li_new_column_number = li_counter + 1
		
	End if				
	
	If (adt_begin > istr_date_range[li_counter].begin_date) AND &
			(adt_begin < istr_date_range[li_counter].end_date) Then

		// 	insert between 2 overlapping ranges
		
		If ii_column_number + 7 > 30 Then
			MessageBox("Long Term by Day of Week Columns Exceeded", "Long Term by Day of Week has exceeded 30 columns - cannot display all columns")
			wf_instances_columns_exceeded(li_counter, adt_begin, adt_end)
			This.SetRedraw(True)
			Return -1			
		End If		
		
		If istr_date_range[li_counter].week_day > 0 Then
			For li_sub_counter = ii_column_number to li_counter step -1
				istr_date_range[li_sub_counter + 7] = istr_date_range[li_sub_counter]
				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
				ls_new_col_name = 'quantity_' + string(li_sub_counter + 7)		
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			Next
			ii_column_number = ii_column_number + 7
			li_counter = li_counter + 7
		Else
			For li_sub_counter = ii_column_number to li_counter + 1 step -1
				istr_date_range[li_sub_counter + 7] = istr_date_range[li_sub_counter]
				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
				ls_new_col_name = 'quantity_' + string(li_sub_counter + 7)	
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			Next
			ii_column_number = ii_column_number + 7		
			li_counter ++
		End If

		// remove any columns not needed because of the new ending date
		For li_sub_counter = li_counter + 7 to ii_column_number 
			if (adt_end >= istr_date_range[li_sub_counter].end_date) Then
				For li_sub_counter2 = li_sub_counter to ii_column_number
					istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
					ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
					ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
					For li_row_counter = 1 to ll_row_count
						tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
					Next
				Next
				ii_column_number --
				li_sub_counter --
			End If	
		Next

		li_week_day = 1
		SetNull(ldc_null_value)		
		For li_sub_counter = li_counter  to li_counter + 6
			istr_date_range[li_sub_counter].begin_date = adt_begin
			istr_date_range[li_sub_counter].end_date = adt_end
			istr_date_range[li_sub_counter].week_day = li_week_day
			li_week_day ++
			ls_new_col_name = 'quantity_' + string(li_sub_counter)
			For li_row_counter = 1 to ll_row_count
				tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
			Next			
		Next
		
		If li_counter + 7 > ii_column_number Then
			// no nothing, no more columns exist
		Else
			If (istr_date_range[li_counter+7].week_day  = 0)  Then
				istr_date_range[li_counter + 7].begin_date = RelativeDate(adt_end, 1)
			Else
				For li_sub_counter = li_counter + 7 to li_counter + 13
					istr_date_range[li_sub_counter].begin_date = RelativeDate(adt_end, 1)
				Next
			End if
		End If
		
		//if necessary, adjust the end date of the previous column
		if li_counter - 1 > 0 Then
			If (istr_date_range[li_counter - 1].end_date >= adt_begin) Then
				If istr_date_range[li_counter - 1].week_day  = 0 Then
					istr_date_range[li_counter - 1].end_date = RelativeDate(adt_begin, -1)
				Else
					For li_sub_counter = li_counter - 1 to li_counter - 7 step -1
						istr_date_range[li_sub_counter].end_date = RelativeDate(adt_begin, -1)
					Next
				End if		
			End If
		End if

		li_new_column_number = li_counter		
	End if			

Next

if ii_column_number = 0 Then
// no columns displayed, so add at beginning

	li_counter = 1
	li_week_day = 1
	SetNull(ldc_null_value)		
	For li_sub_counter = li_counter to li_counter + 6
		istr_date_range[li_sub_counter].begin_date = adt_begin
		istr_date_range[li_sub_counter].end_date = adt_end
		istr_date_range[li_sub_counter].week_day = li_week_day
		li_week_day ++
		ls_new_col_name = 'quantity_' + string(li_sub_counter)
		For li_row_counter = 1 to ll_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next			
	Next	
	li_new_column_number = li_counter 
	ii_column_number = ii_column_number + 7 
	
End if

ls_TitleModify = ''
For li_Counter = 1 to ii_column_number
	If li_Counter <= 30 Then
		If istr_date_range[li_Counter].week_day = 0 Then
			ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
					String(istr_date_range[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
					String(istr_date_range[li_Counter].end_date, "mm/dd/yy")  +  "' " + &
					"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
					"quantity_" + String(li_Counter) + "_t.BackGround.Color = 78682240 " + &
					"quantity_" + String(li_counter) + ".Visible = 1 "
		Else	
			li_week_number = istr_date_range[li_Counter].week_day
			Choose Case li_week_number
				Case 1
					ls_week_day = 'Mon'
				Case 2
					ls_week_day = 'Tue'
				Case 3
					ls_week_day = 'Wed'
				Case 4
					ls_week_day = 'Thu'
				Case 5
					ls_week_day = 'Fri'
				Case 6
					ls_week_day = 'Sat'
				Case 7
					ls_week_day = 'Sun'
			End Choose		
			If li_week_number = 1 Then
				ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
					String(istr_date_range[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
					ls_week_day + "' " + &
					"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
					"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
					"quantity_" + String(li_counter) + ".Visible = 1 " 
			Else		
				If li_week_number = 7 Then
					ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
						String(istr_date_range[li_Counter].end_date, "mm/dd/yy") + "~~r~~n" + &
						ls_week_day + "' " + &
						"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
						"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
						"quantity_" + String(li_counter) + ".Visible = 1 "
				Else
					ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
						"~~r~~n" + &
						ls_week_day + "' " + &
						"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
						"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
						"quantity_" + String(li_counter) + ".Visible = 1 " 	
				End If	
			End If
		End If
	End If
Next

// wf_reset_Title_Text will clear out ii_column number and istr_date_range
li_counter = ii_column_number
lstr_temp_range = istr_date_range
wf_Reset_Title_Text()
ii_column_number = li_counter
istr_date_range = lstr_temp_range
//
tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)
//
//tab_1.tabpage_inst.dw_instances.Reset()
//If ll_row_count > 0 Then
//	// If there are no rows, ls_data just turned to garbage, so don't re import it
//	tab_1.tabpage_inst.dw_instances.ImportString(ls_data)
//End if
//
ls_data = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data
tab_1.tabpage_inst.dw_instances.SetFocus()

//For li_counter = 1 to 30
//	If istr_date_range[li_counter].end_date >=adt_end Then
//		For li_sub_counter = 1 to 30
//			If istr_date_range[li_counter].end_date = lstr_original_date_range[li_sub_counter].end_date AND &
//					istr_date_range[li_counter].week_day = 0 Then
//				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
//				ls_new_col_name = 'quantity_' + string(li_counter)
//				For li_row_counter = 1 to ll_row_count
//					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
//				Next
//			End If
//		Next
//	End if
//Next

This.tab_1.tabpage_inst.dw_instances.Event ue_Set_HSpScroll()

This.SetRedraw(True)

tab_1.tabpage_inst.dw_instances.SetColumn(li_new_column_number + li_num_tabs - 1)

return li_new_column_number

Return 0

end function

public function integer wf_add_date_range_overflow (date adt_begin, date adt_end);Boolean	lb_new_column

Date		ldt_temp
Decimal	ld_value, &
			ld_value2, &
			ldc_null_value

Int	li_counter, &
		li_sub_counter, &
		li_new_column_number, &
		li_row_counter, &
		li_num_tabs, &
		li_week_number, &
		li_sub_counter2

Long	ll_row_count, &
		ll_pos1, &
		ll_pos2, &
		ll_pos3, &
		ll_pos4, &
		ll_pos5, &
		ll_end_of_row, &
		lla_column_amounts[], &
		ll_temp

str_date_range	lstr_temp_range[]
str_quantity	lstr_quantity[], &
					lstr_zero_quantity[]
String	ls_data, &
			ls_TitleModify, &
			ls_columnAmounts, &
			ls_columnName, ls_temp, &
			ls_qty_field, &
			ls_mid1, &
			ls_mid2, &
			ls_xx, &
			ls_xx2, &
			ls_orig_col_name, &
			ls_new_col_name, &
			ls_week_day

u_string_functions		lu_string

DataStore 			lds_original_data
str_date_range		lstr_original_date_range[]

lds_original_data = Create DataStore
lds_original_data.DataObject = "d_pas_instances"

lstr_original_date_range = istr_date_range

If ii_column_number > UpperBound(istr_date_range) - 1 Then Return -1

If adt_Begin < idt_inquire_date Then
	MessageBox("Inquire Date", "New date range cannot be before the inquired Begin Date.  " + &
										"Please reinquire with an earlier Begin Date.")
	return -1
End if

SetPointer(HourGlass!)
This.SetRedraw(False)
ii_start_column = ii_column_number

ls_data = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data

lds_original_data.Reset()
lds_original_data.ImportString(ls_data)

If Right(ls_data, 2) <> '~r~n' Then
	ls_data += '~r~n'
End if
ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()

//  Set li_num_tabs to the number of tabs before the 1st quantity column
//  If column 9 is the 1st quantity column, set li_num_tabs = 8
li_num_tabs = 8

For li_counter = 1 to ii_column_number
	
	// insert a pattern in the middle of a bigger date range
	If adt_begin > istr_date_range[li_counter].begin_date And &
			adt_end < istr_date_range[li_counter].end_date Then

		//break up previous non-pattern date range	
		//For li_sub_counter = ii_column_number to li_counter step -1
		For li_sub_counter = 30 to li_counter step -1			
			istr_date_range[li_sub_counter + 2] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter +2)		
			if (li_sub_counter +2 <= 30) Then
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			End If
		Next	
		li_counter = li_counter + 1
		ii_column_number = ii_column_number + 2
		
		//set values for new pattern 
		SetNull(ldc_null_value)		
		istr_date_range[li_counter].begin_date = adt_begin
		istr_date_range[li_counter].end_date = adt_end
		istr_date_range[li_counter].week_day = 0
		istr_date_range[li_counter].new_column = True
		ls_new_col_name = 'quantity_' + string(li_counter)
		For li_row_counter = 1 to ll_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next
		
		//adjust begin dates after the pattern 
		if li_counter + 1 <= ii_column_number then
			istr_date_range[li_counter + 1].begin_date = RelativeDate(adt_end, 1)
		End If
		
		// adjust the end dates prior to the pattern
		if li_counter > 1 Then
			istr_date_range[li_counter - 1].end_date = RelativeDate(adt_begin, -1)
		End if		
		
	End If	
	

	// break interval into a smaller interval, same ending date
	If (adt_begin >  istr_date_range[li_counter].begin_date) AND &
		(adt_end = istr_date_range[li_counter].end_date)  Then

						
		For li_sub_counter = 30 to li_counter +1 step -1
			istr_date_range[li_sub_counter + 1] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter + 1)		
			if (li_sub_counter + 1) <= 30 Then
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			End If
		Next		
		
		SetNull(ldc_null_value)		
		istr_date_range[li_counter + 1].begin_date = adt_begin
		istr_date_range[li_counter + 1].end_date = adt_end
		istr_date_range[li_counter + 1].week_day = 0
		istr_date_range[li_counter + 1].new_column = True			
		ls_new_col_name = 'quantity_' + string(li_counter + 1)
		For li_row_counter = 1 to ll_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next
		
		istr_date_range[li_counter].end_date = RelativeDate(adt_begin, -1)
		ii_column_number = ii_column_number + 1
		
	End if		
	
	// break interval into a smaller interval, same begin date
	If (adt_begin = istr_date_range[li_counter].begin_date) AND &
			(adt_end <  istr_date_range[li_counter].end_date) Then
	
		For li_sub_counter = 30 to li_counter step -1
			istr_date_range[li_sub_counter + 1] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter + 1)		
			If (li_sub_counter +1 <= 30) Then
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next			
			End If
		Next					
	
		SetNull(ldc_null_value)		
		istr_date_range[li_counter].begin_date = adt_begin
		istr_date_range[li_counter].end_date = adt_end
		istr_date_range[li_counter].week_day = 0
		ls_new_col_name = 'quantity_' + string(li_counter)
		For li_row_counter = 1 to ll_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next			
		
		li_new_column_number = li_counter
		ii_column_number = ii_column_number + 1
		
		if li_counter + 1 <= ii_column_number then
			istr_date_range[li_counter + 1].begin_date = RelativeDate(adt_end, 1)
		End if
		
	End If	
	
	// expand an existing date range on the beginning side
	If (adt_begin <  istr_date_range[li_counter].begin_date) AND &
			(adt_end = istr_date_range[li_counter].end_date)  Then

		//eliminate previous columns with begin date greater than the new begin date
		If li_counter > 1 Then
			For li_sub_counter = li_counter - 1 to 1 step -1
				if (istr_date_range[li_sub_counter].begin_date >= adt_begin) Then
					For li_sub_counter2 = li_sub_counter to ii_column_number
						istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
						ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
						ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
						if li_sub_counter2 + 1 <= 30 Then
							For li_row_counter = 1 to ll_row_count
								tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
							Next
						End If
					Next
					ii_column_number --
					li_counter --
				End If	
			Next
		End If
		
		// set values for the current pattern
		SetNull(ldc_null_value)		
		istr_date_range[li_counter].begin_date = adt_begin
		istr_date_range[li_counter].end_date = adt_end
		istr_date_range[li_counter].week_day = 0
		istr_date_range[li_counter].new_column = True			
		ls_new_col_name = 'quantity_' + string(li_counter)
		For li_row_counter = 1 to ll_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next			

		//if necessary, adjust the end date of the previous column
		if (li_counter > 1) Then
			If (istr_date_range[li_counter - 1].end_date >= adt_begin) Then
				istr_date_range[li_counter - 1].end_date = RelativeDate(adt_begin, -1)
			End If
		End If
		
	End if		
	
	// expand an existing date range on the ending side	
	If (adt_begin = istr_date_range[li_counter].begin_date) AND &
			(adt_end >  istr_date_range[li_counter].end_date) Then
	
		istr_date_range[li_counter].new_column = True	
		
		li_new_column_number = li_counter
		
		// remove any columns not needed because of the new ending date
		For li_sub_counter = li_counter + 1 to ii_column_number 
			if (adt_end >= istr_date_range[li_sub_counter].end_date) Then
				For li_sub_counter2 = li_sub_counter to ii_column_number
					istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
					ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
					ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
					If li_sub_counter2 + 1 <= 30 Then
						For li_row_counter = 1 to ll_row_count
							tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
						Next
					End If
				Next
				ii_column_number --
				li_sub_counter --
			End If	
		Next
		
		SetNull(ldc_null_value)		
		istr_date_range[li_counter].begin_date = adt_begin
		istr_date_range[li_counter].end_date = adt_end
		istr_date_range[li_counter].week_day = 0
		ls_new_col_name = 'quantity_' + string(li_counter)
		For li_row_counter = 1 to ll_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next			
		
		If li_counter + 1 > ii_column_number Then
			// no nothing, no more columns exist
		Else
			istr_date_range[li_counter + 1].begin_date = RelativeDate(adt_end, 1)
		End If
		
	End if		
	
	// expand an existing date range both side of the date range	
	If (adt_begin < istr_date_range[li_counter].begin_date) AND &
			(adt_end >  istr_date_range[li_counter].end_date) Then
	
		istr_date_range[li_counter].new_column = True	
		li_new_column_number = li_counter
		
		//eliminate previous columns with begin date greater than the new begin date
		If li_counter > 1 Then
			For li_sub_counter = li_counter - 1 to 1 step -1
				if (istr_date_range[li_sub_counter].begin_date >= adt_begin) Then
					For li_sub_counter2 = li_sub_counter to ii_column_number
						istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
						ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
						ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
						If li_sub_counter2 + 1 <= 30 Then
							For li_row_counter = 1 to ll_row_count
								tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
							Next
						End If
					Next
					ii_column_number --
					li_counter --
				End If	
			Next
		End If		
		
		// remove any columns not needed because of the new ending date
		For li_sub_counter = li_counter + 1 to ii_column_number 
			if (adt_end >= istr_date_range[li_sub_counter].end_date) Then
				For li_sub_counter2 = li_sub_counter to ii_column_number
					istr_date_range[li_sub_counter2] = istr_date_range[li_sub_counter2 + 1]
					ls_orig_col_name = 'quantity_' + string(li_sub_counter2 + 1)
					ls_new_col_name = 'quantity_' + string(li_sub_counter2) 
					If li_sub_counter2 + 1 <= 30 Then
						For li_row_counter = 1 to ll_row_count
							tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,tab_1.tabpage_inst.dw_instances.GetItemDecimal(li_row_counter, ls_orig_col_name))
						Next
					End If
				Next
				ii_column_number --
				li_sub_counter --
			End If	
		Next
		
		SetNull(ldc_null_value)		
		istr_date_range[li_counter].begin_date = adt_begin
		istr_date_range[li_counter].end_date = adt_end
		istr_date_range[li_counter].week_day = 0
		ls_new_col_name = 'quantity_' + string(li_counter)
		For li_row_counter = 1 to ll_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next			
	
		If li_counter + 1 > ii_column_number Then
			// no nothing, no more columns exist
		Else
			istr_date_range[li_counter + 1].begin_date = RelativeDate(adt_end, 1)
		End If
		
		//if necessary, adjust the end date of the previous column
		if (li_counter > 1) Then
			If (istr_date_range[li_counter - 1].end_date >= adt_begin) Then
				istr_date_range[li_counter - 1].end_date = RelativeDate(adt_begin, -1)
			End if		
		End If		
		
	End if		
	
	If (adt_begin > istr_date_range[li_counter].begin_date) AND &
		(adt_end >  istr_date_range[li_counter].end_date) AND &
		(li_counter = 30) Then
		// 	insert at the end not permitted
		
		MessageBox("Date Range", "Unable to insert at the end of date ranges.  Inquire with a different start date." ) 
		This.SetRedraw(True)
		return 0
	
	End if		
	
	If (adt_begin < istr_date_range[li_counter].begin_date) AND &
			(adt_end <  istr_date_range[li_counter].end_date) AND &
			(li_counter = 1) Then
		// 	insert at the beginning		

		For li_sub_counter = ii_column_number to li_counter step -1
			istr_date_range[li_sub_counter + 1] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter + 1)		
			if li_sub_counter + 1 <= 30 Then
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			End If
		Next
		ii_column_number = ii_column_number + 1

		SetNull(ldc_null_value)		
		istr_date_range[li_counter].begin_date = adt_begin
		istr_date_range[li_counter].end_date = adt_end
		istr_date_range[li_counter].week_day = 0
		ls_new_col_name = 'quantity_' + string(li_counter)
		For li_row_counter = 1 to ll_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next			

		li_new_column_number = li_counter + 1
		
	End if			
	
	If (adt_begin > istr_date_range[li_counter].begin_date) AND &
			(adt_end > istr_date_range[li_counter].end_date) AND &
			(adt_end <  istr_date_range[li_counter+1].begin_date) Then
		// 	insert between 2 ranges
		
		For li_sub_counter = ii_column_number to li_counter + 1 step -1
			istr_date_range[li_sub_counter + 1] = istr_date_range[li_sub_counter]
			ls_orig_col_name = 'quantity_' + string(li_sub_counter)
			ls_new_col_name = 'quantity_' + string(li_sub_counter + 1)		
			If li_sub_counter + 1 <= 30 Then
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter,ls_new_col_name,lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
				Next		
			End If
		Next
		ii_column_number = ii_column_number + 1

		SetNull(ldc_null_value)		

			istr_date_range[ li_counter +1].begin_date = adt_begin
			istr_date_range[ li_counter +1].end_date = adt_end
			istr_date_range[ li_counter +1].week_day = 0
			ls_new_col_name = 'quantity_' + string( li_counter +1)
			If li_counter + 1 < 30 Then
				For li_row_counter = 1 to ll_row_count
					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
				Next	
			End If

			li_new_column_number = li_counter + 1
		
	End if				

Next

ls_TitleModify = ''
For li_Counter = 1 to ii_column_number
	If istr_date_range[li_Counter].week_day = 0 Then
		ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
				String(istr_date_range[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
				String(istr_date_range[li_Counter].end_date, "mm/dd/yy")  +  "' " + &
				"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
				"quantity_" + String(li_Counter) + "_t.BackGround.Color = 78682240 " + &
				"quantity_" + String(li_counter) + ".Visible = 1 "
	Else	
		li_week_number = istr_date_range[li_Counter].week_day
		Choose Case li_week_number
			Case 1
				ls_week_day = 'Mon'
			Case 2
				ls_week_day = 'Tue'
			Case 3
				ls_week_day = 'Wed'
			Case 4
				ls_week_day = 'Thu'
			Case 5
				ls_week_day = 'Fri'
			Case 6
				ls_week_day = 'Sat'
			Case 7
				ls_week_day = 'Sun'
		End Choose		
		If li_week_number = 1 Then
			ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
				String(istr_date_range[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
				ls_week_day + "' " + &
				"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
				"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
				"quantity_" + String(li_counter) + ".Visible = 1 " 
		Else		
			If li_week_number = 7 Then
				ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
					String(istr_date_range[li_Counter].end_date, "mm/dd/yy") + "~~r~~n" + &
					ls_week_day + "' " + &
					"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
					"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
					"quantity_" + String(li_counter) + ".Visible = 1 "
			Else
				ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
					"~~r~~n" + &
					ls_week_day + "' " + &
					"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
					"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
					"quantity_" + String(li_counter) + ".Visible = 1 " 	
			End If	
		End If
	End If
Next

// wf_reset_Title_Text will clear out ii_column number and istr_date_range
li_counter = ii_column_number
lstr_temp_range = istr_date_range
wf_Reset_Title_Text()
ii_column_number = li_counter
istr_date_range = lstr_temp_range
//
tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)
//
//tab_1.tabpage_inst.dw_instances.Reset()
//If ll_row_count > 0 Then
//	// If there are no rows, ls_data just turned to garbage, so don't re import it
//	tab_1.tabpage_inst.dw_instances.ImportString(ls_data)
//End if
//
ls_data = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data
tab_1.tabpage_inst.dw_instances.SetFocus()

//For li_counter = 1 to 30
//	If istr_date_range[li_counter].end_date >=adt_end Then
//		For li_sub_counter = 1 to 30
//			If istr_date_range[li_counter].end_date = lstr_original_date_range[li_sub_counter].end_date AND &
//					istr_date_range[li_counter].week_day = 0 Then
//				ls_orig_col_name = 'quantity_' + string(li_sub_counter)
//				ls_new_col_name = 'quantity_' + string(li_counter)
//				For li_row_counter = 1 to ll_row_count
//					tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, lds_original_data.GetItemDecimal(li_row_counter, ls_orig_col_name))
//				Next
//			End If
//		Next
//	End if
//Next

This.tab_1.tabpage_inst.dw_instances.Event ue_Set_HSpScroll()

This.SetRedraw(True)

tab_1.tabpage_inst.dw_instances.SetColumn(li_new_column_number + li_num_tabs - 1)

return li_new_column_number

Return 0

end function

public function boolean wf_fill_values (string as_display_status);// NOTE: THIS DOESN'T DO A SETREDRAW(FALSE).  DO IT BEFORE CALLING THIS, 
// THIS IS FASTER

Date	ldt_begin, &
		ldt_end

Decimal	ld_value

Int	li_counter, &
		li_temp_step_seq, &
		li_sub_counter, &
		li_week_day

Long	ll_row_count, &
		ll_nv_row_count, &
		ll_row

String	ls_uom, &
			ls_dest_plant, &
			ls_temp, &
			ls_temp2
			
Boolean 	lb_amount_placed			

For li_counter = 1 to tab_1.tabpage_inst.dw_instances.RowCount()
	If ib_date_protection Then
		tab_1.tabpage_inst.dw_instances.SetItem(li_counter, 'protection_status', &
				'Y')
	Else
		tab_1.tabpage_inst.dw_instances.SetItem(li_counter, 'protection_status', &
				'N')
	End If
Next		

/*********************************************************
**  Set Values	
*********************************************************/
// this sort should make finding the instances a little quicker

dnv_instances.SetSort("mfg_step_sequence A, dest_plant A")
dnv_instances.Sort()
ll_nv_row_count = dnv_instances.RowCount()
ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()

ls_temp = dnv_instances.Object.DataWindow.Data
ls_temp2 = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data

// Find all of the unique step/dest_plant combinations and put rows in for them
For li_counter = 1 to ll_nv_row_count
	li_temp_step_seq = dnv_instances.GetItemNumber(li_counter, "mfg_step_Sequence")
	ls_dest_plant = dnv_instances.GetItemString(li_counter, "dest_plant")
	If iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then Continue
	ls_temp = "mfg_step_sequence = " + String(li_temp_step_seq) + &
								" And destination_plant = '" + ls_dest_plant + "'"
	If tab_1.tabpage_inst.dw_instances.Find(ls_temp, 1, ll_row_count) = 0 Then
		// not found, make sure there is not a row there with a plank dest plant
		ll_row = tab_1.tabpage_inst.dw_instances.Find("mfg_step_sequence = " + String(li_temp_step_seq), &
											1, ll_row_count)
		If ll_row = 0 Then
			// serious error here
			return false
		End if
		If iw_frame.iu_string.nf_IsEmpty(tab_1.tabpage_inst.dw_instances.GetItemString(ll_row, "destination_plant")) Then
			tab_1.tabpage_inst.dw_instances.SetItem(ll_row, "destination_plant", ls_dest_plant)
		Else
			// Need to duplicate the row
			tab_1.tabpage_inst.dw_instances.RowsCopy(ll_row, ll_row, Primary!, tab_1.tabpage_inst.dw_instances, ll_row, Primary!)
			tab_1.tabpage_inst.dw_instances.SetItem(ll_row, "destination_plant", ls_dest_plant)
			ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
		End if
	End if
Next

tab_1.tabpage_inst.dw_instances.Sort()

dnv_instances.SetSort("mfg_step_sequence A, dest_plant A")
dnv_instances.Sort()

ls_temp = dnv_instances.Object.DataWindow.Data
ls_temp2 = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data
 
For li_counter = 1 to ll_row_Count
	li_temp_step_seq = tab_1.tabpage_inst.dw_instances.GetItemNumber(li_counter, "mfg_step_Sequence")
	ls_dest_plant = tab_1.tabpage_inst.dw_instances.GetItemString(li_counter, "destination_plant")
	If IsNull(ls_dest_plant) Then ls_dest_plant = ""
	ll_row = 0
	Do
		ls_temp = "mfg_step_sequence = " + String(li_temp_step_seq)
		If Not iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) Then
			ls_temp += " And dest_plant = '" + ls_dest_plant + "'"
		End if
		ll_row = dnv_instances.Find(ls_temp, ll_row + 1, ll_nv_row_count)
		If ll_row = 0 Then Continue
		ldt_begin = dnv_instances.GetItemDate(ll_row, "begin_date")
		ldt_end =   dnv_instances.GetItemDate(ll_row, "end_date")

		// This is to correct a bug in PB 5.0.1 doing GetItemDecimal
		// remove this "if" when the bug is corrected
		If IsNull(dnv_instances.GetItemNumber(ll_row, "amount")) Then 
			ld_value = 0.00
		Else
			ld_value =  dnv_instances.GetItemDecimal(ll_row, "amount")
		End if
		
		If (dnv_instances.GetItemString(ll_row, "pattern_type") = "W") Then
			li_week_day = dnv_instances.GetItemNumber(ll_row, "pattern_day")
			ldt_begin = RelativeDate(dnv_instances.GetItemDate(ll_row, "begin_date"), li_week_day - 1)
			ldt_end = RelativeDate(dnv_instances.GetItemDate(ll_row, "begin_date"), li_week_day - 1)
		else
			li_week_day = 0
		End If
		
		If (as_display_status = 'W') AND (dnv_instances.GetItemString(ll_row, "pattern_type") = "W") Then
			lb_amount_placed = False
			For li_sub_counter = 1 to ii_column_number
				If li_sub_counter > UpperBound(istr_date_range) Then Exit
				If (ldt_begin <= istr_date_range[li_sub_counter].end_date) AND &
							(ldt_end >= istr_date_range[li_sub_counter].begin_date) AND &
							(lb_amount_placed = False) Then
						tab_1.tabpage_inst.dw_instances.SetItem(li_counter, li_sub_counter + 8 + li_week_day - 1,  ld_value)
						lb_amount_placed = True
//						MessageBox("Value placed", "instance row =" + string(ll_row) + ", amount = " + String(ld_value) + ", begin_date = " + string(ldt_begin, "mm/dd/yyyy") + ", end_date = " + string(ldt_end, "mm/dd/yyyy") + ", row =" + string(li_counter) + ", column = " + string(li_sub_counter + 8 + li_week_day - 1)) 
				End if
			Next				
		Else
			For li_sub_counter = 1 to ii_column_number
				If li_sub_counter > UpperBound(istr_date_range) Then Exit
				If ldt_begin <= istr_date_range[li_sub_counter].end_date AND &
							ldt_end >= istr_date_range[li_sub_counter].begin_date Then
						tab_1.tabpage_inst.dw_instances.SetItem(li_counter, li_sub_counter + 8,  ld_value)
//						MessageBox("Value placed", "instance row =" + string(ll_row) + ", amount = " + String(ld_value) + ", begin_date = " + string(ldt_begin, "mm/dd/yyyy") + ", end_date = " + string(ldt_end, "mm/dd/yyyy") + ", row =" + string(li_counter) + ", column = " + string(li_sub_counter + 8 + li_week_day - 1)) 
				End if
			Next
		End If	

	Loop While ll_row < ll_nv_row_count and ll_row > 0
Next

ls_temp = tab_1.tabpage_inst.dw_instances.Object.DataWindow.Data

tab_1.tabpage_inst.dw_instances.ResetUpdate()

return True
end function

public subroutine wf_instances_columns_exceeded (integer ai_column_number, date adt_start_date, date adt_end_date);Integer	li_counter, li_week_number, li_row_counter, li_row_count

String		ls_week_day, ls_TitleModify, ls_new_col_name

Decimal	ldc_null_value

li_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
SetNull(ldc_null_value)	

if ii_column_number > ai_column_number then
	For li_counter = ai_column_number + 1 to 30
		istr_date_range[li_counter].begin_date = Date('1900-01-01')
		istr_date_range[li_counter].end_date = Date('1900-01-01')
		istr_date_range[li_counter].week_day = 0
		ls_new_col_name = 'quantity_' + string(li_counter)
		For li_row_counter = 1 to li_row_count
			tab_1.tabpage_inst.dw_instances.SetItem(li_row_counter, ls_new_col_name, ldc_null_value)
		Next	
	Next
	ii_column_number = ai_column_number
End If	

// Reset column headings
//For li_counter = 1 to 30
//	// Build the string to reset text
//	ls_Modify += "quantity_" + String(li_counter) + "_t.Text = '' " + &
//					"quantity_" + String(li_counter) + "_t.Visible = 0 " + &
//					"quantity_" + String(li_counter) + "_t.BackGround.Color = 78682240 " + &
//					"quantity_" + String(li_counter) + ".Visible = 0 "
//
//Next
//tab_1.tabpage_inst.dw_instances.Modify(ls_Modify)

ls_TitleModify = ''
For li_Counter = 1 to ii_column_number
	If li_Counter <= 30 Then
		If istr_date_range[li_Counter].week_day = 0 Then
			ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
					String(istr_date_range[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
					String(istr_date_range[li_Counter].end_date, "mm/dd/yy")  +  "' " + &
					"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
					"quantity_" + String(li_Counter) + "_t.BackGround.Color = 78682240 " + &
					"quantity_" + String(li_counter) + ".Visible = 1 "
		Else	
			li_week_number = istr_date_range[li_Counter].week_day
			Choose Case li_week_number
				Case 1
					ls_week_day = 'Mon'
				Case 2
					ls_week_day = 'Tue'
				Case 3
					ls_week_day = 'Wed'
				Case 4
					ls_week_day = 'Thu'
				Case 5
					ls_week_day = 'Fri'
				Case 6
					ls_week_day = 'Sat'
				Case 7
					ls_week_day = 'Sun'
			End Choose		
			If li_week_number = 1 Then
				ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
					String(istr_date_range[li_Counter].begin_date, "mm/dd/yy") + "~~r~~n" + &
					ls_week_day + "' " + &
					"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
					"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
					"quantity_" + String(li_counter) + ".Visible = 1 " 
			Else		
				If li_week_number = 7 Then
					ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
						String(istr_date_range[li_Counter].end_date, "mm/dd/yy") + "~~r~~n" + &
						ls_week_day + "' " + &
						"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
						"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
						"quantity_" + String(li_counter) + ".Visible = 1 "
				Else
					ls_TitleModify += "quantity_" + String(li_counter) + "_t.Text = '" + &
						"~~r~~n" + &
						ls_week_day + "' " + &
						"quantity_" + String(li_Counter) + "_t.Visible = 1 " + &
						"quantity_" + String(li_Counter) + "_t.BackGround.Color = 13434879 " + &
						"quantity_" + String(li_counter) + ".Visible = 1 " 	
				End If	
			End If
		End If
	End If
Next

//wf_Reset_Title_Text()
tab_1.tabpage_inst.dw_instances.Modify(ls_TitleModify)
	

end subroutine

event deactivate;iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')

//iw_frame.im_menu.m_holding5.m_addrow.Text = is_addrow_text
//iw_frame.im_menu.m_holding5.m_deleterow.Text = is_deleterow_text

iw_frame.im_menu.mf_set_menu_item_text("m_addrow",is_addrow_text)
iw_frame.im_menu.mf_set_menu_item_text("m_deleterow",is_deleterow_text)
end event

event ue_postopen;call super::ue_postopen;Int	li_counter, &
		li_upper_bound

iw_ThisWindow = This

dnv_instances = Create DataStore
dnv_instances.DataObject = "dnv_pas_instances"

dnv_parameters = Create DataStore
dnv_parameters.DataObject = "dnv_pas_parameters"

dnv_auto_conv = Create DataStore
dnv_auto_conv.DataObject = "dnv_pas_auto_conv"

dnv_update_instances = Create DataStore
dnv_update_instances.DataObject = "dnv_pas_instances"

ids_original_instances = Create DataStore
ids_original_instances.DataObject = "dnv_pas_instances"

ids_original_instances_dw_data = Create DataStore
ids_original_instances_dw_data.DataObject = "dw_pas_instances"

dnv_update_parameters = Create DataStore
dnv_update_parameters.DataObject = "dnv_pas_parameters"

dnv_update_auto_conv = Create DataStore
dnv_update_auto_conv.DataObject = "dnv_pas_auto_conv"

ids_piece_count = Create DataStore
ids_piece_count.DataObject = "d_pas_instances_piece_count"

dnv_ratios = Create DataStore
dnv_ratios.DataObject = "d_pas_inst_ratio"

istr_error_info.se_user_id = sqlca.userid
istr_error_info.se_app_name = "PAS"
istr_error_info.se_window_name = "Instances"

iu_pas202 = Create u_pas202
If Message.ReturnValue = -1 Then Close(This)

iu_pas203 = Create u_pas203
If Message.ReturnValue = -1 Then Close(This)

iu_ws_pas1 = Create u_ws_pas1
If Message.ReturnValue = -1 Then Close(This)

iu_ws_pas3 = Create u_ws_pas3
If Message.ReturnValue = -1 Then Close(This)

li_upper_bound = UpperBound(istr_date_range)
For li_Counter = 1 to li_upper_bound
	istr_date_range[li_counter].begin_date = Date("1/1/1900")
	istr_date_range[li_counter].end_date   = Date("1/1/1900")

	istr_date_range_parm[li_counter].begin_date = Date("1/1/1900")
	istr_date_range_parm[li_counter].end_date   = Date("1/1/1900")
	
	istr_date_range_auto_conv[li_counter].begin_date = Date("1/1/1900")
	istr_date_range_auto_conv[li_counter].end_date   = Date("1/1/1900")

Next

ib_instances_redraw = True
ib_parameters_redraw = True

ib_bypass_check_for_changes = False

This.PostEvent("ue_query")
end event

event close;call super::close;If IsValid(w_pas_output_products) Then Close (w_pas_output_products)
If IsValid(iu_pas202) Then Destroy iu_Pas202
If IsValid(iu_pas203) Then Destroy iu_Pas203
If IsValid(iu_ws_pas1) Then Destroy iu_ws_pas1
If IsValid(iu_ws_pas3) Then Destroy iu_ws_pas3
If IsValid(dnv_instances) Then Destroy dnv_instances
If IsValid(dnv_parameters) Then Destroy dnv_parameters
If IsValid(dnv_update_instances) Then Destroy dnv_update_instances
If IsValid(dnv_update_parameters) Then Destroy dnv_update_parameters
If IsValid(ids_piece_count) Then Destroy ids_piece_count
end event

event activate;call super::activate;iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')

iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')

is_addrow_text = iw_frame.im_menu.mf_get_menu_item_text("m_addrow")
is_deleterow_text = iw_frame.im_menu.mf_get_menu_item_text("m_deleterow") 
iw_frame.im_menu.mf_set_menu_item_text("m_addrow","&Add Date Range")
iw_frame.im_menu.mf_set_menu_item_text("m_deleterow", "&Duplicate Transfer Step")

//If dw_header.GetItemString(1, "display_status") = 'D' Then
//	iw_frame.im_menu.mf_Disable('m_addrow')
//	iw_frame.im_menu.mf_Enable('m_next')
//	iw_frame.im_menu.mf_Enable('m_previous')
//Else
//	iw_frame.im_menu.mf_Disable('m_next')
//	iw_frame.im_menu.mf_Disable('m_previous')
//End if
If tab_1.SelectedTab = 1 Then
	tab_1.tabpage_inst.dw_instances.SetFocus()
	// disable for first time through
Else
	tab_1.tabpage_inst_parm.dw_parameters.SetFocus()
End If

wf_protect(dw_header.GetItemString(1, "display_status"), tab_1.SelectedTab)


end event

event resize;call super::resize;integer li_x = 15, &
        li_y = 45
		  

tab_1.Resize(This.width - tab_1.X - 50, This.height - tab_1.Y - 110)
tab_1.tabpage_inst.Resize(tab_1.width - tab_1.tabpage_inst.X - (li_x - 5), &
		tab_1.height - tab_1.tabpage_inst.Y - li_y)
tab_1.tabpage_inst.dw_instances.Resize(tab_1.tabpage_inst.width - tab_1.tabpage_inst.dw_instances.X - li_x, &
		tab_1.tabpage_inst.height - tab_1.tabpage_inst.dw_instances.Y)
tab_1.tabpage_inst.dw_instances.Event ue_set_hspscroll()

tab_1.tabpage_inst_parm.Resize(tab_1.width - tab_1.tabpage_inst_parm.X - (li_x - 5), &
		tab_1.height - tab_1.tabpage_inst_parm.Y - li_y)
tab_1.tabpage_inst_parm.dw_parameters.Resize(tab_1.tabpage_inst_parm.width - tab_1.tabpage_inst_parm.dw_parameters.X - li_x, &
		tab_1.tabpage_inst_parm.height - tab_1.tabpage_inst_parm.dw_parameters.Y)
tab_1.tabpage_inst_parm.dw_parameters.Event ue_set_hspscroll()

tab_1.tabpage_auto_conv.Resize(tab_1.width - tab_1.tabpage_auto_conv.X -(li_x - 5), &
		tab_1.height - tab_1.tabpage_auto_conv.Y - li_y)
tab_1.tabpage_auto_conv.dw_auto_conv.Resize(tab_1.tabpage_auto_conv.width - tab_1.tabpage_auto_conv.dw_auto_conv.X - li_x, &
		tab_1.tabpage_auto_conv.height - tab_1.tabpage_auto_conv.dw_auto_conv.Y)
tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_set_hspscroll()


end event

event ue_query;call super::ue_query;String	ls_PaRange, &
			ls_group_id, &
			ls_add_auth
			
If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "INSTPAST", ls_PARange) = -1 Then
if (sqlca.sqldbcode = 10005 or sqlca.sqldbcode = 10025) &
		AND ii_Query_Retry_Count < 2  then
		SQLCA.postevent("ue_reconnect")
		this.postevent("ue_query")
		return
	else
		// Row not found
		MessageBox("PA Range", "PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
		Close(This)
	End if
End if

ii_PAPast = Integer(ls_PARange)

ls_group_id = iw_frame.iu_netwise_data.is_groupid

  SELECT group_profile.add_auth
    INTO :ls_add_auth 
    FROM group_profile  
   WHERE ( group_profile.appid = 'PRD' ) AND  
         ( group_profile.group_id = :ls_group_id ) AND  
         ( group_profile.window_name = 'TABPAGE_INST_PARM' ) AND  
         ( group_profile.menuid = 'W_PAS_INSTANCES' )   ;

if (sqlca.sqldbcode = 10005 or sqlca.sqldbcode = 10025) &
		AND ii_Query_Retry_Count < 2  then
	SQLCA.postevent("ue_reconnect")
	this.postevent("ue_query")
	return
end if
		
If ls_add_auth = 'Y' Then
	ib_protection_status = False
Else
	ib_protection_status = True
End If

wf_retrieve()
end event

event open;call super::open;dw_header.InsertRow(0)

// This is definitely a PowerBuilder 8.0.1.90???? hack.  This needs to be fixed.  REM.
	cbx_tab_code.checked = Profileint(gw_base_frame.is_userini, "wincoordinates", "ib_tab_code",  0) = 1
//

cbx_shade_rows.checked =Profileint(gw_base_frame.is_userini, "PAS", "ShadeRows", 0) = 1


end event

on w_pas_instances.create
int iCurrent
call super::create
this.cbx_shade_rows=create cbx_shade_rows
this.uo_next_prev=create uo_next_prev
this.st_next=create st_next
this.st_previous=create st_previous
this.dw_header=create dw_header
this.tab_1=create tab_1
this.cbx_tab_code=create cbx_tab_code
this.cb_copy_button=create cb_copy_button
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_shade_rows
this.Control[iCurrent+2]=this.uo_next_prev
this.Control[iCurrent+3]=this.st_next
this.Control[iCurrent+4]=this.st_previous
this.Control[iCurrent+5]=this.dw_header
this.Control[iCurrent+6]=this.tab_1
this.Control[iCurrent+7]=this.cbx_tab_code
this.Control[iCurrent+8]=this.cb_copy_button
end on

on w_pas_instances.destroy
call super::destroy
destroy(this.cbx_shade_rows)
destroy(this.uo_next_prev)
destroy(this.st_next)
destroy(this.st_previous)
destroy(this.dw_header)
destroy(this.tab_1)
destroy(this.cbx_tab_code)
destroy(this.cb_copy_button)
end on

type cbx_shade_rows from u_base_checkbox_ext within w_pas_instances
integer x = 955
integer y = 300
integer width = 672
integer height = 76
integer taborder = 35
integer textsize = -8
fontcharset fontcharset = ansi!
string facename = "MS Sans Serif"
string text = "Shade Every Other Row"
end type

event clicked;call super::clicked;integer	li_Checked
if cbx_shade_rows.checked then
	li_Checked = 1
else
	li_Checked = 0
end if
	
SetProfileString(gw_base_frame.is_userini, "PAS", "ShadeRows",  string(li_Checked))

wf_shade_rows()

end event

type uo_next_prev from u_spin within w_pas_instances
integer x = 2990
integer y = 140
integer height = 128
integer taborder = 20
end type

on uo_next_prev.destroy
call u_spin::destroy
end on

type st_next from statictext within w_pas_instances
integer x = 2779
integer y = 140
integer width = 155
integer height = 64
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Next"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_previous from statictext within w_pas_instances
integer x = 2688
integer y = 204
integer width = 247
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Previous "
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_header from u_base_dw_ext within w_pas_instances
integer y = 16
integer width = 2848
integer height = 412
integer taborder = 10
string dataobject = "d_pas_instances_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;Boolean	lb_ret

String	ls_gettext, ls_temp, ls_type_code, ls_plant


ls_GetText = This.GetText()
Choose Case This.GetColumnName()
Case "display_status"
	If data = 'W' Then
		ls_plant = This.GetItemString(1, "plant_code")
		
		SELECT tutltypes.type_code 
			INTO :ls_type_code	
			FROM tutltypes
			WHERE (tutltypes.record_type = 'PAINSTWK') AND
					  ((tutltypes.type_code = :ls_plant) or (tutltypes.type_code = 'ALL'));
					  
		If sqlca.sqlcode = 100 Then
			MessageBox("Plant Code Error", "Plant code " + ls_plant  + " is not set up for Long Term by Day of Week")
			Return 1
		End If		
		
	End If
	
	If Not wf_check_for_changes() Then return 

	If ib_Changes_Made Then
		// if they made changes and are now switching, reinquire to ensure constistancy
		ib_reinquire_necessary = True
		ib_bypass_check_for_changes = True
		Parent.PostEvent("ue_query")
		return
	End if
	Parent.SetRedraw(False)

	wf_reset_title_text()
	wf_clear_values(Parent.tab_1.tabpage_inst.dw_instances)
	
	dnv_instances.Reset()
	dnv_instances.ImportString(ids_original_instances.Object.DataWindow.Data)	

	If ls_GetText = "W" Then
		lb_ret = wf_long_term_day_of_week()
	Else	
		If ls_GetText = "L" Then
			lb_ret = wf_long_term()
		Else
			If ls_GetText = "T" Then
				lb_ret = wf_two_week_daily()
			Else
				If ls_GetText = "H" Then
					lb_ret = wf_three_week_daily()
				Else	
					lb_ret = wf_Daily()
				End If
			End if
		End if
	End If

	If lb_ret Then
		wf_fill_values(data)
	End if
	Parent.tab_1.tabpage_inst.dw_instances.Event ue_set_hspscroll()

	wf_reset_title_text_parm()
	wf_clear_values(Parent.tab_1.tabpage_inst_parm.dw_parameters)
	If (ls_GetText = "L") or (ls_GetText = "W") Then
		lb_ret = wf_long_term_parm()
	Else
		If ls_GetText = "T" Then
			lb_ret = wf_two_week_parm()
		Else
			If ls_GetText = "H" Then
				lb_ret = wf_three_week_parm()
			Else
				lb_ret = wf_Daily_parm()
			End If
		End If
	End if

	If lb_ret Then
		wf_fill_values_parm()
	End if
	Parent.tab_1.tabpage_inst_parm.dw_parameters.Event ue_set_hspscroll()
	
	wf_reset_title_text_auto_conv()
	wf_clear_values(Parent.tab_1.tabpage_auto_conv.dw_auto_conv)
	If (ls_GetText = "L") or (ls_GetText = "W") Then
		lb_ret = wf_long_term_auto_conv()
	Else
		If ls_GetText = "T" Then
			lb_ret = wf_two_week_auto_conv()
		Else
			If ls_GetText = "H" Then
				lb_ret = wf_three_week_auto_conv()
			Else	
				lb_ret = wf_Daily_auto_conv()
			End If
		End If
	End if

	If lb_ret Then
		wf_fill_values_auto_conv()
	End if
	Parent.tab_1.tabpage_auto_conv.dw_auto_conv.Event ue_set_hspscroll()
	
	// disable if inquired before today
	IF idt_inquire_date < Today() Then
		iw_frame.im_menu.mf_disable('m_addrow')
		iw_frame.im_menu.mf_disable('m_deleterow')
	End If
	
	wf_shade_rows()	
		
	tab_1.tabpage_inst.dw_instances.ResetUpdate()
	tab_1.tabpage_inst_parm.dw_parameters.ResetUpdate()
	tab_1.tabpage_auto_conv.dw_auto_conv.ResetUpdate()	

	Parent.SetRedraw(True)
End Choose
end event

event constructor;call super::constructor;ib_updateable = False
end event

event itemerror;call super::itemerror;return 1
end event

type tab_1 from tab within w_pas_instances
event create ( )
event destroy ( )
integer y = 396
integer width = 2981
integer height = 1004
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean raggedright = true
integer selectedtab = 1
tabpage_inst tabpage_inst
tabpage_inst_parm tabpage_inst_parm
tabpage_auto_conv tabpage_auto_conv
end type

on tab_1.create
this.tabpage_inst=create tabpage_inst
this.tabpage_inst_parm=create tabpage_inst_parm
this.tabpage_auto_conv=create tabpage_auto_conv
this.Control[]={this.tabpage_inst,&
this.tabpage_inst_parm,&
this.tabpage_auto_conv}
end on

on tab_1.destroy
destroy(this.tabpage_inst)
destroy(this.tabpage_inst_parm)
destroy(this.tabpage_auto_conv)
end on

event selectionchanged;String	ls_menu, ls_window 

wf_protect(dw_header.GetItemString(1, "display_status"), newindex)


Choose Case newindex
	Case 1
		cb_copy_button.Visible = False
		cb_copy_button.Text = '' 
		is_tab_index = '1'
	Case 2
		is_tab_index = '2'
		If ib_save_auto Then
			cb_copy_button.Visible = True
			cb_copy_button.Text = 'Copy to Auto Conv Parm'
		Else
			cb_copy_button.Visible = False
			cb_copy_button.Text = '' 
		End If
	Case 3
		is_tab_index = '3'
		If ib_save_parm Then
			cb_copy_button.Visible = True
			cb_copy_button.Text = 'Copy to Instance Parm'
		Else
			cb_copy_button.Visible = False
			cb_copy_button.Text = '' 
		End If
End Choose


end event

event selectionchanging;Choose Case oldindex
	Case 2 
		If tab_1.tabpage_inst_parm.dw_parameters.AcceptText() = -1 Then
			Return 1
		End If
	Case 3
		If tab_1.tabpage_auto_conv.dw_auto_conv.Accepttext() = -1 Then
			Return 1
		End If
End Choose
end event

type tabpage_inst from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 100
integer width = 2944
integer height = 888
long backcolor = 67108864
string text = "Instances"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_instances dw_instances
end type

on tabpage_inst.create
this.dw_instances=create dw_instances
this.Control[]={this.dw_instances}
end on

on tabpage_inst.destroy
destroy(this.dw_instances)
end on

type dw_instances from u_base_dw_ext within tabpage_inst
event ue_resetupdate ( )
event ue_change_plant ( )
event ue_rbuton_up ( )
event ue_scroll_child ( )
event ue_addrow ( )
event ue_next_week ( )
event ue_set_hspscroll ( )
event ue_prev_week ( )
event ue_duplicate_transfer ( )
event ue_next_two_weeks ( )
event ue_prev_two_weeks ( )
event ue_next_three_weeks ( )
event ue_prev_three_weeks ( )
integer width = 2880
integer height = 796
integer taborder = 2
string dataobject = "d_pas_instances"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_resetupdate;call super::ue_resetupdate;This.ResetUpdate()
end event

event ue_change_plant;call super::ue_change_plant;This.SetText(Message.StringParm)
end event

event ue_rbuton_up;call super::ue_rbuton_up;If IsValid(w_inst_percent_scroll) Then
	Close(w_inst_percent_scroll)
End if
end event

event ue_scroll_child;call super::ue_scroll_child;Long		ll_cw_start_y, &
			ll_cw_end_y


If Not IsValid(w_inst_percent_scroll) Then return

SetMicroHelp(String(Now()) + " " + String(Message.WordParm) + " " + String(Message.LongParm))

ll_cw_start_y = w_inst_percent_scroll.Y
ll_cw_end_y = w_inst_percent_scroll.Y + w_inst_percent_scroll.Height

If iw_frame.PointerY() > ll_cw_end_y Then
	w_inst_percent_scroll.wf_ScrollNext()
	This.PostEvent("ue_scroll_child")
Elseif iw_frame.PointerY() < ll_cw_start_y Then
	w_inst_percent_scroll.wf_ScrollPrevious()
	This.PostEvent("ue_scroll_child")
End if


end event

event ue_addrow();Date	ldt_begin, &
		ldt_end

Int	li_temp, &
		li_ret

String	ls_temp, ls_pattern


// Make sure there is room for the date range
If ii_column_number >= UpperBound(istr_date_range) Then
	li_temp = UpperBound(istr_date_range)
	MessageBox("Date Range", "Maximum Number of Date Ranges already displayed.  " + &
					"Only " + String(li_temp) + " Date Ranges can be displayed.")
	return 
End if
OpenWithParm(w_pas_instance_date_range, iw_ThisWindow)
ls_temp = Message.StringParm

If iw_frame.iu_string.nf_isEmpty(ls_temp) Then return 

ldt_begin = Date(mid(ls_temp, 1, 10))
ldt_end = Date(mid(ls_temp, 12, 10))
ls_pattern = mid(ls_temp, 23, 1)

if ls_pattern = 'Y' Then
	wf_add_date_range_pattern (ldt_begin, ldt_end)
Else
	If ii_column_number >= 30 Then
		wf_add_date_range_overflow(ldt_begin, ldt_end)
	Else
		wf_add_date_range(ldt_begin, ldt_end)
	End If
End If

tab_1.tabpage_inst.dw_instances.SetFocus()

If ii_column_number > 0 Then 
	li_ret = tab_1.tabpage_inst.dw_instances.SetColumn(5)
End if
If tab_1.tabpage_inst.dw_instances.RowCount() > 0 Then 
	li_ret = tab_1.tabpage_inst.dw_instances.SetRow(1)
End if


end event

event ue_next_week();This.SetRedraw(False)
wf_clear_values(This)
wf_Daily()
wf_fill_values(dw_header.GetItemString(1, "display_status"))
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_set_hspscroll();Int	li_position, &
		li_position2

if this.hsplitscroll then
	If Integer(This.Object.DataWindow.HorizontalScrollMaximum) <= 1 Then return
	
	if ib_initial_inquire then
		If This.Find("step_type = 'T'", 1, 10000) > 0 Then 
			li_position = Integer(This.Object.mfg_step_descr.X) + &
								Integer(This.Object.mfg_step_descr.Width) + 15
			li_position2 = li_position + 15
		Else
			li_position = Integer(This.Object.mfg_step_descr.X) + &
								Integer(This.Object.mfg_step_descr.Width) + 15
			li_position2 = Integer(This.Object.destination_plant.X) + &
								Integer(This.Object.destination_plant.Width) + 30
		End if
		
		This.Object.DataWindow.HorizontalScrollSplit = String(li_position)
		This.Object.DataWindow.HorizontalScrollPosition2 =  String(li_position2)
		This.Object.DataWindow.HorizontalScrollPosition =  '0'
	else
		If This.Find("step_type = 'T'", 1, 10000) > 0 Then 
			li_position = Integer(This.Object.mfg_step_descr.X) + &
								Integer(This.Object.mfg_step_descr.Width) + 15
//			li_position2 = li_position + 15
		Else
			li_position = Integer(This.Object.mfg_step_descr.X) + &
								Integer(This.Object.mfg_step_descr.Width) + 15
//			li_position2 = Integer(This.Object.destination_plant.X) + &
//								Integer(This.Object.destination_plant.Width) + 30
		End if
		
		This.Object.DataWindow.HorizontalScrollSplit = String(li_position)
//		This.Object.DataWindow.HorizontalScrollPosition2 =  String(li_position2)
		This.Object.DataWindow.HorizontalScrollPosition =  '0'
	end if
		
End if
end event

event ue_prev_week();This.SetRedraw(False)
wf_clear_values(This)
wf_Daily()
wf_fill_values(dw_header.GetItemString(1, "display_status"))
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_duplicate_transfer;call super::ue_duplicate_transfer;// This really duplicates a transfer step, not deletes a row
Long		ll_current_row

String	ls_data, &
			ls_temp


ll_current_row = This.GetRow()
If ll_current_row < 1 Then 
	iw_frame.SetMicroHelp("Cannot Duplicate Transfer Step; No Row is Current")
	return 
End if
If This.GetItemString(ll_current_row, "step_type") <> 'T' Then
	MessageBox("Duplicate Transfer Step", "Current row is not a transfer step.  " + &
					"Please position the cursor on a transfer step and try again.")
	return 
End if

ls_data = iw_ThisWindow.wf_duplicate_step(This, ll_current_row)
This.ImportString(ls_data)

This.Sort()

end event

event ue_next_two_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_two_week_Daily()
wf_fill_values(dw_header.GetItemString(1, "display_status"))
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_prev_two_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_two_week_daily()
wf_fill_values(dw_header.GetItemString(1, "display_status"))
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_next_three_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_three_week_Daily()
wf_fill_values(dw_header.GetItemString(1, "display_status"))
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_prev_three_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_three_week_daily()
wf_fill_values(dw_header.GetItemString(1, "display_status"))
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event clicked;call super::clicked;If Left(is_BandAtPointer, 6) <> 'detail' Then return

Choose Case dwo.Name
Case	"mfg_step_descr"
	SetPointer(HourGlass!)
	This.SelectRow(0, False)
	This.SelectRow(row, True)
	This.SetRow(row)
	If This.GetItemString(Row, 'step_type') = 'T' Then
		iw_Frame.SetMicrohelp("Transfer Steps cannot have output products")
		return
	End if
	If IsValid(w_pas_output_products) Then
		close(w_pas_output_products)
	end if
	OpenWithParm(w_pas_output_products, iw_ThisWindow)
//		w_pas_output_products.PostEvent("ue_PostOpen")
	
End Choose



 
end event

event constructor;call super::constructor;Int	li_counter, &
		li_upper_Bound
		
String	ls_Modify, &
			ls_UomData

ls_Modify = ''
li_upper_bound = UpperBound(istr_date_range)


For li_counter = 1 to li_upper_bound
	// Build the string to reset text
	ls_Modify += "quantity_" + String(li_counter) + "_t.Text = '' " + &
					"quantity_" + String(li_counter) + "_t.Visible = 0 " + &
					"quantity_" + String(li_counter) + ".Visible = 0 " + &
					"uom_" + String(li_counter) + ".Visible = 0 " 

Next

This.Modify(ls_Modify)


This.SetTabOrder("destination_plant", 5)
This.Object.destination_plant.Tag = 'Double Click Destination Plant to Display List Box'


end event

event doubleclicked;call super::doubleclicked;String	ls_Protected

If dwo.Type <> "column" Then Return

ls_protected = dwo.Protect

If Not IsNumber(ls_protected) Then
	// then it is an expression, evaluate it
	// first char is default value, then tab, then expression
	// the whole thing is surrounded by quotes
	ls_protected = This.Describe('Evaluate("' + Mid(ls_protected, &
									Pos(ls_protected, '~t') + 1, &
									Len(ls_protected) - Pos(ls_protected, '~t') - 1) + &
									'", ' + String(Row) + ')')
End if

If dwo.Name = "destination_plant" and ls_protected = "0" Then
	OpenWithParm(w_pas_plant_dddw, is_dest_plants, parent.GetParent())
End if
end event

event itemchanged;call super::itemchanged;Double			ld_data

Date				ldt_end_date, &
					ldt_begin_date, &
					ldt_inst_date
					
Integer			li_column_number, &
					li_rtn
					
Integer			li_step_seq, &
					li_date_offset

Long				ll_column, &
					ll_row_count, &
					ll_row

String			ls_temp, &
					ls_filter, &
					ls_detail_errors, &
					ls_plant, &
					ls_find_string

u_string_functions			lu_string

ll_Column = Long(dwo.ID)

li_column_number = Integer(mid(dwo.name, 10))

ls_temp = This.Describe('quantity_' + String(li_column_number) + '_t.text')

li_step_seq = This.GetItemNumber(row, 'mfg_step_sequence')
ls_plant = This.GetItemString(row, 'destination_plant')

This.SelectText(1, Len(data))

If Left(dwo.name, 8) = 'quantity' Then
	
	IF IsNull(data) Then
		ld_data = 0
	else
		ld_data = Double(data)
	End If

	// Prevent entering of negative numbers
	If ld_data < 0 Then
		// ItemError Event will take care of the message
		Return 1
	End if

	ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
	If lu_string.nf_IsEmpty(ls_plant) Then
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq)
	Else
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq) + &
						" And destination_plant = '" + ls_plant + "'"
	End If
	ll_row = tab_1.tabpage_inst_parm.dw_parameters.Find(ls_find_string, 1, ll_row_count)

	If ll_row > 0 Then
		If (dw_header.GetItemString(1, 'display_status') = 'L') or (dw_header.GetItemString(1, 'display_status') = 'W') Then
			If (dw_header.GetItemString(1, 'display_status') = 'L') Then
				ldt_begin_date = date(Mid(ls_temp, 2, 10))
				ls_temp = Right(ls_temp, 11)
				ldt_end_date = Date(Left(ls_temp, len(ls_temp) - 1))
//Hack because we are displaying only two digit year in the dates
//for instances.  And if the date includes a year greater than 50
//Powerbuilder converts the century to be '19'.  When we use 12-31-2999
//as the default end of time, it gets converted to 12-31-1999 which has
//already expired.

				if year(ldt_begin_date) < 2000 then
					ldt_begin_date = date("20" + Mid(string(ldt_begin_date, "yyyy-mm-dd"),3,8))
				end if
				if year(ldt_end_date) < 2000 then
					ldt_end_date = date("20" + Mid(string(ldt_end_date, "yyyy-mm-dd"),3,8))
				end if
				li_rtn = wf_check_parms_long(ldt_begin_date, ldt_end_date, ll_row, ld_data)
			Else
				If istr_date_range[li_column_number].week_day > 0 then
					ldt_begin_date = RelativeDate(istr_date_range[li_column_number].begin_date,istr_date_range[li_column_number].week_day - 1) 
					ldt_end_date = ldt_begin_date
					li_date_offset = 0
					DO WHILE ldt_begin_date <= istr_date_range[li_column_number].end_date
						li_rtn = wf_check_parms_long(ldt_begin_date, ldt_end_date, ll_row, ld_data)
						li_date_offset = li_date_offset + 7
						ldt_begin_date = RelativeDate(ldt_begin_date, li_date_offset)
						ldt_end_date = ldt_begin_date
					LOOP
				Else
					ldt_begin_date = istr_date_range[li_column_number].begin_date
					ldt_end_date = istr_date_range[li_column_number].end_date
					li_rtn = wf_check_parms_long(ldt_begin_date, ldt_end_date, ll_row, ld_data)
				End If
			End If
		Else
			ldt_inst_date = date(Mid(ls_temp, 2, 10))
			li_rtn = wf_check_parms_daily(ldt_inst_date, ll_row, ld_data)
		End IF
		Choose Case li_rtn 
			Case -1
				iw_frame.SetMicroHelp("This Instance is less than the minimum")
				Return 1
			Case 1
				iw_frame.SetMicroHelp("This Instance is greater than the maximum")
				Return 1
		End Choose
	End If
	ls_detail_errors = This.GetItemString(row, 'detail_errors')
	if mid(ls_detail_errors, li_column_number, 1) <> 'U' then
		ls_detail_errors = Replace(ls_detail_errors, li_column_number, 1, ' ')
		This.SetItem(row, 'detail_errors', ls_detail_errors)
	end if	
End if

If dwo.name = 'destination_plant' Then
	// make sure they type in a good plant
	is_dest_plants =  String(Integer(data),"000")
	If Not IsNumber(data) Or is_dest_plants = '000' Then
		MessageBox("Destination Plant", "Destination Plant is invalid.")
		Return 1
	End if
	
	// make sure the plant isn't from the plant inquired on.
	If data = dw_header.GetItemString(1, 'plant_code') Then
		MessageBox("Destination Plant", "Destination Plant can't be the same as the plant " + &
				"who produced it.")
		Return 1
	End if
	
	// make sure that there are no duplicate plant/step combinations
	If Len(Trim(data)) = 0 Then return 
	If This.Find("destination_plant = '" + data + "'", 1, row - 1) > 0 Then
		MessageBox("Destination Plant", "Destination Plant must be unique for each step.")
		Return 1
	End if
	If row < This.RowCount() Then
		If This.Find("destination_plant = '" + data + "'", row + 1, This.RowCount()) > 0 Then
			MessageBox("Destination Plant", "Destination Plant must be unique for each step.")
			Return 1
		End if
	End if
	
	// This puts in the leading zeroes if they only entered one or two digits
	This.SetItem(row, "destination_plant",String(Integer(data),"000"))
	This.SetText(String(Integer(data),"000"))
	return 2
End if
iw_frame.SetMicroHelp("Ready")

end event

event itemerror;call super::itemerror;Double					ld_data

Integer					li_rtn

String					ls_column_name

u_string_functions	lu_string


This.SelectText(1, Len(data))

ls_column_name = dwo.Name 

Choose Case Left(ls_column_name, 3)
Case "des"
	Return 1
Case "qua"
	ld_data = Double(data)
	If lu_string.nf_IsEmpty(data) Then
		This.SetItem(row, ls_column_name, 0.00)
		li_rtn = This.Event itemchanged(row, dwo, String(ld_data))
		If li_rtn = 1 Then
			Return 1
		Else
			iw_frame.SetMicrohelp('Ready')
			Return 3
		End IF
	End If

	If ld_data < 0 Then
		tab_1.SelectedTab = 1
		iw_Frame.SetMicroHelp("Quantity cannot be negative")
	Else
		If ld_data > 100.00 Then
			tab_1.SelectedTab = 1
			iw_Frame.SetMicroHelp("Quantity cannot be greater than 100.00")
		Else
			IF Not IsNumber(data) Then
				iw_Frame.SetMicroHelp(data + " is not a valid Instance")
			End If
		End If
	End if
	return 1
End Choose


end event

event itemfocuschanged;call super::itemfocuschanged;Decimal	ld_value

String	ls_quantifier, &
			ls_column_name


ls_column_name = This.GetColumnName()
If ls_column_name <> "destination_plant" Then
	If IsValid(w_pas_plant_dddw) Then
		Close(w_pas_plant_dddw)
	End if
End if
If Left(ls_column_name, 5) = 'quant' Then
	If IsNull(dw_instances.GetItemNumber(1, "sum_" + Right(ls_column_name, &
				Len(ls_column_name) - Pos(ls_column_name, '_', 1)))) Then 
		ld_value = 0.00
	Else
		// Updated 08/21/96 tjb -- had rounding problem with some decimal numbers
		ld_value = Round(dw_instances.GetItemDecimal(1, "sum_" + Right(ls_column_name, &
						Len(ls_column_name) - Pos(ls_column_name, '_', 1))), 3)
	End If
	If ld_value = 0.00 Or IsNull(ld_Value) Then 
		SetMicroHelp("Ready")
		return
	End if
	If ld_value > 100 then
		ls_quantifier = String(Abs(ld_value - 100)) + " % more than"
	Elseif ld_value < 100 Then
		If dw_header.GetItemString(1, "product_type") = 'S' Then
			// product is an sku
			SetMicroHelp("SKU Instance Total is " + String(ld_value, "0.00"))
			return
		End if
		ls_quantifier = String(Abs(ld_value - 100)) + " % less than"
	Else
		ls_quantifier = "equal to"
	End if
	SetMicroHelp("Instances are " + ls_quantifier + " 100 %")
End if

// This is definitely a PowerBuilder 8.0.1.90???? hack.  This needs to be fixed.  REM.
//	if cbx_tab_code.checked then
		this.event ue_set_hspscroll()
//	end if
//
end event

event rbuttondown;call super::rbuttondown;Integer	li_column_number

Decimal	ldc_value, &
			ldc_keyvalue, &
			ldc_ratio, &
			ldc_boxes

String	ls_stringparm, &
			ls_PieceCount, &
			ls_ColumnName, &
			ls_fab_uom, &
			ls_step_desc, &
			ls_date, &
			ls_string, &
			ls_ratio_data

Long		ll_cw_start_y, &
			ll_cw_end_y, &
			ll_sequence, &
			ll_find


If This.AcceptText() = -1 Then return 

If Left(is_BandAtPointer, 6) <> 'detail' Then return

// This is only valid for Daily
If (dw_header.GetItemString(1, 'display_status') = 'L') OR  (dw_header.GetItemString(1, 'display_status') = 'W') Then return
If row < 1 Then return

ls_ColumnName = dwo.Name

li_column_number = Integer(mid(ls_columnName, 10, 2))

If Left(ls_ColumnName, 8) = 'quantity' Then

	// This is a workaround in PB 5.0.1 remove this when the patch is available
	If IsNull(This.GetItemNumber(row, ls_ColumnName)) Then
		ldc_value = 0
	Else
		ldc_value = This.GetItemDecimal(row, ls_ColumnName)
	End if

	// All the quantities are quantity_??
	ls_PieceCount = This.Describe('total_' + Mid(dwo.Name, 10) + '.Text')
	If Long(ls_PieceCount) = 0 Then
		iw_frame.SetMicroHelp("Percent Calculator is not available when Piece Count is zero")
		return
	End if
	If IsNull(This.GetItemNumber(row, 'key_pieces_per_box')) Then
		ldc_keyvalue = 0
	Else
		ldc_keyvalue = This.GetItemDecimal(row,'key_pieces_per_box')
	End If 
	
	ls_ratio_data = dnv_ratios.Describe("DataWindow.Data")
	
	ll_sequence = This.GetItemNumber(row, 'mfg_step_sequence')
	ls_date = This.Describe('quantity_' + String(li_column_number) + '_t.text')
	ls_date = String(date(mid(ls_date, 2,10)), 'yyyy-mm-dd')
	ls_string = "step_sequence = " + String(ll_sequence) + " and ratio_date = " + &
					ls_date
	ll_find = dnv_ratios.Find(ls_string, 1, dnv_ratios.RowCount() + 1)
	If ll_find > 0 Then
		If IsNull(dnv_ratios.GetItemNumber(ll_find, 'ratio')) Then
			ldc_ratio = 0
		Else
			ldc_ratio = dnv_ratios.GetItemDecimal(ll_find,'ratio')
		End If
		If IsNull(dnv_ratios.GetItemNumber(ll_find, 'boxes')) Then
			ldc_boxes = 0
		Else
			ldc_boxes = dnv_ratios.GetItemDecimal(ll_find,'boxes')
		End If
	End if
	ls_fab_uom = This.GetItemString(row, 'fab_uom')
	ls_step_desc = This.GetItemString(row, 'mfg_step_descr')
	ls_StringParm = String(ldc_value,"0.00") + '~t' + ls_PieceCount + '~t' + &
					String(ldc_boxes,"####0.0000") + '~t' + String(ldc_ratio,"0.000000") + '~t' + ls_fab_uom + '~t' + ls_step_desc
	OpenWithParm(w_inst_percent_spin, ls_StringParm)
End If
end event

event scrollhorizontal;call super::scrollhorizontal;If IsValid(w_pas_plant_dddw) Then
	w_pas_plant_dddw.Move(w_pas_plant_dddw.wf_GetXPos(), w_pas_plant_dddw.wf_GetYPos())
End if


end event

event scrollvertical;call super::scrollvertical;If IsValid(w_pas_plant_dddw) Then
	w_pas_plant_dddw.Move(w_pas_plant_dddw.wf_GetXPos(), w_pas_plant_dddw.wf_GetYPos())
End if
end event

event ue_mousemove;call super::ue_mousemove;If IsValid(w_inst_percent_scroll) Then
	This.PostEvent("ue_scroll_child")
End if
end event

type tabpage_inst_parm from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2944
integer height = 888
long backcolor = 67108864
string text = "Instance Parameters"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_parameters dw_parameters
end type

on tabpage_inst_parm.create
this.dw_parameters=create dw_parameters
this.Control[]={this.dw_parameters}
end on

on tabpage_inst_parm.destroy
destroy(this.dw_parameters)
end on

type dw_parameters from u_base_dw_ext within tabpage_inst_parm
event ue_resetupdate ( )
event ue_change_plant ( )
event ue_scroll_child ( )
event ue_addrow ( )
event ue_next_week ( )
event ue_set_hspscroll ( )
event ue_prev_week ( )
event ue_duplicate_transfer ( )
event ue_postitemchanged ( )
event ue_next_two_weeks ( )
event ue_prev_two_weeks ( )
event ue_next_three_weeks ( )
event ue_prev_three_weeks ( )
integer width = 2880
integer height = 796
integer taborder = 2
string dataobject = "d_pas_inst_parms"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_resetupdate;call super::ue_resetupdate;This.ResetUpdate()
end event

event ue_change_plant;call super::ue_change_plant;This.SetText(Message.StringParm)
end event

event ue_addrow();Date				ldt_begin, &
					ldt_end

Integer			li_ret, &
					li_temp

String			ls_temp, ls_pattern


// Make sure there is room for the date range
If ii_column_number_parm >= UpperBound(istr_date_range_parm) Then
	li_temp = UpperBound(istr_date_range_parm)
	MessageBox("Date Range", "Maximum Number of Date Ranges already displayed.  " + &
					"Only " + String(li_temp) + " Date Ranges can be displayed.")
	return 
End if

OpenWithParm(w_pas_instance_date_range, iw_ThisWindow)
ls_temp = Message.StringParm

If iw_frame.iu_string.nf_isEmpty(ls_temp) Then return 

ldt_begin = Date(mid(ls_temp, 1, 10))
ldt_end = Date(mid(ls_temp, 12, 10))
ls_pattern = mid(ls_temp, 23, 1)

wf_add_date_range_parm(ldt_begin, ldt_end)

tab_1.tabpage_inst_parm.dw_parameters.SetFocus()

If ii_column_number_parm > 0 Then 
	li_ret = tab_1.tabpage_inst_parm.dw_parameters.SetColumn(5)
End if
If tab_1.tabpage_inst_parm.dw_parameters.RowCount() > 0 Then 
	li_ret = tab_1.tabpage_inst_parm.dw_parameters.SetRow(1)
End if


end event

event ue_next_week;call super::ue_next_week;This.SetRedraw(False)
wf_clear_values(This)
wf_Daily_parm()
wf_fill_values_parm()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_set_hspscroll();Int	li_position, &
		li_position2


if this.hsplitscroll then
	If Integer(This.Object.DataWindow.HorizontalScrollMaximum) <= 1 Then return
	
	If This.Find("step_type = 'T'", 1, 10000) > 0 Then 
		li_position = Integer(This.Object.priority.X) + &
							Integer(This.Object.priority.Width) + 10
		li_position2 = li_position + 10
	Else
		li_position = Integer(This.Object.priority.X) + &
							Integer(This.Object.priority.Width) + 15
		li_position2 = Integer(This.Object.destination_plant.X) + &
							Integer(This.Object.destination_plant.Width) + 15
	End if
	
	This.Object.DataWindow.HorizontalScrollSplit = String(li_position)
	This.Object.DataWindow.HorizontalScrollPosition2 =  String(li_position2)
	This.Object.DataWindow.HorizontalScrollPosition =  '0'
end if
end event

event ue_prev_week();This.SetRedraw(False)
wf_clear_values(This)
wf_Daily_parm()
wf_fill_values_parm()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_duplicate_transfer();// This really duplicates a transfer step, not deletes a row
Long		ll_current_row

String	ls_data, &
			ls_temp


ll_current_row = This.GetRow()
If ll_current_row < 1 Then 
	iw_frame.SetMicroHelp("Cannot Duplicate Transfer Step; No Row is Current")
	return 
End if

If This.GetItemString(ll_current_row, "step_type") <> 'T' Then
	MessageBox("Duplicate Transfer Step", "Current row is not a transfer step.  " + &
					"Please position the cursor on a transfer step and try again.")
	return 
End if

ls_data = iw_ThisWindow.wf_duplicate_step(This, ll_current_row)
This.ImportString(ls_data)

This.Sort()

end event

event ue_next_two_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_two_week_parm()
wf_fill_values_parm()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_prev_two_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_two_week_parm()
wf_fill_values_parm()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_next_three_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_three_week_parm()
wf_fill_values_parm()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_prev_three_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_three_week_parm()
wf_fill_values_parm()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event itemchanged;call super::itemchanged;Double	ld_data, &
			ld_max, &
			ld_min, &
			ld_temp

Integer	li_column_number, &
			li_step_seq

Long		ll_Column, &
			ll_row, &
			ll_row_count

Date		ldt_begin_date, &
			ldt_end_date, &
			ldt_parm_date

String	ls_filter, &
			ls_parm_date, &
			ls_detail_errors, &
			ls_plant, &
			ls_find_string, &
			ls_data


ll_Column = Long(dwo.ID)

ld_data = Double(data)
li_column_number = Integer(mid(dwo.name, 5))
ls_parm_date = This.Describe('parm_' + String(li_column_number) + '_t.text')

li_step_seq = This.GetItemNumber(row, 'mfg_step_sequence')
ls_plant = This.GetItemString(row, 'destination_plant')

If Left(dwo.name, 3) = 'min' Then
			
	// Prevent entering of negative numbers
	If ld_data < 0 or ld_data > 100.00 Then
		// ItemError Event will take care of the message
		Return 1
	End if

	ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
	If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq)
	Else
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq) + &
						" And destination_plant = '" + ls_plant + "'"
	End If

	ll_row = tab_1.tabpage_inst.dw_instances.Find(ls_find_string, 1, ll_row_count)
	//IF the row on instances doesn't exist build one so they can put in a instance that doesn't
	// violate the minimum
	If ll_row = 0 then
		ll_row = row
		ls_data = iw_ThisWindow.wf_duplicate_step(This, ll_row)
		tab_1.tabpage_inst.dw_instances.ImportString(ls_data)
		ll_row = tab_1.tabpage_inst.dw_instances.RowCount()

		If Not iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
			tab_1.tabpage_inst.dw_instances.SetItem(ll_row, 'destination_plant', ls_plant)
		End If
	End If

	If IsNull(This.GetItemNumber(row, 'max_' + String(li_column_number))) Then 
		ld_max = 00.00
		This.SetItem(row, 'max_' + String(li_column_number), ld_max)
	Else
		ld_max = This.GetItemDecimal(row, 'max_' + String(li_column_number))
	End If
	If (dw_header.GetItemString(1, 'display_status') = 'L') or (dw_header.GetItemString(1, 'display_status') = 'W') Then
		ldt_begin_date = date(Mid(ls_parm_date, 2, 10))
		ls_parm_date = Right(ls_parm_date, 11)
		ldt_end_date = Date(Left(ls_parm_date, len(ls_parm_date) - 1))
		If Not wf_validate_instance_long(ldt_begin_date, ldt_end_date, ll_row, 'min', ld_data, ld_max) then
			iw_frame.SetMicrohelp('Changing this Parameter violates an instance already ' + &
					'entered.')
			Return
		End If
	Else
		ldt_parm_date = date(Mid(ls_parm_date, 2, 10))
		If Not wf_validate_instance_daily(ldt_parm_date, ll_row, 'min', ld_data, ld_max) Then
			iw_frame.SetMicroHelp('Changing this Parameter violates an instance already ' + &
					'entered.')
			Return
		End IF
	End IF
	
	ll_row = tab_1.tabpage_auto_conv.dw_auto_conv.Find(ls_find_string, 1, ll_row_count)
	
	If ll_row > 0 Then
		If ld_data > tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(ll_row, 'min_' + String(li_column_number)) Then
			MessageBox('Min Value Error', 'Min value must be less than or equal to Min Value on Auto Conversion tab')
			Return 1
		End If
	End If
	
End if


If Left(dwo.name, 3) = 'max' Then
	// Prevent entering of negative numbers
	If ld_data < 0 or ld_data > 100.00 Then
		// ItemError Event will take care of the message
		Return 1
	End if
	
	//Check to see that they aren't preventing themselves from entering 100% Instances
	
	If IsNull(This.GetItemNumber(row, 'max_' + String(li_column_number))) Then 
		ld_max = 00.00
	Else
		ld_max = This.GetItemDecimal(row, 'max_' + String(li_column_number)) 
	End IF
	If dw_header.GetItemString(1, "product_type") <> 'S' and &
			This.GetItemDecimal(1, 'sum_max_' + String(li_column_number)) - &
			ld_max + ld_data < 100.00 Then
		MessageBox('Parameter Violation', 'This will not allow you to enter instances' + &
				' totaling 100 percent.  Please enter a valid Parameter.')
		Return 1
	End If
	
	//Find the same row on instances.
	ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
	If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq)
	Else
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq) + &
						" And destination_plant = '" + ls_plant + "'"
	End If

	ll_row = tab_1.tabpage_inst.dw_instances.Find(ls_find_string, 1, ll_row_count)
	If ll_row > 0 then
		If IsNull(This.GetItemNumber(row, 'min_' + String(li_column_number))) Then 
			ld_min = 0.00
		Else
			ld_min = This.GetItemDecimal(row, 'min_' + String(li_column_number))
		End If

		If (dw_header.GetItemString(1, 'display_status') = 'L') or (dw_header.GetItemString(1, 'display_status') = 'W') Then
			ldt_begin_date = date(Mid(ls_parm_date, 2, 10))
			ls_parm_date = Right(ls_parm_date, 11)
			ldt_end_date = Date(Left(ls_parm_date, len(ls_parm_date) - 1))
			If Not wf_validate_instance_long(ldt_begin_date, ldt_end_date, ll_row, 'max', ld_min, ld_data) then
				iw_frame.SetMicroHelp('Changing this Parameter violates an instance already ' + &
						'entered.')
				Return
			End If
		Else
			ldt_parm_date = date(Mid(ls_parm_date, 2, 10))
			If	Not wf_validate_instance_daily(ldt_parm_date, ll_row, 'max', ld_min, ld_data) Then
				iw_frame.SetMicroHelp('Changing this Parameter violates an instance already ' + &
						'entered.')
				Return
			End IF
		End If
	End If
	
	ll_row = tab_1.tabpage_auto_conv.dw_auto_conv.Find(ls_find_string, 1, ll_row_count)
	
	IF ll_row > 0 Then
		If ld_data < tab_1.tabpage_auto_conv.dw_auto_conv.GetItemNumber(ll_row, 'max_' + String(li_column_number)) Then
			MessageBox('Max Value Error', 'Max value must be greater than or equal to Max value on Auto Conversion tab')
			Return 1
		End If
	End If
	
End if

If dwo.name = 'destination_plant' Then
	is_dest_plants =  String(Integer(data),"000")
	If Not IsNumber(data) Or is_dest_plants = '000' Then
		MessageBox("Destination Plant", "Destination Plant is invalid.")
		Return 1
	End if
	
	// make sure the plant isn't from the plant inquired on.
	If data = dw_header.GetItemString(1, 'plant_code') Then
		MessageBox("Destination Plant", "Destination Plant can't be the same as the plant " + &
				"who produced it.")
		Return 1
	End if
	
	// make sure that there are no duplicate plant/step combinations
	If Len(Trim(data)) = 0 Then return 
	If This.Find("destination_plant = '" + data + "'", 1, row - 1) > 0 Then
		MessageBox("Destination Plant", "Destination Plant must be unique for each step.")
		Return 1
	End if
	If row < This.RowCount() Then
		If This.Find("destination_plant = '" + data + "'", row + 1, This.RowCount()) > 0 Then
			MessageBox("Destination Plant", "Destination Plant must be unique for each step.")
			Return 1
		End if
	End if
	
	// This puts in the leading zeroes if they only entered one or two digits
	This.SetItem(row, "destination_plant",String(Integer(data),"000"))
	This.SetText(String(Integer(data),"000"))
	return 2
End if

iw_frame.SetMicroHelp("Ready")


end event

event constructor;call super::constructor;DataWindowChild	dwc_uom, &
						dwc_uom1

Int	li_counter, &
		li_upper_Bound, &
		li_dddw_row_count, &
		li_sub_counter

String	ls_Modify, &
			ls_UomData


ls_Modify = ''
li_upper_bound = UpperBound(istr_date_range_parm)

For li_counter = 1 to li_upper_bound
	// Build the string to reset text
	ls_Modify += "parm_" + String(li_counter) + "_t.Text = '' " + &
					"parm_" + String(li_counter) + "_t.Visible = 0 " + &
					"min_" + String(li_counter) + ".Visible = 0 " + &
					"max_" + String(li_counter) + ".Visible = 0 "

Next

This.Modify(ls_Modify)

This.SetTabOrder("destination_plant", 5)
This.Object.destination_plant.Tag = 'Double Click Destination Plant to Display List Box'


end event

event clicked;call super::clicked;If Left(is_BandAtPointer, 6) <> 'detail' Then return

Choose Case dwo.Name
Case	"mfg_step_descr"
	SetPointer(HourGlass!)
	This.SelectRow(0, False)
	This.SelectRow(row, True)
	This.SetRow(row)
	If This.GetItemString(Row, 'step_type') = 'T' Then
		iw_Frame.SetMicrohelp("Transfer Steps cannot have output products")
		return
	End if
	If IsValid(w_pas_output_products) Then
		close(w_pas_output_products)
	end if
	OpenWithParm(w_pas_output_products, iw_ThisWindow)
//	w_pas_output_products.PostEvent("ue_PostOpen")
	
End Choose
end event

event doubleclicked;call super::doubleclicked;String	ls_Protected

If dwo.Type <> "column" Then Return

ls_protected = dwo.Protect

If Not IsNumber(ls_protected) Then
	// then it is an expression, evaluate it
	// first char is default value, then tab, then expression
	// the whole thing is surrounded by quotes
	ls_protected = This.Describe('Evaluate("' + Mid(ls_protected, &
									Pos(ls_protected, '~t') + 1, &
									Len(ls_protected) - Pos(ls_protected, '~t') - 1) + &
									'", ' + String(Row) + ')')
End if

If dwo.Name = "destination_plant" and ls_protected = "0" Then
	OpenWithParm(w_pas_plant_dddw, is_dest_plants, Parent.GetParent())
End if
end event

event itemerror;call super::itemerror;Double				ld_data

String				ls_column_name

u_string_functions	lu_string


This.SelectText(1, Len(data))

ls_column_name = dwo.Name

Choose Case Left(dwo.Name, 3)
Case "des"
Case "min"
	If lu_string.nf_IsEmpty(data) Then
//		This.SetItem(row, ls_column_name, 0.00)
		iw_frame.SetMicroHelp("Ready")
		Return 3
	End If
	ld_data = Double(data)
	If ld_data < 0 Then
		iw_Frame.SetMicroHelp("Minimum Parameters cannot be negative")
	Else
		If ld_data > 100.00 Then
			iw_frame.SetMicroHelp("Minimum cannot be greater than 100.00")
		Else
			iw_Frame.SetMicroHelp(data + " is not a valid Minimum")
		End If
	End if
Case "max"
	If lu_string.nf_IsEmpty(data) Then
//		This.SetItem(row, ls_column_name, 100.00)
		iw_frame.SetMicroHelp("Ready")
		Return 3
	End If
	ld_data = Double(data)
	If ld_data < 0 Then
		iw_Frame.SetMicroHelp("Maximum Parameters cannot be negative")
	Else
		If ld_data > 100.00 Then
			iw_frame.SetMicroHelp("Maximum cannot be greater than 100.00")
		Else
			iw_Frame.SetMicroHelp(data + " is not a valid Maximum")
		End If
	End if
	
End Choose

Return 1
end event

event itemfocuschanged;call super::itemfocuschanged;Decimal	ld_value

String	ls_quantifier, &
			ls_column_name


ls_column_name = This.GetColumnName()
If ls_column_name <> "destination_plant" Then
	If IsValid(w_pas_plant_dddw) Then
		Close(w_pas_plant_dddw)
	End if
End if


// This is definitely a PowerBuilder 8.0.1.90???? hack.  This needs to be fixed.  REM.
	if  cbx_tab_code.checked then
		this.event ue_set_hspscroll()
	end if
//
end event

event scrollhorizontal;call super::scrollhorizontal;If IsValid(w_pas_plant_dddw) Then
	w_pas_plant_dddw.Move(w_pas_plant_dddw.wf_GetXPos(), w_pas_plant_dddw.wf_GetYPos())
End if
end event

event scrollvertical;call super::scrollvertical;If IsValid(w_pas_plant_dddw) Then
	w_pas_plant_dddw.Move(w_pas_plant_dddw.wf_GetXPos(), w_pas_plant_dddw.wf_GetYPos())
End if
end event

type tabpage_auto_conv from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2944
integer height = 888
long backcolor = 67108864
string text = "Auto Conversion Parameters"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_auto_conv dw_auto_conv
end type

on tabpage_auto_conv.create
this.dw_auto_conv=create dw_auto_conv
this.Control[]={this.dw_auto_conv}
end on

on tabpage_auto_conv.destroy
destroy(this.dw_auto_conv)
end on

type dw_auto_conv from u_base_dw_ext within tabpage_auto_conv
event ue_set_hspscroll ( )
event ue_next_week ( )
event ue_prev_week ( )
event ue_addrow ( )
event ue_duplicate_transfer ( )
event ue_next_two_weeks ( )
event ue_prev_two_weeks ( )
event ue_next_three_weeks ( )
event ue_prev_three_weeks ( )
integer y = 4
integer width = 2875
integer height = 788
integer taborder = 11
string dataobject = "d_pas_auto_conv_parms"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_set_hspscroll();Int	li_position, &
		li_position2


if this.hsplitscroll then
	If Integer(This.Object.DataWindow.HorizontalScrollMaximum) <= 1 Then return
	
	If This.Find("step_type = 'T'", 1, 10000) > 0 Then 
		li_position = Integer(This.Object.priority.X) + &
							Integer(This.Object.priority.Width) + 15
		li_position2 = li_position + 15
	Else
		li_position = Integer(This.Object.priority.X) + &
							Integer(This.Object.priority.Width) + 15
		li_position2 = Integer(This.Object.destination_plant.X) + &
							Integer(This.Object.destination_plant.Width) + 15
	End if
	
	This.Object.DataWindow.HorizontalScrollSplit = String(li_position)
	This.Object.DataWindow.HorizontalScrollPosition2 =  String(li_position2)
	This.Object.DataWindow.HorizontalScrollPosition =  '0'
end if
end event

event ue_next_week();This.SetRedraw(False)
wf_clear_values(This)
wf_Daily_auto_conv()
wf_fill_values_auto_conv()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_prev_week();This.SetRedraw(False)
wf_clear_values(This)
wf_Daily_auto_conv()
wf_fill_values_auto_conv()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_addrow();Date				ldt_begin, &
					ldt_end

Integer			li_ret, &
					li_temp

String			ls_temp, ls_pattern


// Make sure there is room for the date range
If ii_column_number_auto_conv >= UpperBound(istr_date_range_auto_conv) Then
	li_temp = UpperBound(istr_date_range_auto_conv)
	MessageBox("Date Range", "Maximum Number of Date Ranges already displayed.  " + &
					"Only " + String(li_temp) + " Date Ranges can be displayed.")
	return 
End if

OpenWithParm(w_pas_instance_date_range, iw_ThisWindow)
ls_temp = Message.StringParm

If iw_frame.iu_string.nf_isEmpty(ls_temp) Then return 

ldt_begin = Date(mid(ls_temp, 1, 10))
ldt_end = Date(mid(ls_temp, 12, 10))
ls_pattern = mid(ls_temp, 23, 1)

wf_add_date_range_auto_conv(ldt_begin, ldt_end)

tab_1.tabpage_auto_conv.dw_auto_conv.SetFocus()

If ii_column_number_auto_conv > 0 Then 
	li_ret = tab_1.tabpage_auto_conv.dw_auto_conv.SetColumn(5)
End if
If tab_1.tabpage_auto_conv.dw_auto_conv.RowCount() > 0 Then 
	li_ret = tab_1.tabpage_auto_conv.dw_auto_conv.SetRow(1)
End if


end event

event ue_duplicate_transfer();// This really duplicates a transfer step, not deletes a row
Long		ll_current_row

String	ls_data, &
			ls_temp


ll_current_row = This.GetRow()
If ll_current_row < 1 Then 
	iw_frame.SetMicroHelp("Cannot Duplicate Transfer Step; No Row is Current")
	return 
End if

If This.GetItemString(ll_current_row, "step_type") <> 'T' Then
	MessageBox("Duplicate Transfer Step", "Current row is not a transfer step.  " + &
					"Please position the cursor on a transfer step and try again.")
	return 
End if

ls_data = iw_ThisWindow.wf_duplicate_step(This, ll_current_row)
This.ImportString(ls_data)

This.Sort()

end event

event ue_next_two_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_two_week_auto_conv()
wf_fill_values_auto_conv()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_prev_two_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_two_week_auto_conv()
wf_fill_values_auto_conv()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_next_three_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_three_week_auto_conv()
wf_fill_values_auto_conv()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event ue_prev_three_weeks();This.SetRedraw(False)
wf_clear_values(This)
wf_three_week_auto_conv()
wf_fill_values_auto_conv()
This.Event ue_set_hspscroll()
This.SetRedraw(True)

end event

event itemchanged;call super::itemchanged;Double	ld_data, &
			ld_max, &
			ld_min, &
			ld_temp

Integer	li_column_number, &
			li_step_seq

Long		ll_Column, &
			ll_row, &
			ll_row_count

Date		ldt_begin_date, &
			ldt_end_date, &
			ldt_parm_date

String	ls_filter, &
			ls_parm_date, &
			ls_detail_errors, &
			ls_plant, &
			ls_find_string, &
			ls_data


ll_Column = Long(dwo.ID)

ld_data = Double(data)
li_column_number = Integer(mid(dwo.name, 5))
ls_parm_date = This.Describe('parm_' + String(li_column_number) + '_t.text')

li_step_seq = This.GetItemNumber(row, 'mfg_step_sequence')
ls_plant = This.GetItemString(row, 'destination_plant')

If Left(dwo.name, 3) = 'min' Then
			
	// Prevent entering of negative numbers
	If ld_data < 0 or ld_data > 100.00 Then
		// ItemError Event will take care of the message
		Return 1
	End if

	ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
	If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq)
	Else
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq) + &
						" And destination_plant = '" + ls_plant + "'"
	End If

	ll_row = tab_1.tabpage_inst.dw_instances.Find(ls_find_string, 1, ll_row_count)
	//IF the row on instances doesn't exist build one so they can put in a instance that doesn't
	// violate the minimum
	If ll_row = 0 then
		ll_row = row
		ls_data = iw_ThisWindow.wf_duplicate_step(This, ll_row)
		tab_1.tabpage_inst.dw_instances.ImportString(ls_data)
		ll_row = tab_1.tabpage_inst.dw_instances.RowCount()

		If Not iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
			tab_1.tabpage_inst.dw_instances.SetItem(ll_row, 'destination_plant', ls_plant)
		End If
	End If

	If IsNull(This.GetItemNumber(row, 'max_' + String(li_column_number))) Then 
		ld_max = 00.00
		This.SetItem(row, 'max_' + String(li_column_number), ld_max)
	Else
		ld_max = This.GetItemDecimal(row, 'max_' + String(li_column_number))
	End If
//	If dw_header.GetItemString(1, 'display_status') = 'L' Then
//		ldt_begin_date = date(Mid(ls_parm_date, 2, 10))
//		ls_parm_date = Right(ls_parm_date, 11)
//		ldt_end_date = Date(Left(ls_parm_date, len(ls_parm_date) - 1))
//		If Not wf_validate_instance_long(ldt_begin_date, ldt_end_date, ll_row, 'min', ld_data, ld_max) then
//			iw_frame.SetMicrohelp('Changing this Parameter violates an instance already ' + &
//					'entered.')
//			Return
//		End If
//	Else
//		ldt_parm_date = date(Mid(ls_parm_date, 2, 10))
//		If Not wf_validate_instance_daily(ldt_parm_date, ll_row, 'min', ld_data, ld_max) Then
//			iw_frame.SetMicroHelp('Changing this Parameter violates an instance already ' + &
//					'entered.')
//			Return
//		End IF
//	End IF
	
	ll_row = tab_1.tabpage_inst_parm.dw_parameters.Find(ls_find_string, 1, ll_row_count)
	
	If ld_data < tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(ll_row, 'min_' + String(li_column_number)) Then
		MessageBox('Min Value Error', 'Min value must be greater than or equal to Min Value on Instance Parameters tab')
		Return 1
		
	End If

	
End if


If Left(dwo.name, 3) = 'max' Then
	// Prevent entering of negative numbers
	If ld_data < 0 or ld_data > 100.00 Then
		// ItemError Event will take care of the message
		Return 1
	End if
	
	//Check to see that they aren't preventing themselves from entering 100% Instances
	
	If IsNull(This.GetItemNumber(row, 'max_' + String(li_column_number))) Then 
		ld_max = 00.00
	Else
		ld_max = This.GetItemDecimal(row, 'max_' + String(li_column_number)) 
	End IF
	If dw_header.GetItemString(1, "product_type") <> 'S' and &
			This.GetItemDecimal(1, 'sum_max_' + String(li_column_number)) - &
			ld_max + ld_data < 100.00 Then
		MessageBox('Parameter Violation', 'This will not allow you to enter instances' + &
				' totaling 100 percent.  Please enter a valid Parameter.')
		Return 1
	End If
	
	//Find the same row on instances.
	ll_row_count = tab_1.tabpage_inst.dw_instances.RowCount()
	If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq)
	Else
		ls_find_string = "mfg_step_sequence = " + String(li_step_seq) + &
						" And destination_plant = '" + ls_plant + "'"
	End If

	ll_row = tab_1.tabpage_inst.dw_instances.Find(ls_find_string, 1, ll_row_count)
	If ll_row > 0 then
		If IsNull(This.GetItemNumber(row, 'min_' + String(li_column_number))) Then 
			ld_min = 0.00
		Else
			ld_min = This.GetItemDecimal(row, 'min_' + String(li_column_number))
		End If

//		If dw_header.GetItemString(1, 'display_status') = 'L' Then
//			ldt_begin_date = date(Mid(ls_parm_date, 2, 10))
//			ls_parm_date = Right(ls_parm_date, 11)
//			ldt_end_date = Date(Left(ls_parm_date, len(ls_parm_date) - 1))
//			If Not wf_validate_instance_long(ldt_begin_date, ldt_end_date, ll_row, 'max', ld_min, ld_data) then
//				iw_frame.SetMicroHelp('Changing this Parameter violates an instance already ' + &
//						'entered.')
//				Return
//			End If
//		Else
//			ldt_parm_date = date(Mid(ls_parm_date, 2, 10))
//			If	Not wf_validate_instance_daily(ldt_parm_date, ll_row, 'max', ld_min, ld_data) Then
//				iw_frame.SetMicroHelp('Changing this Parameter violates an instance already ' + &
//						'entered.')
//				Return
//			End IF
//		End If
	End If
	
	ll_row = tab_1.tabpage_inst_parm.dw_parameters.Find(ls_find_string, 1, ll_row_count)
	
	If ld_data > tab_1.tabpage_inst_parm.dw_parameters.GetItemNumber(ll_row, 'max_' + String(li_column_number)) Then
		MessageBox('Max Value Error', 'Max value must be less than or equal to Max Value on Instance Parameters tab')
		Return 1
		
	End If

End if

If dwo.name = 'destination_plant' Then
	is_dest_plants =  String(Integer(data),"000")
	If Not IsNumber(data) Or is_dest_plants = '000' Then
		MessageBox("Destination Plant", "Destination Plant is invalid.")
		Return 1
	End if
	
	// make sure the plant isn't from the plant inquired on.
	If data = dw_header.GetItemString(1, 'plant_code') Then
		MessageBox("Destination Plant", "Destination Plant can't be the same as the plant " + &
				"who produced it.")
		Return 1
	End if
	
	// make sure that there are no duplicate plant/step combinations
	If Len(Trim(data)) = 0 Then return 
	If This.Find("destination_plant = '" + data + "'", 1, row - 1) > 0 Then
		MessageBox("Destination Plant", "Destination Plant must be unique for each step.")
		Return 1
	End if
	If row < This.RowCount() Then
		If This.Find("destination_plant = '" + data + "'", row + 1, This.RowCount()) > 0 Then
			MessageBox("Destination Plant", "Destination Plant must be unique for each step.")
			Return 1
		End if
	End if
	
	// This puts in the leading zeroes if they only entered one or two digits
	This.SetItem(row, "destination_plant",String(Integer(data),"000"))
	This.SetText(String(Integer(data),"000"))
	return 2
End if

iw_frame.SetMicroHelp("Ready")


end event

event itemerror;call super::itemerror;Double				ld_data

String				ls_column_name

u_string_functions	lu_string


This.SelectText(1, Len(data))

ls_column_name = dwo.Name

Choose Case Left(dwo.Name, 3)
Case "des"
Case "min"
	If lu_string.nf_IsEmpty(data) Then
//		This.SetItem(row, ls_column_name, 0.00)
		iw_frame.SetMicroHelp("Ready")
		Return 3
	End If
	ld_data = Double(data)
	If ld_data < 0 Then
		iw_Frame.SetMicroHelp("Minimum Parameters cannot be negative")
	Else
		If ld_data > 100.00 Then
			iw_frame.SetMicroHelp("Minimum cannot be greater than 100.00")
		Else
			iw_Frame.SetMicroHelp(data + " is not a valid Minimum")
		End If
	End if
Case "max"
	If lu_string.nf_IsEmpty(data) Then
//		This.SetItem(row, ls_column_name, 100.00)
		iw_frame.SetMicroHelp("Ready")
		Return 3
	End If
	ld_data = Double(data)
	If ld_data < 0 Then
		iw_Frame.SetMicroHelp("Maximum Parameters cannot be negative")
	Else
		If ld_data > 100.00 Then
			iw_frame.SetMicroHelp("Maximum cannot be greater than 100.00")
		Else
			iw_Frame.SetMicroHelp(data + " is not a valid Maximum")
		End If
	End if
	
End Choose

Return 1
end event

type cbx_tab_code from checkbox within w_pas_instances
integer x = 197
integer y = 284
integer width = 622
integer height = 84
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Help Reverse Tabbing"
end type

event clicked;// This is definitely a PowerBuilder 8.0.1.90???? hack.  This needs to be fixed.  REM.
	integer	li_Checked
	if cbx_tab_code.checked then
		li_Checked = 1
	else
		li_Checked = 0
	end if
	
	SetProfileString(gw_base_frame.is_userini, "wincoordinates", "ib_tab_code",  string(li_Checked))
//
end event

type cb_copy_button from u_base_commandbutton_ext within w_pas_instances
integer x = 2473
integer y = 300
integer width = 599
integer height = 80
integer taborder = 38
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
string facename = "MS Sans Serif"
string text = ""
end type

event clicked;call super::clicked;Parent.wf_copy_parm()
end event

