﻿$PBExportHeader$u_mcool_code_dropdown.sru
$PBExportComments$User Object for Product State
forward
global type u_mcool_code_dropdown from datawindow
end type
end forward

global type u_mcool_code_dropdown from datawindow
integer width = 1294
integer height = 84
integer taborder = 10
string dataobject = "d_mcool_code_dropdown"
boolean border = false
boolean livescroll = true
end type
global u_mcool_code_dropdown u_mcool_code_dropdown

forward prototypes
public function long uf_enable (boolean ab_enable)
public function string uf_get_mcool_code ()
public function string uf_get_mcool_code_desc ()
public subroutine uf_mcool_code (string as_mcool_code)
end prototypes

public function long uf_enable (boolean ab_enable);string	ls_temp

If ab_enable Then
// The following line gave us a wierd error for some unknow reason please do not use it
//	This.object.product_state.Background.Color = '16777215'
	this.setitem(1,"enable_mcool","T")

//	This.object.product_state.Protect = 0
Else
// The following line gave us a wierd error for some unknow reason please do not use it
//	This.object.product_state.Background.Color = '12632256'
	this.setitem(1,"enable_mcool","F")
//	This.object.product_state.Protect = '1'
End If

Return 1
end function

public function string uf_get_mcool_code ();
return This.GetItemString(1, "mcool_code")

end function

public function string uf_get_mcool_code_desc ();Datawindowchild			ldwc_temp

String						ls_mcool_code

Long							ll_findrow

u_string_functions		lu_string


ls_mcool_code = This.GetItemString(1, 'mcode_code')

If lu_string.nf_IsEmpty(ls_mcool_code) Then Return ''

This.GetChild('ls_mcool_code', ldwc_temp)
ll_findrow = ldwc_temp.Find('type_code = "' + ls_mcool_code + '"', 1, ldwc_temp.RowCount())
If ll_findrow <= 0 Then Return ''

Return ldwc_temp.GetItemString(ll_findrow, 'type_desc')


end function

public subroutine uf_mcool_code (string as_mcool_code);
This.SetItem(1,"mcode_code",as_mcool_code)

end subroutine

event constructor;DataWindowChild		ldwc_type

String	ls_mcool_code, &
			ls_find_string
			
Long		ll_row			
			
If This.RowCount() = 0 Then This.InsertRow(0)			

This.GetChild("mcool_code", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("MCOOLCDE")
ldwc_type.SetSort("type_code")
ldwc_type.Sort()

ls_mcool_code = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastMCOOLcode", "  ")

This.SetItem(1, 'mcool_code', ls_mcool_code)
This.TriggerEvent(ItemChanged!)

ls_find_string = 'type_code = "' + ls_mcool_code + '"'

ll_row = ldwc_type.Find(ls_find_string, 1, ldwc_type.RowCount())

If ll_row > 0 Then
	This.SetItem(1, 'mcool_desc', ldwc_type.GetItemString(ll_row, 'type_desc'))
End If



end event

on u_mcool_code_dropdown.create
end on

on u_mcool_code_dropdown.destroy
end on

event itemchanged;DataWindowChild		ldwc_type

Long	ll_row, ll_RowCount	

String	ls_find_string

This.GetChild("mcool_code", ldwc_type)

ls_find_string = 'type_code = "' + Trim(data) + '"'

ll_RowCount = ldwc_type.RowCount()

ll_row = ldwc_type.Find(ls_find_string, 1, ldwc_type.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(data + " is an Invalid MCOOL Code")
	This.SetFocus()
	This.SelectText(1, Len(data))
	return 1
Else
	This.SetItem(1, 'mcool_desc', ldwc_type.GetItemString(ll_row, 'type_desc'))
	//This.SetFocus()
	//This.SelectText(1, Len(data))
	SetProfileString( iw_frame.is_UserINI, "Pas", "LastMCOOLcode",Trim(data))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

return 
end event

event getfocus;
If This.GetColumnName() = "mcool_code" Then
	This.SelectText(1, Len(This.GetText()))
End if
end event

