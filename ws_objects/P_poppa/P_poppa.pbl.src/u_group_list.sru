﻿$PBExportHeader$u_group_list.sru
$PBExportComments$User object for optional division/product group inq
forward
global type u_group_list from userobject
end type
type dw_group_detail from u_netwise_dw within u_group_list
end type
type st_label from statictext within u_group_list
end type
type dw_group_header from u_netwise_dw within u_group_list
end type
end forward

global type u_group_list from userobject
integer width = 1307
integer height = 572
long backcolor = 79741120
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
event ue_post_constructor ( )
event ue_group_changed ( )
event ue_find_group ( )
dw_group_detail dw_group_detail
st_label st_label
dw_group_header dw_group_header
end type
global u_group_list u_group_list

type variables
u_ws_sma	iu_ws_sma
s_error		istr_error_info
String			is_group_type, is_char
end variables

forward prototypes
public function integer uf_get_sel_id (ref string as_string)
public function integer uf_get_sel_desc (ref string as_string)
public function string uf_get_owner ()
public subroutine uf_load_groups (string as_group_type)
public subroutine uf_filldddw (ref datawindowchild ldw_childdw)
public function string uf_get_desc_only ()
end prototypes

event ue_post_constructor();//ole_groups.object.GroupType(3)
//ole_Groups.object.LoadObject()


end event

event ue_group_changed();Parent.PostEvent("ue_group_changed")
end event

event ue_find_group();String		ls_find_string

Long		ll_pos

ls_find_string = "mid(group_desc,1,1) = " + "'" + is_char + "'"

ll_pos = dw_group_detail.Find(ls_find_string, 1, dw_group_detail.RowCount())

if ll_pos > 0 then
	dw_group_detail.SelectRow(0, False)
	dw_group_detail.SelectRow(ll_pos, True)
	dw_group_detail.ScrollToRow(ll_pos)
end if

end event

public function integer uf_get_sel_id (ref string as_string);//ibdkdld
//Return uo_prod_group.uf_get_sel_id(as_string)
//as_string = string(ole_groups.object.groupID())
integer li_id, li_row

li_row = dw_group_detail.getrow()

IF li_row > 0 then
	as_string =string(dw_group_detail.getitemnumber(li_row, "group_id"))
	return integer(as_string)
else 
	Return 0
END IF	
//as_string = li_id





//Return 0

end function

public function integer uf_get_sel_desc (ref string as_string);// ibdkdld
//Return uo_prod_group.uf_get_sel_desc(as_string)

//as_string = ole_groups.object.Groupdescription()
integer li_row
string ls_id, ls_desc
li_row = dw_group_detail.getrow()

IF li_row >0 then
	ls_id = string(dw_group_detail.getitemnumber(li_row, "group_id"))
	ls_desc = dw_group_detail.getitemstring(li_row, "group_desc")

	as_string = ls_id +' ' + ls_desc

	if as_string <> "" then
		return 0
	end if
Else
	return 0
END IF 

//return 0


end function

public function string uf_get_owner ();// ibdkdld
//return uo_prod_group.uf_get_owner()
//return ole_groups.object.systemname()
string owner
owner = dw_group_header.getitemstring(1, "group_owner")

Return owner
end function

public subroutine uf_load_groups (string as_group_type);
/* Return group list for the requested group type
'P' - Product
'L' - Location
'C' - Customer
*/
integer li_ret, li_rtn
string ls_group_type, ls_output_string, ls_owner, ls_grp_owner, ls_group, ls_last_group_owner, ls_last_group_id, ls_find_string
long ll_found, ll_sub

istr_error_info.se_event_name = "uf_load_groups of u_group_list"

ls_group_type = as_group_type
is_group_type = as_group_type

ls_output_string = Space(20000)

If Not Isvalid(iu_ws_sma) then
	iu_ws_sma = Create u_ws_sma
end if

li_ret = iu_ws_sma.nf_smas20er(ls_group_type, istr_error_info, ls_output_string)

dw_group_detail.Setredraw(False)
dw_group_detail.Reset()
li_rtn = dw_group_detail.ImportString(ls_output_string)
dw_group_detail.ResetUpdate()
dw_group_detail.SetFocus()

//dw_group_detail.SetSort("string(group_id) asc")
dw_group_detail.SetSort("string(group_desc) asc")
dw_group_detail.Sort()

If as_group_type = 'P' Then
	st_label.Text = 'Product Groups:'
	ls_last_group_owner = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastProductGroupOwner", "  ")	
	ls_last_group_id = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastProductGroupID", "  ")
Else 
	IF as_group_type = 'L' Then
		st_label.Text = 'Location Groups:'
		ls_last_group_owner = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastLocationGroupOwner", "  ")
		ls_last_group_id = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastLocationGroupID", "  ")
	Else
		st_label.Text = 'Customer Groups:'
		ls_last_group_owner = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastCustomerGroupOwner", "  ")
		ls_last_group_id = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastCustomerGroupID", "  ")
	End If
End If

If ls_last_group_owner> ' ' Then
	dw_group_header.SetItem(1, "group_owner", ls_last_group_owner) 
Else
	if isnull(dw_group_header.GetItemString(1, "group_owner")) then 
		dw_group_header.SetItem(1, "group_owner", ls_group) 
	end if
End If

//ls_group = dw_group_detail.GetItemString(1, "group_owner")
////if ls_group_type = 'P' then
//	if isnull(dw_group_header.GetItemString(1, "group_owner")) then 
//		dw_group_header.SetItem(1, "group_owner", ls_group) 
//	end if
////end if

//if ls_group_type = 'L' then
//	if isnull(dw_group_header.GetItemString(1, "group_owner")) then 
//		dw_group_header.SetItem(1, "group_owner", "BILL") 
//	end if
//end if

ls_owner = dw_group_header.getitemstring(1, "group_owner")
dw_group_detail.SetFilter("group_owner = '"+ ls_owner +" ' ")
dw_group_detail.Filter()
dw_group_detail.Sort()

If ls_last_group_id > ' ' Then
	ls_find_string = "group_id = " + ls_last_group_id 
	ll_found = dw_group_detail.Find(ls_find_string,1,dw_group_detail.RowCount())
	If ll_found > 0 Then
		dw_group_detail.SelectRow(ll_found, True)
		dw_group_detail.ScrollToRow(ll_found)
	Else
		dw_group_detail.SelectRow(1, True)
		dw_group_detail.ScrollToRow(ll_found)
	End If
Else
	dw_group_detail.SelectRow(1, True)
	dw_group_detail.ScrollToRow(ll_found)
End if

dw_group_detail.SetRedraw(True)

end subroutine

public subroutine uf_filldddw (ref datawindowchild ldw_childdw);IF ldw_childdw.RowCount()	> 0 THEN RETURN
ldw_childdw.SetTransObject(SQLCA)
ldw_childdw.Retrieve()
end subroutine

public function string uf_get_desc_only ();integer li_row
string ls_id, ls_desc
li_row = dw_group_detail.getrow()

IF li_row >0 then
	Return dw_group_detail.getitemstring(li_row, "group_desc")
ELSE
	return ''
END IF 


end function

on u_group_list.create
this.dw_group_detail=create dw_group_detail
this.st_label=create st_label
this.dw_group_header=create dw_group_header
this.Control[]={this.dw_group_detail,&
this.st_label,&
this.dw_group_header}
end on

on u_group_list.destroy
destroy(this.dw_group_detail)
destroy(this.st_label)
destroy(this.dw_group_header)
end on

event constructor;PostEvent('ue_post_constructor')
end event

event destructor;String	ls_group_id, ls_owner

ls_owner = uf_get_owner()
ls_group_id = String(uf_get_sel_id(ls_group_id))

If is_group_type = 'P' Then
	SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastProductGroupOwner", ls_owner)
	SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastProductGroupID", ls_group_id)
Else
	If is_group_type = 'L' Then
		SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastLocationGroupOwner", ls_owner)
		SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastLocationGroupID", ls_group_id)
	Else
		SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastCustomerGroupOwner", ls_owner)
		SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastCustomerGroupID", ls_group_id)
	End If
End If
end event

type dw_group_detail from u_netwise_dw within u_group_list
integer y = 148
integer width = 1289
integer height = 400
integer taborder = 20
string dataobject = "d_group_detail"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;call super::clicked;If row > 0 then
	This.SelectRow(0, False)
	This.SelectRow(row, True)
	Parent.TriggerEvent("ue_group_changed")
End If
end event

event constructor;call super::constructor;This.InsertRow(0)
end event

event destructor;call super::destructor;//If as_group_type = 'P' Then
//	SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastProductGroupOwner", uf_get_owner())
//	SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastProductGroupID", uf_get_sel_id(""))
//Else 
//	IF as_group_type = 'L' Then
//		SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastLocationGroupOwner", uf_get_owner())
//		SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastLocationGroupID", uf_get_sel_id(""))
//	Else
//		SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastCustomerGroupOwner", uf_get_owner())
//		SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "LastCustomerGroupID", uf_get_sel_id(""))
//	End If
//End If
end event

event ue_keydown;call super::ue_keydown;
Choose Case key
	Case KeyA!
		is_char = 'A'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyB!
		is_char = 'B'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyC!		
		is_char = 'C'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyD!
		is_char = 'D'
		Parent.TriggerEvent("ue_find_group")		
	Case KeyE!
		is_char = 'E'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyF!
		is_char = 'F'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyG!		
		is_char = 'G'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyH!
		is_char = 'H'
		Parent.TriggerEvent("ue_find_group")	
	Case KeyI!
		is_char = 'I'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyJ!
		is_char = 'J'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyK!		
		is_char = 'K'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyL!
		is_char = 'L'
		Parent.TriggerEvent("ue_find_group")		
	Case KeyM!
		is_char = 'M'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyN!
		is_char = 'N'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyO!		
		is_char = 'O'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyP!
		is_char = 'P'
		Parent.TriggerEvent("ue_find_group")			
	 Case KeyQ!		
		is_char = 'Q'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyR!
		is_char = 'R'
		Parent.TriggerEvent("ue_find_group")	
	Case KeyS!
		is_char = 'S'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyT!
		is_char = 'T'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyU!		
		is_char = 'U'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyV!
		is_char = 'V'
		Parent.TriggerEvent("ue_find_group")		
	Case KeyW!
		is_char = 'W'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyX!
		is_char = 'X'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyY!		
		is_char = 'Y'
		Parent.TriggerEvent("ue_find_group")	
	 Case KeyZ!
		is_char = 'Z'
		Parent.TriggerEvent("ue_find_group")		
	 Case Key0!		
		is_char = '0'
		Parent.TriggerEvent("ue_find_group")	
	 Case Key1!
		is_char = '1'
		Parent.TriggerEvent("ue_find_group")	
	Case Key2!
		is_char = '2'
		Parent.TriggerEvent("ue_find_group")	
	 Case Key3!
		is_char = '3'
		Parent.TriggerEvent("ue_find_group")	
	 Case Key4!		
		is_char = '4'
		Parent.TriggerEvent("ue_find_group")	
	 Case Key5!
		is_char = '5'
		Parent.TriggerEvent("ue_find_group")		
	Case Key6!
		is_char = '6'
		Parent.TriggerEvent("ue_find_group")	
	 Case Key7!
		is_char = '7'
		Parent.TriggerEvent("ue_find_group")	
	 Case Key8!		
		If Keydown(KeyShift!) then
			is_char = '*'
		else
			is_char = '8'
		end if
		Parent.TriggerEvent("ue_find_group")	
	 Case Key9!
		If Keydown(KeyShift!) then
			is_char = '('
		else
			is_char = '9'
		end if
		Parent.TriggerEvent("ue_find_group")					
End Choose
	
end event

type st_label from statictext within u_group_list
integer x = 69
integer y = 44
integer width = 448
integer height = 92
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 16777215
boolean focusrectangle = false
end type

type dw_group_header from u_netwise_dw within u_group_list
integer width = 1234
integer height = 140
integer taborder = 10
string dataobject = "d_group_header"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;IF This.RowCount() = 0 Then This.InsertRow(0)

datawindowchild ldwc_Temp

this.getchild('group_owner', ldwc_Temp)
if ldwc_Temp.rowCount() <= 1 then
	uf_filldddw(ldwc_Temp)
end if
end event

event itemchanged;call super::itemchanged;string ls_owner
dw_group_header.AcceptText() 
ls_owner = dw_group_header.getitemstring(1, "group_owner")
dw_group_detail.SetFilter("group_owner = '"+ ls_owner +" ' ")
dw_group_detail.Filter()
dw_group_detail.Sort()
end event

