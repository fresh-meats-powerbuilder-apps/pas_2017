﻿$PBExportHeader$w_loadingtables.srw
forward
global type w_loadingtables from window
end type
type st_1 from statictext within w_loadingtables
end type
type p_1 from picture within w_loadingtables
end type
end forward

global type w_loadingtables from window
integer x = 603
integer y = 344
integer width = 1550
integer height = 584
windowtype windowtype = child!
long backcolor = 12632256
event type integer ue_status ( string as_text )
st_1 st_1
p_1 p_1
end type
global w_loadingtables w_loadingtables

type variables
integer ii_rectangle_width = 51
end variables

event ue_status;st_1.text	=	as_text
RETURN 1
end event

on timer;//int li_rec_width
//
//IF table_name.Text <> message.istr_loadtable_data.table_name THEN
//   table_name.Text      = message.istr_loadtable_data.table_name
//   ii_rectangle_width = 0
//END IF
//li_rec_width = Message.nf_get_rec_width()
//IF li_rec_width > ii_rectangle_width THEN
//   ii_rectangle_width = li_rec_width
//ELSE
//	ii_rectangle_width = ii_rectangle_width +10
//END IF
//r_1.Resize(ii_rectangle_width,45)
//If st_1.visible Then
//	st_1.visible = FALSE
//Else
//	st_1.visible = TRUE
//End If
//
end on

event open;//r_1.Resize(0,45)
//Timer(1)

end event

on w_loadingtables.create
this.st_1=create st_1
this.p_1=create p_1
this.Control[]={this.st_1,&
this.p_1}
end on

on w_loadingtables.destroy
destroy(this.st_1)
destroy(this.p_1)
end on

type st_1 from statictext within w_loadingtables
integer x = 229
integer y = 236
integer width = 1125
integer height = 88
integer textsize = -12
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Loading Tables......Please Wait"
boolean focusrectangle = false
end type

type p_1 from picture within w_loadingtables
integer x = 5
integer width = 1536
integer height = 576
boolean originalsize = true
string picturename = "C:\IBP\PB17\PAS_2017\SPLASH.BMP"
boolean focusrectangle = false
end type

