﻿$PBExportHeader$w_netwise_response.srw
forward
global type w_netwise_response from w_base_response
end type
end forward

global type w_netwise_response from w_base_response
integer x = 1075
integer y = 485
integer height = 1493
end type
global w_netwise_response w_netwise_response

forward prototypes
public function integer setmicrohelp (string as_microhelp)
end prototypes

public function integer setmicrohelp (string as_microhelp);return gw_netwise_frame.SetMicroHelp(as_microhelp)

end function

on w_netwise_response.create
call super::create
end on

on w_netwise_response.destroy
call super::destroy
end on

event open;call super::open;Long	frameX, &
		frameY, &
		responseX, &
		responseY, &
		parent_windowX, &
		parent_windowY
		
responseX = This.X
responseY = This.Y

frameX = gw_netwise_frame.X
frameY = gw_netwise_frame.Y

window	lw_parentwindow

If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		lw_parentwindow = message.powerobjectparm
		parent_windowX = lw_parentwindow.X
		parent_windowY = lw_parentwindow.Y
	Else	
		parent_windowX = 0
		parent_windowY = 0
	End If
Else
	parent_windowX = 0
	parent_windowY = 0
End If

If responseX < frameX Then
	This.Move (frameX + parent_windowX + 400, frameY + parent_windowY + 600)
End If




end event

type cb_base_help from w_base_response`cb_base_help within w_netwise_response
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_netwise_response
end type

type cb_base_ok from w_base_response`cb_base_ok within w_netwise_response
end type

