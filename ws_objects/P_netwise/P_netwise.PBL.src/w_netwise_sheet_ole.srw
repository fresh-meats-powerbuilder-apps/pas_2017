﻿$PBExportHeader$w_netwise_sheet_ole.srw
forward
global type w_netwise_sheet_ole from w_netwise_sheet
end type
end forward

global type w_netwise_sheet_ole from w_netwise_sheet
event ue_pooled ( )
end type
global w_netwise_sheet_ole w_netwise_sheet_ole

type variables
Public:
	boolean 	ib_pooled
	
	int		ii_ID
	time		it_Instance_date
	
Private:
	boolean	ib_ExitPool
	
protected:
	boolean 	ib_poolable
	string	is_window_title
end variables

forward prototypes
public function boolean wf_query_save_changes ()
public subroutine wf_exit_pool ()
public function boolean wf_is_poolable ()
protected function boolean wf_pooled_window (window w_window)
end prototypes

public function boolean wf_query_save_changes ();RETURN TRUE
end function

public subroutine wf_exit_pool ();ib_ExitPool = true
Close(this)
end subroutine

public function boolean wf_is_poolable ();return ib_poolable
end function

protected function boolean wf_pooled_window (window w_window);ClassDefinition 		lu_ClassDef
w_netwise_sheet_ole 	w_ole_sheet

if not IsValid(w_window) then
	return true	
end if

lu_ClassDef = w_window.classdefinition
do while IsValid(lu_ClassDef)
	if lu_ClassDef.name = "w_netwise_sheet_ole" then
		w_ole_sheet = w_window
		if w_ole_sheet.ib_pooled then
			return true	
		end if
		
		return false
	end if
	
	lu_ClassDef = lu_ClassDef.Ancestor		
loop

return false
end function

event closequery;call super::closequery;integer 	intPromptOnSave
Window	lw_NextSheet

gw_base_frame.iu_base_data.Trigger Event ue_get_sheetsettings(intPromptOnSave)

If NOT ib_pooled &
	AND NOT (gw_base_frame.ib_exit and intPromptOnSave = 0) &
	AND (gw_base_frame.im_base_menu.m_file.m_save.enabled) THEN
	IF Not wf_query_save_changes() THEN 
		RETURN 1
	END IF
END IF

IF NOT ib_ExitPool and ib_poolable THEN
	wf_save_win_coord ( )
	this.windowstate 	= Minimized!
	this.y 				= -500
	ib_pooled 			= true
	
	this.title 			= "(Closed) " + is_window_title 
	this.TriggerEvent("ue_pooled")

	lw_NextSheet = gw_base_frame.GetFirstSheet()

	DO While IsValid( lw_NextSheet)
		lw_NextSheet = gw_base_frame.GetNextSHeet( lw_NextSheet)
	
		IF IsValid( lw_NextSheet) THEN
			IF NOT wf_pooled_window(lw_NextSheet) THEN
				exit
			END IF
		END IF
	LOOP
	
	IF IsValid (lw_NextSheet) Then
		lw_NextSheet.SetFocus()
	end if
	
	this.hide()
	this.visible = false
	RETURN 1
END IF

RETURN 0
end event

on w_netwise_sheet_ole.create
call super::create
end on

on w_netwise_sheet_ole.destroy
call super::destroy
end on

event close;call super::close;gw_netwise_frame.iu_window_pool.uf_remove_pooled_window(ii_ID)
end event

event activate;call super::activate;//this.ib_pooled = false

end event

event open;call super::open;u_string_functions	lu_string

if ib_poolable then
	if lu_string.nf_IsEmpty(is_window_title) then
		is_window_title = this.title 
	end if
	
	this.title = is_window_title
end if

ib_ExitPool 		= false
ib_pooled 			= false
it_Instance_date 	= now()

if ii_ID = 0 and ib_poolable then
	gw_netwise_frame.iu_window_pool.uf_add_pooled_window(this)
end if
end event

event other;call super::other;if ib_pooled then
	if this.y > -1 then
		this.windowstate 	= Minimized!
		this.y 				= -500
	end if
end if
end event

