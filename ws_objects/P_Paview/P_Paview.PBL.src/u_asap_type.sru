﻿$PBExportHeader$u_asap_type.sru
forward
global type u_asap_type from datawindow
end type
end forward

global type u_asap_type from datawindow
integer width = 914
integer height = 100
integer taborder = 10
string dataobject = "d_asap_type"
boolean border = false
boolean livescroll = true
end type
global u_asap_type u_asap_type

forward prototypes
public function long uf_enable (boolean ab_enable)
public subroutine uf_set_asap_type (string as_type)
public function string uf_get_asap_type_desc ()
public function string uf_get_asap_type ()
end prototypes

public function long uf_enable (boolean ab_enable);If ab_enable Then
	This.object.country.Background.Color = 16777215
	This.object.country.Protect = 0
Else
	This.object.country.Background.Color = 12632256
	This.object.country.Protect = 1
End If

Return 1
end function

public subroutine uf_set_asap_type (string as_type);This.SetItem(1,"asap_type",as_type)
end subroutine

public function string uf_get_asap_type_desc ();Datawindowchild			ldwc_temp

String						ls_asap_type

Long							ll_findrow

u_string_functions		lu_string


ls_asap_type = This.GetItemString(1, 'asap_type')

If lu_string.nf_IsEmpty(ls_asap_type) Then Return ''

This.GetChild('asap_type', ldwc_temp)
ll_findrow = ldwc_temp.Find('type_short_desc = "' + ls_asap_type + '"', 1, ldwc_temp.RowCount())
If ll_findrow <= 0 Then Return ''

Return ldwc_temp.GetItemString(ll_findrow, 'type_desc')

end function

public function string uf_get_asap_type ();return This.GetItemString(1, "asap_type")
end function

event constructor;DataWindowChild		ldwc_type


This.GetChild("asap_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PAASAPTY")
This.InsertRow(0)
end event

on u_asap_type.create
end on

on u_asap_type.destroy
end on

