﻿$PBExportHeader$w_auto_conversion_message.srw
forward
global type w_auto_conversion_message from w_base_response_ext
end type
type mle_message from u_base_multilineedit_ext within w_auto_conversion_message
end type
end forward

global type w_auto_conversion_message from w_base_response_ext
int Width=1637
int Height=1073
boolean TitleBar=true
string Title="Auto Conversion Message"
long BackColor=12632256
mle_message mle_message
end type
global w_auto_conversion_message w_auto_conversion_message

on open;call w_base_response_ext::open;mle_message.Text = Message.StringParm
end on

on w_auto_conversion_message.create
int iCurrent
call w_base_response_ext::create
this.mle_message=create mle_message
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=mle_message
end on

on w_auto_conversion_message.destroy
call w_base_response_ext::destroy
destroy(this.mle_message)
end on

event ue_base_ok;call super::ue_base_ok;Close( This )
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_auto_conversion_message
int TabOrder=20
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_auto_conversion_message
int TabOrder=40
boolean Visible=false
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_auto_conversion_message
int X=622
int Y=833
int TabOrder=30
end type

type mle_message from u_base_multilineedit_ext within w_auto_conversion_message
int X=37
int Y=25
int Width=1550
int Height=781
int TabOrder=10
boolean HScrollBar=true
boolean VScrollBar=true
boolean DisplayOnly=true
long BackColor=12632256
int TextSize=-9
end type

