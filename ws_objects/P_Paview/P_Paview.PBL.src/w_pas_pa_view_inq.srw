﻿$PBExportHeader$w_pas_pa_view_inq.srw
forward
global type w_pas_pa_view_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_pas_pa_view_inq
end type
type st_1 from statictext within w_pas_pa_view_inq
end type
type uo_begin_date from u_dddw_editmask within w_pas_pa_view_inq
end type
type cbx_ship_off_the_cut from checkbox within w_pas_pa_view_inq
end type
type cbx_ranges from checkbox within w_pas_pa_view_inq
end type
type dw_fab from u_fab_product_code within w_pas_pa_view_inq
end type
type cbx_epa from checkbox within w_pas_pa_view_inq
end type
type cbx_pt from checkbox within w_pas_pa_view_inq
end type
type cbx_1_week from checkbox within w_pas_pa_view_inq
end type
type cbx_2_weeks from checkbox within w_pas_pa_view_inq
end type
type gb_1 from groupbox within w_pas_pa_view_inq
end type
type gb_2 from groupbox within w_pas_pa_view_inq
end type
end forward

global type w_pas_pa_view_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 2016
integer height = 896
string title = "PA View Inquire"
long backcolor = 67108864
event begindatechanged ( )
dw_plant dw_plant
st_1 st_1
uo_begin_date uo_begin_date
cbx_ship_off_the_cut cbx_ship_off_the_cut
cbx_ranges cbx_ranges
dw_fab dw_fab
cbx_epa cbx_epa
cbx_pt cbx_pt
cbx_1_week cbx_1_week
cbx_2_weeks cbx_2_weeks
gb_1 gb_1
gb_2 gb_2
end type
global w_pas_pa_view_inq w_pas_pa_view_inq

type variables
Boolean		ib_valid_return
w_base_sheet	iw_parent

string	is_begin_date
end variables

event begindatechanged();If is_begin_date > "" Then uo_begin_Date.nf_SetText(is_begin_date)
////This.setitem(1,"uo_begin_date",is_begin_date)
//uo_begin_Date.nf_SetText(ls_temp)
end event

event ue_postopen;call super::ue_postopen;Date							ldt_today

Int							li_pos, &
								li_ret

String						ls_header, &
								ls_temp, &
								ls_product_code, &
								ls_product_descr, &
								ls_state, &
								ls_status, &
								ls_epa_pt_info, &
								ls_number_of_weeks

u_string_functions		lu_string


ldt_today = Today()
// jxr - 8/15/2018 added 2 weeks to drop down list
ls_temp = String(ldt_today) + '~r~n' + &
				String(RelativeDate(ldt_today, 7)) + '~r~n' + &
				String(RelativeDate(ldt_today, 14)) + '~r~n' + &
				String(RelativeDate(ldt_today, 21)) + '~r~n' + &
				String(RelativeDate(ldt_today, 28)) + '~r~n' + &
				String(RelativeDate(ldt_today, 35)) + '~r~n'

li_ret = uo_begin_date.ImportString(ls_temp)

iw_parent.Event ue_Get_Data('Begin_Date')
ls_temp = Message.StringParm
uo_begin_Date.nf_SetText(ls_temp)

iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If

iw_parent.Event ue_Get_Data('Product')
ls_product_code = Message.StringParm
iw_parent.Event ue_Get_Data('Product_Descr')
ls_product_descr = Message.StringParm
// State Changes ibdkdld 09/20/02
iw_parent.Event ue_Get_Data('State')
ls_state = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_state) Then dw_fab.uf_set_product_state(ls_state)

iw_parent.Event ue_Get_Data('Status')
ls_status = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_status) Then dw_fab.uf_set_product_status(ls_status)
//dw_product.uf_SetProductCode(ls_product_code, ls_product_descr)
dw_fab.uf_set_product_code( ls_product_code, ls_product_descr)

iw_parent.Event ue_Get_Data('Ship_Off_The_Cut')
ls_temp = Message.StringParm
IF ls_temp = 'Y' Then
	cbx_ship_off_the_cut.Checked = True
Else
	cbx_ship_off_the_cut.Checked = False
End If
	
iw_parent.Event ue_Get_Data('Ranges')
ls_temp = Message.StringParm
IF ls_temp = 'Y' Then
	cbx_ranges.Checked = True
Else
	cbx_ranges.Checked = False
End If

// kelly added epa-pt  check only one or none.
iw_parent.Event ue_Get_Data('epa_view')
ls_epa_pt_info = Message.StringParm
IF ls_epa_pt_info = 'E' Then
	cbx_epa.Checked = True
Else
	cbx_epa.Checked = False
End If
// pjm 09/09/2014 correction for epa and pt options
iw_parent.Event ue_Get_Data('pt_view2')
ls_epa_pt_info = Message.StringParm
IF ls_epa_pt_info = 'P' Then
	cbx_pt.Checked = True
	Else
	cbx_pt.Checked = False
End If

iw_parent.Event ue_Get_Data('number_of_weeks')
ls_number_of_weeks = Message.StringParm
If ls_number_of_weeks = '1' Then
	cbx_1_week.Checked = True
	cbx_2_weeks.Checked = False
Else
	cbx_1_week.Checked = False
	cbx_2_weeks.Checked = True	
End If

//ls_header = iw_parent.wf_GetHeaderInfo()
//If Left(ls_header, 1) = '~t' Then
//	return
//End if

//li_pos = lu_string.nf_npos(ls_header, '~t', 1, 2)
//ls_temp = Left(ls_header, li_pos - 1)
//ls_header = Right(ls_header, Len(ls_header) - li_pos)
//
//dw_plant.Reset()
//dw_plant.ImportString(ls_temp)
//
//li_pos = lu_string.nf_npos(ls_header, '~t', 1, 2)
//ls_temp = Left(ls_header, li_pos - 1)
//ls_header = Right(ls_header, Len(ls_header) - li_pos)
//
//dw_product.Reset()
//dw_product.ImportString(ls_temp)
//
//uo_begin_date.nf_SetText(Left(ls_header, Pos(ls_header, '~t', 1) - 1))

If dw_plant.RowCount() < 1 Then 
	dw_plant.InsertRow(0)
ElseIf Not lu_string.nf_IsEmpty(dw_plant.GetItemString(1,1)) Then
	// State Changes ibdkdld 09/20/02
	//dw_product.SetFocus()
	dw_fab.setfocus( )
End if


end event

event open;call super::open;iw_parent = Message.PowerObjectParm



Window      lw_parent


If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

//Changed this window code to keep it centered JCROM JULY '12
lw_parent = This.ParentWindow()

This.X = lw_parent.X + (lw_parent.Width / 2) - (This.Width / 2)
	This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)


//This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
//			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))
//


If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if
// State Changes ibdkdld 09/20/02
//If dw_product.RowCount() = 0 Then
//	dw_product.InsertRow(0)
//End if
if dw_fab.rowcount( ) = 0 Then
	dw_fab.insertrow( 0)
End If

end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

on w_pas_pa_view_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.st_1=create st_1
this.uo_begin_date=create uo_begin_date
this.cbx_ship_off_the_cut=create cbx_ship_off_the_cut
this.cbx_ranges=create cbx_ranges
this.dw_fab=create dw_fab
this.cbx_epa=create cbx_epa
this.cbx_pt=create cbx_pt
this.cbx_1_week=create cbx_1_week
this.cbx_2_weeks=create cbx_2_weeks
this.gb_1=create gb_1
this.gb_2=create gb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.uo_begin_date
this.Control[iCurrent+4]=this.cbx_ship_off_the_cut
this.Control[iCurrent+5]=this.cbx_ranges
this.Control[iCurrent+6]=this.dw_fab
this.Control[iCurrent+7]=this.cbx_epa
this.Control[iCurrent+8]=this.cbx_pt
this.Control[iCurrent+9]=this.cbx_1_week
this.Control[iCurrent+10]=this.cbx_2_weeks
this.Control[iCurrent+11]=this.gb_1
this.Control[iCurrent+12]=this.gb_2
end on

on w_pas_pa_view_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.st_1)
destroy(this.uo_begin_date)
destroy(this.cbx_ship_off_the_cut)
destroy(this.cbx_ranges)
destroy(this.dw_fab)
destroy(this.cbx_epa)
destroy(this.cbx_pt)
destroy(this.cbx_1_week)
destroy(this.cbx_2_weeks)
destroy(this.gb_1)
destroy(this.gb_2)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date	ldt_temp

String	ls_temp,ls_temp1
Long 		ll_rtn

If dw_plant.AcceptText() = -1 Then 
	dw_plant.SetFocus()
	return
End if
// State Changes ibdkdld 09/20/02
//If dw_product.AcceptText() = -1 Then 
//	dw_product.SetFocus()
//	return 
//End if

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End if

If not dw_fab.uf_validate( ) Then
	dw_fab.setfocus()
	return
End If
// State Changes ibdkdld 09/20/02
//If iw_frame.iu_string.nf_IsEmpty(dw_product.GetItemString(1, "fab_product_code")) Then
//	iw_frame.SetMicroHelp("Product Code is a required field")
//	dw_product.SetFocus()
//	return
//End if
If iw_frame.iu_string.nf_IsEmpty(dw_fab.GetItemString(1, "fab_product_code")) Then
	iw_frame.SetMicroHelp("Product Code is a required field")
	dw_fab.SetFocus()
	return
End if

ldt_temp = Date(uo_begin_date.nf_GetText())
If IsNull(ldt_temp) Then
	iw_frame.SetMicroHelp("Begin Date is a required field")
	uo_begin_date.SetFocus()
	return
End if
If ldt_temp < Today() Then
	iw_frame.SetMicroHelp("Begin Date cannot be earlier than today")
	uo_begin_date.SetFocus()
	return
End If

// ibdkdld PA Ext dates begin
//new
nvuo_pa_business_rules nvuo_pa
ls_temp1 = ''
ll_rtn = nvuo_pa.uf_check_pa_date_ext(LDT_TEMP,ls_temp1)

Choose case ll_rtn
	Case 0 
		// Valid PA Range
		//Return 
	Case 1	
		// Valid Ext PA WeekEnd date
		//Return 
	Case 2
		// Set valid Ext PA WeekEnd date 
		ls_temp = 'The date of ' + string(ldt_temp,'mm/dd/yyyy') + ' is beyond the daily PA range -- ' & 
		  + 'the Begin Date has been changed to the next Sunday date'
		iw_frame.SetMicroHelp(ls_temp)
		is_begin_date = ls_temp1
		This.PostEvent("begindatechanged")
		Return 
	Case 3
		// Invalid Ext PA WeekEnd date/PA Range date
		ls_temp = 'You must select a date between '+ String(today(),'mm/dd/yyyy') + ' and ' &
				+ ls_temp1 + ' or a Sunday date beyond ' + ls_temp1  
		iw_frame.SetMicroHelp(ls_temp)
		uo_begin_date.SetFocus()
		Return 
	Case 4
		ls_temp = 'The date of ' + string(ldt_temp,'mm/dd/yyyy') + ' is beyond the extended PA weekend date -- ' & 
		  + 'the Begin Date has been changed to the max Sunday date'
		iw_frame.SetMicroHelp(ls_temp)
		is_begin_date = ls_temp1
		This.PostEvent("begindatechanged")
		return 
	Case 5 
		ls_temp = 'The date of ' + string(ldt_temp,'mm/dd/yyyy') + ' is beyond the Daily PA date range -- ' & 
		  + 'the date has been changed to the max Daily PA Date'
		iw_frame.SetMicroHelp(ls_temp)		
		is_begin_date = ls_temp1
		This.PostEvent("begindatechanged")
		return 
		
end choose		
// END DATE CHANGE

//ls_temp = dw_plant.Describe("DataWindow.Data") + '~t' + &
//			 dw_product.Describe("DataWindow.Data") + '~t' + &	
//			 uo_begin_date.nf_GetText()

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant_Desc', ls_temp)
// State Changes ibdkdld 09/20/02
//ls_temp = dw_product.uf_getproductcode()
//iw_parent.Event ue_Set_Data('Product', ls_temp)
ls_temp = dw_fab.uf_get_product_code( )
iw_parent.Event ue_Set_Data('Product', ls_temp)
// State Changes ibdkdld 09/20/02
//ls_temp = dw_product.uf_getproductdesc()
//iw_parent.Event ue_Set_Data('Product_Desc', ls_temp)
ls_temp = dw_fab.uf_get_product_desc( )
iw_parent.Event ue_Set_Data('Product_Desc', ls_temp)
// State Changes ibdkdld 09/20/02
ls_temp = dw_fab.uf_get_product_state( )
iw_parent.Event ue_Set_Data('State', ls_temp)
ls_temp = dw_fab.uf_get_product_status( )
iw_parent.Event ue_Set_Data('Status', ls_temp)

ls_temp = dw_fab.uf_get_product_state_desc( )
iw_parent.Event ue_Set_Data('State_Desc', ls_temp)
ls_temp = dw_fab.uf_get_product_status_desc( )
iw_parent.Event ue_Set_Data('Status_Desc', ls_temp)

//
ls_temp = uo_begin_date.nf_GetText()
iw_parent.Event ue_Set_Data('Begin_Date', ls_temp)

If cbx_ship_off_the_cut.Checked Then
	ls_temp = 'Y'
Else
	ls_temp = 'N'
End If
iw_parent.Event ue_Set_Data('Ship_Off_The_Cut', ls_temp)

If cbx_ranges.Checked Then
	ls_temp = 'Y'
Else
	ls_temp = 'N'
End If
iw_parent.Event ue_Set_Data('Ranges', ls_temp)

//kelly added  epa/pt
If cbx_epa.Checked Then
	ls_temp = 'E'
Else
	ls_temp = 'N'
End If
iw_parent.Event ue_Set_Data('epa_view', ls_temp)
// jxr - 8/15/2018 save epa checked to user ini file.
SetProfileString(gw_netwise_frame.is_Userini, Message.nf_Get_App_ID(), "pa_view_inq.epa_checked", ls_temp)

If cbx_pt.Checked Then
	ls_temp = 'P'
Else
	ls_temp = 'N'
End If

iw_parent.Event ue_Set_Data('pt_view2', ls_temp)
// jxr - 8/15/2018 save pt checked to user ini file.
SetProfileString(gw_netwise_frame.is_Userini, Message.nf_Get_App_ID(), "pa_view_inq.pt_checked", ls_temp)

If cbx_1_week.Checked Then
	iw_parent.Event ue_Set_Data('number_of_weeks', '1')
	SetProfileString(gw_netwise_frame.is_Userini, Message.nf_Get_App_ID(), "pa_view_inq.number_of_weeks", '1')
Else
	iw_parent.Event ue_Set_Data('number_of_weeks', '2')
	SetProfileString(gw_netwise_frame.is_Userini, Message.nf_Get_App_ID(), "pa_view_inq.number_of_weeks", '2')
End If

ib_valid_return = True
Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_pa_view_inq
integer x = 1696
integer y = 272
integer taborder = 80
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_pa_view_inq
integer x = 1696
integer y = 148
integer taborder = 70
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_pa_view_inq
integer x = 1696
integer y = 28
integer taborder = 60
end type

type dw_plant from u_plant within w_pas_pa_view_inq
integer x = 183
integer y = 12
integer height = 92
integer taborder = 10
end type

type st_1 from statictext within w_pas_pa_view_inq
integer x = 14
integer y = 428
integer width = 261
integer height = 76
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "Begin Date:"
alignment alignment = center!
boolean focusrectangle = false
end type

type uo_begin_date from u_dddw_editmask within w_pas_pa_view_inq
integer x = 274
integer y = 412
integer width = 494
integer taborder = 30
boolean border = false
long backcolor = 67108864
end type

on constructor;call u_dddw_editmask::constructor;This.nf_SetDataObject('d_date_dddw')
This.DisplayColumn = 'start_date'


end on

on uo_begin_date.destroy
call u_dddw_editmask::destroy
end on

type cbx_ship_off_the_cut from checkbox within w_pas_pa_view_inq
integer x = 1211
integer y = 404
integer width = 439
integer height = 92
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 79741120
string text = "Ship off the cut:"
boolean lefttext = true
end type

type cbx_ranges from checkbox within w_pas_pa_view_inq
integer x = 805
integer y = 476
integer width = 846
integer height = 76
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Show Production Dates as Ranges:"
boolean lefttext = true
end type

type dw_fab from u_fab_product_code within w_pas_pa_view_inq
integer y = 96
integer width = 1609
integer height = 288
integer taborder = 20
boolean bringtotop = true
end type

type cbx_epa from checkbox within w_pas_pa_view_inq
integer x = 64
integer y = 620
integer width = 306
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 79741120
string text = "EPA:"
boolean lefttext = true
end type

event clicked;If this.checked = True Then
	If cbx_pt.checked Then
		cbx_pt.checked = False
	End If
End If
end event

type cbx_pt from checkbox within w_pas_pa_view_inq
integer x = 64
integer y = 684
integer width = 306
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 79741120
string text = "PT:"
boolean lefttext = true
end type

event clicked;If this.checked = True Then
	If cbx_epa.checked Then
		cbx_epa.checked = False
	End If
End If
end event

type cbx_1_week from checkbox within w_pas_pa_view_inq
integer x = 1307
integer y = 636
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "1 Week:"
boolean lefttext = true
end type

event clicked;If this.checked = True Then
	cbx_2_weeks.checked = False
End If
end event

type cbx_2_weeks from checkbox within w_pas_pa_view_inq
integer x = 1307
integer y = 704
integer width = 343
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "2 Weeks:"
boolean lefttext = true
end type

event clicked;If this.checked = True Then
	cbx_1_week.checked = False
End If
end event

type gb_1 from groupbox within w_pas_pa_view_inq
integer x = 27
integer y = 540
integer width = 576
integer height = 252
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 79741120
string text = "Inventory View"
end type

type gb_2 from groupbox within w_pas_pa_view_inq
integer x = 1051
integer y = 572
integer width = 713
integer height = 224
integer taborder = 70
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Number of Weeks"
end type

