﻿$PBExportHeader$w_planned_transfer_plant_resp.srw
forward
global type w_planned_transfer_plant_resp from w_base_response_ext
end type
type dw_plants from datawindow within w_planned_transfer_plant_resp
end type
type dw_include_exclude from u_base_dw_ext within w_planned_transfer_plant_resp
end type
end forward

global type w_planned_transfer_plant_resp from w_base_response_ext
integer width = 1358
integer height = 1388
long backcolor = 67108864
dw_plants dw_plants
dw_include_exclude dw_include_exclude
end type
global w_planned_transfer_plant_resp w_planned_transfer_plant_resp

type variables
String		is_open_parm
end variables

forward prototypes
public function boolean wf_check_duplicates ()
end prototypes

public function boolean wf_check_duplicates ();Long		ll_sub1, ll_sub2

String		ls_plant1, ls_plant2, ls_col1, ls_col2

For ll_sub1 = 1 to 10
	For ll_sub2 = ll_sub1 + 1 to 10
		ls_col1 = "plant" + String(ll_sub1)
		ls_plant1 = dw_plants.GetItemString(1, ls_col1)
		ls_col2 = "plant" + String(ll_sub2)
		ls_plant2 = dw_plants.GetItemString(1, ls_col2)
		If (ls_plant1 = ls_plant2) and (ls_plant1 > '   ') Then
			MessageBox("Planned Transfer Include/Exclude Plants", "Please correct the duplicate Plant.")
			Return False
		End If
	Next
Next

Return True

end function

on w_planned_transfer_plant_resp.create
int iCurrent
call super::create
this.dw_plants=create dw_plants
this.dw_include_exclude=create dw_include_exclude
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plants
this.Control[iCurrent+2]=this.dw_include_exclude
end on

on w_planned_transfer_plant_resp.destroy
call super::destroy
destroy(this.dw_plants)
destroy(this.dw_include_exclude)
end on

event open;call super::open;is_open_parm = Message.StringParm
end event

event ue_postopen;call super::ue_postopen;u_string_functions			lu_string_functions

String		ls_include_exclude, ls_plant_list, ls_plant

ls_include_exclude = lu_string_functions.nf_gettoken(is_open_parm, '~t')
ls_plant_list = lu_string_functions.nf_gettoken(is_open_parm, '~t')
DataWindowChild		ldwch_plant

Long		ll_found

dw_include_exclude.SetItem(1, "include_exclude", ls_include_exclude)

If ls_include_exclude <= ' ' Then
	dw_plants.Object.plant1.Protect = True
	dw_plants.Object.plant2.Protect = True
	dw_plants.Object.plant3.Protect = True
	dw_plants.Object.plant4.Protect = True
	dw_plants.Object.plant5.Protect = True
	dw_plants.Object.plant6.Protect = True
	dw_plants.Object.plant7.Protect = True
	dw_plants.Object.plant8.Protect = True
	dw_plants.Object.plant9.Protect = True
	dw_plants.Object.plant10.Protect = True
	dw_plants.Object.plant1.BackGround.Color = '78682240'
	dw_plants.Object.plant2.BackGround.Color = '78682240'
	dw_plants.Object.plant3.BackGround.Color = '78682240'
	dw_plants.Object.plant4.BackGround.Color = '78682240'
	dw_plants.Object.plant5.BackGround.Color = '78682240'
	dw_plants.Object.plant6.BackGround.Color = '78682240'
	dw_plants.Object.plant7.BackGround.Color = '78682240'
	dw_plants.Object.plant8.BackGround.Color = '78682240'
	dw_plants.Object.plant9.BackGround.Color = '78682240'
	dw_plants.Object.plant10.BackGround.Color = '78682240'
Else
	dw_plants.Object.plant1.Protect = False
	dw_plants.Object.plant2.Protect = False
	dw_plants.Object.plant3.Protect = False
	dw_plants.Object.plant4.Protect = False
	dw_plants.Object.plant5.Protect = False
	dw_plants.Object.plant6.Protect = False
	dw_plants.Object.plant7.Protect = False
	dw_plants.Object.plant8.Protect = False
	dw_plants.Object.plant9.Protect = False
	dw_plants.Object.plant10.Protect = False	
	dw_plants.Object.plant1.BackGround.Color = '16777215'
	dw_plants.Object.plant2.BackGround.Color = '16777215'
	dw_plants.Object.plant3.BackGround.Color = '16777215'
	dw_plants.Object.plant4.BackGround.Color = '16777215'
	dw_plants.Object.plant5.BackGround.Color = '16777215'
	dw_plants.Object.plant6.BackGround.Color = '16777215'
	dw_plants.Object.plant7.BackGround.Color = '16777215'
	dw_plants.Object.plant8.BackGround.Color = '16777215'
	dw_plants.Object.plant9.BackGround.Color = '16777215'
	dw_plants.Object.plant10.BackGround.Color = '16777215'
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant1", ls_plant)
	dw_plants.GetChild('plant1', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc1", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant2", ls_plant)
	dw_plants.GetChild('plant2', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc2", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant3", ls_plant)
	dw_plants.GetChild('plant3', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc3", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant4", ls_plant)
	dw_plants.GetChild('plant4', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc4", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant5", ls_plant)
	dw_plants.GetChild('plant5', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc5", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant6", ls_plant)
	dw_plants.GetChild('plant6', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc6", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant7", ls_plant)
	dw_plants.GetChild('plant7', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc7", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant8", ls_plant)
	dw_plants.GetChild('plant8', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc8", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant9", ls_plant)
	dw_plants.GetChild('plant9', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc9", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

ls_plant =  lu_string_functions.nf_gettoken(ls_plant_list, ',')
If ls_plant > '   ' Then
	dw_plants.SetItem (1, "plant10", ls_plant)
	dw_plants.GetChild('plant10', ldwch_plant)
	ll_found = ldwch_plant.Find("location_code = '" + ls_plant + "'", 1, ldwch_plant.RowCount())			
	if ll_found > 1 then
			dw_plants.SetItem(1, "plant_desc10", ldwch_plant.GetItemString(ll_found, "location_name")) 		
	End If	
End If

end event

event ue_base_ok;call super::ue_base_ok;String		ls_include_exclude, ls_plant_data, ls_return_parms

dw_plants.AcceptText()
dw_include_exclude.AcceptText()

If Not wf_check_duplicates() Then Return

ls_include_exclude = dw_include_exclude.GetItemString(1, "include_exclude")

if dw_plants.GetItemString(1, "plant1") > '   ' Then
	ls_plant_data += dw_plants.GetItemString(1, "plant1") 
End If

if dw_plants.GetItemString(1, "plant2") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant2") 
End If

if dw_plants.GetItemString(1, "plant3") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant3") 
End If

if dw_plants.GetItemString(1, "plant4") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant4") 
End If

if dw_plants.GetItemString(1, "plant5") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant5") 
End If

if dw_plants.GetItemString(1, "plant6") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant6") 
End If

if dw_plants.GetItemString(1, "plant7") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant7") 
End If

if dw_plants.GetItemString(1, "plant8") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant8") 
End If

if dw_plants.GetItemString(1, "plant9") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant9") 
End If

if dw_plants.GetItemString(1, "plant10") > '   ' Then
	ls_plant_data += ',' + dw_plants.GetItemString(1, "plant10") 
End If

ls_return_parms = ls_include_exclude + '~t' + ls_plant_data + '~t' 

CloseWithReturn(This, ls_return_parms)

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_planned_transfer_plant_resp
boolean visible = false
integer x = 544
integer y = 940
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_planned_transfer_plant_resp
integer x = 978
integer y = 1172
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_planned_transfer_plant_resp
integer x = 622
integer y = 1172
end type

type dw_plants from datawindow within w_planned_transfer_plant_resp
integer x = 14
integer y = 108
integer width = 1294
integer height = 1020
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_planned_transfer_plants"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;DataWindowChild		ls_dwch_plant

If This.RowCount() = 0 then This.InsertRow(0)

This.GetChild('plant1', ls_dwch_plant)
CONNECT USING SQLCA;
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant2', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant3', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant4', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant5', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant6', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant7', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant8', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant9', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()

This.GetChild('plant10', ls_dwch_plant)
ls_dwch_plant.SetTransObject(SQLCA)
ls_dwch_plant.Retrieve()
end event

event itemchanged;DataWindowChild	ldwc_temp
Long					ll_found


CHOOSE CASE This.GetColumnName()
		
	Case "plant1"
			If data > '   ' Then
				This.GetChild("plant1", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc1", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant1", "   ")
				This.SetItem(1, "plant_desc1", Space(30))
			end if

	Case "plant2"
			If data > '   ' Then
				This.GetChild("plant2", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc2", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant2", "   ")
				This.SetItem(1, "plant_desc2", Space(30))
			end if

	Case "plant3"
			If data > '   ' Then
				This.GetChild("plant3", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc3", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant3", "   ")
				This.SetItem(1, "plant_desc3", Space(30))
			end if			

	Case "plant4"
			If data > '   ' Then
				This.GetChild("plant4", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc4", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant4", "   ")
				This.SetItem(1, "plant_desc4", Space(30))
			end if			

	Case "plant5"
			If data > '   ' Then
				This.GetChild("plant5", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc5", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant5", "   ")
				This.SetItem(1, "plant_desc5", Space(30))
			end if			
			
	Case "plant6"
			If data > '   ' Then
				This.GetChild("plant6", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc6", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant6", "   ")
				This.SetItem(1, "plant_desc6", Space(30))
			end if			

	Case "plant7"
			If data > '   ' Then
				This.GetChild("plant7", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc7", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant7", "   ")
				This.SetItem(1, "plant_desc7", Space(30))
			end if				

	Case "plant8"
			If data > '   ' Then
				This.GetChild("plant8", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc8", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant8", "   ")
				This.SetItem(1, "plant_desc8", Space(30))
			end if			

	Case "plant9"
			If data > '   ' Then
				This.GetChild("plant9", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc9", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant9", "   ")
				This.SetItem(1, "plant_desc9", Space(30))
			end if		
			
	Case "plant10"
			If data > '   ' Then
				This.GetChild("plant10", ldwc_temp)
				ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
				if ll_found = 0 then
					MessageBox('Invalid Ship Plant', 'Ship plant is not invalid')
					Return 1
				else	
					This.SetItem(1, "plant_desc10", ldwc_temp.GetItemString(ll_found, "location_name")) 
				end if
			else
				This.SetItem(1, "plant10", "   ")
				This.SetItem(1, "plant_desc10", Space(30))
			end if					
End Choose
	
end event

event itemerror;Return 1
end event

type dw_include_exclude from u_base_dw_ext within w_planned_transfer_plant_resp
integer x = 46
integer height = 120
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_planned_transfer_inc_exc"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;If data <= ' ' Then
	dw_plants.SetItem(1, "plant1", '')
	dw_plants.SetItem(1, "plant_desc1", '')
	dw_plants.SetItem(1, "plant2", '')
	dw_plants.SetItem(1, "plant_desc2", '')
	dw_plants.SetItem(1, "plant3", '')
	dw_plants.SetItem(1, "plant_desc3", '')
	dw_plants.SetItem(1, "plant4", '')
	dw_plants.SetItem(1, "plant_desc4", '')
	dw_plants.SetItem(1, "plant5", '')
	dw_plants.SetItem(1, "plant_desc5", '')
	dw_plants.SetItem(1, "plant6", '')
	dw_plants.SetItem(1, "plant_desc6", '')
	dw_plants.SetItem(1, "plant7", '')
	dw_plants.SetItem(1, "plant_desc7", '')
	dw_plants.SetItem(1, "plant8", '')
	dw_plants.SetItem(1, "plant_desc8", '')
	dw_plants.SetItem(1, "plant9", '')
	dw_plants.SetItem(1, "plant_desc9", '')
	dw_plants.SetItem(1, "plant10", '')
	dw_plants.SetItem(1, "plant_desc10", '')
	dw_plants.Object.plant1.Protect = True
	dw_plants.Object.plant2.Protect = True
	dw_plants.Object.plant3.Protect = True
	dw_plants.Object.plant4.Protect = True
	dw_plants.Object.plant5.Protect = True
	dw_plants.Object.plant6.Protect = True
	dw_plants.Object.plant7.Protect = True
	dw_plants.Object.plant8.Protect = True
	dw_plants.Object.plant9.Protect = True
	dw_plants.Object.plant10.Protect = True
	dw_plants.Object.plant1.BackGround.Color = '78682240'
	dw_plants.Object.plant2.BackGround.Color = '78682240'
	dw_plants.Object.plant3.BackGround.Color = '78682240'
	dw_plants.Object.plant4.BackGround.Color = '78682240'
	dw_plants.Object.plant5.BackGround.Color = '78682240'
	dw_plants.Object.plant6.BackGround.Color = '78682240'
	dw_plants.Object.plant7.BackGround.Color = '78682240'
	dw_plants.Object.plant8.BackGround.Color = '78682240'
	dw_plants.Object.plant9.BackGround.Color = '78682240'
	dw_plants.Object.plant10.BackGround.Color = '78682240'
Else
	dw_plants.Object.plant1.Protect = False
	dw_plants.Object.plant2.Protect = False
	dw_plants.Object.plant3.Protect = False
	dw_plants.Object.plant4.Protect = False
	dw_plants.Object.plant5.Protect = False
	dw_plants.Object.plant6.Protect = False
	dw_plants.Object.plant7.Protect = False
	dw_plants.Object.plant8.Protect = False
	dw_plants.Object.plant9.Protect = False
	dw_plants.Object.plant10.Protect = False	
	dw_plants.Object.plant1.BackGround.Color = '16777215'
	dw_plants.Object.plant2.BackGround.Color = '16777215'
	dw_plants.Object.plant3.BackGround.Color = '16777215'
	dw_plants.Object.plant4.BackGround.Color = '16777215'
	dw_plants.Object.plant5.BackGround.Color = '16777215'
	dw_plants.Object.plant6.BackGround.Color = '16777215'
	dw_plants.Object.plant7.BackGround.Color = '16777215'
	dw_plants.Object.plant8.BackGround.Color = '16777215'
	dw_plants.Object.plant9.BackGround.Color = '16777215'
	dw_plants.Object.plant10.BackGround.Color = '16777215'
End If
end event

