﻿$PBExportHeader$w_production_buffer.srw
forward
global type w_production_buffer from w_base_sheet_ext
end type
type dw_sold_pct from u_base_dw_ext within w_production_buffer
end type
type dw_division from u_division within w_production_buffer
end type
type dw_detail from u_base_dw_ext within w_production_buffer
end type
end forward

global type w_production_buffer from w_base_sheet_ext
integer width = 2683
string title = "Production Buffer"
long backcolor = 12632256
dw_sold_pct dw_sold_pct
dw_division dw_division
dw_detail dw_detail
end type
global w_production_buffer w_production_buffer

type variables
u_pas203		iu_pas203
u_ws_pas1		iu_ws_pas1

s_error		istr_error_info


end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();String	ls_input, &
			ls_output, &
			ls_filter, &
			ls_exclude, &
			ls_sold_percent

Integer	li_ret, &
			li_counter
Long		ll_rowcount


IF Not Super::wf_retrieve() Then Return False


SetPointer(HourGlass!)

ls_input  = dw_division.uf_get_division() + "~r~n"

This.SetRedraw(False)
dw_sold_pct.Reset()
dw_sold_pct.InsertRow(0)
dw_detail.Reset()

//li_ret = iu_pas203.nf_inq_prod_buffer(istr_error_info, &
//														ls_input, &
//														ls_output)
li_ret = iu_ws_pas1.nf_pasp54fr(istr_error_info, &
														ls_input, &
														ls_output)

If li_ret <> 0 Then
	This.SetRedraw(True)
	return False
End If
//ls_output = '.5000	003	plant	.05	.04	.06	.06~r~n' + &
//				'004	Dakota City	.03	.03	.04	.03~r~n' + &
//				'006	I dont know	.06	.02	.06	.05~r~n' + &
//				'011	plant 11	.06	.02	.06	.06~r~n' + &
//				'026	lexington	.07	.07	.05	.05~r~n' + &
//				'031	pork plant	.02	.05	.06	.06~r~n' + &
//				'054	ok coral	.04	.03	.02	.02~r~n' + &
//				'063		.01	.05	.02	.01~r~n' + &
//				'083		.05	.05	.06	.04~r~n' 

iw_frame.iu_string.nf_parseleftright(ls_output, '~t', ls_sold_percent, ls_output)
dw_sold_pct.SetItem(1, 'sold_percent', double(ls_sold_percent))
dw_detail.ImportString(ls_output)

dw_sold_pct.ResetUpdate()
dw_detail.ResetUpdate()

This.SetRedraw(True)
return true
end function

public function boolean wf_update ();Boolean	lb_update

Integer	li_ret

Long		ll_modified

String	ls_input_header, &
			ls_input_detail, &
			ls_current_pend, &
			ls_other_pend

			
If dw_detail.AcceptText() < 1 Then Return False
If dw_sold_pct.AcceptText() < 1 Then Return False

lb_update = false
SetPointer(HourGlass!)

ls_input_header  = dw_division.uf_get_division() + "~t"

IF dw_sold_pct.GetItemStatus(1, 'sold_percent', primary!) = DataModified! Then
	lb_update = True
	ls_input_header += String(dw_sold_pct.GetItemDecimal(1,"sold_percent"))
End IF

ls_input_detail = ''
ll_modified = dw_detail.GetNextModified(0, primary!)

Do While ll_modified > 0
	lb_update = True
	ls_input_detail += dw_detail.GetItemString(ll_modified, 'plant_code') + '~t'
	ls_current_pend = dw_detail.GetItemString(ll_modified, 'current_pend')
	If Not iw_frame.iu_string.nf_IsEmpty(ls_current_pend) Then
		ls_input_detail += String(Double(ls_current_pend) * 0.01) + '~t'
	Else
		ls_input_detail += String(dw_detail.GetItemDecimal(ll_modified, 'current_effect')) + '~t'
	End If
	ls_other_pend = dw_detail.GetItemString(ll_modified, 'other_pend')
	If Not iw_frame.iu_string.nf_IsEmpty(ls_other_pend) Then
		ls_input_detail += String(Double(ls_other_pend) * 0.01)
	Else
		ls_input_detail += String(dw_detail.GetItemDecimal(ll_modified, 'other_effect')) 
	End If
		
	ls_input_detail += '~r~n'
	ll_modified = dw_detail.GetNextModified(ll_modified, primary!)
Loop

If Not lb_update Then 
	Return False
End If

//li_ret = iu_pas203.nf_upd_prod_buffer(istr_error_info, &
//															ls_input_header, &
//															ls_input_detail)
															
li_ret = iu_ws_pas1.nf_pasp55fr(istr_error_info, &
												ls_input_header, &
												ls_input_detail)
If li_ret <> 0 Then
	return False
End If

dw_detail.ResetUpdate()
dw_sold_pct.ResetUpdate()

iw_frame.SetMicroHelp("Modification Successful")

return True
end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event close;call super::close;If IsValid(iu_pas203) Then
	Destroy iu_pas203
End If

Destroy iu_ws_pas1
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'division'
		Message.StringParm = dw_division.uf_get_division()
End Choose



end event

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_grnd_beef_prty"
istr_error_info.se_user_id 		= sqlca.userid


iu_pas203 = Create u_pas203
iu_ws_pas1 = Create u_ws_pas1
is_inquire_window_name = 'w_division_inq'

wf_retrieve()

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'division'
		dw_division.uf_set_division(as_value)
End Choose

end event

on w_production_buffer.create
int iCurrent
call super::create
this.dw_sold_pct=create dw_sold_pct
this.dw_division=create dw_division
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sold_pct
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_detail
end on

on w_production_buffer.destroy
call super::destroy
destroy(this.dw_sold_pct)
destroy(this.dw_division)
destroy(this.dw_detail)
end on

event ue_fileprint;call super::ue_fileprint;dw_detail.print()
end event

event resize;call super::resize;constant integer li_x		= 20
constant integer li_y		= 140
  
dw_detail.width	= newwidth - li_x
dw_detail.height	= newheight - li_y


end event

type dw_sold_pct from u_base_dw_ext within w_production_buffer
integer x = 1600
integer y = 16
integer width = 686
integer height = 116
integer taborder = 20
string dataobject = "d_production_buffer_header"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;Choose Case String(dwo.name)
	Case 'sold_percent'
		If Double(data) > 1.00 or Double(data) < 0.00 then
			This.selecttext(1, 100)
			iw_frame.SetMicroHelp('Sold Percent must be between 0 and 100')
			Return 1
		End If
End Choose
end event

type dw_division from u_division within w_production_buffer
integer y = 20
integer taborder = 10
end type

event constructor;call super::constructor;ib_updateable = False
This.Disable()

end event

type dw_detail from u_base_dw_ext within w_production_buffer
event ue_postitemchanged ( long al_row,  string as_data_value,  string as_column_name )
integer y = 124
integer width = 2583
integer height = 1264
integer taborder = 30
string dataobject = "d_production_buffer"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_postitemchanged;call super::ue_postitemchanged;
Choose Case as_column_name
	Case 'current_pend'
		This.SetItem(al_row, 'current_pend',as_data_value)
	Case 'other_pend'
		This.SetItem(al_row, 'other_pend',as_data_value)
End Choose



end event

event itemchanged;call super::itemchanged;Double			ld_value 
String			ls_data_value, &
					ls_column_name
					

If Not IsNumber(data) Then 
	This.selecttext(1, 100)
	iw_frame.SetMicroHelp('Pending Percents must be a number between 0% and 100%')
	Return 1
End If

ld_value = Double(data) * 0.01
ls_data_value = String(Double(data), '###.00')
ls_column_name =  String(dwo.name)
Choose Case ls_column_name
	Case 'current_pend'
		If ld_value > 1.00 or ld_value < 0.00 then
			This.selecttext(1, 100)
			iw_frame.SetMicroHelp('Current Weeks Pending Percent must be between 0% and 100%')
			Return 1
		End If
	Case 'other_pend'
		If ld_value > 1.00 or ld_value < 0.00 then
			This.selecttext(1, 100)
			iw_frame.SetMicroHelp('Other Weeks Pending Percent must be between 0% and 100%')
			Return 1
		End If
End Choose


This.Event Post ue_postitemchanged(row, ls_data_value, ls_column_name)
end event

event itemerror;call super::itemerror;Return 1
end event

