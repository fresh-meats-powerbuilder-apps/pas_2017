﻿$PBExportHeader$w_raw_material_specifications_new.srw
forward
global type w_raw_material_specifications_new from w_base_sheet_ext
end type
type dw_raw_mat_spec_new from u_base_dw_ext within w_raw_material_specifications_new
end type
end forward

global type w_raw_material_specifications_new from w_base_sheet_ext
integer width = 2875
integer height = 1528
string title = "Raw Material Specifications"
windowanimationstyle openanimation = topslide!
dw_raw_mat_spec_new dw_raw_mat_spec_new
end type
global w_raw_material_specifications_new w_raw_material_specifications_new

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product, &
				lb_updating, ib_updating , lb_repeat, invalid_prod

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow, il_last_clicked_row

s_error		istr_error_info

u_ws_pas2       iu_ws_pas2

u_ws_pas1      iu_ws_pas1

String		is_colname, &
				is_input, &
				is_ChangedColumnName, &
				is_debug, &
				prod_code, & 
				from_days_old, &
				to_days_old, &
				from_supply, &
				to_supply
				
w_base_sheet	iw_order_detail
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_validate (integer al_row)
public function integer wf_checkstatus ()
public subroutine wf_delete ()
end prototypes

public function boolean wf_retrieve ();String 	ls_paspdav, ls_outputstring

Long		ll_rec_count

Integer   li_rtn_code


SetPointer(HourGlass!)

istr_error_info.se_event_name = "wf_retrieve"

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

dw_raw_mat_spec_new.Reset()

If (iu_ws_pas2.nf_pasp25gr(ls_outputstring,istr_error_info)) < 0 Then Return False

If Not iw_frame.iu_string.nf_IsEmpty(ls_outputstring) Then
	ll_rec_count = dw_raw_mat_spec_new.ImportString(ls_outputstring)
	If ll_rec_count > 0 then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
	dw_raw_mat_spec_new.SelectRow(1, TRUE)
End if

il_last_clicked_row = 1
dw_raw_mat_spec_new.object.product_code.protect = True
prod_code = dw_raw_mat_spec_new.GetItemString(1,"product_code")
from_days_old = dw_raw_mat_spec_new.GetItemString(1,"from_days_old")
to_days_old = dw_raw_mat_spec_new.GetItemString(1,"to_days_old")
from_supply = dw_raw_mat_spec_new.GetItemString(1,"from_suply_days")
to_supply = dw_raw_mat_spec_new.GetItemString(1,"to_suply_days")
dw_raw_mat_spec_new.ResetUpdate()

return True
end function

public function boolean wf_update ();int				li_count, ii_rec_count, li_rval
long				ll_Row, &
					ll_modrows, &
					ll_delrows, &
					total_rows
Char				lc_status_ind					
string			ls_division, &
					ls_product, ls_update_string

IF dw_raw_mat_spec_new.AcceptText() = -1 Then Return False

total_rows = dw_raw_mat_spec_new.RowCount()
ll_modrows = dw_raw_mat_spec_new.ModifiedCount()
ll_delrows = dw_raw_mat_spec_new.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0 Then Return False

wf_checkstatus()
// INSERT AND UPDATE ----
ll_row = 0
For li_count = 1 To total_rows
	//build update string
	If dw_raw_mat_spec_new.GetItemString(li_count,"update_flag") = 'I' Then
		If Not wf_validate(li_count) Then 
			Return False
		else
			ls_update_string += 'I' + '~t' + dw_raw_mat_spec_new.GetItemString(li_count,"product_code") + '~t' +&
			string(dw_raw_mat_spec_new.GetItemNumber(li_count,"from_days_old")) + '~t' +&
			string(dw_raw_mat_spec_new.GetItemNumber(li_count,"to_days_old")) + '~t' +&
			string(dw_raw_mat_spec_new.GetItemNumber(li_count,"from_suply_days")) + '~t' +&
			string(dw_raw_mat_spec_new.GetItemNumber(li_count,"to_suply_days")) + '~r~n'
		End if
	Else 
		If dw_raw_mat_spec_new.GetItemString(li_count,"update_flag") = 'U' Then
			If Not wf_validate(li_count) Then 
				Return False
			else
				ls_update_string += 'U' + '~t' + dw_raw_mat_spec_new.GetItemString(li_count,"product_code") + '~t' +&
				string(dw_raw_mat_spec_new.GetItemNumber(li_count,"from_days_old")) + '~t' +&
				string(dw_raw_mat_spec_new.GetItemNumber(li_count,"to_days_old")) + '~t' +&
				string(dw_raw_mat_spec_new.GetItemNumber(li_count,"from_suply_days")) + '~t'+&
				string(dw_raw_mat_spec_new.GetItemNumber(li_count,"to_suply_days")) + '~r~n'
			End If
		End If
	End If
Next

If ls_update_string <> '' Then
	li_rval = iu_ws_pas2.nf_pasp26gr(ls_update_string,istr_error_info)
	If li_rval < 0 Then Return False
	
	dw_raw_mat_spec_new.object.product_code.protect = TRUE
	ib_updating = True
	iw_frame.SetMicroHelp("Modification Successful")
	dw_raw_mat_spec_new.ResetUpdate()
	wf_retrieve()
End If

end function

public function boolean wf_validate (integer al_row);integer ls_total_row, li_count
string prod1, prod2

//------------------CHECK IF THE FIELDS ARE EMPTY ------------------------------/
If iw_frame.iu_string.nf_IsEmpty(dw_raw_mat_spec_new.GetItemString &
		(al_row, "product_code")) Then 
		dw_raw_mat_spec_new.ScrollToRow(al_row)
		dw_raw_mat_spec_new.SetColumn("product_code")
		ib_updating = False
		lb_repeat = False
		dw_raw_mat_spec_new.TriggerEvent (Itemerror!)
		Return False
End If

If dw_raw_mat_spec_new.GetItemNumber(al_row,"to_days_old") < &
		dw_raw_mat_spec_new.GetItemNumber(al_row,"from_days_old") Then
		dw_raw_mat_spec_new.ScrollToRow(al_row)
		dw_raw_mat_spec_new.SetColumn("to_days_old")
		dw_raw_mat_spec_new.TriggerEvent (Itemerror!)
		Return False
End If

If dw_raw_mat_spec_new.GetItemNumber(al_row,"to_suply_days") < &
		dw_raw_mat_spec_new.GetItemNumber(al_row,"from_suply_days") Then
		dw_raw_mat_spec_new.ScrollToRow(al_row)
		dw_raw_mat_spec_new.SetColumn("from_suply_days")
		dw_raw_mat_spec_new.TriggerEvent (Itemerror!)
		Return False
End If

//check if there isnt any repeated product code
ls_total_row = dw_raw_mat_spec_new.RowCount()
For li_count = 1 To ls_total_row
	If li_count <> al_row Then
		prod1 = trim(dw_raw_mat_spec_new.GetItemString(al_row,"product_code"))
		prod2 = trim(dw_raw_mat_spec_new.GetItemString(li_count,"product_code"))
		If prod1 = prod2 Then
			dw_raw_mat_spec_new.ScrollToRow(al_row)
			dw_raw_mat_spec_new.SetColumn("product_code")
			lb_repeat = True
			ib_updating = True
			dw_raw_mat_spec_new.TriggerEvent (Itemerror!)
			Return False
		else
			lb_repeat = False
		End If
	End If
Next

If invalid_prod Then
	dw_raw_mat_spec_new.SetColumn("product_code")
	dw_raw_mat_spec_new.TriggerEvent (Itemerror!)
	Return False
End If

Return True
end function

public function integer wf_checkstatus ();//----------------Get modified rows---------------------------
long total_rows, cur_row, ll_Row
integer li_count 
string val

total_rows = dw_raw_mat_spec_new.RowCount()

For li_count = 1 To total_rows 
	ll_row = li_count
	Choose Case dw_raw_mat_spec_new.GetItemStatus(ll_row,0,Primary!)
		Case DataModified!
			dw_raw_mat_spec_new.SetItem(ll_row,"update_flag","U")
		Case NewModified!
			dw_raw_mat_spec_new.SetItem(ll_row,"update_flag","I")
	End Choose
Next

return 0

end function

public subroutine wf_delete ();DataWindowChild			ldwc_week_end

int  						li_rval

long  					    ll_source_row

String						ls_flag, ls_update_string


ll_source_row	= dw_raw_mat_spec_new.GetRow()

//If prod_code <> '' Then
	ls_update_string = 'D' + '~t' +prod_code + '~t' +&
				from_days_old + '~t' +&
				to_days_old + '~t' +&
				from_supply + '~t'+&
				to_supply + '~r~n'
	//		End If

If ls_update_string <> '' Then
	li_rval = iu_ws_pas2.nf_pasp26gr(ls_update_string,istr_error_info)
End If
dw_raw_mat_spec_new.SetRedraw(False)
dw_raw_mat_spec_new.SetRedraw(True)

end subroutine

on w_raw_material_specifications_new.create
int iCurrent
call super::create
this.dw_raw_mat_spec_new=create dw_raw_mat_spec_new
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_raw_mat_spec_new
end on

on w_raw_material_specifications_new.destroy
call super::destroy
destroy(this.dw_raw_mat_spec_new)
end on

event ue_postopen;call super::ue_postopen;iu_ws_pas2 = create u_ws_pas2

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_raw_material_specifications_new"
istr_error_info.se_user_id 		= sqlca.userid

This.PostEvent('ue_query')


end event

event close;call super::close;Destroy iu_ws_pas2
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event ue_addrow;call super::ue_addrow;DataWindowChild			ldwc_week_end

Integer						li_count, &
								li_sequence

Long							ll_row, ll_sel_row, ll_source_row

String						ls_string



ll_row = dw_raw_mat_spec_new.RowCount() + 1
il_last_clicked_row = ll_row
ll_sel_row = dw_raw_mat_spec_new.GetSelectedRow(0)
ll_source_row	= dw_raw_mat_spec_new.GetRow()
dw_raw_mat_spec_new.SetRedraw(False)

/// SET DEFAULT VALUES-----------
dw_raw_mat_spec_new.SetItem(ll_source_row,"from_days_old",0)
dw_raw_mat_spec_new.SetItem(ll_source_row,"to_days_old",0)
dw_raw_mat_spec_new.SetItem(ll_source_row,"from_suply_days",0)
dw_raw_mat_spec_new.SetItem(ll_source_row,"to_suply_days",0)
dw_raw_mat_spec_new.SetItem(ll_source_row,"date_update",today())
dw_raw_mat_spec_new.SetItem(ll_source_row,"userid",sqlca.userid)
dw_raw_mat_spec_new.SetItem(ll_source_row,"update_flag", 'I')

dw_raw_mat_spec_new.object.product_code.protect = False
dw_raw_mat_spec_new.SelectRow(ll_sel_row, FALSE)
dw_raw_mat_spec_new.SelectRow(ll_source_row,TRUE)

dw_raw_mat_spec_new.GroupCalc()
dw_raw_mat_spec_new.ScrollToRow(ll_row)
dw_raw_mat_spec_new.SetColumn( "product_code" )
dw_raw_mat_spec_new.SetFocus()
dw_raw_mat_spec_new.SetRedraw(True)

return 0

end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	


end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

end event

event ue_deleterow;call super::ue_deleterow;wf_delete()
end event

type dw_raw_mat_spec_new from u_base_dw_ext within w_raw_material_specifications_new
integer x = 50
integer y = 44
integer width = 2683
integer height = 1280
integer taborder = 20
string dataobject = "d_raw_mat_specs_new"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event itemerror;call super::itemerror;String		ls_text, &
				ls_error_msg, &
				ls_error_ttl
Long			ll_row
Decimal		ldc_value

ls_text = GetText()
ll_row = This.GetRow()

CHOOSE CASE GetColumnName()
	Case "product_code"
		If not ib_updating Then
			ls_error_msg = "Please enter a Product Code"
		End If
		If lb_repeat Then
			ls_error_msg = "Duplicate Row Found"
		//	Else
		//		Return 1
		End If
	 	If invalid_prod Then
			ls_error_msg = "Invalid Product Code"
		End if
	Case "to_days_old"
		//If ib_updating Then
			ls_error_msg = "To days Old can't be less than From Days Old "
		//Else
		//	Return 1
		//End If
	//
	CASE "from_suply_days"
		//If ib_updating Then
			ls_error_msg = "To Supply Date can't be less than From Supply Date"
		//Else
			//Return 1
//		End If

END CHOOSE

//If not ib_updating and 
If ls_error_msg <> '' Then
	iw_frame.SetMicroHelp(ls_error_msg) 
End if

//Return 1


end event

event itemchanged;call super::itemchanged;Boolean	lb_ret, lb_return

Integer	li_count

long		ll_row

Decimal	ldc_value, &
			ldc_SoldPct, &
			ldc_BufPct

String		ls_output, ls_product_info, &
			ls_prod_descr, ls_product_flag, &
			is_product_info, ac_product


ll_row = This.GetSelectedRow(0)

ac_product = data

IF Not IsValid( iu_ws_pas1 ) THEN
	iu_ws_pas1	=  CREATE u_ws_pas1
END IF

Choose Case GetColumnName()
	Case  "product_code"
		
		lb_return	= iu_ws_pas1.nf_pasp03fr( istr_Error_Info, ac_product, is_product_info)
		ls_product_info = is_product_info
		ls_prod_descr = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')
		ls_product_flag = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')
		
		IF ls_prod_descr = '' THEN
			invalid_prod = True
			iw_frame.SetMicroHelp(ac_product + " is Not a Valid Product Code.")
		else
			invalid_prod = False
			this.setitem(row, "prod_description", ls_prod_descr)
		END IF
		
		

	
End Choose
end event

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color


IF row > 0 Then
	If il_last_clicked_row = 0 and row <> 1 Then
		il_last_clicked_row = This.GetSelectedRow(0)
		If il_last_clicked_row = 0 Then
			il_last_clicked_row = 1
		End If
	End If
	
	If row <> il_last_clicked_row Then 
		SelectRow(row, TRUE)
		SelectRow(il_last_clicked_row,FALSE)
	End If
	
	prod_code = dw_raw_mat_spec_new.GetItemString(row,"product_code")
	from_days_old = dw_raw_mat_spec_new.GetItemString(row,"from_days_old")
	to_days_old = dw_raw_mat_spec_new.GetItemString(row,"to_days_old")
	from_supply = dw_raw_mat_spec_new.GetItemString(row,"from_suply_days")
	to_supply = dw_raw_mat_spec_new.GetItemString(row,"to_suply_days")
	
	This.SetRow(Row)
End If
il_ChangedRow = row
il_last_clicked_row = row

end event

