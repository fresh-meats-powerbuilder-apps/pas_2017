﻿$PBExportHeader$w_pas_prod_variance.srw
forward
global type w_pas_prod_variance from w_base_sheet_ext
end type
type dw_production_variance from u_base_dw_ext within w_pas_prod_variance
end type
type st_1 from statictext within w_pas_prod_variance
end type
type st_4 from statictext within w_pas_prod_variance
end type
type em_variance from editmask within w_pas_prod_variance
end type
type rb_estimated from radiobutton within w_pas_prod_variance
end type
type rb_actual from radiobutton within w_pas_prod_variance
end type
type gb_1 from groupbox within w_pas_prod_variance
end type
type dw_plant_variance from u_base_dw_ext within w_pas_prod_variance
end type
end forward

global type w_pas_prod_variance from w_base_sheet_ext
integer width = 2775
integer height = 1464
string title = "Production Variance"
long backcolor = 12632256
dw_production_variance dw_production_variance
st_1 st_1
st_4 st_4
em_variance em_variance
rb_estimated rb_estimated
rb_actual rb_actual
gb_1 gb_1
dw_plant_variance dw_plant_variance
end type
global w_pas_prod_variance w_pas_prod_variance

type variables
u_pas203		iu_pas203
u_ws_pas1		iu_ws_pas1

s_error		istr_error_info

Integer		ii_rec_count

String		is_update_string
end variables

forward prototypes
public function string wf_get_plant ()
public function boolean wf_retrieve ()
public function string wf_get_shift ()
public function string wf_get_date ()
public subroutine wf_filter ()
public subroutine wf_print ()
end prototypes

public function string wf_get_plant ();String			ls_plant

ls_plant = dw_plant_variance.GetItemString(1, "plant_code") + "~t" + &
					dw_plant_variance.GetItemString(1, "plant_descr") 

Return(ls_plant)
end function

public function boolean wf_retrieve ();Int			li_ret, &
				li_Counter

String		ls_string, &
				ls_end_shift, &
				ls_prod_data, &
				ls_plant_code, &
				ls_start_date, &
				ls_shift, &
				ls_Find

Long			ll_rec_count, &
				ll_Row


Call w_base_sheet::closequery

OpenWithParm(w_pas_prod_variance_inq, This)
ls_string = Message.StringParm
If iw_frame.iu_string.nF_IsEmpty(ls_string) Then Return False

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

dw_plant_variance.Reset()
dw_production_variance.Reset()

If dw_plant_variance.ImportString(ls_string) < 1 Then Return False

istr_error_info.se_event_name = "wf_retrieve"

ls_plant_code = dw_plant_variance.GetItemString(1, "plant_code")
ls_start_date = String(dw_plant_variance.GetItemDate( &
						1, "start_date"), "yyyy-mm-dd")
ls_shift = dw_plant_variance.GetItemString(1, "shift")

dw_production_variance.SetRedraw(False)
//If Not iu_pas203.nf_inq_prod_var(istr_error_info, &
//										ls_plant_code + "~t" + &
//										ls_start_date + "~t" + &
//										ls_shift, &
//										ls_end_shift, &
//										dw_production_variance) Then 
//												dw_production_variance.SetRedraw(True)
//												Return False
//End if

If Not iu_ws_pas1.nf_pasp35fr(istr_error_info, &
										ls_plant_code + "~t" + &
										ls_start_date + "~t" + &
										ls_shift, &
										dw_production_variance) Then 
												dw_production_variance.SetRedraw(True)
												Return False
End if



ll_rec_count = dw_production_variance.RowCount()
If ll_rec_count > 0 Then 
	SetPointer(HourGlass!)
	iw_frame.SetMicroHelp(String(ll_rec_count) + &
		" Rows Retrieved")
	dw_production_variance.Sort()
	dw_production_variance.GroupCalc()
	dw_plant_variance.Modify("variance.Background.Color='16777215'")
	dw_plant_variance.SetTabOrder("variance", 10)
	
	For li_Counter = 1 to ll_rec_count - 1
		do
			ls_Find = 'Trim(fab_product_code) = Trim("' + dw_production_variance.GetItemString( &
						li_Counter, 'fab_product_code') + '") And Trim(product_descr) = Trim("' + &
						dw_production_variance.GetItemString(li_Counter, 'product_descr') + '")'
			ll_Row = dw_production_variance.Find(ls_Find, li_Counter + 1, ll_rec_count)
			If ll_Row > 0 Then
				// Estimated Scheduled is the sum of the 2 numbers, everything else is already totals.
				dw_production_variance.SetItem(li_Counter, 'estimated_scheduled', &
					dw_production_variance.GetItemNumber(li_Counter, 'estimated_scheduled') + &
					dw_production_variance.GetItemNumber(ll_Row, 'estimated_scheduled'))
				dw_production_variance.RowsDiscard(ll_row, ll_row, Primary!)
				ll_rec_count --
			End if
		Loop while ll_row > 0 and li_counter < ll_rec_count
	Next
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If
dw_plant_variance.SetItem(1,"end_of_shift", ls_end_shift)

dw_production_variance.SetRedraw(True)

wf_filter()

Return (True)
end function

public function string wf_get_shift ();String			ls_shift

ls_shift = dw_plant_variance.GetItemString(1, "shift")

Return(ls_shift)
end function

public function string wf_get_date ();String			ls_date

ls_date = string(dw_plant_variance.GetItemdate(1, "start_date"), &
					 "mm/dd/yyyy")
Return(ls_date)
end function

public subroutine wf_filter ();Integer		li_ind

long			ll_RowCount, &
				ll_rtn, &
				ll_firstrow

Decimal		ld_variance

string		ls_filter, &
				ls_findcode


dw_production_variance.SetReDraw( False )

ll_firstrow = Long(dw_production_variance.Describe &
					("DataWindow.FirstRowOnPage"))

If ll_FirstRow > 0 Then
	ls_findcode = dw_production_variance.GetItemString &
					(ll_firstrow, "fab_product_code")
End if

dw_production_variance.Sort()
dw_production_variance.GroupCalc()

dw_production_variance.SetFilter("")
dw_production_variance.Filter()

em_variance.getdata(ld_variance)

ls_filter = "((variance * 100) >= " + &
				string(ld_variance) + ")"
dw_production_variance.SetFilter(ls_filter)
dw_production_variance.Filter()

ll_RowCount = dw_production_variance.RowCount()
iw_frame.SetMicroHelp(String(ll_RowCount) + " rows displayed")

dw_production_variance.Sort()
dw_production_variance.GroupCalc()

ll_rtn = dw_production_variance.Find &
			("fab_product_code = '" + ls_findcode + "'", 1, ll_RowCount)
If ll_rtn > 0 Then
	dw_production_variance.ScrollToRow(ll_rtn)
End If

dw_production_variance.SetReDraw( True )

end subroutine

public subroutine wf_print ();
end subroutine

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_Enable('m_save')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

end on

event ue_postopen;call super::ue_postopen;iu_pas203 = create u_pas203
iu_ws_pas1 = Create u_ws_pas1
If Message.ReturnValue = -1 Then Close(This)

istr_error_info.se_app_name			= Message.nf_Get_App_ID()
istr_error_info.se_window_name		= "prodvar"
istr_error_info.se_user_id				= sqlca.userid

This.rb_actual.PostEvent(Clicked!)

This.PostEvent('ue_query')
end event

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

Destroy iu_ws_pas1
end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

on w_pas_prod_variance.create
int iCurrent
call super::create
this.dw_production_variance=create dw_production_variance
this.st_1=create st_1
this.st_4=create st_4
this.em_variance=create em_variance
this.rb_estimated=create rb_estimated
this.rb_actual=create rb_actual
this.gb_1=create gb_1
this.dw_plant_variance=create dw_plant_variance
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_production_variance
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.em_variance
this.Control[iCurrent+5]=this.rb_estimated
this.Control[iCurrent+6]=this.rb_actual
this.Control[iCurrent+7]=this.gb_1
this.Control[iCurrent+8]=this.dw_plant_variance
end on

on w_pas_prod_variance.destroy
call super::destroy
destroy(this.dw_production_variance)
destroy(this.st_1)
destroy(this.st_4)
destroy(this.em_variance)
destroy(this.rb_estimated)
destroy(this.rb_actual)
destroy(this.gb_1)
destroy(this.dw_plant_variance)
end on

on activate;call w_base_sheet_ext::activate;iw_frame.im_menu.mf_Disable('m_save')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end on

event ue_fileprint;call super::ue_fileprint;String						ls_temp

datawindowchild			ldwc_header, &
								ldwc_detail

DataStore					lds_print


lds_print = Create u_print_datastore
lds_print.DataObject = 'd_production_variance_print'

lds_print.GetChild("dw_header", ldwc_header)
lds_print.GetChild("dw_detail", ldwc_detail)


If rb_actual.Checked Then
	lds_print.Object.View.Text = rb_Actual.Text
Else
	lds_print.Object.View.Text = rb_estimated.Text
End if

dw_plant_variance.sharedata(ldwc_header)
dw_production_variance.sharedata(ldwc_detail)

//ls_temp = dw_plant_variance.object.datawindow.data
//ldwc_header.importstring(ls_temp)
//
//ls_temp = dw_production_variance.object.datawindow.data
//ldwc_detail.importstring(ls_temp)



ldwc_detail.Modify("Difference.Expression = '" + &
					dw_production_Variance.Object.Difference.Expression + "'")

ldwc_detail.Modify("Variance.Expression = '" + &
					dw_production_Variance.Object.Variance.Expression + "'")

lds_print.print()
end event

event resize;call super::resize;constant integer li_x		= 27
constant integer li_y		= 228
  
dw_production_variance.width	= newwidth - (30 + li_x)
dw_production_variance.height	= newheight - (30 + li_y)


end event

type dw_production_variance from u_base_dw_ext within w_pas_prod_variance
integer x = 5
integer y = 228
integer width = 2706
integer height = 1096
integer taborder = 0
string dataobject = "dw_production_variance"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

type st_1 from statictext within w_pas_prod_variance
integer x = 553
integer y = 108
integer width = 306
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Variance >="
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_pas_prod_variance
integer x = 1106
integer y = 108
integer width = 87
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "%"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_variance from editmask within w_pas_prod_variance
event ue_enchange pbm_enchange
integer x = 859
integer y = 96
integer width = 256
integer height = 92
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "##0"
boolean spin = true
double increment = 1
string minmax = "0~~100"
end type

on ue_enchange;wf_filter()
end on

on getfocus;Decimal			ld_value

This.GetData(ld_value)
This.SelectText(1,len(String(ld_value)))
end on

type rb_estimated from radiobutton within w_pas_prod_variance
integer x = 1938
integer y = 68
integer width = 722
integer height = 68
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Est Sched vs Act Prod"
end type

event clicked;dw_production_variance.Modify("difference.Expression = 'prod_sum - estimated_sum'")

dw_production_variance.Modify("variance.Expression = 'if( prod_sum=0 and estimated_sum=0 ,0, " + &
							"if( prod_sum=0, 1.00, if(estimated_sum=0,1.00,(abs(difference / " + &
							"estimated_sum)))))'")

end event

type rb_actual from radiobutton within w_pas_prod_variance
integer x = 1938
integer y = 140
integer width = 709
integer height = 68
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Act Sched vs Act Prod"
boolean checked = true
end type

event clicked;dw_production_variance.Modify("difference.Expression = 'prod_sum - actual_sum'")

dw_production_variance.Modify("variance.Expression = 'if( prod_sum=0 and actual_sum=0 ,0, " + &
							"if( prod_sum=0, 1.00, if(actual_sum=0,1.00,(abs(difference / " + &
							"actual_sum)))))'")

end event

type gb_1 from groupbox within w_pas_prod_variance
integer x = 1915
integer width = 786
integer height = 224
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Variance"
end type

type dw_plant_variance from u_base_dw_ext within w_pas_prod_variance
event ue_enchange pbm_enchange
integer y = 4
integer width = 1806
integer height = 168
integer taborder = 0
string dataobject = "dw_plant_variance"
boolean border = false
end type

on constructor;call u_base_dw_ext::constructor;InsertRow(0)
ib_updateable = False


end on

