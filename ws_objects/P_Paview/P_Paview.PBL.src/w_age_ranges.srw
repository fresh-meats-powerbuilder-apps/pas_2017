﻿$PBExportHeader$w_age_ranges.srw
$PBExportComments$Age Schedule Ranges
forward
global type w_age_ranges from w_netwise_sheet_ole
end type
type ole_age_ranges from olecustomcontrol within w_age_ranges
end type
end forward

global type w_age_ranges from w_netwise_sheet_ole
integer x = 233
integer y = 192
integer width = 2162
integer height = 1164
string title = "Age Ranges"
long backcolor = 79741120
ole_age_ranges ole_age_ranges
end type
global w_age_ranges w_age_ranges

type variables
s_error	istr_error_info
boolean	ib_OLE_Error 

end variables

forward prototypes
public subroutine wf_filenew ()
public subroutine wf_delete ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_deleterow ()
public function boolean wf_addrow ()
public function boolean wf_query_save_changes ()
end prototypes

public subroutine wf_filenew ();wf_addrow()
end subroutine

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_retrieve ();boolean lbln_Return

if ib_OLE_Error then return TRUE
	
istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "ole_age_ranges.Inquire"

If wf_query_save_changes() Then 
	SetPointer(HourGlass!)
	lbln_Return = ole_age_ranges.object.Inquire()
else
	lbln_Return = true
end if

return lbln_Return
end function

public function boolean wf_update ();if ib_OLE_Error then return TRUE

SetPointer(HourGlass!)
return ole_age_ranges.object.Save(true)



 
end function

public function boolean wf_deleterow ();if ib_OLE_Error then return TRUE

return ole_age_ranges.object.DeleteRow()
end function

public function boolean wf_addrow ();if ib_OLE_Error then return TRUE
return ole_age_ranges.object.InsertRow()
end function

public function boolean wf_query_save_changes ();if ib_OLE_Error then return TRUE

if ole_age_ranges.object.DataChanged = true then
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
		CASE 1	// Save Changes
			If ole_age_ranges.object.save(false) = FALSE Then
				RETURN FALSE // Update failed - do not close window
			End If
		CASE 2				 // Do not save changes
			Return TRUE
		CASE 3				 // Cancel the closing of window
			RETURN FALSE			
	END CHOOSE
end if

RETURN TRUE
end function

on w_age_ranges.create
int iCurrent
call super::create
this.ole_age_ranges=create ole_age_ranges
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_age_ranges
end on

on w_age_ranges.destroy
call super::destroy
destroy(this.ole_age_ranges)
end on

event ue_postopen;call super::ue_postopen;string	ls_server_suffix

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Age Ranges"
istr_error_info.se_user_id 		= sqlca.userid

if this.width  > 10 then
	ole_age_ranges.Width = this.width - 50
end if

if this.height > 10 then
	ole_age_ranges.Height = this.height - 150
end if	


ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

if ls_server_suffix = "" then
	ls_server_suffix = " "
end if 

if not ole_age_ranges.object.initializecontrol(sqlca.userid,sqlca.DBpass,ls_server_suffix,Message.nf_Get_App_ID(),"Age Ranges") then
	MessageBox(This.Title, "Initialization Failed. Please call Help desk 3133.", Exclamation!, OK!)
	close(this)
	
elseif not wf_retrieve()  then
	MessageBox(This.Title, "Inquire was unsuccessful, Please try again later", Information!, OK!)
	close(this)
	
end if


end event

event activate;call super::activate;If Not gw_base_frame.im_base_menu.m_file.m_save.enabled Then
	iw_frame.im_menu.mf_Disable('m_addrow')
	iw_frame.im_menu.mf_Disable('m_deleterow')
Else
	iw_frame.im_menu.mf_enable('m_addrow')
	iw_frame.im_menu.mf_enable('m_deleterow')
End if

iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
end event

event resize;if newwidth  > 100 then
	ole_age_ranges.Width = newwidth 
end if

if newheight > 200 then
	ole_age_ranges.Height = newheight 
end if
end event

event deactivate;iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
end event

type ole_age_ranges from olecustomcontrol within w_age_ranges
event messageposted ( ref string strinfomessage )
integer width = 2089
integer height = 1052
integer taborder = 10
boolean bringtotop = true
boolean border = false
long backcolor = 79741120
string binarykey = "w_age_ranges.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event messageposted;iw_frame.SetMicroHelp(strinfomessage)
end event

event getfocus;if ib_OLE_Error then return
ole_age_ranges.object.SetFocusBack()
end event

event error;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

if errornumber <> 20535 then
	lstr_rpc_error_info.se_app_name = "PAS"
	lstr_rpc_error_info.se_window_name = ""
	lstr_rpc_error_info.se_function_name = ""
	lstr_rpc_error_info.se_event_name = errorwindowmenu
	lstr_rpc_error_info.se_procedure_name = errorobject
	lstr_rpc_error_info.se_user_id = ""
	lstr_rpc_error_info.se_return_code = ""
	lstr_rpc_error_info.se_message = "[" + string(errornumber) + "] "  + errortext + "~nThe window will be closed."
	
	lstr_rpc_error_info.se_rval = 0
	lstr_rpc_error_info.se_commerror = 0
	lstr_rpc_error_info.se_commerrmsg = ""
	lstr_rpc_error_info.se_neterror = 0
	lstr_rpc_error_info.se_primaryerror = 0
	lstr_rpc_error_info.se_secondaryerror = 0
	lstr_rpc_error_info.se_neterrmsg = space(100)
	
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
end if

action = ExceptionIgnore! 
close(parent)
end event

event externalexception;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

lstr_rpc_error_info.se_app_name = "PAS"
lstr_rpc_error_info.se_window_name = ""
lstr_rpc_error_info.se_function_name = ""
lstr_rpc_error_info.se_event_name = parent.title
lstr_rpc_error_info.se_procedure_name = source
lstr_rpc_error_info.se_user_id = ""
lstr_rpc_error_info.se_return_code = ""
lstr_rpc_error_info.se_message = "[" + string(resultcode) + "] "  + "[" + string(exceptioncode) + "] "  + description + "~nThe window will be closed."

lstr_rpc_error_info.se_rval = 0
lstr_rpc_error_info.se_commerror = 0
lstr_rpc_error_info.se_commerrmsg = ""
lstr_rpc_error_info.se_neterror = 0
lstr_rpc_error_info.se_primaryerror = 0
lstr_rpc_error_info.se_secondaryerror = 0
lstr_rpc_error_info.se_neterrmsg = space(100)

openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")

action = ExceptionIgnore! 
close(parent)
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
09w_age_ranges.bin 
2200000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff000000030000000000000000000000000000000000000000000000000000000008bd391001c3af7c00000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004f77f5fe911d4155c0100519fdb9028020000000008bd391001c3af7c08bd391001c3af7c000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
2Fffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400002f3b000800034757f20affffffe00065005f00740078006e00650079007400001b2f000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
19w_age_ranges.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
