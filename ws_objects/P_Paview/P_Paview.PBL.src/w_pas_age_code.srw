﻿$PBExportHeader$w_pas_age_code.srw
forward
global type w_pas_age_code from w_base_child_ext
end type
type dw_age_code from u_base_dw_ext within w_pas_age_code
end type
type cb_cancel from u_base_commandbutton_ext within w_pas_age_code
end type
type cb_help from u_help_cb within w_pas_age_code
end type
end forward

global type w_pas_age_code from w_base_child_ext
int X=553
int Y=276
int Width=1198
int Height=1356
long BackColor=12632256
boolean MinBox=false
boolean MaxBox=false
dw_age_code dw_age_code
cb_cancel cb_cancel
cb_help cb_help
end type
global w_pas_age_code w_pas_age_code

event open;call super::open;// StringParm Will Have the All of the data for the window
// it will be in the format:
// type_indicator (I for Inventory, A for Available Amount) ~t Age Code or production date range
//  ~t Invt ~t Avail ~r~n
String	ls_data, &
			ls_ret, &
			ls_filter_string, &
			ls_indicator, &
			ls_plant_code, &
			ls_sort_string,ls_filter_date

u_string_functions		lu_strings
nvuo_pa_business_rules	lu_pa_rules


ls_data = Message.StringParm

ls_filter_date = lu_strings.nf_GetToken(ls_data, '~t')
ls_plant_code = lu_strings.nf_GetToken(ls_data, '~t')
ls_indicator = lu_strings.nf_GetToken(ls_data, '~t')

// remove the indicator and tab before importing
dw_age_code.ImportString(ls_data)

If Upper(ls_indicator) = 'I' Then
	If lu_pa_rules.uf_check_pasldtyp(ls_plant_code) Then
		This.Title = "Inventory By Production Date"
		dw_age_code.object.age_code_t.text = 'Production Date'
		ls_sort_string = "sort_date D"
	Else
		This.Title = "Inventory By Age Code"
		ls_sort_string = "age_code"
	End If
	ls_ret = dw_age_code.Modify("available_qty.Visible = 0")
	If Not iw_frame.iu_string.nf_IsEmpty(ls_ret) Then
		MessageBox("Modify", ls_ret)
	End if
	//dld change
//	ls_filter_string = "inventory_qty <> 0"
	ls_filter_string = "filter_date = date( '" + ls_filter_date + "') and inventory_qty <> 0" //"inventory_qty <> 0"
Else
	If lu_pa_rules.uf_check_pasldtyp(ls_plant_code) Then
		This.Title = "Available By Production Date"
		dw_age_code.object.age_code_t.text = 'Production Date'
		ls_sort_string = "sort_date D"
	Else
		This.Title = "Available By Age Code"
		ls_sort_string = "age_code"
	End If
	ls_ret =dw_age_code.Modify("inventory_qty.Visible = 0")
	If Not iw_frame.iu_string.nf_IsEmpty(ls_ret) Then
		MessageBox("Modify", ls_ret)
	End if
	ls_filter_string = "filter_date = date( '" + ls_filter_date + "') and available_qty <> 0" //"inventory_qty <> 0"
End if

dw_age_code.SetFilter('')
dw_age_code.Filter()
dw_age_code.SetFilter(ls_filter_string)
dw_age_code.Filter()
dw_age_code.SetSort(ls_sort_string)
dw_age_code.Sort()

end event

on w_pas_age_code.create
int iCurrent
call super::create
this.dw_age_code=create dw_age_code
this.cb_cancel=create cb_cancel
this.cb_help=create cb_help
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_age_code
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.cb_help
end on

on w_pas_age_code.destroy
call super::destroy
destroy(this.dw_age_code)
destroy(this.cb_cancel)
destroy(this.cb_help)
end on

type dw_age_code from u_base_dw_ext within w_pas_age_code
int X=9
int Y=8
int Width=1134
int Height=1068
int TabOrder=0
string DataObject="d_pa_view_age_code"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

type cb_cancel from u_base_commandbutton_ext within w_pas_age_code
int X=480
int Y=1104
int Width=265
int TabOrder=10
string Text="&Cancel"
boolean Default=true
boolean Cancel=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
end type

on clicked;call u_base_commandbutton_ext::clicked;Close(Parent)
end on

type cb_help from u_help_cb within w_pas_age_code
int X=786
int Y=1104
int Width=265
int TabOrder=20
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
end type

