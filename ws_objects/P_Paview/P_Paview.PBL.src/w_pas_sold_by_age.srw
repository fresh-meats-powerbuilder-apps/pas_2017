﻿$PBExportHeader$w_pas_sold_by_age.srw
forward
global type w_pas_sold_by_age from w_base_child_ext
end type
type dw_ages from u_base_dw_ext within w_pas_sold_by_age
end type
type cb_cancel from u_base_commandbutton_ext within w_pas_sold_by_age
end type
type cb_1 from u_help_cb within w_pas_sold_by_age
end type
end forward

global type w_pas_sold_by_age from w_base_child_ext
int X=553
int Y=276
int Width=1198
int Height=1356
boolean TitleBar=true
string Title="Sold by Age"
long BackColor=12632256
boolean MinBox=false
boolean MaxBox=false
dw_ages dw_ages
cb_cancel cb_cancel
cb_1 cb_1
end type
global w_pas_sold_by_age w_pas_sold_by_age

event open;call super::open;//The filter date will be the first field on the string then ~t then string of data to filter

Date		ldt_filter

Int		li_pos

String	ls_data, &
			ls_plant_code, &
			ls_sort_string
			
u_string_functions	lu_strings

nvuo_pa_business_rules	lu_pa_rules


ls_data = Message.StringParm

ls_plant_code = lu_strings.nf_GetToken(ls_data, '~t')

ldt_filter = Date(lu_strings.nf_GetToken(ls_data, '~t'))

dw_ages.ImportString(ls_data)

If lu_pa_rules.uf_check_pasldtyp(ls_plant_code) Then
	This.Title = "Sold By Production Date"
	dw_ages.object.age_code_t.text = 'Production Date'
	ls_sort_string = "sort_date D"
Else
	This.Title = "Sold By Age Code"
	ls_sort_string = "age_code"
End If

dw_ages.SetFilter("begin_date = Date('" + String(ldt_filter) + "')")
dw_ages.Filter()
dw_ages.SetSort(ls_sort_string)
dw_ages.Sort()

end event

on w_pas_sold_by_age.create
int iCurrent
call super::create
this.dw_ages=create dw_ages
this.cb_cancel=create cb_cancel
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ages
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.cb_1
end on

on w_pas_sold_by_age.destroy
call super::destroy
destroy(this.dw_ages)
destroy(this.cb_cancel)
destroy(this.cb_1)
end on

type dw_ages from u_base_dw_ext within w_pas_sold_by_age
int X=9
int Y=4
int Width=1138
int Height=1100
int TabOrder=0
string DataObject="d_pa_view_sold_by_age"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

type cb_cancel from u_base_commandbutton_ext within w_pas_sold_by_age
int X=530
int Y=1132
int TabOrder=10
string Text="&Cancel"
boolean Default=true
boolean Cancel=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
end type

on clicked;call u_base_commandbutton_ext::clicked;Close(Parent)
end on

type cb_1 from u_help_cb within w_pas_sold_by_age
int X=823
int Y=1132
int TabOrder=20
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
end type

