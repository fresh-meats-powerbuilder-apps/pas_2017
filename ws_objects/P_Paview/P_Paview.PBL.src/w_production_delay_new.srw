﻿$PBExportHeader$w_production_delay_new.srw
forward
global type w_production_delay_new from w_base_sheet_ext
end type
type dw_production_delay_new from u_base_dw_ext within w_production_delay_new
end type
type dw_division from u_division within w_production_delay_new
end type
end forward

global type w_production_delay_new from w_base_sheet_ext
integer x = 389
integer y = 468
integer width = 4144
integer height = 1843
string title = "Production Delay"
long backcolor = 67108864
dw_production_delay_new dw_production_delay_new
dw_division dw_division
end type
global w_production_delay_new w_production_delay_new

type variables
s_error	istr_error_info
String is_inquire_flag
Boolean ib_reinquire, &
			ib_close_this
Long			il_ChangedRow
u_ws_pas5		iu_ws_pas5

datawindowchild 			idddw_child_state, &
								idddw_child_status, &
								idddw_child, &
								dwc_temp, &
								ldwc_type, &
								idwc_prod_del
								
u_sect_functions		iu_sect_functions
nvuo_fab_product_code invuo_fab_product_code
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_addrow ()
public function boolean wf_update ()
public function boolean wf_deleterow ()
public subroutine wf_delete ()
public function boolean wf_validate (long al_row)
public function boolean wf_validateduplicate (long al_row)
end prototypes

public function boolean wf_retrieve ();String ls_division, &
		 ls_header, &
		 ls_input, &
		 ls_output, &
		 ls_div, &
		 ls_div_code, &
		 ls_protect_ind, ls_div_desc, ls_temp
		 
integer li_pos, &
		  li_start, &
		  li_len, li_count
long ll_total_count, ll_Row, ll_NbrRows

	 
u_string_functions lu_string

//If dw_division.AcceptText() = -1 Then return False

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "div_code", "")
dw_division.setItem(1, "division_code", ls_temp)
ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "div_desc", "")
dw_division.SetItem(1, "division_description", ls_temp)

dw_production_delay_new.ib_updateable = True
This.TriggerEvent('closequery') 
If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
if not message.StringParm = "" then
	ls_div = message.StringParm


li_pos = 1
li_start = 1

Do while li_pos > 0
	li_pos = Pos(ls_div, '>', li_start)
	if li_pos = 0 then 
		ls_division = Mid(ls_div, li_start)
	else 
		ls_division = Mid(ls_div, li_start, li_pos - li_start)
	end if
	
	ls_division = Trim(ls_division)
	if li_count = 0 then 
		dw_division.SetItem(1, "division_code", ls_division)
	end if
	
	if li_count = 1 then
		dw_division.SetItem(1, "division_description", ls_division)
	end if
	
	li_count++
	li_start = li_pos +1
Loop
end if
		
ls_div_desc = dw_division.GetItemString(1, "division_description")
If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Inquiring Database")
This.SetRedraw(False)
dw_production_delay_new.Reset()

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp16gr"
istr_error_info.se_message = Space(71)

dw_division.Modify("division_code.Protect = 1")
ls_div_code = dw_division.getitemstring(1, "division_code")
ls_header = 'I' + '~t' + ls_div_code

//RPC call//
If iu_ws_pas5.nf_pasp16gr(istr_error_info, ls_header, ls_input, ls_output) < 0 Then 
	This.SetRedraw(True)
	Return False
End If

ll_total_count = dw_production_delay_new.ImportString(ls_output)

If ll_total_count > 0 then
	SetMicroHelp(String(ll_total_count) + " Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End If


SetProfileString(iw_frame.is_UserINI, "PAS", "div_code", ls_div_code)
SetProfileString(iw_frame.is_UserINI, "PAS", "div_desc", ls_div_desc)

dw_production_delay_new.ResetUpdate()
This.SetRedraw(True)
dw_production_delay_new.SetFocus()


//return true
end function

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row
				
String		ls_ind, &
				ls_filter

If dw_production_delay_new.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_row = dw_production_delay_new.InsertRow(0)	

dw_production_delay_new.ScrollToRow(ll_row)
dw_production_delay_new.SetItem(ll_row, "update_flag", 'A')
dw_production_delay_new.SetColumn("out_prod")
dw_production_delay_new.SetFocus()
//dw_production_delay_new.Modify("out_prod.Protect = 0")
dw_production_delay_new.SetItem(ll_row, "out_protectind", 'N')
dw_production_delay_new.SetItem(ll_row, "out_startdate", today())
dw_production_delay_new.SetItem(ll_row, "out_enddate", Date('12/31/2999'))

This.SetRedraw(True)

//return true
end function

public function boolean wf_update ();integer li_counter

long ll_row, &
	   ll_rowcount, &
	   ll_NbrRows, &
	   ll_temp, & 
	   ls_flag, &
	   ll_modified_count
		
string ls_Update_string, &
		ls_header_string, &
		ls_output_string
		
If dw_production_delay_new.AcceptText() = -1 then return(False)
ll_temp = dw_production_delay_new.DeletedCount()

If dw_production_delay_new.ModifiedCount() + dw_production_delay_new.DeletedCount() <=0 then return (false)

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Updating the Database")
If Not IsValid(iu_ws_pas5) then
	iu_ws_pas5 = create u_ws_pas5
end if

ll_NbrRows = dw_production_delay_new.RowCount()
//if ll_NbrRows <= 0 and ll_temp <=0 then return( False )

//ll_NbrRows = dw_production_delay_new.RowCount( )

ll_row = 0

Do while ll_row <= ll_NbrRows
	ll_Row = dw_production_delay_new.GetNextModified(ll_row, Primary!)
	if ll_row > 0 then 
		If not wf_validate(ll_row) then 
			Return false
		end if
		If not wf_validateduplicate(ll_row) then
			Return false
		end if		
	else
		ll_Row = ll_NbrRows + 1
	end if
Loop 

//If not wf_validateduplicate(ll_NbrRows) then
//	return false
//end if

ls_header_string = 'U'
ls_update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_production_delay_new)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_pasp16gr"
istr_error_info.se_message = Space(71)

If iu_ws_pas5.nf_pasp16gr(istr_error_info, ls_header_string, ls_update_string, ls_output_string) < 0 Then 
	This.SetRedraw(True)
	Return False
End If

iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_production_delay_new.RowCount()
For li_counter = 1 to ll_rowcount
	dw_production_delay_new.SetItem(li_counter, 'update_flag', ' ')
Next

dw_production_delay_new.ResetUpdate()
dw_production_delay_new.SetFocus()
dw_production_delay_new.SetReDraw(True)

//Return( True )


end function

public function boolean wf_deleterow ();Boolean		lb_ret

Long			ll_row, ll_total_count

this.SetRedraw(False)

ll_row = dw_production_delay_new.GetRow()

if ll_row < 1 then 
	return true
end if

dw_production_delay_new.SetItem(ll_row, "update_flag", 'D')

dw_production_delay_new.SetFocus()
lb_ret = super::wf_deleterow()

dw_production_delay_new.SelectRow(0, False)
dw_production_delay_new.SelectRow(dw_production_delay_new.GetRow(), true)
this.SetRedraw(True)
return lb_ret

//ls_input = 'U'
//ls_update = dw_production_delay_new.GetItemString(ll_row, "out_prod") + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_state") + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_status") + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_plant") + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_delay_type") + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_method") + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_cutoff_time") + '~t' + String(dw_production_delay_new.GetItemNumber(ll_row, "out_delayed_periods")) + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_ship_early") + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_shift") + '~t' + String(dw_production_delay_new.GetItemDate(ll_row,  "out_startdate"), 'yyyy-mm-dd') + '~t' + String(dw_production_delay_new.GetItemDate(ll_row, "out_enddate"), 'yyyy-mm-dd') + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_ppdateind") + '~t' + dw_production_delay_new.GetItemString(ll_row, "out_protectind") + '~t' + dw_production_delay_new.GetItemString(ll_row, "update_flag")
//
//If iu_ws_pas5.nf_pasp16gr(istr_error_info, ls_input, ls_update, ls_output ) < 0 Then 
//	This.SetRedraw(True)
//	Return False
//End If
//
//
//
//dw_production_delay_new.Reset()
//ls_header = 'I' + '~t' + dw_division.getitemstring(1, "division_code")
//If iu_ws_pas5.nf_pasp16gr(istr_error_info, ls_header, '', ls_output ) < 0 Then 
//	This.SetRedraw(True)
//	Return False
//End If
//ll_total_count = dw_production_delay_new.ImportString(ls_output)
//
//If ll_total_count > 0 then
//	SetMicroHelp(String(ll_total_count) + " Rows Retrieved")
//Else
//	SetMicroHelp("0 Rows Retrieved")
//End If
//
//
//This.SetRedraw(True)

end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_validate (long al_row);String		ls_delay_type, &
			ls_method, &
			ls_ship_early, &
			ls_state, &
			ls_plant, &
			ls_product_code, &
			ls_shift, &
			ls_update_flag, &
			ls_searchstring
			
Integer 	li_delay_periods

Date		ldt_effective_date, ldt_expire_date, ldt_temp, ldt_found_effective_date, ldt_found_expire_date

Long 		ll_row_count, ll_rtn
If dw_production_delay_new.AcceptText() = -1 then return (false)

ll_row_count = dw_production_delay_new.RowCount()



ls_update_flag = dw_production_delay_new.GetItemString(al_row, "update_flag")
ldt_temp = dw_production_delay_new.GetItemDate(al_row, "out_startdate")
ls_delay_type = dw_production_delay_new.GetItemString(al_row, "out_delay_type")
ls_method = dw_production_delay_new.GetItemString(al_row, "out_method")
ls_ship_early = dw_production_delay_new.GetItemString(al_row, "out_ship_early")
ls_state = dw_production_delay_new.GetItemString(al_row, "out_state")
ls_plant = dw_production_delay_new.GetItemString(al_row, "out_plant")
ls_product_code = dw_production_delay_new.GetItemString(al_row, "out_prod")
ls_shift = dw_production_delay_new.GetItemString(al_row, "out_shift")
li_delay_periods = dw_production_delay_new.GetItemNumber(al_row, "out_delayed_periods")
ldt_effective_date = dw_production_delay_new.GetItemDate(al_row, "out_startdate")
ldt_expire_date = dw_production_delay_new.GetItemDate(al_row, "out_enddate")

If IsNull(ldt_effective_date) or ldt_temp <= Date('00/00/0000') Then
	MessageBox("Effective Date", "Please enter a Effective Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_startdate")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
End if

If ldt_effective_date < Today() and ls_update_flag = 'A' then
	iw_frame.SetMicroHelp("Effective Date must be greater than Current Day")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_startdate")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If ldt_effective_date > ldt_expire_date and ls_update_flag = 'A' then 
		iw_frame.SetMicroHelp("Effective date should be less than expire date")
		This.SetRedraw(False)
		dw_production_delay_new.ScrollToRow(al_row)
		dw_production_delay_new.Setcolumn("out_startdate")
		dw_production_delay_new.SetFocus()
		This.SetRedraw(True)
		Return False
	end if
End if


If IsNull(ldt_expire_date) or ldt_expire_date <= Date('00/00/0000') Then
	MessageBox("Expire Date", "Please enter a Expire Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_enddate")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
End if
If ldt_expire_date < Today() and ls_update_flag = 'A' then
	iw_frame.SetMicroHelp("Expire Date must be greater than Current Date")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_enddate")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If ldt_expire_date < ldt_effective_date and ls_update_flag = 'A' then 
		iw_frame.SetMicroHelp("Expire date must be equal or greater than Effective Date")
		This.SetRedraw(False)
		dw_production_delay_new.ScrollToRow(al_row)
		dw_production_delay_new.Setcolumn("out_enddate")
		dw_production_delay_new.SetFocus()
		This.SetRedraw(True)
		Return False
	end if
End if


If IsNull(ls_delay_type) then
	iw_frame.SetMicroHelp("A valid Delay Type is required")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_delay_type")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
end if

If IsNull(ls_method) then
	iw_frame.SetMicroHelp("A valid Method Type is required")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_method")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
end if

If IsNull(ls_ship_early) then
	iw_frame.SetMicroHelp("A valid ship early selection is required")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_ship_early")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
end if

If IsNull(ls_state) then
	iw_frame.SetMicroHelp("A valid state code is required")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_state")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
end if


If IsNull(ls_plant) then
	iw_frame.SetMicroHelp("A valid plant code is required")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_plant")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
end if

//If IsNull(ls_shift) then
//	iw_frame.SetMicroHelp("A valid shift is required")
//	This.SetRedraw(False)
//	dw_production_delay_new.ScrollToRow(al_row)
//	dw_production_delay_new.SetColumn("out_shift")
//	dw_production_delay_new.SetFocus()
//	This.SetRedraw(True)
//	Return False
//end if

		
If IsNull(li_delay_periods) then
	iw_frame.SetMicroHelp("A valid shift is required")
	This.SetRedraw(False)
	dw_production_delay_new.ScrollToRow(al_row)
	dw_production_delay_new.SetColumn("out_delayed_periods")
	dw_production_delay_new.SetFocus()
	This.SetRedraw(True)
	Return False
else 
	if li_delay_periods >3 or li_delay_periods < 0 then
		iw_frame.SetMicroHelp("Delay Periods must be a number between 0 and 3") 
		This.SetRedraw(False)
		dw_production_delay_new.ScrollToRow(al_row)
		dw_production_delay_new.SetColumn("out_delayed_periods")
		dw_production_delay_new.SetFocus()
		This.SetRedraw(True)
		Return False
	end if
end if

If not invuo_fab_product_code.uf_check_product(ls_product_code) then
	If invuo_fab_product_code.ib_error_occurred then
		iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
		Return false
	else
		iw_frame.SetMicroHelp(ls_product_code + " is an invalid product code")
		dw_production_delay_new.ScrolltoRow(al_row)
		dw_production_delay_new.SetColumn("out_prod")
		dw_production_delay_new.SetFocus()
		Return False
	End if
End if

//ls_searchstring = "product_code = " ls_product_code + " and state = " + ls_state + " and plant= " + ls_plant + " and delay type = " + ls_delay_type 
//
//Choose case al_row
//	Case 1
//		ll_rtn = dw_production_delay_new.Find(ls_searchstring, al_row + 1, ll_row_count)
//	Case 2 to (ll_row_count - 1)
//		ll_rtn = dw_production_delay_new.Find(ls_searchstring, al_row - 1, 1)
//		If ll_rtn = 0 then ll_rtn = dw_production_delay_new.Find(ls_searchstring, al_row + 1, ll_row_count)
//	Case ll_row_count
//		ll_rtn = dw_production_delay_new.Find(ls_searchstring, al_row - 1, 1)
//End choose
//
//If ll_rtn > 0 then
//	ldt_found_effective_date = dw_production_delay_new.GetItemDate(ll_rtn, "out_startdate")
//	ldt_found_expire_date = dw_production_delay_new.GetItemDate(ll_rtn, "out_enddate")
//	
//	If (ldt_found_effective_date >= ldt_effective_date and ldt_found_effective_date <= ldt_expire_date) Then
//		iw_frame.SetMicroHelp("The Effective date and Expire date cannot overlap with the Effective date and Expire dates in another row with the same product, state, plant, and delay type.")
//	elseif (ldt_found_expire_date >= ldt_effective_Date and ldt_found_expire_Date <= ldt_expire_Date) then
//		iw_frame.SetMicroHelp("The Effective date and Expire date cannot overlap with the Effective date and Expire dates in another row with the same product, state, plant, and delay type.")
//	elseif (ldt_effective_date >= ldt_found_effective_date and ldt_effective_date <= ldt_found_expire_date) Then
//		iw_frame.SetMicroHelp("The Effective date and Expire date cannot overlap with the Effective date and Expire dates in another row with the same product, state, plant, and delay type.")
//	elseif(ldt_expire_Date >= ldt_found_effective_date and ldt_expire_Date <= ldt_found_expire_date) Then
//		iw_frame.SetMicroHelp("The Effective date and Expire date cannot overlap with the Effective date and Expire dates in another row with the same product, state, plant, and delay type.")
//	elseif (ldt_found_effective_date = ldt_effective_Date) then
//		iw_frame.SetMicroHelp("The Effective date and Expire date cannot overlap with the Effective date and Expire dates in another row with the same product, state, plant, and delay type.")
//	else 
//		ll_rtn = 0
//	end if
//end if
//If ll_rtn > 0 then
//	dw_production_delay_new.SetRedraw(False)
//	dw_production_delay_new.ScrollToRow(al_row)
//	dw_production_delay_new.SetColumn("out_startdate")
//	dw_production_delay_new.SetFocus()
//	dw_production_delay_new.SetRedraw(True)
//	Return false
//end if
//		
//		


//Return true


end function

public function boolean wf_validateduplicate (long al_row);String 	ls_product_code, &
			ls_state, &
			ls_status, &
			ls_plant, &
			ls_delay_type, &
			ls_compare_string, &
			ls_row_string
long 		ll_row_count, ll_count

Date		ldt_effective_date, ldt_expire_date, ldt_temp, ldt_found_effective_date, ldt_found_expire_date
			
ll_row_count = dw_production_delay_new.RowCount()
ls_product_code = dw_production_delay_new.GetItemString(al_row, "out_prod")
ls_state = dw_production_delay_new.GetItemString(al_row, "out_state")
ls_status = dw_production_delay_new.GetItemString(al_row, "out_status")
ls_plant = dw_production_delay_new.GetItemString(al_row, "out_plant")
ls_delay_type = dw_production_delay_new.GetItemString(al_row, "out_delay_type")
ldt_effective_date = dw_production_delay_new.GetItemDate(al_row, "out_startdate")
ldt_expire_date = dw_production_delay_new.GetItemDate(al_row, "out_enddate")


// jxr - 2019-02-19 added product status to string and made sure to trim fields
//ls_row_string = ls_product_code +'~t' + trim(ls_state) + '~t' + ls_plant +'~t' + trim(ls_delay_type)
ls_row_string = trim(ls_product_code) +'~t' + trim(ls_state) + '~t' + trim(ls_status) + '~t' + ls_plant +'~t' + trim(ls_delay_type)
ll_count = 1
Do while ll_count < ll_row_count
//	ls_compare_string = trim(dw_production_delay_new.GetItemString(ll_count, "out_prod")) + '~t' + dw_production_delay_new.GetItemString(ll_count, "out_state") + '~t' + dw_production_delay_new.GetItemString(ll_count, "out_plant") + '~t' + trim(dw_production_delay_new.GetItemString(ll_count, "out_delay_type"))
	ls_compare_string = trim(dw_production_delay_new.GetItemString(ll_count, "out_prod")) + '~t' + trim(dw_production_delay_new.GetItemString(ll_count, "out_state")) + '~t' + trim(dw_production_delay_new.GetItemString(ll_count, "out_status")) + '~t' + dw_production_delay_new.GetItemString(ll_count, "out_plant") + '~t' + trim(dw_production_delay_new.GetItemString(ll_count, "out_delay_type"))

//	if ls_row_string = ls_compare_string then 
	if ls_row_string = ls_compare_string and ll_count <> al_row then 
		// jxr - 2019-02-19 added code to check overlapping date range.
		ldt_found_effective_date = dw_production_delay_new.GetItemDate(ll_count, "out_startdate")
		ldt_found_expire_date = dw_production_delay_new.GetItemDate(ll_count, "out_enddate")		
		If (ldt_effective_date >= ldt_found_effective_date and ldt_effective_date <= ldt_found_expire_date) or (ldt_expire_date >= ldt_found_effective_Date and ldt_expire_date <= ldt_found_expire_Date) or &
		   (ldt_found_effective_date >= ldt_effective_date and ldt_found_effective_date <= ldt_expire_date) or (ldt_found_expire_date >= ldt_effective_Date and ldt_found_expire_date <= ldt_expire_Date)  then

			iw_frame.SetMicroHelp("There are overlapping Effective/Expire Dates")
			This.SetRedraw(False)
			dw_production_delay_new.ScrollToRow(al_row)
			dw_production_delay_new.SetColumn("out_prod")
			dw_production_delay_new.SetFocus()
			This.SetRedraw(True)
			Return False
		end if
	end if
	ll_count ++
Loop

return true
end function

on w_production_delay_new.create
int iCurrent
call super::create
this.dw_production_delay_new=create dw_production_delay_new
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_production_delay_new
this.Control[iCurrent+2]=this.dw_division
end on

on w_production_delay_new.destroy
call super::destroy
destroy(this.dw_production_delay_new)
destroy(this.dw_division)
end on

event ue_postopen;call super::ue_postopen;String ls_temp
If dw_production_delay_new.RowCount() = 0 Then dw_production_delay_new.InsertRow(0)
dw_production_delay_new.ib_updateable = True
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "proddelay"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_production_delay_new_inq'
iu_ws_pas5 = Create u_ws_pas5


dw_production_delay_new.GetChild("out_state", idddw_child_state)

idddw_child_state.SetTransObject(SQLCA)
idddw_child_state.Retrieve("PRDSTATE")
idddw_child_state.SetSort("type_code")
idddw_child_state.Sort()
dw_production_delay_new.InsertRow(0)

dw_production_delay_new.GetChild("out_status", idddw_child_status)

idddw_child_status.SetTransObject(SQLCA)
idddw_child_status.Retrieve("PRODSTAT")
idddw_child_status.SetSort("type_code")
idddw_child_status.Sort()

dw_production_delay_new.GetChild('out_plant', dwc_temp)
If IsValid(dwc_temp) Then
	dwc_temp.SetTransObject(SQLCA)
	dwc_temp.Retrieve()
	dwc_temp.InsertRow(1)
	dwc_temp.SetItem(1, 'location_code',  'ALL')
	dwc_temp.SetItem(1, 'location_name', 'ALL PLANTS')
	dwc_temp.SetSort("location_code")
End if

dw_production_delay_new.GetChild('out_ship_early', dwc_temp)
dwc_temp.SetTransObject(SQLCA)
dwc_temp.Retrieve("PPS MPCT")


dw_production_delay_new.GetChild("out_shift", ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("SHIFTALL")
dw_production_delay_new.InsertRow(0)


dw_production_delay_new.GetChild('out_ppdateind', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PPS TOTL")


dw_production_delay_new.GetChild('out_delay_type', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PADLYTYP")

dw_production_delay_new.GetChild('out_method', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PADLYMTH")

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "div_code", "")
dw_division.setItem(1, "division_code", ls_temp)
ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "div_desc", "")
dw_division.SetItem(1, "division_description", ls_temp)



wf_retrieve()
end event

event open;call super::open;If dw_production_delay_new.RowCount() = 0 Then dw_production_delay_new.InsertRow(0)
//dw_1.InsertRow(0)

end event

event close;call super::close;dw_division.SetItem(1,"division_code", "")
Destroy u_ws_pas5
end event

type dw_production_delay_new from u_base_dw_ext within w_production_delay_new
integer x = 40
integer y = 141
integer width = 3950
integer height = 1488
integer taborder = 20
string dataobject = "d_prod_delay_new"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;dwItemStatus		le_RowStatus

long	ll_char_row, &
		ll_source_row, &
		ll_plant_row, ll_delay_periods
		
string ls_temp, ls_columnname, ls_prod, ls_update

nvuo_pa_business_rules u_rule
ls_columnname = dwo.name
ll_source_row = GetRow()
dw_production_delay_new.AcceptText()
Choose Case ls_columnname
	case 'out_prod'
		ls_prod = dw_production_delay_new.GetItemString(ll_source_row, "out_prod")
		If not invuo_fab_product_code.uf_check_product(ls_prod) then
			If invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
			//Return 1
			else
				iw_frame.SetMicroHelp(ls_prod + " is an invalid product code")
				dw_production_delay_new.ScrolltoRow(ll_source_row)
				dw_production_delay_new.SetColumn("out_prod")
				dw_production_delay_new.SetFocus()
				//Return 1
			End if
		End if
	case 'out_delayed_periods'
		ll_delay_periods =  dw_production_delay_new.GetItemNumber(ll_source_row, "out_delayed_periods")
//		If IsNull(ll_delay_periods) then
//			iw_frame.SetMicroHelp("A valid shift is required")
//			This.SetRedraw(False)
//			dw_production_delay_new.ScrollToRow(ll_source_row)
//			dw_production_delay_new.SetColumn("out_delayed_periods")
//			dw_production_delay_new.SetFocus()
//			This.SetRedraw(True)
//			Return 1
//		else 
			if ll_delay_periods >3 or ll_delay_periods < 0 then
				iw_frame.SetMicroHelp("Delay Periods must be a number between 0 and 3") 
				This.SetRedraw(False)
				dw_production_delay_new.ScrollToRow(ll_source_row)
				dw_production_delay_new.SetColumn("out_delayed_periods")
				dw_production_delay_new.SetFocus()
				This.SetRedraw(True)
			//	Return 1
			end if
//		end if
		
		
		
		
end choose


IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

ls_update = dw_production_delay_new.GetItemString(ll_source_row, "update_flag")



//dw_production_delay_new.ib_updateable = true
//iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

//return 0
end event

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

IF row > 0 Then
	If il_last_clicked_row = 0 and row <> 1 Then
		il_last_clicked_row = This.GetSelectedRow(0)
		If il_last_clicked_row = 0 Then
			il_last_clicked_row = 1
		End If
	End If
	
	If row <> il_last_clicked_row Then 
		SelectRow(row, TRUE)
		SelectRow(il_last_clicked_row,FALSE)
	End If
	
	This.SetRow(Row)
End If
il_ChangedRow = row
il_last_clicked_row = row
end event

type dw_division from u_division within w_production_delay_new
integer x = 508
integer y = 42
integer width = 1792
integer height = 90
integer taborder = 10
end type

event itemchanged;call super::itemchanged;return 2
end event

