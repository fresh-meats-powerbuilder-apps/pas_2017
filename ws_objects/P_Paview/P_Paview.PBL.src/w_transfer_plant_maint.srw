﻿$PBExportHeader$w_transfer_plant_maint.srw
$PBExportComments$ibdkdld
forward
global type w_transfer_plant_maint from w_base_sheet_ext
end type
type dw_sort from u_sort_radio within w_transfer_plant_maint
end type
type dw_1 from u_base_dw_ext within w_transfer_plant_maint
end type
end forward

global type w_transfer_plant_maint from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 4663
integer height = 1331
long backcolor = 67108864
dw_sort dw_sort
dw_1 dw_1
end type
global w_transfer_plant_maint w_transfer_plant_maint

type variables
Long		il_SelectedColor, &
		il_SelectedTextColor, &
		il_rec_count
s_error		istr_error_info

string		is_update_string


datastore		ids_tranfer_plant_list
u_dwselect	iu_dwselect
u_pas201		iu_pas201
u_ws_pas4	iu_ws_pas4

date		idt_pa_range_date

end variables

forward prototypes
public subroutine wf_dest_sort ()
public function boolean wf_fillplantdddw ()
public function boolean wf_retrieve ()
public subroutine wf_ship_sort ()
public subroutine wf_filenew ()
public subroutine wf_delete ()
public function boolean wf_addrow ()
public function boolean wf_deleterow ()
public function boolean wf_update ()
public function boolean wf_validate (long al_row)
public function boolean wf_special_check (integer as_row, string as_itemchanged, ref string as_message)
public function boolean wf_fill_transfer_ind ()
public function boolean wf_update_modify (long al_row, character ac_action, string as_buffer)
public subroutine wf_set_pa_range_date ()
end prototypes

public subroutine wf_dest_sort ();DataWindowChild		ldwc_code1, &
							ldwc_descr1, &
							ldwc_code2, &
							ldwc_descr2


dw_1.GetChild('code1', ldwc_code1)
dw_1.GetChild('code1_descr', ldwc_descr1)
ldwc_code1.ShareData(ldwc_descr1)

dw_1.GetChild('code2', ldwc_code2)
dw_1.GetChild('code2_descr', ldwc_descr2)
ldwc_code2.ShareData(ldwc_descr2)

dw_1.setsort("code2 A, code1 A")
dw_1.sort()

dw_1.SetColumn ('code2')
end subroutine

public function boolean wf_fillplantdddw ();DataWindowChild 	dwch_plant1_codes, &
						dwch_plant1_descr, &
						dwch_plant2_codes, &
						dwch_plant2_descr
integer rtncode



rtncode = dw_1.GetChild('code1', dwch_plant1_codes)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Code1 not a DataWindowChild") 
	return false
end if	
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
dwch_plant1_codes.SetTransObject(SQLCA)
dwch_plant1_codes.Retrieve()

rtncode = dw_1.GetChild('code1_descr', dwch_plant1_descr)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Code1_descr not a DataWindowChild") 
	return false
end if
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
dwch_plant1_descr.SetTransObject(SQLCA)
dwch_plant1_descr.Retrieve()

rtncode = dw_1.GetChild('code2', dwch_plant2_codes)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Code2 not a DataWindowChild") 
	return false
end if
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
dwch_plant2_codes.SetTransObject(SQLCA)
dwch_plant2_codes.Retrieve()

rtncode = dw_1.GetChild('code2_descr', dwch_plant2_descr)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Code2_descr not a DataWindowChild") 
	return false
end if
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
dwch_plant2_descr.SetTransObject(SQLCA)
dwch_plant2_descr.Retrieve()

return true
end function

public function boolean wf_retrieve ();string 					ls_return_data
long						ll_rec_count, ll_count
u_string_functions	lu_string



This.TriggerEvent('closequery') 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp90br_inq_trans_dest"
istr_error_info.se_message = Space(71)
dw_1.Setredraw(False)
dw_1.reset()

/*If iu_pas201.nf_pasp90br_inq_valid_plant_transfer(istr_error_info, & 
										ls_return_data) < 0 Then
										This.SetRedraw(True) 
										Return False
End If										
*/
If iu_ws_pas4.NF_PASP90FR(istr_error_info, & 
										ls_return_data) < 0 Then
										This.SetRedraw(True) 
										Return False
End If		



/*ls_return_data = 	'006~t004~r~n' + &
						'006~t069~r~n' + &
						'006~t002~r~n' + &
						'006~t003~r~n' + &
						'006~t005~r~n' + &
						'018~t069~r~n' + &
						'018~t002~r~n' + &
						'018~t003~r~n' + &
						'057~t006~r~n' + &
						'057~t009~r~n' + &
						'057~t011~r~n' + &
						'057~t086~r~n' + &
						'011~t013~r~n' + &
						'011~t014~r~n' + &
						'004~t086~r~n' + &
						'004~t016~r~n' + &
						'016~t069~r~n'*/	
//MessageBox('Return Data',string(ls_weekly_data))
If Not lu_string.nf_IsEmpty(ls_return_data) Then
	ll_rec_count = dw_1.ImportString(ls_return_data)
	If ll_rec_count > 0 Then 
		iw_frame.SetMicroHelp(String(ll_rec_count) + " Rows Retrieved")
		
		For ll_count = 1 to ll_rec_count
			dw_1.setitem(ll_count,"code1_descr",dw_1.getitemstring(ll_count,"code1"))
			dw_1.setitem(ll_count,"code2_descr",dw_1.getitemstring(ll_count,"code2"))
			dw_1.SetItem(ll_count,'protect','1') 
		Next
		If dw_sort.uf_get_choice() = "S" then
			wf_ship_sort()
		Else
			wf_dest_sort()
		End if	
	End if 
End If
If ll_rec_count > 0 Then 
	dw_1.SetRow(1)
Else
	iw_frame.SetMicroHelp("0 Rows Retrieved")
End if 
dw_1.SetFocus()
dw_1.ResetUpdate()
dw_1.Setredraw(True)
Return True

end function

public subroutine wf_ship_sort ();DataWindowChild		ldwc_code1, &
							ldwc_descr1, &
							ldwc_code2, &
							ldwc_descr2


dw_1.GetChild('code1', ldwc_code1)
dw_1.GetChild('code1_descr', ldwc_descr1)
ldwc_code1.ShareData(ldwc_descr1)

dw_1.GetChild('code2', ldwc_code2)
dw_1.GetChild('code2_descr', ldwc_descr2)
ldwc_code2.ShareData(ldwc_descr2)

dw_1.setsort("code1 A, code2 A")
dw_1.sort()

dw_1.SetColumn ('code1')
end subroutine

public subroutine wf_filenew ();wf_addrow()
end subroutine

public subroutine wf_delete ();wf_deleteRow()
end subroutine

public function boolean wf_addrow ();long 				ll_rownum, &
					ll_addrow
dwitemstatus	lis_temp
String			ls_sort,ls_column

This.setredraw(False)
//check to see if there is a row selected
ll_rownum = dw_1.GetSelectedRow(0)
ls_sort = dw_sort.GetItemString(1,1)

IF ll_rownum = 0 Then // There is no row selected
	ll_rownum = dw_1.RowCount()
	// If table is empty
	If ll_rownum = 0 Then
		Super:: wf_AddRow()
			dw_1.SetItem(1,'protect','0') 

			If ls_sort = 'S' Then
				ls_column = 'code1'
			Else
				ls_column = 'code2'
			End If
	Else
		If dw_1.GetItemString(ll_rownum,'code1') > '   ' Then
			Super:: wf_addrow()
			dw_1.SetItem(dw_1.RowCount(),'protect','0') 
			If ls_sort = 'S' Then
				ls_column = 'code1'
			Else
				ls_column = 'code2'
			End If
		Else
			//rev#01 set column
			ls_column = 'code1'
			If dw_sort.getitemstring(1,1) = "S" Then
				iw_frame.SetMicroHelp('You must fill in this Ship Plant before trying to add another Ship Plant')
			Else
				iw_frame.SetMicroHelp('You must fill in this Destination Plant before trying to add another Destination Plant')
			End If
		End If
	End If
// There is a selected row	
Else
	ll_addrow = dw_1.insertrow(ll_rownum + 1)
	dw_1.SetItem(ll_addrow,'protect','0') 

	If ls_sort = "S" Then
		dw_1.setitem(ll_addrow, "code1",dw_1.getitemstring(ll_rownum, "code1"))
		dw_1.setitem(ll_addrow, "code1_descr",dw_1.getitemstring(ll_rownum, "code1"))
		ls_column = 'code1'
	Else
		dw_1.setitem(ll_addrow, "code2",dw_1.getitemstring(ll_rownum, "code2"))
		dw_1.setitem(ll_addrow, "code2_descr",dw_1.getitemstring(ll_rownum, "code2"))
		ls_column = 'code2'
	End if
End if

dw_1.setitem(ll_addrow, "transfer_ind", 'N')

dw_1.setitem(ll_addrow, "add_ship_date_1", date('0001-01-01'))
dw_1.setitem(ll_addrow, "add_ship_date_2", date('0001-01-01'))
dw_1.setitem(ll_addrow, "add_ship_date_3", date('0001-01-01'))
dw_1.setitem(ll_addrow, "add_ship_date_4", date('0001-01-01'))
dw_1.setitem(ll_addrow, "add_ship_date_5", date('0001-01-01'))

dw_1.SetColumn (ls_column)
dw_1.SetFocus()

This.SetRedraw(True)

Return True
end function

public function boolean wf_deleterow ();Long 		ll_rownum

ll_rownum = dw_1.GetSelectedRow(0)
IF ll_rownum = 0 Then
	Beep(1)
	iw_frame.SetMicroHelp('You must have a row selected to delete -- Please select a row and try again')
	Return False
Else
	Super:: wf_deleterow()
End If	

Return True
end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name
dwItemStatus	ldwis_status			
u_string_functions u_string



IF dw_1.AcceptText() = -1 Then 
	Return False
End if

ll_modrows = dw_1.ModifiedCount()
ll_delrows = dw_1.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0 Then 
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if
SetMicroHelp("Wait, Updating ...")
SetPointer(HourGlass!)

This.SetRedraw(False)
is_update_string = ""
ll_row = 0
dw_1.SelectRow(0,False)

For ll_count = 1 To ll_modrows
	ll_Row = dw_1.GetNextModified(ll_Row, Primary!) 
	If Not wf_validate(ll_row) Then 
		This.SetRedraw(True)
		Return False
	End If
	
	ldwis_status = dw_1.GetItemStatus(ll_row,0, Primary!)
	IF ldwis_status = DataModified! Then
		lc_status_ind = 'U'
		If Not This.wf_update_modify(ll_row, lc_status_ind,'P') Then 
			This.SetRedraw(True)
			Return False
		End If
	Else
		lc_status_ind = 'A'
		If Not This.wf_update_modify(ll_row, lc_status_ind,'P') Then 
			This.SetRedraw(True)
			Return False
		End If
	End If
	dw_1.SetItem(ll_row,'protect','1') 
Next

For ll_count = 1 To ll_delrows 
	lc_status_ind = 'D'
	If Not This.wf_update_modify(ll_count, lc_status_ind,'D') Then 
		This.SetRedraw(True)
		Return False
	End if
Next

If il_rec_count > 0 Then
	/*IF Not iu_pas201.nf_pasp91br_upd_valid_plant_transfer(istr_error_info, &
												is_update_string) THEN 
		This.SetRedraw(True)
		Return False
	End if
End If
*/
	IF Not iu_ws_pas4.NF_PASP91FR(istr_error_info, &
			is_update_string) THEN Return False
	il_rec_count = 0
	is_update_string = ""
END IF


iw_frame.SetMicroHelp("Modification Successful")
dw_1.ResetUpdate()
This.SetRedraw(True)

Return( True )


end function

public function boolean wf_validate (long al_row);String						ls_temp,ls_temp2
u_string_functions		lu_string
 

ls_temp = dw_1.GetItemString(al_row, "code1")
If lu_string.nf_IsEmpty(ls_temp) or IsNull(ls_temp) Then
		dw_1.ScrollToRow(al_row)
		dw_1.SetColumn("code1")
		iw_frame.SetMicroHelp('Plant is a is a required field')
		Return False
End If

ls_temp2 = dw_1.GetItemString(al_row, "code2")
If lu_string.nf_IsEmpty(ls_temp2) or IsNull(ls_temp2) Then
		dw_1.ScrollToRow(al_row)
		dw_1.SetColumn("code2")
		iw_frame.SetMicroHelp('Plant is a required field')
		Return False
End If

If ls_temp = ls_temp2 Then
	dw_1.ScrollToRow(al_row)
	dw_1.SelectRow (al_row, True)
	iw_frame.SetMicroHelp("The Ship Plant and Destination Plant can't be the same")
	Return False
End If

// pjm 09/16/2014 - added verification for 2 new columns

ls_temp = String(dw_1.GetItemDate(al_row, "pt_cutoff_date"))
If lu_string.nf_IsEmpty(ls_temp) or IsNull(ls_temp) or NOT IsDate(ls_temp) Then
		dw_1.ScrollToRow(al_row)
		dw_1.SetColumn("pt_cutoff_date")
		iw_frame.SetMicroHelp('PT Cutoff Date is a required field')
		Return False
End If

ls_temp = String(dw_1.GetItemString(al_row, "pt_cutoff_period"))
If lu_string.nf_IsEmpty(ls_temp) or IsNull(ls_temp) Then
		dw_1.ScrollToRow(al_row)
		dw_1.SetColumn("pt_cutoff_period")
		iw_frame.SetMicroHelp('PT Cutoff Period is a required field')
		Return False
End If

Return True

end function

public function boolean wf_special_check (integer as_row, string as_itemchanged, ref string as_message);Long		ll_rowcount,ll_row
String 	ls_ship_plant,ls_dest_plant,ls_choice,ls_temp_plant

ll_rowcount = dw_1.RowCount() 

CHOOSE CASE as_itemchanged
	CASE 'transfer_ind'
		ls_choice = dw_sort.uf_get_choice()
		If ls_choice = "S" Then
			ls_ship_plant = dw_1.GetitemString(as_row,'code1')
			ls_dest_plant = dw_1.GetitemString(as_row,'code2')
		Else
			ls_ship_plant = dw_1.GetitemString(as_row,'code2')
			ls_dest_plant = dw_1.GetitemString(as_row,'code1')
		End If
		ls_ship_plant = dw_1.GetitemString(as_row,'code1')
		ls_dest_plant = dw_1.GetitemString(as_row,'code2')
		FOR ll_row = 1 TO ll_rowcount 
			If ll_row <> as_row Then
				ls_temp_plant = dw_1.GetItemString(ll_row,'code1')
				If ls_temp_plant = ls_dest_plant Then
					If dw_1.GetItemString(ll_row,'transfer_ind') = "Y" Then
						as_message = 'Plants can not be a recipient of Transfers while Shipping Transfers............'
						Return False
					End IF
				End If
				// chech the other side
				ls_temp_plant = dw_1.GetItemString(ll_row,'code2')
				If ls_temp_plant = ls_ship_plant Then
					If dw_1.GetItemString(ll_row,'transfer_ind') = "Y" Then
						as_message = 'Plants can not Ship Transfers while being a recipient of Transfers............'
						Return False
					End if
				End IF
			End if
		NEXT
//	CASE ELSE
//		<statementblock>
END CHOOSE

as_message = "Ready"
Return True
end function

public function boolean wf_fill_transfer_ind ();DataWindowChild 	dwch_transfer_ind

integer rtncode



rtncode = dw_1.GetChild('transfer_ind', dwch_transfer_ind)
IF rtncode = -1 THEN 
	MessageBox( "Error", "transfer_ind not a DataWindowChild") 
	return false
end if	
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
dwch_transfer_ind.SetTransObject(SQLCA)
dwch_transfer_ind.Retrieve()

return true
end function

public function boolean wf_update_modify (long al_row, character ac_action, string as_buffer);String						ls_describe,ls_user_id,ls_sort,ls_temp
Long							ll_temp, ll_time
Time 							lt_time
Date							ldt_date
DWBuffer						ldwb_buffer

ls_user_id = Upper(sqlca.userid)

CHOOSE CASE as_buffer
	CASE 'D'
		ldwb_buffer = Delete!
	CASE 'P'
		ldwb_buffer = Primary!
END CHOOSE


is_update_string += ac_action + '~t'
ls_sort = dw_sort.GetItemString(1,1)
//If ls_sort = "S" Then
	is_update_string += dw_1.GetItemString(al_row,'code1',ldwb_buffer,False) + '~t'
	is_update_string += dw_1.GetItemString(al_row,'code2',ldwb_buffer,False) + '~t'
//Else
//	is_update_string += dw_1.GetItemString(al_row,'code2',ldwb_buffer,False) + '~t'
//	is_update_string += dw_1.GetItemString(al_row,'code1',ldwb_buffer,False) + '~t'
//End If	
ls_temp = dw_1.GetItemString(al_row,'process_hours',ldwb_buffer,False)
If isnull(ls_temp) Then ls_temp = "0000"
is_update_string += ls_temp + '~t'
ll_temp = dw_1.GetItemNumber(al_row,'seq_num',ldwb_buffer,False)
If isnull(ll_temp) Then ll_temp = 0
is_update_string += String(ll_temp) + '~t'
ls_temp = dw_1.GetItemString(al_row,'transfer_ind',ldwb_buffer,False) 
If isnull(ls_temp) Then ls_temp = "N"
is_update_string += ls_temp + '~t'
is_update_string += ls_user_id + '~t'
lt_time = dw_1.GetItemTime(al_row,'std_delv_time',ldwb_buffer,False) 
If IsNull(lt_time) Then
		lt_time = Time('00:00')
	End If
is_update_string += String(lt_time, 'hh:mm:ss') + '~t'
lt_time = dw_1.GetItemTime(al_row,'ext_delv_time',ldwb_buffer,False) 
If IsNull(lt_time) Then
		lt_time = Time('00:00')
	End If
is_update_string += String(lt_time, 'hh:mm:ss') + '~t'
// pjm 09/15/2014 - added new columns.
ldt_date = dw_1.GetItemDate(al_row,'pt_cutoff_date',ldwb_buffer,False)
is_update_string += String(ldt_date, 'YYYY-MM-DD') + '~t'
is_update_string += dw_1.GetItemString(al_row,'pt_cutoff_period',ldwb_buffer,False) + '~t'

ldt_date = dw_1.GetItemDate(al_row,'add_ship_date_1',ldwb_buffer,False)
is_update_string += String(ldt_date, 'YYYY-MM-DD') + '~t'
ldt_date = dw_1.GetItemDate(al_row,'add_ship_date_2',ldwb_buffer,False)
is_update_string += String(ldt_date, 'YYYY-MM-DD') + '~t'
ldt_date = dw_1.GetItemDate(al_row,'add_ship_date_3',ldwb_buffer,False)
is_update_string += String(ldt_date, 'YYYY-MM-DD') + '~t'
ldt_date = dw_1.GetItemDate(al_row,'add_ship_date_4',ldwb_buffer,False)
is_update_string += String(ldt_date, 'YYYY-MM-DD') + '~t'
ldt_date = dw_1.GetItemDate(al_row,'add_ship_date_5',ldwb_buffer,False)
is_update_string += String(ldt_date, 'YYYY-MM-DD') + '~t'
//jxr 9/27/2018 added auto open date
ldt_date = dw_1.GetItemDate(al_row,'auto_open_date',ldwb_buffer,False)
is_update_string += String(ldt_date, 'YYYY-MM-DD') + '~r~n'

//is_update_string += ls_user_id + '~r~n'
il_rec_count ++

If il_rec_count = 100 Then
//	IF Not iu_pas201.nf_pasp91br_upd_valid_plant_transfer(istr_error_info, &
//			is_update_string) THEN Return False
	IF Not iu_ws_pas4.nf_pasp91fr(istr_error_info, &
			is_update_string) THEN Return False
	il_rec_count = 0
	is_update_string = ""
END IF

Return True
end function

public subroutine wf_set_pa_range_date ();Int		li_pa_range


Select 	tutltypes.type_short_desc
Into		:li_pa_range
From  	tutltypes
Where 	(tutltypes.type_code = 'PA RANGE') AND
			(tutltypes.record_type = 'PA RANGE');
			
idt_pa_range_date = RelativeDate(Today(), li_pa_range - 1 )			

end subroutine

on w_transfer_plant_maint.create
int iCurrent
call super::create
this.dw_sort=create dw_sort
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sort
this.Control[iCurrent+2]=this.dw_1
end on

on w_transfer_plant_maint.destroy
call super::destroy
destroy(this.dw_sort)
destroy(this.dw_1)
end on

event open;call super::open;dw_sort.uf_set_box_text("Sort by")
il_rec_count = 0

This.Title = 'Transfer Plant Maintenance'
end event

event ue_postopen;call super::ue_postopen;DataWindowChild			ldwc_temp
String						ls_options
nvuo_pa_business_rules 	lu_rules

ids_tranfer_plant_list = create datastore
ids_tranfer_plant_list.DataObject = 'd_transfer_dest_modified'

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "TranDest"
istr_error_info.se_user_id 		= sqlca.userid

iu_pas201 = Create u_pas201
iu_ws_pas4 = Create u_ws_pas4


//lu_rules.uf_retrieve_plan_tran_options(ls_options)
// import transfer options
//dw_1.GetChild("transfer_ind", ldwc_temp)
//ldwc_temp.ImportString(ls_options)
//ldwc_temp.Sort()

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait.. Inquiring Database")
This.SetRedraw(False) 

wf_fillplantdddw()
wf_fill_transfer_ind()

wf_set_pa_range_date()

wf_retrieve()

this.setredraw(True)
end event

event resize;call super::resize;//integer li_x		= 1
//integer li_y		= 1
//
//dw_1.width	= width - (50 + li_x)
//dw_1.height	= height - (226 + li_y)


//constant integer li_x		= 27
//constant integer li_y		= 288

integer li_x		= 57
integer li_y		= 228


//if il_BorderPaddingWidth >  li_x Then
//                li_x = il_BorderPaddingWidth
//end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

  
dw_1.width	= newwidth - (li_x)
dw_1.height	= newheight - (li_y)

end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

end event

event ue_revisions;call super::ue_revisions;/*****************************************************************
**   REVISION NUMBER: rev#01
**   PROJECT NUMBER:  support
**   DATE:				 November 99            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Fix invalid column specified in wf_addrow
**                    
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

type dw_sort from u_sort_radio within w_transfer_plant_maint
integer width = 592
integer height = 221
integer taborder = 10
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;string			ls_holding_string
long				ll_count, ll_rec_count
Integer			li_x_cord_1, li_y_cord_1, li_x_cord_2, li_y_cord_2
					
dw_1.setredraw(False)
//// get the cord of the two plant ddw
li_x_cord_1 = Integer(dw_1.Object.code1.X)
li_y_cord_1 = Integer(dw_1.Object.code1.Y)
li_x_cord_2 = Integer(dw_1.Object.code2.X)
li_y_cord_2 = Integer(dw_1.Object.code2.Y)
// switch the cord of the two	
DW_1.Object.code1.X = li_x_cord_2
DW_1.Object.code2.X = li_x_cord_1
DW_1.Object.code1.Y = li_Y_cord_2
DW_1.Object.code2.Y = li_Y_cord_1

// get the cord of the two plant descriptions
li_x_cord_1 = Integer(dw_1.Object.code1_descr.X)
li_y_cord_1 = Integer(dw_1.Object.code1_descr.Y)
li_x_cord_2 = Integer(dw_1.Object.code2_descr.X)
li_y_cord_2 = Integer(dw_1.Object.code2_descr.Y)
// switch the cord of the two	
DW_1.Object.code1_descr.X = li_x_cord_2
DW_1.Object.code2_descr.X = li_x_cord_1
DW_1.Object.code1_descr.Y = li_Y_cord_2
DW_1.Object.code2_descr.Y = li_Y_cord_1

// get the cord of the two column headings
li_x_cord_1 = Integer(dw_1.Object.code1_t.X)
li_y_cord_1 = Integer(dw_1.Object.code1_t.Y)
li_x_cord_2 = Integer(dw_1.Object.code2_t.X)
li_y_cord_2 = Integer(dw_1.Object.code2_t.Y)
// switch the cord of the two	
DW_1.Object.code1_t.X = li_x_cord_2
DW_1.Object.code2_t.X = li_x_cord_1
DW_1.Object.code1_t.Y = li_Y_cord_2
DW_1.Object.code2_t.Y = li_Y_cord_1

//Parent.TriggerEvent('closequery')

Choose Case data
	Case "S"
//		ls_holding_string = dw_1.object.datawindow.data
//		dw_1.dataobject = "d_transfer_destinations_ship"
		dw_1.Object.co_choice.Expression='1'
		wf_retrieve()
		wf_fillplantdddw()
		For ll_count = 1 to ll_rec_count
			dw_1.setitem(ll_count,"code1_descr",dw_1.getitemstring(ll_count,"code1"))
			dw_1.setitem(ll_count,"code2_descr",dw_1.getitemstring(ll_count,"code2"))
		Next
		wf_ship_sort()
		dw_1.SetTabOrder ("code1", 10 )
		dw_1.SetTabOrder ("code2", 20 )
	Case "D"
//		ls_holding_string = dw_1.object.datawindow.data
//		dw_1.dataobject = "d_transfer_destinations_dest"
		dw_1.Object.co_choice.Expression='2'
		wf_retrieve()
		wf_fillplantdddw()
		For ll_count = 1 to ll_rec_count
			dw_1.setitem(ll_count,"code1_descr",dw_1.getitemstring(ll_count,"code1"))
			dw_1.setitem(ll_count,"code2_descr",dw_1.getitemstring(ll_count,"code2"))
		Next		
		wf_dest_sort()
		dw_1.SetTabOrder ("code2", 10 )
		dw_1.SetTabOrder ("code1", 20 )
End Choose

//dw_1.ResetUpdate()

dw_1.SetFocus ( )

dw_1.setredraw(True)
end event

event constructor;call super::constructor;ib_updateable = False
end event

type dw_1 from u_base_dw_ext within w_transfer_plant_maint
integer y = 221
integer width = 4447
integer height = 931
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_transfer_destinations_ship"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event clicked;call super::clicked;

IF row > 0 Then
//	messagebox('Clicked Event',string(row))
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
		SelectRow(0, False)
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If

end event

event constructor;call super::constructor;ib_updateable = True

end event

event itemchanged;call super::itemchanged;DataWindowChild 		ldwc_child
Long						ll_find, &
							ll_row
String					ls_message
Date					ldt_test_date
Date					ldt_open_date
Date					ldt_prev_pt_cutoff


Choose case dwo.name
	case "transfer_ind"
		dw_1.GetChild("transfer_ind", ldwc_child)
		ll_row = ldwc_child.Find('type_code = "' + Trim(data) + '"', 1, ldwc_child.RowCount())
		If ll_row <= 0 Then
			iw_Frame.SetMicroHelp(data + " is an Invalid Transfer Indicator")
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1
		Else
// ibdkdld 12/11/02 allow location to be both a shipping/receiving  
//			If data = "Y" Then
//				If Not wf_special_check(row,dwo.name,ls_message) Then
//					iw_Frame.SetMicroHelp(ls_message)
//					This.SetFocus()
//					This.SelectText(1, Len(data))
//					return 1
//				End If
				This.setitem(row,"transfer_ind",data)
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
//			End If
		End if
	case "code1"
		// Find the entered plant code in the table -- don't count the last row, it's blank
		dw_1.GetChild("code1", ldwc_child)
		ll_row = ldwc_child.Find('location_code = "' + Trim(data) + '"', 1, ldwc_child.RowCount())
		If ll_row <= 0 Then
			iw_Frame.SetMicroHelp(data + " is an Invalid Plant Code")
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1
		Else
			This.setitem(row,"code1_descr",data)
			iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
		End if
	case "code2"
		// Find the entered plant code in the table -- don't count the last row, it's blank
		dw_1.GetChild("code2", ldwc_child)
		ll_row = ldwc_child.Find('location_code = "' + Trim(data) + '"', 1, ldwc_child.RowCount())
		If ll_row <= 0 Then
			iw_Frame.SetMicroHelp(data + " is an Invalid Plant Code")
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1
		Else
			This.setitem(row,"code2_descr",data)
			iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
		End if
	case "process_hours"
		ll_find = Long(Mid ( data , 3 ))
		If ll_find > 59 Then
			iw_Frame.SetMicroHelp("Process Minutes can not be greater then 59")
			This.SetFocus()
			This.SelectText(4, Len(data))
			return 1
		End If
//		If data < 0 Then
//			iw_Frame.SetMicroHelp("Process Hours can not be negitive")
//			This.SetFocus()
//			This.SelectText(1, Len(data))
//			return 1
//		End If
//		
	case "seq_num"
		If Long(data) < 0 Then
			iw_Frame.SetMicroHelp("Sequence Number can not be negative")
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1
		End If
// jxr 9/28/18 
	case "pt_cutoff_date"
		ldt_test_date = date(data)
		ldt_prev_pt_cutoff = this.GetItemDate(row,'pt_cutoff_date',primary!,true)
		ldt_open_date = this.GetItemDate(row,'auto_open_date')
		If ldt_test_date >= today() and ldt_prev_pt_cutoff < today() Then
			This.SetItem(row,"auto_open_date",today())
		elseif ldt_open_date > ldt_test_date Then
			This.SetItem(row,"auto_open_date",ldt_test_date)
		end if		
// jxr 9/28/18 if open date changes and is > cutoff date then raise error.
	case "auto_open_date"
		ldt_open_date = date(data)
		ldt_test_date = this.GetItemDate(row,"pt_cutoff_date")
		If ldt_open_date > ldt_test_date or ldt_open_date < relativedate(today(), -1) Then
			iw_Frame.SetMicroHelp ("Auto Open Date must be >= prior day and <= PT Cutoff" ) 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if

	case "add_ship_date_1"
		ldt_test_date = date(data)
		if ldt_test_date < Today() Then
			iw_Frame.SetMicroHelp ("1st ship date must be greater or equal to current date" ) 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 			
		if ldt_test_date > idt_pa_range_date then
			iw_Frame.SetMicroHelp ("1st ship date is greater than PA range date" ) 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 
		
	case "add_ship_date_2"
		ldt_test_date = date(data)
		if ldt_test_date < Today() Then
			iw_Frame.SetMicroHelp ("2nd ship date must be greater or equal to current date" ) 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 		
		if ldt_test_date > idt_pa_range_date then
			iw_Frame.SetMicroHelp ("2nd ship date is greater than PA range date")  
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 		
		
	case "add_ship_date_3"
		ldt_test_date = date(data)
		if ldt_test_date < Today() Then
			iw_Frame.SetMicroHelp ("3rd ship date must be greater or equal to current date" ) 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 		
		if ldt_test_date > idt_pa_range_date then
			iw_Frame.SetMicroHelp ("3rd ship date is greater than PA range date") 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 		
		
	case "add_ship_date_4"
		ldt_test_date = date(data)
		if ldt_test_date < Today() Then
			iw_Frame.SetMicroHelp ("4th ship date must be greater or equal to current date" ) 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 		
		if ldt_test_date > idt_pa_range_date then
			iw_Frame.SetMicroHelp ("4th ship date is greater than PA range date") 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 		
		
	case "add_ship_date_5"
		ldt_test_date = date(data)
		if ldt_test_date < Today() Then
			iw_Frame.SetMicroHelp ("5th ship date must be greater or equal to current date" ) 
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 		
		if ldt_test_date > idt_pa_range_date then
			iw_Frame.SetMicroHelp ("5th ship date is greater than PA range date")  
			This.SetFocus()
			This.SelectText(1, Len(data))
			return 1			
		end if 		
end choose

This.SetFocus()
Return 0
end event

event itemerror;call super::itemerror;Return 1
end event

event ue_keydown;call super::ue_keydown;//Long 		ll_row
//Boolean	lb_selected
//String	ls_column
//
//This.SetRedraw(False)
//
//ll_row = This.GetRow()
//ls_column = This.GetColumnName( )
//ls_column = left(ls_column,4)
//messagebox(ls_column,' ')
//CHOOSE CASE key
//	Case KeyUpArrow!
//		If Not ls_column = 'code' Then
//			IF ll_row > 1 Then
//				ll_row --
//				This.SelectRow(0, False)
//				This.SelectRow(ll_row, True)
//				This.SetRow(ll_Row)
//			End If
//		End If
// 	Case KeyDownArrow!
//		IF ll_row <> dw_1.RowCount( ) Then
//			ll_row ++
//			This.SelectRow(0, False)
//			This.SelectRow(ll_row, True)
//			This.SetRow(ll_Row)
//		End If
//END CHOOSE
//This.SetRedraw(True)
//
//
//Return 0
//
end event

event ue_postconstructor;call super::ue_postconstructor;integer rtncode
datawindowchild dwch_period

rtncode = This.GetChild('pt_cutoff_period', dwch_period)
IF rtncode = -1 THEN 
	MessageBox( "Error", "pt_cutoff_period not a DataWindowChild") 
end if	
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
dwch_period.SetTransObject(SQLCA)
dwch_period.Retrieve()

end event

