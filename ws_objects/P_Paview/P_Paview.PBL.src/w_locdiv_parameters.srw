﻿$PBExportHeader$w_locdiv_parameters.srw
$PBExportComments$Deb's project
forward
global type w_locdiv_parameters from w_base_sheet_ext
end type
type tab_1 from tab within w_locdiv_parameters
end type
type tabpage_1 from userobject within tab_1
end type
type dw_avail_detail from u_base_dw_ext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_avail_detail dw_avail_detail
end type
type tabpage_2 from userobject within tab_1
end type
type dw_chgorder_parms from u_base_dw_ext within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_chgorder_parms dw_chgorder_parms
end type
type tab_1 from tab within w_locdiv_parameters
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
type dw_division from u_division within w_locdiv_parameters
end type
end forward

global type w_locdiv_parameters from w_base_sheet_ext
integer x = 389
integer y = 468
integer width = 2370
integer height = 1421
string title = "Location / Division Parameters"
long backcolor = 12632256
tab_1 tab_1
dw_division dw_division
end type
global w_locdiv_parameters w_locdiv_parameters

type variables
u_pas201		iu_pas201
u_ws_pas4	iu_ws_pas4

s_error		istr_error_info

String		is_check_avail_ind


end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();long			ll_rec_count

string		ls_division, &  
            ls_division_data, &
				ls_temp

u_string_functions	lu_string


IF Not Super::wf_retrieve() Then Return False

SetPointer(HourGlass!)

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp74br_inq_locdiv_parameters"
istr_error_info.se_message = Space(71)

ls_division = dw_division.uf_get_division()

This.SetRedraw(False)

tab_1.tabpage_1.dw_avail_detail.reset()

If iu_ws_pas4.NF_PASP74FR(istr_error_info, &
										ls_division, &
										ls_division_data)  < 0 Then 
										This.SetRedraw(True)
										Return False
End If

//MessageBox('Return Data',string(ls_division_data))
//ls_Division_data = '004~tlexington proc~tA~t3~t08:00~r~n' + &
//						 '026~tlexington proc~tC~t3~t08:00~r~n' + &
//						 '051~tlexington proc~tA~t3~t08:00~r~n' + &
//						 '052~tlexington proc~tC~t3~t08:00~r~n' + &
//						 '053~tlexington proc~tA~t3~t08:00~r~n' + &
//						 '061~tlexington proc~tC~t3~t08:00~r~n' + &
//						 '062~tlexington proc~tA~t3~t08:00~r~n'           
//
If Not lu_string.nf_IsEmpty(ls_division_data) Then
	ll_rec_count = tab_1.tabpage_1.dw_avail_detail.ImportString(ls_Division_Data)
	If ll_rec_count > 0 then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True)

tab_1.tabpage_2.dw_chgorder_parms.ResetUpdate()
tab_1.tabpage_1.dw_avail_detail.ResetUpdate()
tab_1.tabpage_1.dw_avail_detail.SetFocus()
 
Return True
end function

public function boolean wf_update ();String			ls_input_string, &
					ls_output_string, &
					ls_temp, &
					ls_header_string
					
Time				lt_cutoff_time					

Long				ll_row


If tab_1.tabpage_1.dw_avail_detail.AcceptText() < 1 Then Return False
If tab_1.tabpage_2.dw_chgorder_parms.AcceptText() < 1 Then Return False


ll_row = tab_1.tabpage_1.dw_avail_detail.GetNextModified(0, primary!)
If ll_row = 0 Then
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End If

ls_input_string = ''
Do 
	ls_input_string += tab_1.tabpage_1.dw_avail_detail.GetItemString( &
	   ll_row, 'plant_code') + '~t'
	ls_input_string += tab_1.tabpage_1.dw_avail_detail.GetItemString( &
	   ll_row, 'avail_ind') + '~t'
// add tab page ibdkdld
	ls_temp = String(tab_1.tabpage_2.dw_chgorder_parms.GetItemNumber(ll_row, 'chgord_cutoff_days'))
	If IsNull(ls_temp) Then ls_temp = '0'
	ls_input_string += ls_temp + '~t'

	lt_cutoff_time = Time(tab_1.tabpage_2.dw_chgorder_parms.GetItemString(ll_row, 'chgord_cutoff_time'))
	If IsNull(lt_cutoff_time) Then
		lt_cutoff_time = Time('00:00')
	End If
	ls_input_string += String(lt_cutoff_time, 'hh:mm:ss') + '~r~n'
		
		
	ll_row = tab_1.tabpage_1.dw_avail_detail.GetNextModified(ll_row, primary!)
	
Loop While ll_row > 0

ls_header_string = dw_division.uf_get_division()

//messagebox ('wf update',string('division = ' + ls_header_string))

If iu_ws_pas4.NF_PASP75FR(istr_error_info, ls_header_string, &
   ls_input_string) = false then
	Return False
End If

iw_frame.SetMicroHelp('Modification Successful')
tab_1.tabpage_1.dw_avail_detail.ResetUpdate()

Return True

end function

on w_locdiv_parameters.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.dw_division
end on

on w_locdiv_parameters.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.dw_division)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

//If tab_1.SelectedTab = 2 Then
//	If is_protection_status = 'Y' Then
//		iw_frame.im_menu.mf_Disable('m_save')
//	Else
//		iw_frame.im_menu.mf_Enable('m_save')
//	End If
//End If
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'division'
		message.StringParm = dw_division.uf_get_division()
End choose

end event

event ue_postopen;call super::ue_postopen;						
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Locdivpa"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_division_inq'
// create user object to use
iu_pas201 = Create u_pas201
iu_ws_pas4 = Create u_ws_pas4

wf_retrieve()
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'division'
		dw_division.uf_set_division(as_value)
	
End Choose
end event

event close;call super::close;// no destroy's needed in Version 6.0
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

end event

event ue_fileprint;call super::ue_fileprint;
IF Not ib_print_ok Then Return

If tab_1.SelectedTab = 1 Then
	tab_1.tabpage_1.dw_avail_detail.Print()
Else
	tab_1.tabpage_2.dw_chgorder_parms.print()
End If
end event

event resize;call super::resize;constant integer li_tab_x		= 20
constant integer li_tab_y		= 150
  
constant integer li_dw_x		= 70
constant integer li_dw_y		= 290

  
  
tab_1.width	= newwidth - li_tab_x
tab_1.height = newheight - li_tab_y

tab_1.tabpage_1.dw_avail_detail.width = newwidth - li_dw_x
tab_1.tabpage_1.dw_avail_detail.height = newheight - li_dw_y

tab_1.tabpage_2.dw_chgorder_parms.width = newwidth - li_dw_x
tab_1.tabpage_2.dw_chgorder_parms.height = newheight - li_dw_y


end event

type tab_1 from tab within w_locdiv_parameters
integer x = 18
integer y = 141
integer width = 2278
integer height = 1136
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 79741120
boolean raggedright = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

event selectionchanged;//If newindex = 2 Then
//	If is_protection_status = 'Y' Then
//		iw_frame.im_menu.mf_disable('m_save')
//	End If
//Else
//	iw_frame.im_menu.mf_enable('m_save')
//End If
end event

type tabpage_1 from userobject within tab_1
integer x = 15
integer y = 90
integer width = 2249
integer height = 1034
long backcolor = 79741120
string text = "Check Availability"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
dw_avail_detail dw_avail_detail
end type

on tabpage_1.create
this.dw_avail_detail=create dw_avail_detail
this.Control[]={this.dw_avail_detail}
end on

on tabpage_1.destroy
destroy(this.dw_avail_detail)
end on

type dw_avail_detail from u_base_dw_ext within tabpage_1
event ue_post_changed_ind ( long row,  dwobject dwo )
integer y = 16
integer width = 2099
integer height = 963
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_locdiv_parameters_detail"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_post_changed_ind;String				ls_original, &
						ls_new

ls_original = This.GetItemString(row, 'check_avail_ind', primary!, True)
ls_new = This.GetItemString(row, 'check_avail_ind')
	
If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
	This.SetItemStatus(row, 'check_avail_ind', primary!, notmodified!)
End If

end event

event itemchanged;call super::itemchanged;dwitemstatus ls_temp 

If dwo.name = 'check_avail_ind' Then
	This.EVENT POST ue_post_changed_ind(row, dwo)
End if

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_temp


This.GetChild('avail_ind', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve('PAAVLIND')
end event

event rbuttondown;call super::rbuttondown;//m_shortage_queue	lm_popup
//
//lm_popup = Create m_shortage_queue
//
//lm_popup.m_shortagequeue.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())
//
//Destroy lm_popup
//
end event

type tabpage_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 15
integer y = 90
integer width = 2249
integer height = 1034
long backcolor = 79741120
string text = "Sales Orders Parameters"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_chgorder_parms dw_chgorder_parms
end type

on tabpage_2.create
this.dw_chgorder_parms=create dw_chgorder_parms
this.Control[]={this.dw_chgorder_parms}
end on

on tabpage_2.destroy
destroy(this.dw_chgorder_parms)
end on

type dw_chgorder_parms from u_base_dw_ext within tabpage_2
event ue_postitemchanged ( string as_column_name,  string as_data,  long al_row )
integer y = 16
integer width = 1807
integer height = 986
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_chgorder_parms"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_postitemchanged;This.SetItem(al_row, as_column_name, as_data)
end event

event editchanged;String 					ls_temp	
u_string_functions	lu_string
Boolean					lb_numeric


ls_temp = Right(data,1)

Choose Case dwo.name
	CASE "chgord_cutoff_time"
			IF Not IsNumber(ls_temp) THEN
				If Len(Data) > 1 Then
					If lu_string.nf_IsEmpty(ls_temp) or ls_temp = ':' Then
						//do no not
					Else
						iw_frame.SetMicroHelp('Change Order Cut Off Time must be numeric, spaces, or a Colon')		
						Return  1
					End if
				Else
					If not lu_string.nf_IsEmpty(ls_temp) Then
						If ls_temp = ':' Then 
							iw_frame.SetMicroHelp('Change Order Cut Off Time must have a hour(number) before the colon')		
						Else	
							iw_frame.SetMicroHelp('Change Order Cut Off Time must be numeric, spaces, or a Colon')		
						End If
						Return  1
				End if
			End If
		END IF
END CHOOSE

end event

event itemchanged;call super::itemchanged;String 	ls_temp,ls_time
Boolean	lb_data_empty,lb_time
Long		ll_days,ll_error
Time		lt_time	

nvuo_pa_business_rules 	nvuo_pa
u_string_functions		u_string

Choose case dwo.name
	CASE "chgord_cutoff_time"
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				iw_frame.SetMicroHelp(ls_temp + ' is not a valid time') 
				dw_chgorder_parms.SelectText ( 1, 100 )
				Return 1
			End If
			lt_time = Time(ls_temp)
		Else
			lb_data_empty = True
		End If
		If Not lb_data_empty Then
			ls_time = String(lt_time, 'hh:mm')
			This.Event Post ue_postitemchanged(dwo.name,ls_time,row)
		End If			
	CASE "chgord_cutoff_days"
		ll_days = Sign(Long(data))
		If ll_days < 0 Then
				iw_frame.SetMicroHelp('Change Order Cut Off Days can not be negative') 
				dw_chgorder_parms.SelectText ( 1, 100 )
				Return 1
		End If	
End Choose	

end event

event itemerror;call super::itemerror;return 2
end event

event ue_postconstructor;call super::ue_postconstructor;tab_1.tabpage_1.dw_avail_detail.ShareData(tab_1.tabpage_2.dw_chgorder_parms)
end event

type dw_division from u_division within w_locdiv_parameters
integer x = 33
integer y = 29
integer width = 1616
integer height = 106
integer taborder = 20
boolean bringtotop = true
end type

event constructor;call super::constructor;This.Disable()
end event

