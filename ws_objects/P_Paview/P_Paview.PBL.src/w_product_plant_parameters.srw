﻿$PBExportHeader$w_product_plant_parameters.srw
forward
global type w_product_plant_parameters from w_base_sheet_ext
end type
type dw_scroll_product from u_base_dw_ext within w_product_plant_parameters
end type
type dw_division from u_division within w_product_plant_parameters
end type
type dw_plant from u_plant within w_product_plant_parameters
end type
type tab_1 from tab within w_product_plant_parameters
end type
type tab_made_to_order from userobject within tab_1
end type
type dw_made_to_order from u_base_dw_ext within tab_made_to_order
end type
type tab_made_to_order from userobject within tab_1
dw_made_to_order dw_made_to_order
end type
type tab_parameters from userobject within tab_1
end type
type dw_prod_plant_parameters from u_base_dw_ext within tab_parameters
end type
type tab_parameters from userobject within tab_1
dw_prod_plant_parameters dw_prod_plant_parameters
end type
type tab_freeze_days from userobject within tab_1
end type
type dw_freeze_days from u_base_dw_ext within tab_freeze_days
end type
type tab_freeze_days from userobject within tab_1
dw_freeze_days dw_freeze_days
end type
type tab_1 from tab within w_product_plant_parameters
tab_made_to_order tab_made_to_order
tab_parameters tab_parameters
tab_freeze_days tab_freeze_days
end type
end forward

global type w_product_plant_parameters from w_base_sheet_ext
integer x = 27
integer y = 4
integer width = 3534
integer height = 1488
string title = "Product Plant Parameters"
long backcolor = 12632256
windowanimationstyle closeanimation = bottomroll!
dw_scroll_product dw_scroll_product
dw_division dw_division
dw_plant dw_plant
tab_1 tab_1
end type
global w_product_plant_parameters w_product_plant_parameters

type prototypes

end prototypes

type variables
Double		id_task_number, &
		id_Last_record, &
		id_Max_Record

u_pas201		iu_pas201
u_ws_pas4	iu_ws_pas4

s_error		istr_error_info

DataWindow	idw_top_dw

boolean		ib_inquire_flag

Integer		ii_pallet_count

DataStore	ids_parms_all	

		
end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_retrieve ()
public subroutine wf_extract_data ()
end prototypes

public function boolean wf_update ();String			ls_input_string, &
					ls_plant, &
					ls_message

Long				ll_row, &
					ll_sub
					
SetPointer(HourGlass!)

ls_plant = dw_plant.uf_get_plant_code()

tab_1.tab_made_to_order.dw_made_to_order.AcceptText()
//dmk ar8171
tab_1.tab_parameters.dw_prod_plant_parameters.AcceptText()

tab_1.tab_freeze_days.dw_freeze_days.AcceptText()

ll_row = tab_1.tab_made_to_order.dw_made_to_order.RowCount()

For ll_sub = 1 to ll_row
	If GetItemString(tab_1.tab_made_to_order.dw_made_to_order,ll_sub, "made_to_order_ind") = 'Y' AND &
		GetItemString(tab_1.tab_made_to_order.dw_made_to_order,ll_sub, "when_to_produce") = ' ' Then
			ls_message = "When to Produce column needs a value for product " + &
				GetItemString(tab_1.tab_made_to_order.dw_made_to_order,ll_sub, "product_code")
			iw_frame.SetMicroHelp(ls_message) 
			Return False
	End If
Next

ll_row = tab_1.tab_freeze_days.dw_freeze_days.RowCount()
For ll_sub = 1 to ll_row
	If tab_1.tab_freeze_days.dw_freeze_days.GetItemNumber(ll_sub, "freeze_days") < 0 Then
			ls_message = "Freeze days is less than zero for product " + &
				GetItemString( tab_1.tab_freeze_days.dw_freeze_days,ll_sub, "product_code")
			iw_frame.SetMicroHelp(ls_message) 
			Return False
	End If
	
	If tab_1.tab_freeze_days.dw_freeze_days.GetItemNumber(ll_sub, "blast_freeze_days") < 0 Then
			ls_message = "Blast freeze days is less than zero for product " + &
				tab_1.tab_freeze_days.dw_freeze_days.GetItemString( ll_sub, "product_code")
			iw_frame.SetMicroHelp(ls_message) 
			Return False
	End If	
	
	If tab_1.tab_freeze_days.dw_freeze_days.GetItemNumber(ll_sub, "transfer_delay_days") < 0 Then
			ls_message = "Transfer delay days is less than zero for product " + &
				tab_1.tab_freeze_days.dw_freeze_days.GetItemString( ll_sub, "product_code")
			iw_frame.SetMicroHelp(ls_message) 
			Return False
	End If		
				
	If tab_1.tab_freeze_days.dw_freeze_days.GetItemNumber(ll_sub, "freeze_days") + &
			tab_1.tab_freeze_days.dw_freeze_days.GetItemNumber(ll_sub, "blast_freeze_days") + &
			tab_1.tab_freeze_days.dw_freeze_days.GetItemNumber(ll_sub, "transfer_delay_days") > 99 Then
			ls_message = "Total of freeze days, blast freeze days, and transfer_delay_days is greater than 99 for product " + &
				tab_1.tab_made_to_order.dw_made_to_order.GetItemString(ll_sub, "product_code")
			iw_frame.SetMicroHelp(ls_message) 
			Return False
	End If
Next

ls_input_string = ""	

ll_row = tab_1.tab_made_to_order.dw_made_to_order.getnextmodified(0, PRIMARY!)
//dmk sr8171 add days_retained
do while ll_row > 0
	ls_input_string += ls_plant + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'product_code') + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'made_to_order_ind') + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'scheduled_to_order_ind') + '~t' + & 
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'when_to_produce') + '~t' + &
	string(tab_1.tab_made_to_order.dw_made_to_order.getitemnumber(ll_row,'days_prior_to_ship')) + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'no_other_prod_penalty') + '~t' + &
	string(tab_1.tab_made_to_order.dw_made_to_order.getitemnumber(ll_row,'box_threshold')) + '~t' + &
	string(tab_1.tab_parameters.dw_prod_plant_parameters.getitemnumber(ll_row,'days_retained')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'freeze_days')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'blast_freeze_days')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'transfer_delay_days')) + '~r~n'
	
	ll_row = tab_1.tab_made_to_order.dw_made_to_order.getnextmodified(ll_row, PRIMARY!)
	
loop

//dmk sr8171
ll_row = tab_1.tab_parameters.dw_prod_plant_parameters.getnextmodified(0, PRIMARY!)
do while ll_row > 0
	ls_input_string += ls_plant + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'product_code') + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'made_to_order_ind') + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'scheduled_to_order_ind') + '~t' + & 
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'when_to_produce') + '~t' + &
	string(tab_1.tab_made_to_order.dw_made_to_order.getitemnumber(ll_row,'days_prior_to_ship')) + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'no_other_prod_penalty') + '~t' + &
	string(tab_1.tab_made_to_order.dw_made_to_order.getitemnumber(ll_row,'box_threshold')) + '~t' + &
	string(tab_1.tab_parameters.dw_prod_plant_parameters.getitemnumber(ll_row,'days_retained')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'freeze_days')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'blast_freeze_days')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'transfer_delay_days')) + '~r~n'
	
	ll_row = tab_1.tab_parameters.dw_prod_plant_parameters.getnextmodified(ll_row, PRIMARY!)
	
loop

ll_row = tab_1.tab_freeze_days.dw_freeze_days.getnextmodified(0, PRIMARY!)
do while ll_row > 0
	ls_input_string += ls_plant + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'product_code') + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'made_to_order_ind') + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'scheduled_to_order_ind') + '~t' + & 
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'when_to_produce') + '~t' + &
	string(tab_1.tab_made_to_order.dw_made_to_order.getitemnumber(ll_row,'days_prior_to_ship')) + '~t' + &
	tab_1.tab_made_to_order.dw_made_to_order.getitemstring(ll_row,'no_other_prod_penalty') + '~t' + &
	string(tab_1.tab_made_to_order.dw_made_to_order.getitemnumber(ll_row,'box_threshold')) + '~t' + &
	string(tab_1.tab_parameters.dw_prod_plant_parameters.getitemnumber(ll_row,'days_retained')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'freeze_days')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'blast_freeze_days')) + '~t' + &
	string(tab_1.tab_freeze_days.dw_freeze_days.getitemnumber(ll_row, 'transfer_delay_days')) + '~r~n'
	
	ll_row =  tab_1.tab_freeze_days.dw_freeze_days.getnextmodified(ll_row, PRIMARY!)
	
loop



//messagebox("Data",ls_input_string)

/*If Not iu_pas201.nf_pasp93br_update_product_plant_para(istr_error_info, &
																ls_input_string) Then	
																Return False
End If
*/
If Not iu_ws_pas4.NF_PASP93FR(istr_error_info, &
																ls_input_string) Then	
																Return False
End If


iw_frame.SetMicroHelp('Modification Successful')
tab_1.tab_made_to_order.dw_made_to_order.ResetUpdate()

Return True

end function

public function boolean wf_retrieve ();int			li_counter

long			ll_rec_count, &
				ll_row, &
				ll_est_days_retained

string		ls_division, &  
            ls_plant, &
				ls_country, &
				ls_plant_type, &
				ls_header, &
				ls_parameters
				

				
u_string_functions	lu_string


if tab_1.tab_made_to_order.dw_made_to_order.ModifiedCount() > 0 then
	choose case MessageBox ( this.title, "Do you want to save changes", &
						Question!, YesNoCancel!)
		case 1
			wf_update()
		case 2
			
		case 3
			return true
	end choose
end if


OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

if ib_inquire_flag = false then
	return false
end if

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp76br_inq_sold_position"
istr_error_info.se_message = Space(71)

ls_division = dw_division.uf_get_division()
ls_plant = dw_plant.uf_get_plant_code()

ls_header = ls_division + '~t' + &
				ls_plant + '~r~n' 

tab_1.tab_made_to_order.dw_made_to_order.SetRedraw(False)
tab_1.tab_parameters.dw_prod_plant_parameters.SetRedraw(False)
tab_1.tab_freeze_days.dw_freeze_days.SetRedraw(False)

tab_1.tab_made_to_order.dw_made_to_order.reset()

////dmk sr8171
tab_1.tab_parameters.dw_prod_plant_parameters.reset()
tab_1.tab_freeze_days.dw_freeze_days.reset()

ids_parms_all = Create DataStore
ids_parms_all.DataObject = 'd_product_plant_parms_all'
ids_parms_all.Reset()

If iu_ws_pas4.NF_PASP92FR(istr_error_info, & 
										ls_header, &
										ls_parameters) < 0 Then
										This.SetRedraw(True) 
										Return False
End If

//ls_data = '00000001' + '~t' + 'Test 01' + '~t' + 'Y' + '~t' + 'N' + '~t' + 'L' + '~t' + '0' + '~t' + '0' + '~t' + '0' + '~r~n' + &
//			'00000002' + '~t' + 'Test 02' + '~t' + 'Y' + '~t' + 'N' + '~t' + 'D' + '~t' + '0' + '~t' + '0' + '~t' + '0' + '~r~n' + &
//			'00000003' + '~t' + 'Test 03' + '~t' + 'Y' + '~t' + 'N' + '~t' + 'L' + '~t' + '0' + '~t' + '0' + '~t' + '0' + '~r~n' 
//

If Not lu_string.nf_IsEmpty(ls_parameters) Then
	ll_rec_count = ids_parms_all.ImportString(ls_parameters)
	
	wf_extract_data()
	
	SetMicroHelp(String(ll_rec_count) + " Rows Retrieved")
	
	tab_1.tab_made_to_order.dw_made_to_order.setsort("product_code A")
	tab_1.tab_made_to_order.dw_made_to_order.sort()

	tab_1.tab_parameters.dw_prod_plant_parameters.setsort("product_code A")
	tab_1.tab_parameters.dw_prod_plant_parameters.sort()

	tab_1.tab_freeze_days.dw_freeze_days.setsort("product_code A")
	tab_1.tab_freeze_days.dw_freeze_days.sort()
	
	If tab_1.tab_freeze_days.dw_freeze_days.GetItemString(1, "tab_visible_ind") = 'Y' Then
		tab_1.tab_freeze_days.Visible = True
	Else
		tab_1.tab_freeze_days.Visible = False
		idw_top_dw = tab_1.tab_made_to_order.dw_made_to_order
		tab_1.SelectedTab = 1
	End If
Else
	SetMicroHelp("0 Rows Retrieved")
End if

tab_1.tab_made_to_order.dw_made_to_order.SetRedraw(True)
tab_1.tab_parameters.dw_prod_plant_parameters.SetRedraw(True)
tab_1.tab_freeze_days.dw_freeze_days.SetRedraw(True)
This.SetRedraw(True) 

tab_1.tab_made_to_order.dw_made_to_order.ResetUpdate()
tab_1.tab_made_to_order.dw_made_to_order.SetFocus()
//DMK SR8171

tab_1.tab_parameters.dw_prod_plant_parameters.ResetUpdate()
tab_1.tab_freeze_days.dw_freeze_days.ResetUpdate()
Return True

end function

public subroutine wf_extract_data ();Integer	li_RowCount, li_sub, li_row

li_RowCount = ids_parms_all.RowCount()

For li_sub = 1 to li_RowCount

	li_row =tab_1.tab_made_to_order.dw_made_to_order.InsertRow(0)
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "product_code", ids_parms_all.GetItemString(li_row, "product_code"))
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "product_descr", ids_parms_all.GetItemString(li_row, "product_descr"))
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "made_to_order_ind", ids_parms_all.GetItemString(li_row, "made_to_order_ind"))
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "scheduled_to_order_ind", ids_parms_all.GetItemString(li_row, "scheduled_to_order_ind"))
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "when_to_produce", ids_parms_all.GetItemString(li_row, "when_to_produce"))
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "days_prior_to_ship", ids_parms_all.GetItemNumber(li_row, "days_prior_to_ship"))
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "no_other_prod_penalty", ids_parms_all.GetItemString(li_row, "no_other_prod_penalty"))
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "box_threshold", ids_parms_all.GetItemNumber(li_row, "box_threshold"))
	tab_1.tab_made_to_order.dw_made_to_order.SetItem(li_row, "boxes_per_pallet", ids_parms_all.GetItemNumber(li_row, "boxes_per_pallet"))
	 
	li_row = tab_1.tab_parameters.dw_prod_plant_parameters.InsertRow(0) 
	tab_1.tab_parameters.dw_prod_plant_parameters.SetItem(li_row, "product_code", ids_parms_all.GetItemString(li_row, "product_code"))
	tab_1.tab_parameters.dw_prod_plant_parameters.SetItem(li_row, "product_descr", ids_parms_all.GetItemString(li_row, "product_descr"))	
	tab_1.tab_parameters.dw_prod_plant_parameters.SetItem(li_row, "days_retained", ids_parms_all.GetItemNumber(li_row, "days_retained"))
	
	li_row = tab_1.tab_freeze_days.dw_freeze_days.InsertRow(0) 
	tab_1.tab_freeze_days.dw_freeze_days.SetItem(li_row, "product_code", ids_parms_all.GetItemString(li_row, "product_code"))
	tab_1.tab_freeze_days.dw_freeze_days.SetItem(li_row, "product_descr", ids_parms_all.GetItemString(li_row, "product_descr"))		
	tab_1.tab_freeze_days.dw_freeze_days.SetItem(li_row, "freeze_days", ids_parms_all.GetItemNumber(li_row, "freeze_days"))
	tab_1.tab_freeze_days.dw_freeze_days.SetItem(li_row, "blast_freeze_days", ids_parms_all.GetItemNumber(li_row, "blast_freeze_days"))
	tab_1.tab_freeze_days.dw_freeze_days.SetItem(li_row, "transfer_delay_days", ids_parms_all.GetItemNumber(li_row, "transfer_delay_days"))
	tab_1.tab_freeze_days.dw_freeze_days.SetItem(li_row, "tab_visible_ind", ids_parms_all.GetItemString(li_row, "tab_visible_ind"))

Next

end subroutine

on w_product_plant_parameters.create
int iCurrent
call super::create
this.dw_scroll_product=create dw_scroll_product
this.dw_division=create dw_division
this.dw_plant=create dw_plant
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_scroll_product
this.Control[iCurrent+2]=this.dw_division
this.Control[iCurrent+3]=this.dw_plant
this.Control[iCurrent+4]=this.tab_1
end on

on w_product_plant_parameters.destroy
call super::destroy
destroy(this.dw_scroll_product)
destroy(this.dw_division)
destroy(this.dw_plant)
destroy(this.tab_1)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_nonvisprint')
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
end event

event ue_postopen;call super::ue_postopen;datawindowchild		ldwc_product,&
							ldwc_product_descr

string					ls_description

iu_pas201 = Create u_pas201
iu_ws_pas4 = Create u_ws_pas4

If Message.ReturnValue = -1 Then
	Close(This)
	return
End if

is_inquire_window_name = 'w_product_plant_parameters_inq'

istr_error_info.se_user_id = sqlca.userid
istr_error_info.se_Window_name = "Made2ord"

//tab_1.tab_made_to_order.dw_maid_to_order.GetChild( "product_code", ldwc_product )
//tab_1.tab_made_to_order.dw_maid_to_order.GetChild( "product_desc", ldwc_product_descr )

Choose Case tab_1.SelectedTab
	Case 1
		idw_top_dw = tab_1.tab_made_to_order.dw_made_to_order
	Case Else
		idw_top_dw = tab_1.tab_made_to_order.dw_made_to_order
		tab_1.SelectedTab = 1
End Choose

tab_1.tab_freeze_days.Visible = False

// Establish the connection if not already connected
CONNECT USING SQLCA;	
SELECT tutltypes.type_short_desc  
  INTO :ls_description  
  FROM tutltypes  
  WHERE ( tutltypes.record_type = 'PA RANGE' ) AND  
         ( tutltypes.type_code = 'PLTPARM ' )   ;

ii_pallet_count = Integer(ls_description)

iw_frame.im_menu.m_file.m_inquire.TriggerEvent(Clicked!)
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'division'
		Message.StringParm = dw_division.uf_get_division()
	Case 'plant'
		Message.StringParm = dw_plant.uf_get_plant_code()
End Choose
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'division'
		dw_division.uf_set_division(as_value)
	case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	case 'close'
		ib_inquire_flag = false
	case 'ok'
		ib_inquire_flag = true
End Choose

end event

event resize;call super::resize;constant integer li_tab_x		= 10
constant integer li_tab_y		= 350
  
integer li_dw_x		= 30
integer li_dw_y		= 550


if il_BorderPaddingWidth > li_dw_x Then
   li_dw_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_dw_y Then
   li_dw_y = il_BorderPaddingHeight
End If


   
tab_1.width	= newwidth - li_tab_x
tab_1.height = newheight - li_tab_y

tab_1.tab_made_to_order.dw_made_to_order.width = newwidth - li_dw_x
tab_1.tab_made_to_order.dw_made_to_order.height = newheight - li_dw_y

//dmk sr8171
tab_1.tab_parameters.dw_prod_plant_parameters.width = newwidth - li_dw_x
tab_1.tab_parameters.dw_prod_plant_parameters.height = newheight - li_dw_y

tab_1.tab_freeze_days.dw_freeze_days.width = newwidth - li_dw_x
tab_1.tab_freeze_days.dw_freeze_days.height = newheight - li_dw_y
end event

type dw_scroll_product from u_base_dw_ext within w_product_plant_parameters
integer x = 91
integer y = 236
integer width = 1152
integer height = 80
integer taborder = 10
string dataobject = "d_scroll_product"
boolean border = false
end type

event constructor;call super::constructor;InsertRow(0)
ib_updateable = False
end event

event editchanged;Long	ll_row, &
		ll_first_row, &
		ll_last_row
		
if mid(data,len(data),1) = "'" then
	data = left(data,len(data) - 1)
	this.setitem(1,"product_code",data)
	this.setfocus()
	this.SelectText(len(data) + 1,0)
	return
end if

ll_row = idw_top_dw.Find("product_code >= '" + &
		data + "'",1,idw_top_dw.RowCount()+1)

If ll_row > 0 Then 
	idw_top_dw.ScrollToRow(ll_row)
	idw_top_dw.SetRow(ll_row + 1)
End If

ll_first_row = Long(idw_top_dw.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(idw_top_dw.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	idw_top_dw.SetRedraw(False)
	idw_top_dw.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	idw_top_dw.ScrollToRow(ll_row)
	idw_top_dw.SetRow(ll_row + 1)
	idw_top_dw.SetRedraw(True)
End If
end event

type dw_division from u_division within w_product_plant_parameters
integer x = 27
integer y = 20
integer height = 92
integer taborder = 40
end type

on constructor;call u_division::constructor;This.Disable()
end on

type dw_plant from u_plant within w_product_plant_parameters
integer x = 59
integer y = 132
integer height = 92
integer taborder = 20
boolean bringtotop = true
end type

event constructor;call super::constructor;This.Disable()
end event

type tab_1 from tab within w_product_plant_parameters
integer y = 352
integer width = 3465
integer height = 1028
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean raggedright = true
integer selectedtab = 1
tab_made_to_order tab_made_to_order
tab_parameters tab_parameters
tab_freeze_days tab_freeze_days
end type

event selectionchanging;String ls_firstrow

If IsValid(idw_top_dw) Then
	ls_firstrow = idw_top_dw.Describe("DataWindow.VerticalScrollPosition")

	// This should synchronize the two datawindows' scrolling
	Choose Case newindex
	 	Case 1   // Switched to Made to order
			idw_top_dw = This.tab_made_to_order.dw_made_to_order
		Case 2   // Switched to plant parameters
			idw_top_dw = This.tab_parameters.dw_prod_plant_parameters
		Case 3   // Switched to plant parameters
			idw_top_dw = This.tab_freeze_days.dw_freeze_days			
End Choose
	idw_top_dw.Modify("DataWindow.VerticalScrollPosition = '" + ls_firstrow + "'")	
End If
return 0  // Allow the tabpage selection to change
end event

on tab_1.create
this.tab_made_to_order=create tab_made_to_order
this.tab_parameters=create tab_parameters
this.tab_freeze_days=create tab_freeze_days
this.Control[]={this.tab_made_to_order,&
this.tab_parameters,&
this.tab_freeze_days}
end on

on tab_1.destroy
destroy(this.tab_made_to_order)
destroy(this.tab_parameters)
destroy(this.tab_freeze_days)
end on

type tab_made_to_order from userobject within tab_1
integer x = 18
integer y = 152
integer width = 3429
integer height = 860
long backcolor = 79741120
string text = "     Make to~r~nSchedule/Order"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
dw_made_to_order dw_made_to_order
end type

on tab_made_to_order.create
this.dw_made_to_order=create dw_made_to_order
this.Control[]={this.dw_made_to_order}
end on

on tab_made_to_order.destroy
destroy(this.dw_made_to_order)
end on

type dw_made_to_order from u_base_dw_ext within tab_made_to_order
integer x = 14
integer y = 16
integer width = 3415
integer height = 820
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_made_to_order"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;datawindowchild		ldwc_made_to_order, &
							ldwc_sche_to_order, &
							ldwc_when_to_produce

This.GetChild('made_to_order_ind', ldwc_made_to_order)
ldwc_made_to_order.SetTransObject(SQLCA)
ldwc_made_to_order.Retrieve()

This.GetChild('scheduled_to_order_ind', ldwc_sche_to_order)
ldwc_sche_to_order.SetTransObject(SQLCA)
ldwc_sche_to_order.Retrieve()

This.GetChild('when_to_produce', ldwc_when_to_produce)
ldwc_when_to_produce.SetTransObject(SQLCA)
ldwc_when_to_produce.Retrieve()
end event

event itemchanged;call super::itemchanged;Long	ll_find

If dwo.Name = "made_to_order_ind" then
	if data = 'N' then
		This.SetItem(row, "when_to_produce", " ")
		This.SetItem(row, "days_prior_to_ship", 0)
		This.SetItem(row, "no_other_prod_penalty", '0000')
		This.SetItem(row, "box_threshold", 0)
	else
		This.SetItem(row, "box_threshold", This.GetItemNumber(row, "boxes_per_pallet") * ii_pallet_count)
	end if
end if
	
If dwo.Name = "when_to_produce" then
	if data = 'L' then
		This.SetItem(row, "days_prior_to_ship", 0)
	end if
end if	

If dwo.Name = "no_other_prod_penalty" then
	ll_find = Long(Mid ( data , 3 ))
	If ll_find > 59 Then
			iw_Frame.SetMicroHelp("Penalty Minutes can not be greater then 59")
			This.SetFocus()
			This.SelectText(4, Len(data))
			return 1
	End If
End If
end event

event itemerror;call super::itemerror;Return 1
end event

type tab_parameters from userobject within tab_1
integer x = 18
integer y = 152
integer width = 3429
integer height = 860
long backcolor = 67108864
string text = "Parameters"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_prod_plant_parameters dw_prod_plant_parameters
end type

on tab_parameters.create
this.dw_prod_plant_parameters=create dw_prod_plant_parameters
this.Control[]={this.dw_prod_plant_parameters}
end on

on tab_parameters.destroy
destroy(this.dw_prod_plant_parameters)
end on

type dw_prod_plant_parameters from u_base_dw_ext within tab_parameters
integer x = 14
integer y = 16
integer width = 1673
integer height = 820
integer taborder = 11
string dataobject = "d_prod_plant_parameters"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type tab_freeze_days from userobject within tab_1
string tag = "Freeze Days"
integer x = 18
integer y = 152
integer width = 3429
integer height = 860
long backcolor = 67108864
string text = "Freeze Days"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_freeze_days dw_freeze_days
end type

on tab_freeze_days.create
this.dw_freeze_days=create dw_freeze_days
this.Control[]={this.dw_freeze_days}
end on

on tab_freeze_days.destroy
destroy(this.dw_freeze_days)
end on

type dw_freeze_days from u_base_dw_ext within tab_freeze_days
integer x = 14
integer y = 16
integer width = 2112
integer height = 816
integer taborder = 11
string dataobject = "d_freeze_days"
boolean vscrollbar = true
boolean border = false
end type

