﻿$PBExportHeader$w_pas_where_produced.srw
forward
global type w_pas_where_produced from w_base_sheet_ext
end type
type dw_where_produced from u_base_dw_ext within w_pas_where_produced
end type
type dw_header from u_base_dw_ext within w_pas_where_produced
end type
end forward

global type w_pas_where_produced from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 2802
integer height = 1516
long backcolor = 67108864
dw_where_produced dw_where_produced
dw_header dw_header
end type
global w_pas_where_produced w_pas_where_produced

type variables
String	is_direction, &
			is_inquire
			
time		it_PT_Instance_date 

u_ws_pas1	iu_ws_pas1

w_planned_transfer_new	iw_planned_transfer_new

end variables

on w_pas_where_produced.create
int iCurrent
call super::create
this.dw_where_produced=create dw_where_produced
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_where_produced
this.Control[iCurrent+2]=this.dw_header
end on

on w_pas_where_produced.destroy
call super::destroy
destroy(this.dw_where_produced)
destroy(this.dw_header)
end on

event open;call super::open;// format for this string will be 'F' or 'T' ~t then header info
String	ls_header,ls_temp

ls_header = Message.StringParm


is_direction = Left(ls_header, 1)


If is_direction = 'F' Then
	This.Title = "Where Produced From"
//	ls_temp = sqlca.userid
//	If upper(ls_temp) = 'DCSCTLK' Then
//		dw_where_produced.Object.txt_terry.Visible= 1
//	Else
//		dw_where_produced.Object.txt_terry.Visible= 0
//	End If
Else
	This.Title = "Where Produced To"
End if

ls_header = Right(ls_header, Len(ls_header) - 2)

dw_header.reset( )
dw_header.ImportString(ls_header)

//dw_header.gets
end event

event ue_postopen;call super::ue_postopen;u_pas202		lu_pas202
iu_ws_pas1	= Create u_ws_pas1

s_error		lstr_error_info

String		ls_input,&
				ls_product_code,&
				ls_product_state,&
				ls_product_status,&
				ls_plant_code,&
				ls_begin_date


SetPointer(HourGlass!)
iw_Frame.SetMicroHelp("Retrieving " + This.Title + " data...")

lstr_error_info.se_user_id = sqlca.userid
lstr_error_info.se_window_name = "WherePrd"

lu_pas202 = Create u_pas202

// I'm assuming there are valid values in the header
If len(is_direction) = 0 or isnull(is_direction) or  &
	(is_direction <> 'F' and is_direction <> 'T') Then
	iw_Frame.SetMicroHelp("You must currently access this window through PA View...")
	Close(this)
	Return 1
End If

ls_product_code 	= dw_header.GetItemString(1, "fab_product_code")
ls_product_state	= dw_header.GetItemString(1, "product_state")
ls_product_status	= dw_header.GetItemString(1, "product_status")
ls_plant_code		= dw_header.GetItemString(1, "plant_code") 
ls_begin_date		= String(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd")

//IBDKEEM ** 08/26/2002 ** MRFREEZE logic until next release
If len(ls_product_state) = 0 or isnull(ls_product_state) Then
	If right(trim(ls_product_code),1) = "*" then
		ls_product_code = left(ls_product_code, pos(ls_product_code, "*") - 1)
		ls_product_state = "2"
		dw_header.setitem(1, "fab_product_code",ls_product_code)	
	else
		ls_product_state = "1"
	end if
	
	dw_header.setitem(1, "product_state",ls_product_state)
End If

ls_input = ls_product_code + '~t' + ls_product_state + '~t' + ls_product_status + '~t' + &
			  ls_plant_code	+ '~t' + ls_begin_date + '~t' + &
			  is_direction + '~r~n'

//If lu_pas202.nf_inq_where_produced(lstr_error_info, ls_input, dw_where_produced) Then
If iu_ws_pas1.nf_pasp18fr(lstr_error_info, ls_input, dw_where_produced) Then
	Iw_Frame.SetMicroHelp(String(dw_where_produced.RowCount()) + ' ' + This.Title + " Row(s) Retrieved")
End if

dw_where_produced.ResetUpdate()
dw_where_produced.SetFocus()

Destroy lu_pas202


end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_save')


end event

event deactivate;iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')
iw_frame.im_menu.mf_Enable('m_inquire')
end event

event ue_set_data;call super::ue_set_data;String	ls_temp,ls_res,ls_gpo,ls_month,ls_day,ls_year, &
			ls_order_num,ls_line_num, ls_params
			
Window	lw_temp
Int		li_rtn
Long		ll_row  

Choose Case as_data_item
	Case 'change order'
		ls_temp = dw_where_produced.GetItemString (dw_where_produced.GetRow ( ), 'from_fab_product')
		ls_order_num = left(ls_temp,5)
		ls_line_num = Mid ( ls_temp, 7 , 4)
		ls_temp = ls_order_num + '~t' + ls_line_num
		OpenSheetWithParm(lw_temp, ls_temp, "w_change_order_production_dates_new",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)

//		ls_temp = dw_where_produced.GetItemString (dw_where_produced.GetRow ( ), 'from_fab_product')
//		ls_temp = left(ls_temp,5)
//		OpenSheetWithParm(lw_temp, ls_temp, "w_change_order_production_dates_new",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
	Case 'plan tran'
//		is_inquire 	= ""
//		is_inquire 	=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'from_plant') + '~t' 
//		is_inquire +=	String(dw_where_produced.GetItemDate(dw_where_produced.GetRow(),'source_date'), "yyyy-mm-dd")+ '~t'
//		is_inquire +=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'weight_range') + '~t'
//		is_inquire += 	dw_header.GetItemString(1, "plant_code") + '~t'
//		is_inquire +=	String(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd") + '~t'
//		is_inquire +=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'sex') + '~t' 
////IBDKEEM 08/23/2002 pass to PT window to find row.		
//		is_inquire +=	trim(dw_where_produced.GetItemString(dw_where_produced.GetRow(),'from_fab_product')) + '~t'
//		is_inquire +=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'from_fab_state') + '~t' 
////		is_inquire +=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'from_fab_status') + '~t'
//		is_inquire +=  string(dw_where_produced.GetItemNumber(dw_where_produced.GetRow(),'quantity')) + '~t'  
//		IF IsValid(iw_plan_transfer) THEN
//			if NOT iw_plan_transfer.ib_pooled &
//	 		 and iw_plan_transfer.it_Instance_date = it_PT_Instance_date then
//				iw_plan_transfer.setFocus()
//				iw_plan_transfer.TriggerEvent ('ue_inquire2') 
//			else
//				iw_plan_transfer = gw_netwise_frame.iu_window_pool.uf_get_pooled_window( "w_planned_transfer", This)				
//			end if
//		else
//			iw_plan_transfer = gw_netwise_frame.iu_window_pool.uf_get_pooled_window( "w_planned_transfer", This)
//			//OpenSheetWithParm(lw_temp, This, "w_planned_transfer",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
//		end If
//		
//		it_PT_Instance_date = iw_plan_transfer.it_Instance_date

		ls_params = dw_where_produced.GetItemString(dw_where_produced.GetRow(), "from_plant") + '~t'
		ls_params += String(dw_where_produced.GetItemDate(dw_where_produced.GetRow(), "source_date"), "mm/dd/yyyy")  + '~t'  
		ls_params += dw_where_produced.GetItemString(dw_where_produced.GetRow(), "weight_range") + '~t'
		ls_params +=dw_header.GetItemString(1, "plant_code") + '~t'  
		ls_params += String(dw_header.GetItemDate(1, "begin_date"), "mm/dd/yyyy") + '~t' 
		ls_params += dw_where_produced.GetItemString(dw_where_produced.GetRow(), "sex") + '~t'
		ls_params += dw_header.GetItemString(1, "fab_product_code") + '~t'
		
		If isValid(iw_planned_transfer_new) then
			iw_planned_transfer_new.SetFocus()
			iw_planned_transfer_new.wf_pass_params(ls_params)
		Else	
			OpenSheetWithParm ( iw_planned_transfer_new, ls_params, "w_planned_transfer_new", iw_frame	, 0, iw_frame.im_menu.iao_arrangeopen )
		End If
	
	Case 'sched det'
		ls_temp 	= ""
		ls_temp 	=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'from_plant') + '~t' 
	//	ls_temp 	=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'plant_description') + '~t' 
		ls_temp +=	trim(dw_where_produced.GetItemString(dw_where_produced.GetRow(),'from_fab_product')) + '~t'
	//	ls_temp 	=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'fab_product_descr') + '~t' 
		ls_temp +=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'from_fab_state') + '~t' 
		ls_temp +=	dw_where_produced.GetItemString(dw_where_produced.GetRow(),'from_fab_status') + '~t'
		ls_temp +=	String(dw_where_produced.GetItemDate(dw_where_produced.GetRow(),'source_date'), "yyyy-mm-dd")+ '~t'
		OpenSheetWithParm(lw_temp, ls_temp, "w_raw_mat_det_sched",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)	
End Choose

end event

event resize;call super::resize;//constant integer li_x		= 9//40 //0
//constant integer li_y		= 188//26 //96

integer li_x		= 39
integer li_y		= 218

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

  
//dw_where_produced.width	= newwidth - (30 + li_x)
//dw_where_produced.height	= newheight - (30 + li_y)

dw_where_produced.width	= newwidth - (li_x)
dw_where_produced.height	= newheight - (li_y)
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	case 'Inquire'		
		message.StringParm = is_inquire
End Choose
end event

event close;call super::close;Destroy iu_ws_pas1
end event

type dw_where_produced from u_base_dw_ext within w_pas_where_produced
integer y = 192
integer width = 2706
integer height = 1152
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_pa_view_where_produced"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;ib_updateable = False
is_selection = '1'
end event

event rbuttondown;call super::rbuttondown;m_detail_information	lm_popup
String ls_temp
integer li_rtn

IF row > 0 Then
	dw_where_produced.SetRow ( row )
	dw_where_produced.SelectRow ( row, True )
	lm_popup = Create m_detail_information
	ls_temp = This.GetItemString ( row, "source_fab_product")
	CHOOSE CASE ls_temp
	CASE 'PLAN TRAN '
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		If iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_planned_transfer") Then
			lm_popup.m_manifestdetail.m_plannedtransfer.Enable()
		Else
			lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		End If			
		lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
		lm_popup.m_manifestdetail.m_scheduledetail.Disable()
	CASE 'TRAN ORDER'
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		lm_popup.m_manifestdetail.m_scheduledetail.Disable()
		If iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_change_order_production_dates_new") Then
			lm_popup.m_manifestdetail.m_changeorderproductiondates.Enable()
		Else
			lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
		End If	
	CASE 'RAW MAT   '
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
		If iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_raw_mat_det_sched") Then
			lm_popup.m_manifestdetail.m_scheduledetail.Enable()
		Else
			lm_popup.m_manifestdetail.m_scheduledetail.Disable()
		End If		
	CASE Else
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
		lm_popup.m_manifestdetail.m_scheduledetail.Disable()
	END CHOOSE
	lm_popup.m_manifestdetail.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())
	Destroy lm_popup
End If


end event

type dw_header from u_base_dw_ext within w_pas_where_produced
integer width = 2181
integer height = 172
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_where_produced_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
end event

