﻿$PBExportHeader$w_planned_transfer_new.srw
forward
global type w_planned_transfer_new from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_planned_transfer_new
end type
type dw_header from u_base_dw_ext within w_planned_transfer_new
end type
end forward

global type w_planned_transfer_new from w_base_sheet_ext
integer width = 4165
integer height = 1204
string title = "Planned Transfer"
long backcolor = 67108864
dw_detail dw_detail
dw_header dw_header
end type
global w_planned_transfer_new w_planned_transfer_new

type variables
String			is_open_parm 

u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

DataStore	ids_originaldata

Boolean		ib_bypassclosequery

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_addrow ()
public function boolean wf_update ()
public subroutine wf_apply_ie_plants (ref string as_include_exclude, ref string as_plant_string, long al_detail_row_number)
public function boolean wf_edit_rows ()
public subroutine wf_delete ()
public function boolean wf_deleterow ()
public subroutine wf_filenew ()
public subroutine wf_unprotect_header ()
public subroutine wf_protect_header ()
public subroutine wf_pass_params (string as_params)
public subroutine wf_process_inquire ()
end prototypes

public function boolean wf_retrieve ();String			ls_input_string, &
				ls_output_string

Long			ll_rowcount, ll_sub	, ll_deleted_rows

Decimal{2}	ldt_total_quantity

dw_header.ResetUpdate()

ll_deleted_rows = dw_detail.DeletedCount()

if ib_bypassclosequery then
	//do nothing
else
	Call w_base_sheet::closequery
end if

ib_bypassclosequery = False

ls_input_string =	'U' + '~t' + & 	
						dw_header.GetItemString(1, "ship_plant") + '~t' + &
	  					String(dw_header.GetItemDate(1, "ship_date"), "yyyy-mm-dd") + '~t' + &
						dw_header.GetItemString(1, "ship_period") + '~t' + &
						dw_header.GetItemString(1, "delivery_plant") + '~t' + &
						String(dw_header.GetItemDate(1, "delivery_date"), "yyyy-mm-dd") + '~t' + &
						dw_header.GetItemString(1, "delivery_period") + '~t'

SetRedraw (False)

If Not iu_ws_pas_share.nf_pasp05gr(istr_error_info, &
													ls_input_string, &
													ls_output_string) Then												
													
				
													
	SetRedraw(True)
	Return False
End If

dw_detail.Reset()
dw_detail.ImportString(ls_output_string)

ll_rowcount = dw_detail.RowCount()
For ll_sub = ll_rowcount to 1 Step -1
	If ll_sub = 1 then
		ldt_total_quantity += dw_detail.GetItemNumber(ll_sub, "transfer_qty")
		dw_detail.SetItem(ll_sub, "total_quantity", ldt_total_quantity)
	Else
		if dw_detail.GetItemString(ll_sub, "sku_product_code") = dw_detail.GetItemString(ll_sub - 1, "sku_product_code") then
			ldt_total_quantity += dw_detail.GetItemNumber(ll_sub, "transfer_qty")
			dw_detail.SetItem(ll_sub, "total_quantity", 0)
		else	
			ldt_total_quantity += dw_detail.GetItemNumber(ll_sub, "transfer_qty")
			dw_detail.SetItem(ll_sub, "total_quantity", ldt_total_quantity)
			ldt_total_quantity = 0
		End If
	End If
Next

ids_originaldata = Create DataStore
ids_originaldata.DataObject = 'd_planned_transfer_detail'
ids_originaldata.Reset()
ids_originaldata.ImportString(ls_output_string)
ids_originaldata.ResetUpdate()

dw_detail.ResetUpdate()
dw_detail.SetFocus()

wf_protect_header()

SetRedraw (True)

Return True
end function

public function boolean wf_addrow ();Long	ll_newrow

ll_newrow = dw_detail.InsertRow(0) 

dw_detail.SetItem(ll_newrow, "item_number", 0)
dw_detail.SetItem(ll_newrow, "sku_product_code", "")
dw_detail.SetItem(ll_newrow, "product_state", "1")
dw_detail.SetItem(ll_newrow, "from_production_date", dw_header.GetItemDate(1, "ship_date"))
dw_detail.SetItem(ll_newrow, "to_production_date",  dw_header.GetItemDate(1, "ship_date"))
dw_detail.SetItem(ll_newrow, "from_supply_date", dw_header.GetItemDate(1, "ship_date"))
dw_detail.SetItem(ll_newrow, "to_supply_date", dw_header.GetItemDate(1, "ship_date"))
dw_detail.SetItem(ll_newrow, "latest_delivery_date",  dw_header.GetItemDate(1, "delivery_date"))
dw_detail.SetItem(ll_newrow, "latest_delivery_period", Integer(dw_header.GetItemString(1, "delivery_period")))
dw_detail.SetItem(ll_newrow, "ave_product_weight", 0)
dw_detail.SetItem(ll_newrow, "transfer_qty", 0)
dw_detail.SetItem(ll_newrow, "transfer_status", "M")
dw_detail.SetItem(ll_newrow, "include_exclude", " ")
dw_detail.SetItem(ll_newrow, "plant_list", "")
dw_detail.SetItem(ll_newrow, "planned_transfer_type", "D")
dw_detail.SetItem(ll_newrow, "total_quantity", 0)
dw_detail.SetItem(ll_newrow, "action_ind", 'N')

dw_detail.ScrolltoRow(ll_newrow)
dw_detail.SetRow(ll_newrow)

Return True


end function

public function boolean wf_update ();Long		ll_row, ll_RowCount, ll_deletedRowCount

String		ls_header_string, ls_update

ls_header_string = dw_header.GetItemString(1, "ship_plant") +"~t"
ls_header_string += String(dw_header.GetItemDate(1, "ship_date"), "yyyy-mm-dd") +"~t"
ls_header_string += dw_header.GetItemString(1, "ship_period") +"~t"
ls_header_string += dw_header.GetItemString(1, "delivery_plant") +"~t"
ls_header_string += String(dw_header.GetItemDate(1, "delivery_date"), "yyyy-mm-dd") +"~t"
ls_header_string += dw_header.GetItemString(1, "delivery_period") +"~r~n"

ls_update = ''

dw_detail.AcceptText()

ll_RowCount = dw_detail.RowCount()

If not wf_edit_rows() then Return False

Do 
	ll_row = dw_detail.GetNextModified(ll_row, Primary!)
	if ll_row > 0 then
		If (dw_detail.getItemstring(ll_row,'action_ind') = 'C') or (dw_detail.getItemstring(ll_row,'action_ind') = 'T')  Then
			ls_update += 'C' + '~t' 
			ls_update += dw_detail.getitemstring(ll_row,'sku_product_code') + '~t' 
			ls_update += dw_detail.getitemstring(ll_row,'product_state') + '~t' 
			ls_update += String(dw_detail.getitemdate(ll_row,'from_production_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(dw_detail.getitemdate(ll_row,'to_production_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(dw_detail.getitemdate(ll_row,'from_supply_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(dw_detail.getitemdate(ll_row,'to_supply_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(dw_detail.getitemdate(ll_row,'latest_delivery_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(dw_detail.getitemNumber(ll_row,'latest_delivery_period')) + '~t' 
			ls_update += String(dw_detail.getitemNumber(ll_row,'transfer_qty')) + '~t' 
			ls_update += dw_detail.getitemstring(ll_row,'include_exclude') + '~t'
			ls_update += dw_detail.getitemstring(ll_row,'plant_list') + '~t' 
			ls_update += dw_detail.getitemstring(ll_row,'planned_transfer_type') + '~r~n' 
		End If
		If (dw_detail.getItemstring(ll_row,'action_ind') = 'C') Then
			ls_update += 'X' + '~t' 
			ls_update += ids_originaldata.getitemstring(ll_row,'sku_product_code') + '~t' 
			ls_update += ids_originaldata.getitemstring(ll_row,'product_state') + '~t' 
			ls_update += String(ids_originaldata.getitemdate(ll_row,'from_production_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(ids_originaldata.getitemdate(ll_row,'to_production_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(ids_originaldata.getitemdate(ll_row,'from_supply_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(ids_originaldata.getitemdate(ll_row,'to_supply_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(ids_originaldata.getitemdate(ll_row,'latest_delivery_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(ids_originaldata.getitemNumber(ll_row,'latest_delivery_period')) + '~t' 
			ls_update += '0' + '~t' 
			ls_update += ids_originaldata.getitemstring(ll_row,'include_exclude') + '~t'
			ls_update += ids_originaldata.getitemstring(ll_row,'plant_list') + '~t' 
			ls_update += ids_originaldata.getitemstring(ll_row,'planned_transfer_type') + '~r~n' 
		End If
		If (dw_detail.getItemstring(ll_row,'action_ind') = 'N') Then
			ls_update += 'N' + '~t' 
			ls_update += dw_detail.getitemstring(ll_row,'sku_product_code') + '~t' 
			ls_update += dw_detail.getitemstring(ll_row,'product_state') + '~t' 
			ls_update += String(dw_detail.getitemdate(ll_row,'from_production_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(dw_detail.getitemdate(ll_row,'to_production_date'), 'yyyy-mm-dd') + '~t' 
			
//	 	On a new line, from/to production dates and from/to supply dates were set to ship date from header
//		If production date is changed and supply date is not, change supply dates to match production dates

			If (dw_detail.getitemdate(ll_row,'from_supply_date') = dw_header.GetItemDate(1, 'ship_date'))  &
					AND (dw_detail.getitemdate(ll_row, 'from_production_date') <> dw_header.GetItemDate(1, 'ship_date')) then
				dw_detail.SetItem(ll_row, 'from_supply_date', dw_detail.GetItemDate(ll_row, 'from_production_date'))	
				ls_update += String(dw_detail.getitemdate(ll_row,'from_supply_date'), 'yyyy-mm-dd') + '~t' 	
			else
				ls_update += String(dw_detail.getitemdate(ll_row,'from_supply_date'), 'yyyy-mm-dd') + '~t' 
			end if
				
			If (dw_detail.getitemdate(ll_row,'to_supply_date') = dw_header.GetItemDate(1, 'ship_date'))  &
					AND (dw_detail.getitemdate(ll_row, 'to_production_date') <> dw_header.GetItemDate(1, 'ship_date')) then
				dw_detail.SetItem(ll_row, 'to_supply_date', dw_detail.GetItemDate(ll_row, 'to_production_date'))	
				ls_update += String(dw_detail.getitemdate(ll_row,'to_supply_date'), 'yyyy-mm-dd') + '~t' 	
			else
				ls_update += String(dw_detail.getitemdate(ll_row,'to_supply_date'), 'yyyy-mm-dd') + '~t' 
			end if				

			ls_update += String(dw_detail.getitemdate(ll_row,'latest_delivery_date'), 'yyyy-mm-dd') + '~t' 
			ls_update += String(dw_detail.getitemNumber(ll_row,'latest_delivery_period')) + '~t' 
			ls_update += String(dw_detail.getitemNumber(ll_row,'transfer_qty')) + '~t' 
			ls_update += dw_detail.getitemstring(ll_row,'include_exclude') + '~t'
			ls_update += dw_detail.getitemstring(ll_row,'plant_list') + '~t' 
			ls_update += dw_detail.getitemstring(ll_row,'planned_transfer_type') + '~r~n' 
		End If		
	end if
	
Loop While ll_row > 0 and ll_row < ll_RowCount

ll_deletedRowCount = dw_detail.DeletedCount()


For ll_row = 1 to ll_deletedRowCount
	ls_update += 'X' + '~t' 
	ls_update += dw_detail.getitemstring(ll_row,'sku_product_code', Delete!, True) + '~t' 
	ls_update += dw_detail.getitemstring(ll_row,'product_state', Delete!, True) + '~t' 
	ls_update += String(dw_detail.getitemdate(ll_row,'from_production_date', Delete!, True), 'yyyy-mm-dd') + '~t' 
	ls_update += String(dw_detail.getitemdate(ll_row,'to_production_date', Delete!, True), 'yyyy-mm-dd') + '~t' 
	ls_update += String(dw_detail.getitemdate(ll_row,'from_supply_date', Delete!, True), 'yyyy-mm-dd') + '~t' 
	ls_update += String(dw_detail.getitemdate(ll_row,'to_supply_date', Delete!, True), 'yyyy-mm-dd') + '~t' 
	ls_update += String(dw_detail.getitemdate(ll_row,'latest_delivery_date', Delete!, True), 'yyyy-mm-dd') + '~t' 
	ls_update += String(dw_detail.getitemNumber(ll_row,'latest_delivery_period', Delete!, True)) + '~t' 
	ls_update += '0' + '~t' 
	ls_update += dw_detail.getitemstring(ll_row,'include_exclude', Delete!, True) + '~t'
	ls_update += dw_detail.getitemstring(ll_row,'plant_list', Delete!, True) + '~t' 
	ls_update += dw_detail.getitemstring(ll_row,'planned_transfer_type', Delete!, True) + '~r~n' 
Next
	
If Not iu_ws_pas_share.nf_pasp06gr(istr_error_info, &
													ls_header_string, &
													ls_update) Then												
													
				
													
	SetRedraw(True)
	Return False
End If

ib_bypassclosequery = True
wf_retrieve()
iw_frame.SetMicroHelp("Update Successful")
dw_detail.ResetUpdate()

Return True


end function

public subroutine wf_apply_ie_plants (ref string as_include_exclude, ref string as_plant_string, long al_detail_row_number);String			ls_return, ls_open_parms 



u_string_functions			lu_string_functions

ls_open_parms = as_include_exclude + '~t' + as_plant_string 
OpenWithParm(w_planned_transfer_plant_resp, ls_open_parms)

ls_return = Message.StringParm

If ls_return = 'Cancel' Then
	dw_detail.SetItem(al_detail_row_number, "include_exclude", as_include_exclude)
	dw_detail.SetItem(al_detail_row_number, "plant_list", as_plant_string)
Else
	as_include_exclude = lu_string_functions.nf_gettoken(ls_return, '~t')
	as_plant_string = lu_string_functions.nf_gettoken(ls_return, '~t')
	dw_detail.SetItem(al_detail_row_number, "include_exclude", as_include_exclude)
	dw_detail.SetItem(al_detail_row_number, "plant_list", as_plant_string)	
	
	If dw_detail.GetItemString(al_detail_row_number, "action_ind") = 'N' Then
		// do nothing for a new line //
	Else
		dw_detail.SetItem(al_detail_row_number, "action_ind", "C") 
	End If
End If
	


end subroutine

public function boolean wf_edit_rows ();Long	ll_row, ll_rowcount

Do 
	ll_row = dw_detail.GetNextModified(ll_row, Primary!)
	if ll_row > 0 then
		if dw_detail.getitemNumber(ll_row,'transfer_qty') > 0 then
			if dw_detail.getitemstring(ll_row,'sku_product_code') <= '          ' then
				iw_Frame.SetMicroHelp('Product Code not Found')
				Return False
				end if
		end if		
		
		if dw_detail.GetItemDate(ll_row, "from_production_date") > dw_detail.GetItemDate(ll_row, "to_production_date") then
			iw_Frame.SetMicroHelp("'From Production Date cannot be greater than To Production Date")
			Return False
		end if	

		if dw_detail.GetItemDate(ll_row, "from_supply_date") > dw_detail.GetItemDate(ll_row, "to_supply_date") then
			iw_Frame.SetMicroHelp("'From Supply Date cannot be greater than To Supply Date")
			Return False
		end if	
		
		if dw_detail.GetItemDate(ll_row, "from_supply_date") < dw_detail.GetItemDate(ll_row, "from_production_date") then
			iw_Frame.SetMicroHelp("'From Supply Date cannot be earlier than the From Production Date")
			Return False
		end if	
		
		if dw_detail.GetItemDate(ll_row, "to_supply_date") > dw_detail.GetItemDate(ll_row, "to_production_date") then
			iw_Frame.SetMicroHelp("'To Supply Date cannot be greater than the To Production Date")
			Return False
		end if	
		
		If dw_detail.GetItemString(ll_row, "action_ind") = 'N' Then // new line
		      // do nothing
		Else
			If (dw_detail.GetItemDate(ll_row, "to_supply_date") = ids_originaldata.GetItemDate(ll_row, "to_supply_date")) AND &
					(dw_detail.GetItemDate(ll_row, "to_supply_date") <> dw_detail.GetItemDate(ll_row, "to_production_date")) then
				dw_detail.SetItem(ll_row, "to_supply_date", dw_detail.GetItemDate(ll_row, "to_production_date"))
			end if
	End If
	
	end if	
	
Loop While ll_row > 0 and ll_row < ll_RowCount

Return True
end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_deleterow ();Long		ll_selectedrow

ll_selectedrow = dw_detail.GetSelectedRow(0)

If ll_selectedrow = 0 Then
		iw_frame.SetMicroHelp("Select at least 1 row to delete")
	return True
End If

If MessageBox("Delete Rows", "Are you sure you want to delete the selected row?", &
		Question!, YesNo!, 2) = 2 Then Return True
		
if ll_selectedrow > 0 Then
	dw_detail.DeleteRow(ll_selectedrow)
	ids_originaldata.DeleteRow(ll_selectedrow)
End If

// set the next row selected, unless at the bottom of the datawindow
if ll_selectedrow <= dw_detail.RowCount() Then
	dw_detail.SelectRow(ll_selectedrow, True)
End if

Return True
end function

public subroutine wf_filenew ();wf_unprotect_header()

dw_header.SetFocus()

dw_detail.Reset()
end subroutine

public subroutine wf_unprotect_header ();dw_header.object.ship_plant.Protect = 0
dw_header.object.ship_plant.background.color = 16777215

dw_header.object.ship_date.Protect = 0
dw_header.object.ship_date.background.color = 16777215

dw_header.object.ship_period.Protect = 0
dw_header.object.ship_period.background.color = 16777215

dw_header.object.delivery_plant.Protect = 0
dw_header.object.delivery_plant.background.color = 16777215

dw_header.object.delivery_date.Protect = 0
dw_header.object.delivery_date.background.color = 16777215

dw_header.object.delivery_period.Protect = 0
dw_header.object.delivery_period.background.color = 16777215
end subroutine

public subroutine wf_protect_header ();dw_header.object.ship_plant.Protect = 1
dw_header.object.ship_plant.background.color = 78682240

dw_header.object.ship_date.Protect = 1
dw_header.object.ship_date.background.color = 78682240

dw_header.object.ship_period.Protect = 1
dw_header.object.ship_period.background.color = 78682240

dw_header.object.delivery_plant.Protect = 1
dw_header.object.delivery_plant.background.color = 78682240

dw_header.object.delivery_date.Protect = 1
dw_header.object.delivery_date.background.color = 78682240

dw_header.object.delivery_period.Protect = 1
dw_header.object.delivery_period.background.color = 78682240

end subroutine

public subroutine wf_pass_params (string as_params);is_open_parm = as_params

wf_process_inquire()
		

end subroutine

public subroutine wf_process_inquire ();String		ls_ship_plant, ls_ship_date, ls_ship_period, &
			ls_delivery_plant, ls_delivery_date, ls_delivery_period, ls_params , ls_product

DataWindowChild	ldwch_ship_plant, ldwch_delv_plant				
			
Long		ll_found, ll_row

		
ls_ship_plant =  iw_frame.iu_string.nf_GetToken(is_open_parm, '~t')
ls_ship_date =  iw_frame.iu_string.nf_GetToken(is_open_parm, '~t')
ls_ship_period =  iw_frame.iu_string.nf_GetToken(is_open_parm, '~t')

ls_delivery_plant =  iw_frame.iu_string.nf_GetToken(is_open_parm, '~t')
ls_delivery_date =  iw_frame.iu_string.nf_GetToken(is_open_parm, '~t')	
ls_delivery_period = iw_frame.iu_string.nf_GetToken(is_open_parm, '~t')
ls_product = iw_frame.iu_string.nf_GetToken(is_open_parm, '~t')

dw_header.SetItem(1,"ship_plant", ls_ship_plant)
dw_header.SetItem(1,"ship_date", Date(ls_ship_date))
dw_header.SetItem(1,"ship_period", ls_ship_period)

dw_header.SetItem(1,"delivery_plant", ls_delivery_plant)
dw_header.SetItem(1,"delivery_date", Date(ls_delivery_date))
dw_header.SetItem(1,"delivery_period", ls_delivery_period)

dw_header.GetChild('ship_plant', ldwch_ship_plant)
dw_header.GetChild('delivery_plant', ldwch_delv_plant)

ll_found = ldwch_ship_plant.Find("location_code = '" + ls_ship_plant + "'", 1, ldwch_ship_plant.RowCount())			
if ll_found < 1 then
	iw_frame.SetMicrohelp('Ship plant is invalid')
	dw_header.SetColumn ('ship_plant')
else
	dw_header.SetItem(1, "ship_plant_desc", ldwch_ship_plant.GetItemString(ll_found, "location_name")) 		
end if

ll_found = ldwch_delv_plant.Find("location_code = '" + ls_delivery_plant + "'", 1, ldwch_delv_plant.RowCount())			
if ll_found < 1 then
	iw_frame.SetMicrohelp('Delivery plant is invalid')
	dw_header.SetColumn ('delivery_plant')
else
	dw_header.SetItem(1, "delivery_plant_desc", ldwch_delv_plant.GetItemString(ll_found, "location_name")) 		
end if

dw_header.ResetUpdate()

wf_retrieve()

if ls_product > '          ' then
	ll_row = dw_detail.Find("sku_product_code = '" + ls_product + "'", 1, dw_detail.RowCount())
	if ll_row > 0 Then
//	this is a bit of a hack, scroll past the row (ll_row + 25), then scroll to row(ll_row), I'm trying to put selected row at top of window	IBDKSCS	
		dw_detail.ScrolltoRow(ll_row + 25)
		dw_detail.ScrolltoRow(ll_row)
		dw_detail.SelectRow(0, False)
		dw_detail.SelectRow(ll_row, True)
		dw_detail.SetRow(ll_row)
		dw_detail.SetColumn(4)
	end if
end if




end subroutine

on w_planned_transfer_new.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_planned_transfer_new.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event open;call super::open;is_open_parm = Message.StringParm

iu_ws_pas_share = Create u_ws_pas_share


end event

event ue_postopen;call super::ue_postopen;ib_bypassclosequery = False

wf_process_inquire()
end event

event resize;call super::resize;long	ll_x = 50, ll_y = 105

if il_BorderPaddingWidth > ll_x Then
	ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
	ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(&
						this.width - dw_detail.x - ll_x, &
						this.height - dw_detail.y - ll_y)
end event

event ue_fileprint;call super::ue_fileprint;DataStore					lds_print

DataWindowChild			ldwc_type

lds_print = create u_print_datastore
lds_print.DataObject = 'd_planned_transfer_detail_print'

lds_print.GetChild("planned_transfer_type", ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PTFRTYPE")

lds_print.GetChild("product_state", ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PRDSTAT ")

dw_detail.sharedata(lds_print)

lds_print.Object.t_ship_plant_code.Text = dw_header.GetItemString(1, "ship_plant")
lds_print.Object.t_ship_plant_name.Text = dw_header.GetItemString(1, "ship_plant_desc")
lds_print.Object.t_ship_date.Text = String(dw_header.GetItemDate(1, "ship_date"),'mm/dd/yyyy')
lds_print.Object.t_ship_period.Text = dw_header.GetItemString(1, "ship_period")

lds_print.Object.t_delivery_plant_code.Text = dw_header.GetItemString(1, "delivery_plant")
lds_print.Object.t_delivery_plant_name.Text = dw_header.GetItemString(1, "delivery_plant_desc")
lds_print.Object.t_delivery_date.Text = String(dw_header.GetItemDate(1, "delivery_date"),'mm/dd/yyyy')
lds_print.Object.t_delivery_period.Text = dw_header.GetItemString(1, "delivery_period")

lds_print.print()
end event

type dw_detail from u_base_dw_ext within w_planned_transfer_new
integer y = 288
integer width = 4119
integer height = 768
integer taborder = 20
string dataobject = "d_planned_transfer_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
boolean ib_updateable = true
end type

event constructor;call super::constructor;DataWindowChild	ldwc_type

IF This.RowCount() = 0 then This.InsertRow(0)

This.GetChild("planned_transfer_type", ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PTFRTYPE")

This.GetChild("product_state", ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PRDSTAT ")

is_selection= "1"
ib_Updateable = True
end event

event clicked;call super::clicked;String		ls_include_exclude, ls_plant_string

If Row > 0 Then
	Choose Case dwo.name
		Case 'plant_list'	
			ls_include_exclude = This.GetItemString(row, "include_exclude")
			ls_plant_string = This.GetItemString(row, "plant_list")
			wf_apply_ie_plants(ls_include_exclude, ls_plant_string, row)			
	END Choose
End If		
end event

event itemchanged;call super::itemchanged;If dwo.name = "transfer_qty" Then
	If this.GetItemString(row, "action_ind") <= ' ' Then
		this.SetItem(row, "action_ind", "T")  //only transfer qty changed, update will only need to send a C changed line //
	End If
Else
	If This.GetItemString(row, "action_ind") = 'N' Then
		// do nothing for a new line //
	Else
		This.SetItem(row, "action_ind", "C")  // for any other fields updated, update will need to send a C changed line and X deleted line
	End If
End If

If dwo.name = "to_production_date" Then
	If This.GetItemString(row, "action_ind") = 'N' Then  
		// new line
		If dw_detail.GetItemDate(row, "to_supply_date") <> date(data) then
			dw_detail.SetItem(row, "to_supply_date", date(data))
		End If
	Else
		// existing line
		If (dw_detail.GetItemDate(row, "to_supply_date") = ids_originaldata.GetItemDate(row, "to_supply_date")) AND &
					(dw_detail.GetItemDate(row, "to_supply_date") <> date(data)) then
				dw_detail.SetItem(row, "to_supply_date", date(data))
		end if
	End If
end if


	

		
end event

type dw_header from u_base_dw_ext within w_planned_transfer_new
integer x = 5
integer y = 8
integer width = 3259
integer height = 268
integer taborder = 10
string dataobject = "d_planned_transfer_header"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)

DataWindowChild	ldwch_ship_plant, ldwch_delv_plant

dw_header.GetChild('ship_plant', ldwch_ship_plant)

CONNECT USING SQLCA;
ldwch_ship_plant.SetTransObject(SQLCA)
ldwch_ship_plant.Retrieve()

dw_header.GetChild('delivery_plant', ldwch_delv_plant)
CONNECT USING SQLCA;
ldwch_delv_plant.SetTransObject(SQLCA)
ldwch_delv_plant.Retrieve()

end event

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_temp
Long					ll_found


CHOOSE CASE This.GetColumnName()
		
	Case "ship_plant"
			dw_header.GetChild("ship_plant", ldwc_temp)
			ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
			if ll_found < 1 then
				iw_frame.SetMicrohelp('Ship plant is invalid')
				This.SetColumn ('ship_plant')
				Return 1
			else
				dw_header.SetItem(1, "ship_plant_desc", ldwc_temp.GetItemString(ll_found, "location_name")) 
				SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipplant",Trim(data))
			end if
			
	Case	"delivery_plant"
			dw_header.GetChild("delivery_plant", ldwc_temp)
			ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())			
			if ll_found < 1 then
				iw_frame.SetMicrohelp('Delivery plant is invalid')
				This.SetColumn ('delivery_plant')
				Return 1
			else
				dw_header.SetItem(1, "delivery_plant_desc", ldwc_temp.GetItemString(ll_found, "location_name")) 		
				SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvplant",Trim(data))
			end if	
			
//	Case "ship_begin_date"		
//		if date(data) > dw_header.GetItemDate(1, "ship_end_date") then
//			iw_frame.SetMicroHelp("Ship Begin Date is greater than Ship End Date")
//		end if
//		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipbegindate",String(date(data),"yyyy-mm-dd"))
//		
//	Case "ship_end_date"		
//		if dw_header.GetItemDate(1, "ship_begin_date") > date(data) then
//			iw_frame.SetMicroHelp("Ship Begin Date is greater than Ship End Date")
//		else
//			if date(data) > idt_pa_range_date Then
//				iw_frame.SetMicroHelp("Ship End Date must be within the PA period, max. date is " + String(idt_pa_range_date, 'mm/dd/yyyy')) 
//			End If		
//		End If
//		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipenddate",String(date(data),"yyyy-mm-dd"))		
//		
//	Case "delivery_begin_date"		
//		if date(data) > dw_header.GetItemDate(1, "delivery_end_date") then
//			iw_frame.SetMicroHelp("Delivery Begin Date is greater than Delivery End Date")
//		end if
//		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvbegindate",String(date(data),"yyyy-mm-dd"))	
//		
//	Case "delivery_end_date"	
//		if dw_header.GetItemDate(1, "delivery_begin_date") > date(data) then
//			iw_frame.SetMicroHelp("Delivery Begin Date is greater than Delivery End Date")
//		else
//			if date(data) > idt_pa_range_date then
//				iw_frame.SetMicroHelp("The date of " + string(date(data),'mm/dd/yyyy') + " is beyonod the daily PA Range -- the " + &
//					" date has been changed to the next Sunday date")
//				Parent.postEvent("ue_change_delivery_date")	
//			end if
//		end if
//			
//		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvenddate",String(date(data),"yyyy-mm-dd"))		
		
	Case "ship_period"		
		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipperiod",Trim(data))		
		
	Case "delivery_period"		
		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvperiod",Trim(data))		
			
End Choose
	
end event

