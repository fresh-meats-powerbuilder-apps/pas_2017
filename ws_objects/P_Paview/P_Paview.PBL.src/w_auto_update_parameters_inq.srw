﻿$PBExportHeader$w_auto_update_parameters_inq.srw
forward
global type w_auto_update_parameters_inq from w_base_response_ext
end type
type dw_auto_update_type from u_base_dw_ext within w_auto_update_parameters_inq
end type
type st_1 from statictext within w_auto_update_parameters_inq
end type
type dw_plant from u_plant within w_auto_update_parameters_inq
end type
type dw_day_of_week from u_base_dw_ext within w_auto_update_parameters_inq
end type
type st_2 from statictext within w_auto_update_parameters_inq
end type
type cbx_plant from checkbox within w_auto_update_parameters_inq
end type
type cbx_day_of_week from checkbox within w_auto_update_parameters_inq
end type
end forward

global type w_auto_update_parameters_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1915
integer height = 732
long backcolor = 67108864
dw_auto_update_type dw_auto_update_type
st_1 st_1
dw_plant dw_plant
dw_day_of_week dw_day_of_week
st_2 st_2
cbx_plant cbx_plant
cbx_day_of_week cbx_day_of_week
end type
global w_auto_update_parameters_inq w_auto_update_parameters_inq

on w_auto_update_parameters_inq.create
int iCurrent
call super::create
this.dw_auto_update_type=create dw_auto_update_type
this.st_1=create st_1
this.dw_plant=create dw_plant
this.dw_day_of_week=create dw_day_of_week
this.st_2=create st_2
this.cbx_plant=create cbx_plant
this.cbx_day_of_week=create cbx_day_of_week
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_auto_update_type
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_plant
this.Control[iCurrent+4]=this.dw_day_of_week
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.cbx_plant
this.Control[iCurrent+7]=this.cbx_day_of_week
end on

on w_auto_update_parameters_inq.destroy
call super::destroy
destroy(this.dw_auto_update_type)
destroy(this.st_1)
destroy(this.dw_plant)
destroy(this.dw_day_of_week)
destroy(this.st_2)
destroy(this.cbx_plant)
destroy(this.cbx_day_of_week)
end on

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('parameter_type')
If Message.StringParm = '' Then
	dw_auto_update_type.SetItem(1, "auto_update_type", "ATPRDUPD")
else
	dw_auto_update_type.SetItem(1, "auto_update_type", Message.StringParm)
end If

iw_parentwindow.Event ue_get_data('location')
If Message.Stringparm = 'ALL' Then
	cbx_plant.Checked = False
	dw_plant.uf_display_label_only()
	//dw_plant.Visible = False
Else	
	cbx_plant.Checked = True
	dw_plant.uf_display_all()
	dw_plant.uf_set_plant_code(Message.StringParm)
End if

iw_parentwindow.Event ue_get_data('day_of_week')
If Message.StringParm = 'A' Then
	cbx_day_of_week.Checked = False
	dw_day_of_week.Visible = False
else	
	cbx_day_of_week.Checked = True
	dw_day_of_week.Visible = True
	If Message.StringParm = '' Then
		dw_day_of_week.SetItem(1, "day_of_week", "1")
	else
		dw_day_of_week.SetItem(1, "day_of_week", Message.StringParm)
	end if
end If



end event

event ue_base_ok;call super::ue_base_ok;dw_auto_update_type.AcceptText()
dw_plant.AcceptText()
dw_day_of_week.AcceptText()

iw_parentwindow.Event ue_set_data('parameter_type', dw_auto_update_type.GetItemString(1, "auto_update_type"))

If cbx_plant.Checked Then
	iw_parentwindow.Event ue_set_data('location', dw_plant.uf_get_plant_code())
Else
	iw_parentwindow.Event ue_set_data('location', 'ALL')
End If

If cbx_day_of_week.Checked Then
	iw_parentwindow.Event ue_set_data('day_of_week', dw_day_of_week.GetItemString(1, "day_of_week"))
Else
	iw_parentwindow.Event ue_set_data('day_of_week', 'A')
End If

ib_ok_to_close = True

Close(This)

end event

event ue_base_cancel;call super::ue_base_cancel;Close (This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_auto_update_parameters_inq
integer x = 951
integer y = 872
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_auto_update_parameters_inq
integer x = 910
integer y = 512
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_auto_update_parameters_inq
integer x = 567
integer y = 512
end type

type dw_auto_update_type from u_base_dw_ext within w_auto_update_parameters_inq
integer x = 594
integer y = 76
integer width = 1129
integer height = 72
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_auto_update_type"
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild('auto_update_type', ldwc_temp)

ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()







end event

type st_1 from statictext within w_auto_update_parameters_inq
integer x = 416
integer y = 88
integer width = 169
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Type:"
boolean focusrectangle = false
end type

type dw_plant from u_plant within w_auto_update_parameters_inq
integer x = 407
integer y = 208
integer width = 1435
integer taborder = 11
boolean bringtotop = true
end type

type dw_day_of_week from u_base_dw_ext within w_auto_update_parameters_inq
integer x = 571
integer y = 344
integer width = 489
integer height = 72
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_day_of_week"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type st_2 from statictext within w_auto_update_parameters_inq
integer x = 247
integer y = 360
integer width = 343
integer height = 52
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Day of Week:"
boolean focusrectangle = false
end type

type cbx_plant from checkbox within w_auto_update_parameters_inq
integer x = 96
integer y = 208
integer width = 82
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 16777215
end type

event clicked;If This.Checked Then
	//dw_plant.Visible = True
	dw_plant.uf_display_all()
Else
	//dw_plant.Visible = False
	dw_plant.uf_display_label_only()
End If
end event

type cbx_day_of_week from checkbox within w_auto_update_parameters_inq
integer x = 96
integer y = 364
integer width = 82
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 16777215
end type

event clicked;If This.Checked Then
	dw_day_of_week.Visible = True
Else
	dw_day_of_week.Visible = False
End If
end event

