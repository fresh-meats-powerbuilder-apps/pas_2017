﻿$PBExportHeader$w_num_shells_to_generate.srw
$PBExportComments$Defauld Sched Parameters
forward
global type w_num_shells_to_generate from w_base_sheet_ext
end type
type cb_1 from u_help_cb within w_num_shells_to_generate
end type
type cb_cancel from u_base_commandbutton_ext within w_num_shells_to_generate
end type
type dw_num_shells_to_generate from u_base_dw_ext within w_num_shells_to_generate
end type
end forward

global type w_num_shells_to_generate from w_base_sheet_ext
integer width = 2409
integer height = 1680
string title = "Number of Shells to Generate"
long backcolor = 67108864
event ue_set_detail_handle ( window aw_order_detail )
cb_1 cb_1
cb_cancel cb_cancel
dw_num_shells_to_generate dw_num_shells_to_generate
end type
global w_num_shells_to_generate w_num_shells_to_generate

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
		ib_toolbarVisible, &
		ib_ReInquire, &
		ib_Dont_Increment_timeout, &
		ib_good_product

//Double		id_p24b_task_number, &
//		id_p24b_last_record, &
//		id_p24b_max_record
//
DWObject	idwo_last_clicked

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long		il_SelectedColor, &
		il_SelectedTextColor,&
		il_lastclicked_background, &
		il_ChangedRow

s_error		istr_error_info

// This is for the async call
u_pas201		iu_pas201
u_ws_pas3		iu_ws_pas3

////String		is_qty_by_age_code, &
////		is_shift_production, &
////		is_sold_qty_by_type, &
////		is_sold_qty_by_age, &
////		is_carry_over,&
//string	is_input 
//String	is_debug
String		is_colname, &
				is_product, &
				is_input, &
				is_output, &
				is_ChangedColumnName, &
				is_debug, &
				is_start_date, &
				is_end_Date
//w_base_sheet	iw_order_detail
end variables

forward prototypes
public subroutine wf_open_carry_over ()
public subroutine wf_set_dates ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public subroutine wf_open_carry_over ();
end subroutine

public subroutine wf_set_dates ();
end subroutine

public function boolean wf_retrieve ();
	
This.dw_num_shells_to_generate.ImportString(is_output)

return true

end function

public function boolean wf_update ();integer			li_ColNbr, &
    				li_Counter

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_output_string, &
					ls_build_string, &
					ls_temp, &
					ls_header_string, &
					ls_input_string

dwItemStatus	lis_status


SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_num_shells_to_generate.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	  dw_num_shells_to_generate.SetItem(ll_row, "update_flag", 'U')
		ll_Row = ll_NbrRows + 1
LOOP

ls_temp = dw_num_shells_to_generate.getitemstring(1, "update_flag")

ls_output_string = iw_frame.iu_string.nf_BuildUpdateString(dw_num_shells_to_generate)


ls_header_string = 'G'+'~r~n'
//			ls_output_string)  < 0 Then
//if iu_pas201.nf_pasp75cr_gen_tfr_orders(istr_error_info, &
//									ls_input_string, &
//									ls_header_string, &
//									ls_output_string)  <> 0 Then

if iu_ws_pas3.uf_pasp75gr(istr_error_info, &
									ls_input_string, &
									ls_header_string, &
									ls_output_string)  <> 0 Then
									This.SetRedraw(True)	
//									SetPointer(Arrow!)																										return false
									Return False
end if
		
dw_num_shells_to_generate.SetReDraw(False)
//iw_frame.SetMicroHelp("Shells Generated Successfully")
//

MessageBox(This.Title, "Shells Generated Successfully")

ll_RowCount = dw_num_shells_to_generate.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_num_shells_to_generate.SetItem(li_Counter, 'update_flag', ' ')
Next
dw_num_shells_to_generate.ResetUpdate()
dw_num_shells_to_generate.SetFocus()
dw_num_shells_to_generate.SelectRow(0, FALSE)
dw_num_shells_to_generate.SetReDraw(True)


Close(This)

Return( True )




end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)
If IsValid(iu_ws_pas3) Then Destroy(iu_ws_pas3)


end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_inquire')

iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;wf_retrieve()

end event

event activate;call super::activate;

// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')
iw_frame.im_menu.mf_disable('m_inquire')

If not ib_good_product Then
	iw_frame.im_menu.mf_disable('m_new')
	iw_frame.im_menu.mf_disable('m_addrow')
	iw_frame.im_menu.mf_disable('m_save')
	iw_frame.im_menu.mf_disable('m_deleterow')
End If


end event

event open;call super::open;



String	ls_data 
			
u_string_functions	lu_strings


ls_data = Message.StringParm


dw_num_shells_to_generate.ImportString(ls_data)





end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Num Shells To Generate"
istr_error_info.se_user_id = sqlca.userid



This.PostEvent("ue_query")

end event

event key;call super::key;//Int	li_curr_col
//
//If KeyDown(KeyTab!) Then
//	li_curr_col = dw_rmt_detail.GetColumn()
//	If li_curr_col < Integer(dw_rmt_detail.Describe("DataWindow.Column.Count")) Then
//		dw_rmt_detail.SetColumn(li_curr_col + 1)
//	Else
//		dw_rmt_detail.SetColumn(1)
//	End if
//	dw_rmt_detail.SetFocus()
//End if
end event

on w_num_shells_to_generate.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_cancel=create cb_cancel
this.dw_num_shells_to_generate=create dw_num_shells_to_generate
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.dw_num_shells_to_generate
end on

on w_num_shells_to_generate.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_cancel)
destroy(this.dw_num_shells_to_generate)
end on

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_num_shells_to_generate.x * 2) + 30 
li_y = dw_num_shells_to_generate.y + 115

if width > li_x Then
	dw_num_shells_to_generate.width	= width - li_x
end if

if height > li_y then
	dw_num_shells_to_generate.height	= height - li_y
end if
end event

type cb_1 from u_help_cb within w_num_shells_to_generate
integer x = 603
integer y = 1380
integer width = 389
integer height = 108
integer taborder = 30
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = "Generate Shells"
end type

event clicked;call super::clicked;wf_update ()
end event

type cb_cancel from u_base_commandbutton_ext within w_num_shells_to_generate
integer x = 1093
integer y = 1380
integer height = 108
integer taborder = 30
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = "&Cancel"
boolean cancel = true
boolean default = true
end type

on clicked;call u_base_commandbutton_ext::clicked;Close(Parent)
end on

type dw_num_shells_to_generate from u_base_dw_ext within w_num_shells_to_generate
integer x = 64
integer y = 28
integer width = 2235
integer height = 1300
integer taborder = 20
string dataobject = "d_num_of_shells_to_generate"
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return


end event

event constructor;call super::constructor;ib_updateable = True

end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus


//int			li_rc, &
//				li_seconds,li_temp
//
long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

//string		ls_GetText, &
string		ls_temp
				
Date			ldt_temp	

nvuo_pa_business_rules	u_rule


//is_ColName = GetColumnName()
//ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0

CHOOSE CASE dwo.name
	CASE "uom"
		If  isnull(data) Then
			iw_frame.SetMicroHelp("Invalid UOM")
			This.selecttext(1,100)
			return 1
		End if
	CASE "start_date", "end_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
	
	CASE "min_quantity_hour", "max_quantity_hour"
		If Not IsNumber(data) Then
			iw_frame.SetMicroHelp("Quantity must be a number")
			This.selecttext(1,100)
			return 1
		End if
		If Real(data) < 0 Then
			iw_frame.SetMicroHelp("Quantity cannot be negative")
			This.selecttext(1,100)
			return 1
		End if
END CHOOSE

// Update the Update_Flag column so the RPC will know how to update the row
//IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
//	IF Long(This.Describe("update_flag.id")) > 0 THEN
//		ls_temp_update_flag = this.describe("update_flag.id")
//		ls_temp_update_flag = This.GetItemString(ll_source_row, "update_flag")
//		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
//			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
//			CHOOSE CASE le_RowStatus
//			CASE NewModified!, New!
//				This.SetItem(ll_source_row, "update_flag", "A")
//			CASE DataModified!, NotModified!
//				This.SetItem(ll_source_row, "update_flag", "U")
//			END CHOOSE		
//		End if
//	END IF
//END IF
//
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

//parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0


end event

event itemerror;call super::itemerror;return 1

end event

