﻿$PBExportHeader$w_planned_transfer_range_new.srw
$PBExportComments$Planned Transfer Range. For Inquiring from TPASPLTR for a given range of values.
forward
global type w_planned_transfer_range_new from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_planned_transfer_range_new
end type
type dw_header from u_base_dw_ext within w_planned_transfer_range_new
end type
end forward

global type w_planned_transfer_range_new from w_base_sheet_ext
integer width = 3808
integer height = 1488
string title = "Planned Transfer Range"
long backcolor = 67108864
event ue_open_planned_transfer ( )
event ue_change_delivery_date ( )
dw_detail dw_detail
dw_header dw_header
end type
global w_planned_transfer_range_new w_planned_transfer_range_new

type variables
u_ws_pas_share	iu_ws_pas_share

s_error		istr_error_info

Long			il_selected_row

Date			idt_pa_range_date

w_planned_transfer_new	iw_planned_transfer_new


end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_rightclick (long al_row)
public subroutine wf_load_values ()
public function boolean wf_check_header_fields ()
public subroutine wf_get_pa_range_date ()
public subroutine wf_change_delivery_date ()
public subroutine wf_open_planned_transfer ()
end prototypes

event ue_open_planned_transfer();wf_open_planned_transfer()



end event

event ue_change_delivery_date();wf_change_delivery_date()
end event

public function boolean wf_retrieve ();String			ls_input_string, &
				ls_output_string, &
				ls_ship_plant, &
				ls_delivery_plant

u_string_functions		lu_string

dw_header.AcceptText()
dw_header.ResetUpdate()

Call w_base_sheet::closequery

If  not wf_check_header_fields() then
	Return False
End if

if dw_header.GetItemString(1, "ship_plant") = 'ALL' then
	ls_ship_plant = '   '
else
	ls_ship_plant = dw_header.GetItemString(1, "ship_plant")
end if

if dw_header.GetItemString(1, "delivery_plant") = 'ALL' then
	ls_delivery_plant = '   '
else
	ls_delivery_plant = dw_header.GetItemString(1, "delivery_plant")
end if

ls_input_string =	'R' + '~t' + & 	
						ls_ship_plant + '~t' + &
	  					String(dw_header.GetItemDate(1, "ship_begin_date"), "yyyy-mm-dd") + '~t' + &
						String(dw_header.GetItemDate(1, "ship_end_date"), "yyyy-mm-dd") + '~t' + &
						dw_header.GetItemString(1, "ship_period") + '~t' + &
						ls_delivery_plant + '~t' + &
						String(dw_header.GetItemDate(1, "delivery_begin_date"), "yyyy-mm-dd") + '~t' + &
						String(dw_header.GetItemDate(1, "delivery_end_date"), "yyyy-mm-dd") + '~t' + &
						dw_header.GetItemString(1, "delivery_period") + '~t'

SetRedraw (False)

If Not iu_ws_pas_share.nf_pasp05gr(istr_error_info, &
													ls_input_string, &
													ls_output_string) Then												
													
				
													
	SetRedraw(True)
	Return False
End If

dw_detail.Reset()
dw_detail.ImportString(ls_output_string)
dw_detail.ResetUpdate()
dw_detail.SetFocus()
dw_detail.SetRow(1)

SetRedraw (True)

Return True
end function

public subroutine wf_rightclick (long al_row);m_planned_transfer	NewMenu

if dw_detail.IsSelected(al_row) then
	Newmenu = Create m_planned_transfer
	NewMenu.PopMenu(IW_FRAME.PointerX(),iw_frame.PointerY())
end if

Destroy NewMenu


end subroutine

public subroutine wf_load_values ();
end subroutine

public function boolean wf_check_header_fields ();IF isnull(dw_header.GetItemString(1, "ship_plant")) then
	iw_frame.SetMicrohelp("Please enter a Ship Plant or ALL")
	Return False
End If

IF isnull(dw_header.GetItemString(1, "ship_period")) then
	iw_frame.SetMicroHelp("Please enter a Ship Period or ALL")
	Return False
End If

IF isnull(dw_header.GetItemString(1, "delivery_plant")) then
	iw_frame.SetMicroHelp("Please enter a Delivery Plant or ALL")
	Return False
End If

IF isnull(dw_header.GetItemString(1, "delivery_period")) then
	iw_frame.SetMicroHelp("Please enter a Delivery Period or ALL")
	Return False
End If

IF isnull(dw_header.GetItemDate(1, "ship_begin_date")) then
	iw_frame.SetMicroHelp("Please enter a Ship Begin Date")
	Return False
End If

IF isnull(dw_header.GetItemDate(1, "ship_end_date")) then
	iw_frame.SetMicroHelp("Please enter a Ship End Date")
	Return False
End If

IF isnull(dw_header.GetItemDate(1, "delivery_begin_date")) then
	iw_frame.SetMicroHelp("Please enter a Delivery Begin Date")
	Return False
End If

IF isnull(dw_header.GetItemDate(1, "delivery_end_date")) then
	iw_frame.SetMicroHelp("Please enter a Delivery End Date")
	Return False
End If

If (dw_header.GetItemString(1, "ship_plant") =  'ALL') and (dw_header.GetItemString(1, "delivery_plant") =  'ALL') Then
	iw_frame.SetMicroHelp("Ship Plant and Delivery Plant cannot both be ALL")
	Return False	
End If

If (dw_header.GetItemDate(1, "ship_begin_date")) > (dw_header.GetItemDate(1, "ship_end_date")) Then
	iw_frame.SetMicroHelp("Ship Begin Date cannot be greater than Ship End Date")
	Return False
End If

If (dw_header.GetItemDate(1, "delivery_begin_date")) > (dw_header.GetItemDate(1, "delivery_end_date")) Then
	iw_frame.SetMicroHelp("Delivery Begin Date cannot be greater than Delivery End Date")
	Return False
End If

if (dw_header.GetItemDate(1, "ship_end_date")) > idt_pa_range_date Then
	iw_frame.SetMicroHelp("Ship End Date must be within the PA period, max. date is " + String(idt_pa_range_date, 'mm/dd/yyyy')) 
	Return False
End If


if (dw_header.GetItemDate(1, "delivery_end_date")) > idt_pa_range_date Then
	if DayNumber(dw_header.GetItemDate(1, "delivery_end_date")) <> 1 Then
		iw_frame.SetMicroHelp("The Date of " + string(dw_header.GetItemDate(1, "delivery_end_date"), "dd/dd/yyyy") + &
			" is beyond the PA daily range --  the date has been changed to the next Sunday date") 
		wf_change_delivery_date()	
	End If
End If

Return True

end function

public subroutine wf_get_pa_range_date ();Integer	li_pa_range
String		ls_PARange


If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "PA RANGE", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
End if
li_pa_range = Integer(ls_PARange) - 1

idt_pa_range_date = RelativeDate(Today(), li_pa_range)
end subroutine

public subroutine wf_change_delivery_date ();Date		ldt_new_date, ldt_temp
Integer	li_day_number

ldt_temp = dw_header.GetItemDate(1, "delivery_end_date")

li_day_number = DayNumber(ldt_temp)

Choose case li_day_number
	Case	2
		ldt_new_date = RelativeDate(ldt_temp, 6)
		dw_header.SetItem(1, "delivery_end_date", ldt_new_date)
	Case	3
		ldt_new_date = RelativeDate(ldt_temp, 5)
		dw_header.SetItem(1, "delivery_end_date", ldt_new_date)
	Case	4
		ldt_new_date = RelativeDate(ldt_temp, 4)
		dw_header.SetItem(1, "delivery_end_date", ldt_new_date)
	Case	5
		ldt_new_date = RelativeDate(ldt_temp, 3)
		dw_header.SetItem(1, "delivery_end_date", ldt_new_date)
	Case	6
		ldt_new_date = RelativeDate(ldt_temp, 2)
		dw_header.SetItem(1, "delivery_end_date", ldt_new_date)
	Case	7
		ldt_new_date = RelativeDate(ldt_temp, 1)
		dw_header.SetItem(1, "delivery_end_date", ldt_new_date)
	Case	2
		ldt_new_date = ldt_temp
		dw_header.SetItem(1, "delivery_end_date", ldt_new_date)
End Choose


end subroutine

public subroutine wf_open_planned_transfer ();String		ls_ship_plant, ls_ship_date, ls_ship_period, &
			ls_delivery_plant, ls_delivery_date, ls_delivery_period, ls_params, ls_product


ls_ship_plant = dw_detail.GetItemString(il_selected_row, 'ship_plant')
ls_ship_date = String(dw_detail.GetItemDate(il_selected_row,'ship_date'), "mm/dd/yyyy")
ls_ship_period = dw_detail.GetItemString(il_selected_row, 'ship_period')

ls_delivery_plant = dw_detail.GetItemString(il_selected_row, 'delivery_plant')	
ls_delivery_date = String(dw_detail.GetItemDate(il_selected_row,'delivery_date'), "mm/dd/yyyy")	
ls_delivery_period = dw_detail.GetItemString(il_selected_row, 'delivery_period')	
ls_product = ''

ls_params = ls_ship_plant + '~t' + ls_ship_date + '~t' + ls_ship_period + '~t' + &
				ls_delivery_plant + '~t' + ls_delivery_date + '~t' + ls_delivery_period + '~t' + ls_product + '~t'
				
If isValid(iw_planned_transfer_new) then
	iw_planned_transfer_new.SetFocus()
	iw_planned_transfer_new.wf_pass_params(ls_params)
Else	
	OpenSheetWithParm ( iw_planned_transfer_new, ls_params, "w_planned_transfer_new", iw_frame	, 0, iw_frame.im_menu.iao_arrangeopen )
End If
end subroutine

on w_planned_transfer_range_new.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_planned_transfer_range_new.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event ue_postopen;call super::ue_postopen;iu_ws_pas_share = Create u_ws_pas_share

wf_get_pa_range_date()

wf_retrieve()

end event

event close;call super::close;IF IsValid( iu_ws_pas_share ) THEN
	DESTROY iu_ws_pas_share
END IF


end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_save')

//iw_frame.im_menu.mf_Disable('m_print')
//iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

//iw_frame.im_menu.mf_Enable('m_print')
//iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

end event

event resize;call super::resize;long	ll_x = 50, ll_y = 105

if il_BorderPaddingWidth > ll_x Then
	ll_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ll_y Then
	ll_y = il_BorderPaddingHeight
End If

dw_detail.Resize(&
						this.width - dw_detail.x - ll_x, &
						this.height - dw_detail.y - ll_y)
end event

event ue_fileprint;call super::ue_fileprint;DataStore					lds_print

DataWindowChild			ldwc_type

lds_print = create u_print_datastore
lds_print.DataObject = 'd_planned_transfer_range_detail_print'

lds_print.GetChild("pt_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PTFRTYPE")

dw_detail.sharedata(lds_print)

if dw_header.GetItemString(1, "ship_plant") = '   ' Then
	lds_print.Object.t_ship_plant.Text = 'ALL'
else
	lds_print.Object.t_ship_plant.Text = dw_header.GetItemString(1, "ship_plant")
end if

lds_print.Object.t_ship_plant_name.Text = dw_header.GetItemString(1, "ship_plant_desc")
lds_print.Object.t_ship_begin_date.Text = String(dw_header.GetItemDate(1, "ship_begin_date"), 'mm/dd/yyyy')
lds_print.Object.t_ship_end_date.Text = String(dw_header.GetItemDate(1, "ship_end_date"), 'mm/dd/yyyy')

If dw_header.GetItemString(1, "ship_period") = ' ' then
	lds_print.Object.t_ship_period.Text = 'ALL'
else
	lds_print.Object.t_ship_period.Text = dw_header.GetItemString(1, "ship_period")
end if

if dw_header.GetItemString(1, "delivery_plant") = '   ' Then
	lds_print.Object.t_delivery_plant.Text = 'ALL'
else
	lds_print.Object.t_delivery_plant.Text = dw_header.GetItemString(1, "delivery_plant")
end if 

lds_print.Object.t_delivery_plant_name.Text = dw_header.GetItemString(1, "delivery_plant_desc")
lds_print.Object.t_delivery_begin_date.Text = String(dw_header.GetItemDate(1, "delivery_begin_date"), 'mm/dd/yyyy')
lds_print.Object.t_delivery_end_date.Text = String(dw_header.GetItemDate(1, "delivery_end_date"), 'mm/dd/yyyy')

If dw_header.GetItemString(1, "delivery_period") = ' ' then
	lds_print.Object.t_delivery_period.Text = 'ALL'
else
	lds_print.Object.t_delivery_period.Text = dw_header.GetItemString(1, "delivery_period")
end if

lds_print.print()
end event

type dw_detail from u_base_dw_ext within w_planned_transfer_range_new
event ue_reset_selected_row ( )
integer x = 23
integer y = 412
integer width = 3721
integer height = 948
integer taborder = 20
string dataobject = "d_planned_transfer_range_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event ue_reset_selected_row();dw_detail.SetRow(il_selected_row)
end event

event constructor;call super::constructor;DatawindowChild		ldwc_type

This.GetChild("pt_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PTFRTYPE")

This.InsertRow(0)

is_selection= "1"

end event

event rbuttondown;call super::rbuttondown;il_selected_row = row

wf_rightclick(row)


end event

event ue_keydown;call super::ue_keydown;if key = KeyEnter! Then
	
	il_selected_row = dw_detail.GetSelectedRow(0)
	If il_selected_row > 0 Then
			wf_open_planned_transfer()
	End If
End if

end event

type dw_header from u_base_dw_ext within w_planned_transfer_range_new
integer x = 18
integer y = 12
integer width = 3127
integer height = 396
integer taborder = 10
string dataobject = "d_planned_transfer_range_header"
boolean minbox = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;String		ls_temp

datawindowchild ldwch_ship_plant, ldwch_delv_plant

Long		ll_found

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild('ship_plant', ldwch_ship_plant)
CONNECT USING SQLCA;
ldwch_ship_plant.SetTransObject(SQLCA)
ldwch_ship_plant.Retrieve()
ldwch_ship_plant.InsertRow(1)
ldwch_ship_plant.SetItem(1, 'location_code', 'ALL')
ldwch_ship_plant.SetItem(1, 'location_name', 'ALL')

This.GetChild('delivery_plant', ldwch_delv_plant)
CONNECT USING SQLCA;
ldwch_delv_plant.SetTransObject(SQLCA)
ldwch_delv_plant.Retrieve()
ldwch_delv_plant.InsertRow(1)
ldwch_delv_plant.SetItem(1, 'location_code', 'ALL')
ldwch_delv_plant.SetItem(1, 'location_name', 'ALL')

ls_temp = ProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipplant", "")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	ls_temp = '   '
End If
This.SetItem(1,"ship_plant", ls_temp)
ll_found = ldwch_ship_plant.Find("location_code = '" + ls_temp + "'", 1, ldwch_ship_plant.RowCount())			
if ll_found < 1 then
	iw_frame.SetMicrohelp('Ship plant is invalid')
	This.SetColumn ('ship_plant')
else
	dw_header.SetItem(1, "ship_plant_desc", ldwch_ship_plant.GetItemString(ll_found, "location_name")) 		
End If	

ls_temp = ProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipbegindate", "")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	This.SetItem(1,"ship_begin_date", Today())
Else
	if Date(ls_temp) < Today() Then
		This.SetItem(1,"ship_begin_date", Today())
	else	
		This.SetItem(1,"ship_begin_date", Date(ls_temp))
	end if
End If

ls_temp = ProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipenddate", "")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	This.SetItem(1,"ship_end_date", Today())
Else
	if Date(ls_temp) < Today() Then
		This.SetItem(1,"ship_end_date", Today())
	else	
		This.SetItem(1,"ship_end_date", Date(ls_temp))
	End If
End If

ls_temp = ProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipperiod", "")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	This.SetItem(1,"ship_period", ' ')
Else
	This.SetItem(1,"ship_period", ls_temp)
End If

ls_temp = ProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvplant", "")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	ls_temp = '   '
End If
This.SetItem(1,"delivery_plant", ls_temp)
ll_found = ldwch_delv_plant.Find("location_code = '" + ls_temp + "'", 1, ldwch_delv_plant.RowCount())			
if ll_found < 1 then
	iw_frame.SetMicrohelp('Delivery plant is invalid')
	This.SetColumn ('delivery_plant')
else
	dw_header.SetItem(1, "delivery_plant_desc", ldwch_delv_plant.GetItemString(ll_found, "location_name")) 		
End If		

ls_temp = ProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvbegindate", "")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	This.SetItem(1,"delivery_begin_date", Today())
Else
	if Date(ls_temp) < Today() Then
		This.SetItem(1,"delivery_begin_date", Today())
	else
		This.SetItem(1,"delivery_begin_date", Date(ls_temp))
	end if
End If

ls_temp = ProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvenddate", "")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	This.SetItem(1,"delivery_end_date", Today())
Else
	if Date(ls_temp) < Today() Then
		This.SetItem(1,"delivery_end_date", Today())
	else
		This.SetItem(1,"delivery_end_date", Date(ls_temp))
	end if
End If

ls_temp = ProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvperiod", "")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	This.SetItem(1,"delivery_period", ' ')
Else
	This.SetItem(1,"delivery_period", ls_temp)
End If
end event

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_temp
Long					ll_found


CHOOSE CASE This.GetColumnName()
		
	Case "ship_plant"
			dw_header.GetChild("ship_plant", ldwc_temp)
			ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())
			if ll_found < 1 then
				iw_frame.SetMicrohelp('Ship plant is invalid')
				This.SetColumn ('ship_plant')
				Return 1
			else
				dw_header.SetItem(1, "ship_plant_desc", ldwc_temp.GetItemString(ll_found, "location_name")) 
				SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipplant",Trim(data))
			end if
			
	Case	"delivery_plant"
			dw_header.GetChild("delivery_plant", ldwc_temp)
			ll_found = ldwc_temp.Find("location_code = '" + data + "'", 1, ldwc_temp.RowCount())			
			if ll_found < 1 then
				iw_frame.SetMicrohelp('Delivery plant is invalid')
				This.SetColumn ('delivery_plant')
				Return 1
			else
				dw_header.SetItem(1, "delivery_plant_desc", ldwc_temp.GetItemString(ll_found, "location_name")) 		
				SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvplant",Trim(data))
			end if	
			
	Case "ship_begin_date"		
		if date(data) > dw_header.GetItemDate(1, "ship_end_date") then
			iw_frame.SetMicroHelp("Ship Begin Date is greater than Ship End Date")
		end if
		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipbegindate",String(date(data),"yyyy-mm-dd"))
		
	Case "ship_end_date"		
		if dw_header.GetItemDate(1, "ship_begin_date") > date(data) then
			iw_frame.SetMicroHelp("Ship Begin Date is greater than Ship End Date")
		else
			if date(data) > idt_pa_range_date Then
				iw_frame.SetMicroHelp("Ship End Date must be within the PA period, max. date is " + String(idt_pa_range_date, 'mm/dd/yyyy')) 
			End If		
		End If
		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipenddate",String(date(data),"yyyy-mm-dd"))		
		
	Case "delivery_begin_date"		
		if date(data) > dw_header.GetItemDate(1, "delivery_end_date") then
			iw_frame.SetMicroHelp("Delivery Begin Date is greater than Delivery End Date")
		end if
		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvbegindate",String(date(data),"yyyy-mm-dd"))	
		
	Case "delivery_end_date"	
		if dw_header.GetItemDate(1, "delivery_begin_date") > date(data) then
			iw_frame.SetMicroHelp("Delivery Begin Date is greater than Delivery End Date")
		else
			if date(data) > idt_pa_range_date then
				iw_frame.SetMicroHelp("The date of " + string(date(data),'mm/dd/yyyy') + " is beyonod the daily PA Range -- the " + &
					" date has been changed to the next Sunday date")
				Parent.postEvent("ue_change_delivery_date")	
			end if
		end if
			
		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvenddate",String(date(data),"yyyy-mm-dd"))		
		
	Case "ship_period"		
		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFShipperiod",Trim(data))		
		
	Case "delivery_period"		
		SetProfileString( iw_frame.is_UserINI, "PAS", "PLTFDelvperiod",Trim(data))		
			
End Choose
	
end event

