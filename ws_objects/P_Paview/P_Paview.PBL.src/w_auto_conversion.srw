﻿$PBExportHeader$w_auto_conversion.srw
forward
global type w_auto_conversion from w_base_sheet_ext
end type
type dw_fab_product_code from u_fab_product_code within w_auto_conversion
end type
type dw_auto_conversion from u_base_dw_ext within w_auto_conversion
end type
type dw_plant from u_plant within w_auto_conversion
end type
end forward

global type w_auto_conversion from w_base_sheet_ext
integer width = 1787
integer height = 844
string title = "Auto Conversion"
long backcolor = 80269524
dw_fab_product_code dw_fab_product_code
dw_auto_conversion dw_auto_conversion
dw_plant dw_plant
end type
global w_auto_conversion w_auto_conversion

type prototypes
function integer WBGetCommHandle( ref integer CommHnd ) library "wrkbench.dll"
function integer WBReleaseCommHandle( int CommHnd ) library "wrkbench.dll"
function integer WBSetServerAlias( string serveralias, integer CommHnd) library "wrkbench.dll" alias for "WBSetServerAlias;Ansi"
function integer WBCkCompleted( integer CommHnd ) library "pas203.dll"
function integer WBSetSecurity( string user_id, string password, string encryptionkey, integer CommHnd ) library "wrkbench.dll" alias for "WBSetSecurity;Ansi"

end prototypes

type variables
Boolean                ib_SaveInProgress,&
		ib_plant_is_live,&
		ib_focus

Integer                  ii_CommHnd, &
		ii_PARange

u_pas203              iu_pas203
u_ws_pas1			iu_ws_pas1

s_error                  istr_error_info

                           


end variables

forward prototypes
public function integer wf_modified ()
public function boolean wf_update ()
end prototypes

public function integer wf_modified ();return 0
end function

public function boolean wf_update ();Boolean			lb_Return

integer			li_rc, &
					li_IncreaseAmount

character		lc_Plant[3], &
					lc_Product[10], &
					lc_AgeCode

string			ls_Plant, &
					ls_Product, &
					ls_state, &
					ls_product_status, &
					ls_UpdateSource, &
					ls_ReturnMessage, &
					ls_ConversionDate

date				ld_Date,ldt_to_date,ldt_from_date,ldt_conv_date


IF ib_SaveInProgress THEN
	MessageBox( "Working", "Auto Conversion in Progress.  Please Wait...", Information! )
	Return( False )
END IF

IF dw_auto_conversion.AcceptText() = -1 &
	or dw_plant.accepttext() = -1 &
		or not dw_fab_product_code.uf_validate( ) then
 	Return( False )
End if

ls_Plant = dw_plant.nf_Get_Plant_Code()
If iw_frame.iu_string.nf_IsEmpty(ls_Plant) Then
	iw_frame.SetMicroHelp("Plant Code is a required Field")
	dw_plant.SetFocus()
	return False
End if

ls_Product =  dw_fab_product_code.uf_Get_Product_Code()
ls_state =  dw_fab_product_code.uf_get_product_state( )
//1-13 jac
ls_product_status =  dw_fab_product_code.uf_get_product_status( )
//

IF Not ib_plant_is_live Then
	lc_AgeCode = dw_auto_conversion.GetItemString( 1, "age_code" )
	If iw_frame.iu_string.nf_IsEmpty(lc_AgeCode) Then
		iw_frame.SetMicroHelp("Age Code is a required field")
		dw_auto_conversion.SetColumn("Age_Code")
		dw_auto_conversion.SetFocus()
		return False
	End if
Else
	ldt_from_date = dw_auto_conversion.getitemDate(1,"from_date")
	ldt_to_date   = dw_auto_conversion.getitemDate(1,"to_date")
	ldt_conv_date = dw_auto_conversion.GetItemDate(1, "conversion_date")
	
	If ldt_from_date > ldt_conv_date Then
		SetMicroHelp("From Date must be less than or equal to the Conversion Date")
		dw_auto_conversion.SetColumn("from_date")
		dw_auto_conversion.SetFocus()
		return False
	End if
	
	If ldt_from_date <= Today() Then
		SetMicroHelp("From Date must be greater than today")
		dw_auto_conversion.SetColumn("from_date")
		dw_auto_conversion.SetFocus()
		return False
	End if
	
	If ldt_from_date > ldt_to_date Then
		SetMicroHelp("From Date must be less than the To Date")
		dw_auto_conversion.SetColumn("from_date")
		dw_auto_conversion.SetFocus()
		return False
	End if
	
	If ldt_to_date > ldt_conv_date Then
		SetMicroHelp("To Date must be less than or equal to the Conversion Date")
		dw_auto_conversion.SetColumn("to_date")
		dw_auto_conversion.SetFocus()
		return False
	End if
	
	If ldt_to_date <= Today() Then
		SetMicroHelp("To Date must be greater than today")
		dw_auto_conversion.SetColumn("to_date")
		dw_auto_conversion.SetFocus()
		return False
	End if

	If ldt_to_date < ldt_from_date Then
		SetMicroHelp("To Date must be greater than or equal to the From Date")
		dw_auto_conversion.SetColumn("to_date")
		dw_auto_conversion.SetFocus()
		return False
	End if
	
	If ldt_conv_date <= Today() Then
		SetMicroHelp("Conversion Date must be greater than today")
		dw_auto_conversion.SetColumn("conversion_date")
		dw_auto_conversion.SetFocus()
		return False
	End if

	If ldt_conv_date > RelativeDate(Today(), ii_PaRange) Then
		SetMicroHelp("Conversion Date must be within " + String(ii_PARange) + " days of today")
		dw_auto_conversion.SetColumn("conversion_date")
		dw_auto_conversion.SetFocus()
		return False
	End if
End If

ld_Date = dw_auto_conversion.GetItemDate( 1, "conversion_date" )
If ld_Date < Today() Then
	iw_frame.SetMicroHelp("Date must be greater than or equal to today")
	dw_auto_conversion.SetColumn("conversion_date")
	dw_auto_conversion.SetFocus()
	return False
End if

li_IncreaseAmount = dw_auto_conversion.GetItemNumber( 1, "increase_amount" )
ls_ConversionDate = String(ld_Date, "yyyy-mm-dd")

iw_frame.SetMicroHelp("Wait... Updating Database")

istr_error_info.se_function_name		= "wf_update"
istr_error_info.se_event_name			= ""
istr_error_info.se_procedure_name	= "pasp23br"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF
//1-13 jac  added product_status
IF Not ib_plant_is_live Then
	ls_UpdateSource	=	' ' + "~t" + &
								ls_Product + "~t" + &
								ls_State + "~t" + &
								ls_product_status + "~t" + &
								ls_Plant + "~t" + &
								ls_ConversionDate + "~t" + &
								lc_AgeCode + "~t" + &
								'          ' + "~t" + &
								'          ' + "~t" + &
								string(li_IncreaseAmount) + "~t"
Else
	ls_UpdateSource	=	'L' + "~t" + &
								ls_Product + "~t" + &
								ls_State + "~t" + &
								ls_product_status + "~t" + &
								ls_Plant + "~t" + &
								ls_ConversionDate + 	"~t" + &
								' ' + "~t" + &
								String(ldt_from_date, "yyyy-mm-dd") + "~t" + &
								String(ldt_to_date, "yyyy-mm-dd") + "~t" + &
								string(li_IncreaseAmount) + "~t"
	
End IF

//lb_Return = iu_pas203.nf_initiate_auto_conv( istr_error_info, &
//															ls_UpdateSource, &
//															ls_ReturnMessage, &
//															ii_commhnd , &
//															True)

lb_Return = iu_ws_pas1.nf_pasp23fr( istr_error_info, &
															ls_UpdateSource, &
															ls_ReturnMessage)
															
SetMicroHelp(iw_Frame.ia_Application.MicroHelpDefault)
	If lb_return Then
		OpenWithParm( w_auto_conversion_message, Trim( ls_ReturnMessage ))
	End if

//IF lb_Return THEN 
//	Timer(1,This)
//	This.Pointer = 'async.cur'
//	dw_plant.Modify("DataWindow.Pointer = 'async.cur'")
//	dw_fab_product_code.Modify("DataWindow.Pointer = 'async.cur'")
//	dw_auto_conversion.Modify("DataWindow.Pointer = 'async.cur'")
//	ib_saveinprogress = True
//END IF

Return( lb_Return )
end function

event ue_postopen;call super::ue_postopen;Int		li_rc

String	ls_PARange,ls_plant
nvuo_pa_business_rules 	nvuo_pa		 

ls_plant = dw_plant.nf_Get_Plant_Code()
If nvuo_pa.uf_check_pasldtyp(ls_plant) Then
	ib_plant_is_live = True
	dw_auto_conversion.Object.age_code.Visible= '0'
	dw_auto_conversion.Object.age_code_t.Visible = '0' 
	dw_auto_conversion.Object.from_date_t.Visible = '1' 
	dw_auto_conversion.Object.from_date.Visible = '1' 
	dw_auto_conversion.Object.to_date_t.Visible = '1' 
	dw_auto_conversion.Object.to_date.Visible = '1' 
Else
	dw_auto_conversion.Object.age_code.Visible= '1'
	dw_auto_conversion.Object.age_code_t.Visible = '1' 
	dw_auto_conversion.Object.from_date_t.Visible = '0' 
	dw_auto_conversion.Object.from_date.Visible = '0' 
	dw_auto_conversion.Object.to_date_t.Visible = '0' 
	dw_auto_conversion.Object.to_date.Visible = '0' 
End IF	


istr_error_info.se_user_id = sqlca.userid
istr_error_info.se_Window_Name = 'AutoConv'

iu_pas203 = Create u_pas203
iu_ws_pas1 = Create u_ws_pas1
If Message.ReturnValue = -1 then Close(This)

ii_CommHnd = iu_pas203.nf_Get_Async_CommHandle()

// get the number of valid days for PA
If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "FUTURE", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "Future PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
End if
ii_PARange = Integer(ls_PARange)



end event

event timer;call super::timer;boolean		lb_return

integer		li_rc

string		ls_update_source, &
				ls_ConversionMessage


IF iu_pas203.nf_Async_Complete( ii_CommHnd ) THEN
	ls_ConversionMessage = Space(500)
//	lb_return = iu_pas203.nf_initiate_auto_conv( istr_error_info, ls_update_source, &
//																	ls_ConversionMessage, ii_commhnd, False)
	lb_return = iu_ws_pas1.nf_pasp23fr( istr_error_info, ls_update_source, &
																	ls_ConversionMessage)
	Timer(0,This)
	This.Pointer = ''
	dw_plant.Modify("DataWindow.Pointer = ''")
	dw_fab_product_code.Modify("DataWindow.Pointer = ''")
	dw_auto_conversion.Modify("DataWindow.Pointer = ''")
	ib_saveinprogress = False
	SetMicroHelp(iw_Frame.ia_Application.MicroHelpDefault)
	If lb_return Then
		OpenWithParm( w_auto_conversion_message, Trim( ls_ConversionMessage ))
	End if
END IF


end event

event close;call super::close;IF ii_commhnd > 0 THEN	
	iu_pas203.nf_Release_CommHandle( ii_CommHnd ) 
End if
	
IF IsValid( iu_pas203 ) THEN	DESTROY iu_pas203

Destroy iu_ws_pas1

end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')

iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleteRow')

iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')

iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleteRow')

iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')
end event

on closequery;call w_base_sheet_ext::closequery;IF ib_saveinprogress THEN
	If KeyDown(KeyControl!) And KeyDown(KeyShift!) Then 
		Message.ReturnValue = 0
		return
	End if
	MessageBox( "Working", "Window cannot be Closed. "+ "Auto Conversion in Progress.  Please Wait.", Information! )
	Message.ReturnValue = 1
	Return
END IF

end on

on w_auto_conversion.create
int iCurrent
call super::create
this.dw_fab_product_code=create dw_fab_product_code
this.dw_auto_conversion=create dw_auto_conversion
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fab_product_code
this.Control[iCurrent+2]=this.dw_auto_conversion
this.Control[iCurrent+3]=this.dw_plant
end on

on w_auto_conversion.destroy
call super::destroy
destroy(this.dw_fab_product_code)
destroy(this.dw_auto_conversion)
destroy(this.dw_plant)
end on

type dw_fab_product_code from u_fab_product_code within w_auto_conversion
integer x = 37
integer y = 96
integer width = 1513
integer taborder = 20
end type

type dw_auto_conversion from u_base_dw_ext within w_auto_conversion
integer y = 404
integer width = 1650
integer height = 192
integer taborder = 30
string dataobject = "d_auto_conversion"
boolean border = false
end type

event itemchanged;call super::itemchanged;String	ls_GetText,&
			ls_user_id
			

ls_GetText = This.GetText()

Choose Case This.GetColumnName()
Case "from_date"
//	If Date(data) > This.GetItemDate(1, "conversion_date") Then
//		SetMicroHelp("From Date must be less than the Conversion Date")
//		return 1
//	End if
	If Date(data) <= Today() Then
		SetMicroHelp("From Date must be greater than today")
		return 1
	End if
//	If Date(data) > This.GetItemDate(1, "to_date") Then
//		SetMicroHelp("From Date must be less than the To Date")
//		return 1
//	End if
Case "to_date"
//	If Date(data) > This.GetItemDate(1, "conversion_date") Then
//		SetMicroHelp("To Date must be less than the Conversion Date")
//		return 1
//	End if
	If Date(data) <= Today() Then
		SetMicroHelp("To Date must be greater than today")
		return 1
	End if
//	If Date(data) < This.GetItemDate(1, "from_date") Then
//		SetMicroHelp("To Date must be greater than the From Date")
//		return 1
//	End if
Case "conversion_date"
//	If Date(ls_getText) <= Today() Then
//		SetMicroHelp("Conversion Date must be greater than today")
//		return 1
//	End if

	If Date(ls_GetText) > RelativeDate(Today(), ii_PaRange) Then
		SetMicroHelp("Conversion Date must be within " + String(ii_PARange) + " days of today")
		return 1
	End if
Case 'increase_amount'
	If Integer(ls_GetText) = 0 Then
		iw_frame.SetMicroHelp("Amount can't be zero")
	return 1
	End If
End Choose

return 0
end event

event itemerror;call super::itemerror;Choose Case This.GetColumnName()
Case "conversion_date", 'increase_amount', 'to_date', 'from_date' 
	This.SelectText(1, Len(This.GetText()))
	return 1
End Choose

return 0
end event

event constructor;call super::constructor;TriggerEvent( "ue_insertrow" )
ib_Updateable = False

SetItem( GetRow(), "conversion_date", RelativeDate ( Today(), 1 ) )
SetItem( GetRow(), "from_date", RelativeDate ( Today(), 1 ) )
SetItem( GetRow(), "to_date", RelativeDate ( Today(), 1 ) )
SetItem( GetRow(), "increase_amount", 1 )



end event

event getfocus;//If Ib_focus then
//	IF ib_plant_is_live Then
//		//This.SetColumn('from_date')
//	Else
//		This.SetColumn('age_code')
//	End If
//End If
//
//ib_focus = False	

end event

type dw_plant from u_plant within w_auto_conversion
integer x = 91
integer y = 16
integer width = 1445
integer taborder = 10
end type

event itemchanged;call super::itemchanged;nvuo_pa_business_rules 	nvuo_pa		 

//dw_auto_conversion.SetRedraw ( False )

If nvuo_pa.uf_check_pasldtyp(data) Then
	ib_plant_is_live = True
	dw_auto_conversion.Object.age_code.Visible= '0'
	dw_auto_conversion.Object.age_code_t.Visible = '0' 
	dw_auto_conversion.Object.from_date_t.Visible = '1' 
	dw_auto_conversion.Object.from_date.Visible = '1' 
	dw_auto_conversion.Object.to_date_t.Visible = '1' 
	dw_auto_conversion.Object.to_date.Visible = '1' 
	
	//** IBDKEEM ** 10/30/2002 ** Fix Tab order problem.
	dw_auto_conversion.Object.from_date.tabsequence = '10' 
	dw_auto_conversion.Object.age_code.tabsequence= '20'

Else
	ib_plant_is_live = False
	dw_auto_conversion.Object.age_code.Visible= '1'
	dw_auto_conversion.Object.age_code_t.Visible = '1' 
	dw_auto_conversion.Object.from_date_t.Visible = '0' 
	dw_auto_conversion.Object.from_date.Visible = '0' 
	dw_auto_conversion.Object.to_date_t.Visible = '0' 
	dw_auto_conversion.Object.to_date.Visible = '0' 

	//** IBDKEEM ** 10/30/2002 ** Fix Tab order problem.
	dw_auto_conversion.Object.age_code.tabsequence= '10'
	dw_auto_conversion.Object.from_date.tabsequence = '20' 

End IF	

//dw_auto_conversion.SetRedraw ( true )

end event

