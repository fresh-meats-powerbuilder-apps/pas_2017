﻿$PBExportHeader$w_production_pt_parameters_inq.srw
forward
global type w_production_pt_parameters_inq from w_base_response_ext
end type
type dw_division from u_division within w_production_pt_parameters_inq
end type
end forward

global type w_production_pt_parameters_inq from w_base_response_ext
integer x = 46
integer y = 304
integer width = 1659
integer height = 492
string title = ""
boolean controlmenu = false
long backcolor = 12632256
dw_division dw_division
end type
global w_production_pt_parameters_inq w_production_pt_parameters_inq

event ue_base_ok;call super::ue_base_ok;String		ls_division
				

If dw_division.AcceptText() = -1 Then return

ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End If

iw_parentwindow.Event ue_set_data('division',ls_division)

ib_ok_to_close = True

Close(This)

end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('division')
dw_division.uf_set_division(Message.StringParm)
iw_parentwindow.Event ue_set_data('close', "true")



end event

on w_production_pt_parameters_inq.create
int iCurrent
call super::create
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
end on

on w_production_pt_parameters_inq.destroy
call super::destroy
destroy(this.dw_division)
end on

event ue_base_cancel;call super::ue_base_cancel;Close (This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_production_pt_parameters_inq
integer x = 923
integer y = 252
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_production_pt_parameters_inq
integer x = 645
integer y = 252
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_production_pt_parameters_inq
integer x = 366
integer y = 252
integer taborder = 20
end type

type dw_division from u_division within w_production_pt_parameters_inq
integer x = 32
integer y = 100
integer taborder = 10
end type

