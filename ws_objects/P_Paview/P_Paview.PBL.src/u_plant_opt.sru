﻿$PBExportHeader$u_plant_opt.sru
forward
global type u_plant_opt from u_base_dw_ext
end type
end forward

global type u_plant_opt from u_base_dw_ext
int Width=453
int Height=240
string DataObject="d_plant_option"
end type
global u_plant_opt u_plant_opt

forward prototypes
public subroutine uf_enable (boolean ab_enable)
public function string uf_get_plant_option ()
public subroutine uf_set_plant_option (string as_plant_option)
end prototypes

public subroutine uf_enable (boolean ab_enable);
CHOOSE CASE ab_enable
	CASE True
		This.Modify("plant_option.Protect = 0 " + &
				"plant_option.Pointer = 'Arrow!'")
		ib_updateable = True		
	Case False
		This.Modify("plant_option.Protect = 1 " + &
				"plant_option.Pointer = 'Beam!'")
		ib_updateable = False
END CHOOSE

end subroutine

public function string uf_get_plant_option ();Return This.GetItemString(1, "plant_option")
end function

public subroutine uf_set_plant_option (string as_plant_option);	This.SetItem(1,"plant_option",as_plant_option)

end subroutine

event constructor;call super::constructor;This.InsertRow(0)
end event

