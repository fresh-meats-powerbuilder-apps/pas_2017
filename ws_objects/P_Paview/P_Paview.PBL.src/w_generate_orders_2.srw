﻿$PBExportHeader$w_generate_orders_2.srw
forward
global type w_generate_orders_2 from w_base_sheet_ext
end type
type uo_groups from u_group_list within w_generate_orders_2
end type
type em_num_days from editmask within w_generate_orders_2
end type
type st_1 from statictext within w_generate_orders_2
end type
type cbx_seperate_ind from checkbox within w_generate_orders_2
end type
type dw_options from u_base_dw_ext within w_generate_orders_2
end type
type dw_planned_tran_type from u_base_dw_ext within w_generate_orders_2
end type
type st_ship_date from statictext within w_generate_orders_2
end type
type dw_date from u_base_dw_ext within w_generate_orders_2
end type
type dw_complex from u_base_dw_ext within w_generate_orders_2
end type
type dw_complex_plant from u_base_dw_ext within w_generate_orders_2
end type
type gb_complex_plant from groupbox within w_generate_orders_2
end type
type dw_dest_plant from u_plant within w_generate_orders_2
end type
type gb_options from groupbox within w_generate_orders_2
end type
type dw_phase_options from u_base_dw_ext within w_generate_orders_2
end type
type gb_phase_options from groupbox within w_generate_orders_2
end type
type dw_load_option from u_base_dw_ext within w_generate_orders_2
end type
type gb_indicators from groupbox within w_generate_orders_2
end type
type gb_1 from groupbox within w_generate_orders_2
end type
type gb_source from groupbox within w_generate_orders_2
end type
type dw_source from u_base_dw_ext within w_generate_orders_2
end type
end forward

global type w_generate_orders_2 from w_base_sheet_ext
integer width = 3223
integer height = 2456
string title = "Generate Transfer Orders"
long backcolor = 67108864
uo_groups uo_groups
em_num_days em_num_days
st_1 st_1
cbx_seperate_ind cbx_seperate_ind
dw_options dw_options
dw_planned_tran_type dw_planned_tran_type
st_ship_date st_ship_date
dw_date dw_date
dw_complex dw_complex
dw_complex_plant dw_complex_plant
gb_complex_plant gb_complex_plant
dw_dest_plant dw_dest_plant
gb_options gb_options
dw_phase_options dw_phase_options
gb_phase_options gb_phase_options
dw_load_option dw_load_option
gb_indicators gb_indicators
gb_1 gb_1
gb_source gb_source
dw_source dw_source
end type
global w_generate_orders_2 w_generate_orders_2

type variables
Boolean		ib_async_running, &
		ib_toolbarVisible, &
		ib_ReInquire, &
		ib_Dont_Increment_timeout, &
		ib_good_product

//Double		id_p24b_task_number, &
//		id_p24b_last_record, &
//		id_p24b_max_record
//
DWObject	idwo_last_clicked

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long		il_SelectedColor, &
		il_SelectedTextColor,&
		il_lastclicked_background, &
		il_ChangedRow
		
		
s_error		istr_error_info

// This is for the async call
u_pas201		iu_pas201
u_ws_pas3		iu_ws_pas3

////String		is_qty_by_age_code, &
////		is_shift_production, &
////		is_sold_qty_by_type, &
////		is_sold_qty_by_age, &
////		is_carry_over,&
//string	is_input 
//String	is_debug
String		is_colname, &
				is_product, &
				is_input, &
				is_output, &
				is_ChangedColumnName, &
				is_debug, &
				is_start_date, &
				is_end_Date
//w_base_sheet	iw_order_detail
end variables

forward prototypes
public subroutine wf_print ()
public function integer wf_modified ()
public subroutine wf_open_shells ()
public function boolean wf_validate ()
public function boolean wf_update ()
end prototypes

public subroutine wf_print ();
end subroutine

public function integer wf_modified ();dw_date.ResetUpdate()
dw_complex.ResetUpdate()
dw_complex_plant.ResetUpdate()
dw_planned_tran_type.ResetUpdate()
dw_options.ResetUpdate()
dw_phase_options.ResetUpdate()
dw_load_option.ResetUpdate()

return 0
end function

public subroutine wf_open_shells ();String	ls_inventory
Window	lw_temp


OpenSheetWithParm(lw_temp, is_input , "w_num_shells_to_generate",iw_frame,0)

return 

end subroutine

public function boolean wf_validate ();String	ls_build_string, &
         ls_output_string, &
			ls_location_code, &
			ls_complex_plant, &	
			ls_planned_tr_type, &
			ls_load_option, &
			ls_phase_option, &
			ls_options, &
			ls_num_days, &
			ls-seperate-ind, &
		   ls_PaDateRange
			
nvuo_pa_business_rules luo_pa
			
Date		ldt_ship_date
			
Long		ll_ret

dw_dest_plant.AcceptText()
dw_complex.AcceptText()
dw_planned_tran_type.AcceptText()
dw_phase_options.AcceptText()
dw_options.AcceptText()
dw_date.AcceptText()
dw_load_option.AcceptText()


ls_complex_plant = dw_complex_plant.GetItemString(1, "complex_plant_rb")
// pjm 09/08/2014 - changed for new options
CHOOSE CASE ls_complex_plant		 
   CASE 'SP','DP' 
	  dw_dest_plant.AcceptText()      
     ls_location_code = dw_dest_plant.GetItemString(1, "location_code")
     If isnull(ls_location_code) or ls_location_code <= '   ' Then
	    MessageBox('Plant Code Error', "Plant Code is Missing")
	    dw_dest_plant.SetFocus()
	    Return False
     End If
	CASE 'SC','DC'	
	  dw_complex.AcceptText()
     ls_location_code = dw_complex.GetItemString(1, "complex_code")
     If isnull(ls_location_code) or ls_location_code <= '   ' Then
	     MessageBox('Complex Code Error', "Complex Code is Missing")
	     dw_complex.SetFocus()
	     Return False
	  End if	
End CHOOSE

If Dec(em_num_days.Text) > 0 Then
	ls_num_days = string(dec(em_num_days.Text))
end if

ldt_ship_date = dw_date.GetItemDate(1, "sched_date")

if ldt_ship_date  < today() then
	MessageBox('Ship date error', "Ship date cannot be less than current date")
	Return False
End If


if not luo_pa.uf_check_pa_date(dw_date.GetItemDate(1, "sched_date"), ls_PaDateRange)then
	iw_frame.SetMicroHelp("Ship Date cannot be later than " + ls_PaDateRange)
	dw_date.SetFocus()
	dw_date.SetColumn("sched_date")
	return false
End If


ls_planned_tr_type = dw_planned_tran_type.GetItemString(1, "planned_tr_type")
If isnull(ls_planned_tr_type) or ls_planned_tr_type <= '   ' Then
	MessageBox('Planned Transfer type error', "Planned Transfer type is Missing")
	dw_planned_tran_type.SetFocus()
	Return False
End If

	  dw_options.AcceptText()
     ls_options = dw_options.GetItemString(1, "options")
if ls_options = 'S'THEN
   ls_load_option = dw_load_option.GetItemString(1, "load_option")
   If isnull(ls_load_option) or ls_load_option <= '   ' Then
	   MessageBox('Load Option Error', "Load Option is Missing")
	   dw_load_option.SetFocus()
	   Return False
   End If
end if

return ( true )

end function

public function boolean wf_update ();String	ls_header_string, &
         ls_output_string, &
			ls_input_string, &
			ls_location_code, &
			ls_complex_plant, &	
			ls_planned_tr_type, &
			ls_load_option, &
			ls_phase_option, &
			ls_option, &
			ls_num_days, &
			ls-seperate-ind, &
			ls_inquire_option, &
			ls_input, &
			ls_source_option, &
			ls_location_group_groupid, ls_temp
			
Date		ldt_ship_date
			
Long		ll_ret

Window   lw_temp


dw_dest_plant.AcceptText()
dw_complex.AcceptText()
dw_planned_tran_type.AcceptText()
dw_phase_options.AcceptText()
dw_options.AcceptText()
dw_date.AcceptText()
dw_load_option.AcceptText()


ls_complex_plant = dw_complex_plant.GetItemString(1, "complex_plant_rb")
// pjm 09/08/2014 - changed for new options
CHOOSE CASE ls_complex_plant		 
   CASE 'SP','DP' 
	  dw_dest_plant.AcceptText()      
     ls_location_code = dw_dest_plant.GetItemString(1, "location_code")
	CASE 'SC','DC'	
	  dw_complex.AcceptText()
     ls_location_code = dw_complex.GetItemString(1, "complex_code")
End CHOOSE

If (ls_complex_plant = 'SP') or (ls_complex_plant = 'SC') Then
	ls_source_option = ' '
	ls_location_group_groupid = '    '    	
Else
	ls_source_option = dw_source.GetItemString(1, "source_rb")
	If ls_source_option = 'A' Then
		ls_location_group_groupid = '    '
	Else
//		ls_location_group_groupid = string(ole_location_group.object.groupID())		
		ls_location_group_groupid = String(uo_groups.uf_get_sel_id(ls_location_group_groupid))
	End If
End If

If Dec(em_num_days.Text) > 0 Then
	ls_num_days = string(dec(em_num_days.Text))
else
	ls_num_days = '0'
end if

ldt_ship_date = dw_date.GetItemDate(1, "sched_date")

ls_planned_tr_type = dw_planned_tran_type.GetItemString(1, "planned_tr_type")

ls_load_option = dw_load_option.GetItemString(1, "load_option")

ls_phase_option = dw_phase_options.GetItemString(1, "phase_options")

ls_option = dw_options.GetItemString(1, "options")

If cbx_seperate_ind.checked then
	ls-seperate-ind = 'Y'
else 
	ls-seperate-ind = 'N'
end if

ls_inquire_option = 'I'

If Not wf_validate() Then
	Return False
End If

ls_header_string  = ls_inquire_option + '~t'
ls_header_string += ls_complex_plant + '~t'
ls_header_string += ls_location_code + '~t'
ls_header_string += String(ldt_ship_date,'yyyy-mm-dd') +'~t' 
ls_header_string += ls_planned_tr_type + '~t'
ls_header_string += ls_option + '~t'
ls_header_string += ls_phase_option + '~t'
ls_header_string += ls-seperate-ind + '~t'
ls_header_string += ls_num_days + '~t'
ls_header_string += ls_load_option + '~t'
ls_header_string += ls_source_option + '~t'
ls_header_string += ls_location_group_groupid + '~r~n'

 
is_input = ls_header_string

//li_ret = iu_pas201.nf_pasp40cr_inq_rmt_consumption(istr_error_info, &
//									is_input, &
//									ls_output_values) 
//						

//if iu_pas201.nf_pasp75cr_gen_tfr_orders(istr_error_info, &
//									ls_output_string, &
//									ls_header_string, &
//									ls_input_string)  < 0 Then

if iu_ws_pas3.uf_pasp75gr(istr_error_info, &
									ls_output_string, &
									ls_header_string, &
									ls_input_string)  < 0 Then
									Return False
end if
		
	
If ls_option = 'S'  then
	is_input = ls_output_string
	wf_open_shells()
//   OpenWithParm(lw_temp, ls_output_string, "w_num_shells_to_generate")
end if 



ls_input = Message.StringParm
//s_error = istr_error_info
//
//MessageBox(This.Title, s_error)
//

SetProfileString(iw_frame.is_UserINI, 'PAS', 'w_generate_orders.PlantComplex', ls_complex_plant )
SetProfileString(iw_frame.is_UserINI, 'PAS', 'w_generate_orders.numberdays', ls_num_days)

dw_date.ResetUpdate()
dw_complex.ResetUpdate()
dw_complex_plant.ResetUpdate()
dw_planned_tran_type.ResetUpdate()
dw_options.ResetUpdate()
dw_phase_options.ResetUpdate()
dw_load_option.ResetUpdate()


return true

end function

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_inquire')

iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

end event

event resize;call super::resize;//integer li_x		= 1
//integer li_y		= 1
//
//dw_pas_test_upd.width	= width - (50 + li_x)
//dw_pas_test_upd.height	= height - (226 + li_y)
//
//
end event

event ue_postopen;call super::ue_postopen;Environment		le_env

String		ls_temp

iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Generate Orders"
istr_error_info.se_user_id = sqlca.userid


dw_load_option.visible= false
		
		
dw_complex_plant.AcceptText()	
ls_temp = dw_complex_plant.GetItemString(1, "complex_plant_rb")
If left(dw_complex_plant.GetItemString(1, "complex_plant_rb"),1) = 'S' Then
	gb_source.Visible = False
	dw_source.Visible = False
Else
	gb_source.Visible = True
	dw_source.Visible = True
End If

//ole_location_group.object.GroupType(1)
//ole_location_group.object.LoadObject()
//ole_location_group.Visible = False
uo_groups.uf_load_groups('L')
uo_groups.Visible = False




end event

event activate;call super::activate;string   ls_input 


iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_inquire')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_disable('m_deleterow')
	
	
ls_input = Message.StringParm


end event

event ue_query;call super::ue_query;
wf_retrieve()

end event

on w_generate_orders_2.create
int iCurrent
call super::create
this.uo_groups=create uo_groups
this.em_num_days=create em_num_days
this.st_1=create st_1
this.cbx_seperate_ind=create cbx_seperate_ind
this.dw_options=create dw_options
this.dw_planned_tran_type=create dw_planned_tran_type
this.st_ship_date=create st_ship_date
this.dw_date=create dw_date
this.dw_complex=create dw_complex
this.dw_complex_plant=create dw_complex_plant
this.gb_complex_plant=create gb_complex_plant
this.dw_dest_plant=create dw_dest_plant
this.gb_options=create gb_options
this.dw_phase_options=create dw_phase_options
this.gb_phase_options=create gb_phase_options
this.dw_load_option=create dw_load_option
this.gb_indicators=create gb_indicators
this.gb_1=create gb_1
this.gb_source=create gb_source
this.dw_source=create dw_source
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_groups
this.Control[iCurrent+2]=this.em_num_days
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cbx_seperate_ind
this.Control[iCurrent+5]=this.dw_options
this.Control[iCurrent+6]=this.dw_planned_tran_type
this.Control[iCurrent+7]=this.st_ship_date
this.Control[iCurrent+8]=this.dw_date
this.Control[iCurrent+9]=this.dw_complex
this.Control[iCurrent+10]=this.dw_complex_plant
this.Control[iCurrent+11]=this.gb_complex_plant
this.Control[iCurrent+12]=this.dw_dest_plant
this.Control[iCurrent+13]=this.gb_options
this.Control[iCurrent+14]=this.dw_phase_options
this.Control[iCurrent+15]=this.gb_phase_options
this.Control[iCurrent+16]=this.dw_load_option
this.Control[iCurrent+17]=this.gb_indicators
this.Control[iCurrent+18]=this.gb_1
this.Control[iCurrent+19]=this.gb_source
this.Control[iCurrent+20]=this.dw_source
end on

on w_generate_orders_2.destroy
call super::destroy
destroy(this.uo_groups)
destroy(this.em_num_days)
destroy(this.st_1)
destroy(this.cbx_seperate_ind)
destroy(this.dw_options)
destroy(this.dw_planned_tran_type)
destroy(this.st_ship_date)
destroy(this.dw_date)
destroy(this.dw_complex)
destroy(this.dw_complex_plant)
destroy(this.gb_complex_plant)
destroy(this.dw_dest_plant)
destroy(this.gb_options)
destroy(this.dw_phase_options)
destroy(this.gb_phase_options)
destroy(this.dw_load_option)
destroy(this.gb_indicators)
destroy(this.gb_1)
destroy(this.gb_source)
destroy(this.dw_source)
end on

event ue_set_data;call super::ue_set_data;//Choose Case as_data_item
//	case 'division'
//		dw_division.uf_set_division(as_value)
//End Choose

end event

event closequery;call super::closequery;dw_date.ResetUpdate()
dw_complex.ResetUpdate()
dw_complex_plant.ResetUpdate()
dw_planned_tran_type.ResetUpdate()
dw_options.ResetUpdate()
dw_phase_options.ResetUpdate()
dw_load_option.ResetUpdate()

call super::closequery;

end event

event open;call super::open;
string   ls_value, &
         ls_complex_code, &
         ls_value_days

if dw_complex_plant.rowcount( ) = 0 Then
	dw_complex_plant.insertrow( 0)
End If

if dw_date.rowcount( ) = 0 Then
	dw_date.insertrow( 0)
End If

if dw_complex.rowcount( ) = 0 Then
	dw_complex.insertrow( 0)
End If

if dw_planned_tran_type.rowcount( ) = 0 Then
	dw_planned_tran_type.insertrow( 0)
End If

if dw_options.rowcount( ) = 0 Then
	dw_options.insertrow( 0)
End If


if dw_load_option.rowcount( ) = 0 Then
	dw_load_option.insertrow( 0)
End If


dw_Date.SetItem(1, "sched_date", RelativeDate(Today(), 1))

dw_date.ResetUpdate()

 
ls_value = ProfileString(iw_frame.is_UserINI, 'PAS', 'w_generate_orders.PlantComplex', "None") 
// pjm 09/08/2014 changed for new options
If Right(ls_value,1) = 'C' Then
		dw_dest_plant.Visible = false
		dw_complex.Visible = true
		dw_complex_plant.SetItem(1, "complex_plant_rb", ls_value)
		ls_complex_code = ProfileString( 'ibpuser.ini', 'Pas', 'w_generate_orders.Lastcomplexcode',"None")
		If ls_complex_code = 'C' Then ls_complex_code = 'SC'  // convert old option to new
		dw_complex.SetItem( 1, "complex_code", ls_complex_code)
Else
      dw_dest_plant.Visible = TRUE
		dw_complex.Visible = False
End If
If Left(ls_value,1) = 'D' Then
		dw_options.dataobject='d_options_choice_dest'
		dw_options.insertrow( 0)
		st_ship_date.Text = 'Delivery Date'
Else
		st_ship_date.Text = 'Ship Date'
End If
//end pjm


ls_value_days = ProfileString(iw_frame.is_UserINI, 'PAS', 'w_generate_orders.numberdays', "None")

If ls_value_days = 'None' then
   em_num_days.text = '1'
else 
   em_num_days.text = ls_value_days
end if
		

		











end event

event close;call super::close;
If IsValid(iu_pas201) Then Destroy(iu_pas201)
If IsValid(iu_ws_pas3) Then Destroy(iu_ws_pas3)

dw_date.ResetUpdate()
dw_complex.ResetUpdate()
dw_complex_plant.ResetUpdate()
dw_planned_tran_type.ResetUpdate()
dw_options.ResetUpdate()
dw_phase_options.ResetUpdate()
dw_load_option.ResetUpdate()

end event

type uo_groups from u_group_list within w_generate_orders_2
integer x = 1705
integer y = 56
integer taborder = 20
end type

on uo_groups.destroy
call u_group_list::destroy
end on

type em_num_days from editmask within w_generate_orders_2
integer x = 933
integer y = 2120
integer width = 128
integer height = 92
integer taborder = 100
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
string text = "none"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "##"
boolean usecodetable = true
end type

type st_1 from statictext within w_generate_orders_2
integer x = 59
integer y = 2120
integer width = 827
integer height = 68
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Number of Days Out to Pull Pallets:"
boolean focusrectangle = false
end type

type cbx_seperate_ind from checkbox within w_generate_orders_2
integer x = 55
integer y = 1976
integer width = 1006
integer height = 128
integer taborder = 90
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Separate Orders by Latest Delivery Date     "
boolean lefttext = true
end type

type dw_options from u_base_dw_ext within w_generate_orders_2
integer x = 69
integer y = 1044
integer width = 841
integer height = 320
integer taborder = 70
string dataobject = "d_options_choice"
boolean border = false
end type

event itemchanged;call super::itemchanged;choose case data
	case 'S'		
		dw_phase_options.Visible = False
		gb_phase_options.visible = False
		em_num_days.visible = False
		st_1.visible = false
		cbx_seperate_ind.visible = False
		gb_indicators.visible = False
		dw_load_option.visible = True		
   case 'O'
		gb_phase_options.visible = true
		dw_phase_options.Visible = true
      gb_indicators.visible = true
		em_num_days.visible = true
		st_1.visible = true
		cbx_seperate_ind.visible = true		
		dw_load_option.visible = false
	case 'R'
		gb_phase_options.visible = true
		dw_phase_options.Visible = true
		gb_indicators.visible = true
		em_num_days.visible = true
		st_1.visible = true
		cbx_seperate_ind.visible = true		
		dw_load_option.visible = false
end choose
end event

type dw_planned_tran_type from u_base_dw_ext within w_generate_orders_2
integer x = 64
integer y = 864
integer width = 1088
integer height = 64
integer taborder = 50
string dataobject = "d_planned_tran_type"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild		ldwc_type


This.GetChild("planned_tr_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PTFRTYPE")
This.InsertRow(0)

dw_planned_tran_type.ResetUpdate()
end event

type st_ship_date from statictext within w_generate_orders_2
integer x = 37
integer y = 760
integer width = 526
integer height = 88
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ship Date:"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type dw_date from u_base_dw_ext within w_generate_orders_2
integer x = 613
integer y = 748
integer width = 393
integer height = 84
integer taborder = 40
string dataobject = "d_asap_date"
boolean border = false
end type

type dw_complex from u_base_dw_ext within w_generate_orders_2
integer x = 50
integer y = 612
integer width = 690
integer height = 108
integer taborder = 30
string dataobject = "d_complex"
boolean border = false
end type

event constructor;call super::constructor;String				ls_complex_code
integer				li_sqlcode

DataWindowChild	ldwc_child

ib_updateable = False

This.GetChild("complex_code", ldwc_child)

ldwc_child.SetTransObject(SQLCA)
li_sqlcode = ldwc_child.Retrieve("COMPLEX2")

This.InsertRow(0)

//ls_complex_code = ProfileString('ibpuser.ini', 'Pas', 'w_generate_orders.Lastcomplexcode' ,"None")
//if ls_complex_code = '' then
//	ls_complex_code = string('   ')
//end if
//


//This.SetItem( 1, "complex_code", ls_complex_code)




dw_complex.ResetUpdate()
end event

event itemchanged;call super::itemchanged;string ls_complex_code

dw_complex.AcceptText()
ls_complex_code = dw_complex.GetItemString(1, "complex_code")

SetProfileString('ibpuser.ini', 'Pas', 'w_generate_orders.Lastcomplexcode', ls_complex_code)
end event

type dw_complex_plant from u_base_dw_ext within w_generate_orders_2
integer x = 59
integer y = 72
integer width = 608
integer height = 316
integer taborder = 10
string dataobject = "d_complex_plant_radio"
boolean border = false
end type

event itemchanged;call super::itemchanged;// pjm 09/08/2014 - changed for new options
Choose Case Right(data,1)
	Case 'P'
		dw_dest_plant.Visible = TRUE
		dw_complex.Visible = False
	Case 'C'
      dw_dest_plant.Visible = False
		dw_complex.Visible = True
End Choose

dw_options.SetRedraw(False)
Choose Case Left(data,1)
	Case 'S'
		st_ship_date.Text = 'Ship Date'
		dw_options.dataobject='d_options_choice'
		dw_options.insertrow( 0)
		gb_source.visible = False
		dw_source.visible = False
//		ole_location_group.Visible = False
		uo_groups.Visible = False
		
	Case 'D'
		gb_phase_options.visible = true
		dw_phase_options.Visible = true
      gb_indicators.visible = true
		em_num_days.visible = true
		st_1.visible = true
		cbx_seperate_ind.visible = true		
		dw_load_option.visible = false
      st_ship_date.Text = 'Delivery Date'
		dw_options.dataobject='d_options_choice_dest'
		dw_options.insertrow( 0)
		gb_source.visible = true
		dw_source.visible = true
		If dw_source.GetItemString(1, "source_rb") = 'A' Then
//			ole_location_group.Visible = False
			uo_groups.Visible = False
		Else
//			ole_location_group.Visible = True
			uo_groups.Visible = True
		End If
		
End Choose

dw_options.SetRedraw(True)

SetProfileString( iw_frame.is_UserINI, "Pas", "w_generate_orders.PlantComplex",data)

end event

type gb_complex_plant from groupbox within w_generate_orders_2
integer x = 37
integer y = 20
integer width = 645
integer height = 392
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Complex or Plant Option"
end type

type dw_dest_plant from u_plant within w_generate_orders_2
integer x = 32
integer y = 616
integer width = 1445
integer taborder = 20
end type

type gb_options from groupbox within w_generate_orders_2
integer x = 37
integer y = 988
integer width = 983
integer height = 404
integer taborder = 120
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Options"
end type

type dw_phase_options from u_base_dw_ext within w_generate_orders_2
integer x = 46
integer y = 1484
integer width = 965
integer height = 384
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_phase_options_choice"
boolean border = false
end type

event constructor;call super::constructor;this.InsertRow(0)
end event

type gb_phase_options from groupbox within w_generate_orders_2
integer x = 37
integer y = 1436
integer width = 983
integer height = 456
integer taborder = 130
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Phase Options"
end type

type dw_load_option from u_base_dw_ext within w_generate_orders_2
integer x = 50
integer y = 1464
integer width = 919
integer height = 104
integer taborder = 140
boolean bringtotop = true
string dataobject = "d_load_option"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild		ldwc_type


This.GetChild("load_option", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("LOADOPT")
This.InsertRow(0)


dw_load_option.ResetUpdate()
end event

type gb_indicators from groupbox within w_generate_orders_2
integer x = 37
integer y = 1920
integer width = 1088
integer height = 332
integer taborder = 150
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Indicators"
end type

type gb_1 from groupbox within w_generate_orders_2
integer x = 37
integer y = 988
integer width = 983
integer height = 404
integer taborder = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Options"
end type

type gb_source from groupbox within w_generate_orders_2
integer x = 841
integer y = 64
integer width = 704
integer height = 356
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Source"
end type

type dw_source from u_base_dw_ext within w_generate_orders_2
integer x = 914
integer y = 128
integer width = 622
integer height = 256
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_source_radio"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;Choose Case data
	Case 'A'
//		ole_location_group.Visible = False
		uo_groups.Visible = False
	Case 'I', 'E'
//		ole_location_group.Visible = True
		uo_groups.Visible = True
End Choose
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
07w_generate_orders_2.bin 
2600000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000800ff86001d2d95a00000003000001000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000800ff86001d2d95a800ff86001d2d95a00000000000000000000000000000001fffffffe00000003fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
26ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000200000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
17w_generate_orders_2.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
