﻿$PBExportHeader$w_epa_view_new_inq.srw
forward
global type w_epa_view_new_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_epa_view_new_inq
end type
type dw_1 from u_base_dw_ext within w_epa_view_new_inq
end type
end forward

global type w_epa_view_new_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 2263
integer height = 688
long backcolor = 67108864
event type boolean uf_product ( string as_product )
dw_plant dw_plant
dw_1 dw_1
end type
global w_epa_view_new_inq w_epa_view_new_inq

type variables
Boolean		ib_required = TRUE
Boolean		ib_error_occurred = FALSE

datawindowchild 			idddw_child_state, &
								idddw_child_period
nvuo_fab_product_code	invuo_fab_product_code
s_error						istr_error_info								
end variables

forward prototypes
public function boolean uf_product (string as_product)
public function boolean uf_set_product_desc (string as_product_desc)
public function boolean wf_product_state_changed (string as_product_state)
public function boolean wf_period_changed (string as_period)
public function boolean wf_date_changed (string as_date)
public function boolean wf_oldest_production_date_changed (string as_oldest_prod_date)
public function boolean wf_indicator_changed (string as_indicator)
end prototypes

event type boolean uf_product(string as_product);Boolean			lb_return
			
					
lb_return	= invuo_fab_product_code.uf_check_product(as_product)

If not lb_return then
	If	invuo_fab_product_code.ib_error_occurred then
		iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
	Else
		iw_frame.SetMicroHelp(as_product + " is an invalid Product Code")
	End If
End If
	
Return lb_return
end event

public function boolean uf_product (string as_product);Boolean			lb_return
			
					
lb_return	= invuo_fab_product_code.uf_check_product(as_product)

If not lb_return then
	If	invuo_fab_product_code.ib_error_occurred then
		iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
	Else
		iw_frame.SetMicroHelp(as_product + " is an invalid Product Code")
	End If
End If
	
Return lb_return
end function

public function boolean uf_set_product_desc (string as_product_desc);Return (dw_1.SetItem( 1, "fab_product_description", as_product_desc) = 1)
end function

public function boolean wf_product_state_changed (string as_product_state);string 	ls_product_state_desc
			
long 		ll_row

ll_row = idddw_child_state.Find('type_code = "' + Trim(as_product_state) + '"', 1, idddw_child_state.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(as_product_state + " is an Invalid Product State")

//	dw_instances_inq.SetFocus()
//	dw_instances_inq.SelectText(1, Len(as_product_state))
	
	return false
End If

//ls_product_state_desc = idddw_child_state.GetItemString(ll_row, 'type_short_desc')
//dw_instances_inq.setitem(1,"product_state_description",(ls_product_state_desc))
SetProfileString( iw_frame.is_UserINI, "Pas", "LastProductState",Trim(as_product_state))

//This.SetFocus()
//This.SelectText(1, Len(as_product_state))
//
return true
end function

public function boolean wf_period_changed (string as_period);long 		ll_row

ll_row = idddw_child_state.Find('type_code = "' + Trim(as_period) + '"', 1, idddw_child_state.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(as_period + " is an Invalid Period")

//	dw_instances_inq.SetFocus()
//	dw_instances_inq.SelectText(1, Len(as_product_state))
	
	return false
End If

//ls_product_state_desc = idddw_child_state.GetItemString(ll_row, 'type_short_desc')
//dw_instances_inq.setitem(1,"product_state_description",(ls_product_state_desc))
SetProfileString( iw_frame.is_UserINI, "Pas", "LastPeriod",Trim(as_period))

//This.SetFocus()
//This.SelectText(1, Len(as_product_state))
//
return true
end function

public function boolean wf_date_changed (string as_date);//long 		ll_row

//ll_row = idddw_child_state.Find('type_code = "' + Trim(as_period) + '"', 1, idddw_child_state.RowCount())
If as_date = '' Then
	iw_Frame.SetMicroHelp(as_date + " is an Invalid date")

//	dw_instances_inq.SetFocus()
//	dw_instances_inq.SelectText(1, Len(as_product_state))
	
	return false
End If

//ls_product_state_desc = idddw_child_state.GetItemString(ll_row, 'type_short_desc')
//dw_instances_inq.setitem(1,"product_state_description",(ls_product_state_desc))
SetProfileString( iw_frame.is_UserINI, "Pas", "LastDate",Trim(as_date))

//This.SetFocus()
//This.SelectText(1, Len(as_product_state))
//
return true
end function

public function boolean wf_oldest_production_date_changed (string as_oldest_prod_date);//long 		ll_row

//ll_row = idddw_child_state.Find('type_code = "' + Trim(as_period) + '"', 1, idddw_child_state.RowCount())
If as_oldest_prod_date = '' Then
	iw_Frame.SetMicroHelp(as_oldest_prod_date + " is an Invalid date")

//	dw_instances_inq.SetFocus()
//	dw_instances_inq.SelectText(1, Len(as_product_state))
	
	return false
End If

//ls_product_state_desc = idddw_child_state.GetItemString(ll_row, 'type_short_desc')
//dw_instances_inq.setitem(1,"product_state_description",(ls_product_state_desc))
SetProfileString( iw_frame.is_UserINI, "Pas", "LastOldestProductionDate",Trim(as_oldest_prod_date))

//This.SetFocus()
//This.SelectText(1, Len(as_product_state))
//
return true
end function

public function boolean wf_indicator_changed (string as_indicator);//long 		ll_row

//ll_row = idddw_child_state.Find('type_code = "' + Trim(as_period) + '"', 1, idddw_child_state.RowCount())
If as_indicator = '' Then
	iw_Frame.SetMicroHelp(as_indicator + " is an Invalid indicator")

//	dw_instances_inq.SetFocus()
//	dw_instances_inq.SelectText(1, Len(as_product_state))
	
	return false
End If

//ls_product_state_desc = idddw_child_state.GetItemString(ll_row, 'type_short_desc')
//dw_instances_inq.setitem(1,"product_state_description",(ls_product_state_desc))
SetProfileString( iw_frame.is_UserINI, "Pas", "LastIndicator",Trim(as_indicator))

//This.SetFocus()
//This.SelectText(1, Len(as_product_state))
//
return true
end function

on w_epa_view_new_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_1
end on

on w_epa_view_new_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_1)
end on

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

event ue_postopen;call super::ue_postopen;Date	ldt_temp
string ls_Last_State, &
		ls_Last_Period, &
		ls_Last_Indicator
		
//iw_parentwindow.Event ue_get_data('plant')
//dw_plant.uf_set_plant_code(Message.StringParm)
	
iw_parentwindow.Event ue_get_data('product_code')
dw_1.SetItem( 1, "fab_product_code", Message.StringParm)
iw_parentwindow.Event ue_get_data('product_desc')
dw_1.SetItem( 1, "fab_product_description", Message.StringParm)

dw_1.GetChild("product_state", idddw_child_state)

idddw_child_state.SetTransObject(SQLCA)
idddw_child_state.Retrieve("PRDSTATE")
idddw_child_state.SetSort("type_code")
idddw_child_state.Sort()
//idddw_child.InsertRow(0)
//dw_1.InsertRow(0)


ls_Last_State = ProfileString( iw_frame.is_UserINI, "Pas", "LastProductState","2")
dw_1.SetItem( 1, "product_state", ls_Last_State)

//iw_parentwindow.Event ue_get_data('state')
//dw_1.SetItem( 1, "product_state", Message.StringParm)


dw_1.GetChild("period", idddw_child_period)

idddw_child_period.SetTransObject(SQLCA)
idddw_child_period.Retrieve("PAPERIOD")
idddw_child_period.SetSort("type_code")
idddw_child_period.Sort()
//idddw_child.InsertRow(0)
//This.InsertRow(0)

ls_Last_Period = ProfileString( iw_frame.is_UserINI, "Pas", "LastPeriod","1")
dw_1.SetItem( 1, "period", ls_Last_Period)

//iw_parentwindow.Event ue_get_data('period')
//dw_1.SetItem( 1, "period", Message.StringParm)

//ldt_temp = Date(iw_parent_window.dw_begin_end_date.uf_GetBeginDate())
//ldt_temp = Date(Today())
//dw_1.SetItem( 1, "date", ldt_temp)

//iw_parentwindow.Event ue_get_data('date')
//dw_1.SetItem( 1, "date", Date(Message.StringParm))

//ldt_temp = Date(iw_parent_window.dw_begin_end_date.uf_GetBeginDate())
ldt_temp = Date(ProfileString( iw_frame.is_UserINI, "Pas", "LastDate",''))
dw_1.SetItem( 1, "date", ldt_temp)

//iw_parentwindow.Event ue_get_data('oldest_date')
//dw_1.SetItem( 1, "oldest_production_date", Date(Message.StringParm))

ldt_temp = Date(ProfileString( iw_frame.is_UserINI, "Pas", "LastOldestProductionDate",''))
dw_1.SetItem( 1, "oldest_production_date", ldt_temp)

//dw_1.SetItem(1, "available_to_ship", "Y")
iw_parentwindow.Event ue_get_data('available')
dw_1.SetItem( 1, "available_to_ship", Message.StringParm)

ls_Last_Indicator = ProfileString( iw_frame.is_UserINI, "Pas", "LastIndicator","Y")
dw_1.SetItem( 1, "available_to_ship", ls_Last_Indicator)
end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
 			ls_plant_desc, &
			ls_product_code, &
			ls_product_desc, & 
			ls_state, &
			ls_period, &
			ls_date, &
			ls_oldest_date, &
			ls_available, &
			ls_PaDateRange
			
u_string_functions		lu_strings

nvuo_pa_business_rules luo_pa

If dw_plant.AcceptText() = -1 &
	or dw_1.AcceptText() = -1 Then return
	
ls_plant = dw_plant.uf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End if
ls_plant_desc = dw_plant.uf_get_plant_descr()

ls_product_code = Trim(dw_1.GetItemString( 1, "fab_product_code"))
If lu_strings.nf_IsEmpty(ls_product_code) Then
	iw_frame.SetMicroHelp("Product is a required field")
	dw_1.SetFocus()
	dw_1.SetColumn("fab_product_code")
	Return
End if
ls_product_desc = Trim(dw_1.GetItemString( 1, "fab_product_description"))

ls_state = Trim(dw_1.GetItemString( 1, "product_state"))
If lu_strings.nf_IsEmpty(ls_state) Then
	iw_frame.SetMicroHelp("Product State is a required field")
	dw_1.SetFocus()
	dw_1.SetColumn("product_state")
	Return
End if

ls_period = Trim(dw_1.GetItemString( 1, "period"))
If lu_strings.nf_IsEmpty(ls_period) Then
	iw_frame.SetMicroHelp("Period is a required field")
	dw_1.SetFocus()
	dw_1.SetColumn("period")
	Return
End if

ls_date = Trim(String(dw_1.GetItemDate( 1, "date")))
If lu_strings.nf_IsEmpty(ls_date) Then
	iw_frame.SetMicroHelp("Date is a required field")
	dw_1.SetFocus()
	dw_1.SetColumn("date")
	Return
End if

if not luo_pa.uf_check_pa_date(date(ls_date), ls_PaDateRange) then
	iw_frame.SetMicroHelp("Date invalid")
	//iw_frame.SetMicroHelp("Ship Date cannot be later than " + ls_PaDateRange)
	dw_1.SetFocus()
	dw_1.SetColumn("date")
	Return
End If		

ls_oldest_date = Trim(String(dw_1.GetItemDate( 1, "oldest_production_date")))
If lu_strings.nf_IsEmpty(ls_oldest_date) Then
	iw_frame.SetMicroHelp("Oldest Production Date is a required field")
	dw_1.SetFocus()
	dw_1.SetColumn("oldest_production_date")
	Return
End if

ls_available = Trim(dw_1.GetItemString( 1, "available_to_ship"))
If lu_strings.nf_IsEmpty(ls_available) Then
	iw_frame.SetMicroHelp("Available to ship indicator is a required field")
	dw_1.SetFocus()
	dw_1.SetColumn("available_to_ship")
	Return
End if

iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('plant_desc',ls_plant_desc)
iw_parentwindow.Event ue_set_data('product_code',ls_product_code)
iw_parentwindow.Event ue_set_data('product_desc',ls_product_desc)
iw_parentwindow.Event ue_set_data('state',ls_state)
iw_parentwindow.Event ue_set_data('period',ls_period)
iw_parentwindow.Event ue_set_data('date',ls_date)
iw_parentwindow.Event ue_set_data('oldest_date',ls_oldest_date)
iw_parentwindow.Event ue_set_data('available',ls_available)

Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_epa_view_new_inq
boolean visible = false
integer x = 1691
integer y = 324
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_epa_view_new_inq
integer x = 1929
integer y = 456
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_epa_view_new_inq
integer x = 1623
integer y = 456
integer taborder = 30
end type

type dw_plant from u_plant within w_epa_view_new_inq
integer x = 64
integer y = 24
integer taborder = 10
boolean bringtotop = true
end type

type dw_1 from u_base_dw_ext within w_epa_view_new_inq
integer x = 9
integer y = 104
integer width = 2194
integer height = 272
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_epa_view_new_inq"
boolean border = false
boolean ib_scrollable = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;String	ls_product_code, &
			ls_product_state, &
			ls_product_state_desc, &
			ls_product_status_desc, &
			ls_color, &
			ls_product_status, &
			ls_period, &
			ls_date, &
			ls_indicator, &
			ls_PaDateRange
			
Boolean	lb_fresh_ind, &
			lb_frozen_ind			
			
Long		ll_row

nvuo_pa_business_rules luo_pa

If ib_Updateable Then
	choose case dwo.name
		case "fab_product_code"	
			
			ls_product_code = data
			IF Len(Trim(ls_product_code)) <> 0 THEN
				If uf_product( ls_product_code ) Then
					uf_set_product_desc(invuo_fab_product_code.uf_get_product_description( ))
					iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				Else
					This.SelectText(1, Len(ls_product_Code))
					Return 1
				END IF
			Else
				This.SelectText(1, Len(ls_product_Code))
				uf_set_product_desc("")
				If ib_required Then
					iw_frame.SetMicroHelp("Product Code is a required field")
					Return 1
				End If
			END IF
			
		case "product_state" 
			ls_product_state = data
			
			IF NOT wf_product_state_changed(ls_product_state) Then
				return 1
			End If
			
		case "period" 
			ls_period = data
			
			IF NOT wf_period_changed(ls_period) Then
				return 1
			End If	
			
		case "date" 
			ls_date = string(date(data))
			
			if not luo_pa.uf_check_pa_date(date(ls_date), ls_PaDateRange) then
				iw_frame.SetMicroHelp("Date invalid")
				//iw_frame.SetMicroHelp("Ship Date cannot be later than " + ls_PaDateRange)
				dw_1.SetFocus()
				dw_1.SetColumn("date")
				return 1
			End If					
			
			IF NOT wf_date_changed(ls_date) Then
				return 1
			End If	
			
		case "oldest_production_date" 
			ls_date = string(date(data))
			
			IF NOT wf_oldest_production_date_changed(ls_date) Then
				return 1
			End If	
			
		case "available_to_ship" 
			ls_indicator = data
			
			IF NOT wf_indicator_changed(ls_indicator) Then
				return 1
			End If				
	end choose	
End If

Return 0			
end event

event itemerror;call super::itemerror;Return 1
end event

event getfocus;call super::getfocus;If This.GetRow() = 1 and This.GetColumn() = 1 Then
	This.selectText(1,10)
End If 
end event

