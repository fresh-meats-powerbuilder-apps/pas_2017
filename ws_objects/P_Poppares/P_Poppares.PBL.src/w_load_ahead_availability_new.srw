﻿$PBExportHeader$w_load_ahead_availability_new.srw
forward
global type w_load_ahead_availability_new from w_base_sheet_ext
end type
type cb_5 from commandbutton within w_load_ahead_availability_new
end type
type dw_2 from u_base_dw_ext within w_load_ahead_availability_new
end type
type st_2 from statictext within w_load_ahead_availability_new
end type
type st_1 from statictext within w_load_ahead_availability_new
end type
type pb_2 from picturebutton within w_load_ahead_availability_new
end type
type pb_1 from picturebutton within w_load_ahead_availability_new
end type
type cb_4 from commandbutton within w_load_ahead_availability_new
end type
type cb_3 from commandbutton within w_load_ahead_availability_new
end type
type cb_2 from commandbutton within w_load_ahead_availability_new
end type
type dw_1 from u_plant within w_load_ahead_availability_new
end type
type dw_load_ahead_availability_dtl from u_base_dw_ext within w_load_ahead_availability_new
end type
type dw_load_ahead_move_orders from u_base_dw_ext within w_load_ahead_availability_new
end type
type dw_load_ahead_order from u_base_dw_ext within w_load_ahead_availability_new
end type
type dw_load_status from u_base_dw_ext within w_load_ahead_availability_new
end type
type dw_load_ahead_hdr1 from u_base_dw_ext within w_load_ahead_availability_new
end type
type cb_1 from commandbutton within w_load_ahead_availability_new
end type
end forward

global type w_load_ahead_availability_new from w_base_sheet_ext
boolean visible = false
integer width = 3675
integer height = 2164
string title = "Load Ahead Availability"
boolean maxbox = false
long backcolor = 67108864
cb_5 cb_5
dw_2 dw_2
st_2 st_2
st_1 st_1
pb_2 pb_2
pb_1 pb_1
cb_4 cb_4
cb_3 cb_3
cb_2 cb_2
dw_1 dw_1
dw_load_ahead_availability_dtl dw_load_ahead_availability_dtl
dw_load_ahead_move_orders dw_load_ahead_move_orders
dw_load_ahead_order dw_load_ahead_order
dw_load_status dw_load_status
dw_load_ahead_hdr1 dw_load_ahead_hdr1
cb_1 cb_1
end type
global w_load_ahead_availability_new w_load_ahead_availability_new

type variables
s_error		istr_error_info
u_ws_pas5		iu_ws_pas5
string is_option, is_output_string, is_fill_calc = 'N', is_sort, is_confirm_string
datawindowchild	dwc_temp, ldwc_type
long il_ChangedRow
DataStore  ids_print
Datawindowchild idddw_child
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function integer wf_status_convert (integer total_rows)
public subroutine wf_load_detail (string as_primary_order, long as_row_number, string as_load_detail_string, ref string as_output)
public function boolean wf_moveorder ()
public function integer wf_deadlinedates ()
public function integer wf_creditstatus ()
public subroutine wf_update_load_status (string as_load_number)
end prototypes

public function boolean wf_retrieve ();long	ll_rec_count, &
		ll_row, &
		ll_count, &
		ll_max, ll_Nbrrows, ll_total_count, ll_cont, ll_seq, ll_rowcount
		
string		ls_input_string, &
			ls_output_string, &
			ls_temp, ls_input, ls_value, ls_ship_plant, ls_ship_date, ls_string_temp, ls_fill, ls_act, ls_las
			
integer	li_pos, li_start, li_count, li_fill

			
u_string_functions lu_string


cb_1.Visible = true
cb_1.eNABLED = TRUE

is_fill_calc = 'N'
cb_5.Visible = false
OpenWithParm(iw_inquirewindow, this, is_inquire_window_name)
ls_input = message.StringParm
dw_2.InsertRow(0)
If is_inquire_window_name = "w_load_ahead_availability_new_inq" then
dw_load_ahead_order.Visible = False
If not ls_input = "" then
li_pos = 1
li_start = 1	
do While li_pos > 0 
	li_pos = Pos(ls_input, '>', li_start)
	if li_pos = 0 then
		ls_value = Mid(ls_input, li_start)
	else
		ls_value = Mid(ls_input, li_start, li_pos - li_start)
	end if
	
	ls_value = Trim(ls_value)
	If li_count = 0 then
		dw_1.SetItem(1, "location_code", ls_value)
		dw_2.SetItem(1, "plant", ls_value)
	end if
	If li_count = 1 then
		dw_2.SetItem(1, "plant_descriptio", ls_value)
		dw_1.SetItem(1, "location_name", ls_value)
	end if
	If li_count = 2 then
		dw_load_ahead_hdr1.SetItem(1, "ship_date", date(ls_value))
	
	end if
	If li_count =3 then
		if ls_value = 'Y' then
			dw_load_status.SetItem(1, "full_loads", "Y")
		else
			dw_load_status.SetItem(1, "full_loads", "N")
		end if
	end if
	If li_count = 4 then
		if ls_value = 'Y' then 
			dw_load_status.SetItem(1, "combined_loads", "Y")
		else 
			dw_load_status.SetItem(1, "combined_loads", "N")
		end if
	end if
	if li_count =5 then
		if ls_value = 'Y' then 
		dw_load_status.SetItem(1, "uncombined_loads", "Y")
	else
		dw_load_status.SetItem(1, "uncombined_loads", "N")
		end if
	end if
	li_count ++
	li_start = li_pos + 1
Loop
end if

If Not ib_inquire Then
	Return False
End if

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait.. Inquiring Database")
This.SetRedraw(False)
dw_load_ahead_availability_dtl.Reset()
istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp18gr"
istr_error_info.se_message = Space(71)
dw_1.Modify("location_code.Protect = 1")
dw_load_ahead_hdr1.Modify("ship_date.Protect = 1")
dw_load_status.Modify("full_loads.Protect = 1")
dw_load_status.Modify("combined_loads.Protect = 1")
dw_load_status.Modify("uncombined_loads.Protect = 1")

ls_input_string = 'L' + '~t' + '' + '~t' + dw_2.GetItemString(1, "plant") + '~t' + string(dw_load_ahead_hdr1.GetItemDate(1, "ship_date"), "yyyy-mm-dd") + '~t' + dw_load_status.GetItemString(1, "full_loads") + '~t' + dw_load_status.GetItemString(1, "combined_loads") + '~t' + dw_load_status.GetItemString(1, "uncombined_loads")


If iu_ws_pas5.nf_pasp18gr(istr_error_info, ls_input_string, ls_output_string, ls_ship_plant, ls_ship_date) < 0 then
	This.SetRedraw(True)
	Return False
End if

ll_total_count = dw_load_ahead_availability_dtl.ImportString(ls_output_string)

dw_load_ahead_availability_dtl.SelectRow(1, TRUE)

wf_status_convert(ll_total_count)

If ll_total_count > 0 then
	SetMicroHelp(String(ll_total_count) + " Rows Retrieved")
else
	SetMicroHelp("0 Rows Retrieved")
End if


end if



If is_inquire_window_name = 'w_load_ahead_availability_order_new_inq' then
	
	dw_load_status.Visible = False
	dw_load_ahead_order.Visible = True
	
	dw_load_ahead_order.SetItem(1, "order_number", ls_input)
	If Not ib_inquire Then
		Return False
	End if
	SetPointer(HourGlass!)
	iw_frame.SetMicroHelp("Wait... Inquiring Database")
	This.SetRedraw(False)
	dw_load_ahead_availability_dtl.Reset()
	istr_error_info.se_event_name = "wf_retrieve"
	istr_error_info.se_procedure_name = "nf_pasp18gr"
	istr_error_info.se_message = Space(71)
	
	ls_input_string = 'O' + '~t' + ls_input
	
	If iu_ws_pas5.nf_pasp18gr(istr_error_info, ls_input_string, ls_output_string, ls_ship_plant, ls_ship_date) < 0 then
		This.SetRedraw(True)
		Return False
	End if
	
	dw_2.Reset()
	dw_1.reset()
	ll_count = dw_2.ImportString(ls_ship_plant)
	ll_count = dw_1.ImportString(ls_ship_plant)
//	dw_1.SetItem(1, "location_code", ls_ship_plant)
//	dw_2.SetItem(1, "plant_descriptio", ls_ship_plant)
	dw_1.AcceptText()
	

	ll_row = idddw_child.Find('plant = "' + Trim(ls_ship_plant) + '"', 1, idddw_child.RowCount())
	If ll_row <= 0 Then
		iw_Frame.SetMicroHelp(ls_ship_plant + " is an Invalid Plant Code")
		dw_2.SetFocus()
		dw_2.SelectText(1, Len(ls_ship_plant))
	Else
		dw_2.SetItem(1, 'plant_descriptio', idddw_child.GetItemString(ll_row, 'plant_descriptio'))
		dw_2.SetFocus()
		dw_2.SelectText(1, Len(ls_ship_plant))
		iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	End if

	
	
	
	
	
	
	
	
	
	
	
	ll_cont = dw_load_ahead_hdr1.ImportString(ls_ship_date)
	dw_load_ahead_hdr1.SetItem(1, "ship_date", date(ls_ship_date))
	dw_load_ahead_availability_dtl.Reset()
	ll_total_count= dw_load_ahead_availability_dtl.ImportString(ls_output_string)
	if ll_count = -1 and ll_cont = -1 then
		//dw_1.InsertRow(0)
		iw_frame.SetMicroHelp("Order not found")
		dw_load_ahead_availability_dtl.ResetUpdate()
		dw_2.ResetUpdate()
		dw_load_ahead_hdr1.ResetUpdate()
		This.SetRedraw(true)
		return true
	end if
	dw_2.ResetUpdate()
	dw_load_ahead_hdr1.ResetUpdate()
	
	dw_load_ahead_availability_dtl.SelectRow(1, TRUE)

	wf_status_convert(ll_total_count)



	If ll_total_count > 0 then
		SetMicroHelp(String(ll_total_count) + " Rows Retrieved")
	else
		SetMicroHelp("0 Rows Retrieved")
	End if

end if
//wf_deadlinedates()
ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "sort", "")
dw_load_ahead_availability_dtl.SetSort(ls_temp)
dw_load_ahead_availability_dtl.Sort()
dw_load_ahead_availability_dtl.GroupCalc()
is_sort = ls_temp
wf_deadlinedates()
wf_creditstatus()

dw_load_ahead_move_orders.SetItem(1, "move_date_to", dw_load_ahead_hdr1.GetItemDate(1, "ship_date"))

This.SetRedraw(True)
dw_load_ahead_availability_dtl.ResetUpdate()
dw_load_ahead_availability_dtl.SetFocus()
//dw_load_ahead_availability_dtl.GroupCalc()

ll_row = 1
ll_seq = 100
ll_rowcount = dw_load_ahead_availability_dtl.RowCount()

ls_las = ""

Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order") <> ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", ll_seq)
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++ 
		ll_seq += 100
		
	else
		ll_row ++
	end if
	//ls_las = ls_act
Loop
ll_row = 2
ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(ll_row,  "primaty_order") = ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", dw_load_ahead_availability_dtl.GetItemNumber(ll_row -1,"sequence"))
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		
		ll_row ++ 
		//ll_seq += 100
	else
		
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++
	end if
	//ls_las = ls_act
Loop
//wf_deadlinedates()

//ll_row = 1
//Do while ll_row <= ll_rowcount
//	
//	dw_load_ahead_availability_dtl.SetItem(ll_row, "deadline_date", dw_load_ahead_availability_dtl.GetItemDate(ll_row, "deadline_departure"))
//	ll_row ++
//Loop
//
//
//ll_row = 2
//ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
//Do while ll_row <= ll_rowcount
//	If dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order") = ls_las then
//		dw_load_ahead_availability_dtl.SetItem(ll_row, "deadline_date", dw_load_ahead_availability_dtl.GetItemDate(ll_row -1, "deadline_date"))
//		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
//		
//		ll_row ++
//	else
//		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
//		ll_row ++
//	end if
//Loop
//
//dw_load_ahead_availability_dtl.SetSort(ls_temp)
//dw_load_ahead_availability_dtl.Sort()
dw_load_ahead_move_orders.SetItem(1, "move_plant", "")
dw_load_ahead_availability_dtl.SetSort(ls_temp)
dw_load_ahead_availability_dtl.Sort()
dw_load_ahead_availability_dtl.GroupCalc()

return true
end function

public function boolean wf_update ();string ls_todate, ls_plant, ls_header, ls_primary_order, ls_update_string, ls_output, ls_load, ls_move_status_ind, ls_msg, ls_confirm_string, ls_temp, ls_order, ls_p_order
integer li_row, li_rowcount, li_sel_row, li_fillpercent, li_count, li_m_rowcount
long ll_case, ll_row, ll_count
Date ldt_moveto
DataStore lds_moves

li_row = 1
li_count = 1
ll_row = 1
li_rowcount = dw_load_ahead_availability_dtl.RowCount()


dw_load_ahead_move_orders.AcceptText()
ldt_moveto = dw_load_ahead_move_orders.GetItemDate(1, "move_date_to")
ls_plant =  dw_load_ahead_move_orders.GetItemString(1, "move_plant")
li_fillpercent = dw_load_ahead_move_orders.GetItemNumber(1, "fill_percent")
If is_fill_calc = 'N' then
	iw_frame.SetMicroHelp("Fill Percent must be calculated before saving")
else
	If IsNull(ls_plant) or ls_plant = "" then
		
		Do while ll_row <= li_rowcount
			If dw_load_ahead_availability_dtl.GetItemNumber(ll_row, "fiell_percent") >= li_fillpercent &
			   and dw_load_ahead_availability_dtl.GetItemString(ll_row, "exclude") = 'N' then
				ll_count ++
				ll_row ++
			else
				ll_row ++
			end if
		Loop
		ll_case = MessageBox("Confirm move", String(ll_count) + " orders/Loads will be moved to date" + " " + string(ldt_moveto), Question!, OkCancel!)
	else 
		
		Do while ll_row <= li_rowcount
			If dw_load_ahead_availability_dtl.GetItemNumber(ll_row, "fiell_percent") >= li_fillpercent &
   			   and dw_load_ahead_availability_dtl.GetItemString(ll_row, "exclude") = 'N' then
				ll_count ++
				ll_row ++
			else
				ll_row ++
			end if
		Loop
		ll_case = MessageBox("Confirm move", String(ll_count) + " order(s)/Load(s) will be moved to plant " + ls_plant + " on date " + string(ldt_moveto), Question!, OkCancel!)
	end if
end if

If ll_case = 1 and ll_count > 0 then

If is_fill_calc = 'N' then
	iw_frame.SetMicroHelp("Fill Percent must be calculated before saving")
else
	If dw_load_ahead_move_orders.GetItemDate(1, "move_date_to") < Today() then
		iw_frame.SetMicroHelp("move Orders To Ship Date is not within the PA Date Range")
	else
		iw_frame.SetMicroHelp("Wait... Updating the Database")
		dw_load_ahead_move_orders.AcceptText()
		ls_todate =  string(dw_load_ahead_move_orders.GetItemDate(1, "move_date_to"), 'yyyy-mm-dd')
		ls_plant =  dw_load_ahead_move_orders.GetItemString(1, "move_plant")
		li_fillpercent = dw_load_ahead_move_orders.GetItemNumber(1, "fill_percent")
		If IsNull(ls_plant) then
			ls_plant = ""
		end if	
		ls_header = ls_todate + '~t' + ls_plant + '~t'
		
		//SLH SR25459
		Do while li_row <= li_rowcount
			If dw_load_ahead_availability_dtl.GetItemNumber(li_row, "fiell_percent") >= li_fillpercent &
			 and dw_load_ahead_availability_dtl.GetItemString(li_row, "exclude") = 'N' then
				//li_row = dw_load_ahead_availability_dtl.GetRow()
				// 'N' is initial confirmed_ind data value
				ls_order = dw_load_ahead_availability_dtl.GetItemString(li_row, "order_number")
				ls_p_order = dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order")
				if ls_order = ls_p_order Then
					ls_primary_order += ls_order + '~t' + 'N' + '~t'
				End If	
				li_row ++
			else
				li_row ++
			end if
		Loop
	

		ls_update_string = ls_header + '>' + ls_primary_order + '>'
		istr_error_info.se_event_name = "wf_update"
		istr_error_info.se_procedure_name = "nf_pasp20gr"
		istr_error_info.se_message = Space(71)

		If iu_ws_pas5.nf_pasp20gr(istr_error_info, ls_update_string, ls_output) < 0 then
				This.SetRedraw(True)
			Return False
		End if
		
		//SLH SR25459
		if not IsNull(ls_output) then
			
			lds_moves = Create DataStore
			lds_moves.DataObject = 'd_load_ahead_confirm_ds'
			lds_moves.ImportString(ls_output)
			
			li_m_rowcount = lds_moves.RowCount()
			li_row = 1
			li_count = 1
			
			//get load number/primary order and move_status_ind
			//if Move status ind = 'M' then find load# in dw_load_ahead_availability_dtl and set items
			//if Move status ind <> 'M' then append all 3 columns to ls_confirm_string
			//Next
			//move_status_ind: M = Moved, C=Confirm Needed, E=Error
			
			Do While li_row <= li_m_rowcount
				
				ls_load = lds_moves.GetItemString(li_row, "load_number")
				ls_move_status_ind = lds_moves.GetItemString(li_row, "move_status_ind")
				ls_msg = lds_moves.GetItemString(li_row, "message")
				
				If ls_move_status_ind = 'M' Then
					
						li_count = 1
						Do while li_count <= li_rowcount
								ls_temp = dw_load_ahead_availability_dtl.GetItemString(li_count, "primaty_order")
								If ls_temp = ls_load Then
									dw_load_ahead_availability_dtl.SetItem(li_count, "exclude", 'Y')
									dw_load_ahead_availability_dtl.SetItem(li_count, "load_status", 'MOVED')
								End If
							li_count ++
						Loop						
				Else
						ls_confirm_string += ls_load + '~t' + ls_move_status_ind + '~t' + ls_msg + '~t' + 'N' + '~r~n'				
				End If
				li_row ++
			Loop
				
			If len(ls_confirm_string) > 0 Then
				is_confirm_string = ls_confirm_string
				OpenWithParm(w_load_ahead_confirm_resp, This)
			End If
			
			iw_frame.SetMicroHelp("Modifications Successful")
						
		end if
					
	end if
end if
else
	//
end if
This.SetRedraw(True)
return true
end function

public function integer wf_status_convert (integer total_rows);integer I
string ls_val, ls_cred, ls_weight

for I = 1 to total_rows
	ls_val = dw_load_ahead_availability_dtl.GetItemString(I,'load_status')
	ls_cred = dw_load_ahead_availability_dtl.GetItemString(I, 'credit_status')
	ls_weight = dw_load_ahead_availability_dtl.GetItemString(I, 'weight_status')
	choose case ls_val
		case 'F'
			dw_load_ahead_availability_dtl.SetItem(I,'load_status','FULL')
		case 'C'
			dw_load_ahead_availability_dtl.SetItem(I,'load_status','COMBINED')
		case 'U'
			dw_load_ahead_availability_dtl.SetItem(I,'load_status','UNCOMBINED')
		case 'M'
			dw_load_ahead_availability_dtl.SetItem(I,'load_status','MOVED')

	end choose
	
	choose case ls_cred
		case 'S'
			dw_load_ahead_availability_dtl.SetItem(I, 'credit_status', 'SYSTEM APPR')
		case 'P'
			dw_load_ahead_availability_dtl.SetItem(I, 'credit_status', 'PENDING')
		case 'A'
			dw_load_ahead_availability_dtl.SetItem(I, 'credit_status', 'APPROVED')
		case 'O'
			dw_load_ahead_availability_dtl.SetItem(I,'credit_status','PENDING')
		case 'M'
			dw_load_ahead_availability_dtl.SetItem(I,'credit_status','APPROVED')
		case 'C'
			dw_load_ahead_availability_dtl.SetItem(I,'credit_status','REL TO PORT')
		case 'D'
			dw_load_ahead_availability_dtl.SetItem(I,'credit_status','APPR TO REL')
		case 'H'
			dw_load_ahead_availability_dtl.SetItem(I,'credit_status','HOLD')
		case 'L'
			dw_load_ahead_availability_dtl.SetItem(I,'credit_status','APPR TO LOAD')
		case 'R'
			dw_load_ahead_availability_dtl.SetItem(I,'credit_status','REJECT')
	
			
	End choose
	
	choose case ls_weight
		Case 'U'
			dw_load_ahead_availability_dtl.SetItem(I, 'weight_status', 'UNDER')
		case 'O'
			dw_load_ahead_availability_dtl.SetItem(I, 'weight_status', 'OVER')
	End Choose
	
next

return 1

end function

public subroutine wf_load_detail (string as_primary_order, long as_row_number, string as_load_detail_string, ref string as_output);DataStore lds_load_detail

DatawindowChild ldwc_temp
Window lw_temp

Long		ll_detail_row_count, &
			ll_sub, &
			ll_found_row, &
			ll_start_pos, &
			ll_new_row
			
String		ls_find_line_number, &
			ls_find_string, ls_order, ls_output_string
			
		
lds_load_detail = Create DataStore
lds_load_detail.DataObject = 'd_load_ahead_availability_fill_detail'
ll_detail_row_count = lds_load_detail.ImportString(as_load_detail_string)

ls_find_line_number = as_primary_order
//"primary_order = '" 
ls_find_string = "order_number = '" 
ls_find_string += ls_find_line_number + "'"

ll_start_pos = 1


//dw_load_ahead_availability_dtl.GetChild("primaty_order", ldwc_temp)

ll_found_row = lds_load_detail.Find(ls_find_string, ll_start_pos, ll_detail_row_count)

If ll_found_row > 0 then
	Do 
		ls_output_string += lds_load_detail.GetItemString(ll_found_row, "primary_order") 
		ls_output_string += '~t'
		ls_output_string += lds_load_detail.GetItemString(ll_found_row, "order_number")
		ls_output_string += '~t'
		ls_output_string += lds_load_detail.GetItemString(ll_found_row, "line_number")
		ls_output_string += '~t'
		ls_output_string += lds_load_detail.GetItemString(ll_found_row, "product")
		ls_output_string += '~t'
		ls_output_string += lds_load_detail.GetItemString(ll_found_row, "product_state")
		ls_output_string += '~t'
		ls_output_string += string(lds_load_detail.GetItemNumber(ll_found_row, "schedule_qty"))
		ls_output_string += '~t'
		ls_output_string += string(lds_load_detail.GetItemNumber(ll_found_row, "available_qty"))
		ls_output_string += '~t'
		ls_output_string += string(lds_load_detail.GetItemNumber(ll_found_row, "variance"))
		ls_output_string += '~r'
		ll_start_pos = ll_found_row + 1
		ll_found_row = lds_load_detail.Find(ls_find_string, ll_start_pos, ll_detail_row_count)
		
	Loop while ((ll_found_row > 0) and (ll_start_pos <= ll_detail_row_count))
end if

OpenSheetWithParm(lw_temp, ls_output_string, "w_load_ahead_availability_detail_new", iw_frame,0,iw_frame.im_menu.iao_arrangeopen)

end subroutine

public function boolean wf_moveorder ();string ls_todate, ls_plant, ls_header, ls_primary_order, ls_update_string, ls_output, ls_primary, ls_primary2, ls_confirm_string, ls_temp, ls_msg, ls_move_status_ind, ls_load
integer li_row, li_rowcount, li_sel_row, li_fillpercent, li_m_rowcount, li_count
long ll_num_row, ll_row
DataStore lds_moves

li_rowcount = dw_load_ahead_availability_dtl.RowCount()

dw_load_ahead_move_orders.AcceptText()

li_row = dw_load_ahead_availability_dtl.GetRow()

If is_fill_calc = 'N' then
	iw_frame.SetMicroHelp("Fill Percent must be calculated before saving")
else
	If dw_load_ahead_move_orders.GetItemDate(1, "move_date_to") < Today() then
		iw_frame.SetMicroHelp("move Orders To Ship Date is not within the PA Date Range")
	else
	iw_frame.SetMicroHelp("Wait... Updating the Database")
		dw_load_ahead_move_orders.AcceptText()
		ls_todate =  string(dw_load_ahead_move_orders.GetItemDate(1, "move_date_to"), 'yyyy-mm-dd')
		ls_plant =  dw_load_ahead_move_orders.GetItemString(1, "move_plant")
		li_fillpercent = dw_load_ahead_move_orders.GetItemNumber(1, "fill_percent")
		If IsNull(ls_plant) then
			ls_plant = ""
		end if	
		ls_header = ls_todate + '~t' + ls_plant + '~t'
		
		//Only need the primary order
		ls_primary_order = dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order")
		
//		ls_primary_order = dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order")
//		ls_primary = dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order")
//		ll_num_row = li_row
//		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row + 1, "primaty_order")
//		Do while ls_primary = ls_primary2
//			ll_num_row ++
//			ls_primary_order += ls_primary2 + '~t'
//			ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row + 1, "primaty_order")
//		Loop
//				
//		li_row = dw_load_ahead_availability_dtl.GetRow()
//		//ls_primary_order = dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order")
//		ls_primary = dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order")
//		ll_num_row = li_row
//		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row - 1, "primaty_order")
//		Do while ls_primary = ls_primary2
//			ll_num_row --
//			ls_primary_order += ls_primary2 + '~t'
//			ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row - 1, "primaty_order")
//		Loop

		ls_update_string = ls_header + '>' + ls_primary_order + '~t' + 'N' + '~t' +  '>'
		istr_error_info.se_event_name = "wf_update"
		istr_error_info.se_procedure_name = "nf_pasp20gr"
		istr_error_info.se_message = Space(71)

		If iu_ws_pas5.nf_pasp20gr(istr_error_info, ls_update_string, ls_output) < 0 then
				This.SetRedraw(True)
			Return False
		End if
		ll_row = 1
		
		if not IsNull(ls_output) then
	
			//SLH SR25459
			lds_moves = Create DataStore
			lds_moves.DataObject = 'd_load_ahead_confirm_ds'
			lds_moves.ImportString(ls_output)
			
			li_m_rowcount = lds_moves.RowCount()
			li_row = 1
			li_count = 1

			//get load number/primary order and move_status_ind
			//if Move status ind = 'M' then find load# in dw_load_ahead_availability_dtl and set items
			//if Move status ind <> 'M' then append all 3 columns to ls_confirm_string
			//Next
			//move_status_ind: M = Moved, C=Confirm Needed, E=Error			
			
			
			Do While li_row <= li_m_rowcount
				
				ls_load = lds_moves.GetItemString(li_row, "load_number")
				ls_move_status_ind = lds_moves.GetItemString(li_row, "move_status_ind")
				ls_msg = lds_moves.GetItemString(li_row, "message")
				
				If ls_move_status_ind = 'M' Then
					
						li_count = 1
						Do while li_count <= li_rowcount
								ls_temp = dw_load_ahead_availability_dtl.GetItemString(li_count, "primaty_order")
								If ls_temp = ls_load Then
									dw_load_ahead_availability_dtl.SetItem(li_count, "exclude", 'Y')
									dw_load_ahead_availability_dtl.SetItem(li_count, "load_status", 'MOVED')
								End If
							li_count ++
						Loop						
				Else
						ls_confirm_string += ls_load + '~t' + ls_move_status_ind + '~t' + ls_msg + '~t' + 'N' + '~r~n'				
				End If
				li_row ++
			Loop
				
			If len(ls_confirm_string) > 0 Then
				is_confirm_string = ls_confirm_string
				OpenWithParm(w_load_ahead_confirm_resp, This)
			End If
		End If		
			iw_frame.SetMicroHelp("Modifications Successful")										
	end if
end if
This.SetRedraw(True)
return true
end function

public function integer wf_deadlinedates ();long ll_row, ll_rowcount
string ls_las

ll_rowcount = dw_load_ahead_availability_dtl.RowCount()
ll_row = 1
Do while ll_row <= ll_rowcount
	dw_load_ahead_availability_dtl.SetItem(ll_row, "deadline_date", dw_load_ahead_availability_dtl.GetItemDate(ll_row, "deadline_departure"))
	ll_row ++
Loop


ll_row = 2
ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
Do while ll_row <= ll_rowcount
	If dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order") = ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "deadline_date", dw_load_ahead_availability_dtl.GetItemDate(ll_row -1, "deadline_date"))
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		
		ll_row ++
	else
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++
	end if
Loop



dw_load_ahead_availability_dtl.SetSort(is_sort)
dw_load_ahead_availability_dtl.Sort()
dw_load_ahead_availability_dtl.GroupCalc()
return 0

end function

public function integer wf_creditstatus ();long ll_row, ll_rowcount
string ls_las
ll_rowcount = dw_load_ahead_availability_dtl.RowCount()
ll_row = 1
Do while ll_row <= ll_rowcount
	dw_load_ahead_availability_dtl.SetItem(ll_row, "credit", dw_load_ahead_availability_dtl.GetItemString(ll_row, "credit_status"))
	ll_row ++
Loop

ll_row = 2
ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
Do while ll_row <= ll_rowcount
	If dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order") = ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "credit", dw_load_ahead_availability_dtl.GetItemString(ll_row - 1, "credit"))
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++
	else
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++
	end if
Loop

dw_load_ahead_availability_dtl.SetSort(is_sort)
dw_load_ahead_availability_dtl.Sort()
dw_load_ahead_availability_dtl.GroupCalc()

return 0
end function

public subroutine wf_update_load_status (string as_load_number);String ls_temp
Integer li_count, li_rowcount

li_count = 1
li_rowcount = dw_load_ahead_availability_dtl.RowCount()

Do while li_count <= li_rowcount
	ls_temp = dw_load_ahead_availability_dtl.GetItemString(li_count, "primaty_order")
	If ls_temp = as_load_number Then
			dw_load_ahead_availability_dtl.SetItem(li_count, "exclude", 'Y')
			dw_load_ahead_availability_dtl.SetItem(li_count, "load_status", 'MOVED')
	End If
	li_count ++
Loop		

dw_load_ahead_availability_dtl.SetRedraw(True)

end subroutine

on w_load_ahead_availability_new.create
int iCurrent
call super::create
this.cb_5=create cb_5
this.dw_2=create dw_2
this.st_2=create st_2
this.st_1=create st_1
this.pb_2=create pb_2
this.pb_1=create pb_1
this.cb_4=create cb_4
this.cb_3=create cb_3
this.cb_2=create cb_2
this.dw_1=create dw_1
this.dw_load_ahead_availability_dtl=create dw_load_ahead_availability_dtl
this.dw_load_ahead_move_orders=create dw_load_ahead_move_orders
this.dw_load_ahead_order=create dw_load_ahead_order
this.dw_load_status=create dw_load_status
this.dw_load_ahead_hdr1=create dw_load_ahead_hdr1
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_5
this.Control[iCurrent+2]=this.dw_2
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.pb_2
this.Control[iCurrent+6]=this.pb_1
this.Control[iCurrent+7]=this.cb_4
this.Control[iCurrent+8]=this.cb_3
this.Control[iCurrent+9]=this.cb_2
this.Control[iCurrent+10]=this.dw_1
this.Control[iCurrent+11]=this.dw_load_ahead_availability_dtl
this.Control[iCurrent+12]=this.dw_load_ahead_move_orders
this.Control[iCurrent+13]=this.dw_load_ahead_order
this.Control[iCurrent+14]=this.dw_load_status
this.Control[iCurrent+15]=this.dw_load_ahead_hdr1
this.Control[iCurrent+16]=this.cb_1
end on

on w_load_ahead_availability_new.destroy
call super::destroy
destroy(this.cb_5)
destroy(this.dw_2)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.pb_2)
destroy(this.pb_1)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.dw_1)
destroy(this.dw_load_ahead_availability_dtl)
destroy(this.dw_load_ahead_move_orders)
destroy(this.dw_load_ahead_order)
destroy(this.dw_load_status)
destroy(this.dw_load_ahead_hdr1)
destroy(this.cb_1)
end on

event ue_postopen;call super::ue_postopen;String ls_temp, ls_las, ls_text
long ll_row, ll_rowcount, ll_seq
dw_load_ahead_availability_dtl.ib_updateable = False
dw_load_ahead_move_orders.ib_updateable = False
dw_load_ahead_order.ib_updateable = False
dw_load_ahead_hdr1.ib_updateable = False
dw_1.ib_updateable = False
dw_2.ib_updateable = False
dw_load_status.ib_updateable = False

This.SetRedraw(true)

is_option = Message.StringParm

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "aheadavail"
istr_error_info.se_user_id 		= sqlca.userid

Choose case is_option
	Case "Plant"
		is_inquire_window_name = 'w_load_ahead_availability_new_inq'
	Case "Order"
		is_inquire_window_name = 'w_load_ahead_availability_order_new_inq'

End Choose

dw_load_ahead_move_orders.GetChild('move_plant', dwc_temp)
If IsValid(dwc_temp) Then
	dwc_temp.SetTransObject(SQLCA)
	dwc_temp.Retrieve()
	dwc_temp.SetSort("location_code")
End if

dw_load_ahead_move_orders.GetChild('delayed_prod', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PPS TOTL")



iu_ws_pas5 = Create u_ws_pas5

ids_print = Create DataStore
ids_print.DataObject = 'd_load_ahead_print'
dw_load_ahead_availability_dtl.ShareData(ids_print)
wf_retrieve()

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "sort", "")
dw_load_ahead_availability_dtl.SetSort(ls_temp)
dw_load_ahead_availability_dtl.Sort()

ls_temp = ProfileString(iw_frame.is_UserINI, "PAS", "fill_percent", "")
dw_load_ahead_move_orders.SetItem(1, "fill_percent", Integer(ls_temp))
dw_load_ahead_availability_dtl.GroupCalc()

ls_las = ""

ll_row = 1
ll_seq = 100
ll_rowcount = dw_load_ahead_availability_dtl.RowCount()
Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order") <> ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", ll_seq)
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++ 
		ll_seq += 100
	else
		ll_row ++
	end if
	//ls_las = ls_act
Loop

ll_row = 2
ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(ll_row,  "primaty_order") = ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", dw_load_ahead_availability_dtl.GetItemNumber(ll_row -1,"sequence"))
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++ 
		//ll_seq += 100
	else
		
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++
	end if
	//ls_las = ls_act
Loop

dw_load_ahead_move_orders.SetItem(1, "move_plant", "")
end event

event ue_set_data;call super::ue_set_data;String ls_temp, ls_order_num, ls_output, ls_plant
Long ll_row, ll_detail_row_count, ll_case
Integer li_row
DataStore lds_load_detail
Window lw_temp
Date ldt_move
//Case 'Availability Detail'
//	ls_temp = ""
choose Case as_data_item
	Case 'change order'
		ls_temp = dw_load_ahead_availability_dtl.GetItemString(dw_load_ahead_availability_dtl.GetRow(), 'order_number')
		OpenSheetWithParm(lw_temp, ls_temp, "w_change_order_production_dates_new",iw_frame,0,iw_frame.im_menu.iao_arrangeopen) 
	
Case 'availability detail'
		
		If is_fill_calc = 'Y' then
			ls_temp = dw_load_ahead_availability_dtl.GetItemString(dw_load_ahead_availability_dtl.GetRow(), 'order_number')
			ll_row = dw_load_ahead_availability_dtl.GetRow()
			wf_load_detail(ls_temp, ll_row, is_output_string, ls_output)
		else
			
			iw_frame.SetMicroHelp("Fill Percent Must be calculated first")
		end if
		
		
		//OpenSheetWithParm(lw_temp, ls_output, "w_load_ahead_availability_detail_new", iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
		
//		OpenSheetWithParm(lw_temp, ls_temp, "w_load_ahead_availability_detail_new", iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
	Case 'move'
		ldt_move = dw_load_ahead_move_orders.GetItemDate(1, "move_date_to")
		ls_plant =  dw_load_ahead_move_orders.GetItemString(1, "move_plant")
	 	If is_fill_calc = 'N' then
			iw_frame.SetMicroHelp("Fill Percent must be calculated before saving")
		else		
			li_row = dw_load_ahead_availability_dtl.GetRow()
			If dw_load_ahead_availability_dtl.GetItemString(li_row, "exclude") = 'N' Then
					
				If IsNull(ls_plant) or ls_plant = "" then
					ll_case = MessageBox("Confirm move", "1 Orders/Loads will be moved to date" + " " + string(ldt_move), Question!, OkCancel!)
				else 
					ll_case = MessageBox("Confirm move", "Order(s)/Load(s) will be moved to plant " + ls_plant + " on date " + string(ldt_move), Question!, OkCancel!)
				end if
				If ll_case = 1 then 
					wf_moveorder()
				else 
					//
				end if
			end if		
	End If
End Choose

end event

event ue_get_data;call super::ue_get_data;Date ldt_moveto
String ls_temp

Choose Case as_value
	Case 'Plant'
		Message.StringParm = dw_2.GetItemString(1, 'plant')
	Case 'MovePlant'
		ls_temp = dw_load_ahead_move_orders.GetItemString(1,'move_plant')
		If IsNull(ls_temp) Then
			ls_temp = ""
		End If
		Message.StringParm = ls_temp
	Case 'MoveDate'
		ldt_moveto = dw_load_ahead_move_orders.GetItemDate(1, 'move_date_to')
		Message.StringParm =  string(dw_load_ahead_move_orders.GetItemDate(1, "move_date_to"), 'yyyy-mm-dd')
	Case 'ConfirmData'
		Message.Stringparm = is_confirm_string
End choose
end event

event ue_fileprint;call super::ue_fileprint;string ls_data

ids_print.Object.plant_code.text = dw_2.GetItemString(1, "plant")
ids_print.Object.plant_desc.text = dw_2.GetItemString(1, "plant_descriptio")
ids_print.Object.date_text.text = string(dw_load_ahead_hdr1.GetItemDate(1, "ship_date"), "yyyy-mm-dd")
ids_print.Object.move_date.text = string(dw_load_ahead_move_orders.GetItemDate(1, "move_date_to"), "yyyy-mm-dd")
ids_print.Object.delayed_production.text = dw_load_ahead_move_orders.GetItemString(1, "delayed_prod")
ids_print.Object.with_fill.text = string(dw_load_ahead_move_orders.GetItemNumber(1, "fill_percent"))
ls_data = dw_load_ahead_availability_dtl.Describe('Datawindow.Data')
ids_print.ImportString(ls_data)
ids_print.AcceptText()
ids_print.GroupCalc()
ids_print.Print()
ls_data = ""
ids_print.ImportString(ls_data)
//

end event

event close;call super::close;If isValid(iu_ws_pas5) Then
	Destroy(iu_ws_pas5)
End If
end event

type cb_5 from commandbutton within w_load_ahead_availability_new
integer x = 3003
integer y = 416
integer width = 439
integer height = 140
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Sort By Sequence"
end type

event clicked;long ll_row, ll_rowcount
string ls_las

dw_load_ahead_availability_dtl.SetSort("sequence A")
dw_load_ahead_availability_dtl.Sort()
ll_rowcount = dw_load_ahead_availability_dtl.RowCount()
ll_row = 2
ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(ll_row,  "primaty_order") = ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", dw_load_ahead_availability_dtl.GetItemNumber(ll_row -1,"sequence"))
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++ 
		//ll_seq += 100
	else
		
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++
	end if
	//ls_las = ls_act
Loop

cb_5.Visible = False
end event

type dw_2 from u_base_dw_ext within w_load_ahead_availability_new
integer x = 50
integer y = 28
integer width = 1481
integer height = 120
integer taborder = 30
string dataobject = "d_load_plant"
boolean border = false
end type

type st_2 from statictext within w_load_ahead_availability_new
integer x = 2592
integer y = 520
integer width = 169
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Down"
boolean focusrectangle = false
end type

type st_1 from statictext within w_load_ahead_availability_new
integer x = 2592
integer y = 416
integer width = 151
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Up"
boolean focusrectangle = false
end type

type pb_2 from picturebutton within w_load_ahead_availability_new
integer x = 2473
integer y = 504
integer width = 110
integer height = 92
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string picturename = "SPIN2.BMP"
alignment htextalign = left!
end type

event clicked;long ll_sel_row, ll_new_row, ll_num_row, ll_row, ll_rowcount, ll_temp, ll_newsel_row
integer li_num_lines, li_transit_hours, li_credit_status, li_schedule_qty, li_fill_percent, li_seq, li_avail_qty, &
		   li_num_lines2, li_transit_hours2, li_credit_status2, li_schedule_qty2, li_fill_percent2, li_seq2, li_avail_qty2, li_new_seq, ll_seq, li_row
String ls_primary, &
		ls_primary2, &
		ls_primary3, ls_primary4, ls_las
ll_rowcount = dw_load_ahead_availability_dtl.RowCount()		
ll_sel_row = dw_load_ahead_availability_dtl.GetRow()
if dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "primaty_order") = dw_load_ahead_availability_dtl.GetItemString(ll_rowcount, "primaty_order") then
	iw_frame.SetMicroHelp("This row is already at the bottom")
else
	if ll_sel_row = dw_load_ahead_availability_dtl.RowCount() then
		iw_frame.SetMicroHelp("This row is already at the bottom")
	else
		ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "primaty_order")
		ll_num_row = ll_sel_row
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row, "primaty_order")
		ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row - 1, "primaty_order")
		Do while ls_primary2 = ls_primary3
			ll_num_row --
			ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row - 1, "primaty_order")
		Loop

		ll_row =  ll_sel_row
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_row + 1, "primaty_order")
		Do while ls_primary2 = ls_primary3
			ll_row ++
			ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_row + 1, "primaty_order")
		Loop



		ll_new_row = ll_row + 1
		ll_temp = ll_new_row
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_new_row , "primaty_order")
		ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_new_row+ 1, "primaty_order")
		Do while ls_primary2 = ls_primary3
			ll_new_row ++
			ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_new_row + 1, "primaty_order")
		Loop
		If ll_new_row = ll_temp then 
			ll_new_row = ll_temp
		end if


		dw_load_ahead_availability_dtl.RowsMove(ll_num_row, ll_row, Primary!, dw_load_ahead_availability_dtl, ll_new_row + 1, Primary!)

		dw_load_ahead_availability_dtl.SelectRow(0, false)
		ll_newsel_row =  ll_new_row
		dw_load_ahead_availability_dtl.SelectRow(ll_newsel_row, TRUE)
		//dw_load_ahead_availability_dtl.SelectRow(ll_sel_row, FALSE)
		dw_load_ahead_availability_dtl.SetRow(ll_newsel_row)




		ls_las = ""
		ll_rowcount = dw_load_ahead_availability_dtl.RowCount()
		li_row = 1
		ll_seq = 100
		Do while li_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		If dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order") <> ls_las then
			dw_load_ahead_availability_dtl.SetItem(li_row, "sequence", ll_seq)
			ls_las = dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order")
			li_row ++ 
			ll_seq += 100
		
		else
			li_row ++
		end if
	//ls_las = ls_act
		Loop

		ll_row = 2
		ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
		Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		If dw_load_ahead_availability_dtl.GetItemString(ll_row,  "primaty_order") = ls_las then
			dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", dw_load_ahead_availability_dtl.GetItemNumber(ll_row -1,"sequence"))
			ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
			ll_row ++ 
		//ll_seq += 100
		else
		
			ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
			ll_row ++
		end if
	//ls_las = ls_act
		Loop
	end if
end if
//
//
//

end event

type pb_1 from picturebutton within w_load_ahead_availability_new
integer x = 2473
integer y = 396
integer width = 110
integer height = 92
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
boolean originalsize = true
string picturename = "SPIN1.BMP"
alignment htextalign = left!
end type

event clicked;long ll_sel_row, ll_new_row, ll_num_row, ll_row, ll_rowcount, ll_newsel_row
integer li_num_lines, li_transit_hours, li_credit_status, li_schedule_qty, li_fill_percent, li_seq, li_avail_qty, &
		   li_num_lines2, li_transit_hours2, li_credit_status2, li_schedule_qty2, li_fill_percent2, li_seq2, li_avail_qty2, li_new_seq, ll_seq, li_row
String ls_primary, &
		ls_primary2, &
		ls_primary3, ls_primary4, ls_las
		
ll_sel_row = dw_load_ahead_availability_dtl.GetRow()

if dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "primaty_order") = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order") then
	iw_frame.SetMicroHelp("This row is already at the top")
else
if ll_sel_row = 1 then
	iw_frame.SetMicroHelp("This row is already at the top")
else
ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "primaty_order")
ll_num_row = ll_sel_row
ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row, "primaty_order")
ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row - 1, "primaty_order")
Do while ls_primary2 = ls_primary3
	ll_num_row --
	ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row - 1, "primaty_order")
Loop

ll_row =  ll_sel_row
ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_row + 1, "primaty_order")
Do while ls_primary2 = ls_primary3
	ll_row ++
	ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_row + 1, "primaty_order")
Loop



ll_new_row = ll_num_row - 1

ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_new_row, "primaty_order")


ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_new_row - 1, "primaty_order")
Do while ls_primary2 = ls_primary3
	ll_new_row --
	ls_primary3 = dw_load_ahead_availability_dtl.GetItemString(ll_new_row - 1, "primaty_order")
Loop
dw_load_ahead_availability_dtl.RowsMove(ll_num_row, ll_row, Primary!, dw_load_ahead_availability_dtl, ll_new_row, Primary!)

dw_load_ahead_availability_dtl.SelectRow(0, false)
ll_newsel_row =  ll_new_row
dw_load_ahead_availability_dtl.SelectRow(ll_newsel_row, TRUE)
//dw_load_ahead_availability_dtl.SelectRow(ll_sel_row, FALSE)
dw_load_ahead_availability_dtl.SetRow(ll_newsel_row)





ls_las = ""
ll_rowcount = dw_load_ahead_availability_dtl.RowCount()
li_row = 1
ll_seq = 100
Do while li_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order") <> ls_las then
		dw_load_ahead_availability_dtl.SetItem(li_row, "sequence", ll_seq)
		ls_las = dw_load_ahead_availability_dtl.GetItemString(li_row, "primaty_order")
		li_row ++ 
		ll_seq += 100
		
	else
		li_row ++
	end if
	//ls_las = ls_act
Loop

ll_row = 2
ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(ll_row,  "primaty_order") = ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", dw_load_ahead_availability_dtl.GetItemNumber(ll_row -1,"sequence"))
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++ 
		//ll_seq += 100
	else
		
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++
	end if
	//ls_las = ls_act
Loop
iw_frame.SetMicroHelp("Ready")
end if
end if
//
//
//

end event

type cb_4 from commandbutton within w_load_ahead_availability_new
integer x = 2510
integer y = 32
integer width = 343
integer height = 188
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Sort"
end type

event clicked;string ls_sort_values, ls_returned_parms, ls_las
long ll_rowcount, ll_row, ll_seq, ll_temp
openwithparm(w_load_ahead_availability_sort_new, ls_sort_values)
ls_returned_parms = Message.StringParm
dw_load_ahead_availability_dtl.SetSort(ls_returned_parms)
dw_load_ahead_availability_dtl.Sort()


ll_row = 1
ll_seq = 100
ll_rowcount = dw_load_ahead_availability_dtl.RowCount()
//Do while ll_row <= ll_rowcount
//	dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", ll_seq)
//	ll_row ++ 
//	ll_seq += 100
//Loop






SetProfileString(iw_frame.is_UserINI, "PAS", "sort", ls_returned_parms)
dw_load_ahead_availability_dtl.GroupCalc()
ls_las = ""

Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order") <> ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", ll_seq)
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++ 
		ll_seq += 100
	else
		ll_row ++
	end if
	//ls_las = ls_act
Loop

ll_row = 2
ls_las = dw_load_ahead_availability_dtl.GetItemString(1, "primaty_order")
Do while ll_row <= ll_rowcount
	//ls_act = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	If dw_load_ahead_availability_dtl.GetItemString(ll_row,  "primaty_order") = ls_las then
		dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", dw_load_ahead_availability_dtl.GetItemNumber(ll_row -1,"sequence"))
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++ 
		//ll_seq += 100
	else
		
		ls_las = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ll_row ++
	end if
	//ls_las = ls_act
Loop

end event

type cb_3 from commandbutton within w_load_ahead_availability_new
boolean visible = false
integer x = 2386
integer y = 504
integer width = 169
integer height = 92
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Down"
end type

event clicked;long ll_sel_row
integer li_num_lines, li_transit_hours, li_credit_status, li_schedule_qty, li_fill_percent, li_seq, li_avail_qty, &
		   li_num_lines2, li_transit_hours2, li_credit_status2, li_schedule_qty2, li_fill_percent2, li_seq2, li_avail_qty2, li_new_seq
String ls_primary, &
		ls_load_status, &
		ls_weight_status, ls_multiplant, ls_departure_date, ls_order_num, ls_credit_status, &
		ls_deadline_departure, ls_ship_plant, ls_customer_name, ls_city, ls_state, ls_exclude, ls_order_status, &
		ls_primary2, &
		ls_load_status2, &
		ls_weight_status2, ls_multiplant2, ls_departure_date2, ls_order_num2, ls_credit_status2, &
		ls_deadline_departure2, ls_ship_plant2, ls_customer_name2, ls_city2, ls_state2, ls_exclude2, ls_order_status2
		
ll_sel_row = dw_load_ahead_availability_dtl.GetRow()
if ll_sel_row = dw_load_ahead_availability_dtl.RowCount() then
	iw_frame.SetMicroHelp("This row is already at the bottom")
else
ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "primaty_order")
ls_load_status = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row,  "load_status")
li_num_lines = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "num_lines")
li_transit_hours = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "transit_hours")
ls_weight_status = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "weight_status")
ls_multiplant = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "multi_plant")
li_credit_status = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "creditstatus_weight")
ls_departure_date = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "departureweight_date")
ls_order_num = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "order_number")
ls_credit_status = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "credit_status")
ls_customer_name = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "customer_name")
li_schedule_qty = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "schedule_qty")
ls_deadline_departure = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "deadline_departure")
ls_ship_plant = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "ship_plant")
ls_city = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "city")
ls_state = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "state")
ls_exclude = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "exclude")
ls_order_status = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "order_status")
li_fill_percent = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "fiell_percent")
li_seq = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "sequence")
li_avail_qty =  dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "available_qty")


ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "primaty_order")
ls_load_status2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1,  "load_status")
li_num_lines2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row + 1, "num_lines")
li_transit_hours2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row + 1, "transit_hours")
ls_weight_status2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "weight_status")
ls_multiplant2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "multi_plant")
li_credit_status2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row + 1, "creditstatus_weight")
ls_departure_date2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "departureweight_date")
ls_order_num2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "order_number")
ls_credit_status2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "credit_status")
ls_customer_name2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "customer_name")
li_schedule_qty2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row + 1, "schedule_qty")
ls_deadline_departure2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "deadline_departure")
ls_ship_plant2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "ship_plant")
ls_city2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "city")
ls_state2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "state")
ls_exclude2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "exclude")
ls_order_status2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row + 1, "order_status")
li_fill_percent2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row + 1, "fiell_percent")
li_seq2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row + 1, "sequence")
li_avail_qty2 =  dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row + 1, "available_qty")

li_new_seq = li_seq2 + 5

dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "primaty_order", ls_primary)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "load_status", ls_load_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "num_lines", li_num_lines)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "transit_hours", li_transit_hours)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "weight_status", ls_weight_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "multi_plant", ls_multiplant)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "creditstatus_weight", li_credit_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "departureweight_date", ls_departure_date)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "order_number", ls_order_num)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "credit_status", ls_credit_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "customer_name", ls_customer_name)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "schedule_qty", li_schedule_qty)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "deadline_departure", ls_deadline_departure)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "ship_plant", ls_ship_plant)
dw_load_ahead_availability_dtl.setItem(ll_sel_row + 1, "city", ls_city)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "state", ls_state)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "exclude", ls_exclude)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "order_status", ls_order_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "fiell_percent", li_fill_percent)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "sequence", li_new_seq)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row + 1, "available_qty", li_avail_qty)

dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "primaty_order", ls_primary2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "load_status", ls_load_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "num_lines", li_num_lines2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "transit_hours", li_transit_hours2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "weight_status", ls_weight_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "multi_plant", ls_multiplant2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "creditstatus_weight", li_credit_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "departureweight_date", ls_departure_date2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "order_number", ls_order_num2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "credit_status", ls_credit_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "customer_name", ls_customer_name2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "schedule_qty", li_schedule_qty2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "deadline_departure", ls_deadline_departure2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "ship_plant", ls_ship_plant2)
dw_load_ahead_availability_dtl.setItem(ll_sel_row, "city", ls_city2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "state", ls_state2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "exclude", ls_exclude2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "order_status", ls_order_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "fiell_percent", li_fill_percent2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "sequence", li_seq2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "available_qty", li_avail_qty2)







end if


end event

type cb_2 from commandbutton within w_load_ahead_availability_new
boolean visible = false
integer x = 2382
integer y = 396
integer width = 174
integer height = 92
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Up"
end type

event clicked;long ll_sel_row
integer li_num_lines, li_transit_hours, li_credit_status, li_schedule_qty, li_fill_percent, li_seq, li_avail_qty, &
		   li_num_lines2, li_transit_hours2, li_credit_status2, li_schedule_qty2, li_fill_percent2, li_seq2, li_avail_qty2, li_new_seq
String ls_primary, &
		ls_load_status, &
		ls_weight_status, ls_multiplant, ls_departure_date, ls_order_num, ls_credit_status, &
		ls_deadline_departure, ls_ship_plant, ls_customer_name, ls_city, ls_state, ls_exclude, ls_order_status, &
		ls_primary2, &
		ls_load_status2, &
		ls_weight_status2, ls_multiplant2, ls_departure_date2, ls_order_num2, ls_credit_status2, &
		ls_deadline_departure2, ls_ship_plant2, ls_customer_name2, ls_city2, ls_state2, ls_exclude2, ls_order_status2
		
ll_sel_row = dw_load_ahead_availability_dtl.GetRow()
if ll_sel_row = 1 then
	iw_frame.SetMicroHelp("This row is already at the top")
else
ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "primaty_order")
ls_load_status = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row,  "load_status")
li_num_lines = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "num_lines")
li_transit_hours = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "transit_hours")
ls_weight_status = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "weight_status")
ls_multiplant = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "multi_plant")
li_credit_status = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "creditstatus_weight")
ls_departure_date = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "departureweight_date")
ls_order_num = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "order_number")
ls_credit_status = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "credit_status")
ls_customer_name = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "customer_name")
li_schedule_qty = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "schedule_qty")
ls_deadline_departure = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "deadline_departure")
ls_ship_plant = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "ship_plant")
ls_city = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "city")
ls_state = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "state")
ls_exclude = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "exclude")
ls_order_status = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row, "order_status")
li_fill_percent = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "fiell_percent")
li_seq = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "sequence")
li_avail_qty =  dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row, "available_qty")


ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "primaty_order")
ls_load_status2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1,  "load_status")
li_num_lines2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row - 1, "num_lines")
li_transit_hours2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row - 1, "transit_hours")
ls_weight_status2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "weight_status")
ls_multiplant2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "multi_plant")
li_credit_status2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row - 1, "creditstatus_weight")
ls_departure_date2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "departureweight_date")
ls_order_num2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "order_number")
ls_credit_status2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "credit_status")
ls_customer_name2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "customer_name")
li_schedule_qty2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row - 1, "schedule_qty")
ls_deadline_departure2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "deadline_departure")
ls_ship_plant2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "ship_plant")
ls_city2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "city")
ls_state2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "state")
ls_exclude2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "exclude")
ls_order_status2 = dw_load_ahead_availability_dtl.GetItemString(ll_sel_row - 1, "order_status")
li_fill_percent2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row - 1, "fiell_percent")
li_seq2 = dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row - 1, "sequence")
li_avail_qty2 =  dw_load_ahead_availability_dtl.GetItemNumber(ll_sel_row - 1, "available_qty")


li_new_seq = li_seq2 - 5






dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "primaty_order", ls_primary)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "load_status", ls_load_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "num_lines", li_num_lines)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "transit_hours", li_transit_hours)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "weight_status", ls_weight_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "multi_plant", ls_multiplant)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "creditstatus_weight", li_credit_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "departureweight_date", ls_departure_date)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "order_number", ls_order_num)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "credit_status", ls_credit_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "customer_name", ls_customer_name)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "schedule_qty", li_schedule_qty)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "deadline_departure", ls_deadline_departure)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "ship_plant", ls_ship_plant)
dw_load_ahead_availability_dtl.setItem(ll_sel_row - 1, "city", ls_city)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "state", ls_state)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "exclude", ls_exclude)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "order_status", ls_order_status)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "fiell_percent", li_fill_percent)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "sequence", li_new_seq)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row - 1, "available_qty", li_avail_qty)

dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "primaty_order", ls_primary2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "load_status", ls_load_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "num_lines", li_num_lines2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "transit_hours", li_transit_hours2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "weight_status", ls_weight_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "multi_plant", ls_multiplant2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "creditstatus_weight", li_credit_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "departureweight_date", ls_departure_date2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "order_number", ls_order_num2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "credit_status", ls_credit_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "customer_name", ls_customer_name2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "schedule_qty", li_schedule_qty2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "deadline_departure", ls_deadline_departure2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "ship_plant", ls_ship_plant2)
dw_load_ahead_availability_dtl.setItem(ll_sel_row, "city", ls_city2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "state", ls_state2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "exclude", ls_exclude2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "order_status", ls_order_status2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "fiell_percent", li_fill_percent2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "sequence", li_seq2)
dw_load_ahead_availability_dtl.SetItem(ll_sel_row, "available_qty", li_avail_qty2)







end if




end event

type dw_1 from u_plant within w_load_ahead_availability_new
integer x = 50
integer y = 48
integer taborder = 30
boolean enabled = false
string dataobject = "d_pas_plant_local"
end type

type dw_load_ahead_availability_dtl from u_base_dw_ext within w_load_ahead_availability_new
integer x = 27
integer y = 588
integer width = 3566
integer height = 1380
integer taborder = 40
string dataobject = "d_load_ahead_availability_dtl"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event clicked;call super::clicked;//INTEGER li_row
////li_row = 1
//li_row = dw_load_ahead_availability_dtl.GetSelectedRow(0)
//This.SelectRow(li_row, true)


String	ls_temp
Boolean 	lb_color

IF row > 0 Then
	If il_last_clicked_row = 0 and row <> 1 Then
		il_last_clicked_row = This.GetSelectedRow(0)
		If il_last_clicked_row = 0 Then
			il_last_clicked_row = 1
		End If
	End If
	
	If row <> il_last_clicked_row Then 
		SelectRow(0, false)
		SelectRow(row, TRUE)
		//SelectRow(il_last_clicked_row,FALSE)
	End If
	
	This.SetRow(Row)
End If
il_ChangedRow = row
il_last_clicked_row = row
//
//




end event

event rbuttondown;call super::rbuttondown;If message.nf_get_app_id() <> 'PAS' then return
m_load_ahead_availability_popup lm_popup

lm_popup = Create m_load_ahead_availability_popup
if is_fill_calc = 'N' then
	lm_popup.m_LoadAhead.m_0.disable()
	lm_popup.m_LoadAhead.m_1.disable()
else
	lm_popup.m_LoadAhead.m_0.Enable()
	lm_popup.m_LoadAhead.m_1.Enable()
end if
lm_popup.m_LoadAhead.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup
end event

event itemchanged;call super::itemchanged;long ll_row, ll_num_row
string ls_primary, ls_primary2, ls_temp, ls_columnname
dw_load_ahead_availability_dtl.AcceptText()
ll_row = dw_load_ahead_availability_dtl.GetRow()
ll_num_row =  ll_row + 1 
If data = 'Y' then 
	ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row, "primaty_order")
	Do while ls_primary = ls_primary2
		dw_load_ahead_availability_dtl.SetItem(ll_num_row, "exclude", 'Y')
		//hacer invisible el checkbox
		ll_num_row ++
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row, "primaty_order")
	Loop
	ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_row - 1, "primaty_order")
	Do while ls_primary = ls_primary2
		dw_load_ahead_availability_dtl.SetItem(ll_row, "exclude", 'Y')
		//hacer invisible el checkbox
		ll_row --
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
	Loop
else 
	If data = 'N' then 
		ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row, "primaty_order")
		Do while ls_primary = ls_primary2
			dw_load_ahead_availability_dtl.SetItem(ll_num_row, "exclude", 'N')
			//hacer invisible el checkbox
			ll_num_row ++
			ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row, "primaty_order")
		Loop
		ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_row - 1, "primaty_order")
		Do while ls_primary = ls_primary2
			dw_load_ahead_availability_dtl.SetItem(ll_row, "exclude", 'N')
			//hacer invisible el checkbox
			ll_row --
			ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		Loop
	end if
end if

ls_columnname = dwo.name
Choose Case ls_columnname
	case 'sequence'
		ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row, "primaty_order")
		Do while ls_primary = ls_primary2
			dw_load_ahead_availability_dtl.SetItem(ll_num_row, "sequence", dw_load_ahead_availability_dtl.GetItemNumber(ll_row, "sequence"))
			ll_num_row ++
			ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_num_row, "primaty_order")
		Loop
		ls_primary = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_row - 1, "primaty_order")
		Do while ls_primary = ls_primary2
			dw_load_ahead_availability_dtl.SetItem(ll_row, "sequence", dw_load_ahead_availability_dtl.GetItemNumber(ll_row + 1, "sequence"))
			//hacer invisible el checkbox
			ll_row --
			ls_primary2 = dw_load_ahead_availability_dtl.GetItemString(ll_row, "primaty_order")
		Loop
		cb_5.Visible = true
End Choose
	
		
end event

type dw_load_ahead_move_orders from u_base_dw_ext within w_load_ahead_availability_new
integer x = 18
integer y = 384
integer width = 2400
integer height = 236
integer taborder = 30
string dataobject = "d_load_ahead_move_orders"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)


end event

event itemchanged;call super::itemchanged;string ls_temp, ls_columnname
long ll_row, ll_rowcount

dw_load_ahead_move_orders.AcceptText()
ls_temp = string(dw_load_ahead_move_orders.GetItemNumber(1, "fill_percent"))
SetProfileString(iw_frame.is_UserINI, "PAS", "fill_percent", ls_temp)
ll_rowcount = dw_load_ahead_availability_dtl.RowCount()
ll_row = 1


ls_columnname = dwo.name
Choose Case ls_columnname
	case 'move_date_to' 
		Do while ll_row <= ll_rowcount
			dw_load_ahead_availability_dtl.SetItem(ll_row, "fiell_percent", 0)
			dw_load_ahead_availability_dtl.SetItem(ll_row, "available_qty", 0)
			ll_row ++
		Loop
		
	case  'move_plant'
		Do while ll_row <= ll_rowcount
			dw_load_ahead_availability_dtl.SetItem(ll_row, "fiell_percent", 0)
			dw_load_ahead_availability_dtl.SetItem(ll_row, "available_qty", 0)
			ll_row ++
		Loop
		
	case 'delayed_prod'
		Do while ll_row <= ll_rowcount
			dw_load_ahead_availability_dtl.SetItem(ll_row, "fiell_percent", 0)
			dw_load_ahead_availability_dtl.SetItem(ll_row, "available_qty", 0)
			ll_row ++
		Loop
		is_fill_calc = 'N'
End choose
	
end event

type dw_load_ahead_order from u_base_dw_ext within w_load_ahead_availability_new
integer x = 800
integer y = 160
integer width = 745
integer height = 140
integer taborder = 20
boolean enabled = false
string dataobject = "d_load_ahead_order_num"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_load_status from u_base_dw_ext within w_load_ahead_availability_new
integer x = 1554
integer y = 8
integer width = 526
integer height = 360
integer taborder = 20
string dataobject = "d_load_status_new"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_load_ahead_hdr1 from u_base_dw_ext within w_load_ahead_availability_new
integer x = 46
integer y = 152
integer width = 736
integer height = 148
integer taborder = 10
boolean enabled = false
string dataobject = "d_load_ahead_availability_date"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type cb_1 from commandbutton within w_load_ahead_availability_new
event ue_opendetail ( )
integer x = 2907
integer y = 32
integer width = 361
integer height = 188
integer taborder = 40
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Calculate Fill %"
end type

event clicked;String ls_to_date, &
		ls_from_date, &
		ls_to_plant, &
		ls_from_plant, &
		ls_delayedprod, &
		ls_primary_order, &
		ls_order_number, &
		ls_input_string, &
		ls_order_string, &
		ls_output_string, ls_primary_ord, ls_primary_detail
integer li_rowcount, li_count, li_sched_qty, li_avail_qty, li_qty, li_fill, li_counter, li_num, li_entry
long 	ll_total_count, ll_count, ll_num, ll_entry
Window lw_temp

DataStore  lds_detail
lds_detail = Create DataStore
lds_detail.DataObject = 'd_load_ahead_availability_fill_detail'
//lds_detail.DataObject = 'd_load_ahead_availability_fill'


li_rowcount = dw_load_ahead_availability_dtl.RowCount()
ll_count = 1
Do while ll_count <= li_rowcount
	dw_load_ahead_availability_dtl.SetItem(ll_count, "fiell_percent", 0)
	dw_load_ahead_availability_dtl.SetItem(ll_count, "available_qty", 0)
	ll_count ++
Loop
	
SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")
This.SetRedraw(False)
istr_error_info.se_event_name = "button_clicked"
istr_error_info.se_procedure_name = "nf_pasp19gr"
istr_error_info.se_message = Space(71)

li_num = 1
ll_num = 1
dw_load_ahead_move_orders.AcceptText()	
If dw_load_ahead_move_orders.GetItemDate(1, "move_date_to") < Today() then
	iw_frame.SetMicroHelp("Move Orders To Ship Date is not within the PA Date Range")
else
	
	Do while li_num <= li_rowcount
		If dw_load_ahead_availability_dtl.GetItemString(li_num, "exclude") = 'Y' then 
			li_num ++
			li_entry ++
			if li_entry = li_rowcount then
				iw_frame.SetMicroHelp("No Loads were available for Calculate Fill Percent")
			end if
				
		else
			
			ls_to_date = string(dw_load_ahead_move_orders.GetItemDate(1, "move_date_to"), 'yyyy-mm-dd')
			ls_from_date = string(dw_load_ahead_hdr1.GetItemDate(1, "ship_date"), 'yyyy-mm-dd')
			ls_to_plant = dw_load_ahead_move_orders.GetItemString(1, "move_plant")
			ls_from_plant = dw_2.GetItemString(1, "plant")
			ls_delayedprod = dw_load_ahead_move_orders.GetItemString(1, "delayed_prod")
		
			If IsNull(ls_to_plant) then
				ls_to_plant = ""
			end if
		
			ls_primary_order = dw_load_ahead_availability_dtl.GetItemString(li_num, "primaty_order")
			ls_order_number = dw_load_ahead_availability_dtl.GetItemString(li_num, "order_number")
			ls_order_string += ls_primary_order + '~t' + ls_order_number + '~t'
			li_num ++
		end if
	Loop
	


	ls_input_string = ls_to_date + '~t' + ls_from_date + '~t' + ls_to_plant + '~t' + ls_from_plant + '~t' + ls_delayedprod
	If ls_order_string= "" then
			li_num = 1
			li_entry = 1
			Do while li_num <= li_rowcount
				If dw_load_ahead_availability_dtl.GetItemString(li_num, "exclude") = 'Y' then 
					li_num ++
					li_entry ++
					if li_entry = li_rowcount then
						is_fill_calc = 'N'
						iw_frame.SetMicroHelp("No Loads were available for Calculate Fill Percent")			
//						li_num ++
//					li_entry ++
					end if
				else
					li_num ++
					li_entry ++
				end if
			loop
		else

	If iu_ws_pas5.nf_pasp19gr(istr_error_info, ls_input_string, ls_output_string, ls_order_string) < 0 then
		This.SetRedraw(True)
	End if

	ll_total_count = lds_detail.ImportString(ls_output_string)
	is_output_string = ls_output_string
	li_rowcount = dw_load_ahead_availability_dtl.rowcount()
	li_count = 1
	li_counter = 1
	

	Do while li_counter <= li_rowcount
		If dw_load_ahead_availability_dtl.GetItemString(li_counter, "exclude") = 'Y' then
			li_counter ++ 
		else
			Do while li_count <= ll_total_count
				If dw_load_ahead_availability_dtl.GetItemString(li_counter, "order_number") =  lds_detail.GetItemString(li_count, "order_number") and dw_load_ahead_availability_dtl.GetItemString(li_counter, "primaty_order") =  lds_detail.GetItemString(li_count, "primary_order") then 
					li_sched_qty += lds_detail.GetItemNumber(li_count, "schedule_qty")
					li_avail_qty += lds_detail.GetItemNumber(li_count, "available_qty")
					if li_sched_qty = 0 then
						li_fill = 0
					else
						li_fill = (li_avail_qty/li_sched_qty) *100
					end if
					dw_load_ahead_availability_dtl.SetItem(li_counter, "available_qty", li_avail_qty)
					dw_load_ahead_availability_dtl.SetItem(li_counter, "fiell_percent", li_fill)
				end if
				li_count ++
			Loop
		li_sched_qty = 0
		li_avail_qty = 0
		li_count = 1	
		li_counter++
		end if
	Loop
	
	
	li_count = 1
	li_counter = 1
	Do while li_counter <= li_rowcount
		If dw_load_ahead_availability_dtl.GetItemString(li_counter, "exclude") = 'Y' then
			li_counter ++ 
		else
			Do while li_count <= ll_total_count
				If  dw_load_ahead_availability_dtl.GetItemString(li_counter, "primaty_order") =  lds_detail.GetItemString(li_count, "primary_order") then 
					li_sched_qty += lds_detail.GetItemNumber(li_count, "schedule_qty")
					li_avail_qty += lds_detail.GetItemNumber(li_count, "available_qty")
					if li_sched_qty = 0 then
						li_fill = 0
					else
						li_fill = (li_avail_qty/li_sched_qty) *100
					end if
					dw_load_ahead_availability_dtl.SetItem(li_counter, "fiell_percent", li_fill)
				end if
				li_count ++
			Loop
		li_sched_qty = 0
		li_avail_qty = 0
		li_count = 1	
		li_counter++
		end if
	Loop
			
	

		
	is_fill_calc = 'Y'
	iw_frame.SetMicroHelp("Fill Percent Calculation Succesful")
	
	
end if

end if
	

			
	
	
	
	
	
	
	
	

	
	This.SetRedraw(True)
	








end event

