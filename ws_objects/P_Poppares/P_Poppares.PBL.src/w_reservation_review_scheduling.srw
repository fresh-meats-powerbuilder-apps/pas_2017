﻿$PBExportHeader$w_reservation_review_scheduling.srw
$PBExportComments$Reservation Review Window for Scheduling
forward
global type w_reservation_review_scheduling from w_netwise_sheet
end type
type cb_next from commandbutton within w_reservation_review_scheduling
end type
type cb_prev from commandbutton within w_reservation_review_scheduling
end type
type tab_1 from tab within w_reservation_review_scheduling
end type
type tabpage_1 from u_reservation_review_tab within tab_1
end type
type tabpage_1 from u_reservation_review_tab within tab_1
end type
type tabpage_2 from u_reservation_review_tab within tab_1
end type
type tabpage_2 from u_reservation_review_tab within tab_1
end type
type tabpage_3 from u_reservation_review_tab within tab_1
end type
type tabpage_3 from u_reservation_review_tab within tab_1
end type
type tabpage_4 from u_reservation_review_tab within tab_1
end type
type tabpage_4 from u_reservation_review_tab within tab_1
end type
type tabpage_5 from u_reservation_review_tab within tab_1
end type
type tabpage_5 from u_reservation_review_tab within tab_1
end type
type tab_1 from tab within w_reservation_review_scheduling
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type
end forward

global type w_reservation_review_scheduling from w_netwise_sheet
integer width = 2894
integer height = 1636
string title = "Reservation Review"
long backcolor = 12632256
event ue_complete_order ( )
event ue_cascadeplant ( )
cb_next cb_next
cb_prev cb_prev
tab_1 tab_1
end type
global w_reservation_review_scheduling w_reservation_review_scheduling

type variables
DataStore	ids_res_list
DataStore	ids_customer
DataStore	ids_salespeople
DataStore	ids_servicecenters
String		is_inquireData,&
		is_movement
u_orp001		iu_orp001
u_orp002		iu_orp002
u_pas201		iu_pas201

s_error		istr_error_info

u_reservation_review_tab	iu_tabs[]
Integer		ii_start_res
Boolean		ib_no_inquire
u_ws_orp3  iu_ws_orp3
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_select_tab (integer ai_tab)
public function boolean wf_update ()
public subroutine wf_remove_reservation ()
public subroutine wf_show_reservations (integer ai_start_res)
end prototypes

event ue_complete_order;call super::ue_complete_order;If IsValid(iu_tabs[tab_1.SelectedTab]) Then 
	iu_tabs[tab_1.SelectedTab].uf_completeorder()
End If
end event

event ue_cascadeplant;call super::ue_cascadeplant;iu_tabs[tab_1.SelectedTab].uf_cascadeplants()
end event

public function boolean wf_retrieve ();Long	ll_result, &
		ll_row_count

String	ls_startres, &
			ls_detail_info, &
			ls_option, &
			ls_tab_title, &
			ls_customers, &
			ls_division, &
			ls_inquire_data, &
			ls_gpo_date, &
			ls_movement

Integer	li_counter

ll_result = w_base_sheet::EVENT CloseQuery( )
If ll_result > 0 then return false

If Not ib_no_inquire Then
	OpenWithParm(w_reservation_review_inq, This)
	is_InquireData = Message.StringParm
Else
	ib_no_inquire = False
End If
/////
//OpenWithParm(w_reservation_review_inq, This)
//is_InquireData = Message.StringParm

If iw_frame.iu_string.nf_IsEmpty(is_InquireData) Then
	return False
End if

SetPointer(HourGlass!)
This.SetRedraw(False)
istr_error_info.se_event_name = 'wf_retrieve'

// is_InquireData is:
// reservation number + gpo_date + "S" + division
ls_inquire_data = is_InquireData
ls_StartRes = iw_frame.iu_string.nf_gettoken(ls_inquire_data,'~t')
ls_gpo_date = iw_frame.iu_string.nf_gettoken(ls_inquire_data,'~t')
ls_option = iw_frame.iu_string.nf_gettoken(ls_inquire_data,'~t')		// Should be "S"
ls_division = iw_frame.iu_string.nf_gettoken(ls_inquire_data,'~t')
If Not iw_frame.iu_string.nf_IsEmpty(ls_StartRes) Then
	// If they entered a reservation, only inquire on that
	ls_detail_info = ls_StartRes 
Else
	ls_detail_info = ''
End If
ls_detail_info += '~t~t~t~t'

If This.Title = "Reservation Movement" then
	ls_movement = 'Y'
else
	ls_movement = ' '
end if	

ls_detail_info += '~t' + ls_division + '~t' + String(Date(ls_gpo_date),"mm/dd/yyyy") + '~t' + ls_movement

//iu_orp001.nf_orpo62ar(istr_error_info, &
//								ls_detail_info, &
//								ls_option)
iu_ws_orp3.uf_orpo62fr(istr_error_info, &
								ls_detail_info, &
								ls_option)

ids_res_list.Reset()
ll_row_count = ids_res_list.ImportString(ls_detail_info)

ids_res_list.SetRow(1)

// Scheduling is using this, they don't have the custom drop down filled in
ls_Customers = ''
For li_Counter = 1  to ids_res_list.RowCount()
	ls_Customers += ids_res_list.GetItemString(li_Counter, 'customer_id') + '~t' + &
						ids_res_list.GetItemString(li_Counter, 'customer_name') + '~t' + &
						ids_res_list.GetItemString(li_Counter, 'city') + '~t' + &
						ids_res_list.GetItemString(li_Counter, 'state') + '~r~n'
Next
	
ids_customer.Reset()
Long ll_num_rows
ll_num_rows = ids_customer.ImportString(ls_Customers)

String ls_temp
ls_temp = ids_customer.Describe("DataWindow.Data")

If ll_num_rows > UpperBound(iu_tabs) Then
	cb_next.Visible = True
End If

For li_counter = 1 to UpperBound(iu_tabs)
	iu_tabs[li_counter].Visible = False
Next
wf_show_reservations(1)

This.SetRedraw(True)
If ids_res_list.RowCount() > 0 Then 
	This.POST wf_select_tab(1)
Else
	MessageBox("Reservation Review", "There are no reservations to display. Reinquire " + &
		"to check the scheduling queue again.", Information!)
End If

return True

end function

public subroutine wf_select_tab (integer ai_tab);If ai_tab = tab_1.SelectedTab Then
	tab_1.SelectTab(ai_tab)
	iu_tabs[ai_tab].uf_selected()
Else
	tab_1.SelectTab(ai_tab)
End If
end subroutine

public function boolean wf_update ();If IsValid(iu_tabs[tab_1.SelectedTab]) Then return iu_tabs[tab_1.SelectedTab].uf_update()

Return TRUE
end function

public subroutine wf_remove_reservation ();Integer	li_selected, &
			li_counter, &
			li_upperbound
			
String	ls_tab_title

//Remove the accepted order from the queue
li_selected = tab_1.SelectedTab
li_upperbound = UpperBound(iu_tabs)
ids_res_list.DeleteRow(li_selected + ii_start_res - 1)
If ids_res_list.RowCount() = 0 Then
	iu_tabs[li_selected].Visible = False
	MessageBox("Reservation Review","There are no more reservations to display. Reinquire " + &
		"to check the scheduling queue again.", Information!)
	return
End If
wf_show_reservations(ii_start_res)

//Now select the next visible tab
Boolean lb_done = FALSE
li_counter = li_selected
Do While li_counter <= li_upperbound And Not lb_done
	If iu_tabs[li_counter].Visible Then
		iu_tabs[li_counter].ib_retrieved = FALSE
		tab_1.SelectTab(li_counter)
		iu_tabs[li_counter].uf_selected()
		lb_done = TRUE
	End If
	li_counter ++
Loop
li_counter = 1
Do While li_counter < li_selected And Not lb_done
	If iu_tabs[li_counter].Visible Then
		iu_tabs[li_counter].ib_retrieved = FALSE
		tab_1.SelectTab(li_counter)
		iu_tabs[li_counter].uf_selected()
		lb_done = TRUE
	End If
	li_counter ++
Loop
If Not lb_done Then
	MessageBox("Reservation Review","There are no more reservations to display. Reinquire " + &
		"to check the scheduling queue again.", Information!)
	return	
End If
return

end subroutine

public subroutine wf_show_reservations (integer ai_start_res);Integer	li_counter
String	ls_tab_title
If ai_start_res + UpperBound(iu_tabs) > ids_res_list.RowCount() Then
	ai_start_res = ids_res_list.RowCount() - UpperBound(iu_tabs) + 1
End If
If ai_start_res < 1 Then ai_start_res = 1
ii_start_res = ai_start_res
For li_counter = 1 to UpperBound(iu_tabs)
	iu_tabs[li_counter].Visible = FALSE
Next
For li_counter = ai_start_res to ids_res_list.RowCount()
	If li_counter - ai_start_res + 1 <= UpperBound(iu_tabs) Then
		iu_tabs[li_counter - ai_start_res + 1].Visible = TRUE	
		iu_tabs[li_counter - ai_start_res + 1].ib_retrieved = FALSE
		iu_tabs[li_counter - ai_start_res + 1].Text = ids_res_list.GetItemString(li_counter,"order_id") + &
			"~r~n" + String(ids_res_list.GetItemDate(li_counter,"gpo_date"),"mm/yyyy")
		iu_tabs[li_counter - ai_start_res + 1].id_gpo_date = ids_res_list.GetItemDate(li_counter,"gpo_date")
		ls_tab_title = Trim(ids_res_list.GetItemString(li_counter,"customer_name"))
		If Not iw_frame.iu_string.nf_IsEmpty(ls_tab_title) Then
			iu_tabs[li_counter - ai_start_res + 1].PowerTipText = ls_tab_title
		Else
			iu_tabs[li_counter - ai_start_res + 1].PowerTipText = Trim(ids_res_list.GetItemString(li_counter,"customer_id"))
		End If
		iu_tabs[li_counter - ai_start_res + 1].is_order_status = ids_res_list.GetItemString(li_counter,"order_status")
	End If
Next

If ai_start_res + UpperBound(iu_tabs) <= ids_res_list.RowCount() Then
	cb_next.Visible = True
Else
	cb_next.Visible = False
End If
If ai_start_res > 1 Then
	cb_prev.Visible = True
Else
	cb_prev.Visible = False
End If
end subroutine

event ue_postopen;call super::ue_postopen;String	ls_system_id

ids_res_list  = Create DataStore
ids_res_list.DataObject = "d_reservation_list"
ids_customer = Create DataStore
ids_customer.DataObject = "d_customer_data"

ids_salespeople = Create DataStore
ids_salespeople.DataObject = "d_sales_people"
ids_salespeople.SetTransObject(SQLCA)
ids_salespeople.Retrieve('000', 'ZZZ')

ids_servicecenters = Create DataStore
ids_servicecenters.DataObject = "d_service_centers"
ids_servicecenters.SetTransObject(SQLCA)
ids_servicecenters.Retrieve('000', 'ZZZ')

iu_ws_orp3 = Create u_ws_orp3
iu_orp001 = Create u_orp001
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

iu_orp002 = Create u_orp002
If Message.ReturnValue = -1 Then
	Close(This)
	Return
end if

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then
	Close(This)
	Return
end if

ls_system_id = Trim(Message.nf_Get_App_ID())

istr_error_info.se_app_name = ls_system_id 
istr_error_info.se_user_id = sqlca.userid
istr_error_info.se_window_name = "Res Revu"

Integer li_counter
For li_counter = 1 to UpperBound(tab_1.Control)
	Choose Case li_counter
		Case 1
			iu_tabs[li_counter] = tab_1.tabpage_1
		Case 2
			iu_tabs[li_counter] = tab_1.tabpage_2
		Case 3
			iu_tabs[li_counter] = tab_1.tabpage_3
		Case 4
			iu_tabs[li_counter] = tab_1.tabpage_4
		Case 5
			iu_tabs[li_counter] = tab_1.tabpage_5
		Case Else
	End Choose			
Next

This.wf_retrieve()
end event

on w_reservation_review_scheduling.create
int iCurrent
call super::create
this.cb_next=create cb_next
this.cb_prev=create cb_prev
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_next
this.Control[iCurrent+2]=this.cb_prev
this.Control[iCurrent+3]=this.tab_1
end on

on w_reservation_review_scheduling.destroy
call super::destroy
destroy(this.cb_next)
destroy(this.cb_prev)
destroy(this.tab_1)
end on

event close;call super::close;If IsValid(ids_customer) Then Destroy ids_customer
If IsValid(ids_res_list) Then Destroy ids_res_list
If IsValid(ids_servicecenters) then Destroy ids_servicecenters

If IsValid(iu_orp001) Then
	Destroy iu_orp001
End If

If IsValid(iu_orp002) Then
	Destroy iu_orp002
End If

Destroy iu_ws_orp3
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')  
iw_frame.im_menu.mf_Enable('m_previous')
iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_completeorder')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')  
iw_frame.im_menu.mf_Disable('m_previous')
iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_completeorder')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')

end event

event closequery;Integer	li_counter

For li_counter = 1 to UpperBound(iu_tabs)
	If IsValid(iu_tabs[li_counter]) Then
		If iu_tabs[li_counter].ib_modified Then
			tab_1.SelectTab(li_counter)
			Choose Case MessageBox(This.Title + " - " + iu_tabs[li_counter].Text, &
								"Do you want to save changes?", Question!, YesNoCancel!)
				Case 1
					If Not iu_tabs[li_counter].uf_update() Then Return 1
					iu_tabs[li_counter].ib_modified = False
				Case 2
					iu_tabs[li_counter].ib_modified = False
				Case 3
					Return 1
			End Choose
		End If
	End If
Next

Return 0
end event

event open;call super::open;string ls_option

ls_option = Message.StringParm
CHOOSE CASE len(ls_option)
	CASE 1
		If ls_option = 'M' then
			This.Title = "Reservation Movement"
			is_movement = ls_option
		else	
			This.Title = "Reservation Review"
			is_movement = ' '
		End If
	ib_no_inquire = False
	CASE ELSE
		If lower(ls_option) = 'w_reservation_review_scheduling' Then
			This.Title = "Reservation Review"
			is_movement = ' '
			ib_no_inquire = False
		Else		
			ib_no_inquire = True
			is_inquiredata = ls_option + '~t' + 'M'
			This.Title = "Reservation Movement"
			is_movement = 'M'
		End If
END CHOOSE

	
end event

type cb_next from commandbutton within w_reservation_review_scheduling
boolean visible = false
integer x = 2629
integer y = 4
integer width = 215
integer height = 68
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Next"
end type

event clicked;wf_show_reservations(ii_start_res + UpperBound(iu_tabs))
iu_tabs[tab_1.SelectedTab].ib_retrieved = False
iu_tabs[tab_1.SelectedTab].uf_selected()

end event

type cb_prev from commandbutton within w_reservation_review_scheduling
event clicked pbm_bnclicked
boolean visible = false
integer x = 2327
integer y = 4
integer width = 302
integer height = 68
integer taborder = 10
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Previous"
end type

event clicked;wf_show_reservations(ii_start_res - UpperBound(iu_tabs))
iu_tabs[tab_1.SelectedTab].ib_retrieved = False
iu_tabs[tab_1.SelectedTab].uf_selected()

end event

type tab_1 from tab within w_reservation_review_scheduling
integer x = 5
integer y = 4
integer width = 2843
integer height = 1524
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
boolean powertips = true
boolean boldselectedtext = true
boolean createondemand = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type

event rightclicked;If index <= UpperBound(iu_tabs) And index > 0 Then
	If IsValid(iu_tabs[index]) Then
		iu_tabs[index].ib_retrieved = False
		This.SelectTab(index)
		iu_tabs[index].uf_selected()
	End If
End If

end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
end on

event selectionchanged;If newindex <= UpperBound(iu_tabs) Then
	If IsValid(iu_tabs[newindex]) Then iu_tabs[newindex].uf_selected()
End If
end event

type tabpage_1 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2807
integer height = 1492
end type

type tabpage_2 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2807
integer height = 1492
end type

type tabpage_3 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2807
integer height = 1492
end type

type tabpage_4 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2807
integer height = 1492
end type

type tabpage_5 from u_reservation_review_tab within tab_1
boolean visible = false
integer x = 18
integer y = 16
integer width = 2807
integer height = 1492
end type

