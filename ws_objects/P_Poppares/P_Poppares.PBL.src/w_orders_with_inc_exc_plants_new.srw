﻿$PBExportHeader$w_orders_with_inc_exc_plants_new.srw
forward
global type w_orders_with_inc_exc_plants_new from w_base_sheet_ext
end type
type dw_detail from u_base_dw_ext within w_orders_with_inc_exc_plants_new
end type
type dw_dates from u_base_dw_ext within w_orders_with_inc_exc_plants_new
end type
type dw_plant from u_base_dw_ext within w_orders_with_inc_exc_plants_new
end type
end forward

global type w_orders_with_inc_exc_plants_new from w_base_sheet_ext
integer width = 2857
integer height = 1820
string title = "Orders with inc exc plants new"
long backcolor = 67108864
dw_detail dw_detail
dw_dates dw_dates
dw_plant dw_plant
end type
global w_orders_with_inc_exc_plants_new w_orders_with_inc_exc_plants_new

type variables
u_ws_pas1	iu_ws_pas1
s_error     		istr_error_info


Boolean		ib_reinquire, &
		ib_close_this
		
		
string is_plants,is_original


Int					ii_PARange, &
						ii_PAPast
						
						
INTEGER				ii_Query_Retry_Count
end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_apply_ie_plants (string as_plant_string, long as_detail_row_number)
end prototypes

public function boolean wf_retrieve ();string ls_plant,&
		 ls_begin_date,&
	     ls_end_date,ls_input_string,ls_data, ls_string,ls_output,ls_location_name


int li_pos,li_len,li_start, li_return, li_count
		



OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
iw_frame.SetMicroHelp("Wait... Inquiring Database")
If Not ib_inquire Then
	Return False
End If

ls_data = Message.StringParm

li_pos = 1
li_start = 1
li_count = 0

DO WHILE li_pos > 0

li_pos = Pos ( ls_data, '<', li_start )

if li_pos = 0 then
ls_string = Mid ( ls_data, li_start )
else
ls_string = Mid ( ls_data, li_start, li_pos - li_start )
end if

ls_string = Trim(ls_string)

if li_count = 0 then
ls_plant = 	ls_string
//dw_plant.SetItem(1,"location_code",ls_string)
end if

if li_count = 1 then
ls_begin_date = ls_string
ls_location_name = ls_string
end if

if li_count = 2 then
ls_begin_date = ls_string
end if



if li_count = 3 then
	ls_end_date = ls_string
//dw_dates.SetItem(1,"end_date",ls_string)
end if

li_count++

li_start = li_pos + 1

loop

dw_plant.SetItem(1,"location_code",ls_plant)
dw_plant.SetItem(1,"location_name",ls_location_name)
dw_dates.SetItem(1,"begin_date",Date(ls_begin_date))
dw_dates.SetItem(1,"end_date",Date(ls_end_date))

ls_begin_date = String(dw_dates.GetItemDate( 1, "begin_date"),"yyyy-mm-dd")
ls_end_date = String(dw_dates.GetItemDate( 1, "end_date"),"yyyy-mm-dd")




istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_pasp12gr"
istr_error_info.se_message				= Space(70)

ls_input_string = ls_plant + '~t'+ ls_begin_date+'~t'+ ls_end_date
dw_detail.Reset()
li_return = iu_ws_pas1.nf_pasp12gr(istr_error_info,ls_input_string,ls_output,is_plants)

if li_return = 0 then
	
dw_detail.ImportString(ls_output)
dw_detail.sort()

is_original = dw_detail.GetItemString(1,'inc_ind')

end if

If dw_detail.RowCount() = 0 Then dw_detail.InsertRow(0)


end function

public subroutine wf_apply_ie_plants (string as_plant_string, long as_detail_row_number);DataStore	lds_ie_plant

DataWindowChild	ldwc_temp

Long			ll_plant_row_count, &
				ll_sub, &
				ll_found_row, &
				ll_start_pos, &
				ll_new_row
				
String		ls_find_line_number, &
				ls_find_string, &
				ls_plant
				
lds_ie_plant =	Create DataStore
lds_ie_plant.DataObject = 'd_inc_exc_plants_new'
ll_plant_row_count = lds_ie_plant.importstring(as_plant_string)

ls_find_line_number = dw_detail.GetItemString(as_detail_row_number, "detail")
ls_find_string = "detail = '" 
ls_find_string += ls_find_line_number + "'"
ll_start_pos = 1

dw_detail.GetChild("inc_ind", ldwc_temp)

ldwc_temp.Reset();

ll_found_row = lds_ie_plant.Find(ls_find_string, ll_start_pos, ll_plant_row_count)

If ll_found_row > 0 Then
	Do
		ll_new_row = ldwc_temp.InsertRow(0)
		ldwc_temp.SetItem(ll_new_row, "detail", ls_find_line_number)
		ls_plant = lds_ie_plant.GetItemString(ll_found_row, "plant_code")
		ldwc_temp.SetItem(ll_new_row, "plant_code", ls_plant) 
		ll_start_pos = ll_found_row + 1
		ll_found_row = lds_ie_plant.Find(ls_find_string, ll_start_pos, ll_plant_row_count)
		
	Loop While ((ll_found_row > 0) AND (ll_start_pos <= ll_plant_row_count)) 
//	ll_new_row = ldwc_temp.InsertRow(0)
//	ldwc_temp.SetItem(ll_new_row, "plant_code", is_original) 
	
End If		
	
	





end subroutine

on w_orders_with_inc_exc_plants_new.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_dates=create dw_dates
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_dates
this.Control[iCurrent+3]=this.dw_plant
end on

on w_orders_with_inc_exc_plants_new.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_dates)
destroy(this.dw_plant)
end on

event ue_postopen;call super::ue_postopen;iu_ws_pas1 = Create u_ws_pas1


//This.PostEvent("ue_query")




is_inquire_window_name = 'w_orders_with_inc_exc_plants_new_inq'

wf_retrieve()

dw_plant.ib_updateable = False
dw_detail.ib_updateable = False
dw_dates.ib_updateable = False


end event

event ue_query;call super::ue_query;String		ls_PARange,test


If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "FUTURE", ls_PARange) = -1 Then
	if (sqlca.sqldbcode = 10005 or sqlca.sqldbcode = 10025) &
		and ii_Query_Retry_Count < 2 then
		
		ii_Query_Retry_Count += 1
		SQLCA.postevent("ue_reconnect")
		this.postevent("ue_query")
		return
	end if

	// Row not found
	MessageBox("PA Range", "Future PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
ii_PARange = Integer(ls_PARange)

If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "PAST", ls_PARange) = -1 Then
	if (sqlca.sqldbcode = 10005 or sqlca.sqldbcode = 10025) &
		and ii_Query_Retry_Count < 2 then
		
		ii_Query_Retry_Count += 1
		SQLCA.postevent("ue_reconnect")
		this.postevent("ue_query")
		return
	end if
	
	// Row not found
	MessageBox("PA Range", "Past PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
ii_PAPast = Integer(ls_PARange)

test = String(dw_dates.SetItem(1,"end_date",(RelativeDate(Today(), ii_PARange))))


dw_dates.SetItem( 1, "begin_date", now())


//dw_dates.uf_SetEndDate(RelativeDate(Today(), ii_PARange))
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_Save')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleteRow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')
end event

type dw_detail from u_base_dw_ext within w_orders_with_inc_exc_plants_new
integer y = 208
integer width = 2766
integer height = 1384
integer taborder = 20
string dataobject = "d_ord_inc_exc_plants_new_detail"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;//If This.RowCount() = 0 Then This.InsertRow(0)
end event

event clicked;call super::clicked;If Row > 0 Then
	Choose Case dwo.name
		Case 'inc_ind'	
			dw_detail.SetRedraw(True)
			wf_apply_ie_plants(is_plants, row)			

	END Choose
End If			
end event

event itemchanged;call super::itemchanged;
dw_detail.SetItem(row,'inc_ind',is_original)
return 2
end event

type dw_dates from u_base_dw_ext within w_orders_with_inc_exc_plants_new
integer x = 1618
integer width = 800
integer height = 200
integer taborder = 20
boolean enabled = false
string dataobject = "d_source_begin_end_date"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type dw_plant from u_base_dw_ext within w_orders_with_inc_exc_plants_new
integer x = 55
integer y = 60
integer width = 1554
integer height = 88
integer taborder = 10
boolean enabled = false
string dataobject = "d_plant_inc_exc_new"
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

