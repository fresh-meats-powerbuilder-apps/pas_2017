﻿$PBExportHeader$w_load_ahead_availability_new_inq.srw
forward
global type w_load_ahead_availability_new_inq from w_base_response_ext
end type
type dw_1 from u_base_dw_ext within w_load_ahead_availability_new_inq
end type
type dw_2 from u_base_dw_ext within w_load_ahead_availability_new_inq
end type
type st_1 from statictext within w_load_ahead_availability_new_inq
end type
type dw_3 from u_plant within w_load_ahead_availability_new_inq
end type
end forward

global type w_load_ahead_availability_new_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1979
integer height = 824
string title = "Load Ahead Availability Inquire"
long backcolor = 67108864
dw_1 dw_1
dw_2 dw_2
st_1 st_1
dw_3 dw_3
end type
global w_load_ahead_availability_new_inq w_load_ahead_availability_new_inq

type variables
string  is_plant, &
		is_plant_desc, &
		is_ship_date, &
		is_full_loads, &
		is_combined, &
		is_uncombined, &
		is_input_string
w_base_sheet	iw_parent
end variables

on w_load_ahead_availability_new_inq.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_2=create dw_2
this.st_1=create st_1
this.dw_3=create dw_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_2
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.dw_3
end on

on w_load_ahead_availability_new_inq.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_2)
destroy(this.st_1)
destroy(this.dw_3)
end on

event ue_base_ok;call super::ue_base_ok;string ls_plant, &
		ls_plant_desc, &
		ls_full_loads, &
		ls_combined, &
		ls_uncombined, ls_value, ls_temp
boolean lb_bool
Date 	ldt_ship
		
If dw_3.AcceptText() = -1 then return
If dw_1.AcceptText() = -1 then return
If dw_2.AcceptText() = -1 then return


is_plant = dw_3.getItemString(1, "location_code")
is_plant_desc = dw_3.getItemString(1, "location_name")
is_ship_date = string(dw_1.GetItemDate(1, "ship_date"), 'yyyy-mm-dd')

If dw_2.GetItemString(1, "full_loads") = 'Y'  then
	is_full_loads = 'Y'
else 
	is_full_loads = 'N'
end if

If dw_2.GetItemString(1, "combined_loads") = 'Y' then
	is_combined = 'Y'
else
	is_combined = 'N'
end if

If dw_2.GetItemString(1, "uncombined_loads") = 'Y' then
	is_uncombined = 'Y'
else
	is_uncombined = 'N'
end if
SetProfileString(iw_frame.is_UserINI, "Pas", "LastFull", is_full_loads)
SetProfileString(iw_frame.is_UserINI, "Pas", "Lastcombined", is_combined)
SetProfileString(iw_frame.is_UserINI, "Pas", "Lastuncombined", is_uncombined)



ls_temp = dw_3.uf_get_plant_code()
iw_ParentWindow.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_3.uf_get_plant_descr()
iw_ParentWindow.Event ue_Set_Data('Plant_Desc', ls_temp)


is_input_string = is_plant + '>' + is_plant_desc + '>' + is_ship_date + '>' + is_full_loads + '>' + is_combined + '>' + is_uncombined
ib_ok_to_close = True





Close(this)


end event

event ue_base_cancel;call super::ue_base_cancel;Close(this)
end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm
This.Title = iw_ParentWindow.title + " Inquire"


iw_frame.SetMicroHelp("Ready")
end event

event close;call super::close;closewithreturn(this, is_input_string)
end event

event ue_postopen;call super::ue_postopen;String		ls_temp
u_string_functions		lu_string

ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "LastDate", "")
dw_1.SetItem(1, "ship_date", date(ls_temp))

ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "LastFull", "")
dw_2.SetItem(1, "full_loads", ls_temp)

ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "Lastcombined", "")
dw_2.SetItem(1, "combined_loads", ls_temp)

ls_temp = ProfileString(iw_frame.is_UserINI, "Pas", "Lastuncombined", "")
dw_2.SetItem(1, "uncombined_loads", ls_temp)

iw_ParentWindow.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_3.uf_set_plant_code(ls_temp)
End If


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_load_ahead_availability_new_inq
boolean visible = false
integer x = 1641
integer y = 324
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_load_ahead_availability_new_inq
integer x = 1641
integer y = 200
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_load_ahead_availability_new_inq
integer x = 1641
integer y = 76
integer taborder = 40
end type

type dw_1 from u_base_dw_ext within w_load_ahead_availability_new_inq
integer x = 14
integer y = 180
integer width = 709
integer height = 128
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_load_ahead_availability_hd1_new"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
//dw_1.SetItem(1, "ship_date", Now())
end event

event itemchanged;call super::itemchanged;string ls_date

dw_1.AcceptText()
ls_date = string(dw_1.GetItemDate(1, "ship_date"))
SetProfileString(iw_frame.is_UserINI, "Pas", "LastDate", ls_date)
end event

type dw_2 from u_base_dw_ext within w_load_ahead_availability_new_inq
integer x = 384
integer y = 316
integer width = 535
integer height = 392
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_load_status_new"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;//string ls_date, ls_full, ls_combined, ls_uncombined
//
//dw_2.AcceptText()
//ls_full = dw_2.GetItemString(1, "full_loads")
//SetProfileString(iw_frame.is_UserINI, "Pas", "LastFull", ls_full)
//ls_combined = dw_2.GetItemString(1, "combined_loads")
//SetProfileString(iw_frame.is_UserINI, "Pas", "Lastcombined", ls_combined)
//ls_uncombined = dw_2.GetItemString(1, "uncombined_loads")
//SetProfileString(iw_frame.is_UserINI, "Pas", "Lastuncombined", ls_uncombined)
end event

type st_1 from statictext within w_load_ahead_availability_new_inq
integer x = 41
integer y = 320
integer width = 343
integer height = 68
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Load status"
boolean focusrectangle = false
end type

type dw_3 from u_plant within w_load_ahead_availability_new_inq
integer x = 32
integer y = 76
integer taborder = 10
boolean bringtotop = true
end type

