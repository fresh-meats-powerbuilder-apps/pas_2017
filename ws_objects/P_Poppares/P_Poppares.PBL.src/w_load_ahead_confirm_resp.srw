﻿$PBExportHeader$w_load_ahead_confirm_resp.srw
forward
global type w_load_ahead_confirm_resp from w_base_response_ext
end type
type dw_load_ahead_confirm from u_base_dw_ext within w_load_ahead_confirm_resp
end type
type st_1 from statictext within w_load_ahead_confirm_resp
end type
end forward

global type w_load_ahead_confirm_resp from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 2903
string title = "Loads Not Confirmed"
dw_load_ahead_confirm dw_load_ahead_confirm
st_1 st_1
end type
global w_load_ahead_confirm_resp w_load_ahead_confirm_resp

type variables
String is_header_data
s_error		istr_error_info
u_ws_pas5		iu_ws_pas5
w_load_ahead_availability_new iw_parent

end variables

on w_load_ahead_confirm_resp.create
int iCurrent
call super::create
this.dw_load_ahead_confirm=create dw_load_ahead_confirm
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_load_ahead_confirm
this.Control[iCurrent+2]=this.st_1
end on

on w_load_ahead_confirm_resp.destroy
call super::destroy
destroy(this.dw_load_ahead_confirm)
destroy(this.st_1)
end on

event open;call super::open;String ls_confirm_string

iw_parentwindow = Message.PowerObjectParm
iw_parent = Message.PowerObjectParm

iw_parentwindow.Event ue_get_data('ConfirmData')
ls_confirm_string = Message.StringParm

dw_load_ahead_confirm.Reset()
dw_load_ahead_confirm.ImportString(ls_confirm_string)
dw_load_ahead_confirm.SetSort('move_status_ind')
dw_load_ahead_confirm.Sort()

iu_ws_pas5 = Create u_ws_pas5

iw_frame.SetMicroHelp("Ready")
end event

event close;call super::close;If isValid(iu_ws_pas5) Then
	Destroy(iu_ws_pas5)
End If

Close(this)
end event

event ue_base_cancel;call super::ue_base_cancel;Close(this)
end event

event ue_base_ok;call super::ue_base_ok;integer li_rowcount, li_row, li_m_rowcount, li_count
string ls_update_string, ls_load_number, ls_output, ls_move_status_ind, ls_msg, ls_confirm_string, ls_temp
DataStore lds_moves

li_row = 1
li_rowcount = dw_load_ahead_confirm.RowCount()

dw_load_ahead_confirm.AcceptText()

//move_status_ind: M = Moved, C=Confirm Needed, E=Error

iw_parentwindow.Event ue_Get_Data('MoveDate')
is_header_data = Message.StringParm
iw_parentwindow.Event ue_Get_Data('MovePlant')
is_header_data += '~t' + Message.StringParm

ls_output = is_header_data

Do while li_row <= li_rowcount
	If dw_load_ahead_confirm.GetItemString(li_row, "confirm_ind") = 'Y' Then
		ls_load_number += dw_load_ahead_confirm.GetItemString(li_row, "load_number") + '~t' + 'Y' + '~t'				
	End If
	li_row ++
Loop

ls_update_string = is_header_data + '>' + ls_load_number + '>'
istr_error_info.se_event_name = "ue_base_ok"
istr_error_info.se_procedure_name = "nf_pasp20gr"
istr_error_info.se_message = Space(71)

If iu_ws_pas5.nf_pasp20gr(istr_error_info, ls_update_string, ls_output) < 0 then
		This.SetRedraw(True)
End if

If IsNull(ls_output) Then
	Close(this)
Else
	dw_load_ahead_confirm.Reset()
	
	lds_moves = Create DataStore
	lds_moves.DataObject = 'd_load_ahead_confirm_ds'
	lds_moves.ImportString(ls_output)
			
	li_m_rowcount = lds_moves.RowCount()
	li_row = 1
	li_count = 1
						
	Do While li_row <= li_m_rowcount
				
		ls_load_number = lds_moves.GetItemString(li_row, "load_number")
		ls_move_status_ind = lds_moves.GetItemString(li_row, "move_status_ind")
		ls_msg = lds_moves.GetItemString(li_row, "message")
				
		If ls_move_status_ind = 'M' Then		
  				iw_parent.wf_update_load_status(ls_load_number)
		Else
				ls_confirm_string += ls_load_number + '~t' + ls_move_status_ind + '~t' + ls_msg + '~t'	 + 'N' + '~r~n'				
		End If

		li_row ++
	Loop
				
	If len(ls_confirm_string) = 0 Then
		Close(This)
	Else
		dw_load_ahead_confirm.Reset()
		dw_load_ahead_confirm.ImportString(ls_confirm_string)
		dw_load_ahead_confirm.SetSort('move_status_ind')
		dw_load_ahead_confirm.Sort()
		dw_load_ahead_confirm.SetColumn("confirm_ind")
		dw_load_ahead_confirm.SelectText(1,1)
		dw_load_ahead_confirm.SetFocus( )
	End If
										
End If
end event

event ue_postopen;call super::ue_postopen;This.Title = "Loads Not Confirmed"
dw_load_ahead_confirm.SetColumn("confirm_ind")
dw_load_ahead_confirm.SelectText(1,1)
dw_load_ahead_confirm.SetFocus( )
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_load_ahead_confirm_resp
boolean visible = false
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_load_ahead_confirm_resp
integer x = 1111
integer y = 1240
integer height = 124
integer taborder = 30
string text = "Return"
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_load_ahead_confirm_resp
integer x = 329
integer y = 1240
integer width = 590
integer height = 124
integer taborder = 20
boolean enabled = false
string text = "&Move Confirmed Loads"
end type

type dw_load_ahead_confirm from u_base_dw_ext within w_load_ahead_confirm_resp
integer x = 37
integer y = 24
integer width = 2798
integer height = 1172
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_load_ahead_confirm"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;if this.rowcount() = 0 then this.insertrow(0)
	
end event

event itemchanged;call super::itemchanged;dw_load_ahead_confirm.AcceptText()

If dwo.name = 'confirm_ind' Then
	cb_base_ok.Enabled = True	
End If
end event

type st_1 from statictext within w_load_ahead_confirm_resp
integer x = 2062
integer y = 1292
integer width = 745
integer height = 88
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "*All other loads have been moved."
boolean focusrectangle = false
end type

