﻿$PBExportHeader$u_reservation_review_tab.sru
forward
global type u_reservation_review_tab from userobject
end type
type cbx_showall from checkbox within u_reservation_review_tab
end type
type st_showall from statictext within u_reservation_review_tab
end type
type st_lines from statictext within u_reservation_review_tab
end type
type dw_header from u_netwise_dw within u_reservation_review_tab
end type
type dw_detail from u_netwise_dw within u_reservation_review_tab
end type
end forward

global type u_reservation_review_tab from userobject
integer width = 2789
integer height = 1276
long backcolor = 12632256
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 553648127
event ue_cascadeplant ( )
event ue_load_inc_exc ( )
cbx_showall cbx_showall
st_showall st_showall
st_lines st_lines
dw_header dw_header
dw_detail dw_detail
end type
global u_reservation_review_tab u_reservation_review_tab

type variables
Boolean	ib_retrieved = FALSE
Boolean	ib_modified = FALSE
String	is_order_status
Date	id_gpo_date
String	is_filtered
DataStore	ids_inc_exc
Window		d_reservation_change_pa
u_ws_orp2 iu_ws_orp2
s_error		istr_error_info
u_ws_pas5	iu_ws_pas5
u_ws_orp5	iu_ws_orp5
end variables

forward prototypes
public subroutine uf_selected ()
public subroutine uf_filldropdowns ()
public function boolean uf_update ()
public subroutine uf_cascadeplants ()
public subroutine uf_completeorder ()
public subroutine uf_showall (boolean ab_showall)
public function string uf_describerow (ref long al_rowtodescribe, ref datawindow adw_datawindow)
public subroutine uf_fill_inc_exc (string as_fill_string)
public subroutine uf_change_plants ()
end prototypes

event ue_cascadeplant;This.uf_cascadeplants()
end event

public subroutine uf_selected ();Int		li_ret
Boolean 	lb_first_time

Double	ld_Page_number, &
			ld_task_number
			

String	ls_ResNum, &
			ls_header, &
			ls_detail, &
			ls_sales_info, &
			ls_temp,ls_inc_exc
			

If ib_retrieved then return

ib_retrieved = TRUE

SetPointer(Hourglass!)

w_reservation_review_scheduling	lw_parent
lw_parent = Parent.GetParent()
iu_ws_orp5 = Create u_ws_orp5
Choose Case is_order_status
	Case 'N'
		dw_detail.DataObject = 'd_reservation_short_pa'
		uf_showall(True)
	Case 'I'
		dw_detail.DataObject = 'd_reservation_long_pa'
		uf_showall(True)
	Case Else
		dw_detail.DataObject = 'd_reservation_change_pa'
		uf_showall(False)
End Choose
uf_FillDropDowns()

ls_temp = This.Text
ls_resnum = iw_frame.iu_string.nf_gettoken(ls_temp,"~r~n")

ls_header = ls_ResNum + '~t~t~t' + String(id_gpo_date,"mm/dd/yyyy") + '~t~t~tA~t~tI~t~t' + 'S' + '~t'

lw_parent.istr_error_info.se_event_name = 'wf_retrieve_reservation'

ld_task_number = 0
ld_page_number = 0
dw_header.Reset()
// This insertrow is just for looks.  If the rpc takes awhile, this makes it look better
dw_header.InsertRow(0)
dw_detail.Reset()
This.SetRedraw(False)

//Do
//	li_ret = lw_parent.iu_pas201.nf_orpo68ar_schedres(lw_parent.istr_error_info, &
//										ls_header, &
//										ls_detail,&
//										ls_inc_exc,&
//										ld_task_number, &
//										ld_page_number)
	li_ret = iu_ws_orp5.nf_orpo68fr(lw_parent.istr_error_info, &
										ls_header, &
										ls_detail, ls_inc_exc)
//
	destroy u_ws_orp5
	If Not iw_frame.iu_string.nf_IsEmpty(ls_header) Then
		dw_header.Reset()
		dw_header.ImportString(ls_header)
	End if
//	messagebox('detail',ls_detail)
	dw_detail.ImportString(ls_detail)
	
/// ibdkdld 
	If Not iw_frame.iu_string.nf_IsEmpty(ls_inc_exc) Then
			uf_fill_inc_exc(ls_inc_exc)
	End If
		
//Loop While ld_Task_Number <> 0 And ld_page_number <> 0


// Test SCRIPT REMOVE WHEN DONE
dw_header.ResetUpdate()

String ls_location
Long ll_row
//ls_location = dw_header.Describe("Evaluate('LookUpDisplay(sales_location_code) ', " &
//	+ String(1) + ")")
ls_location = dw_header.GetItemString(1,"sales_location_code")
ll_row = lw_parent.ids_salespeople.Find("smancode = '" + ls_location + &
		"'and smantype = 'S'",1,lw_parent.ids_salespeople.RowCount())
If ll_row > 0 Then
	ls_temp = lw_parent.ids_salespeople.GetItemString(ll_row,"smanloc")
	ll_row = lw_parent.ids_servicecenters.Find("service_center_code = '" + &
			ls_temp + "'",1,lw_parent.ids_salespeople.RowCount())
	If ll_row > 0 Then
		ls_location = lw_parent.ids_servicecenters.GetItemString(ll_row,"service_center_name")
	End If
End If
dw_header.SetItem(1,"service_center_name",ls_location)

dw_detail.ResetUpdate()
dw_detail.Filter()
If dw_detail.RowCount() < 1 Then uf_showall(True)

dw_Detail.SetColumn( "ordered_units")

dw_Detail.SetFocus()
dw_Detail.SetRow(1)
This.SetRedraw(True)
end subroutine

public subroutine uf_filldropdowns ();DataWindowChild	dwc_temp


dw_detail.GetChild('plant', dwc_temp)
If IsValid(dwc_temp) Then
	dwc_temp.SetTransObject(SQLCA)
	dwc_temp.Retrieve()
//	iw_frame.iu_netwise_data.nf_GetLocations(dwc_temp)
End if

dw_detail.GetChild('change_plant_code', dwc_temp)
If IsValid(dwc_temp) Then
	dwc_temp.SetTransObject(SQLCA)
	dwc_temp.Retrieve()
//	iw_frame.iu_netwise_data.nf_GetLocations(dwc_temp)
End if

dw_detail.GetChild('ordered_units_uom', dwc_temp)
dwc_temp.SetTransObject(SQLCA)
dwc_temp.Retrieve("OUOM")
//iw_frame.iu_netwise_data.nf_gettutltype_dwc('OUOM', dwc_temp)

return
end subroutine

public function boolean uf_update ();Boolean	lb_ErrorOccured

Int		li_ret, &
			li_ChangedCounter, &
			li_Counter, &
			li_temp

Double	ld_page_number, &
			ld_task_number
			
Long		ll_row, &
			ll_RowCount, &
			lla_ChangedRows[], &
			ll_end, &
			ll_find

String	ls_header, &
			ls_detail, &
			ls_RPC_Detail, &
			ls_Returned_Detail, &
			ls_sales_info, &
			ls_temp, &
			ls_temp2,ls_inc_exc
			
Date		ld_null_date
 
 
u_orp003		lu_orp003
//iu_ws_pas5 = Create u_ws_pas5
iu_ws_orp5	= Create u_ws_orp5

w_reservation_review_scheduling	lw_parent
lw_parent = Parent.GetParent()

If dw_detail.AcceptText() = -1 Then return False

ll_rowcount = dw_detail.RowCount()

If  dw_detail.Find("Pos(detail_errors, 'E') > 0", 1, ll_RowCount) > 0 Then
	MessageBox('Update Errors', 'There were errors encountered.  Please fix the errors before updating.')
	Return False
End If

// Loop Through the detail and make sure that all plants are filled in
ll_Row = dw_detail.Find("plant <= '000' or IsNull(plant)", 1, 10000)
If ll_row > 0 Then
	iw_Frame.SetMicroHelp("All plants must be valid before updating")
	dw_detail.SetFocus()
	dw_detail.Filter()
	dw_detail.SetRow(ll_Row)
	dw_detail.SetColumn('plant')
	return False
End if

SetPointer(Hourglass!)

dw_header.SetItem(1, "update_flag", "")
ls_header = dw_header.Describe("DataWindow.Data")

SetNull(ld_null_date)
ll_RowCount = dw_detail.RowCount()
FOR li_Counter = 1 TO ll_RowCount
	ls_temp2=String(dw_detail.GetItemDate(li_Counter, 'change_ship_date'), &
				"yyyy-mm-dd")
	IF NOT IsDate(ls_temp2) THEN
		dw_detail.SetItem(li_Counter,'change_ship_date',ld_null_date)
	END IF
NEXT

ls_detail = iw_frame.iu_string.nf_BuildUpdateString(dw_detail)

ll_RowCount = dw_detail.RowCount()
For li_Counter = 1 to ll_RowCount
	ls_temp = dw_detail.GetItemString(li_counter, 'update_flag')
	If ls_temp = 'A' or ls_temp = 'U' Then
		li_ChangedCounter ++
		lla_ChangedRows[li_ChangedCounter] = li_Counter
	End if
Next



If Not iw_frame.iu_string.nf_IsEmpty(ls_Detail) Then
	lw_parent.istr_error_info.se_event_name = 'uf_update'

	ld_task_number = 0
	ld_page_number = 0
	ls_Returned_detail = ''

	Do
		li_temp = iw_frame.iu_string.nf_npos(ls_Detail, '~r~n', 1, 99)
		If li_temp > 0 Then
			ls_RPC_Detail = Left(ls_Detail, li_Temp + 1)
			ls_Detail = Mid(ls_Detail, li_Temp + 2)
		Else
			ls_RPC_Detail = ls_detail
			ls_detail = ''
		End if

//		li_ret = lw_parent.iu_pas201.nf_orpo68ar_schedres(lw_parent.istr_error_info, &
//												ls_header, &
//												ls_RPC_detail, &
//												ls_inc_exc,&
//												ld_task_number, &
//												ld_page_number)
		li_ret = iu_ws_orp5.nf_orpo68fr(lw_parent.istr_error_info, &
												ls_header, &
												ls_RPC_detail, &
												ls_inc_exc)

		If Left(ls_RPC_Detail, 2) <> '~r~n' And Not iw_frame.iu_string.nf_IsEmpty(ls_RPC_Detail) Then	
			ls_RPC_Detail += '~r~n'
		End if

		ls_Returned_Detail += ls_RPC_detail
		If li_ret < 0 Then 
			iw_frame.iu_string.nf_ReplaceRows(ls_Returned_Detail, lla_ChangedRows, dw_detail)
			dw_detail.Filter()
			return False
		End if
		If li_ret > 0 Then lb_ErrorOccured = True
	Loop while Not iw_frame.iu_string.nf_IsEmpty(ls_detail)
End if
//Destroy iu_ws_pas5
Destroy iu_ws_orp5

iw_frame.iu_string.nf_ReplaceRows(ls_Returned_Detail, lla_ChangedRows, dw_detail)

If lb_ErrorOccured Then 	
	// Find the first column with an error
	// The end value is one greater than the row count
	ll_end = dw_detail.RowCount( ) + 1
	ll_find = 1
	ll_find = dw_detail.Find("Pos(detail_errors,'E') > 0",  &
			ll_find, ll_end)
	If ll_find > 0 Then
		//Set position to that of the field with the error
		dw_detail.SetRow(ll_find)
		dw_detail.ScrollToRow(ll_find)
		dw_detail.SetColumn( &
				Pos(dw_detail.GetItemString(ll_find,"detail_errors"),'E') )
		dw_detail.SelectText(1,Len(dw_detail.GetText()))
	End If
	dw_detail.Filter()
	If dw_detail.RowCount() < 1 Then uf_showall(True)
	return False
End If
dw_detail.ResetUpdate()

If dw_detail.DataObject = 'd_reservation_short_pa' Then
	// If this is the first time it was saved, blow it out
	lu_orp003 = Create u_orp003
	iu_ws_orp2 = Create u_ws_orp2

	If Message.ReturnValue = -1 Then
		iw_Frame.SetMicroHelp("Reservation Could not be rolled out")
		dw_detail.Filter()
		return False
	End if	

	ls_temp = This.Text

	ls_header = iw_frame.iu_string.nf_gettoken(ls_temp,"~r~n") + '~t~t~t' + &
		String(This.id_gpo_date,"mm/dd/yyyy") + '~t~t~t~t~t~t~t~t~t~t~t~tR~t'

	ls_detail = ""

	lw_parent.istr_error_info.se_event_name = 'uf_update'

//	li_ret = lu_orp003.nf_orpo43ar( lw_parent.istr_error_info, &
//									ls_header, &
//									ls_detail)
	li_ret = iu_ws_orp2.nf_orpo43fr(ls_header, ls_detail, lw_parent.istr_error_info)


	Destroy lu_orp003
	Destroy iu_ws_orp2

	If li_ret = 0 Then
		lw_parent.ids_res_list.SetItem(lw_parent.tab_1.SelectedTab + &
				lw_parent.ii_start_res - 1, 'order_status', 'I')
		This.is_order_status = 'I'
		This.ib_retrieved = FALSE
		This.Post uf_selected()
	Else
		dw_detail.Filter()
		if dw_detail.RowCount() < 1 Then uf_showall(True)
		return False
	End if	
	
End if

This.ib_modified = FALSE
dw_detail.Filter()
if dw_detail.RowCount() < 1 Then uf_showall(True)
return True
end function

public subroutine uf_cascadeplants ();DataWindowChild		ldwc_inc

Long	ll_ClickedRow,&
		ll_Tabpos,&
		ll_LoopCount,&
		ll_RowCount

String	ls_CopyPlant,&
			ls_Errors, &
			ls_ClickedCol,ls_inc_exc
			
If dw_detail.AcceptText() = -1 Then return

ll_TabPos = Pos(dw_Detail.is_ObjectAtPointer, "~t")
ll_ClickedRow = Long(Mid(dw_Detail.is_ObjectAtPointer, ll_TabPos + 1))

// This should be either plant or change_plant_code
ls_ClickedCol = Left(dw_detail.is_ObjectAtPointer,ll_TabPos - 1)
ll_RowCount = dw_Detail.RowCount()
ls_CopyPlant = dw_Detail.GetItemString( ll_ClickedRow, ls_ClickedCol)

dw_Detail.SetRedraw( False)
IF dw_Detail.IsSelected( ll_ClickedRow) Then
	For ll_LoopCount = ll_ClickedRow to ll_RowCount
	 	IF dw_Detail.IsSelected( ll_LoopCount) Then 
				ls_Errors = dw_Detail.GetItemString( ll_LoopCount, "detail_errors")
				Choose Case ls_ClickedCol
					Case "plant"
						//Ibdkdld start
						ls_inc_exc = Trim(dw_detail.GetitemString(ll_loopcount,'inc_exc_ind'))
						dw_detail.GetChild('inc_exc_ind', ldwc_inc)
						ldwc_inc.SetFilter( "#1 = '" + dw_detail.GetItemString(ll_loopcount,'customer_id') + "'")
						ldwc_inc.Filter()
						ldwc_inc.SetSort ( '#2 A' )
						ldwc_inc.Sort()
						If ls_inc_exc = 'I' Then
							IF ldwc_inc.Find( "plant = '" +ls_CopyPlant+"'", 1 , ldwc_inc.RowCount() ) < 1 Then
								iw_frame.SetMicroHelp("Invalid plant, This plant is not an Included plant")
								Continue
							End If
						Else
							If ls_inc_exc = 'E' Then
								IF ldwc_inc.Find( "plant = '" +ls_CopyPlant+"'", 1 , ldwc_inc.RowCount() ) > 0 Then
									iw_frame.SetMicroHelp("Invalid plant, This plant is an Excluded plant")
									Continue
								End If
							End If
						End If
						//Ibdkdld end
						If Mid(ls_Errors,7,1) = 'V' Then Continue
						dw_detail.SetItem( ll_LoopCount, "detail_errors",&
							Left( ls_Errors,6) + 'M' + Mid( ls_Errors, 8))
					Case "change_plant_code"
						//Ibdkdld start
						ls_inc_exc = Trim(dw_detail.GetitemString(ll_loopcount,'inc_exc_ind'))
						dw_detail.GetChild('inc_exc_ind', ldwc_inc)
						ldwc_inc.SetFilter( "#1 = '" + dw_detail.GetItemString(ll_loopcount,'customer_id') + "'")
						ldwc_inc.Filter()
						ldwc_inc.SetSort ( '#2 A' )
						ldwc_inc.Sort()
						If ls_inc_exc = 'I' Then
							IF ldwc_inc.Find( "plant = '" +ls_CopyPlant+"'", 1 , ldwc_inc.RowCount() ) < 1 Then
								iw_frame.SetMicroHelp("Invalid plant, This plant is not an Included plant")
								Continue
							End If
						Else
							If ls_inc_exc = 'E' Then
								IF ldwc_inc.Find( "plant = '" +ls_CopyPlant+"'", 1 , ldwc_inc.RowCount() ) > 0 Then
									iw_frame.SetMicroHelp("Invalid plant, This plant is an Excluded plant")
									Continue
								End If
							End If
						End If
						//Ibdkdld end
						If Mid(ls_Errors,15,1) = 'V' Then Continue
						dw_detail.SetItem( ll_LoopCount, "detail_errors",&
							Left( ls_Errors,14) + 'M' + Mid( ls_Errors, 16))
				End Choose   						
				dw_detail.SetItem( ll_LoopCount, ls_ClickedCol , ls_CopyPlant)
				//ibdkdld
				//IF dw_Detail.GetItemStatus( ll_LoopCount, 0, Primary!) = NewModified!  OR &
				IF	dw_detail.GetItemString( ll_LoopCount,"update_flag") = 'A' Then
					dw_Detail.SetItem( ll_LoopCount, "update_flag", 'A')
				ELSE
					dw_Detail.SetItem( ll_LoopCount, "update_flag", 'U')
				END IF
		END IF		
	Next
ELSE
	For ll_LoopCount = ll_ClickedRow to ll_RowCount
		IF Not(dw_Detail.IsSelected( ll_LoopCount)) Then 
				ls_Errors = dw_Detail.GetItemString( ll_LoopCount, "detail_errors")
				Choose Case ls_ClickedCol
					Case "plant"
						//Ibdkdld start
						ls_inc_exc = Trim(dw_detail.GetitemString(ll_loopcount,'inc_exc_ind'))
						dw_detail.GetChild('inc_exc_ind', ldwc_inc)
						ldwc_inc.SetFilter( "#1 = '" + dw_detail.GetItemString(ll_loopcount,'customer_id') + "'")
						ldwc_inc.Filter()
						ldwc_inc.SetSort ( '#2 A' )
						ldwc_inc.Sort()
						If ls_inc_exc = 'I' Then
							IF ldwc_inc.Find( "plant = '" +ls_CopyPlant+"'", 1 , ldwc_inc.RowCount() ) < 1 Then
								iw_frame.SetMicroHelp("Invalid plant, This plant is not an Included plant")
								Continue
							End If
						Else
							If ls_inc_exc = 'E' Then
								IF ldwc_inc.Find( "plant = '" +ls_CopyPlant+"'", 1 , ldwc_inc.RowCount() ) > 0 Then
									iw_frame.SetMicroHelp("Invalid plant, This plant is an Excluded plant")
									Continue
								End If
							End If
						End If
						//Ibdkdld end
						If Mid(ls_Errors,7,1) = 'V' Then Continue
						dw_detail.SetItem( ll_LoopCount, "detail_errors",&
							Left( ls_Errors,6) + 'M' + Mid( ls_Errors, 8))
					Case "change_plant_code"
						//Ibdkdld start
						ls_inc_exc = Trim(dw_detail.GetitemString(ll_loopcount,'inc_exc_ind'))
						dw_detail.GetChild('inc_exc_ind', ldwc_inc)
						ldwc_inc.SetFilter( "#1 = '" + dw_detail.GetItemString(ll_loopcount,'customer_id') + "'")
						ldwc_inc.Filter()
						ldwc_inc.SetSort ( '#2 A' )
						ldwc_inc.Sort()
						If ls_inc_exc = 'I' Then
							IF ldwc_inc.Find( "plant = '" +ls_CopyPlant+"'", 1 , ldwc_inc.RowCount() ) < 1 Then
								iw_frame.SetMicroHelp("Invalid plant, This plant is not an Included plant")
								Continue
							End If
						Else
							If ls_inc_exc = 'E' Then
								IF ldwc_inc.Find( "plant = '" +ls_CopyPlant+"'", 1 , ldwc_inc.RowCount() ) > 0 Then
									iw_frame.SetMicroHelp("Invalid plant, This plant is an Excluded plant")
									Continue
								End If
							End If
						End If
						//Ibdkdld end
						If Mid(ls_Errors,15,1) = 'V' Then Continue
						dw_detail.SetItem( ll_LoopCount, "detail_errors",&
							Left( ls_Errors,14) + 'M' + Mid( ls_Errors, 16))
				End Choose   										
				dw_detail.SetItem( ll_LoopCount, ls_ClickedCol, ls_CopyPlant)
				//ibdkdld
				//IF dw_Detail.GetItemStatus( ll_LoopCount, 0, Primary!) = NewModified!  OR &
				IF	dw_detail.GetItemString( ll_LoopCount,"update_flag") = 'A' Then
					dw_Detail.SetItem( ll_LoopCount, "update_flag", 'A')
				ELSE
					dw_Detail.SetItem( ll_LoopCount, "update_flag", 'U')
				END IF
		END IF
	Next
END IF
dw_Detail.SetRedraw( True)

end subroutine

public subroutine uf_completeorder ();Int		li_ret, &
			li_Counter, &
			li_selected
Double	ld_page_number, &
			ld_task_number

String	ls_ResNum, &
			ls_header, &
			ls_detail, &
			ls_sales_info, &
			ls_temp,ls_inc_exc

u_reservation_review_tab lu_tabs[]

w_reservation_review_scheduling	lw_parent
lw_parent = Parent.GetParent()

// Need to update first
If Not uf_update() Then return 
iu_ws_orp5 = Create u_ws_orp5
This.SetRedraw(False)

ls_temp = This.Text
ls_resnum = iw_frame.iu_string.nf_gettoken(ls_temp,"~r~n")

ls_header = ls_ResNum + '~t~t~t' + String(id_gpo_date,"mm/dd/yyyy") + '~t~t~t~t~tX~t~t' + 'S' + '~t~t'

lw_parent.istr_error_info.se_event_name = 'uf_completeorder'

ld_task_number = 0
ld_page_number = 0

//li_ret = lw_parent.iu_pas201.nf_orpo68ar_schedres(lw_parent.istr_error_info, &
//										ls_header, &
//										ls_detail, &
//										ls_inc_exc,&
//										ld_task_number, &
//										ld_page_number)
li_ret = iu_ws_orp5.nf_orpo68fr(lw_parent.istr_error_info, &
										ls_header, &
										ls_detail, &
										ls_inc_exc)
This.SetRedraw(True)

lw_parent.Post wf_remove_reservation()
Destroy u_ws_orp5
return

end subroutine

public subroutine uf_showall (boolean ab_showall);If ab_showall Then
	is_filtered = "N"
	cbx_showall.Checked=True
	dw_detail.SetFilter("")
	dw_detail.Filter()
Else
	is_filtered = "Y"
	cbx_showall.Checked=False
	dw_detail.SetFilter("line_status = 'S' OR line_status = 'M' " + &
			"OR line_status = 'N'")
	dw_detail.Filter()
End If

end subroutine

public function string uf_describerow (ref long al_rowtodescribe, ref datawindow adw_datawindow);Long  ll_RowstoMove,&
		ll_RowCount,&
		ll_LoopCount

String	ls_Return_String

adw_DataWindow.SetRedraw( False)

ll_RowsToMove = al_RowToDescribe - 1
IF ll_RowsToMove > 0 Then &
	adw_DataWindow.RowsMove( 1, ll_RowsToMove, Primary!, adw_DataWindow, &
									 1, Filter!)

adw_DataWindow.RowsCopy( 1, 1, Primary!, adw_DataWindow, &
								adw_DataWindow.FilteredCount() + 1, Filter!)

ll_RowsToMove = adw_DataWindow.RowCount()

IF ll_RowsToMove > 1 Then &
	adw_DataWindow.RowsMove( 2, ll_RowsToMove, Primary!, adw_DataWindow, &
									 adw_DataWindow.FilteredCount() + 1, Filter!)
	

ls_Return_String = adw_DataWindow.Describe("DataWindow.Data")

adw_DataWindow.RowsDiscard( 1,1, Primary!)


adw_DataWindow.RowsMove( 1, adw_DataWindow.FilteredCount(), Filter!,&
								 adw_DataWindow, 1, Primary!)

adw_DataWindow.SetRedraw( True)

Return ls_Return_String


end function

public subroutine uf_fill_inc_exc (string as_fill_string);DataWindowChild	dwc_temp


dw_detail.GetChild('inc_exc_ind', dwc_temp)
If IsValid(dwc_temp) Then
	dwc_temp.ImportString(as_fill_string)
	dwc_temp.Resetupdate()
End if

end subroutine

public subroutine uf_change_plants ();//
//
//FOR ll_count = 1 TO dw_detail.RowCount()
//	//????
//	dw_detail.GetChild("inc_exc_ind",ldwc_plant)
//	ldwc_plant.SetFilter( "#1 = '" + This.GetItemString(This.GetRow(),'customer_id') + "'")
//	ldwc_plant.Filter()
//	ldwc_plant.SetSort ( '#2 A' )
//	ldwc_plant.Sort()
//
//	If dw_detail.GetItemString(ll_count,'inc_exc_ind') = "I" Then
//		Delete all plants info
//		Set plants info = inc plants
//	Else
//		If dw_detail.GetItemString(ll_count,'inc_exc_ind') = "E" Then
//			Delete the plants that are excluded
//		End If
//	End if
//NEXT
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//This.GetChild("inc_exc_ind",ldwc_plant)
//ldwc_plant.SetFilter( "#1 = '" + This.GetItemString(This.GetRow(),'customer_id') + "'")
//ldwc_plant.Filter()
//ldwc_plant.SetSort ( '#2 A' )
//ldwc_plant.Sort()
//
//If This.GetItemString(This.Getrow(),'inc_exc_ind') = "I" Then
//	
//
//
//
//This.GetChild("plant",ldwc_plant)
//ls_plant = This.GetItemString(This.GetRow(),"plant")
//ldwc_plant.Sort()
//ldwc_plant.ScrollToRow(ldwc_plant.Find("location_code = '" + ls_plant + "'",1,ldwc_plant.RowCount()))
//If This.DataObject = 'd_reservation_change_pa' Then
//	This.GetChild("change_plant_code",ldwc_plant)
//	ls_plant = This.GetItemString(This.GetRow(),"change_plant_code")
//	ldwc_plant.Sort()
//	ldwc_plant.ScrollToRow(ldwc_plant.Find("location_code = '" + ls_plant + "'",1,ldwc_plant.RowCount()))
//End if
//
end subroutine

on u_reservation_review_tab.create
this.cbx_showall=create cbx_showall
this.st_showall=create st_showall
this.st_lines=create st_lines
this.dw_header=create dw_header
this.dw_detail=create dw_detail
this.Control[]={this.cbx_showall,&
this.st_showall,&
this.st_lines,&
this.dw_header,&
this.dw_detail}
end on

on u_reservation_review_tab.destroy
destroy(this.cbx_showall)
destroy(this.st_showall)
destroy(this.st_lines)
destroy(this.dw_header)
destroy(this.dw_detail)
end on

type cbx_showall from checkbox within u_reservation_review_tab
integer x = 2437
integer y = 4
integer width = 91
integer height = 72
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
end type

event clicked;If is_filtered ="Y" Then
	uf_showall(True)
Else
	uf_showall(False)
End If

end event

type st_showall from statictext within u_reservation_review_tab
integer x = 2519
integer y = 4
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Show All"
boolean focusrectangle = false
end type

event clicked;cbx_showall.Event Clicked()
end event

type st_lines from statictext within u_reservation_review_tab
integer x = 2523
integer y = 56
integer width = 247
integer height = 76
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
string text = "Lines"
boolean focusrectangle = false
end type

event clicked;cbx_showall.Event Clicked()
end event

type dw_header from u_netwise_dw within u_reservation_review_tab
integer x = 9
integer width = 2779
integer height = 336
integer taborder = 10
string dataobject = "d_reservation_header"
boolean border = false
end type

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_customer_name, &
						ldwc_customer_city, &
						ldwc_salesman_name, &
						ldwc_salesman_loc, &
						ldwc_sales_locations
DataWindowChild	ldwc_trans

This.GetChild('trans_mode', ldwc_trans)
ldwc_trans.SetTransObject(SQLCA)
ldwc_trans.Retrieve("TRANMODE")

w_reservation_review_scheduling	lw_parent
lw_parent = Parent.GetParent().GetParent()

This.GetChild('sales_person_name', ldwc_salesman_name)
This.GetChild('sales_location_code', ldwc_salesman_loc)
This.GetChild('service_center_name',ldwc_sales_locations)
	
lw_parent.ids_salespeople.ShareData(ldwc_salesman_name)
lw_parent.ids_salespeople.ShareData(ldwc_salesman_loc)
lw_parent.ids_servicecenters.ShareData(ldwc_sales_locations)

This.GetChild('customer_name', ldwc_customer_name)
This.GetChild('customer_city', ldwc_customer_city)

lw_parent.ids_customer.ShareData(ldwc_customer_name)
lw_parent.ids_customer.ShareData(ldwc_customer_city)


end event

event constructor;call super::constructor;This.InsertRow(0)

This.SetItem(1, 'fields_in_error', Fill('V', 20))
end event

type dw_detail from u_netwise_dw within u_reservation_review_tab
event ue_dwndropdown pbm_dwndropdown
integer x = 32
integer y = 336
integer width = 2761
integer height = 924
integer taborder = 20
string dataobject = "d_reservation_long_pa"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dwndropdown;DataWindowChild ldwc_plant
String ls_plant
Long ll_row

// ibdkdld

//uf_filter_inc_exc()
This.GetChild("inc_exc_ind",ldwc_plant)
ldwc_plant.SetFilter( "#1 = '" + This.GetItemString(This.GetRow(),'customer_id') + "'")
ldwc_plant.Filter()
ldwc_plant.SetSort ( '#2 A' )
ldwc_plant.Sort()



This.GetChild("plant",ldwc_plant)
ls_plant = This.GetItemString(This.GetRow(),"plant")
ldwc_plant.Sort()
ldwc_plant.ScrollToRow(ldwc_plant.Find("location_code = '" + ls_plant + "'",1,ldwc_plant.RowCount()))
If This.DataObject = 'd_reservation_change_pa' Then
	This.GetChild("change_plant_code",ldwc_plant)
	ls_plant = This.GetItemString(This.GetRow(),"change_plant_code")
	ldwc_plant.Sort()
	ldwc_plant.ScrollToRow(ldwc_plant.Find("location_code = '" + ls_plant + "'",1,ldwc_plant.RowCount()))
End if
		
end event

event itemerror;call super::itemerror;
// If change_ship_date is spaces, allow it to be empty.
// The text color is changed to be the same as the background color.
IF Trim(data) = "" then
	return 2
end if

return 1
end event

event itemchanged;call super::itemchanged;DataWindowChild		ldwc_Plant,& 
							ldwc_inc

Long		ll_row

Integer	li_ship_date_id

String	ls_errors, &
			ls_UpdateFlag, &
			ls_detail_errors,ls_inc_exc,ls_plant


ll_row = This.GetRow()
If dwo.name = 'inc_exc_ind' Then
	Return 2
End If


//ibdkdld	
IF dwo.name = 'plant' or dwo.name = 'change_plant_code' Then
	This.GetChild('plant', ldwc_plant)
	IF ldwc_Plant.Find( "location_code = '" +This.GetText()+"'", 1 ,&
							 ldwc_Plant.RowCount() ) < 1 Then
		iw_frame.SetMicroHelp("Invalid plant, please enter a valid plant")
		Return 1
	Else
		ls_plant = data
		ls_inc_exc = Trim(This.GetitemString(row,'inc_exc_ind'))
		This.GetChild('inc_exc_ind', ldwc_inc)
		ldwc_inc.SetFilter( "#1 = '" + This.GetItemString(Row,'customer_id') + "'")
		ldwc_inc.Filter()
		ldwc_inc.SetSort ( '#2 A' )
		ldwc_inc.Sort()
		If ls_inc_exc = 'I' Then
			IF ldwc_inc.Find( "plant = '" +ls_plant+"'", 1 , ldwc_inc.RowCount() ) < 1 Then
				iw_frame.SetMicroHelp("Invalid plant, This plant is not an Included plant")
				Return 1
			Else
				iw_frame.SetMicroHelp("Ready")
			End If
		Else
			If ls_inc_exc = 'E' Then
				IF ldwc_inc.Find( "plant = '" +ls_plant+"'", 1 , ldwc_inc.RowCount() ) > 0 Then
					iw_frame.SetMicroHelp("Invalid plant, This plant is an Excluded plant")
					Return 1
				Else
					iw_frame.SetMicroHelp("Ready")
				End If
			Else
				iw_frame.SetMicroHelp("Ready")
			End If
		End If			
	END IF
END iF

IF This.GetColumnName() = 'ordered_units'Then
	IF Dec(This.GetText())  < 0 Then
		iw_frame.SetMicroHelp("Ordered units cannot be negative")
		Return 1
	ELSE
		iw_frame.SetMicroHelp("Ready")
	END IF
END IF	


IF Long(This.Describe("update_flag.id")) > 0 THEN
	// This is specific for this window.  Since the only way 
	//	to add rows is through splitting them, and splitting them 
	// already puts the 'A' there, everything else must be an
	// existing row, and thus have a status of 'U'
	If This.GetItemString(ll_Row, "update_flag") <> 'A' Then
		This.SetItem	(ll_Row, "update_flag", "U")
	End if
END IF

IF This.GetColumnName() = 'change_ship_date'Then
	li_ship_date_id = Integer(This.object.scheduled_ship_date.id)
	ls_detail_errors = This.GetItemString(ll_row, 'detail_errors')
	If Mid(ls_detail_errors, li_ship_date_id, 1) = 'E' Then
		ls_errors = Replace(ls_detail_errors, &
				li_ship_date_id, 1, 'V')
		This.SetItem(ll_row, 'detail_errors', ls_errors)
	End If
END IF	


ls_errors = Replace(This.GetItemString(ll_row, 'detail_errors'), This.GetColumn(), 1, 'M')
ib_modified = True

This.SetItem(ll_row, 'detail_errors', ls_errors)

end event

event constructor;call super::constructor;ib_updateable = True
is_Selection = '3'
ib_firstcolumnonnextrow = FALSE

end event

event clicked;String	ls_ColumnName

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

IF ls_ColumnName = "line_number" Then	
	Call Super::Clicked
ELSE
	This.SelectRow(0,False)

END IF


end event

event doubleclicked;call super::doubleclicked;Long		ll_ClickedRow,&
			ll_RowCount,&
			ll_LoopCount

String	ls_ClickedColumnName,&
			ls_ReturnString, &
			ls_temp2
			
Date		ld_null_date

ll_ClickedRow = row
IF ll_ClickedRow < 1 Then Return

IF dwo.Type = "column" THEN
	ls_ClickedColumnName = dwo.Name
END IF

// If Order is already accepted, don't allow them to split
If dw_header.GetItemString(1, 'line_status') = 'A' Then return
// If Order is not a new reservation, don't allow them to split
If is_order_status <> 'N' Then return

IF ls_ClickedColumnName  = "line_number" Then
	// First set "spaced-out" fields to NULL
	SetNull(ld_null_date)
	ls_temp2=String(dw_detail.GetItemDate(ll_ClickedRow, 'change_ship_date'), &
				"yyyy-mm-dd")
	IF NOT IsDate(ls_temp2) THEN
		dw_detail.SetItem(ll_ClickedRow,'change_ship_date',ld_null_date)
	END IF
	
	OpenWithParm( w_split_response, uf_describerow( ll_ClickedRow, dw_detail ))
	ls_ReturnString = Message.StringParm
	IF ls_ReturnString <> "Cancel" Then
		dw_Detail.RowsMove( ll_ClickedRow +1, dw_detail.RowCount(), Primary!,&
									dw_detail, 1, Filter!)
		dw_Detail.RowsDiscard( ll_ClickedRow, ll_ClickedRow, Primary!)
		ll_RowCount = dw_detail.ImportString(	ls_ReturnString) + ll_ClickedRow
		// we know that the row was originally there.  Since Scheduling can't add rows.
		dw_Detail.SetItem( ll_ClickedRow, "update_flag", 'U')
		dw_Detail.uf_ChangeRowStatus(ll_ClickedRow, DataModified!)
		For ll_LoopCount = ll_ClickedRow + 1 to ll_RowCount
			dw_Detail.SetItem( ll_LoopCount, "line_number", 0)
			dw_Detail.SetItem( ll_LoopCount, "update_flag", 'A')
		Next
		dw_Detail.RowsMove(  1, dw_detail.FilteredCount(), Filter!,&
									dw_detail, dw_detail.RowCount()+1, Primary!)
	END IF
	dw_detail.ScrollToRow(ll_ClickedRow)
END IF


end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1, Len(This.GetText()))
end event

event rbuttondown;call super::rbuttondown;m_reservation_review_popup	lm_PopMenu
String	ls_ColName, &
			ls_errors
Long		ll_Row

ls_ColName = Left(is_ObjectAtPointer,Pos(is_ObjectAtPointer,"~t") -1)
ll_Row = Long(Mid(is_ObjectAtPointer,Pos(is_ObjectAtPointer,"~t") +1))
If ll_row < 1 Then return
ls_errors = This.GetItemString(ll_row,"detail_errors")

Choose Case ls_ColName
	Case "plant"
		If Mid(ls_errors,7,1) = 'V' Then return		
	Case "change_plant_code"
		If Mid(ls_errors,15,1) = 'V' Then return
	Case Else
		return
End Choose

lm_PopMenu = Create m_reservation_review_popup
lm_PopMenu.m_file.PopMenu(iw_frame.PointerX(), iw_Frame.PointerY())
Destroy lm_PopMenu

end event

