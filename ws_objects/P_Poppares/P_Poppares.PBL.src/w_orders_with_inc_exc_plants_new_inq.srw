﻿$PBExportHeader$w_orders_with_inc_exc_plants_new_inq.srw
forward
global type w_orders_with_inc_exc_plants_new_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_orders_with_inc_exc_plants_new_inq
end type
type dw_dates from u_begin_end_dates_order_new within w_orders_with_inc_exc_plants_new_inq
end type
end forward

global type w_orders_with_inc_exc_plants_new_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1655
integer height = 544
string title = "Orders with inc exc plants new inq"
dw_plant dw_plant
dw_dates dw_dates
end type
global w_orders_with_inc_exc_plants_new_inq w_orders_with_inc_exc_plants_new_inq

type variables
w_orders_with_inc_exc_plants_new	iw_Parent_Window
end variables

on w_orders_with_inc_exc_plants_new_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_dates=create dw_dates
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_dates
end on

on w_orders_with_inc_exc_plants_new_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_dates)
end on

event ue_postopen;call super::ue_postopen;Date	ldt_temp
int ii_PAPast,ii_Query_Retry_Count,li_PARange
String ls_PARange


//ldt_temp = Date(iw_parent_window.dw_begin_end_date.uf_GetBeginDate())
//dw_dates.Modify("begin_date.Protect = 0")
//dw_dates.Modify("end_date.Protect = 0")

If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "FUTURE", ls_PARange) = -1 Then
	if (sqlca.sqldbcode = 10005 or sqlca.sqldbcode = 10025) &
		and ii_Query_Retry_Count < 2 then
		
		ii_Query_Retry_Count += 1
		SQLCA.postevent("ue_reconnect")
		this.postevent("ue_query")
		return
	end if

	// Row not found
	MessageBox("PA Range", "Future PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
li_PARange = Integer(ls_PARange)



ldt_temp = Date(dw_dates.uf_Get_End_Date())
dw_dates.SetItem( 1, "begin_date",now())
 dw_dates.uf_Set_End_Date(RelativeDate(Today(), li_PARange))
//dw_dates.SetItem( 1, "end_date",now())
end event

event ue_base_cancel;call super::ue_base_cancel;Close(this)
end event

event ue_base_ok;call super::ue_base_ok;string ls_plant_code,&
		ls_begin_date,&
		ls_end_date,ls_params,ls_location_name
		
Date ld_begin_date,ld_end_date
		
		
	If dw_plant.AcceptText() = -1 then return
	dw_dates.AcceptText()
	
ls_plant_code = dw_plant.uf_get_plant_code()
ls_location_name = dw_plant.GetItemString(1,"location_name")
ld_begin_Date	= dw_dates.GetItemDate( 1, "begin_date" )
ld_end_date		= dw_dates.GetItemDate( 1, "end_date" )

ls_begin_date = String(ld_begin_date)
ls_end_date = String(ld_end_date)


If ld_end_date < ld_begin_date Then
	iw_frame.SetMicroHelp("End Date must be greater than Begin Date")
	dw_dates.SetColumn("end_date")
	dw_dates.SetFocus()
	dw_dates.TriggerEvent(ItemError!)
	Return
End If


ls_params = ls_plant_code +'<'+ ls_location_name+'<'+ls_begin_date + '<'+ ls_end_date


closewithreturn(this, ls_params) 
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_orders_with_inc_exc_plants_new_inq
integer x = 2423
integer y = 1352
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_orders_with_inc_exc_plants_new_inq
integer x = 1349
integer y = 340
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_orders_with_inc_exc_plants_new_inq
integer x = 841
integer y = 340
end type

type dw_plant from u_plant within w_orders_with_inc_exc_plants_new_inq
integer x = 14
integer y = 16
integer width = 1609
integer taborder = 10
boolean bringtotop = true
end type

type dw_dates from u_begin_end_dates_order_new within w_orders_with_inc_exc_plants_new_inq
integer x = 18
integer y = 120
integer width = 1605
integer height = 212
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_pas_begin_end_date"
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;this.AcceptText()
end event

