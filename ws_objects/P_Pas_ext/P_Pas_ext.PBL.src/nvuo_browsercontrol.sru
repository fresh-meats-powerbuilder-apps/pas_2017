﻿$PBExportHeader$nvuo_browsercontrol.sru
forward
global type nvuo_browsercontrol from nonvisualobject
end type
end forward

global type nvuo_browsercontrol from nonvisualobject autoinstantiate
end type

type prototypes
Function long ShellExecute( long hwnd,  string lpOperation, string lpFile, string lpParameters,  string lpDirectory,  integer nShowCmd) Library "shell32.dll" alias for "ShellExecuteW" 
end prototypes

type variables
Public:
Constant String is_browser_default = "default"
Constant String is_browser_chrome = "chrome"
Constant String is_browser_firefox = "firefox"
Constant String is_browser_edge = "edge"
Constant String is_browser_internetexplorer = "iexplore"
end variables

forward prototypes
public subroutine of_open_link (string as_url, string as_browser)
private function window of_get_parentwindow ()
public function boolean of_is_browser_installed (string as_browser)
end prototypes

public subroutine of_open_link (string as_url, string as_browser);// Arguments:	
// as_url			URL to open
// as_browser	is_browser_default, is_browser_chrome, is_browser_firefox, is_browser_edge or is_browser_internetexplorer
//
// Example: lnvuo_browsercontrol.of_open_link("http://www.google.de",lnvuo_browsercontrol.is_browser_chrome)
string 	ls_null, ls_exe
setnull(ls_null)

choose case as_browser
	case is_browser_default
		ShellExecute( Handle(of_get_parentwindow()), 'Open', as_url, ls_null, "", 1)
	case is_browser_chrome, is_browser_firefox, is_browser_internetexplorer
		RegistryGet("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\"+as_browser+".exe","", RegString!, ls_exe)
		if not isnull(ls_exe) and ls_exe <> "" then
			if fileexists(ls_exe) then
				run(ls_exe+" "+ as_url)
			end if
		end if
	case is_browser_edge
		ShellExecute( Handle(of_get_parentwindow()), 'Open', "microsoft-edge:"+as_url, ls_null, "", 1)
end choose
end subroutine

private function window of_get_parentwindow ();
POWEROBJECT parent_obj 

parent_obj = this.GetParent() 
DO WHILE IsValid (parent_obj) 
	IF parent_obj.TypeOf() <> Window! THEN 
		parent_obj = parent_obj.GetParent() 
	ELSE 
		EXIT 
	END IF 
LOOP 

if isValid( parent_obj ) = false then
	SetNull( parent_obj )
end if

return parent_obj
end function

public function boolean of_is_browser_installed (string as_browser);// Arguments:	
// as_browser	 is_browser_chrome, is_browser_firefox or is_browser_internetexplorer
//
// Example: lnvuo_browsercontrol.of_browser_exists(lnvuo_browsercontrol.is_browser_chrome)

boolean 	lb_exists
string		ls_exe

choose case as_browser
	case is_browser_chrome, is_browser_firefox, is_browser_internetexplorer
		RegistryGet("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\"+as_browser+".exe","", RegString!, ls_exe)
		if not isnull(ls_exe) and ls_exe <> "" then
			if fileexists(ls_exe) then
				lb_exists = true
			end if
		end if
end choose

return lb_exists
end function

on nvuo_browsercontrol.create
call super::create
TriggerEvent( this, "constructor" )
end on

on nvuo_browsercontrol.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

