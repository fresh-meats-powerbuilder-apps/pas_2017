﻿$PBExportHeader$w_plant_date_inq.srw
forward
global type w_plant_date_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_plant_date_inq
end type
type dw_begin_end_dates from u_begin_end_dates within w_plant_date_inq
end type
type cbx_production_buffer from checkbox within w_plant_date_inq
end type
type uo_div_group_options from u_pa_neg_div_prod_grp within w_plant_date_inq
end type
end forward

global type w_plant_date_inq from w_base_response_ext
integer width = 2505
integer height = 1268
string title = "Display PA Negatives Inquire"
long backcolor = 67108864
event ue_postconstructor ( )
dw_plant dw_plant
dw_begin_end_dates dw_begin_end_dates
cbx_production_buffer cbx_production_buffer
uo_div_group_options uo_div_group_options
end type
global w_plant_date_inq w_plant_date_inq

type variables
Boolean		IsValidReturn

Window		iw_parent_window

DataWindowChild	idwc_parent_plant

end variables

event ue_postconstructor;
IF iw_parentwindow.title = 'Display PA Negatives Inquire' Then
	dw_begin_end_dates.uf_set_date_type('PANEG')
Else
	dw_begin_end_dates.uf_set_date_type('PARANGE')
End If
end event

on w_plant_date_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_begin_end_dates=create dw_begin_end_dates
this.cbx_production_buffer=create cbx_production_buffer
this.uo_div_group_options=create uo_div_group_options
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_begin_end_dates
this.Control[iCurrent+3]=this.cbx_production_buffer
this.Control[iCurrent+4]=this.uo_div_group_options
end on

on w_plant_date_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_begin_end_dates)
destroy(this.cbx_production_buffer)
destroy(this.uo_div_group_options)
end on

event open;call super::open;iw_parent_window = Message.PowerObjectParm
If Not IsValid(iw_parent_window) Then 
	Close(This)
	return
End if

This.Title = iw_parent_window.Title + " Inquire"


If iw_parent_window.Title = 'Display PA Negatives' Then 
	cbx_production_buffer.Show()
Else
	cbx_production_buffer.Hide()
End If
	

end event

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_plant
Integer				li_PA_Range

iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(message.StringParm)

iw_parentwindow.Event ue_get_data('begin_date')
dw_begin_end_dates.uf_set_begin_date(Date(message.StringParm))

iw_parentwindow.Event ue_get_data('end_date')
dw_begin_end_dates.uf_set_end_date(Date(message.StringParm))

If iw_parent_window.Title = 'Display PA Negatives' Then 
	iw_parentwindow.Event ue_get_data('buffer')
	If message.stringparm = 'Y'Then
		cbx_production_buffer.checked = True
	Else
		cbx_production_buffer.checked = False
	End If
End If

//  RevGLL *(Begin)
iw_parentwindow.Event ue_get_data('division')
uo_div_group_options.uf_set_division(Message.StringParm)

// set owner first then set selected
iw_parentwindow.Event ue_get_data('product_owner')
//uo_div_group_options.uf_set_owner(Message.StringParm)

iw_parentwindow.Event ue_get_data('product_group_id')
//uo_div_group_options.uf_set_default_by_id(Message.StringParm)

iw_parentwindow.Event ue_get_data('inquire_options')
if isnull(message.stringParm) or Message.StringParm='' then
	uo_div_group_options.uf_set_inquire_options('A')
Else
	uo_div_group_options.uf_set_inquire_options(Message.StringParm)
end if
//messagebox(Message.StringParm,'')

uo_div_group_options.uf_setfocus('options')
//  RevGLL *(End)

dw_plant.SetFocus()
dw_plant.SelectText(1, 100)

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;Date		ldt_date

String	ls_plant, &
			ls_begin_date, &
			ls_end_date, &
			ls_prod_group_id, &
			ls_prod_group_desc, &
			ls_owner, &	
			ls_inquire, &
			ls_division, &
			ls_product_state, &
			ls_new_string, &
			ls_desc

Integer	li_rtn

Long		ll_string_len

u_String_Functions		lu_string

If dw_plant.AcceptText() < 0 Then Return
ls_plant = dw_plant.uf_get_plant_code()

If lu_string.nf_IsEmpty(ls_plant) Then 
	dw_plant.Setfocus()
	iw_frame.SetMicroHelp('Plant is a required field')
	Return
End If

ldt_date = dw_begin_end_dates.uf_get_begin_date()

ls_begin_date = String(ldt_date,"yyyy-mm-dd")
If lu_string.nf_IsEmpty(ls_begin_date) Then 
	dw_begin_end_dates.Setfocus()
	Return
End If

ldt_date = dw_begin_end_dates.uf_get_end_date()

ls_end_date = String(ldt_date,"yyyy-mm-dd")
If lu_string.nf_IsEmpty(ls_end_date) Then 
	dw_begin_end_dates.Setfocus()
	Return
End If

iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('begin_date',ls_begin_date)
iw_parentwindow.Event ue_set_data('end_date',ls_end_date)

If iw_parent_window.Title = 'Display PA Negatives' Then 
	If cbx_production_buffer.checked Then
		iw_parentwindow.Event ue_set_data('buffer','Y')
	Else
		iw_parentwindow.Event ue_set_data('buffer','N')
	End If
End If

//  RevGLL	*(Begin)

ls_inquire = uo_div_group_options.uf_get_inquire_options()
IF ls_inquire = 'D' Then
	ls_division = uo_div_group_options.uf_get_division()
	If lu_string.nf_IsEmpty(ls_division) Then
		iw_frame.SetMicroHelp("Division is a required field")
		uo_div_group_options.uf_SetFocus('division')
	return
	End If
	
	iw_parentwindow.Event ue_set_data('div_visible','True')
	iw_parentwindow.Event ue_set_data('division',ls_division)
	iw_parentwindow.Event ue_set_data('prod_group_visible','False')
	iw_parentwindow.Event ue_set_data('inquire_options',ls_inquire)
ELSEIF ls_inquire = 'P' Then 

		ls_owner = uo_div_group_options.uf_get_owner()
		If lu_string.nf_IsEmpty(ls_owner) Then
			iw_frame.SetMicroHelp("Product System is a required field")
			uo_div_group_options.uf_SetFocus('prodgrp')
		return
		End If
		
		li_rtn = uo_div_group_options.uf_get_sel_id(ls_prod_group_id)
		If li_rtn < 0 Then
			iw_frame.SetMicroHelp("Product Group is a required field")
			uo_div_group_options.uf_SetFocus('prodgrp')
			Return
		else
		
		li_rtn = uo_div_group_options.uf_get_sel_desc(ls_prod_group_desc)
		End if
		
		ls_new_string = ''
		Do 
			ls_desc = lu_string.nf_gettoken(ls_prod_group_desc,'~r~n')
			ls_new_string += Trim(ls_desc) + ', '
		Loop While Len(ls_prod_group_desc) > 0 
		ll_string_len = len(ls_new_string) - 2		
		ls_new_string = Left ( ls_new_string, ll_string_len)
		
		iw_parentwindow.Event ue_set_data('div_visible','False')
		iw_parentwindow.Event ue_set_data('prod_group_visible','True')
		iw_parentwindow.Event ue_set_data('product_owner',ls_owner)
		iw_parentwindow.Event ue_set_data('product_group_desc',ls_new_string)
		iw_parentwindow.Event ue_set_data('product_group_id',ls_prod_group_id)
		iw_parentwindow.Event ue_set_data('inquire_options',ls_inquire)
	ELSEIF ls_inquire = 'B' Then 
		
		ls_division = uo_div_group_options.uf_get_division()
		If lu_string.nf_IsEmpty(ls_division) Then
			iw_frame.SetMicroHelp("Division is a required field")
			uo_div_group_options.uf_SetFocus('division')
		return
		End If
		
		ls_owner = uo_div_group_options.uf_get_owner()
		If lu_string.nf_IsEmpty(ls_owner) Then
			iw_frame.SetMicroHelp("Product System is a required field")
			uo_div_group_options.uf_SetFocus('prodgrp')
		return
		End If
		
		li_rtn = uo_div_group_options.uf_get_sel_id(ls_prod_group_id)
		If li_rtn < 0 Then
			iw_frame.SetMicroHelp("Product Group is a required field")
			uo_div_group_options.uf_SetFocus('prodgrp')
			Return
		else
			li_rtn = uo_div_group_options.uf_get_sel_desc(ls_prod_group_desc)
		End if
		
		ls_new_string = ''
		Do 
			ls_desc = lu_string.nf_gettoken(ls_prod_group_desc,'~r~n')
			ls_new_string += Trim(ls_desc) + ', '
		Loop While Len(ls_prod_group_desc) > 0 
		ll_string_len = len(ls_new_string) - 2		
		ls_new_string = Left ( ls_new_string, ll_string_len)

		iw_parentwindow.Event ue_set_data('prod_group_visible','True')
		iw_parentwindow.Event ue_set_data('div_visible','True')
		iw_parentwindow.Event ue_set_data('division',ls_division)
		iw_parentwindow.Event ue_set_data('product_owner',ls_owner)
		iw_parentwindow.Event ue_set_data('product_group_desc',ls_new_string)
		iw_parentwindow.Event ue_set_data('product_group_id',ls_prod_group_id)
		iw_parentwindow.Event ue_set_data('inquire_options',ls_inquire)
	Else
		iw_parentwindow.Event ue_set_data('div_visible','False')
		iw_parentwindow.Event ue_set_data('prod_group_visible', 'False')
		iw_parentwindow.Event ue_set_data('inquire_options',ls_inquire)

END IF
//  RevGLL	*(End)

ib_ok_to_close = True

Close(This)

end event

event close;call super::close;Close(This)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_plant_date_inq
integer x = 1362
integer y = 992
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_plant_date_inq
integer x = 1079
integer y = 992
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_plant_date_inq
integer x = 795
integer y = 992
integer taborder = 40
end type

type dw_plant from u_plant within w_plant_date_inq
integer x = 229
integer taborder = 10
end type

event constructor;call super::constructor;This.Enable()
end event

type dw_begin_end_dates from u_begin_end_dates within w_plant_date_inq
event ue_postconstructor ( )
integer x = 119
integer y = 76
integer taborder = 20
end type

event ue_postconstructor;IF iw_parentwindow.title = 'Display PA Negatives Inquire' Then
	This.uf_set_date_type('PANEG')
Else
	This.uf_set_date_type('PARANGE')
End If
end event

event constructor;call super::constructor;This.InsertRow(0)
This.uf_Enable(True)

This.PostEvent('ue_postconstructor')
end event

type cbx_production_buffer from checkbox within w_plant_date_inq
integer x = 987
integer y = 100
integer width = 663
integer height = 76
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Remove Production Buffer:"
boolean checked = true
boolean lefttext = true
end type

type uo_div_group_options from u_pa_neg_div_prod_grp within w_plant_date_inq
integer x = 9
integer y = 264
integer taborder = 40
end type

on uo_div_group_options.destroy
call u_pa_neg_div_prod_grp::destroy
end on

