﻿$PBExportHeader$w_base_login_ext.srw
forward
global type w_base_login_ext from w_netwise_login
end type
end forward

global type w_base_login_ext from w_netwise_login
end type
global w_base_login_ext w_base_login_ext

on w_base_login_ext.create
call w_netwise_login::create
end on

on w_base_login_ext.destroy
call w_netwise_login::destroy
end on

type p_1 from w_netwise_login`p_1 within w_base_login_ext
boolean BringToTop=true
end type

