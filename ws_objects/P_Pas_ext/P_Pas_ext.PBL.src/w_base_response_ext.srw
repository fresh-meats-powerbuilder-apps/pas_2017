﻿$PBExportHeader$w_base_response_ext.srw
forward
global type w_base_response_ext from w_netwise_response
end type
end forward

global type w_base_response_ext from w_netwise_response
end type
global w_base_response_ext w_base_response_ext

type variables
w_base_sheet		iw_parentwindow
Boolean			ib_ok_to_close
end variables

on w_base_response_ext.create
call super::create
end on

on w_base_response_ext.destroy
call super::destroy
end on

event open;call super::open;If IsValid(message.powerobjectparm) Then
	If message.powerobjectparm.TypeOf() = window! Then
		iw_parentwindow = message.powerobjectparm
	End If
End If
end event

event ue_postopen;If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_get_data('title')
	This.Title = Message.StringParm + ' Inquire'
End If

ib_ok_to_close = False
end event

event ue_base_ok;call super::ue_base_ok;ib_ok_to_close = True
end event

event ue_base_cancel;call super::ue_base_cancel;//IF IsValid(This) Then Close(This)
ib_ok_to_close = False

end event

event close;call super::close;String			ls_setvalue


If ib_ok_to_close Then
	ls_setvalue = 'True'
Else
	ls_setvalue = 'False'
End If

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_set_data('ib_inquire', ls_setvalue)
End IF
		
end event

type cb_base_help from w_netwise_response`cb_base_help within w_base_response_ext
int TabOrder=30
end type

type cb_base_ok from w_netwise_response`cb_base_ok within w_base_response_ext
FontCharSet FontCharSet=Ansi!
end type

