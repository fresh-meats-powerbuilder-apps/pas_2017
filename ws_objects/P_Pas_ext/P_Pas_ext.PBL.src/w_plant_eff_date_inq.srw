﻿$PBExportHeader$w_plant_eff_date_inq.srw
forward
global type w_plant_eff_date_inq from w_base_response_ext
end type
type dw_effective_date from u_effective_date within w_plant_eff_date_inq
end type
type dw_plant from u_plant within w_plant_eff_date_inq
end type
end forward

global type w_plant_eff_date_inq from w_base_response_ext
int Width=1742
int Height=592
long BackColor=12632256
dw_effective_date dw_effective_date
dw_plant dw_plant
end type
global w_plant_eff_date_inq w_plant_eff_date_inq

type variables

end variables

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(message.StringParm)

iw_parentwindow.Event ue_get_data('effective_date')
dw_effective_date.uf_set_effective_date(Date(message.StringParm))

dw_plant.SetFocus()
end event

on w_plant_eff_date_inq.create
int iCurrent
call super::create
this.dw_effective_date=create dw_effective_date
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_effective_date
this.Control[iCurrent+2]=this.dw_plant
end on

on w_plant_eff_date_inq.destroy
call super::destroy
destroy(this.dw_effective_date)
destroy(this.dw_plant)
end on

event close;call super::close;Close(This)
end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String	ls_plant, &
			ls_effective_date
Date		ldt_effective_date

u_string_functions		lu_strings


If dw_plant.AcceptText() < 0 Then Return
ls_plant = dw_plant.uf_get_plant_code()

If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp('Plant is a required field')
	dw_plant.SetFocus()
	return
End if
	
ldt_effective_date = dw_effective_date.uf_get_effective_date()
ls_effective_date = String(ldt_effective_date, 'yyyy-mm-dd')

If Not IsDate(ls_effective_date) Then
	iw_frame.SetMicroHelp('Effective Date is a required field')
	dw_plant.SetFocus()
	return
End if
	
iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('effective_date',ls_effective_date)

ib_ok_to_close = True

Close(This)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_plant_eff_date_inq
int X=974
int Y=280
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_plant_eff_date_inq
int X=681
int Y=280
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_plant_eff_date_inq
int X=389
int Y=280
int TabOrder=20
end type

type dw_effective_date from u_effective_date within w_plant_eff_date_inq
int X=14
int Y=136
int Width=773
int Height=84
int TabOrder=10
end type

event constructor;call super::constructor;This.InsertRow(0)
This.uf_Enable(True)
end event

type dw_plant from u_plant within w_plant_eff_date_inq
int X=233
int Y=48
int Width=1445
int TabOrder=2
end type

event constructor;call super::constructor;This.Enable()
end event

