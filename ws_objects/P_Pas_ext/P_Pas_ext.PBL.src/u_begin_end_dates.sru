﻿$PBExportHeader$u_begin_end_dates.sru
$PBExportComments$IBDKDLD
forward
global type u_begin_end_dates from datawindow
end type
end forward

global type u_begin_end_dates from datawindow
integer width = 709
integer height = 200
integer taborder = 1
string dataobject = "d_begin_end_dates"
boolean border = false
boolean livescroll = true
event ue_keydown pbm_dwnkey
event ue_revision ( )
end type
global u_begin_end_dates u_begin_end_dates

type variables
Date		idt_parange_end_date

Integer		ii_PA_Range

//Type of date function
String	is_date_type
	
// Datawindow Row Selection (0,1,2,3)
// 	0 - No rows selected (default)
//	1 - One row selected
//	2 - Multiple rows selected
//	3 - Multiple rows with CTRL and ALT support
string	is_selection= "0"

// Does datawindow have update capabilities
boolean	ib_Updateable

// Reset Datawindow on insert of new row
//	TRUE - Reset datawindow on Insert
// 	FALSE- Do not Reset DW on Insert (default)
boolean	ib_ResetOnInsert

//used to set the top row in datawindow as current row
boolean	ib_ScrollSetsCurrentRow
// Parent window established in constructor event
window	iw_parentwindow

// Contains the row number of the current row
long	il_dwRow

// Holds the number of the last clicked row
// Used for selecting rows with Shift Key
long	il_last_clicked_row

// Draggable Datawindow
// Need to issue Drag(Begin!) in clicked event
// ue_lbuttonup automatically turns Drag Off
boolean ib_draggable

// Holds the name of the object at pointer the user clicked on
string	is_ObjectAtPointer

// Holds the name of the band the user clicked in
string	is_BandAtPointer

// Holds the name of the current column
string	is_ColumnName

// Set this variable to Move the focus to the first column 
// in next row when enter is hit
boolean	ib_firstcolumnonnextrow

// Enable scrolling in the datawindow
// FALSE	- Do not allow scrolling
// TRUE	- Allow scrolling (default)
boolean	ib_scrollable = TRUE

//nonvisual used for find replace dialog boxes
u_findreplace	inv_find

end variables

forward prototypes
public function date uf_get_begin_date ()
public function date uf_get_end_date ()
public function Integer uf_modified ()
public function Integer uf_set_begin_date (date adt_begin_date)
public function integer uf_set_end_date (date adt_end_date)
public function integer uf_set_date_type (string as_date_type)
public function integer uf_get_pa_range ()
public function integer uf_enable (boolean ab_enable)
end prototypes

event ue_keydown;INTEGER		li_Column, &
				li_ColumnCount


long			ll_CurrentRow, &
				ll_CurrentColumn
				
String		ls_date, &
				ls_ColumnType

str_parms	lstr_parms

CHOOSE CASE TRUE
CASE	KeyDown(KeyEnter!)
	IF NOT ib_firstcolumnonnextrow THEN RETURN
	IF il_dwrow < This.RowCount() THEN
		li_ColumnCount = INTEGER(This.Describe("DataWindow.Column.Count"))
		FOR li_Column = 1 TO li_ColumnCount
			IF This.Describe("#" + STRING(li_Column) + ".TabSequence") = "10" THEN
				This.SetColumn(li_Column)
			END IF
		NEXT
	END IF
CASE	Keydown(KeyControl!) and Keydown(KeyDownArrow!)
	IF This.Describe("DataWindow.ReadOnly") = 'yes' THEN RETURN 
	
	// Get the current row and column that has focus
			ll_CurrentRow = This.GetRow()
			ll_CurrentColumn = This.GetColumn()
			
	// Get the current column name
			is_ColumnName = GetColumnName()

	// Get the column type of the clicked field
			ls_ColumnType = Lower(This.Describe("#" + String( &
												ll_CurrentColumn) + ".ColType"))

//	// Get the X and Y coordinates of the place that was clicked
lstr_parms.integer_arg[1] = iw_parentwindow.PointerX() + iw_parentwindow.WorkSpaceX() - 50
CHOOSE CASE	lstr_parms.integer_arg[1]
CASE IS > 2253
	lstr_parms.integer_arg[1] = 2253
CASE is < 113
	lstr_parms.integer_arg[1] = 113
END CHOOSE

lstr_parms.integer_arg[2] = iw_parentwindow.PointerY() + iw_parentwindow.WorkSpaceY() - 500
CHOOSE CASE	lstr_parms.integer_arg[2]
CASE IS > 1053
	lstr_parms.integer_arg[2] = 1053
CASE is < 0
	lstr_parms.integer_arg[2] = 0
END CHOOSE

	IF ls_ColumnType = "date" or &
		ls_ColumnType = "datetime" THEN
			
			Choose Case ls_ColumnType
				Case "date"
					lstr_parms.date_arg[1] = This.GetItemDate(ll_CurrentRow, &
																	ll_CurrentColumn)
					IF IsNull(lstr_parms.date_arg[1]) THEN
						lstr_parms.date_arg[1] = Today()
					END IF
					OpenWithParm(w_Calendar, lstr_parms)
					// Get the return string (date) from the calendar window
					// If an empty string do not do anything
					ls_date = Message.StringParm
// If you hit cntl downarrow the calendar pops up.  When the datawindow gets
// focus it scrolls to the next row.
// This bug has been fixed, however, it was determined to be more trouble than it was
// worth.  So, If it has been decided to implement, uncomment the Down arrow bug
// below.
//downarrow bug					This.SetRedraw(False)
					If ls_date <> "" Then
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						SetText(ls_Date)
					Else
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						Return
					End If
					
				Case "datetime"
					lstr_parms.date_arg[1] = Date(This.GetItemDateTime(ll_CurrentRow, &
																		ll_CurrentColumn))
					OpenWithParm(w_Calendar, lstr_parms)
					// Get the return string (date) from the calendar window
					// If an empty string do not do anything
					ls_date = Message.StringParm
						This.SetRedraw(False)
					If ls_date <> "" Then
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						SetText(ls_Date)
					Else
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						Return
					End If
				End Choose
	END IF
CASE ELSE

	// Used to not allow scrolling of a datawindow
	// If scroll ins allowed then return out of script
	If ib_scrollable Then Return

	// Turn the vertical scroll bar off
	vscrollbar=False

		IF (KeyDown(KeyTab!))			or &
			(KeyDown(KeyEnter!))			or &
			(KeyDown(KeyDownArrow!))	or &
		 	(KeyDown(KeyUpArrow!))		or &
		 	(KeyDown(KeyPageDown!))		or &
		 	(KeyDown(KeyPageUp!))			THEN
			 	// IBDKEEM ** 07/30/2002 ** Removed the Im here egg and SetRedraw(False)
				//MessageBox("","Im here")
				//SetRedraw(False)								
				PostEvent("ue_no_scroll")
		END IF
END CHOOSE

end event

event ue_revision;/*****************************************************************
**   REVISION NUMBER: rev#01
**   PROJECT NUMBER:  support
**   DATE:				 August 1999            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Added ue_revisions,added ue_keydown to get 
**                    the calander to be accessable by the keyboard.
******************************************************************
**   REVISION NUMBER: rev#02
**   PROJECT NUMBER:  support
**   DATE:				 August 1999            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Added a if check to the doubleclicked event
**                    to fix a bug.
******************************************************************
**   REVISION NUMBER: rev#03
**   PROJECT NUMBER:  support
**   DATE:				 September 1999            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Added code to uf_set_date_type to fix a bug with
**                    PA Range end date.
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

public function date uf_get_begin_date ();Date				ldt_begin_date

If This.AcceptText() = -1 then 
	This.SetFocus()
	SetNull(ldt_begin_date)
	return ldt_begin_date
End if

ldt_begin_date = This.GetItemDate(1, 'begin_date')

Return ldt_begin_date
end function

public function date uf_get_end_date ();Date				ldt_end_date

If This.AcceptText() = -1 then 
	This.SetFocus()
	SetNull(ldt_end_date)
	return ldt_end_date
End if

ldt_end_date = This.GetItemDate(1, 'end_date')

Return ldt_end_date
end function

public function Integer uf_modified ();Return 0
end function

public function Integer uf_set_begin_date (date adt_begin_date);Long	ll_row, &
		ll_row_count

String	ls_text

This.SetItem(1,"begin_date",Date(String(adt_begin_date, 'yyyy-mm-dd')))
This.SetFocus()
This.SelectText(1, Len(String(adt_begin_date)) + 5)

return 0
end function

public function integer uf_set_end_date (date adt_end_date);Long	ll_row, &
		ll_row_count

String	ls_text

This.SetItem(1,"end_date",Date(String(adt_end_date, 'yyyy-mm-dd')))
This.SetFocus()
This.SelectText(1, Len(String(adt_end_date)) + 5)

return 0
end function

public function integer uf_set_date_type (string as_date_type);If Upper(as_date_type) = 'PARANGE' then
	is_date_type = 'PARANGE'
// get the number of valid days for PA
  SELECT tutltypes.type_short_desc  
    INTO :ii_PA_Range  
    FROM tutltypes  
   WHERE ( tutltypes.type_code = 'PA RANGE' ) AND  
         ( tutltypes.record_type = 'PA RANGE' )   ;
	// added -1 to set pa range date right Rev#03
	ii_PA_Range --
	idt_parange_end_date = RelativeDate(Today(),ii_PA_Range)
Else
	// Users wanted PA NEG to be 21 days out
	If Upper(as_date_type) = 'PANEG' then
		is_date_type = 'PANEG'
	  	SELECT tutltypes.type_short_desc  
    	INTO :ii_PA_Range  
    	FROM tutltypes  
   	WHERE ( tutltypes.type_code = 'THREEWK' ) AND  
      	   ( tutltypes.record_type = 'PA RANGE' )   ;
		ii_PA_Range --
		idt_parange_end_date = RelativeDate(Today(),ii_PA_Range)
	Else
		Return -1
	End if
End If

Return 0

end function

public function integer uf_get_pa_range ();Return ii_pa_range
end function

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.begin_date.Background.Color = 16777215
	This.object.end_date.Background.Color = 16777215
	This.object.begin_date.Protect = 0
	This.object.end_date.Protect = 0
Else
	This.object.begin_date.Background.Color = 67108864
	This.object.end_date.Background.Color = 67108864
	This.object.Begin_date.Protect = 1
	This.object.end_date.Protect = 1
End If

Return 1
end function

event doubleclicked;String		ls_date, &
				ls_columnname

str_parms	lstr_parms

ls_columnname = dwo.name
If ls_columnname = 'begin_date' and This.object.begin_date.Protect = '1' Then Return -1
If ls_columnname = 'end_date' and This.object.end_date.Protect = '1' Then Return -1
// Add to fix bug rev#02
If ls_columnname = 'begin_date' or ls_columnname = 'end_date' Then
	// Get the X and Y coordinates of the place that was clicked
	lstr_parms.integer_arg[1] = iw_parentwindow.PointerX() + iw_parentwindow.WorkSpaceX() - 50
	CHOOSE CASE	lstr_parms.integer_arg[1]
	CASE IS > 2253
		lstr_parms.integer_arg[1] = 2253
	CASE is < 113
		lstr_parms.integer_arg[1] = 113
	END CHOOSE
	
	lstr_parms.integer_arg[2] = iw_parentwindow.PointerY() + iw_parentwindow.WorkSpaceY() - 500
	CHOOSE CASE	lstr_parms.integer_arg[2]
	CASE IS > 1053
		lstr_parms.integer_arg[2] = 1053
	CASE is < 0
		lstr_parms.integer_arg[2] = 0
	END CHOOSE
	
	lstr_parms.date_arg[1] = This.GetItemDate(1, ls_columnname)
	OpenWithParm(w_Calendar, lstr_parms)
	// Get the return string (date) from the calendar window
	// If an empty string do not do anything
	ls_date = Message.StringParm
	If ls_date <> "" Then
		SetText(ls_Date)
	Else
		Return
	End If
End If

Return
end event

event constructor;iw_parentwindow = Parent
end event

event itemchanged;String				ls_columnname

Date					ldt_value,ldt_parange_end_date
Integer				li_PA_Range


ls_columnname = dwo.name

Choose Case ls_columnname 
	Case "begin_date" 
		ldt_value = Date(Data)
		If ldt_value < today() Then
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("Begin Date must be greater than or equal to today")   
			Return 1
		End If
		If GetItemStatus(row, "end_date", primary!) = &
				DataModified! Then
			If ldt_value > Getitemdate(row,"end_date") Then
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("Begin Date must be less than End Date")   
				Return 1
			End If
		End If
	Case "end_date" 
		ldt_value = Date(Data)
		IF Upper(is_date_type) = 'PARANGE' Then
			If ldt_value > idt_parange_end_date then 
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("End date cannot be later than " + &
					String(idt_parange_end_date))
				return 1
			End If
		End IF

		IF Upper(is_date_type) = 'PANEG' Then
		  SELECT tutltypes.type_short_desc  
			 INTO :li_PA_Range  
			 FROM tutltypes  
			WHERE ( tutltypes.type_code = 'PA RANGE' ) AND  
					( tutltypes.record_type = 'PA RANGE' )   ;
			li_PA_Range --
			ldt_parange_end_date = RelativeDate(Today(),li_PA_Range)
			
			If ldt_value > ldt_parange_end_date then 
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("End date cannot be later than " + &
					String(ldt_parange_end_date))
				return 1
			End If
		End IF
		
		
		If GetItemStatus(row, "begin_date", primary!) = &
				DataModified! Then
			If ldt_value < Getitemdate(row,"begin_date") Then
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("End Date must be greater than the Begin Date")				
				Return 1
			End If
		End If

End Choose


end event

event itemerror;Return 1
end event

on u_begin_end_dates.create
end on

on u_begin_end_dates.destroy
end on

