﻿$PBExportHeader$w_base_frame_ext.srw
forward
global type w_base_frame_ext from w_netwise_frame
end type
end forward

global type w_base_frame_ext from w_netwise_frame
string title = "Product Availability"
string menuname = "m_base_menu_ext"
end type
global w_base_frame_ext w_base_frame_ext

type variables
m_base_menu_ext		im_menu


end variables

event ue_postopen;call super::ue_postopen;integer 	li_rtn
u_olecom	lu_oleReportList
String ls_userid,ls_Region, ls_web_service_address
IF ib_login_cancel THEN RETURN
// IBDKDLD 01/02/03
ls_userid = ProfileString( gw_netwise_frame.is_UserIni,"System Settings","UserID","")
If upper(left(ls_userid,4)) = "IBDK" or upper(left(ls_userid,4)) = "QATE" Then
                ls_web_service_address = ProfileString(gw_netwise_frame.is_WorkingDir + "ibp002.ini", "Web Service URL", "pasp00SRpath", "") 
//            ls_Region =  ProfileString( gw_netwise_frame.is_ApplicationINI,"Netwise Server Info","ServerSuffix","")
//            IF UPPER(left(ls_Region,1)) = "P" Then
                IF POS(ls_web_service_address, 'cics00b') > 0 Then
                                This.title += "            ****************  WARNING YOU ARE CONNECTED TO PRODUCTION  ****************"
                END IF
ENd IF


end event

event open;call super::open;im_menu = This.MenuID
//ib_userid = True
Idle(10)

end event

on w_base_frame_ext.create
call super::create
if IsValid(this.MenuID) then destroy(this.MenuID)
if this.MenuName = "m_base_menu_ext" then this.MenuID = create m_base_menu_ext
end on

on w_base_frame_ext.destroy
call super::destroy
if IsValid(MenuID) then destroy(MenuID)
end on

