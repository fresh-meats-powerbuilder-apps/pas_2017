﻿$PBExportHeader$w_base_web_sheet_ext.srw
forward
global type w_base_web_sheet_ext from w_base_sheet_ext
end type
type ole_web from olecustomcontrol within w_base_web_sheet_ext
end type
type stlabel from statictext within w_base_web_sheet_ext
end type
end forward

global type w_base_web_sheet_ext from w_base_sheet_ext
integer width = 2793
integer height = 1476
ole_web ole_web
stlabel stlabel
end type
global w_base_web_sheet_ext w_base_web_sheet_ext

type variables
String is_URL
Boolean ib_inProcess
end variables

forward prototypes
public function boolean wf_retrieve ()
public function oleobject wf_get_handle ()
public subroutine wf_print ()
end prototypes

public function boolean wf_retrieve ();If Not ib_inprocess Then
	ole_web.object.Navigate (is_url)
End if
return true
end function

public function oleobject wf_get_handle ();return ole_web.object.object 
end function

public subroutine wf_print ();// exacute the the parent windows print
ole_web.object.Document.parentWindow.execScript ("window.print();")
end subroutine

event open;call super::open;is_url = ProfileString ("Ibp002.ini", "OPPA Web Config", classname(this),"about:blank") 
if is_url <> "about:blank" then
	// append the needed arguments on the end of the url
	is_url += "?UID=" + sqlca.userid + "&" +"PWD=" + sqlca.dbpass
End IF
end event

event resize;call super::resize;long  lltemp = 1
This.SetRedraw(False)
IF newwidth  > lltemp THEN
	ole_web.Width = UnitsToPixels(newwidth,XUnitsToPixels!) - lltemp
	ole_web.object.Width = ole_web.Width 
	stlabel.width = newwidth	
END IF

IF newheight > lltemp THEN
	ole_web.Height = UnitsToPixels(newheight,YUnitsToPixels!) - lltemp
	ole_web.object.Height = ole_web.Height 
	stlabel.height = newheight	
END if
This.SetRedraw(True)
end event

event ue_fileprint;This.wf_print()
end event

event ue_postopen;call super::ue_postopen;ole_web.object.RegisterAsBrowser = True
// do this so the control will redraw with scroll bars
This.Postevent("resize") 

wf_retrieve()
end event

event ue_printwithsetup;call super::ue_printwithsetup;this.wf_print()
end event

event ue_revisions;call super::ue_revisions;// author: Elwin McKernan
// New window 11/14/2002
end event

on w_base_web_sheet_ext.create
int iCurrent
call super::create
this.ole_web=create ole_web
this.stlabel=create stlabel
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_web
this.Control[iCurrent+2]=this.stlabel
end on

on w_base_web_sheet_ext.destroy
call super::destroy
destroy(this.ole_web)
destroy(this.stlabel)
end on

type ole_web from olecustomcontrol within w_base_web_sheet_ext
event statustextchange ( string text )
event progresschange ( long progress,  long progressmax )
event commandstatechange ( long command,  boolean enable )
event downloadbegin ( )
event downloadcomplete ( )
event titlechange ( string text )
event propertychange ( string szproperty )
event beforenavigate2 ( oleobject pdisp,  any url,  any flags,  any targetframename,  any postdata,  any headers,  ref boolean bcancel )
event newwindow2 ( ref oleobject ppdisp,  ref boolean bcancel )
event navigatecomplete2 ( oleobject pdisp,  any url )
event documentcomplete ( oleobject pdisp,  any url )
event onquit ( )
event onvisible ( boolean ocx_visible )
event ontoolbar ( boolean toolbar )
event onmenubar ( boolean menubar )
event onstatusbar ( boolean statusbar )
event onfullscreen ( boolean fullscreen )
event ontheatermode ( boolean theatermode )
event windowsetresizable ( boolean bresizable )
event windowsetleft ( long left )
event windowsettop ( long top )
event windowsetwidth ( long ocx_width )
event windowsetheight ( long ocx_height )
event windowclosing ( boolean ischildwindow,  ref boolean bcancel )
event clienttohostwindow ( ref long cx,  ref long cy )
event setsecurelockicon ( long securelockicon )
event filedownload ( ref boolean bcancel )
event navigateerror ( oleobject pdisp,  any url,  any frame,  any statuscode,  ref boolean bcancel )
event printtemplateinstantiation ( oleobject pdisp )
event printtemplateteardown ( oleobject pdisp )
event updatepagestatus ( oleobject pdisp,  any npage,  any fdone )
event privacyimpactedstatechange ( boolean bimpacted )
integer width = 2761
integer height = 1376
integer taborder = 10
boolean border = false
borderstyle borderstyle = stylelowered!
long backcolor = 67108864
boolean focusrectangle = false
string binarykey = "w_base_web_sheet_ext.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event statustextchange(string text);string ls_temp

ls_temp = iw_frame.iu_string.nf_gettoken(text, '?')
iw_frame.setmicrohelp(ls_temp)
end event

event downloadbegin();iw_frame.setmicrohelp("Wait ... Inquiring")
parent.stLabel.Visible= True

ib_inprocess = true
end event

event downloadcomplete();parent.stLabel.Visible= False 
ib_inprocess = False
iw_frame.setmicrohelp("Ready")
end event

event titlechange(string text);string ls_temp

ls_temp = iw_frame.iu_string.nf_gettoken(text, '?')
parent.title = ls_temp
end event

event newwindow2(ref oleobject ppdisp, ref boolean bcancel);//frmWB.brwWebBrowser.RegisterAsBrowser = True
//   ppDisp = frmWB.brwWebBrowser.Object

w_base_web_sheet_ext lw_temp
//window	lw_temp

OpenSheet (lw_temp,"w_base_web_sheet",iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)
//lw_temp.ole_web.object.RegisterAsBrowser = True
ppdisp = lw_temp.wf_get_handle()
bcancel = true
end event

event onquit();close(parent)
end event

type stlabel from statictext within w_base_web_sheet_ext
integer width = 2761
integer height = 1376
integer textsize = -24
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Wait ... Inquiring"
alignment alignment = center!
boolean focusrectangle = false
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Dw_base_web_sheet_ext.bin 
2A00000a00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000100000000000000000000000000000000000000000000000000000000249dd1c001c28cbf00000003000000c00000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000102001affffffff00000002ffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000001001affffffffffffffff000000038856f96111d0340ac0006ba9a205d74f00000000249dd1c001c28cbf249dd1c001c28cbf000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000009c000000000000000100000002fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
2Affffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000004c00003e6d0000238e0000000000000000000000000000000000000000000000000000004c0000000000000000000000010057d0e011cf3573000869ae62122e2b00000008000000000000004c0002140100000000000000c04600000000000080000000000000000000000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000200028002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d0062006f005f00650070005d006e006f0000006800740072006500280020007500200073006e006700690065006e006c0064006e006f002000670070007700720061006d00610020002c006f006c0067006e006c002000610070006100720020006d002000290072002000740065007200750073006e006c0020006e006f002000670070005b006d0062006f005f00680074007200650000005d000024b800000000000024d600000000000024f00000000000002514000000000000252c0000000000002540000000000000255c000000000000257c00000000000025a200000000000025b600000000000025d80000000000002604000000000000262c000000000000264a000000000000266a000000000000269e00000000000026c200000000000026ea000000000000270e000000000000272800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Dw_base_web_sheet_ext.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
