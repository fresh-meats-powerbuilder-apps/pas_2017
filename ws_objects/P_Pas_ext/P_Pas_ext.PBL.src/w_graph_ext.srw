﻿$PBExportHeader$w_graph_ext.srw
forward
global type w_graph_ext from w_new_graph
end type
end forward

global type w_graph_ext from w_new_graph
WindowState WindowState=normal!
end type
global w_graph_ext w_graph_ext

on w_graph_ext.create
call w_new_graph::create
end on

on w_graph_ext.destroy
call w_new_graph::destroy
end on

event open;int 				li_rc
datawindow		ldw_graph

ldw_graph	=	message.Powerobjectparm

dw_graph.dataobject = Message.StringParm
li_rc = dw_graph.Importstring( ldw_graph.Describe("DataWindow.Data") )
IF li_rc = -1 THEN MessageBox( "Data Import", "Importing Data Failed" )

li_rc = dw_graph.Sort()
li_rc = dw_graph.GroupCalc ( )

This.Title	=	dw_graph.object.gr_1.title
//set focus on the graphtype datawindow
tab_1.tabpage_type.dw_graph_type.TriggerEvent(getfocus!)

end event

type tabpage_type from w_new_graph`tabpage_type within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
end type

type tabpage_category from w_new_graph`tabpage_category within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
end type

type tabpage_values from w_new_graph`tabpage_values within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
end type

type tabpage_series from w_new_graph`tabpage_series within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
end type

type tabpage_data from w_new_graph`tabpage_data within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
end type

type tabpage_title from w_new_graph`tabpage_title within tab_1
int X=19
int Y=113
int Width=2858
int Height=205
end type

