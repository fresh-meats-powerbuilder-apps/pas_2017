﻿$PBExportHeader$u_dddw_editmask.sru
forward
global type u_dddw_editmask from UserObject
end type
type dw_data from datawindow within u_dddw_editmask
end type
type p_1 from picture within u_dddw_editmask
end type
type em_data from editmask within u_dddw_editmask
end type
end forward

global type u_dddw_editmask from UserObject
int Width=512
int Height=112
boolean Border=true
long BackColor=12632256
long PictureMaskColor=25166016
long TabTextColor=33554432
long TabBackColor=67108864
event ue_dddw_select pbm_custom01
event ue_keypressed pbm_keydown
dw_data dw_data
p_1 p_1
em_data em_data
end type
global u_dddw_editmask u_dddw_editmask

type variables
String	DisplayColumn, &
	DataColumn

Decimal	PercentWidth

Private:
Boolean		ib_AllowEdit
Long	il_CurrentRow

MaskDataType	imdt_MaskType
end variables

forward prototypes
public function string nf_setdataobject (string as_dataobject)
public function string nf_getdataobject ()
public subroutine nf_getdddwxy (ref integer ai_x, ref integer ai_y)
public function long importstring (string as_import)
public subroutine nf_settext (string as_text)
public function string nf_gettext ()
public function integer nf_getdddwwidth ()
public subroutine nf_allowedit (boolean ab_allowedit)
public function integer reset ()
end prototypes

on ue_dddw_select;Long		ll_ClickedRow
String	ls_ColType, &
			ls_text

ll_ClickedRow = Message.LongParm
If ll_ClickedRow < 1 Then return
il_CurrentRow = ll_ClickedRow

ls_ColType = Lower(Left(dw_data.Describe(displaycolumn + '.ColType'), 5))
Choose Case ls_ColType
Case 'char'
	em_data.Text = dw_data.GetItemString(ll_ClickedRow, displaycolumn)
Case 'date'
	ls_Text = String(dw_data.GetItemDate(ll_ClickedRow, displaycolumn), em_data.Mask)
	em_data.Text = ls_text
Case 'datet'
	em_data.Text = String(dw_data.GetItemDateTime(ll_ClickedRow, displaycolumn))
Case 'decim'
	em_data.Text = String(dw_data.GetItemDecimal(ll_ClickedRow, displaycolumn))
Case 'numbe'
	em_data.Text = String(dw_data.GetItemNumber(ll_ClickedRow, displaycolumn))
Case 'time', 'times'
	em_data.Text = String(dw_data.GetItemTime(ll_ClickedRow, displaycolumn))
End Choose


end on

on ue_keypressed;If KeyDown(KeyDownArrow!) And KeyDown(KeyControl!) Then
	em_data.PostEvent("DoubleClicked")
	return
End if

If KeyDown(KeyDownArrow!) And KeyDown(KeyAlt!) Then
	p_1.PostEvent(Clicked!)
	return
End if

If KeyDown(KeyDownArrow!) Then
	If il_currentRow < dw_data.RowCount() Then
		il_CurrentRow ++
		This.nf_SetText(This.nf_GetText())
	End if
End if

If KeyDown(KeyUpArrow!) Then
	If il_currentrow > 1 Then
		il_CurrentRow --
		This.nf_SetText(This.nf_GetText())
	End if
End if
end on

public function string nf_setdataobject (string as_dataobject);dw_data.DataObject = as_dataobject
displaycolumn = dw_data.Describe("#1.Name")
DataColumn = dw_data.Describe("#1.Name")

return ''
end function

public function string nf_getdataobject ();return dw_data.DataObject
end function

public subroutine nf_getdddwxy (ref integer ai_x, ref integer ai_y);Window	lw_parent

lw_parent = Parent
ai_x = This.X + lw_Parent.WorkSpaceX()
ai_y = This.Y + lw_Parent.WorkSpaceY() + This.Height
end subroutine

public function long importstring (string as_import);return dw_data.ImportString(as_import)
end function

public subroutine nf_settext (string as_text);String	ls_Find


Choose Case Lower(Left(dw_data.Describe(DataColumn + '.ColType'), 5))
Case 'char'
	ls_Find = DataColumn + " = '" + as_text + "'"
Case 'date'
	ls_Find = DataColumn + " = Date('" + as_text + "')"
Case 'datet'
	ls_Find = DataColumn + " = DateTime(Date('" + Left(as_text, &
						Pos(as_text, ' ') - 1) + "'), Time('" + &
						Right(as_text, Len(as_text) - Pos(as_text, ' ')) + &
						"'))"
Case 'decim', 'numbe'
	ls_Find = DataColumn + " = " + as_text
Case 'time', 'times'
	ls_find = DataColumn + " = Time('" + as_text + "')"
End Choose

il_CurrentRow = dw_Data.Find(ls_Find, 1, dw_Data.RowCount())
If il_CurrentRow > 0 Then
	Choose Case Lower(Left(dw_data.Describe(DisplayColumn + '.ColType'), 5))
	Case 'char'
		em_Data.Text = dw_data.GetItemString(il_CurrentRow, DataColumn)
	Case 'date'
		em_Data.Text = String(dw_data.GetItemDate(il_CurrentRow, DataColumn), em_data.Mask)
	Case 'datet'
		em_Data.Text = String(dw_data.GetItemDateTime(il_CurrentRow, DataColumn), em_data.Mask)
	Case 'decim'
		em_Data.Text = String(dw_data.GetItemDecimal(il_CurrentRow, DataColumn), em_data.Mask)
	Case 'numbe'
		em_Data.Text = String(dw_data.GetItemNumber(il_CurrentRow, DataColumn), em_data.Mask)
	Case 'time', 'times'
		em_Data.Text = String(dw_data.GetItemTime(il_CurrentRow, DataColumn), em_data.Mask)
	End Choose
Else
	// not found
	em_data.Text = as_text
End if

end subroutine

public function string nf_gettext ();String	ls_ColType


If Len(Trim(datacolumn)) = 0 Then
	// If no DataColumn has been defined, then just return the text of the edit mask
	Return em_data.Text
End if

If il_CurrentRow < 1 Then Return em_Data.Text

ls_ColType = Lower(Left(dw_data.Describe(DataColumn + '.ColType'), 5))
Choose Case ls_ColType
Case 'char'
	Return dw_data.GetItemString(il_CurrentRow, DataColumn)
Case 'date'
	Return String(dw_data.GetItemDate(il_CurrentRow, DataColumn), em_data.Mask)
Case 'datet'
	Return String(dw_data.GetItemDateTime(il_CurrentRow, DataColumn), em_data.Mask)
Case 'decim'
	Return String(dw_data.GetItemDecimal(il_CurrentRow, DataColumn), em_data.Mask)
Case 'numbe'
	Return String(dw_data.GetItemNumber(il_CurrentRow, DataColumn), em_data.Mask)
Case 'time', 'times'
	Return String(dw_data.GetItemTime(il_CurrentRow, DataColumn), em_data.Mask)
End Choose

// If that fails, return nothing
return ''


end function

public function integer nf_getdddwwidth ();Return ((This.Width * percentwidth) / 100)
end function

public subroutine nf_allowedit (boolean ab_allowedit);//ib_AllowEdit = ab_AllowEdit
//em_Data.TriggerEvent("ue_AllowEdit")
//If ab_allowEdit Then
//	em_Data.Resize(This.Width - p_1.Width - 50, em_data.Height)
//Else
//	em_Data.Resize(This.Width - p_1.Width, em_data.Height)
//End if
end subroutine

public function integer reset ();em_data.Text = ''
return dw_data.Reset()
end function

on constructor;p_1.Move(This.Width - p_1.Width, p_1.Y)

em_data.Resize(This.Width - p_1.Width - 50, em_Data.Height)

// Default Percent Width to be as wide as control
This.PercentWidth = 100
end on

on u_dddw_editmask.create
this.dw_data=create dw_data
this.p_1=create p_1
this.em_data=create em_data
this.Control[]={this.dw_data,&
this.p_1,&
this.em_data}
end on

on u_dddw_editmask.destroy
destroy(this.dw_data)
destroy(this.p_1)
destroy(this.em_data)
end on

type dw_data from datawindow within u_dddw_editmask
int X=457
int Y=28
int Width=46
int Height=40
int TabOrder=20
boolean Visible=false
string DataObject="d_settings"
boolean Border=false
end type

type p_1 from picture within u_dddw_editmask
int X=384
int Width=87
int Height=88
string PictureName="ddlb.bmp"
boolean FocusRectangle=false
end type

on clicked;Long		ll_ClickedRow

If IsValid(w_dddw_editmask) Then 
	Close(w_dddw_editmask)
	return
End if

OpenWithParm(w_dddw_editmask, Parent)

end on

type em_data from editmask within u_dddw_editmask
event doubleclicked pbm_lbuttondblclk
event setmasktype pbm_custom01
event ue_allowedit pbm_custom02
int Width=343
int Height=92
int TabOrder=10
BorderStyle BorderStyle=StyleLowered!
string Mask="mm/dd/yyyy"
MaskDataType MaskDataType=DateMask!
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event doubleclicked;// If the double clicked field is a date or datetime, open w_Calendar
// to allow date editing.

long			ll_ClickedRow,	ll_ClickedColumn
integer		li_ColumnNumber
string		ls_ColumnType, ls_date, &
				ls_protected
str_parms	lstr_parms


// Get the column type of the clicked field
If This.MaskDataType <> DateMask! And This.MaskDataType <> DateTimeMask! Then
	Return 
End if

// Only display the clock if the column has edit capablilities
If This.DisplayOnly Then return 


// Get the X and Y coordinates of the place that was clicked
lstr_parms.integer_arg[1] = iw_Frame.PointerX() + iw_Frame.WorkSpaceX() - 50
CHOOSE CASE	lstr_parms.integer_arg[1]
CASE IS > 2253
	lstr_parms.integer_arg[1] = 2253
CASE is < 113
	lstr_parms.integer_arg[1] = 113
END CHOOSE

lstr_parms.integer_arg[2] = iw_Frame.PointerY() + iw_Frame.WorkSpaceY() - 500
CHOOSE CASE	lstr_parms.integer_arg[2]
CASE IS > 1053
	lstr_parms.integer_arg[2] = 1053
CASE is < 0
	lstr_parms.integer_arg[2] = 0
END CHOOSE

lstr_parms.date_arg[1] = Date(Parent.nf_GetText())

OpenWithParm(w_Calendar, lstr_parms)
// Get the return string (date) from the calendar window
// If an empty string do not do anything
ls_date = Message.StringParm
If ls_date <> "" Then
	Parent.nf_SetText(ls_Date)
Else
	Return
End If

end event

on setmasktype;//This.MaskDataType = imdt_MaskType
end on

on ue_allowedit;//This.DisplayOnly = Not ib_AllowEdit
end on

event modified;Parent.nf_SetText(This.Text)
end event

