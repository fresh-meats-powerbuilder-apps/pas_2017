﻿$PBExportHeader$u_pa_neg_div_prod_grp.sru
$PBExportComments$This is the same as u_div_prod_group for the exception that this one has All Products in the inquire options.
forward
global type u_pa_neg_div_prod_grp from userobject
end type
type uo_groups from u_group_list within u_pa_neg_div_prod_grp
end type
type dw_division from u_division within u_pa_neg_div_prod_grp
end type
type dw_inq_options from datawindow within u_pa_neg_div_prod_grp
end type
end forward

global type u_pa_neg_div_prod_grp from userobject
integer width = 2437
integer height = 728
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
event ue_post_constructor ( )
uo_groups uo_groups
dw_division dw_division
dw_inq_options dw_inq_options
end type
global u_pa_neg_div_prod_grp u_pa_neg_div_prod_grp

forward prototypes
public function string uf_get_division ()
public function string uf_get_division_desc ()
public function string uf_get_inquire_options ()
public subroutine uf_set_division (string as_division)
public subroutine uf_set_inquire_options (string as_inquire_options)
public function string uf_get_owner ()
public function integer uf_get_sel_desc (ref string as_string)
public function integer uf_get_sel_id (ref string as_string)
public subroutine uf_setfocus (string as_setfocus)
end prototypes

event ue_post_constructor();//ole_groups.object.GroupType(3)
uo_groups.uf_load_groups('P')
//ole_Groups.object.LoadObject()

end event

public function string uf_get_division ();return dw_division.uf_get_division()


end function

public function string uf_get_division_desc ();return dw_division.uf_get_division_descr()
end function

public function string uf_get_inquire_options ();Return dw_inq_options.GetItemString(1,'inquire_options')
end function

public subroutine uf_set_division (string as_division);dw_division.uf_set_division(as_division)
end subroutine

public subroutine uf_set_inquire_options (string as_inquire_options);u_string_functions 	lu_strings
dwobject					ldwo_object

// There is nothing in ldwo_object just used to fulfill
// the arguments needed to trigger itemchanged! code.

If Not lu_strings.nf_isempty(as_inquire_options) Then 
	dw_inq_options.SetItem(1,'inquire_options',as_inquire_options)
	dw_inq_options.Event itemchanged(1,ldwo_object,as_inquire_options )
Else
	dw_inq_options.SetItem(1,'inquire_options','A')
End If
Return


end subroutine

public function string uf_get_owner ();//return ole_groups.object.systemname()
return uo_groups.uf_get_owner()

end function

public function integer uf_get_sel_desc (ref string as_string);Integer	li_rtn

//as_string = ole_groups.object.Groupdescription()
li_rtn = uo_groups.uf_get_sel_desc(as_string)
return 0


end function

public function integer uf_get_sel_id (ref string as_string);String		ls_string

//as_string = string(ole_groups.object.groupID())
as_string = string(uo_groups.uf_get_sel_id(ls_string))
Return 0

end function

public subroutine uf_setfocus (string as_setfocus);CHOOSE CASE as_setfocus
	CASE 'division'
		dw_division.SetFocus()
	CASE 'options'
		dw_inq_options.SetFocus()
	CASE 'prodgrp'
//		ole_groups.SetFocus()
		uo_groups.SetFocus()
END CHOOSE

end subroutine

event constructor;This.uf_setfocus('options')
uo_groups.Visible = False
PostEvent('ue_post_constructor')
end event

on u_pa_neg_div_prod_grp.create
this.uo_groups=create uo_groups
this.dw_division=create dw_division
this.dw_inq_options=create dw_inq_options
this.Control[]={this.uo_groups,&
this.dw_division,&
this.dw_inq_options}
end on

on u_pa_neg_div_prod_grp.destroy
destroy(this.uo_groups)
destroy(this.dw_division)
destroy(this.dw_inq_options)
end on

type uo_groups from u_group_list within u_pa_neg_div_prod_grp
integer x = 1102
integer y = 124
integer width = 1312
integer taborder = 30
end type

on uo_groups.destroy
call u_group_list::destroy
end on

type dw_division from u_division within u_pa_neg_div_prod_grp
integer x = 818
integer y = 24
integer taborder = 10
end type

type dw_inq_options from datawindow within u_pa_neg_div_prod_grp
event ue_postconstructor ( )
integer x = 9
integer width = 814
integer height = 336
integer taborder = 20
string dataobject = "d_pa_neg_inq_options"
boolean border = false
boolean livescroll = true
end type

event constructor;This.InsertRow(0)
end event

event itemchanged;CHOOSE CASE data
	Case 'A'
		dw_division.Visible = False
//		ole_groups.Visible = False
		uo_groups.Visible = False
	Case 'D'
		dw_division.Visible = True
//		ole_groups.Visible = False
		uo_groups.Visible = False
	Case 'P'
//		ole_groups.Visible = True
		uo_groups.Visible = True
		dw_division.Visible = False
	Case 'B'
		dw_division.Visible = True
//		ole_groups.Visible = True
		uo_groups.Visible = True
	Case Else
		dw_division.Visible = False
//		ole_groups.Visible = False
		uo_groups.Visible = False
END CHOOSE

end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
03u_pa_neg_div_prod_grp.bin 
2800000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff00000003000000000000000000000000000000000000000000000000000000006239811001c6267600000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f3000000006239811001c626766239811001c62676000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
2Cffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
13u_pa_neg_div_prod_grp.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
