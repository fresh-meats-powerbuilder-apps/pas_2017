﻿$PBExportHeader$w_working_scroll.srw
forward
global type w_working_scroll from Window
end type
type st_display from statictext within w_working_scroll
end type
end forward

global type w_working_scroll from Window
int X=1925
int Y=193
int Width=654
int Height=145
long BackColor=8421376
boolean ControlMenu=true
WindowType WindowType=popup!
st_display st_display
end type
global w_working_scroll w_working_scroll

type variables
Boolean ib_Scroll
end variables

on open;DECIMAL	ld_width

INTEGER	li_CommaPos1

STRING	ls_StringParm, &
			ls_Text
			
			
			
ls_StringParm = Message.StringParm

li_CommaPos1 = POS(ls_StringParm, ",")

IF li_CommaPos1 = 0 THEN
	ls_Text = Trim(ls_StringParm)
	ib_scroll = FALSE
ElSE
	ls_Text = Trim(MID(ls_StringParm, 1, li_CommaPos1 - 1))
	IF Upper(Trim(Mid(ls_StringParm,li_commaPos1 + 1))) = "TRUE" THEN
		ib_scroll = TRUE
	ELSE
		ib_scroll = FALSE
	END IF
END IF

IF Len(Trim(ls_text)) = 0  Then
	ls_Text = "Working..."
END IF

IF Len(Trim(ls_text)) > 14 THEN
	ld_width = 39.215 * Len(Trim(ls_text))	
	IF ld_width <= 1162	THEN
		This.st_display.Width = ld_width
		This.Width = ld_width + 144
		This.Move(1925 - ( ld_width - 714),144)
	Else
		This.st_display.Width = 1162
		This.Width = 1306
		This.Move(1431,144)
		ib_Scroll = TRUE
	END IF	
END IF

IF ib_scroll THEN
	ls_text = ls_text+Space(3)
   This.st_display.Text = ls_text
	Timer(.5)
ELSE
	This.st_display.Text = ls_Text
END IF
end on

on close;Timer(0)
end on

on w_working_scroll.create
this.st_display=create st_display
this.Control[]={ this.st_display}
end on

on w_working_scroll.destroy
destroy(this.st_display)
end on

on timer;String ls_changeText

ls_changeText = This.st_display.Text

This.St_display.Text = Mid(ls_changeText,2)+Left(ls_changeText,1)
end on

type st_display from statictext within w_working_scroll
event ue_postconstructor pbm_custom34
int X=33
int Y=29
int Width=581
int Height=73
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
string Text="Working...."
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="System"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

