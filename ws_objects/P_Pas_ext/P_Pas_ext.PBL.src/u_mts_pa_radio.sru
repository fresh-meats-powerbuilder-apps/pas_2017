﻿$PBExportHeader$u_mts_pa_radio.sru
forward
global type u_mts_pa_radio from datawindow
end type
end forward

global type u_mts_pa_radio from datawindow
integer width = 521
integer height = 212
integer taborder = 10
string dataobject = "d_mts_pa_radio"
boolean border = false
boolean livescroll = true
end type
global u_mts_pa_radio u_mts_pa_radio

forward prototypes
public subroutine uf_set_box_text (string as_title)
public function string uf_get_value ()
public subroutine uf_enable (boolean ab_enable)
public subroutine uf_set_value (string as_choice)
end prototypes

public subroutine uf_set_box_text (string as_title);this.object.display.text = as_title
end subroutine

public function string uf_get_value ();return This.GetItemString(1, "mts_pa_inq")
end function

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("mts_pa_inq.Protect = 0 " + &
				"mts_pa_inq.Pointer = 'Arrow!'")
	Case False
		This.Modify("mts_pa_inq.Protect = 1 " + &
				"mts_pa_inq.Pointer = 'Beam!'")
END CHOOSE

end subroutine

public subroutine uf_set_value (string as_choice);This.SetItem(1,"mts_pa_inq",as_choice)
end subroutine

event constructor;This.InsertRow(0)
end event

on u_mts_pa_radio.create
end on

on u_mts_pa_radio.destroy
end on

