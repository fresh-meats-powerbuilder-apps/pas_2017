﻿$PBExportHeader$u_output_choice.sru
$PBExportComments$ibdkdld
forward
global type u_output_choice from u_base_dw_ext
end type
end forward

global type u_output_choice from u_base_dw_ext
integer width = 594
integer height = 292
string dataobject = "d_output_choice"
boolean border = false
end type
global u_output_choice u_output_choice

forward prototypes
public subroutine uf_enable (boolean ab_enable)
public function string uf_get_choice ()
public subroutine uf_set_choice (string as_choice)
public subroutine uf_set_box_text (string as_title)
end prototypes

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("choice.Protect = 0 " + &
				"choice.Pointer = 'Arrow!'")
	Case False
		This.Modify("choice.Protect = 1 " + &
				"choice.Pointer = 'Beam!'")
		This.Ib_updateable = False		
END CHOOSE

end subroutine

public function string uf_get_choice ();return This.GetItemString(1, "choice")
end function

public subroutine uf_set_choice (string as_choice);This.SetItem(1,"choice",as_choice)
end subroutine

public subroutine uf_set_box_text (string as_title);this.object.display.text = as_title
end subroutine

event constructor;call super::constructor;This.InsertRow(0)
end event

on u_output_choice.create
end on

on u_output_choice.destroy
end on

