﻿$PBExportHeader$w_raw_mat_sap_po.srw
$PBExportComments$Add pallet
forward
global type w_raw_mat_sap_po from w_base_sheet_ext
end type
type rb_deselect from radiobutton within w_raw_mat_sap_po
end type
type rb_select from radiobutton within w_raw_mat_sap_po
end type
type dw_header from u_base_dw_ext within w_raw_mat_sap_po
end type
type dw_rmt_detail from u_base_dw_ext within w_raw_mat_sap_po
end type
type gb_select from groupbox within w_raw_mat_sap_po
end type
end forward

global type w_raw_mat_sap_po from w_base_sheet_ext
integer width = 4343
integer height = 2056
string title = "Review SAP Purchase Orders"
long backcolor = 67108864
rb_deselect rb_deselect
rb_select rb_select
dw_header dw_header
dw_rmt_detail dw_rmt_detail
gb_select gb_select
end type
global w_raw_mat_sap_po w_raw_mat_sap_po

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product, &
				ib_m_Print_Status

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow, &
				il_lastrow

s_error		istr_error_info

u_pas201		iu_pas201

u_ws_pas2      iu_ws_pas2

String		is_colname, &
				is_input, &
				is_ChangedColumnName, &
				is_debug, &
				is_data
				
w_base_sheet	iw_order_detail
end variables

forward prototypes
public function boolean wf_set_pa_row (long al_row)
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_set_pa_row (long al_row);Long					ll_rtn, &
						ll_nbrrows, &
						ll_value

String				ls_temp

//this function will reset all the indicators and make the
//SAP and PA row match where they where different.
IF al_row < 0 THEN Return True

If dw_rmt_detail.GetItemString(al_row, "vendor_descr_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "vendor_descr", &
	   dw_rmt_detail.GetItemString(al_row, "vendor_descr"))
	dw_rmt_detail.Setitem (al_row, "vendor_descr_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "vendor_descr_ind",' ')
End If

If dw_rmt_detail.GetItemString(al_row, "dest_plant_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "dest_plant", &
	   dw_rmt_detail.GetItemString(al_row, "dest_plant"))
	dw_rmt_detail.Setitem (al_row, "dest_plant_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "dest_plant_ind",' ')
End If

If dw_rmt_detail.GetItemString(al_row, "delivery_date_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "delivery_date", &
	   dw_rmt_detail.GetItemString(al_row, "delivery_date"))
	dw_rmt_detail.Setitem (al_row, "delivery_date_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "delivery_date_ind",' ')
End If

If dw_rmt_detail.GetItemString(al_row, "material_code_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "material_code", &
	   dw_rmt_detail.GetItemString(al_row, "material_code"))
	dw_rmt_detail.Setitem (al_row, "material_code_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "material_code_ind",' ')
End If

If dw_rmt_detail.GetItemString(al_row, "product_code_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "product_code", &
	   dw_rmt_detail.GetItemString(al_row, "product_code"))
	dw_rmt_detail.Setitem (al_row, "product_code_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "product_code_ind",' ')
End If

If dw_rmt_detail.GetItemString(al_row, "boxes_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "boxes", &
	   dw_rmt_detail.GetItemNumber(al_row, "boxes"))
	dw_rmt_detail.Setitem (al_row, "boxes_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "boxes_ind",' ')
End If

If dw_rmt_detail.GetItemString(al_row, "weight_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "weight", &
	   dw_rmt_detail.GetItemNumber(al_row, "weight"))
	dw_rmt_detail.Setitem (al_row, "weight_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "weight_ind",' ')
End If

If dw_rmt_detail.GetItemString(al_row, "price_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "price", &
	   dw_rmt_detail.GetItemNumber(al_row, "price"))
	dw_rmt_detail.Setitem (al_row, "price_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "price_ind",' ')
End If

If dw_rmt_detail.GetItemString(al_row, "price_uom_ind") = 'D' Then
	dw_rmt_detail.Setitem (al_row + 1, "price_uom", &
	   dw_rmt_detail.GetItemString(al_row, "price_uom"))
	dw_rmt_detail.Setitem (al_row, "price_uom_ind",' ')
	dw_rmt_detail.Setitem (al_row + 1, "price_uom_ind",' ')
End If

Return True
end function

public function boolean wf_retrieve ();Integer	li_ret

String	ls_input, &
			ls_output_values, &
			ls_line_ind	
			
Long		ll_value, &
			ll_rtn, &
			ll_row

u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false
ls_input = Message.StringParm
If Not ib_ReInquire Then
	OpenWithParm(w_raw_mat_sap_po_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

rb_deselect.Checked = TRUE

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")

ls_input = This.dw_header.GetItemString(1, "plant_code") + '~t' + &
				dw_header.GetItemString(1, "po_option") + '~t' + &
				String(This.dw_header.GetItemDate(1, 'from_date'), 'yyyy-mm-dd') + '~t' + &
				String(This.dw_header.GetItemDate(1, 'to_date'), 'yyyy-mm-dd') + '~r~n' 

is_input = ls_input
ls_output_values = ''

//li_ret = iu_pas201.nf_pasp47cr_inq_sap_purch_ord(istr_error_info, &
//									is_input, &
//									ls_output_values)

li_ret = iu_ws_pas2.nf_pasp47gr(is_input, &
									ls_output_values, istr_error_info)

This.dw_rmt_detail.Reset()

If li_ret = 0 Then
	This.dw_rmt_detail.ImportString(ls_output_values)

	ll_value = dw_rmt_detail.RowCount()
	If ll_value < 0 Then ll_value = 0

	dw_rmt_detail.ResetUpdate()

	IF ll_value > 0 THEN
 		dw_rmt_detail.SetFocus()
		dw_rmt_detail.ScrollToRow(1)
		dw_rmt_detail.SetColumn( "accept_change" )
		dw_rmt_detail.TriggerEvent("RowFocusChanged")
	END IF

	SetMicroHelp(String(ll_value) + " rows retrieved")
	ib_good_product = True
	This.SetRedraw( True )
	iw_frame.im_menu.mf_enable('m_save')
	//Return( True )
Else
	ll_rtn = iw_frame.im_menu.mf_disable('m_save')
	ib_good_product = False
	This.SetRedraw(True)
	Return( False )
End If

ll_row = 1

DO WHILE ll_row <= ll_value 
	
	IF ll_Row > 0 THEN 
		if dw_rmt_detail.GetItemString(ll_row, "po_uom_ind") = 'E' then
			SetMicroHelp("Undetermined PO unit of measure")
			ll_row = ll_value + 1
		end if
	END IF
	ll_row = ll_row + 1
LOOP

Message.StringParm = ''
return true

end function

public function boolean wf_update ();Boolean			lb_update_ind

integer			li_ColNbr, &
					li_Counter, &
					li_rtn

long				ll_NbrRows, &
					ll_Row, &
					ll_RowCount, &
					ll_6th_row

string			ls_Update_string, &
					ls_output_string, &
					ls_RPC_detail

u_string_functions		lu_string_functions
dwItemStatus	lis_status

dw_rmt_detail.SetReDraw(False)

lb_update_ind = False
ll_NbrRows = dw_rmt_detail.RowCount( )
For li_Counter = 1 to ll_NbrRows
	if dw_rmt_detail.GetItemString(li_counter,"po_status") = 'A' Then
		//do nothing
	else
	if dw_rmt_detail.GetItemString(li_counter,"po_status") = 'R' & 
		and dw_rmt_detail.GetItemString(li_counter,"accept_change") <> 'A' Then	
		//do nothing
	else
	if dw_rmt_detail.GetItemString(li_counter,"accept_change") = 'A' Then
		dw_rmt_detail.SetItem (li_counter + 1 , "po_status", 'A')
		if dw_rmt_detail.GetItemString(li_counter,"po_type") = 'SAP' Then
			dw_rmt_detail.Setitem (li_counter , "update_flag", 'U')
			lb_update_ind = True
		end if	
	else
		dw_rmt_detail.Setitem (li_counter , "po_status", 'R')
		
		dw_rmt_detail.Setitem (li_counter , "accept_change", 'R')
		if dw_rmt_detail.GetItemString(li_counter,"po_type") = 'SAP' Then
			dw_rmt_detail.Setitem (li_counter , "update_flag", 'U')
			lb_update_ind = True	
		end if	
	end if
	end if
	end if
Next
dw_rmt_detail.SetReDraw(True)
IF dw_rmt_detail.AcceptText() = -1 THEN Return( False )

If lb_update_ind = False Then Return (False)
dw_rmt_detail.ResetUpdate()

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_row = 0

ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_rmt_detail)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "pasp48fr"
istr_error_info.se_message = Space(71)

DO 

	ll_6th_Row = lu_string_functions.nf_nPos( ls_Update_string, "~r~n",1,6)
	IF ll_6th_Row > 0 Then
		ls_RPC_Detail = Left( ls_Update_string, ll_6th_Row + 1)
		ls_Update_string = Mid( ls_Update_string, ll_6th_Row + 2)
	ELSE
		ls_Rpc_detail = ls_Update_string
		ls_Update_string = ''
	END IF

//	if Not iu_pas201.nf_pasp48cr_acct_sap_purch_ord(istr_error_info, &
//											ls_RPC_detail, &
//											ls_output_string ) Then 
//		Return False 
//	End if
	
	if Not iu_ws_pas2.nf_pasp48gr(ls_RPC_detail, &
											ls_output_string, istr_error_info ) Then 
		Return False 
	End if


Loop While Not lu_string_functions.nf_IsEmpty( ls_Update_string)

iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	if dw_rmt_detail.GetItemString(li_counter,"accept_change") = 'A' Then
		If Not wf_set_pa_row(li_counter) Then Return False
	end if
	dw_rmt_detail.SetItem(li_Counter, 'update_flag', ' ')
	dw_rmt_detail.SetItem(li_Counter, 'accept_change', ' ')
Next

dw_rmt_detail.ResetUpdate()
dw_rmt_detail.SetFocus()
dw_rmt_detail.SetReDraw(True)

Return( True )
end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)
If IsValid(iu_ws_pas2) Then Destroy(iu_ws_pas2)



end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
//iw_frame.im_menu.mf_enable('m_print')
//iw_frame.im_menu.mf_enable('m_nonvisprint')

If Not ib_m_print_status Then iw_frame.im_menu.mf_Disable('m_print')

iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')



	

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
//iw_frame.im_menu.mf_disable('m_print')
//iw_frame.im_menu.mf_disable('m_nonvisprint')

ib_m_print_status = iw_frame.im_menu.mf_Enabled('m_print')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')

If not ib_good_product Then
	iw_frame.im_menu.mf_disable('m_save')
End If

end event

event open;call super::open;String				ls_input_string, &
						ls_input_plant, &
						ls_input_option, &
						ls_location_name, &
						ls_input_from_date, &
						ls_input_to_date
Long					ls_len
Date					ldt_input_date


dw_header.InsertRow(0)
dw_rmt_detail.InsertRow(0)

dw_header.ib_Updateable=False

ib_reinquire = False


end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_pas201 = Create u_pas201
iu_ws_pas2 = Create u_ws_pas2
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Raw Material Default Schedule"
istr_error_info.se_user_id = sqlca.userid

rb_deselect.Checked = TRUE
This.PostEvent("ue_query")



end event

on w_raw_mat_sap_po.create
int iCurrent
call super::create
this.rb_deselect=create rb_deselect
this.rb_select=create rb_select
this.dw_header=create dw_header
this.dw_rmt_detail=create dw_rmt_detail
this.gb_select=create gb_select
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_deselect
this.Control[iCurrent+2]=this.rb_select
this.Control[iCurrent+3]=this.dw_header
this.Control[iCurrent+4]=this.dw_rmt_detail
this.Control[iCurrent+5]=this.gb_select
end on

on w_raw_mat_sap_po.destroy
call super::destroy
destroy(this.rb_deselect)
destroy(this.rb_select)
destroy(this.dw_header)
destroy(this.dw_rmt_detail)
destroy(this.gb_select)
end on

event ue_get_data;call super::ue_get_data;Long			ll_window_x, &
				ll_window_y, &
				ll_datawindow_x, &
				ll_datawindow_y
				
string		ls_temp				

Choose Case as_value
	Case 'data'
		message.stringparm = is_data
	Case 'xy_values'
		ll_window_x = This.PointerX()
		ll_window_y = This.PointerY()
		ll_datawindow_x = dw_rmt_detail.PointerX() + &
				Long(dw_rmt_detail.object.boxes.width) 
		ll_datawindow_y = dw_rmt_detail.PointerY()
		message.StringParm = String(ll_datawindow_x + (ll_window_x - ll_window_x)) + '~t' + &
				String(ll_datawindow_y + (ll_window_y - ll_datawindow_y)) + '~t'
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'from_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'from_date'), 'mm/dd/yyyy')
	Case 'to_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'to_date'), 'mm/dd/yyyy')
	Case 'po_option' 
		Message.StringParm = dw_header.GetItemString(1, 'po_option')
End Choose

end event

event ue_set_data;call super::ue_set_data;String			ls_purchase_order, &
					ls_temp

Window			lw_temp

Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant_Desc' 
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'from_date'
		dw_header.SetItem(1, 'from_date', Date(as_value))
	Case 'to_date'
		dw_header.SetItem(1, 'to_date', Date(as_value))
	Case 'po_option'
		dw_header.SetItem(1, 'po_option', as_value)
	Case 'po det'
		ls_purchase_order = dw_rmt_detail.GetItemString(dw_rmt_detail.GetSelectedRow (0), "purchase_order")
		ls_temp 	= ""
		ls_temp = ls_purchase_order
	
		OpenSheetWithParm(lw_temp, ls_temp, "w_raw_mat_purch_ord_detail",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
End Choose

end event

event resize;call super::resize;integer li_x		
integer li_y	
integer ly_height = 115

li_x = (dw_rmt_detail.x * 2) + 30 
//li_y = dw_rmt_detail.y + 115

if il_BorderPaddingHeight > ly_height Then
                ly_height = il_BorderPaddingHeight
End If

li_y = dw_rmt_detail.y + ly_height

if width > li_x Then
	dw_rmt_detail.width	= width - li_x
end if

if height > li_y then
	dw_rmt_detail.height	= height - li_y
end if
end event

event ue_fileprint;call super::ue_fileprint;String			ls_temp

DataStore					lds_print


lds_print = create u_print_datastore
lds_print.DataObject = 'd_rmt_sap_po_detail'

lds_print.Modify("DataWindow.Print.Orientation=1")

dw_rmt_detail.sharedata(lds_print)

//ls_temp = lds_print.modify(ls_mod)

lds_print.print()

end event

type rb_deselect from radiobutton within w_raw_mat_sap_po
integer x = 1563
integer y = 156
integer width = 343
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Deselect All"
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	if dw_rmt_detail.GetItemString(li_counter,"po_type") = 'SAP' &
			and dw_rmt_detail.GetItemString(li_counter,"line_ind") <> 'X' Then
		dw_rmt_detail.SetItem(li_Counter, 'accept_change', 'R')
	end if
Next	

end event

type rb_select from radiobutton within w_raw_mat_sap_po
integer x = 1563
integer y = 64
integer width = 343
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Select All"
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	if dw_rmt_detail.GetItemString(li_counter,"po_type") = 'SAP' &
			and dw_rmt_detail.GetItemString(li_counter,"line_ind") <> 'X' &
			and dw_rmt_detail.GetItemString(li_counter,"material_code_ind") <> 'E' Then
		dw_rmt_detail.SetItem(li_Counter, 'accept_change', 'A')
	end if
Next	
end event

type dw_header from u_base_dw_ext within w_raw_mat_sap_po
integer y = 4
integer width = 1385
integer height = 308
integer taborder = 0
string dataobject = "d_rmt_sap_po_header"
boolean controlmenu = true
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False


end event

type dw_rmt_detail from u_base_dw_ext within w_raw_mat_sap_po
integer y = 372
integer width = 4306
integer height = 1152
integer taborder = 20
string dataobject = "d_rmt_sap_po_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return

IF row > 0 Then
//	messagebox('Clicked Event',string(row))
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
		SelectRow(0, False)
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If




end event

event constructor;call super::constructor;ib_updateable = True

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

dw_rmt_detail.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()


end event

event itemerror;call super::itemerror;return (1)
end event

event ue_mousemove;call super::ue_mousemove;String			ls_name, &
					ls_code


Choose Case dwo.name
	Case 'material_code'
		If This.GetItemString ( row, "material_code_ind") = 'E' Then
			This.SetFocus()
			is_data = ""
			is_data = 'Conversion Error'
			OpenWithParm(w_order_detail_tooltip, Parent, Parent)
		End If
	Case 'boxes', 'weight'
		If This.GetItemString ( row, "prod_date_ind") = 'E' Then
			This.SetFocus()
			is_data = ""
			is_data = 'Multiple Production Dates Exist'
			OpenWithParm(w_order_detail_tooltip, Parent, Parent)
		Else 
			If This.GetItemString ( row, "po_uom_ind") = 'E' Then
				This.SetFocus()
				is_data = ""
				is_data = 'Undetermined PO unit of measure'
				OpenWithParm(w_order_detail_tooltip, Parent, Parent)
			End If
		End If
		
	Case Else
	//	il_LastRow = 0
		If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
End Choose

end event

event itemchanged;call super::itemchanged;This.SetItem(row, "update_flag", This.GetItemString(row, "line_ind"))
end event

event rbuttondown;call super::rbuttondown;m_rmt_po_detail	lm_popup
alignment la_align

String 	ls_temp, &
			ls_column			

integer 	li_rtn, &
			li_curr_col, &
			li_width, &
			li_date_position
			
ls_column = dw_rmt_detail.GetColumnName()

lm_popup = Create m_rmt_po_detail
lm_popup.m_rmtdetail.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())


Destroy lm_popup
end event

type gb_select from groupbox within w_raw_mat_sap_po
integer x = 1527
integer y = 12
integer width = 411
integer height = 256
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

