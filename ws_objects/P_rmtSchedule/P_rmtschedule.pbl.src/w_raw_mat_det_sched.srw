﻿$PBExportHeader$w_raw_mat_det_sched.srw
$PBExportComments$Add pallet
forward
global type w_raw_mat_det_sched from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_raw_mat_det_sched
end type
type dw_rmt_detail from u_base_dw_ext within w_raw_mat_det_sched
end type
end forward

global type w_raw_mat_det_sched from w_base_sheet_ext
integer width = 2757
integer height = 1692
string title = "Raw Material Schedule Detail"
long backcolor = 67108864
dw_header dw_header
dw_rmt_detail dw_rmt_detail
end type
global w_raw_mat_det_sched w_raw_mat_det_sched

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

u_pas201		iu_pas201

u_ws_pas2      iu_ws_pas2

String		is_colname, &
				is_input, &
				is_ChangedColumnName, &
				is_debug
				
w_base_sheet	iw_order_detail
end variables

forward prototypes
public subroutine wf_delete ()
public function boolean wf_deleterow ()
public function boolean wf_addrow ()
public function boolean wf_update ()
public function boolean wf_unstring_output (string as_output_string)
public function boolean wf_validate (long al_row)
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_deleterow ();Boolean		lb_ret

dw_rmt_detail.SetFocus()
lb_ret = super::wf_deleterow()

dw_rmt_detail.SelectRow(0,False)
dw_rmt_detail.SelectRow(dw_rmt_detail.GetRow(),True)
return lb_ret
end function

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row

If dw_rmt_detail.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_current_row = dw_rmt_detail.GetRow()

If ll_current_row > 0 Then
	ll_row = dw_rmt_detail.InsertRow(ll_current_row + 1)
	dw_rmt_detail.SetItem(ll_row, "sched_date", dw_rmt_detail.GetItemDate(ll_current_row, "sched_date"))
	dw_rmt_detail.SetItem(ll_row, "product_code", dw_rmt_detail.GetItemString(ll_current_row, "product_code"))
	dw_rmt_detail.SetItem(ll_row, "product_state", dw_rmt_detail.GetItemString(ll_current_row, "product_state"))
//1-13 jac
	dw_rmt_detail.SetItem(ll_row, "product_status", dw_rmt_detail.GetItemString(ll_current_row, "product_status"))
//
Else
	ll_row = dw_rmt_detail.InsertRow(0)
	dw_rmt_detail.SetItem(ll_row, "sched_date", Today())
	dw_rmt_detail.SetItem(ll_row, "product_code", ' ')
End If

dw_rmt_detail.SetItem(ll_row, "update_flag", 'A')
dw_rmt_detail.ScrollToRow(ll_row)
dw_rmt_detail.SetRow(dw_rmt_detail.RowCount())

dw_rmt_detail.SetColumn("product_code")
dw_rmt_detail.SetFocus()
This.SetRedraw(True)

return true
end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdateRMSH, &
					ls_header_string, &
					ls_output_string

dwItemStatus	lis_status

IF dw_rmt_detail.AcceptText() = -1 THEN Return( False )
IF dw_rmt_detail.ModifiedCount() + dw_rmt_detail.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_rmt_detail.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_rmt_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_header_string = is_input
ls_UpdateRMSH = iw_frame.iu_string.nf_BuildUpdateString(dw_rmt_detail)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp37cr_upd_rmt_sched_detail"
istr_error_info.se_message = Space(71)

//If not iu_pas201.nf_pasp37cr_upd_rmt_sched_detail(istr_error_info, ls_UpdateRMSH, ls_output_string, ls_header_string) Then
//	if ls_output_string > '' Then
//		wf_unstring_output(ls_output_string)
//	End If
//	Return False
//end If

If not iu_ws_pas2.nf_pasp37gr(ls_UpdateRMSH, ls_output_string, ls_header_string,istr_error_info) Then
	if ls_output_string > '' Then
		wf_unstring_output(ls_output_string)
	End If
	Return False
end If

dw_rmt_detail.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_rmt_detail.SetItem(li_Counter, 'update_flag', ' ')
Next
dw_rmt_detail.ResetUpdate()
//1-13 jac
//dw_rmt_detail.SetSort("product_code A, product_state A, sched_date A, shift A")
dw_rmt_detail.SetSort("product_code A, product_state A, product_status A, sched_date A, shift A")
//
dw_rmt_detail.Sort()
dw_rmt_detail.GroupCalc()
dw_rmt_detail.SetFocus()
dw_rmt_detail.SetReDraw(True)

Return( True )
end function

public function boolean wf_unstring_output (string as_output_string);Date					ldt_start_date, &
						ldt_end_date, &
						ldt_temp

Integer				li_min_days, &
						li_max_days
						
Long					ll_rtn, &
						ll_nbrrows
						

String				ls_division_code, &
						ls_fab_product, &
						ls_product_state, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag, & 
						ls_sched_date, &
						ls_shift, &
						ls_product_status

u_string_functions	lu_string_functions
					
ll_nbrrows = dw_rmt_detail.RowCount()

dw_rmt_detail.SelectRow(0,False)

//find the row that matches the product code, product state, schedule date and shift
//exists already.

ll_nbrrows = dw_rmt_detail.RowCount()
ls_fab_product = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_product_state = lu_string_functions.nf_gettoken(as_output_string, '~t')
//1-13 jac
ls_product_status = lu_string_functions.nf_gettoken(as_output_string, '~t')
//
ls_sched_date = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_shift = lu_string_functions.nf_gettoken(as_output_string, '~t')

ls_SearchString = 	"product_code = '"+ ls_fab_product +&
							"' and product_state = '" + ls_product_state + "'" +&
							"' and product_status = '" + ls_product_status + "'" +&
							" and sched_date = date('" + ls_sched_date+ "')" +& 
							" and shift = '" + ls_shift + "'"
	
ll_rtn = dw_rmt_detail.Find  &
					( ls_SearchString, 1, ll_nbrrows)
		
If ll_rtn > 0 Then
	dw_rmt_detail.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(ll_rtn)
	dw_rmt_detail.SetRow(ll_rtn)
	dw_rmt_detail.SelectRow(ll_rtn, True)
	dw_rmt_detail.SetRedraw(True)
End If

as_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function

public function boolean wf_validate (long al_row);Date					ldt_start_date, &
						ldt_end_date, &
						ldt_temp

Integer				li_min_days, &
						li_max_days
						
Long					ll_rtn, &
						ll_nbrrows

String				ls_division_code, &
						ls_fab_product, &
						ls_product_state, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag, & 
						ls_sched_date, &
						ls_shift, &
						ls_product_status
					


ll_nbrrows = dw_rmt_detail.RowCount()

ls_temp = dw_rmt_detail.GetItemString(al_row, "product_code")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	MessageBox("Product Code", "Please enter a product code.  Product Code cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("product_code")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If not invuo_fab_product_code.uf_check_product(ls_temp) then
		If	invuo_fab_product_code.ib_error_occurred then
			iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
			Return False
		Else
			iw_frame.SetMicroHelp(ls_temp + " is an invalid Product Code")
			Return False
		End If
	End If
End If

ls_temp = dw_rmt_detail.GetItemString(al_row, "product_state")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a product state.  Product state cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("product_state")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	ls_fab_product = dw_rmt_detail.getitemstring(al_row, "product_code")
	if trim(ls_temp) <> "1" then
		if not invuo_fab_product_code.uf_check_product_state_rmt(ls_fab_product, Trim(ls_temp)) then	
			iw_Frame.SetMicroHelp(ls_temp + " is an invalid Product State for this Product")
			dw_rmt_detail.setfocus( )
			dw_rmt_detail.setcolumn("product_state") 
			dw_rmt_detail.SelectText(1, Len(ls_temp))	
			Return False
		end if
	end if
End If
//1-13 jac
ls_temp = dw_rmt_detail.GetItemString(al_row, "product_status")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	iw_Frame.SetMicroHelp("Please enter a product status.  Product status cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("product_status")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
end if
// Check that the sched date has a value and is > current date.
ldt_temp = dw_rmt_detail.GetItemDate(al_row, "sched_date")
IF IsNull(ldt_temp) or ldt_temp < Date('01/01/0001') Then 
	iw_Frame.SetMicroHelp("Please enter a Schedule Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("sched_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

IF ldt_temp < today() Then 
	iw_Frame.SetMicroHelp("Please enter a Schedule Date greater than or equal to current date.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("sched_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

IF ldt_temp > Date('12/31/2999') Then 
	iw_Frame.SetMicroHelp("Please enter a Schedule Date less than 12/31/2999")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("sched_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If


ls_update_flag = dw_rmt_detail.GetItemString(al_row, "update_flag")
ldt_start_date = dw_rmt_detail.GetItemDate(al_row, "start_date")
ldt_end_date = dw_rmt_detail.GetItemDate(al_row, "end_date")

IF IsNull(ldt_start_date) or ldt_start_date < Date('01/01/0001') Then 
	iw_Frame.SetMicroHelp("Please enter a From Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("start_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If
IF ldt_start_date > Date('12/31/2999') Then 
	iw_Frame.SetMicroHelp("Please enter a From Date less than 12/31/2999")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("start_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ldt_start_date > ldt_end_Date Then
	iw_Frame.SetMicroHelp("Please enter a From Date less than or equal to the To Date")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("start_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

// Check that the end date has a value 
IF IsNull(ldt_end_date) or ldt_end_date < Date('01/01/0001') Then 
	iw_Frame.SetMicroHelp("Please enter a To Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("end_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If
IF ldt_end_date > Date('12/31/2999') Then 
	iw_Frame.SetMicroHelp("Please enter a To Date less than 12/31/2999")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("end_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

// Check that the start date and end date are less than the sched date
IF ldt_start_date > ldt_temp or ldt_end_date > ldt_temp Then 
	iw_Frame.SetMicroHelp("Please enter Production Dates less than the Schedule Date")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("start_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

//check to see if the product code, product state, product status, schedule date and shift
//exists already.

ll_nbrrows = dw_rmt_detail.RowCount()
ls_fab_product = dw_rmt_detail.GetItemString(al_row, "product_code")
ls_product_state = dw_rmt_detail.GetItemString(al_row, "product_state")
//1-13 jac
ls_product_status = dw_rmt_detail.GetItemString(al_row, "product_status")
//
ls_sched_date = String(ldt_temp)
ls_shift = dw_rmt_detail.GetItemString(al_row, "shift")
//1-13 jac added product status to search string
//	"' and product_status = '" + ls_product_status + "'" +&
If ll_nbrrows > 1 Then
	ls_SearchString = 	"product_code = '"+ ls_fab_product +&
							"' and product_state = '" + ls_product_state + "'" +&		
							" and product_status = '" + ls_product_status + "'" +& 
							" and sched_date = date('" + ls_sched_date+ "')" +& 
							" and shift = '" + ls_shift + "'" 						
	
	CHOOSE CASE al_row 
		CASE 1
			ll_rtn = dw_rmt_detail.Find  &
					( ls_SearchString, al_row + 1, ll_nbrrows)
		CASE 2 to (ll_nbrrows - 1)
			ll_rtn = dw_rmt_detail.Find ( ls_SearchString, al_row - 1, 1)
			If ll_rtn = 0 Then ll_rtn = dw_rmt_detail.Find  &
				(ls_SearchString, al_row + 1, ll_nbrrows)
		CASE ll_nbrrows 
			ll_rtn = dw_rmt_detail.Find ( ls_SearchString, al_row - 1, 1)
	END CHOOSE

	If ll_rtn > 0 Then
		iw_Frame.SetMicroHelp( "There are duplicate products with the" + &
  						" same Product State, Product Status, Schedule Date and Shift.")
		dw_rmt_detail.SetRedraw(False)
		dw_rmt_detail.ScrollToRow(al_row)
		dw_rmt_detail.SetColumn("start_date")
		dw_rmt_detail.SetRow(al_row)
		dw_rmt_detail.SelectRow(ll_rtn, True)
		dw_rmt_detail.SelectRow(al_row, True)
		dw_rmt_detail.SetRedraw(True)
		Return False
	End If
End If
						
Return True
end function

public function boolean wf_retrieve ();Integer	li_ret

String	ls_input, &
			ls_output_values, &
			ls_product
		
			
Long		ll_value, &
			ll_rtn

u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false
ls_input = Message.StringParm
If Not ib_ReInquire Then
	OpenWithParm(w_raw_mat_det_sched_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")
//if ls_input = '' then
//1-13 jac added product status to input string
is_debug = This.dw_header.GetItemString(1, 'fab_product_code')
ls_input = This.dw_header.GetItemString(1, "plant_code") + '~t' + &
				This.dw_header.GetItemString(1, "fab_product_code") + '~t' + &
				string(This.dw_header.GetItemnumber(1, "product_state")) + '~t' + &
				This.dw_header.GetItemString(1, "product_status") + '~t' + &
				String(This.dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~r~n'
//end if
is_input = ls_input

//li_ret = iu_pas201.nf_pasp36cr_inq_rmt_sched_detail(istr_error_info, &
//									is_input, &
//									ls_output_values)
								

li_ret = iu_ws_pas2.nf_pasp36gr(is_input, ls_output_values,istr_error_info)

This.dw_rmt_detail.Reset()

If li_ret = 0 Then
	This.dw_rmt_detail.ImportString(ls_output_values)

	ll_value = dw_rmt_detail.RowCount()
	If ll_value < 0 Then ll_value = 0

	dw_rmt_detail.ResetUpdate()

	IF ll_value > 0 THEN
 		dw_rmt_detail.SetFocus()
		dw_rmt_detail.ScrollToRow(1)
		dw_rmt_detail.SetColumn( "start_date" )
		dw_rmt_detail.TriggerEvent("RowFocusChanged")
	END IF

	SetMicroHelp(String(ll_value) + " rows retrieved")
	ib_good_product = True
	This.SetRedraw( True )
	iw_frame.im_menu.mf_enable('m_save')
	iw_frame.im_menu.mf_enable('m_new')
	iw_frame.im_menu.mf_enable('m_addrow')
	iw_frame.im_menu.mf_enable('m_deleterow')
	Return( True )
Else
	ll_rtn = iw_frame.im_menu.mf_disable('m_save')
	ll_rtn = iw_frame.im_menu.mf_disable('m_new')
	ll_rtn = iw_frame.im_menu.mf_disable('m_addrow')
	ib_good_product = False
	This.SetRedraw(True)
	Return( False )
End If


return true

end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)
If IsValid(iu_ws_pas2) Then Destroy(iu_ws_pas2)




end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_deleterow')



	

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

If not ib_good_product Then
	iw_frame.im_menu.mf_disable('m_new')
	iw_frame.im_menu.mf_disable('m_addrow')
	iw_frame.im_menu.mf_disable('m_save')
	iw_frame.im_menu.mf_disable('m_deleterow')
End If

end event

event open;call super::open;Long					ll_len
String				ls_text, &
						ls_option, & 
						ls_temp, &
						ls_input_string, &
						ls_input_plant, &
						ls_input_product, &
						ls_input_state, &
						ls_long_description, &
						ls_location_name, ls_input_date, &
						ls_input_status
Long					ls_len
Date					ldt_input_date


dw_header.InsertRow(0)
dw_rmt_detail.InsertRow(0)
dw_header.ib_Updateable=False
ib_good_product = False
//ib_reinquire = False

ls_input_string = Message.StringParm
//1-13 jac added product status to input string
ll_len = len(ls_input_string)
if ll_len > 1 then
	Do While Not iw_frame.iu_string.nf_IsEmpty(ls_input_string)
		ls_input_plant = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
		ls_input_product = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
		ls_input_state = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
		ls_input_status = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
		ls_input_date = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
	Loop	
		
	SELECT locations.LOCATION_NAME
    INTO :ls_location_name
  	 FROM locations
  	 WHERE ( locations.LOCATION_CODE = :ls_input_plant);
	
	SELECT sku_products.LONG_DESCRIPTION  
	INTO :ls_long_description 
  	 FROM sku_products 
  	 WHERE ( sku_products.SKU_PRODUCT_CODE = :ls_input_product);
	 
	dw_header.SetItem(1, "plant_code", ls_input_plant)
	dw_header.SetItem(1, 'plant_description', ls_location_name)
	dw_header.SetItem(1, "fab_product_code", ls_input_product)
	dw_header.SetItem(1, 'fab_product_descr', ls_long_description)
	dw_header.SetItem(1, "product_state", integer(ls_input_state))
	//1-13 jac
	dw_header.SetItem(1, "product_status", ls_input_status)
	//
	dw_header.SetItem(1, "sched_date", date(ls_input_date))
	dw_header.acceptText()
	if message.StringParm = 'OK' then
		ib_reinquire = False
	else
		ib_reinquire = True
	End if	
	ib_good_product = False
	dw_header.ib_Updateable=False
	Message.StringParm = ''
End if







end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

iu_ws_pas2 = Create u_ws_pas2
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Raw Material Default Schedule"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")

end event

on w_raw_mat_det_sched.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_rmt_detail=create dw_rmt_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_rmt_detail
end on

on w_raw_mat_det_sched.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_rmt_detail)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Product'
		Message.StringParm = dw_header.GetItemString(1, 'fab_product_code')
	Case 'Product_Descr'
		Message.StringParm = dw_header.GetItemString(1, 'fab_product_descr')
	Case 'State'
		Message.StringParm = String(dw_header.GetItemNumber(1, 'product_state'))
	//1-13 jac
   Case 'product_status'
		Message.StringParm = dw_header.GetItemString(1, 'product_status')
	//
	Case 'sched_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
		
End Choose

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant_Desc' 
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Product'
		dw_header.SetItem(1, 'fab_product_code', as_value)
	Case 'Product_Desc'
		dw_header.SetItem(1, 'fab_product_descr', as_value)
	Case 'State'
		dw_header.SetItem(1, 'product_state', integer(as_value))
	//1-13 jac
	Case 'Product_status'
		dw_header.SetItem(1, 'product_status', as_value)
	//
	Case 'sched_date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
End Choose

end event

event resize;call super::resize;integer li_x		
integer li_y	
integer ly_height = 115


li_x = (dw_rmt_detail.x * 2) + 30 
//li_y = dw_rmt_detail.y + 115

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if


if il_BorderPaddingHeight > ly_height Then
                ly_height = il_BorderPaddingHeight
End If


li_y = dw_rmt_detail.y + ly_height



if width > li_x Then
	dw_rmt_detail.width	= width - li_x
end if

if height > li_y then
	dw_rmt_detail.height	= height - li_y
end if
end event

type dw_header from u_base_dw_ext within w_raw_mat_det_sched
integer y = 4
integer width = 1719
integer height = 316
integer taborder = 0
string dataobject = "d_rmt_sched_header"
boolean controlmenu = true
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False

end event

type dw_rmt_detail from u_base_dw_ext within w_raw_mat_det_sched
integer y = 364
integer width = 2665
integer height = 1152
integer taborder = 20
string dataobject = "d_rmt_sched_det_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return



end event

event constructor;call super::constructor;ib_updateable = True
This.SetItem(1, 'start_date', Today())
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state, ldwc_status

dw_rmt_detail.GetChild('uom', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("RAWUOM")

dw_rmt_detail.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()

//1-13 jac
dw_rmt_detail.GetChild('product_status', ldwc_status)
ldwc_status.SetTransObject(SQLCA)
ldwc_status.Retrieve("PRODSTAT")
ldwc_status.SetSort("type_code")
ldwc_status.Sort()
//

dw_rmt_detail.GetChild('shift', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("SHIFT")
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_GetText, &
				ls_temp
Date			ldt_temp	
nvuo_pa_business_rules	u_rule


is_ColName = GetColumnName()
ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0

CHOOSE CASE is_ColName
	CASE "product_code"
		If not invuo_fab_product_code.uf_check_product(data) then
			If	invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
				Return 1
			Else
				iw_frame.SetMicroHelp(data + " is an invalid Product Code")
				Return 1
			End If
		End If
	CASE "product_state"
		string ls_product
		ls_product = this.getitemstring(row, "product_code")
		if trim(data) <> "1" then
			if not invuo_fab_product_code.uf_check_product_state_rmt(ls_product, Trim(data)) then	
				iw_Frame.SetMicroHelp(data + " is an invalid Product State for this Product")
				this.setfocus( )
				this.setcolumn("product_state") 
				This.SelectText(1, Len(data))	
				Return 1
			end if
		end if
	CASE "sched_date" 
		Date ldt_sched_Date
		ldt_sched_date = Date(data)
		If ldt_sched_date < today() Then
			iw_Frame.SetMicroHelp("Schedule date must be greater than or equal to current date.")
			this.setfocus( )
			this.setcolumn("sched_date") 
			This.SelectText(1, Len(data))	
			Return 1
		end if
	CASE "start_date", "end_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
	Case "days_old_min", "days_old_max"
		If Not IsNumber(ls_GetText) Then
			iw_frame.SetMicroHelp("Days must be a number")
			This.selecttext(1,100)
			return 1
		End if
		If Real(ls_GetText) < 0 Then
			iw_frame.SetMicroHelp("Days cannot be negative")
			This.selecttext(1,100)
			return 1
		End if
	Case "quantity"
		If Not IsNumber(ls_GetText) Then
			iw_frame.SetMicroHelp("Quantity must be a number")
			This.selecttext(1,100)
			return 1
		End if
		If Real(ls_GetText) < 0 Then
			iw_frame.SetMicroHelp("Quantity cannot be negative")
			This.selecttext(1,100)
			return 1
		End if
END CHOOSE

// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

//parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;return (1)
end event

