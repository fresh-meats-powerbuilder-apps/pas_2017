﻿$PBExportHeader$w_raw_mat_sched_parameters_inq.srw
forward
global type w_raw_mat_sched_parameters_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_raw_mat_sched_parameters_inq
end type
type dw_product from u_fab_product_code within w_raw_mat_sched_parameters_inq
end type
type dw_shift from u_shift within w_raw_mat_sched_parameters_inq
end type
end forward

global type w_raw_mat_sched_parameters_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 1719
integer height = 780
string title = "Raw Material Schedule Parameters Inquire"
long backcolor = 67108864
event begindatechanged ( )
dw_plant dw_plant
dw_product dw_product
dw_shift dw_shift
end type
global w_raw_mat_sched_parameters_inq w_raw_mat_sched_parameters_inq

type variables
Boolean		ib_valid_return
w_base_sheet	iw_parent

string	is_begin_date
end variables

event ue_postopen;call super::ue_postopen;Int							li_pos, &
								li_ret

String						ls_header, &
								ls_temp, &
								ls_product_code, &
								ls_product_descr,ls_state, &
								ls_shift, &
								ls_status

u_string_functions		lu_string

Window   lw_parent

lw_parent = this.parentwindow()
This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)

iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If

iw_parent.Event ue_Get_Data('Product')
ls_product_code = Message.StringParm
iw_parent.Event ue_Get_Data('Product_Descr')
ls_product_descr = Message.StringParm
iw_parent.Event ue_Get_Data('State')
ls_state = Message.StringParm
//1-13 jac
iw_parent.Event ue_Get_Data('product_status')
ls_status = Message.StringParm
//

if Not lu_string.nf_IsEmpty(ls_state) Then dw_product.uf_set_product_state(ls_state)
dw_product.uf_set_product_code( ls_product_code, ls_product_descr)
//1-13 jac
if Not lu_string.nf_IsEmpty(ls_status) Then dw_product.uf_set_product_status(ls_status)
//
iw_parent.Event ue_Get_Data('Shift')
ls_shift = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_shift) Then dw_shift.uf_set_shift(ls_shift)

If dw_plant.RowCount() < 1 Then 
	dw_plant.InsertRow(0)
ElseIf Not lu_string.nf_IsEmpty(dw_plant.GetItemString(1,1)) Then
	dw_product.setfocus( )
End if


end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

if dw_product.rowcount( ) = 0 Then
	dw_product.insertrow( 0)
End If

if dw_shift.rowcount( ) = 0 Then
	dw_shift.insertrow( 0)
End If

end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

on w_raw_mat_sched_parameters_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_product=create dw_product
this.dw_shift=create dw_shift
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_product
this.Control[iCurrent+3]=this.dw_shift
end on

on w_raw_mat_sched_parameters_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_product)
destroy(this.dw_shift)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date	ldt_temp

String	ls_temp,ls_temp1
Long 		ll_rtn

If dw_plant.AcceptText() = -1 Then 
	dw_plant.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End if

If not dw_product.uf_validate_rmt( ) Then
	dw_product.setfocus()
	return
End If

If iw_frame.iu_string.nf_IsEmpty(dw_product.GetItemString(1, "fab_product_code")) Then
	iw_frame.SetMicroHelp("Product Code is a required field")
	dw_product.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_shift.GetItemString(1, "shift")) Then
	iw_frame.SetMicroHelp("Shift is a required field")
	dw_shift.SetFocus()
	return
End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant_Desc', ls_temp)

ls_temp = dw_product.uf_get_product_code( )
iw_parent.Event ue_Set_Data('Product', ls_temp)

ls_temp = dw_product.uf_get_product_desc( )
iw_parent.Event ue_Set_Data('Product_Desc', ls_temp)

ls_temp = dw_product.uf_get_product_state( )
iw_parent.Event ue_Set_Data('State', ls_temp)
//1-13 jac
ls_temp = dw_product.uf_get_product_status( )
iw_parent.Event ue_Set_Data('Product_status', ls_temp)
//
ls_temp = dw_shift.uf_get_shift( )
iw_parent.Event ue_Set_Data('Shift', ls_temp)

ib_valid_return = True
Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_raw_mat_sched_parameters_inq
boolean visible = false
integer x = 1696
integer y = 392
integer taborder = 80
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_raw_mat_sched_parameters_inq
integer x = 818
integer y = 496
integer taborder = 70
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_raw_mat_sched_parameters_inq
integer x = 315
integer y = 496
integer taborder = 60
end type

type dw_plant from u_plant within w_raw_mat_sched_parameters_inq
integer x = 96
integer y = 12
integer taborder = 10
end type

type dw_product from u_fab_product_code within w_raw_mat_sched_parameters_inq
integer x = 41
integer y = 88
integer width = 1627
integer height = 392
integer taborder = 20
boolean bringtotop = true
boolean ib_required = false
end type

type dw_shift from u_shift within w_raw_mat_sched_parameters_inq
integer x = 238
integer y = 376
integer height = 76
integer taborder = 20
boolean bringtotop = true
end type

