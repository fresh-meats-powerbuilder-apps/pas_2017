﻿$PBExportHeader$w_auto_load_list_maint_except.srw
forward
global type w_auto_load_list_maint_except from w_base_sheet_ext
end type
type dw_auto_load_list_except_detail_rpt from u_base_dw_ext within w_auto_load_list_maint_except
end type
type dw_loads_not_transmitted_prt from u_base_dw_ext within w_auto_load_list_maint_except
end type
type dw_auto_load_list_maint_detail from u_base_dw_ext within w_auto_load_list_maint_except
end type
type dw_header_total_combos from u_load_list_total_combo within w_auto_load_list_maint_except
end type
type dw_header_total_boxes from u_load_list_total_box within w_auto_load_list_maint_except
end type
type dw_header_load_list_opt from u_load_list_by_option within w_auto_load_list_maint_except
end type
type dw_header from u_base_dw_ext within w_auto_load_list_maint_except
end type
type dw_1 from u_base_dw_ext within w_auto_load_list_maint_except
end type
type tab_1 from u_load_list_tab within w_auto_load_list_maint_except
end type
type tab_1 from u_load_list_tab within w_auto_load_list_maint_except
end type
end forward

global type w_auto_load_list_maint_except from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 3118
integer height = 1840
string title = "Load List Maintenance"
long backcolor = 12632256
event ue_sendloadlist ( )
event ue_prtloadlistloc ( )
event ue_prtloadlistplt ( )
event ue_prtloadlisttrf ( )
event ue_prtpartloadlistloc ( )
event ue_prtpartloadlisttrf ( )
event ue_prtpartloadlistplt ( )
event ue_emailpartloadlist ( )
event ue_faxpartloadlist ( )
event ue_faxloadlist ( )
event ue_emailloadlist ( )
dw_auto_load_list_except_detail_rpt dw_auto_load_list_except_detail_rpt
dw_loads_not_transmitted_prt dw_loads_not_transmitted_prt
dw_auto_load_list_maint_detail dw_auto_load_list_maint_detail
dw_header_total_combos dw_header_total_combos
dw_header_total_boxes dw_header_total_boxes
dw_header_load_list_opt dw_header_load_list_opt
dw_header dw_header
dw_1 dw_1
tab_1 tab_1
end type
global w_auto_load_list_maint_except w_auto_load_list_maint_except

type variables
u_pas201		iu_pas201
s_error		istr_error_info

boolean		ib_data_modified, &
				ib_first_time_in = true

integer		ii_keystyped, ii_last_row_highlighted, ii_active_tab
string		is_whatwastyped, is_email_fax_ind, is_report_type
u_ws_pas5		iu_ws_pas5
end variables

forward prototypes
public subroutine wf_replace_rows (ref datawindow adw_replace_dw, string as_replace_string)
public function boolean wf_check_for_changes ()
private subroutine wf_search_row (datawindow adw_search_detail, datawindow adw_search_key, string as_data)
public subroutine wf_inq_after_update ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

event ue_sendloadlist();String	ls_input

Integer	li_rtn

	
ls_input = 	dw_header.GetItemString( 1, "load_list_type") + '~t' + &
				dw_header.GetItemString( 1, "location_code") + '~t' + &
				String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
				dw_header.GetItemString( 1, "division_code") + '~t' + &
				dw_header.getitemstring( 1, "complex_code") + '~t' + &
				dw_header.getitemstring( 1, "plant_type") + '~t' + &
				dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
				'Y' + '~t' + '1' + '~t' + 'Y' + '~t' + '05' + '~t' 

istr_error_info.se_event_name = "ue_sendloadlist"			  

SetPointer(HourGlass!)

//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
li_rtn = iu_ws_pas5.nf_pasp82fr(istr_error_info, ls_input)
												
SetPointer(Arrow!)												



end event

event ue_prtloadlistloc();String	ls_input

Integer	li_rtn

ls_input = 	dw_header.GetItemString( 1, "load_list_type") + '~t' + &
				dw_header.GetItemString( 1, "location_code") + '~t' + &
				String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
				dw_header.GetItemString( 1, "division_code") + '~t' + &
				dw_header.getitemstring( 1, "complex_code") + '~t' + &
				dw_header.getitemstring( 1, "plant_type") + '~t' + &
				dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
				'Y' + '~t' + '1' + '~t' + 'Y' + '~t' + '03' + '~r~t' 

			  
istr_error_info.se_event_name = "ue_prtloadlistloc"			  

SetPointer(HourGlass!)

//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, &
//												ls_input)
li_rtn = iu_ws_pas5.nf_pasp82fr(istr_error_info, ls_input)
												
SetPointer(Arrow!)					
end event

event ue_prtloadlistplt();String	ls_input

Integer	li_rtn

li_rtn = messagebox ('Confirm Full Load List Send', 'Do you want to send the Full Load List to the Plant?', Question!, YesNo!, 2)

If li_rtn = 1 then	
	ls_input = 	dw_header.GetItemString( 1, "load_list_type") + '~t' + &
					dw_header.GetItemString( 1, "location_code") + '~t' + &
					String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
					dw_header.GetItemString( 1, "division_code") + '~t' + &
					dw_header.getitemstring( 1, "complex_code") + '~t' + &
					dw_header.getitemstring( 1, "plant_type") + '~t' + &
					dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
					'Y' + '~t' + '1' + '~t' + 'Y' + '~t' + '05' + '~t' 
	 
	istr_error_info.se_event_name = "ue_sendloadlist"			  
	
	SetPointer(HourGlass!)
	
	//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
	li_rtn = iu_ws_pas5.nf_pasp82fr(istr_error_info, ls_input)
													
	SetPointer(Arrow!)	
end if	

end event

event ue_prtloadlisttrf();String	ls_input

Integer	li_rtn

ls_input = 	dw_header.GetItemString( 1, "load_list_type") + '~t' + &
				dw_header.GetItemString( 1, "location_code") + '~t' + &
				String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
				dw_header.GetItemString( 1, "division_code") + '~t' + &
				dw_header.getitemstring( 1, "complex_code") + '~t' + &
				dw_header.getitemstring( 1, "plant_type") + '~t' + &
				dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
				'Y' + '~t' + '1' + '~t' + 'Y' + '~t' + '06' + '~t' 

			  
istr_error_info.se_event_name = "ue_sendloadlist"			  

SetPointer(HourGlass!)

//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, &
//												ls_input)
li_rtn = iu_ws_pas5.nf_pasp82fr(istr_error_info, ls_input)
												
SetPointer(Arrow!)	
end event

event ue_prtpartloadlistloc();String	ls_input
Integer	li_rtn

ls_input = 	dw_header.GetItemString( 1, "load_list_type") + '~t' + &
				dw_header.GetItemString( 1, "location_code") + '~t' + &
				String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
				dw_header.GetItemString( 1, "division_code") + '~t' + &
				dw_header.getitemstring( 1, "complex_code") + '~t' + &
				dw_header.getitemstring( 1, "plant_type") + '~t' + &
				dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
				'Y' + '~t' + '1' + '~t' + 'Y' + '~t' + '04' + '~t' 
			  
istr_error_info.se_event_name = "ue_sendloadlist"			  
SetPointer(HourGlass!)
//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
li_rtn = iu_ws_pas5.nf_pasp82fr(istr_error_info, ls_input)
SetPointer(Arrow!)

end event

event ue_prtpartloadlisttrf();String	ls_input
Integer	li_rtn

ls_input = 	dw_header.GetItemString( 1, "load_list_type") + '~t' + &
				dw_header.GetItemString( 1, "location_code") + '~t' + &
				String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
				dw_header.GetItemString( 1, "division_code") + '~t' + &
				dw_header.getitemstring( 1, "complex_code") + '~t' + &
				dw_header.getitemstring( 1, "plant_type") + '~t' + &
				dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
				'Y' + '~t' + '1' + '~t' + 'Y' + '~t' + '09' + '~t' 

istr_error_info.se_event_name = "ue_sendloadlist"			  
SetPointer(HourGlass!)
//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
li_rtn = iu_ws_pas5.nf_pasp82fr(istr_error_info, ls_input)
SetPointer(Arrow!)	
end event

event ue_prtpartloadlistplt();String	ls_input
Integer	li_rtn

ls_input = 	dw_header.GetItemString( 1, "load_list_type") + '~t' + &
				dw_header.GetItemString( 1, "location_code") + '~t' + &
				String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
				dw_header.GetItemString( 1, "division_code") + '~t' + &
				dw_header.getitemstring( 1, "complex_code") + '~t' + &
				dw_header.getitemstring( 1, "plant_type") + '~t' + &
				dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
				'Y' + '~t' + '1' + '~t' + 'Y' + '~t' + '07' + '~t' 
			  
istr_error_info.se_event_name = "ue_sendloadlist"			  
SetPointer(HourGlass!)
//li_rtn = iu_pas201.nf_pasp82br_send_load_list(istr_error_info, ls_input)
li_rtn = iu_ws_pas5.nf_pasp82fr(istr_error_info, ls_input)
SetPointer(Arrow!)

end event

event ue_emailpartloadlist();String	ls_return_string

is_email_fax_ind = 'E'

is_report_type = '12'

OpenWithParm(w_fax_email_response, This)
end event

event ue_faxpartloadlist();String	ls_return_string

is_email_fax_ind = 'F'

is_report_type = '13'

OpenWithParm(w_fax_email_response, This)
end event

event ue_faxloadlist();String	ls_return_string

is_email_fax_ind = 'F'

is_report_type = '11'

OpenWithParm(w_fax_email_response, This)
end event

event ue_emailloadlist();String	ls_return_string

is_email_fax_ind = 'E'

is_report_type = '10'

OpenWithParm(w_fax_email_response, This)
end event

public subroutine wf_replace_rows (ref datawindow adw_replace_dw, string as_replace_string);//DataStore						lds_datastore
//Integer							li_count
//String							ls_selected
//u_string_functions	lu_string_functions
//
//lds_datastore = CREATE datastore 
//lds_datastore.DataObject = adw_replace_dw.DataObject
//lds_DataStore.Object.Data = adw_replace_dw.Object.Data.Selected
//
//ls_selected = lds_DataStore.Object.DataWindow.Data
//
//IF lu_string_functions.nf_isempty(ls_selected) Then
//	adw_replace_dw.Reset()
//	adw_replace_dw.ImportString(as_replace_string)
//	Return
//ENd IF 
//lds_DataStore.Reset()
//li_count = lds_datastore.ImportString(as_replace_string)
//If li_Count <  1 Then
//		// For some reason the Data did not import.. Put back the rows that were there selected
//		li_count = lds_datastore.ImportString(ls_selected)
//END IF
//adw_replace_dw.Object.Data.Selected = lds_DataStore.Object.Data
//Destroy lds_DataStore
//Return
////////////////////////////////////////////////
DataStore						lds_datastore
Long								ll_arowcount, ll_arow, ll_hold
Integer							li_count
String							ls_selected						
u_string_functions			lu_string_functions

lds_datastore = CREATE datastore 
lds_datastore.DataObject = adw_replace_dw.DataObject
lds_DataStore.Object.Data = adw_replace_dw.Object.Data.Selected

ls_selected = lds_DataStore.Object.DataWindow.Data
//bad if statement you loose all lines that are not selected possibly delete the reset
IF lu_string_functions.nf_isempty(ls_selected) Then
	adw_replace_dw.Reset()
	adw_replace_dw.ImportString(as_replace_string)
	Return
End if 
lds_DataStore.Reset()
li_count = lds_datastore.ImportString(as_replace_string)
If li_Count <  1 Then
		//For some reason the Data did not import.. Put back the rows that were there selected
		li_count = lds_datastore.ImportString(ls_selected)
END IF
////////////////sem
ll_arowcount = lds_DataStore.RowCount()
ll_arow = adw_replace_dw.GetSelectedRow(0)
If ll_arow > 0 Then
	ll_hold = 1
	Do
		If ll_hold > ll_arowcount Then
			MessageBox("u_project_functions","nf_replace_rows")
		Else
			adw_replace_dw.RowsDiscard(ll_arow, ll_arow, Primary!)
			lds_DataStore.RowsCopy(ll_hold, ll_hold, Primary!, adw_replace_dw, ll_arow, Primary!)
			adw_replace_dw.SelectRow(ll_arow, True)
			ll_hold += 1
			ll_arow = adw_replace_dw.GetSelectedRow(ll_arow)
		End If
	Loop Until ll_arow  <= 0
End If
////////////////sem
//adw_replace_dw.Object.Data.Selected = lds_DataStore.Object.Data
//Destroy lds_DataStore
Return
end subroutine

public function boolean wf_check_for_changes ();Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return false

return True
end function

private subroutine wf_search_row (datawindow adw_search_detail, datawindow adw_search_key, string as_data);String	ls_found_primary_order, &
			ls_primary_order, &
			ls_find

integer	li_row, li_rowcount, li_this_row

datastore	lds_detail

ii_keystyped ++
if len(as_data) = 0 then
	li_row = adw_search_detail.GetSelectedRow(0)
	if li_row > 0 then
		adw_search_detail.selectrow(li_row, false)
	end if
	return
end if

If ii_keystyped > Len(as_data) Then ii_keystyped = Len(as_data)
If is_whatwastyped <> as_data Then
	is_whatwastyped += Right(as_data,1)
End if

li_row = 1 
if adw_search_detail.rowcount() = 0 then return

lds_detail = create datastore
choose case ii_active_tab
	case 1, 2
		choose case ii_active_tab
			case 1
				lds_detail.DataObject = 'd_auto_load_list_maint_detail'
			case 2
				lds_detail.DataObject = 'd_auto_load_list_except_detail'
		end choose
		lds_detail.importstring(adw_search_detail.Object.DataWindow.Data)
		lds_detail.setsort("primary_order A")
		lds_detail.sort()
		li_rowcount = lds_detail.rowcount()
		do until as_data <= Left(trim(lds_detail.GetItemString(li_row, "primary_order")), Len(as_data)) or li_row = li_rowcount
			li_row ++
		loop 
		ls_find = 'primary_order = "' + lds_detail.getitemstring(li_row,"primary_order") + '"'
		
	case 3
		lds_detail.DataObject = 'd_loads_not_transmitted'
		lds_detail.importstring(adw_search_detail.Object.DataWindow.Data)
		lds_detail.setsort("load_number A")
		lds_detail.sort()
		li_rowcount = lds_detail.rowcount()
		do until as_data <= Left(trim(lds_detail.GetItemString(li_row, "load_number")), Len(as_data)) or li_row = li_rowcount
			li_row ++
		loop 
		ls_find = 'load_number = "' + lds_detail.getitemstring(li_row,"load_number") + '"'
		
end choose

li_this_row = adw_search_detail.find(ls_find, 1, adw_search_detail.rowcount())

adw_search_detail.selectrow(adw_search_detail.GetSelectedRow(0), false)
adw_search_detail.selectrow(li_this_row, true)
adw_search_detail.ScrollToRow ( li_this_row )
adw_search_key.setfocus()
adw_search_detail.SetRedraw(True)
adw_search_key.ResetUpdate()
//adw_detail.resetupdate()

end subroutine

public subroutine wf_inq_after_update ();String		ls_input, &
				ls_maint_string, &
				ls_except_string, &
				ls_load_totals
				
Integer		li_rtn

long			ll_rowcount_detail, ll_rowcount_ds, ll_rowcount, ll_count

datastore	lds_total_loads

////If Not wf_Check_for_changes() Then return false
//
//OpenWithParm(w_auto_load_list_maint_inq, This)
//IF Message.StringParm = "Cancel" then return False 
//
//This.SetRedraw(False)

ls_input = dw_header.GetItemString( 1, "location_code") + '~t' + &
			  String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
			  dw_header.GetItemString( 1, "load_list_type") + '~t' + &
			  dw_header.GetItemString( 1, "division_code") + '~t' + &
			  dw_header.GetItemString( 1, "calculate_load_times_ind") + '~t' +&
			  dw_header.GetItemString( 1, "start_time") + '~t' +&
			  String(dw_header.GetItemNumber( 1, "loads_per_hour")) + '~t' + &
			  dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
			  dw_header.getitemstring( 1, "complex_code") + '~t' + & 
			  dw_header.getitemstring( 1, "plant_type") + '~r~n'
			  
istr_error_info.se_event_name = "wf_retrieve"			  

//li_rtn = iu_pas201.nf_pasp78br_inq_load_list_maint(istr_error_info, &
//												ls_input, &
//												ls_maint_string, &
//												ls_except_string, &
//												ls_load_totals)
li_rtn = iu_ws_pas5.nf_pasp78fr(istr_error_info, &
												ls_input, &
												ls_maint_string, &
												ls_except_string, &
												ls_load_totals)

lds_total_loads = Create DataStore
lds_total_loads.DataObject = 'd_auto_load_list_totals'
ll_rowcount_ds = lds_total_loads.importstring(ls_load_totals)
for ll_count = 1 to ll_rowcount
	dw_header.setitem(ll_count,"total_number_of_loads",lds_total_loads.getitemnumber(ll_rowcount,"total_num_of_loads"))
	dw_header.setitem(ll_count,"total_loads_not_transmitted",lds_total_loads.getitemnumber(ll_rowcount,"total_num_transmitted"))
next

tab_1.tabpage_1.dw_maintenance.Reset()
tab_1.tabpage_1.dw_maintenance.ImportString(ls_maint_string)
tab_1.tabpage_1.dw_maintenance.SetSort("load_time A, deadline_depart_date A, deadline_depart_time A")
tab_1.tabpage_1.dw_maintenance.Sort()

tab_1.tabpage_1.dw_maintenance.SetRedraw(True)
tab_1.tabpage_1.dw_maintenance.ResetUpdate()

//tab_1.tabpage_2.dw_exceptions.Reset()
//tab_1.tabpage_2.dw_exceptions.ImportString(ls_except_string)
//tab_1.tabpage_2.dw_exceptions.SetSort("load_time A, deadline_depart_date A, deadline_depart_time A, &
//													primary_order A, sales_order A, line_number A")
//tab_1.tabpage_2.dw_exceptions.Sort()


//tab_1.SelectedTab = 1
tab_1.tabpage_1.dw_maintenance.SetRedraw(True)
//tab_1.tabpage_2.dw_exceptions.SetRedraw(True)
//tab_1.tabpage_1.dw_maintenance.SetFocus()
//
//This.SetRedraw(True)
				

end subroutine

public function boolean wf_retrieve ();String		ls_input, &
				ls_maint_string, &
				ls_except_string, &
				ls_load_totals, &
				ls_calculate_type, &
				ls_calculate_ind, &
				ls_inq_plt_cplx
				
Integer		li_rtn

long			ll_rowcount_detail, ll_rowcount_ds, ll_rowcount, ll_count

datastore	lds_total_loads

If Not wf_Check_for_changes() Then return false

OpenWithParm(w_auto_load_list_maint_inq, This)
IF Message.StringParm = "Cancel" then return False 

ls_calculate_type = dw_header.GetItemString( 1, "calculate_load_times_ind")
If ls_calculate_type = 'No Calculation' Then
	ls_calculate_ind = 'N'
Else
	If ls_calculate_type = 'With Availability' Then
		ls_calculate_ind = 'W'
	Else
		If ls_calculate_type = 'Case Ready' Then
			ls_calculate_ind = 'C'
		End If
	End If
End If

string ls_debug
ls_debug = dw_header.describe("DataWindow.Data")

ls_input = dw_header.GetItemString( 1, "location_code") + '~t' + &
			  String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
			  dw_header.GetItemString( 1, "load_list_type") + '~t' + &
			  dw_header.GetItemString( 1, "division_code") + '~t' + &
			  ls_calculate_ind + '~t' + &
			  dw_header.GetItemString( 1, "start_time") + '~t' + &
			  String(dw_header.GetItemNumber( 1, "loads_per_hour")) + '~t' + &
			  dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
			  dw_header.getitemstring( 1, "complex_code") + '~t' + &
			  dw_header.getitemstring( 1, "plant_type") + '~t' + &
			  dw_header.getitemstring( 1, "grind_start_time") + '~t' + &
			  dw_header.getitemstring( 1, "grind_load_time") + '~t' + &
			  dw_header.getitemstring( 1, "other_start_time") + '~t' + &
			  dw_header.getitemstring( 1, "other_load_time") + '~t' + '~r~t'

istr_error_info.se_event_name = "wf_retrieve"			  

//li_rtn = iu_pas201.nf_pasp78br_inq_load_list_maint(istr_error_info, &
//												ls_input, &
//												ls_maint_string, &
//												ls_except_string, &
//												ls_load_totals)
li_rtn = iu_ws_pas5.nf_pasp78fr(istr_error_info, &
												ls_input, &
												ls_maint_string, &
												ls_except_string, &
												ls_load_totals)

lds_total_loads = Create DataStore
lds_total_loads.DataObject = 'd_auto_load_list_totals'
ll_rowcount_ds = lds_total_loads.importstring(ls_load_totals)
for ll_count = 1 to ll_rowcount_ds
	dw_header.setitem(ll_count,"total_number_of_loads",lds_total_loads.getitemnumber(ll_rowcount_ds,"total_num_of_loads"))
	dw_header.setitem(ll_count,"total_loads_not_transmitted",lds_total_loads.getitemnumber(ll_rowcount_ds,"total_num_transmitted"))
	dw_header_total_boxes.setitem(ll_count,"total_num_boxes", lds_total_loads.getitemnumber(ll_rowcount_ds, "total_num_boxes"))
	dw_header_total_combos.setitem(ll_count, "total_num_combos", lds_total_loads.getitemnumber(ll_rowcount_ds, "total_num_combos"))
next

//  REVGLL  //

ls_inq_plt_cplx = dw_header_load_list_opt.getitemstring( 1, "load_list_by_option")

If ls_inq_plt_cplx = 'P' Then
	dw_header.Modify("complex_code_t.Visible=0")
	dw_header.Modify("plant_type_t.Visible=0")
	dw_header.Modify("complex_code.Visible=0")
	dw_header.Modify("plant_type.Visible=0")
	
	dw_header.Modify("plant_t.Visible=1")
	dw_header.Modify("location_code.Visible=1")
	dw_header.Modify("location_text.Visible=1")
	dw_header.Modify("division_t.Visible=1")
	dw_header.Modify("division_code.Visible=1")
	dw_header.Modify("division_text.Visible=1")

Else
	If ls_inq_plt_cplx = 'C' Then
		dw_header.Modify("plant_t.Visible=0")
		dw_header.Modify("location_code.Visible=0")
		dw_header.Modify("location_text.Visible=0")
		dw_header.Modify("division_t.Visible=0")
		dw_header.Modify("division_code.Visible=0")
		dw_header.Modify("division_text.Visible=0")
		
		dw_header.Modify("complex_code_t.Visible=1")
		dw_header.Modify("plant_type_t.Visible=1")
		dw_header.Modify("complex_code.Visible=1")
		dw_header.Modify("plant_type.Visible=1")
	End If
End If

If ls_calculate_ind = 'N' Then
	dw_header.Modify("start_time_t.Visible=0")
	dw_header.Modify("start_time.Visible=0")
	dw_header.Modify("loads_per_hour_t.Visible=0")
	dw_header.Modify("loads_per_hour.Visible=0")

	dw_header.Modify("grinds_group_box_t.Visible=0")
	dw_header.Modify("grinds_start_load_times_t.Visible=0")
	dw_header.Modify("grind_start_time.Visible=0")
	dw_header.Modify("grind_load_time.Visible=0")
	
	dw_header.Modify("other_group_box_t.Visible=0")
	dw_header.Modify("other_start_load_times_t.Visible=0")
	dw_header.Modify("other_start_time.Visible=0")
	dw_header.Modify("other_load_time.Visible=0")
	
Else
	If ls_calculate_ind = 'W' Then
		dw_header.Modify("start_time_t.Visible=1")
		dw_header.Modify("start_time.Visible=1")
		dw_header.Modify("loads_per_hour_t.Visible=1")
		dw_header.Modify("loads_per_hour.Visible=1")
	
		dw_header.Modify("grinds_group_box_t.Visible=0")
		dw_header.Modify("grinds_start_load_times_t.Visible=0")
		dw_header.Modify("grind_start_time.Visible=0")
		dw_header.Modify("grind_load_time.Visible=0")
		
		dw_header.Modify("other_group_box_t.Visible=0")
		dw_header.Modify("other_start_load_times_t.Visible=0")
		dw_header.Modify("other_start_time.Visible=0")
		dw_header.Modify("other_load_time.Visible=0")

	Else
		If ls_calculate_ind = 'C' Then
			dw_header.Modify("start_time_t.Visible=0")
			dw_header.Modify("start_time.Visible=0")
			dw_header.Modify("loads_per_hour_t.Visible=0")
			dw_header.Modify("loads_per_hour.Visible=0")
		
			dw_header.Modify("grinds_group_box_t.Visible=1")
			dw_header.Modify("grinds_start_load_times_t.Visible=1")
			dw_header.Modify("grind_start_time.Visible=1")
			dw_header.Modify("grind_load_time.Visible=1")
			
			dw_header.Modify("other_group_box_t.Visible=1")
			dw_header.Modify("other_start_load_times_t.Visible=1")
			dw_header.Modify("other_start_time.Visible=1")
			dw_header.Modify("other_load_time.Visible=1")

		End If
	End If
End If

//

This.SetRedraw(False)
dw_header.ResetUpdate()

tab_1.tabpage_1.dw_maintenance.Reset()
tab_1.tabpage_1.dw_maintenance.ImportString(ls_maint_string)
tab_1.tabpage_1.dw_maintenance.SetSort("load_time A, deadline_depart_date A, deadline_depart_time A")
tab_1.tabpage_1.dw_maintenance.Sort()
tab_1.tabpage_1.dw_maintenance.ResetUpdate()
tab_1.tabpage_1.dw_maintenance.SetRedraw(True)
tab_1.tabpage_1.dw_maintenance.ResetUpdate()

tab_1.tabpage_2.dw_exceptions.Reset()
tab_1.tabpage_2.dw_exceptions.ImportString(ls_except_string)
tab_1.tabpage_2.dw_exceptions.SetSort("load_time A, deadline_depart_date A, deadline_depart_time A, primary_order A, sales_order A, line_number A")
tab_1.tabpage_2.dw_exceptions.Sort()
tab_1.tabpage_2.dw_exceptions.SetRedraw(True)
tab_1.tabpage_2.dw_exceptions.ResetUpdate()

tab_1.SelectedTab = 1
tab_1.tabpage_1.dw_maintenance.SetFocus()

This.SetRedraw(True)
				
Return TRUE
end function

public function boolean wf_update ();String	ls_header, &
			ls_detail, &
			ls_Updateflag, &
			ls_Omitflag, &
			ls_detail_returned, &
			ls_exceptions, &
			ls_exceptions_returned, &
			ls_MicroHelp, &
			ls_business_rules

Integer	li_rtn

Long		ll_rowcount, &
			ll_pos, &
			ll_col, ll_row
			
Boolean	lb_error_found

u_string_functions	lu_string_functions

if tab_1.tabpage_1.dw_maintenance.AcceptText() = -1 then
	Return False
end if

if tab_1.tabpage_2.dw_exceptions.AcceptText() = -1 then
	Return False
end if

/* Added checks on ii_active_tab - IBDKSCS - Feb 2023 */

If ii_active_tab = 1 then
	ll_row = tab_1.tabpage_1.dw_maintenance.GetSelectedRow(0)
	do while ll_row > 0
		if ll_row > 0 then
			if tab_1.tabpage_1.dw_maintenance.getitemstring(ll_row,"update_flag") <> "A" and &
				tab_1.tabpage_1.dw_maintenance.getitemstring(ll_row,"update_flag") <> "U" then
					tab_1.tabpage_1.dw_maintenance.selectrow(ll_row, false)
			end if
		end if
		ll_row = tab_1.tabpage_1.dw_maintenance.GetSelectedRow(ll_row)
	loop
End If

If ii_active_tab = 2 Then
	ll_row = tab_1.tabpage_2.dw_exceptions.GetSelectedRow(0)
	do while ll_row > 0
		if ll_row > 0 then
			if tab_1.tabpage_2.dw_exceptions.getitemstring(ll_row,"update_flag") <> "A" and &
				tab_1.tabpage_2.dw_exceptions.getitemstring(ll_row,"update_flag") <> "U" then
					tab_1.tabpage_2.dw_exceptions.selectrow(ll_row, false)
			end if
		end if
		ll_row = tab_1.tabpage_2.dw_exceptions.GetSelectedRow(ll_row)
	loop
End If


ls_header = dw_header.GetItemString( 1, "location_code") + '~t' + &
			  String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
			  dw_header.GetItemString( 1, "load_list_type") + '~t' + &
			  dw_header.GetItemString( 1, "division_code") + '~t' + &
			  dw_header.GetItemString( 1, "calculate_load_times_ind") + '~t' +&
			  dw_header.GetItemString( 1, "start_time") + '~t' +&
			  String(dw_header.GetItemNumber( 1, "loads_per_hour")) + '~t' + &
			  dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
			  dw_header.getitemstring( 1, "complex_code") + '~t' + &
			  dw_header.getitemstring( 1, "plant_type") + '~r~n'

//This.SetRedraw(False)

//if tab_1.SelectedTab = 1 then
//	ll_rowcount = tab_1.tabpage_1.dw_maintenance.RowCount()
//	//tab_1.tabpage_1.dw_maintenance.SetRedraw(False)
//	//tab_1.SetRedraw(False)
//
//	For ll_pos = 1 to ll_rowcount
//		ls_UpdateFlag = tab_1.tabpage_1.dw_maintenance.GetItemString( ll_pos, "update_flag")
//		ls_OmitFlag = tab_1.tabpage_1.dw_maintenance.GetItemString( ll_pos, "omit_ind")
//		IF (ls_UpDateFlag = 'U' OR ls_UpDateFlag = 'A') then
//			if (ls_OmitFlag = 'Y') Then
//				tab_1.tabpage_1.dw_maintenance.SetItem(ll_pos, "update_flag", ' ')
//			else
//				tab_1.tabpage_1.dw_maintenance.SelectRow(ll_pos, TRUE)
//			end if
//		end if
//	Next
//
//	ls_detail = lu_string_functions.nf_BuildUpdateString(tab_1.tabpage_1.dw_maintenance)
//	ls_exceptions = ''
//
//else
//
//	ll_rowcount = tab_1.tabpage_2.dw_exceptions.RowCount()
//	//tab_1.tabpage_2.dw_exceptions.SetRedraw(False)
//	//tab_1.SetRedraw(False)
//
//	For ll_pos = 1 to ll_rowcount
//		ls_UpdateFlag = tab_1.tabpage_2.dw_exceptions.GetItemString( ll_pos, "update_flag")
//		IF (ls_UpDateFlag = 'U' OR ls_UpDateFlag = 'A') then
//			tab_1.tabpage_2.dw_exceptions.SelectRow(ll_pos, TRUE)
//		end if
//	Next
//
//	ls_exceptions = lu_string_functions.nf_BuildUpdateString(tab_1.tabpage_2.dw_exceptions)
//	ls_detail = ''
//	
//end if

If ii_active_tab = 1 Then

	ll_rowcount = tab_1.tabpage_1.dw_maintenance.RowCount()
	
	For ll_pos = 1 to ll_rowcount
		ls_UpdateFlag = tab_1.tabpage_1.dw_maintenance.GetItemString( ll_pos, "update_flag")
		ls_OmitFlag = tab_1.tabpage_1.dw_maintenance.GetItemString( ll_pos, "omit_ind")
		IF (ls_UpDateFlag = 'U' OR ls_UpDateFlag = 'A') then
			if (ls_OmitFlag = 'Y') Then
				tab_1.tabpage_1.dw_maintenance.SetItem(ll_pos, "update_flag", ' ')
			else
				tab_1.tabpage_1.dw_maintenance.SelectRow(ll_pos, TRUE)
			end if
		end if
	Next
	
	ls_detail = lu_string_functions.nf_BuildUpdateString(tab_1.tabpage_1.dw_maintenance)
	ls_exceptions = ''


	This.SetRedraw(False)
	SetPointer(HourGlass!)

	if len(ls_detail) > 0 then
	//	li_rtn = iu_pas201.nf_pasp81br_update_load_list_maint( istr_error_info, &
	//																		ls_header, &
	//																		ls_detail, &
	//																		ls_detail_returned, &
	//																		ls_exceptions, &
	//																		ls_exceptions_returned)
		li_rtn = iu_ws_pas5.nf_pasp81fr(istr_error_info, ls_header, ls_detail, ls_detail_returned, ls_exceptions, ls_exceptions_returned)																	
	
		This.wf_Replace_Rows(tab_1.tabpage_1.dw_maintenance, ls_detail_returned)
		tab_1.tabpage_1.dw_maintenance.SelectRow(0,False)
		tab_1.tabpage_1.dw_maintenance.SetRedraw(True)
	
		if li_rtn = 0 then
			tab_1.tabpage_1.dw_maintenance.ResetUpdate()
		else
			ll_pos = 1
			lb_error_found = FALSE
			ll_rowcount = tab_1.tabpage_1.dw_maintenance.RowCount()
			DO UNTIL (ll_pos > ll_rowcount) or lb_error_found
				ls_business_rules = tab_1.tabpage_1.dw_maintenance.GetItemString( ll_pos, "business_rules")
				ll_col = lu_string_functions.nf_npos(ls_business_rules, 'E', 1, 1)
				if ll_col > 0 then
					lb_error_found = TRUE
					tab_1.tabpage_1.dw_maintenance.ScrolltoRow(ll_pos)
					tab_1.tabpage_1.dw_maintenance.SetRow(ll_pos)
					tab_1.tabpage_1.dw_maintenance.SetColumn(ll_col)
					tab_1.tabpage_1.dw_maintenance.Selecttext(1,0)
				end if
				ll_pos = ll_pos + 1
			Loop
			tab_1.SetRedraw(True)
			This.SetRedraw(True)
			Return False	
		end if
	Else
		This.SetRedraw(True)
	end if	
End If

if ii_active_tab = 2 Then
	ll_rowcount = tab_1.tabpage_2.dw_exceptions.RowCount()
	
	For ll_pos = 1 to ll_rowcount
		ls_UpdateFlag = tab_1.tabpage_2.dw_exceptions.GetItemString( ll_pos, "update_flag")
		IF (ls_UpDateFlag = 'U' OR ls_UpDateFlag = 'A') then
			tab_1.tabpage_2.dw_exceptions.SelectRow(ll_pos, TRUE)
		end if
	Next
	
	ls_exceptions = lu_string_functions.nf_BuildUpdateString(tab_1.tabpage_2.dw_exceptions)
	ls_detail = ''

	if len(ls_exceptions) > 0 then
	
	//	li_rtn = iu_pas201.nf_pasp81br_update_load_list_maint( istr_error_info, &
	//				   														ls_header, &
	//																			ls_detail, &
	//																			ls_detail_returned, &
	//																			ls_exceptions, &
	//																			ls_exceptions_returned)
		li_rtn = iu_ws_pas5.nf_pasp81fr(istr_error_info, ls_header, ls_detail, ls_detail_returned, ls_exceptions, ls_exceptions_returned)
												
		This.wf_Replace_Rows(tab_1.tabpage_2.dw_exceptions, ls_exceptions_returned)
		tab_1.tabpage_2.dw_exceptions.SelectRow(0,False)
		tab_1.tabpage_2.dw_exceptions.SetRedraw(True)
	
		if li_rtn = 0 then
			tab_1.tabpage_2.dw_exceptions.ResetUpdate()
			ls_MicroHelp = iw_frame.wf_getmicrohelp()
//			This.wf_inq_after_update()
			iw_frame.SetMicroHelp( ls_MicroHelp)
			tab_1.SetRedraw(True)
			This.SetRedraw(True)
	
		else
			ll_pos = 1
			lb_error_found = FALSE
			ll_rowcount = tab_1.tabpage_2.dw_exceptions.RowCount()
			DO UNTIL (ll_pos > ll_rowcount) or lb_error_found
				ls_business_rules = tab_1.tabpage_2.dw_exceptions.GetItemString( ll_pos, "business_rules")
				ll_col = lu_string_functions.nf_npos(ls_business_rules, 'E', 1, 1)
				if ll_col > 0 then
					lb_error_found = TRUE
					tab_1.tabpage_2.dw_exceptions.ScrolltoRow(ll_pos)
					tab_1.tabpage_2.dw_exceptions.SetRow(ll_pos)
					tab_1.tabpage_2.dw_exceptions.SetColumn(ll_col)
				end if
				ll_pos = ll_pos + 1
			Loop
			tab_1.SetRedraw(True)
			This.SetRedraw(True)
			Return False
	
		end if
	Else
		This.SetRedraw(True)
	end if
End If

tab_1.tabpage_3.dw_loads_not_transmitted.SetRedraw(True)
tab_1.SetRedraw(True)
This.SetRedraw(True)
//
//if li_rtn = 1 then
//	ll_pos = 1
//	lb_error_found = FALSE
//	if tab_1.SelectedTab = 1 then
//		ll_rowcount = tab_1.tabpage_1.dw_maintenance.RowCount()
//		DO UNTIL (ll_pos > ll_rowcount) or lb_error_found
//			ls_business_rules = tab_1.tabpage_1.dw_maintenance.GetItemString( ll_pos, "business_rules")
//			ll_col = lu_string_functions.nf_npos(ls_business_rules, 'E', 1, 1)
//			if ll_col > 0 then
//				lb_error_found = TRUE
//				tab_1.tabpage_1.dw_maintenance.ScrolltoRow(ll_pos)
//				tab_1.tabpage_1.dw_maintenance.SetRow(ll_pos)
//				tab_1.tabpage_1.dw_maintenance.SetColumn(ll_col)
//				tab_1.tabpage_1.dw_maintenance.Selecttext(1,0)
//			end if
//			ll_pos = ll_pos + 1
//		Loop
//	else
//		ll_rowcount = tab_1.tabpage_2.dw_exceptions.RowCount()
//		DO UNTIL (ll_pos > ll_rowcount) or lb_error_found
//			ls_business_rules = tab_1.tabpage_2.dw_exceptions.GetItemString( ll_pos, "business_rules")
//			ll_col = lu_string_functions.nf_npos(ls_business_rules, 'E', 1, 1)
//			if ll_col > 0 then
//				lb_error_found = TRUE
//				tab_1.tabpage_2.dw_exceptions.ScrolltoRow(ll_pos)
//				tab_1.tabpage_2.dw_exceptions.SetRow(ll_pos)
//				tab_1.tabpage_2.dw_exceptions.SetColumn(ll_col)
//			end if
//			ll_pos = ll_pos + 1
//		Loop
//	end if
//end if

																		
SetPointer(Arrow!)																		

Return True


end function

on w_auto_load_list_maint_except.create
int iCurrent
call super::create
this.dw_auto_load_list_except_detail_rpt=create dw_auto_load_list_except_detail_rpt
this.dw_loads_not_transmitted_prt=create dw_loads_not_transmitted_prt
this.dw_auto_load_list_maint_detail=create dw_auto_load_list_maint_detail
this.dw_header_total_combos=create dw_header_total_combos
this.dw_header_total_boxes=create dw_header_total_boxes
this.dw_header_load_list_opt=create dw_header_load_list_opt
this.dw_header=create dw_header
this.dw_1=create dw_1
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_auto_load_list_except_detail_rpt
this.Control[iCurrent+2]=this.dw_loads_not_transmitted_prt
this.Control[iCurrent+3]=this.dw_auto_load_list_maint_detail
this.Control[iCurrent+4]=this.dw_header_total_combos
this.Control[iCurrent+5]=this.dw_header_total_boxes
this.Control[iCurrent+6]=this.dw_header_load_list_opt
this.Control[iCurrent+7]=this.dw_header
this.Control[iCurrent+8]=this.dw_1
this.Control[iCurrent+9]=this.tab_1
end on

on w_auto_load_list_maint_except.destroy
call super::destroy
destroy(this.dw_auto_load_list_except_detail_rpt)
destroy(this.dw_loads_not_transmitted_prt)
destroy(this.dw_auto_load_list_maint_detail)
destroy(this.dw_header_total_combos)
destroy(this.dw_header_total_boxes)
destroy(this.dw_header_load_list_opt)
destroy(this.dw_header)
destroy(this.dw_1)
destroy(this.tab_1)
end on

event open;call super::open;iu_pas201 = Create u_pas201
iu_ws_pas5 = Create u_ws_pas5
end event

event ue_postopen;call super::ue_postopen;String	ls_date
DataWindowChild	ldwc_temp
Integer	li_ret

If Daynumber(Today()) = 7 Then
	ls_date = String(RelativeDate(Today(), 2),"mm/dd/yyyy")
Else
	ls_date = String(RelativeDate(Today(), 1),"mm/dd/yyyy")
End If

SetProfileString( iw_frame.is_UserINI, "Pas", "Lastshipdate",ls_date)

This.PostEvent("ue_query")
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event close;call super::close;If IsValid(iu_pas201) Then Destroy iu_Pas201
Destroy iu_ws_pas5
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'location_code'
		dw_header.SetItem( 1, "location_code", as_value)
	Case 'location_text'
		dw_header.SetItem( 1, "location_text", as_value)
	Case 'division_code'
		dw_header.SetItem( 1, "division_code", as_value)
	Case 'division_text'
		dw_header.SetItem( 1, "division_text", as_value)
	Case 'load_list_type'
		dw_header.SetItem( 1, "load_list_type", as_value)
	Case 'ship_date'
	 	dw_header.SetItem( 1, "ship_date", Date(as_value))
	Case 'calculate_load_times_ind'
		If as_value = 'N' Then
			dw_header.SetItem( 1, "calculate_load_times_ind", 'No Calculation')
		Else
			If as_value = 'W' Then
				dw_header.SetItem( 1, "calculate_load_times_ind", 'With Availability')
			Else
				If as_value = 'C' Then
					dw_header.SetItem( 1, "calculate_load_times_ind", 'Case Ready')
				End If
			End If
		End If
	Case 'start_time'
		dw_header.SetItem( 1, "start_time", as_value)
	Case 'loads_per_hour'
		dw_header.SetItem( 1, "loads_per_hour", Integer(as_value))
	case 'load_list_by_option'
		dw_header_load_list_opt.setitem( 1, "load_list_by_option", as_value)
	case 'complex_code'
		dw_header.setitem( 1, "complex_code", as_value)
	case 'plant_type'
		dw_header.setitem( 1, "plant_type", as_value)
	case 'grind_start_time'
		dw_header.setitem( 1, "grind_start_time", as_value)
	case 'grind_load_time'
		dw_header.setItem( 1, "grind_load_time", as_value)
	case 'other_start_time'
		dw_header.setItem( 1, "other_start_time", as_value)
	case 'other_load_time'
		dw_header.setItem( 1, "other_load_time", as_value)
End Choose
end event

event rbuttondown;m_load_list_maint_popup	lm_popup

lm_popup = Create m_load_list_maint_popup

lm_popup.m_loadlistmaintenance.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup

end event

event deactivate;iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')

end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event resize;call super::resize;tab_1.Resize(This.width - tab_1.X - 50, This.height - tab_1.Y - 110)

tab_1.tabpage_1.Resize(tab_1.width - tab_1.tabpage_1.X - 10, &
		tab_1.height - tab_1.tabpage_1.Y - 25)
tab_1.tabpage_1.dw_maintenance.Resize(tab_1.tabpage_1.width - tab_1.tabpage_1.dw_maintenance.X - 15, &
		tab_1.tabpage_1.height - tab_1.tabpage_1.dw_maintenance.Y)

tab_1.tabpage_2.Resize(tab_1.width - tab_1.tabpage_1.X - 10, &
		tab_1.height - tab_1.tabpage_2.Y - 25)
tab_1.tabpage_2.dw_exceptions.Resize(tab_1.tabpage_2.width - tab_1.tabpage_2.dw_exceptions.X - 15, &
		tab_1.tabpage_2.height - tab_1.tabpage_2.dw_exceptions.Y)

tab_1.tabpage_3.Resize(tab_1.width - tab_1.tabpage_1.X - 10, &
		tab_1.height - tab_1.tabpage_3.Y - 25)
tab_1.tabpage_3.dw_loads_not_transmitted.Resize(tab_1.tabpage_3.width - tab_1.tabpage_3.dw_loads_not_transmitted.X - 15, &
		tab_1.tabpage_3.height - tab_1.tabpage_3.dw_loads_not_transmitted.Y)

end event

event ue_fileprint;call super::ue_fileprint;Integer		li_tab
DataWindowChild	ldwc_temp
Integer	li_ret

li_tab = tab_1.selectedtab

Choose Case li_tab
	Case 1
		dw_auto_load_list_maint_detail.getchild("loading_instruction",ldwc_temp)
		ldwc_temp.settransobject(sqlca)
		li_ret = ldwc_temp.Retrieve()		
		tab_1.tabpage_1.dw_maintenance.ShareData(dw_auto_load_list_maint_detail)
		dw_auto_load_list_maint_detail.object.ship_date_t.text = String(dw_header.GetItemDate(1, 'ship_date'), 'mm/dd/yyyy')
		dw_auto_load_list_maint_detail.object.plant_t.text = dw_header.GetItemString(1, 'location_code')
		dw_auto_load_list_maint_detail.object.calculation_t.text = dw_header.GetItemString(1, 'calculate_load_times_ind')
		dw_auto_load_list_maint_detail.Print()
		tab_1.tabpage_1.dw_maintenance.ShareDataOff()
	Case 2
		dw_auto_load_list_except_detail_rpt.getchild("loading_instruction",ldwc_temp)
		ldwc_temp.settransobject(sqlca)
		li_ret = ldwc_temp.Retrieve()		
		tab_1.tabpage_2.dw_exceptions.ShareData(dw_auto_load_list_except_detail_rpt)
		dw_auto_load_list_except_detail_rpt.object.ship_date_t.text = String(dw_header.GetItemDate(1, 'ship_date'), 'mm/dd/yyyy')
		dw_auto_load_list_except_detail_rpt.object.plant_t.text = dw_header.GetItemString(1, 'location_code')
		dw_auto_load_list_except_detail_rpt.object.calculation_t.text = dw_header.GetItemString(1, 'calculate_load_times_ind')
		dw_auto_load_list_except_detail_rpt.Print()
		tab_1.tabpage_2.dw_exceptions.ShareDataOff()
	Case 3
		tab_1.tabpage_3.dw_loads_not_transmitted.ShareData(dw_loads_not_transmitted_prt)
		dw_loads_not_transmitted_prt.object.ship_date_t.text = String(dw_header.GetItemDate(1, 'ship_date'), 'mm/dd/yyyy')
		dw_loads_not_transmitted_prt.object.plant_t.text = dw_header.GetItemString(1, 'location_code')
		dw_loads_not_transmitted_prt.object.calculation_t.text = dw_header.GetItemString(1, 'calculate_load_times_ind')
		dw_loads_not_transmitted_prt.Print()
		tab_1.tabpage_3.dw_loads_not_transmitted.ShareDataOff()		
End Choose

end event

event ue_get_data;call super::ue_get_data;String 	ls_temp

Choose Case as_value
	Case 'calculate_load_times_ind'
		
		ls_temp = dw_header.GetItemString(1, 'calculate_load_times_ind')
		
		If IsNull(ls_temp) Then
			Message.StringParm = 'N'
		Else
			If ls_temp = 'No Calculation' Then
				Message.StringParm = 'N'
			Else
				If ls_temp = 'With Availability' Then
					Message.StringParm = 'W'
				Else
					If ls_temp = 'Case Ready' Then
						Message.StringParm = 'C'
					End If
				End If
			End If
		End IF
	Case "email_fax_ind"
		message.StringParm = is_email_fax_ind
	Case "report_type"
		message.StringParm = is_report_type		
	Case "plant_code"
		message.StringParm = dw_header.getitemstring( 1, "location_code")		
	Case "load_list_type"
		message.StringParm = dw_header.GetItemString( 1, "load_list_type")
	Case "ship_date"
		message.StringParm = String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY")
	Case "division_code"	
		message.StringParm = dw_header.GetItemString( 1, "division_code")
	Case "complex_code"	
		message.StringParm = dw_header.GetItemString( 1, "complex_code")
	Case "plant_type"	
		message.StringParm = dw_header.GetItemString( 1, "plant_type")
	Case "load_list_by_option"	
		message.StringParm = dw_header_load_list_opt.getitemstring( 1, "load_list_by_option")
		
End Choose
end event

type dw_auto_load_list_except_detail_rpt from u_base_dw_ext within w_auto_load_list_maint_except
boolean visible = false
integer x = 667
integer y = 1252
integer width = 667
integer height = 288
integer taborder = 50
string dataobject = "d_auto_load_list_except_detail_prt"
end type

type dw_loads_not_transmitted_prt from u_base_dw_ext within w_auto_load_list_maint_except
boolean visible = false
integer x = 777
integer y = 1096
integer width = 859
integer height = 376
integer taborder = 40
string dataobject = "d_loads_not_transmitted_prt"
end type

type dw_auto_load_list_maint_detail from u_base_dw_ext within w_auto_load_list_maint_except
boolean visible = false
integer x = 731
integer y = 988
integer width = 1115
integer height = 388
integer taborder = 30
string dataobject = "d_auto_load_list_maint_detail_prt"
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp
Integer	li_ret

This.getchild("loading_instruction",ldwc_temp)
ldwc_temp.settransobject(sqlca)
li_ret = ldwc_temp.Retrieve()
end event

type dw_header_total_combos from u_load_list_total_combo within w_auto_load_list_maint_except
integer x = 2510
integer y = 260
integer width = 562
integer taborder = 31
boolean border = false
end type

event constructor;call super::constructor;dw_header_total_combos.uf_disable()
end event

type dw_header_total_boxes from u_load_list_total_box within w_auto_load_list_maint_except
integer x = 1952
integer y = 260
integer width = 530
integer taborder = 21
boolean border = false
end type

event constructor;call super::constructor;dw_header_total_boxes.uf_disable()
end event

type dw_header_load_list_opt from u_load_list_by_option within w_auto_load_list_maint_except
integer x = 2048
integer taborder = 21
end type

event constructor;call super::constructor;dw_header_load_list_opt.uf_disable()
end event

type dw_header from u_base_dw_ext within w_auto_load_list_maint_except
integer x = 18
integer y = 8
integer width = 1883
integer height = 440
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_auto_load_list_maint_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.InsertRow(0)
end event

event rbuttondown;call super::rbuttondown;m_load_list_maint_popup	lm_popup

lm_popup = Create m_load_list_maint_popup

lm_popup.m_loadlistmaintenance.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup

end event

type dw_1 from u_base_dw_ext within w_auto_load_list_maint_except
integer x = 2738
integer y = 20
integer width = 325
integer height = 220
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_auto_load_list_find_primary"
borderstyle borderstyle = styleraised!
end type

event constructor;call super::constructor;ib_updateable = False
this.insertrow(0)
end event

event editchanged;datawindow	adw_detail

choose case ii_active_tab
	case 1
		adw_detail = tab_1.tabpage_1.dw_maintenance
	case 2
		adw_detail = tab_1.tabpage_2.dw_exceptions
	case 3
		adw_detail = tab_1.tabpage_3.dw_loads_not_transmitted
end choose

wf_search_row(adw_detail, this, data)
end event

type tab_1 from u_load_list_tab within w_auto_load_list_maint_except
integer y = 440
integer width = 3054
integer height = 1284
integer taborder = 20
end type

event selectionchanged;String		ls_input, &
				ls_output_string
				
Integer		li_rtn, &
				li_row

datawindow	adw_detail

ii_active_tab = newindex

If newindex = 3 Then
	This.SetRedraw(False)
	ls_input = dw_header.GetItemString( 1, "location_code") + '~t' + &
				  String(dw_header.GetItemDate( 1, "ship_date"), "MM/DD/YYYY") + '~t' + &
				  dw_header.GetItemString( 1, "division_code") + '~t' + &
				  dw_header_load_list_opt.getitemstring( 1, "load_list_by_option") + '~t' + &
				  dw_header.getitemstring( 1, "complex_code") + '~t' + &
				  dw_header.getitemstring( 1, "plant_type") + '~r~t'
				  
	istr_error_info.se_event_name = "wf_retrieve"			  

//	li_rtn = iu_pas201.nf_pasp97br_loads_not_on_list(istr_error_info, &
//												ls_input, &
//												ls_output_string)

	li_rtn = iu_ws_pas5.nf_pasp97fr(istr_error_info, &
												ls_input, &
												ls_output_string)

	tab_1.tabpage_3.dw_loads_not_transmitted.Reset()
	tab_1.tabpage_3.dw_loads_not_transmitted.ImportString(ls_output_string)
	tab_1.tabpage_3.dw_loads_not_transmitted.SetRedraw(True)
	tab_1.tabpage_3.dw_loads_not_transmitted.ResetUpdate()
	tab_1.tabpage_3.dw_loads_not_transmitted.SetSort('deadline_date A, deadline_time A,  delivery_date A, load_number A')
	tab_1.tabpage_3.dw_loads_not_transmitted.Sort()
	This.SetRedraw(True)
End If

choose case ii_active_tab
	case 1
		adw_detail = tab_1.tabpage_1.dw_maintenance
	case 2
		adw_detail = tab_1.tabpage_2.dw_exceptions
	case 3
		adw_detail = tab_1.tabpage_3.dw_loads_not_transmitted
end choose

if not (ib_first_time_in) then
	dw_1.accepttext()
	if len(dw_1.getitemstring(1,1)) > 0 then
		wf_search_row(adw_detail, dw_1, dw_1.getitemstring(1,1))
	ELSE
		li_row = adw_detail.GetSelectedRow(0)
		if li_row > 0 then
			adw_detail.selectrow(li_row, false)
		end if
	return
	end if
end if
ib_first_time_in = false


end event

