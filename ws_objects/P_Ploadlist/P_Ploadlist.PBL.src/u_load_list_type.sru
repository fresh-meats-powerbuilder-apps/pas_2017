﻿$PBExportHeader$u_load_list_type.sru
forward
global type u_load_list_type from datawindow
end type
end forward

global type u_load_list_type from datawindow
int Width=763
int Height=104
int TabOrder=10
string DataObject="d_load_list_type"
boolean Border=false
boolean LiveScroll=true
end type
global u_load_list_type u_load_list_type

forward prototypes
public function string uf_get_load_list_type ()
public subroutine uf_set_load_list_type (string as_load_list_type)
end prototypes

public function string uf_get_load_list_type ();return Trim(This.GetItemString(1, "load_list_type"))
end function

public subroutine uf_set_load_list_type (string as_load_list_type);This.SetItem(1, "load_list_type", as_load_list_type)
end subroutine

event constructor;String	ls_text

DataWindowChild		ldwc_type

This.InsertRow(0)

This.GetChild("load_list_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("LDLSTTYP")

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "Lastloadlisttype","")

if ls_text = '' then
	ls_text = 'S'
end if

This.SetItem( 1, "load_list_type", ls_text)



end event

event itemchanged;Long			ll_RowFound

String 		ls_FindExpression , &
				ls_columnname

datawindowchild	ldw_childdw

if dwo.Name = "load_list_type" then
	This.GetChild('load_list_type', ldw_ChildDW)
	ll_rowfound	=	ldw_ChildDW.RowCount()
	ls_FindExpression = "type_code = ~"" + Trim(data) + "~""
	ll_RowFound = ldw_ChildDW.Find(ls_FindExpression, 0, ldw_ChildDW.RowCount())
	If  ll_RowFound <= 0 Then 
		MessageBox('Invalid Data', 'Load List Type must be S-SYSTEMS or Y-BUYERS.')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF	
END IF

SetProfileString( iw_frame.is_UserINI, "Pas", "Lastloadlisttype",data)
end event

event getfocus;If This.gettext() = 'S' then
	This.SetText("SYSTEMS")
else
	If This.gettext() = 'Y' then
		This.SetText("BUYERS")
	end if
end if

This.SelectText(1, 10)
end event

event itemerror;Return 1
end event

