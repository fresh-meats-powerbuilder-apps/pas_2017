﻿$PBExportHeader$u_load_list_total_box.sru
forward
global type u_load_list_total_box from u_netwise_dw
end type
end forward

global type u_load_list_total_box from u_netwise_dw
integer width = 475
integer height = 96
string dataobject = "d_total_num_boxes"
end type
global u_load_list_total_box u_load_list_total_box

forward prototypes
public subroutine uf_disable ()
public subroutine uf_enable ()
public function string uf_get_total_boxes ()
end prototypes

public subroutine uf_disable ();This.object.total_num_boxes.Background.Color = 12632256
This.object.total_num_boxes.Protect = 1
end subroutine

public subroutine uf_enable ();This.object.total_num_boxes.Background.Color = 16777215
This.object.total_num_boxes.Protect = 0

end subroutine

public function string uf_get_total_boxes ();return Trim(This.GetItemString(1, "total_num_boxes"))
end function

on u_load_list_total_box.create
end on

on u_load_list_total_box.destroy
end on

event constructor;call super::constructor;this.insertrow(0)
end event

