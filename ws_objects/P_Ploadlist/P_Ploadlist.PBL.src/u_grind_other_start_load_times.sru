﻿$PBExportHeader$u_grind_other_start_load_times.sru
$PBExportComments$BB1845
forward
global type u_grind_other_start_load_times from u_netwise_dw
end type
end forward

global type u_grind_other_start_load_times from u_netwise_dw
integer width = 1216
integer height = 180
string dataobject = "d_grind_other_start_load_times"
end type
global u_grind_other_start_load_times u_grind_other_start_load_times

type variables
Window		iw_parentwindow
end variables

forward prototypes
public function long uf_get_grind_load_time ()
public function long uf_get_other_load_time ()
public function string uf_get_grind_start_time ()
public function string uf_get_other_start_time ()
end prototypes

public function long uf_get_grind_load_time ();return This.GetItemNumber(1, "grind_load_time")
end function

public function long uf_get_other_load_time ();return This.GetItemNumber(1, "other_load_time")
end function

public function string uf_get_grind_start_time ();return This.GetItemString(1, "grind_start_time")
end function

public function string uf_get_other_start_time ();return This.GetItemString(1, "other_start_time")
end function

on u_grind_other_start_load_times.create
end on

on u_grind_other_start_load_times.destroy
end on

event constructor;call super::constructor;String		ls_time

iw_parentwindow = Parent
This.InsertRow(0)

// grind start time
ls_time = ProfileString( iw_frame.is_UserINI, "Pas", "LastGrindStartTime","")

if IsNull(ls_time) then
	ls_time = '0600'
end if

This.SetItem( 1, "grind_start_time", ls_time)

// grind load time
ls_time = ProfileString( iw_frame.is_UserINI, "Pas", "LastGrindLoadTime","")

if IsNull(ls_time) then
	ls_time = '30'
end if

This.SetItem( 1, "grind_load_time", Long(ls_time))

// other start time
ls_time = ProfileString( iw_frame.is_UserINI, "Pas", "LastOtherStartTime","")

if IsNull(ls_time) then
	ls_time = '0600'
end if

This.SetItem( 1, "other_start_time", ls_time)

// other load time
ls_time = ProfileString( iw_frame.is_UserINI, "Pas", "LastOtherLoadTime","")

if IsNull(ls_time) then
	ls_time = '40'
end if

This.SetItem( 1, "other_load_time", Long(ls_time))

end event

event getfocus;call super::getfocus;This.SelectText(1, Len(This.GetText()))
end event

event itemerror;call super::itemerror;Return 1
end event

event itemchanged;call super::itemchanged;Integer		li_grind_start_time, li_grind_load_time, li_other_start_time, li_other_load_time
String		ls_time

if dwo.Name = "grind_start_time" then
	li_grind_start_time = integer(data)
	if (li_grind_start_time > 2359) or (li_grind_start_time < 0000) then 
		MessageBox('Invalid Data', 'Grinds Start Time Per Hour must be between 0 and 2359.')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF
	SetProfileString( iw_frame.is_UserINI, "Pas", "LastGrindStartTime",Trim(data))
END IF

if dwo.Name = "grind_load_time" then
	li_grind_load_time = integer(data)
	if (li_grind_load_time > 59) or (li_grind_load_time < 01) then 
		MessageBox('Invalid Data', 'Grinds Load Time Per Hour must be between 1 and 59.')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF
	SetProfileString( iw_frame.is_UserINI, "Pas", "LastGrindLoadTime",trim(data))
END IF

if dwo.Name = "other_start_time" then
	li_other_start_time = integer(data)
	if (li_other_start_time > 2359) or (li_other_start_time < 0000) then 
		MessageBox('Invalid Data', 'Other Start Time Per Hour must be between 0 and 2359.')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF
	SetProfileString( iw_frame.is_UserINI, "Pas", "LastOtherStartTime",Trim(data))
END IF

if dwo.Name = "other_load_time" then
	li_other_load_time = integer(data)
	if (li_other_load_time > 59) or (li_other_load_time < 01) then 
		MessageBox('Invalid Data', 'Other Load Time Per Hour must be between 1 and 59.')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF
	SetProfileString( iw_frame.is_UserINI, "Pas", "LastOtherLoadTime",trim(data))
END IF
end event

