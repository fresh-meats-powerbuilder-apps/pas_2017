﻿$PBExportHeader$u_load_list_total_combo.sru
forward
global type u_load_list_total_combo from u_netwise_dw
end type
end forward

global type u_load_list_total_combo from u_netwise_dw
integer width = 503
integer height = 92
string dataobject = "d_total_num_combos"
end type
global u_load_list_total_combo u_load_list_total_combo

forward prototypes
public subroutine uf_disable ()
public subroutine uf_enable ()
public function string uf_get_total_combos ()
end prototypes

public subroutine uf_disable ();This.object.total_num_combos.Background.Color = 12632256
This.object.total_num_combos.Protect = 1
end subroutine

public subroutine uf_enable ();This.object.total_num_combos.Background.Color = 16777215
This.object.total_num_combos.Protect = 0

end subroutine

public function string uf_get_total_combos ();return Trim(This.GetItemString(1, "total_num_combos"))
end function

on u_load_list_total_combo.create
end on

on u_load_list_total_combo.destroy
end on

event constructor;call super::constructor;this.insertrow(0)
end event

