﻿$PBExportHeader$w_load_combination_inq.srw
forward
global type w_load_combination_inq from w_base_response_ext
end type
type dw_detail from u_base_dw_ext within w_load_combination_inq
end type
type dw_header from u_base_dw_ext within w_load_combination_inq
end type
end forward

global type w_load_combination_inq from w_base_response_ext
integer x = 553
integer y = 240
integer width = 1861
integer height = 1076
string title = "Load Combination"
long backcolor = 12632256
dw_detail dw_detail
dw_header dw_header
end type
global w_load_combination_inq w_load_combination_inq

type variables
String	is_primary	

u_pas201		iu_pas201

s_error		istr_error_info
u_ws_pas5 	iu_ws_pas5
end variables

on w_load_combination_inq.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
this.Control[iCurrent+2]=this.dw_header
end on

on w_load_combination_inq.destroy
call super::destroy
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event open;call super::open;is_primary = Message.StringParm

iu_pas201 = Create u_pas201
iu_ws_pas5 = Create u_ws_pas5
end event

event ue_postopen;call super::ue_postopen;String	ls_output, &
			ls_header_in, &
			ls_header_out

Integer	li_rtn

istr_error_info.se_event_name = "ue_postopen"			  

ls_header_in = is_primary
//li_rtn = iu_pas201.nf_pasp79br_inq_load_combination(istr_error_info, &
//												ls_header_in, &
//												ls_header_out, &
//												ls_output)
li_rtn = iu_ws_pas5.nf_pasp79fr(istr_error_info, ls_header_in, ls_header_out, ls_output)
												
dw_header.Reset()
dw_detail.Reset()
												
dw_header.ImportString(ls_header_out)
dw_detail.ImportString(ls_output)

dw_header.SetRedraw(True)
dw_detail.SetRedraw(True)

This.SetRedraw(True)

												
end event

event close;call super::close;If IsValid(iu_pas201) Then Destroy iu_Pas201
end event

event ue_base_ok;call super::ue_base_ok;CloseWithReturn(This, "OK")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_load_combination_inq
boolean visible = false
integer x = 2135
integer y = 424
integer taborder = 40
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_load_combination_inq
boolean visible = false
integer x = 2135
integer y = 300
integer taborder = 30
boolean enabled = false
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_load_combination_inq
integer x = 745
integer y = 864
integer width = 288
end type

type dw_detail from u_base_dw_ext within w_load_combination_inq
integer x = 14
integer y = 220
integer width = 1810
integer height = 608
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_load_combination_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

type dw_header from u_base_dw_ext within w_load_combination_inq
integer x = 18
integer y = 24
integer width = 1806
integer height = 192
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_load_combination_header"
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

