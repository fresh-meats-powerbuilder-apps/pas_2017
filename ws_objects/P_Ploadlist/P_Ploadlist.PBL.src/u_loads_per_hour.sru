﻿$PBExportHeader$u_loads_per_hour.sru
forward
global type u_loads_per_hour from datawindow
end type
end forward

global type u_loads_per_hour from datawindow
int Width=581
int Height=84
int TabOrder=10
string DataObject="d_loads_per_hour"
boolean Border=false
boolean LiveScroll=true
end type
global u_loads_per_hour u_loads_per_hour

forward prototypes
public function integer uf_get_loads_per_hour ()
public subroutine uf_set_loads_per_hour (integer ai_loads_per_hour)
public subroutine uf_enable ()
public subroutine uf_disable ()
end prototypes

public function integer uf_get_loads_per_hour ();return This.GetItemNumber(1, "loads_per_hour")
end function

public subroutine uf_set_loads_per_hour (integer ai_loads_per_hour);This.SetItem(1, "loads_per_hour", ai_loads_per_hour)
end subroutine

public subroutine uf_enable ();This.object.loads_per_hour.Background.Color = 16777215
This.object.loads_per_hour.Protect = 0

end subroutine

public subroutine uf_disable ();This.object.loads_per_hour.Background.Color = 12632256
This.object.loads_per_hour.Protect = 1

end subroutine

event constructor;String	ls_text

This.InsertRow(0)

This.Object.loads_per_hour.EditMask.SpinRange = "0~~~~10"

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "Lastloadsperhour","")

if ls_text = '' then
	ls_text = '5'
end if

This.SetItem( 1, "loads_per_hour", Integer(ls_text) )

end event

event itemchanged;Integer		li_loads

if dwo.Name = "loads_per_hour" then
	li_loads = integer(data)
	if (li_loads > 10) or (li_loads < 1) then 
		MessageBox('Invalid Data', 'Loads Per Hour must be between 1 and 10.')
		This.SelectText(1, Len(data))
		RETURN	1
	END IF	
END IF

SetProfileString( iw_frame.is_UserINI, "Pas", "Lastloadsperhour",trim(data))
end event

event getfocus;This.SelectText(1, Len(This.GetText()))
end event

event itemerror;Return 1
end event

