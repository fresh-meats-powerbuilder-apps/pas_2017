﻿$PBExportHeader$u_type.sru
forward
global type u_type from datawindow
end type
end forward

global type u_type from datawindow
int Width=983
int Height=72
int TabOrder=10
string DataObject="d_work_hours_type"
boolean Border=false
boolean LiveScroll=true
end type
global u_type u_type

forward prototypes
public function string uf_get_type ()
public subroutine uf_set_type (string as_type)
public function integer uf_enable (boolean ab_enable)
public function integer uf_modified ()
end prototypes

public function string uf_get_type ();Return This.GetItemString(1, "worktype")

end function

public subroutine uf_set_type (string as_type);	This.SetItem(1,"worktype",as_type)
  
end subroutine

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.worktype.Background.Color = 16777215
	This.object.worktype.Protect = 0
Else
	This.object.worktype.Background.Color = 12632256
	This.object.worktype.Protect = 1
End If

Return 1
end function

public function integer uf_modified ();Return 0
end function

event constructor;DataWindowChild		ldwc_type


This.GetChild("worktype", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("worktype")
This.InsertRow(0)

end event

