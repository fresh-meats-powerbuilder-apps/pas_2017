﻿$PBExportHeader$u_select_time.sru
forward
global type u_select_time from datawindow
end type
end forward

global type u_select_time from datawindow
int Width=695
int Height=260
int TabOrder=10
string DataObject="d_select_time"
boolean Border=false
boolean LiveScroll=true
end type
global u_select_time u_select_time

forward prototypes
public subroutine uf_set_select_time (string as_select_time)
public function string uf_get_select_time ()
public function integer uf_modified ()
public function integer uf_enable (boolean ab_enable)
end prototypes

public subroutine uf_set_select_time (string as_select_time);	This.SetItem(1,"time",as_select_time)
  
end subroutine

public function string uf_get_select_time ();	Return This.GetItemString(1, "time")
end function

public function integer uf_modified ();Return 0
end function

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.time.Protect = 0
Else
	This.object.time.Protect = 1
End If

Return 1
end function

event constructor;This.InsertRow(0)
 

end event

event itemchanged;//w_work_hours_inq.wf_determine_selected_time(data)
end event

