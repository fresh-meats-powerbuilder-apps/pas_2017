﻿$PBExportHeader$w_cooler_inventory.srw
forward
global type w_cooler_inventory from w_base_sheet_ext
end type
type dw_temp_cooler from u_base_dw_ext within w_cooler_inventory
end type
type dw_hot_box_cooler from u_base_dw_ext within w_cooler_inventory
end type
type dw_header from u_base_dw_ext within w_cooler_inventory
end type
end forward

global type w_cooler_inventory from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 4265
integer height = 1584
string title = "Cooler Inventory"
long backcolor = 67108864
event ue_keydown pbm_dwnkey
event ue_recalc_hot_box ( )
event ue_recalc_temp_cooler ( )
dw_temp_cooler dw_temp_cooler
dw_hot_box_cooler dw_hot_box_cooler
dw_header dw_header
end type
global w_cooler_inventory w_cooler_inventory

type variables
s_error	istr_error_info

u_pas201			iu_pas201

u_ws_pas4           iu_ws_pas4

String	is_slaughter_plant, &
			is_processing_plant, &
			is_d_shift_ind
			
			
Long		il_changed_hot_box_row, &
			il_changed_temp_cooler_row

end variables

forward prototypes
public subroutine wf_recalc_hot_box (long al_current_row)
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_recalc_temp_cooler (long al_current_row)
public subroutine wf_recalc_temp_cooler_line (long al_current_row)
end prototypes

event ue_recalc_hot_box();wf_recalc_hot_box (il_changed_hot_box_row)
end event

event ue_recalc_temp_cooler();wf_recalc_temp_cooler (il_changed_temp_cooler_row)
end event

public subroutine wf_recalc_hot_box (long al_current_row);Long		ll_sub, &
			ll_RowCount, &
			ll_begin_invent, &
			ll_end_invent, &
			ll_a_shift_slaughter_total, &
			ll_b_shift_slaughter_total, &
			ll_d_shift_slaughter_total, &
			ll_transfer_total

ll_RowCount = dw_hot_box_cooler.RowCount()

If dw_hot_box_cooler.GetItemString(al_current_row, "shift") = "D" Then
	al_current_row = al_current_row - 2 
End If

If dw_hot_box_cooler.GetItemString(al_current_row, "shift") = "B" Then
	al_current_row --
End If

If dw_hot_box_cooler.GetItemString(al_current_row, "d_shift_info_ind") = 'Y' Then
	
	For ll_sub = al_current_row To ll_RowCount Step 3 //always on 'A' shift row
	
		If ll_sub = 1 Then
			ll_begin_invent = dw_hot_box_cooler.GetItemNumber(1, "beginning_inventory")
		Else
			ll_begin_invent = dw_hot_box_cooler.GetItemNumber(ll_sub - 1, "ending_inventory")
		End If
		
		ll_a_shift_slaughter_total = dw_hot_box_cooler.GetItemNumber(ll_sub, "slaughter_total")
		ll_b_shift_slaughter_Total = dw_hot_box_cooler.GetItemNumber(ll_sub + 1, "slaughter_total") 
		ll_d_shift_slaughter_Total = dw_hot_box_cooler.GetItemNumber(ll_sub + 2, "slaughter_total")
		
		//only D shift line has transfer totals
		ll_transfer_total = dw_hot_box_cooler.GetItemNumber(ll_sub + 2, "transfer_total")
		
		ll_end_invent = ll_begin_invent + ll_a_shift_slaughter_total + ll_b_shift_slaughter_total + ll_d_shift_slaughter_total - ll_transfer_total
	
		dw_hot_box_cooler.SetItem(ll_sub + 2, "ending_inventory", ll_end_invent)
		
		If dw_temp_cooler.RowCount() > 0 Then
			wf_recalc_temp_cooler_line (ll_sub)
		End If
		
	Next
	
Else

	For ll_sub = al_current_row To ll_RowCount Step 2  //always on 'A' shift row
	
		If ll_sub = 1 Then
			ll_begin_invent = dw_hot_box_cooler.GetItemNumber(1, "beginning_inventory")
		Else
			ll_begin_invent = dw_hot_box_cooler.GetItemNumber(ll_sub - 1, "ending_inventory")
		End If
		
		ll_a_shift_slaughter_total = dw_hot_box_cooler.GetItemNumber(ll_sub, "slaughter_total")
		ll_b_shift_slaughter_Total = dw_hot_box_cooler.GetItemNumber(ll_sub + 1, "slaughter_total") 
		
		//only B shift line has transfer totals
		ll_transfer_total = dw_hot_box_cooler.GetItemNumber(ll_sub + 1, "transfer_total")
		
		ll_end_invent = ll_begin_invent + ll_a_shift_slaughter_total + ll_b_shift_slaughter_total - ll_transfer_total
	
		dw_hot_box_cooler.SetItem(ll_sub + 1, "ending_inventory", ll_end_invent)
		
		If dw_temp_cooler.RowCount() > 0 Then
			wf_recalc_temp_cooler_line (ll_sub)
		End If
		
	Next
	
End If


end subroutine

public function boolean wf_retrieve ();Long					ll_rec_count_pas
				
String				ls_plant, &
						ls_plant_desc, & 
						ls_header, &
						ls_hot_box_info, &
						ls_temp_cooler_info, &
						ls_temp, &
						ls_plant_info, &
						ls_hot_box_title, &
						ls_temp_cooler_title

This.TriggerEvent('closequery') 

SetPointer(HourGlass!)
dw_hot_box_cooler.ResetUpdate()
dw_temp_cooler.ResetUpdate()
dw_header.ResetUpdate()


OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
If Not ib_inquire Then
	Return False
End If

dw_header.ResetUpdate()

ls_header = dw_header.GetItemString(1, "complex_code") + '~t' + & 
				String(dw_header.GetItemDate(1, "start_date"), "mm/dd/yyyy") + '~r~n'

iw_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"

//If iu_pas201.nf_pasp81cr_cooler_inventory_inq(istr_error_info, & 
//										ls_header, &
//										ls_hot_box_info, &
//										ls_temp_cooler_info, &
//										ls_plant_info) = -1 Then
//										This.SetRedraw(True) 
//										Return False

If iu_ws_pas4.nf_pasp81gr(istr_error_info, & 
										ls_header, &
										ls_hot_box_info, &
										ls_temp_cooler_info, &
										ls_plant_info) = -1 Then
										This.SetRedraw(True) 
										Return False
End If			

dw_hot_box_cooler.Reset()
ll_rec_count_pas = dw_hot_box_cooler.ImportString(ls_hot_box_info)

dw_temp_cooler.Reset()


If ll_rec_count_pas > 0 Then 
	SetMicroHelp(String(ll_rec_count_pas) + " Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

is_slaughter_plant = Mid(ls_plant_info,1,3)
is_processing_plant = Mid(ls_plant_info,5,3)

is_d_shift_ind = Mid(ls_plant_info,9,1)

ls_hot_box_title = '<--------------------------------------------- Hot Box Cooler ('
ls_hot_box_title += is_slaughter_plant
ls_hot_box_title += ')---------------------------------------------------->'
dw_hot_box_cooler.Object.t_hot_box_title.Text = ls_hot_box_title

If ls_temp_cooler_info > '' Then
	dw_temp_cooler.Visible = True
	dw_temp_cooler.ImportString(ls_temp_cooler_info)

	ls_temp_cooler_title = '<---------------------------------- Temp Cooler ('
	ls_temp_cooler_title += is_processing_plant
	ls_temp_cooler_title += ') ---------------------------------->'
	dw_temp_cooler.Object.t_temp_cooler_title.Text = ls_temp_cooler_title
Else
	dw_temp_cooler.Visible = False
End If	

dw_hot_box_cooler.ResetUpdate()
dw_temp_cooler.ResetUpdate()

This.SetRedraw(True) 

dw_hot_box_cooler.SetFocus()

Return True

end function

public function boolean wf_update ();String		ls_header, &
				ls_hot_box_info, &
				ls_temp_cooler_info

Long			ll_modrows


If dw_hot_box_cooler.AcceptText() = -1 Then 
	Return False
End if

If dw_temp_cooler.AcceptText() = -1 Then 
	Return False
End if

ls_header = dw_header.Describe( "DataWindow.Data") + '~t' + is_slaughter_plant + & 
				'~t' + is_processing_plant + '~t' + is_d_shift_ind + '~r~n'

ll_modrows = dw_hot_box_cooler.ModifiedCount() + dw_temp_cooler.ModifiedCount()				

IF ll_modrows <= 0 Then 
	SetMicroHelp('No Update Necessary')
	Return False
Else
 	SetMicroHelp("Wait... Updating Database")
	SetPointer(HourGlass!)
End if
				
ls_hot_box_info = dw_hot_box_cooler.Describe( "DataWindow.Data")
ls_temp_cooler_info = dw_temp_cooler.Describe( "DataWindow.Data")
				
istr_error_info.se_event_name = "wf_retrieve"

/*If Not iu_pas201.nf_pasp82cr_cooler_inventory_upd(istr_error_info, & 
										ls_hot_box_info, &
										ls_temp_cooler_info, &
										ls_header)  Then
	This.SetRedraw(True) 
	Return False
End If	
*/
If Not iu_ws_pas4.NF_PASP82GR(istr_error_info, & 
										ls_hot_box_info, &
										ls_temp_cooler_info, &
										ls_header)  Then
	This.SetRedraw(True) 
	
	Return False
End If	


iw_frame.SetMicroHelp("Update Successful")
SetPointer(Arrow!)

dw_header.ResetUpdate()
dw_hot_box_cooler.ResetUpdate()
dw_temp_cooler.ResetUpdate()

dw_header.ib_updateable = False
dw_hot_box_cooler.ib_updateable = False
dw_temp_cooler.ib_updateable = False

Return True
end function

public subroutine wf_recalc_temp_cooler (long al_current_row);Long		ll_sub, &
			ll_RowCount, &
			ll_begin_invent, &
			ll_end_invent, &
			ll_a_shift_processing_total, &
			ll_b_shift_processing_total, &
			ll_transfer_in_total, &
			ll_tram_in_total

ll_RowCount = dw_temp_cooler.RowCount()

If dw_temp_cooler.GetItemString(al_current_row, "shift") = "D" Then
	al_current_row = al_current_row - 2
End If

If dw_temp_cooler.GetItemString(al_current_row, "shift") = "B" Then
	al_current_row --
End If

If dw_temp_cooler.GetItemSTring(al_current_row, "d_shift_info_ind") = "Y" Then

	For ll_sub = al_current_row To ll_RowCount Step 3 //always on 'A' shift row
	
		If ll_sub = 1 Then
			ll_begin_invent = dw_temp_cooler.GetItemNumber(1, "beginning_inventory")
		Else
			ll_begin_invent = dw_temp_cooler.GetItemNumber(ll_sub - 2, "ending_inventory")
		End If
		
		ll_a_shift_processing_total = dw_temp_cooler.GetItemNumber(ll_sub, "processing_total")
		ll_b_shift_processing_total = dw_temp_cooler.GetItemNumber(ll_sub + 1, "processing_total") 
		
		//only B shift line has transfer in and tram in totals
		ll_transfer_in_total = dw_temp_cooler.GetItemNumber(ll_sub + 1, "transfer_in")
		ll_tram_in_total = dw_temp_cooler.GetItemNumber(ll_sub + 1, "tram_in")
		
		ll_end_invent = ll_begin_invent + ll_transfer_in_total + ll_tram_in_total -&
							 ll_a_shift_processing_total - ll_b_shift_processing_total
	
		dw_temp_cooler.SetItem(ll_sub + 1, "ending_inventory", ll_end_invent)
		
	Next		
	
Else
	
	For ll_sub = al_current_row To ll_RowCount Step 2  //always on 'A' shift row
	
		If ll_sub = 1 Then
			ll_begin_invent = dw_temp_cooler.GetItemNumber(1, "beginning_inventory")
		Else
			ll_begin_invent = dw_temp_cooler.GetItemNumber(ll_sub - 1, "ending_inventory")
		End If
		
		ll_a_shift_processing_total = dw_temp_cooler.GetItemNumber(ll_sub, "processing_total")
		ll_b_shift_processing_total = dw_temp_cooler.GetItemNumber(ll_sub + 1, "processing_total") 
		
		//only B shift line has transfer in and tram in totals
		ll_transfer_in_total = dw_temp_cooler.GetItemNumber(ll_sub + 1, "transfer_in")
		ll_tram_in_total = dw_temp_cooler.GetItemNumber(ll_sub + 1, "tram_in")
		
		ll_end_invent = ll_begin_invent + ll_transfer_in_total + ll_tram_in_total -&
							 ll_a_shift_processing_total - ll_b_shift_processing_total
	
		dw_temp_cooler.SetItem(ll_sub + 1, "ending_inventory", ll_end_invent)
	
	
	Next
	
End If

end subroutine

public subroutine wf_recalc_temp_cooler_line (long al_current_row);Long		ll_begin_invent, &
			ll_end_invent, &
			ll_a_shift_processing_total, &
			ll_b_shift_processing_total, &
			ll_transfer_in_total, &
			ll_tram_in_total

If dw_temp_cooler.GetItemString(al_current_row, "shift") = "D" Then
	al_current_row = al_current_row - 2
End If

If dw_temp_cooler.GetItemString(al_current_row, "shift") = "B" Then
	al_current_row --
End If

If al_current_row = 1 Then
	ll_begin_invent = dw_temp_cooler.GetItemNumber(1, "beginning_inventory")
Else
	If dw_temp_cooler.GetItemString(al_current_row, "d_shift_info_ind") = "Y" Then
		ll_begin_invent = dw_temp_cooler.GetItemNumber(al_current_row - 2, "ending_inventory")
	Else
		ll_begin_invent = dw_temp_cooler.GetItemNumber(al_current_row - 1, "ending_inventory")
	End If
End If

ll_a_shift_processing_total = dw_temp_cooler.GetItemNumber(al_current_row, "processing_total")
ll_b_shift_processing_total = dw_temp_cooler.GetItemNumber(al_current_row + 1, "processing_total") 

If dw_temp_cooler.GetItemString(al_current_row,"d_shift_info_ind") = "Y" Then
End If

//only B shift line has transfer in and tram in totals
ll_transfer_in_total = dw_temp_cooler.GetItemNumber(al_current_row + 1, "transfer_in")
ll_tram_in_total = dw_temp_cooler.GetItemNumber(al_current_row + 1, "tram_in")

ll_end_invent = ll_begin_invent + ll_transfer_in_total + ll_tram_in_total -&
					 ll_a_shift_processing_total - ll_b_shift_processing_total

dw_temp_cooler.SetItem(al_current_row + 1, "ending_inventory", ll_end_invent)

	


end subroutine

on w_cooler_inventory.create
int iCurrent
call super::create
this.dw_temp_cooler=create dw_temp_cooler
this.dw_hot_box_cooler=create dw_hot_box_cooler
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_temp_cooler
this.Control[iCurrent+2]=this.dw_hot_box_cooler
this.Control[iCurrent+3]=this.dw_header
end on

on w_cooler_inventory.destroy
call super::destroy
destroy(this.dw_temp_cooler)
destroy(this.dw_hot_box_cooler)
destroy(this.dw_header)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
//iw_frame.im_menu.mf_disable('m_save')
//iw_frame.im_menu.mf_Disable('m_delete')
//iw_frame.im_menu.mf_Disable('m_new')
//iw_frame.im_menu.mf_Disable('m_addrow')
//iw_frame.im_menu.mf_Disable('m_deleterow')


end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
//iw_frame.im_menu.mf_enable('m_save')
//iw_frame.im_menu.mf_enable('m_delete')
//iw_frame.im_menu.mf_enable('m_new')
//iw_frame.im_menu.mf_enable('m_addrow')
//iw_frame.im_menu.mf_enable('m_deleterow')
//
end event

event open;call super::open;This.Title = 'Cooler Inventory'

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'complex_code'
		message.StringParm = dw_header.GetItemString(1, "complex_code")
	Case 'start_date'
		message.StringParm = String(dw_header.GetItemDate(1, "start_date"),"mm/dd/yyyy")		
End choose

end event

event ue_postopen;call super::ue_postopen;//Environment	le_env
//u_sdkcalls				lu_sdk
//
//
////  structure for calls to the rpc's
//istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
//istr_error_info.se_window_name 	= "hotbox"
//istr_error_info.se_user_id 		= sqlca.userid
//
//// get the users color setup
//lu_sdk = Create u_sdkcalls
//GetEnvironment(le_env)
//If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
// 	il_SelectedColor = lu_sdk.nf_GetSysColor(13)
//	il_SelectedTextColor = lu_sdk.nf_GetSysColor(14)
//Else
// 	il_SelectedColor = 255
//	il_SelectedTextColor = 0
//End if
//Destroy(lu_sdk)

is_inquire_window_name = 'w_cooler_inventory_inq'

iu_pas201 = Create u_pas201

iu_ws_pas4 = Create u_ws_pas4

wf_retrieve()

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'complex_code'
		dw_header.SetItem(1, "complex_code", as_value)
	Case 'complex_name'
		dw_header.SetItem(1, "complex_name", as_value)		
	Case 'start_date'
		dw_header.SetItem(1, "start_date", Date(as_value))
End Choose


end event

event resize;call super::resize;////constant integer li_x		= 27
////constant integer li_y		= 288
//
//integer li_x = 37
//integer li_y = 190
//
////dw_hot_box.width	= newwidth - (30 + li_x)
////dw_hot_box.height	= newheight - (30 + li_y)
//
//if il_BorderPaddingWidth >  li_x Then
//                li_x = il_BorderPaddingWidth
//end if
//
//if il_BorderPaddingHeight > li_y Then
//                li_y = il_BorderPaddingHeight
//End If
//
//dw_hot_box.width	= newwidth - li_x
//dw_hot_box.height	= newheight - li_y
end event

type dw_temp_cooler from u_base_dw_ext within w_cooler_inventory
integer x = 2377
integer y = 160
integer width = 1719
integer height = 1216
integer taborder = 30
string dataobject = "d_temp_cooler"
boolean vscrollbar = true
boolean border = false
end type

event scrollvertical;call super::scrollvertical;dw_hot_box_cooler.object.datawindow.verticalscrollposition = scrollpos
end event

event itemchanged;call super::itemchanged;Long		ll_processing_speed, &
			ll_processing_total

Decimal{2}	ld_processing_hours_disp, &
				ld_processing_hours_calc, &
				ld_break

CHOOSE CASE dwo.name
		
	CASE 'processing_hours_disp'
		ld_processing_hours_disp = Dec(data)
		
		If ld_processing_hours_disp > 10.00 Then
			ld_break = .75
		Else
			If ld_processing_hours_disp > 8.00 Then
				ld_break = .50
			Else
				If ld_processing_hours_disp > 0.00 Then
					ld_break = .25
				End If
			End If
		End If	
		
		ld_processing_hours_calc = ld_processing_hours_disp - ld_break
		This.SetItem(row, "processing_hours_calc", ld_processing_hours_calc)
		ll_processing_speed = This.GetItemNumber(row, "processing_speed")
		ll_processing_total = truncate(ld_processing_hours_calc * ll_processing_speed, 0)
		This.SetItem(row, "processing_total", ll_processing_total)
				
	CASE 'processing_speed'
		ld_processing_hours_calc = This.GetItemNumber(row, "processing_hours_calc")
		ll_processing_speed = Int(Dec(data))
		ll_processing_total = truncate(ld_processing_hours_calc * ll_processing_speed, 0)
		This.SetItem(row, "processing_total", ll_processing_total)		
		
	CASE 'processing_total'
		ll_processing_total = Long(data)
		ld_processing_hours_calc = ll_processing_total / This.GetItemNumber(row, "processing_speed")
		
		If ld_processing_hours_calc > 10.00 Then
			ld_break = .75
			ld_processing_hours_disp = ld_processing_hours_calc + ld_break
		Else
			If ld_processing_hours_calc > 8.00 Then
				ld_break = .50
				ld_processing_hours_disp = ld_processing_hours_calc + ld_break
				If ld_processing_hours_disp > 10.00 Then
					ld_processing_hours_disp = ld_processing_hours_disp + .25
				End If
			Else
				If ld_processing_hours_calc > 0.00 Then
					ld_break = .25
					ld_processing_hours_disp = ld_processing_hours_calc + ld_break
					If ld_processing_hours_disp > 8.00 Then
						ld_processing_hours_disp = ld_processing_hours_disp + .25
					End If					
				End If
			End If
		End If	
		
		This.SetItem(row, "processing_hours_disp", ld_processing_hours_disp)  
		This.SetItem(row, "processing_hours_calc", ld_processing_hours_calc)		

END CHOOSE		

il_changed_temp_cooler_row = row

parent.PostEvent("ue_recalc_temp_cooler")	
end event

type dw_hot_box_cooler from u_base_dw_ext within w_cooler_inventory
event ue_recalc_hot_box ( )
event ue_recalc_temp_cooler ( )
integer y = 160
integer width = 2377
integer height = 1216
integer taborder = 20
string dataobject = "d_hot_box_cooler"
boolean vscrollbar = true
boolean border = false
end type

event ue_recalc_temp_cooler();wf_recalc_temp_cooler (il_changed_temp_cooler_row)
end event

event scrollvertical;call super::scrollvertical;dw_temp_cooler.object.datawindow.verticalscrollposition = scrollpos
end event

event itemchanged;call super::itemchanged;Long		ll_slaughter_speed, &
			ll_transfer_speed, &
			ll_slaughter_total, &
			ll_transfer_total, &
			ll_a_shift_transfer_total, &
			ll_b_shift_transfer_total, &
			ll_d_shift_transfer_total
			
Decimal{2}	ld_slaughter_hours_disp, &
				ld_slaughter_hours_calc, &
				ld_transfer_hours_disp, &
				ld_transfer_hours_calc, &
				ld_break
			

CHOOSE CASE dwo.name
		
	CASE 'slaughter_hours_disp'
		ld_slaughter_hours_disp = Dec(data)
		
		If ld_slaughter_hours_disp > 10.00 Then
			ld_break = .75
		Else
			If ld_slaughter_hours_disp > 8.00 Then
				ld_break = .50
			Else
				If ld_slaughter_hours_disp > 0.25 Then
					ld_break = .25
				End If
			End If
		End If	
		
		ld_slaughter_hours_calc = ld_slaughter_hours_disp - ld_break
		This.SetItem(row, "slaughter_hours_calc", ld_slaughter_hours_calc)
		ll_slaughter_speed = This.GetItemNumber(row, "slaughter_speed")
		ll_slaughter_total = truncate(ld_slaughter_hours_calc * ll_slaughter_speed, 0)
		This.SetItem(row, "slaughter_total", ll_slaughter_total)
				
	CASE 'slaughter_speed'
		ld_slaughter_hours_calc = This.GetItemNumber(row, "slaughter_hours_calc")
		ll_slaughter_speed = Long(Dec(data))
		ll_slaughter_total = truncate(ld_slaughter_hours_calc * ll_slaughter_speed, 0)
		This.SetItem(row, "slaughter_total", ll_slaughter_total)		
		
	CASE 'slaughter_total'
		ll_slaughter_total = Long(data)
		ld_slaughter_hours_calc = ll_slaughter_total / This.GetItemNumber(row, "slaughter_speed")
		
		If ld_slaughter_hours_calc > 10.00 Then
			ld_break = .75
			ld_slaughter_hours_disp = ld_slaughter_hours_calc + ld_break
		Else
			If ld_slaughter_hours_calc > 8.00 Then
				ld_break = .50
				ld_slaughter_hours_disp = ld_slaughter_hours_calc + ld_break
				if ld_slaughter_hours_disp > 10.00 Then
					ld_slaughter_hours_disp = ld_slaughter_hours_disp + .25
				End If
			Else
				If ld_slaughter_hours_calc > 0.00 Then
					ld_break = .25
					ld_slaughter_hours_disp = ld_slaughter_hours_calc + ld_break
					if ld_slaughter_hours_disp > 8.00 Then
						ld_slaughter_hours_disp = ld_slaughter_hours_disp + .25
					End If
				End If
			End If
		End If	
		
	
		This.SetItem(row, "slaughter_hours_disp", ld_slaughter_hours_disp)  
		This.SetItem(row, "slaughter_hours_calc", ld_slaughter_hours_calc)
		
	
	Case 'transfer_hours_disp' 
		
		ld_transfer_hours_disp = Dec(data)
		
		If ld_transfer_hours_disp > 10.00 Then
			ld_break = .75
		Else
			If ld_transfer_hours_disp > 8.00 Then
				ld_break = .50
			Else
				If ld_transfer_hours_disp > 0.25 Then
					ld_break = .25
				End If
			End If
		End If	
		
		ld_transfer_hours_calc = ld_transfer_hours_disp - ld_break
		This.SetItem(row, "transfer_hours_calc", ld_transfer_hours_calc)
		ll_transfer_speed = This.GetItemNumber(row, "transfer_speed")
		
		IF This.GetItemString(row, "d_shift_info_ind") = 'Y' Then
			
			If This.GetItemString(row, "shift") = 'A' Then
				ll_a_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
				ll_b_shift_transfer_total = truncate(This.GetItemNumber(row + 1, "transfer_hours_calc") * &
								This.GetItemNumber(row + 1, "transfer_speed"), 0)
				ll_d_shift_transfer_total = truncate(This.GetItemNumber(row + 2, "transfer_hours_calc") * &
								This.GetItemNumber(row + 2, "transfer_speed"), 0)								
													
							
				This.SetItem(row, "transfer_total", 0)	
				This.SetItem(row + 1, "transfer_total", 0)
				This.SetItem(row + 2, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
				If dw_temp_cooler.RowCount() > 0 Then
					dw_temp_cooler.SetItem(row + 1, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
				End If
			Else
				
				If This.GetItemString(row, "shift") = 'B' Then
					ll_a_shift_transfer_total = truncate(This.GetItemNumber(row - 1, "transfer_hours_calc") * &
									This.GetItemNumber(row - 1, "transfer_speed"), 0)
					ll_b_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
					ll_d_shift_transfer_total = truncate(This.GetItemNumber(row + 1, "transfer_hours_calc") * &
									This.GetItemNumber(row + 1, "transfer_speed"), 0)					
								
					This.SetItem(row - 1, "transfer_total", 0)	
					This.SetItem(row, "transfer_total", 0)
					This.SetItem(row + 1, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
					If dw_temp_cooler.RowCount() > 0 Then
						dw_temp_cooler.SetItem(row, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
					End If
				Else
					ll_a_shift_transfer_total = truncate(This.GetItemNumber(row - 2, "transfer_hours_calc") * &
									This.GetItemNumber(row - 2, "transfer_speed"), 0)
					ll_b_shift_transfer_total = truncate(This.GetItemNumber(row - 1, "transfer_hours_calc") * &
									This.GetItemNumber(row - 1, "transfer_speed"), 0)									
					ll_d_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
		
							
					This.SetItem(row - 2, "transfer_total", 0)
					This.SetItem(row - 1, "transfer_total", 0)					

					This.SetItem(row , "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
					If dw_temp_cooler.RowCount() > 0 Then
						dw_temp_cooler.SetItem(row - 1, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
					End If
				End If
				
			End If
			
			
		Else	
			If This.GetItemString(row, "shift") = 'A' Then
				ll_a_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
				ll_b_shift_transfer_total = truncate(This.GetItemNumber(row + 1, "transfer_hours_calc") * &
								This.GetItemNumber(row + 1, "transfer_speed"), 0)
													
							
				This.SetItem(row, "transfer_total", 0)	
				This.SetItem(row + 1, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total)
				If dw_temp_cooler.RowCount() > 0 Then
					dw_temp_cooler.SetItem(row + 1, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total)
				End If
			Else
				ll_a_shift_transfer_total = truncate(This.GetItemNumber(row - 1, "transfer_hours_calc") * &
								This.GetItemNumber(row - 1, "transfer_speed"), 0)
				ll_b_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
							
				This.SetItem(row - 1, "transfer_total", 0)	
				This.SetItem(row, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total)
				If dw_temp_cooler.RowCount() > 0 Then
					dw_temp_cooler.SetItem(row, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total)
				End If
			End If
		End If
		
		
	Case 'transfer_speed'
		ld_transfer_hours_calc = This.GetItemNumber(row, "transfer_hours_calc")
		ll_transfer_speed = Long(data)
		
		IF This.GetItemString(row, "d_shift_info_ind") = 'Y' Then
			
			If This.GetItemString(row, "shift") = 'A' Then
				ll_a_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
				ll_b_shift_transfer_total = truncate(This.GetItemNumber(row + 1, "transfer_hours_calc") * &
								This.GetItemNumber(row + 1, "transfer_speed"), 0)
				ll_d_shift_transfer_total = truncate(This.GetItemNumber(row + 2, "transfer_hours_calc") * &
								This.GetItemNumber(row + 2, "transfer_speed"), 0)								
							
				This.SetItem(row, "transfer_total", 0)	
				This.SetItem(row + 1, "transfer_total", 0)
				This.SetItem(row + 2, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
				If dw_temp_cooler.RowCount() > 0 Then
					dw_temp_cooler.SetItem(row + 1, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
				End If
				
			Else
				If This.GetItemString(row, "shift") = 'B' Then
					ll_a_shift_transfer_total = truncate(This.GetItemNumber(row - 1, "transfer_hours_calc") * &
									This.GetItemNumber(row - 1, "transfer_speed"), 0)
					ll_b_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
					ll_d_shift_transfer_total = truncate(This.GetItemNumber(row + 1, "transfer_hours_calc") * &
									This.GetItemNumber(row + 1, "transfer_speed"), 0)					
								
					This.SetItem(row - 1, "transfer_total", 0)
					This.SetItem(row, "transfer_total", 0)
					This.SetItem(row + 1, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
					If dw_temp_cooler.RowCount() > 0 Then
						dw_temp_cooler.SetItem(row, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
					End If
				Else
					
					ll_a_shift_transfer_total = truncate(This.GetItemNumber(row - 2, "transfer_hours_calc") * &
									This.GetItemNumber(row - 2, "transfer_speed"), 0)
					ll_b_shift_transfer_total = truncate(This.GetItemNumber(row - 1, "transfer_hours_calc") * &
									This.GetItemNumber(row - 1, "transfer_speed"), 0)									
					ll_d_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
							
					This.SetItem(row - 2, "transfer_total", 0)
					This.SetItem(row - 1, "transfer_total", 0)
					This.SetItem(row, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
					If dw_temp_cooler.RowCount() > 0 Then
						dw_temp_cooler.SetItem(row - 1, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total + ll_d_shift_transfer_total)
					End If
				End If	
				
			End If			
			
		Else

			If This.GetItemString(row, "shift") = 'A' Then
				ll_a_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
				ll_b_shift_transfer_total = truncate(This.GetItemNumber(row + 1, "transfer_hours_calc") * &
								This.GetItemNumber(row + 1, "transfer_speed"), 0)
							
				This.SetItem(row, "transfer_total", 0)	
				This.SetItem(row + 1, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total)
				If dw_temp_cooler.RowCount() > 0 Then
					dw_temp_cooler.SetItem(row + 1, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total)
				End If
				
			Else
				ll_a_shift_transfer_total = truncate(This.GetItemNumber(row - 1, "transfer_hours_calc") * &
								This.GetItemNumber(row - 1, "transfer_speed"), 0)
				ll_b_shift_transfer_total = truncate(ld_transfer_hours_calc * ll_transfer_speed, 0)
							
				This.SetItem(row - 1, "transfer_total", 0)	
				This.SetItem(row, "transfer_total", ll_a_shift_transfer_total + ll_b_shift_transfer_total)
				If dw_temp_cooler.RowCount() > 0 Then
					dw_temp_cooler.SetItem(row, "transfer_in", ll_a_shift_transfer_total + ll_b_shift_transfer_total)
				End If
			End If
			
		End If
		
	CASE 'transfer_total'
		
		If This.GetItemDate(row, "work_date") >= Today() Then
		
			ll_transfer_total = Long(data)
			
			IF This.GetItemString(row, "d_shift_info_ind") = 'Y' Then
				
				ll_a_shift_transfer_total = truncate(This.GetItemNumber(row - 2, "transfer_hours_calc") * &
								This.GetItemNumber(row - 2, "transfer_speed"), 0)
								
				ll_d_shift_transfer_total = truncate(This.GetItemNumber(row, "transfer_hours_calc") * &
								This.GetItemNumber(row,  "transfer_speed"), 0)								
				
				ld_transfer_hours_calc = (ll_transfer_total - ll_a_shift_transfer_total - ll_d_shift_transfer_total) / This.GetItemNumber(row - 1, "transfer_speed")

				
			Else
				ll_a_shift_transfer_total = truncate(This.GetItemNumber(row - 1, "transfer_hours_calc") * &
								This.GetItemNumber(row - 1, "transfer_speed"), 0)
								
				ld_transfer_hours_calc = (ll_transfer_total - ll_a_shift_transfer_total) / This.GetItemNumber(row - 1, "transfer_speed")

			End If
											
			
			If ld_transfer_hours_calc > 10.00 Then
				ld_break = .75
				ld_transfer_hours_disp = ld_transfer_hours_calc + ld_break
			Else
				If ld_transfer_hours_calc > 8.00 Then
					ld_break = .50
					ld_transfer_hours_disp = ld_transfer_hours_calc + ld_break
					If ld_transfer_hours_disp > 10.00 Then
						ld_transfer_hours_disp = ld_transfer_hours_disp + .25
					End If					
					
				Else
					If ld_transfer_hours_calc > 0.00 Then
						ld_break = .25
						ld_transfer_hours_disp = ld_transfer_hours_calc + ld_break
						If ld_transfer_hours_disp > 8.00 Then
							ld_transfer_hours_disp = ld_transfer_hours_disp + .25
						End If						
					End If
				End If
			End If	
			
			IF This.GetItemString(row, "d_shift_info_ind") = 'Y' Then
				This.SetItem(row - 1, "transfer_hours_disp", ld_transfer_hours_disp)  
				This.SetItem(row - 1, "transfer_hours_calc", ld_transfer_hours_calc)

				If dw_temp_cooler.RowCount() > 0 Then
					dw_temp_cooler.SetItem(row - 1, "transfer_in", ll_transfer_total)
				End If
				
			Else
				This.SetItem(row, "transfer_hours_disp", ld_transfer_hours_disp)  
				This.SetItem(row, "transfer_hours_calc", ld_transfer_hours_calc)

				If dw_temp_cooler.RowCount() > 0 Then
					dw_temp_cooler.SetItem(row, "transfer_in", ll_transfer_total)
				End If
				
			End If
	End If			

END CHOOSE		

il_changed_hot_box_row = row

parent.PostEvent("ue_recalc_hot_box")


end event

type dw_header from u_base_dw_ext within w_cooler_inventory
integer x = 366
integer y = 32
integer width = 2450
integer height = 128
integer taborder = 10
string dataobject = "d_cooler_invt_hdr"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

