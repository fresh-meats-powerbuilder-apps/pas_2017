﻿$PBExportHeader$w_carcass_downgrade.srw
forward
global type w_carcass_downgrade from w_base_response_ext
end type
type dw_carcass_downgrade from u_base_dw_ext within w_carcass_downgrade
end type
type dw_downgrade_header from u_base_dw_ext within w_carcass_downgrade
end type
end forward

global type w_carcass_downgrade from w_base_response_ext
integer width = 2399
string title = "downgrade_from_to_t"
long backcolor = 67108864
event ue_query ( unsignedlong wparam,  long lparam )
dw_carcass_downgrade dw_carcass_downgrade
dw_downgrade_header dw_downgrade_header
end type
global w_carcass_downgrade w_carcass_downgrade

type variables
s_error			istr_error_info

datastore			ids_tree,&
			ids_desc

u_rmt001			iu_rmt001
u_pas203			iu_pas203
u_ws_pas4	iu_ws_pas4

w_carcass_tracking_view	iw_parent
Boolean			ib_updating,&
			ib_char_key, &
			ib_no_inquire

Long			il_char_count, &
			il_rec_count, &
			il_SelectedColor, &
			il_SelectedTextColor 



String			is_update_string,&  
			is_desc, &
			is_header
Window			iw_temp
end variables

forward prototypes
public function boolean wf_retrieve ()
public function integer wf_update_modify (long al_row)
public function boolean wf_update ()
end prototypes

event ue_query(unsignedlong wparam, long lparam);istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "downgrade"
istr_error_info.se_user_id 		= sqlca.userid

iu_rmt001 = Create u_rmt001

//dw_carcass_downgrade.InsertRow(0)

wf_update()

end event

public function boolean wf_retrieve ();Long					ll_rec_count_pas
				
String				ls_plant, &
						ls_plant_desc, & 
						ls_header, &
						ls_detail, &
						ls_temp, &
						ls_date, &
						ls_from_to
Boolean				lb_return

//This.TriggerEvent('closequery') 

dw_carcass_downgrade.reset()
//dw_downgrade_header.reset()

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"

ls_header  = dw_downgrade_header.GetItemString( 1, "plant") + "~t"
ls_header += String( dw_downgrade_header.GetItemDate( 1, "downgrade_date"),"YYYY-MM-DD")+"~t"
ls_header += dw_downgrade_header.GetItemString( 1, "from_to_char") + "~t"
ls_header += dw_downgrade_header.GetItemstring(1,"from_to_ind") +"~r~n"

dw_carcass_downgrade.reset()

/*lb_return = iu_rmt001.uf_rmtr28mr_carcass_downgrade_inq(istr_error_info, & 
										ls_header, &
										ls_detail)
*/
									
//Changed old netwise function to new web services function
lb_return = iu_ws_pas4.NF_RMTR28NR(istr_error_info, & 
										ls_header, &
										ls_detail)
if NOT lb_return  Then
	This.SetRedraw(True) 
	Return False
End If			

//ls_from_to = dw_downgrade_header.GetItemstring(1,"from_to_ind")
//if ls_from_to = 'F' then
//	dw_carcass_downgrade.Object.downgrade_from_to_t.Text = 'Downgrade From'
//else
//	dw_carcass_downgrade.Object.downgrade_from_to_t.Text = 'Downgrade To'
//end if

ll_rec_count_pas = dw_carcass_downgrade.ImportString(ls_detail)


If ll_rec_count_pas > 0 Then 
	SetMicroHelp(String(ll_rec_count_pas) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if
//
This.SetRedraw(True) 

dw_carcass_downgrade.ResetUpdate()
dw_carcass_downgrade.SetFocus()

Return True

end function

public function integer wf_update_modify (long al_row);String						ls_describe

is_update_string +=  dw_carcass_downgrade.GetItemString(al_row,"to_char") + "~t"
is_update_string += string(dw_carcass_downgrade.GetItemNumber(al_row, "shift_a_qty")) + "~t"
is_update_string += string(dw_carcass_downgrade.GetItemNumber(al_row, "shift_b_qty")) + "~r~n"	 

Return 0
end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num, &
					ll_row_count

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name, &
					ls_row_name, &
					ls_process_date
dwItemStatus	ldwis_status			
u_string_functions u_string



IF dw_carcass_downgrade.AcceptText() = -1 Then 
	Return False
End if

ls_header = ''
ls_header  = dw_downgrade_header.GetItemString( 1, "plant") + "~t"
ls_header += String( dw_downgrade_header.GetItemDate( 1, "downgrade_date"),"YYYY-MM-DD")+"~t"
ls_header += dw_downgrade_header.GetItemString( 1, "from_to_char") + "~t"
ls_header += dw_downgrade_header.GetItemstring(1,"from_to_ind") +"~r~n"
is_update_string = ""
ll_row_count = dw_carcass_downgrade.RowCount()
ll_modrows = dw_carcass_downgrade.ModifiedCount()

This.SetRedraw(False)
DO WHILE ll_row <= ll_row_count
	ll_Row = dw_carcass_downgrade.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		wf_update_modify(ll_row)  
	else
		ll_Row = ll_row_count + 1
	END IF
LOOP

/*IF Not iu_rmt001.uf_rmtr29mr_carcass_downgrade_upd(istr_error_info, ls_header, &
											is_update_string) THEN 
	This.SetRedraw(True)
	Return False
End If
*/
IF Not iu_ws_pas4.NF_RMTR29NR(istr_error_info, ls_header, &
											is_update_string) THEN 
	This.SetRedraw(True)
	Return False
End If

dw_carcass_downgrade.ResetUpdate()
This.SetRedraw(True)
Return( True )

end function

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "downgrade"
istr_error_info.se_user_id 		= sqlca.userid

string ls_from_to
date   ldt_date

ls_from_to = dw_downgrade_header.GetItemString( 1, "from_to_ind") 

if ls_from_to = 'F' then
	This.Title = "Downgrade From"
else
	This.Title = "Downgrade To"
end if


iu_rmt001 = Create u_rmt001
iu_ws_pas4	= Create u_ws_pas4

//dw_carcass_downgrade.InsertRow(0)

ldt_date = dw_downgrade_header.GetItemDate( 1, "downgrade_date")
if ldt_date < today() then
	cb_base_ok.Enabled = false
else
	cb_base_ok.Enabled = true
end if
wf_retrieve()

end event

on w_carcass_downgrade.create
int iCurrent
call super::create
this.dw_carcass_downgrade=create dw_carcass_downgrade
this.dw_downgrade_header=create dw_downgrade_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carcass_downgrade
this.Control[iCurrent+2]=this.dw_downgrade_header
end on

on w_carcass_downgrade.destroy
call super::destroy
destroy(this.dw_carcass_downgrade)
destroy(this.dw_downgrade_header)
end on

event ue_base_ok();call super::ue_base_ok;long ll_row, ll_current_value
string ls_col, ls_row_col, ls_char, ls_temp
u_string_functions		lu_strings

integer li_total, li_row_total, li_row_count, li_row, li_row_add, li_current_value, li_current_col_qty, &
		li_orig_col_qty,  li_new_col_qty, li_shift_a_qty, li_shift_b_qty, li_shift_a_change, li_shift_b_change

iw_frame.SetMicroHelp("Updating Database ...")

wf_update() 

//	iw_frame.SetMicroHelp("Error saving shift A information.")
//	return
//elseif not wf_update(dw_shift_b, "B") then 
//	iw_frame.SetMicroHelp("Error saving shift B information.")
//	return
//end if

if dw_downgrade_header.GetItemString(1, 'from_to_ind') = 'F' then
	li_row_add = 1
else 
	li_row_add = -1
end if

dw_carcass_downgrade.AcceptText()

iw_parent.wf_get_row_col()
ls_row_col = Message.StringParm
ll_row = Long(lu_strings.nf_gettoken(ls_row_col, '~t'))
ls_col = ls_row_col
li_orig_col_qty = iw_parent.dw_satellite_cattle.GetItemNumber(ll_row, ls_col)
li_row_count = dw_carcass_downgrade.RowCount()
li_row = 1

do while li_row <= li_row_count
	ls_char = dw_carcass_downgrade.GetItemString(li_row, "to_char")
//	li_shift_a_qty = dw_carcass_downgrade.GetItemNumber(li_row, "shift_a_qty")
//	li_shift_b_qty = dw_carcass_downgrade.GetItemNumber(li_row, "shift_b_qty")
//	li_shift_a_change = dw_carcass_downgrade.GetItemNumber(li_row, "shift_a_change")
//	li_shift_b_change = dw_carcass_downgrade.GetItemNumber(li_row, "shift_b_change")
//	li_row_total = li_shift_a_qty - li_shift_a_change &
//						+ li_shift_b_qty - li_shift_b_change
	li_row_total = dw_carcass_downgrade.GetItemNumber(li_row, "shift_a_change") &
					 + dw_carcass_downgrade.GetItemNumber(li_row, "shift_b_change")
//	
	
	li_total += li_row_total
	dw_carcass_downgrade.SetItem(li_row, "shift_a_change", 0)
	dw_carcass_downgrade.SetItem(li_row, "shift_b_change", 0)
	 
	iw_parent.wf_get_col_names(ls_char + '~t')
	ls_temp = Message.StringParm
	ls_col = lu_strings.nf_gettoken(ls_temp, '~t')

	 li_current_col_qty = iw_parent.dw_satellite_cattle.GetItemNumber(ll_row + li_row_add, ls_col)
	 li_new_col_qty = li_current_col_qty - li_row_total
//	sap li_new_row_total = li_current_col_qty - li_orig_col_qty + li_row_total
	iw_parent.dw_satellite_cattle.SetItem(ll_row + li_row_add, ls_col, li_new_col_qty)
	iw_parent.dw_satellite_cattle.SetItemStatus(ll_row + li_row_add, ls_col, Primary!,notmodified!) 

	
	li_row++
loop
li_current_col_qty = iw_parent.dw_satellite_cattle.GetItemNumber(ll_row, ls_row_col)
//sapli_current_value = iw_parent.dw_satellite_cattle.GetItemNumber(ll_row, ls_row_col)
li_current_value = li_current_col_qty - li_total
//li_total = li_current_value - li_total
// sapiw_parent.dw_satellite_cattle.SetItem(ll_row, ls_row_col, li_current_value)
iw_parent.dw_satellite_cattle.SetItem(ll_row, ls_row_col, li_current_value)
iw_parent.dw_satellite_cattle.SetItemStatus(ll_row,ls_row_col, Primary!,notmodified!) 

iw_parent.wf_get_modifyable_data()

iw_frame.SetMicroHelp("Update successful.")

//CloseWithReturn(This, "Y")

Close(this)
end event

event ue_base_cancel();call super::ue_base_cancel;Close(this)
end event

event ue_set_data(string as_data_item, string as_value);call super::ue_set_data;//Choose Case as_data_item
//	Case 'plant'
//		dw_plant.uf_set_plant_code(as_value)
//	Case 'transfer_date'
//		dw_date.uf_set_transfer_date(Date(as_value))
//	Case 'from_char'
//		dw_from_char.uf_set_from_char(as_value)
//End Choose

end event

event type string ue_get_data(string as_value);call super::ue_get_data;//Choose Case as_value
//	Case 'plant'
//		message.StringParm = dw_plant.uf_get_plant_code()
//	Case 'transfer_date'
//		message.StringParm = String(dw_date.uf_get_transfer_date(),'mm-dd-yyyy')
//	Case 'from_char'
//		message.StringParm = dw_from_char.uf_get_from_char()
//End choose
Return as_value
end event

event open;call super::open;String	ls_inquire, &
			ls_from_to, &
			ls_direction
Long		ll_ret


iw_parent = Message.PowerObjectParm
iw_parent.wf_get_header_data()
ls_inquire = Message.StringParm


If iw_frame.iu_string.nf_IsEmpty(ls_inquire) Then 
	ib_no_inquire = False
Else
	ib_no_inquire = True
	//	dw_manage_inventory_header.Reset()
	ll_ret = dw_downgrade_header.ImportString(ls_inquire)
	
End If

//ls_plant = dw_downgrade_header.GetItemString( 1, "plant") 
	
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_inquire')
iw_frame.im_menu.mf_enable('m_save')

end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_save')




end event

type cb_base_help from w_base_response_ext`cb_base_help within w_carcass_downgrade
boolean visible = false
integer x = 1668
integer y = 1261
integer taborder = 0
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_carcass_downgrade
integer x = 1375
integer y = 1261
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_carcass_downgrade
integer x = 1090
integer y = 1264
integer taborder = 20
end type

event cb_base_ok::clicked;call super::clicked;//This.PostEvent('ue_query')

end event

type dw_carcass_downgrade from u_base_dw_ext within w_carcass_downgrade
integer x = 410
integer y = 205
integer width = 1302
integer height = 877
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_carcass_downgrade"
boolean controlmenu = true
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;//If This.RowCount() = 0 Then This.InsertRow(0)


end event

event itemchanged;call super::itemchanged;integer	li_orig_shift_qty, li_shift_qty


CHOOSE CASE dwo.name
		
	CASE 'shift_a_qty'
		li_orig_shift_qty = dw_carcass_downgrade.GetItemNumber(row, "shift_a_qty", Primary!, true)
		li_orig_shift_qty = li_orig_shift_qty - Dec(data)
		dw_carcass_downgrade.SetItem(row, "shift_a_change", li_orig_shift_qty)

	CASE 'shift_b_qty'
		li_orig_shift_qty = dw_carcass_downgrade.GetItemNumber(row, "shift_b_qty", Primary!, true)
		li_orig_shift_qty = li_orig_shift_qty - Dec(data)
		dw_carcass_downgrade.SetItem(row, "shift_b_change", li_orig_shift_qty)
END CHOOSE		
end event

type dw_downgrade_header from u_base_dw_ext within w_carcass_downgrade
integer x = 37
integer width = 2161
integer height = 208
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_downgrade_header"
boolean border = false
end type

