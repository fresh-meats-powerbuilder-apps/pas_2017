﻿$PBExportHeader$w_cooler_actual.srw
forward
global type w_cooler_actual from w_base_sheet_ext
end type
type dw_cooler from u_base_dw_ext within w_cooler_actual
end type
type dw_plant from u_plant within w_cooler_actual
end type
type dw_dates from u_begin_end_dates within w_cooler_actual
end type
end forward

global type w_cooler_actual from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 2875
integer height = 1517
long backcolor = 67108864
dw_cooler dw_cooler
dw_plant dw_plant
dw_dates dw_dates
end type
global w_cooler_actual w_cooler_actual

type variables
u_rmt001		iu_rmt001
u_ws_pas4	iu_ws_pas4

s_error		istr_error_info

String		is_update_string

Long		il_rec_count,&
		il_SelectedColor,&
		il_SelectedTextColor

Boolean		ib_no_inquire

Window		iw_parentwindow
end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();Long						ll_rec_count, &
							ll_row, &
							ll_count, &
							ll_column = 1
							
Int	il_temp							
							
String					ls_plant, &
							ls_column, & 
							ls_header, &
							ls_column_headings, &
							ls_detail, &
							ls_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_describe
Boolean					lb_no_characteristics							
u_string_functions 	lu_string				



If dw_plant.AcceptText() = -1 Then Return False
If dw_dates.AcceptText() = -1 Then Return False
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

If Not ib_no_inquire Then
	OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
	If not ib_inquire then
		Return False
	End If
End If


SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 
// clear out prev values reset the datawindow
ls_describe = "col_" + String(ll_column) + ".Visible"
Do While dw_cooler.Describe(ls_describe) > '0'
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 0 " + &
		" col_" + String(ll_column) + ".Visible = 0 " 
	If ll_column = 1 Then
		ls_TitleModify += " co_col_" + String(ll_column) + ".Visible = 0 " + &
			" t_total_" + String(ll_column) + ".Visible = 0 " + &
			" date" +  ".Visible = 0 " + &
			" detail_text_" + String(ll_column) + ".Visible = 0 " 
	End If
	ll_column ++
	ls_describe = "col_" + String(ll_column) + ".Visible"
Loop
//messagebox('reset values',ls_titlemodify)
//messagebox('modify result',dw_cooler.Modify (ls_TitleModify))
//messagebox('problem',Mid ( ls_TitleModify, 124 ))

dw_cooler.Modify ( ls_TitleModify )
ll_column = 1	
ls_TitleModify = ''

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr20mr_inq_carcass_view"

ls_header  = dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_dates.uf_get_begin_date(), 'yyyy-mm-dd') + '~t'
ls_header += String(dw_dates.uf_get_end_date(), 'yyyy-mm-dd') + '~t'

//MessageBox("header",ls_header)

dw_cooler.reset()
//
/*If Not iu_rmt001.uf_rmtr26mr_inq_cooler_actual(istr_error_info, & 
										ls_column_headings, &
										ls_detail, &
										ls_header) Then 
										This.SetRedraw(True) 
										Return False
End If			
*/
If Not iu_ws_pas4.NF_RMTR26NR(istr_error_info, & 
										ls_column_headings, &
										ls_detail, &
										ls_header) Then 
										This.SetRedraw(True) 
										Return False
End If			

//MessageBox("Return header",ls_header)
//MessageBox("Column headings",ls_column_headings)
//MessageBox("Detail",ls_detail)


//ls_column_headings = 'NOROLL~tNO ROLL~tDAVE 3~tDAVE 4~tDAVE 5~tDAVE 6~tDAVE 7~tDAVE 8~tDAVE 9~tDAVE 10~tDAVE 11~tDAVE 12~t'
If lu_string.nf_amiempty(ls_column_headings) Then
	MessageBox('No Plant Characteristics','There are no PA/Plant Characteristics assigned for Plant ' + dw_plant.uf_get_plant_code() + &
	'.  You will not be able to use this window until they have been entered.',StopSign!,OK!)
	lb_no_characteristics = True 
End If

DO WHILE Not lu_string.nf_amiempty(ls_column_headings) or ll_column > 50
	//messagebox('The loop',string(ll_column))
	ls_temp = lu_string.nf_gettoken(ls_column_headings,"~t")
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 1 " + &
		" col_" + String(ll_column) + ".Visible = 1 " 
		If ll_column = 1 Then
			ls_TitleModify += " co_col_" + String(ll_column) + ".Visible = 1 " + &
				" t_total_" + String(ll_column) + ".Visible = 1 " + &
				" date" +  ".Visible = 1 " + &
				" detail_text_" + String(ll_column) + ".Visible = 1 " 
		End If
	ll_column ++
LOOP
//messagebox('update values',ls_titlemodify)
//messagebox('modify result',dw_cooler.Modify (ls_TitleModify))
//messagebox('problem',Mid ( ls_TitleModify, 131 ))
dw_cooler.Modify ( ls_TitleModify )


//ls_detail = &
//'02/11/2000~t-11~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t-22~t0~t0~t0~t0~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t3~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t4~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t5~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t6~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t7~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t8~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t9~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'02/11/2000~t10~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' 
//MessageBox("ls_detail",ls_detail)

ll_rec_count = dw_cooler.ImportString(ls_detail)

dw_cooler.SetItem(1, "back_color", il_SelectedColor)
dw_cooler.SetItem(1, "text_color", il_SelectedTextColor)

If ll_rec_count > 0 and Not lb_no_characteristics Then 
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

// Must do because of the reinquire
dw_cooler.Object.DataWindow.HorizontalScrollSplit = '0'
dw_cooler.Object.DataWindow.HorizontalScrollPosition2 =  '0'
dw_cooler.Object.DataWindow.HorizontalScrollSplit = '570'
dw_cooler.Object.DataWindow.HorizontalScrollPosition2 =  '570'

This.SetRedraw(True) 

dw_cooler.ResetUpdate()
dw_cooler.SetFocus()

Return True

end function

on w_cooler_actual.create
int iCurrent
call super::create
this.dw_cooler=create dw_cooler
this.dw_plant=create dw_plant
this.dw_dates=create dw_dates
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cooler
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_dates
end on

on w_cooler_actual.destroy
call super::destroy
destroy(this.dw_cooler)
destroy(this.dw_plant)
destroy(this.dw_dates)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_save')


end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_save')

end event

event open;call super::open;String 					ls_temp
u_string_functions 	lu_string


ls_temp = Message.StringParm	

This.Title = 'Actual Transfer'

dw_dates.uf_enable(false)
dw_plant.disable()
dw_dates.uf_set_begin_date(Today())
dw_dates.uf_set_end_date(Today())


end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'begin_date'
		message.StringParm = String(dw_dates.uf_get_begin_date(), 'YYYY-MM-dd')
	Case 'end_date'
		message.StringParm = String(dw_dates.uf_get_end_date(), 'YYYY-MM-dd')
End choose

end event

event ue_postopen;call super::ue_postopen;Environment	le_env
u_sdkcalls				lu_sdk


//This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "cooleraC"
istr_error_info.se_user_id 		= sqlca.userid

// get the users color setup
lu_sdk = Create u_sdkcalls
GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = lu_sdk.nf_GetSysColor(13)
	il_SelectedTextColor = lu_sdk.nf_GetSysColor(14)
Else
 	il_SelectedColor = 255
	il_SelectedTextColor = 0
End if
Destroy(lu_sdk)

// open inquire window
is_inquire_window_name = 'w_hot_box_actual_inq'

iu_rmt001 = Create u_rmt001
iu_ws_pas4 = Create u_ws_pas4

wf_retrieve()


end event

event ue_set_data;call super::ue_set_data;
Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'begin_date'
		dw_dates.uf_set_begin_date( Date(as_value ))
	Case 'end_date'
		dw_dates.uf_set_end_date( Date(as_value ))
End Choose


end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288

integer li_x		= 57
integer li_y		= 190

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If
  
//dw_cooler.width	= newwidth - (30 + li_x)
//dw_cooler.height	= newheight - (30 + li_y)

dw_cooler.width	= newwidth - (li_x)
dw_cooler.height	= newheight - (li_y)


// Must do because of the split horizonal bar 
dw_cooler.Object.DataWindow.HorizontalScrollSplit = '0'
dw_cooler.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_cooler.Object.DataWindow.HorizontalScrollSplit = '570'
dw_cooler.Object.DataWindow.HorizontalScrollPosition2 =  '570'


end event

type dw_cooler from u_base_dw_ext within w_cooler_actual
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
integer y = 189
integer width = 2757
integer height = 1200
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_cooler_actual"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event itemchanged;call super::itemchanged;String		ls_name

// I did this to make the code more generic
ls_name = dwo.name
ls_name = Left(ls_name, Len(ls_name) - 2)

CHOOSE CASE ls_name
	Case 'col'
		IF Not IsNumber(data) Then
			iw_frame.SetMicroHelp("All Beginning Inventory columns must be numeric")
			This.SelectText(1, 100)
			Return 1
		End IF	
		IF Dec(data) < 0 Then
			iw_frame.SetMicroHelp("No Cattle Characteristic's Column can have negative Beginning Inventory")
			This.SelectText(1, 100)
			Return 1
		End IF	
End Choose
end event

event constructor;call super::constructor;iw_parentwindow = Parent
end event

event itemerror;call super::itemerror;Return 2
end event

type dw_plant from u_plant within w_cooler_actual
integer width = 1503
integer height = 90
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()

end event

type dw_dates from u_begin_end_dates within w_cooler_actual
integer x = 2121
integer width = 677
integer height = 202
integer taborder = 20
boolean bringtotop = true
end type

event constructor;call super::constructor;This.InsertRow(0)
end event

