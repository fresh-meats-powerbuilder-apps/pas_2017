﻿$PBExportHeader$w_hot_box_perpetual.srw
forward
global type w_hot_box_perpetual from w_base_sheet_ext
end type
type dw_hot_box from u_base_dw_ext within w_hot_box_perpetual
end type
type dw_plant from u_plant within w_hot_box_perpetual
end type
end forward

global type w_hot_box_perpetual from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 2962
integer height = 1572
long backcolor = 67108864
event ue_keydown pbm_dwnkey
dw_hot_box dw_hot_box
dw_plant dw_plant
end type
global w_hot_box_perpetual w_hot_box_perpetual

type variables
s_error			istr_error_info

datastore			ids_tree,&
			ids_desc

u_rmt001			iu_rmt001
u_pas203			iu_pas203
u_ws_pas4           iu_ws_pas4

Boolean			ib_updating,&
			ib_char_key

Long			il_char_count, &
			il_rec_count, &
			il_SelectedColor, &
			il_SelectedTextColor 



String			is_update_string,&  
			is_desc
Window			iw_temp
end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();Long					ll_rec_count_pas
				
String				ls_plant, &
						ls_plant_desc, & 
						ls_header, &
						ls_detail, &
						ls_temp

This.TriggerEvent('closequery') 

SetPointer(HourGlass!)

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
If Not ib_inquire Then
	Return False
End If

ls_plant = dw_plant.uf_get_plant_code()

ls_header = ls_plant + '~r~n' 

iw_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"

dw_hot_box.reset()

//If iu_rmt001.uf_rmtr06mr_inq_hot_box_perpetual(istr_error_info, & 
//										ls_header, &
//										ls_detail) = -1 Then
//										This.SetRedraw(True) 
//										Return False
//End If

If iu_ws_pas4.nf_rmtr06nr(istr_error_info, & 
										ls_header, &
										ls_detail) = -1 Then
										This.SetRedraw(True) 
										Return False
End If		


//ls_header = '2000~t'
//dw_hot_box.Object.beg_inv_1st_row_co.Expression=ls_header
//ls_detail = &
//'1999-05-10~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-11~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-12~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-13~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-14~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-15~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-16~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-17~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-18~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-19~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-20~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' + &
//'1999-05-21~t2000~t16~t410~t6560~t16~t410~t6400~t2120~t3000~r~n' 
//

//MessageBox("ls_detail",ls_detail)

ll_rec_count_pas = dw_hot_box.ImportString(ls_detail)

dw_hot_box.SetItem(1, "back_color", il_SelectedColor)
dw_hot_box.SetItem(1, "text_color", il_SelectedTextColor)

If ll_rec_count_pas > 0 Then 
	SetMicroHelp(String(ll_rec_count_pas) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True) 

dw_hot_box.ResetUpdate()
dw_hot_box.SetFocus()

Return True

end function

on w_hot_box_perpetual.create
int iCurrent
call super::create
this.dw_hot_box=create dw_hot_box
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_hot_box
this.Control[iCurrent+2]=this.dw_plant
end on

on w_hot_box_perpetual.destroy
call super::destroy
destroy(this.dw_hot_box)
destroy(this.dw_plant)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')


end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;This.Title = 'Hot Box Perpetual'

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
End choose

end event

event ue_postopen;call super::ue_postopen;Environment	le_env
u_sdkcalls				lu_sdk


//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "hotbox"
istr_error_info.se_user_id 		= sqlca.userid

// get the users color setup
lu_sdk = Create u_sdkcalls
GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = lu_sdk.nf_GetSysColor(13)
	il_SelectedTextColor = lu_sdk.nf_GetSysColor(14)
Else
 	il_SelectedColor = 255
	il_SelectedTextColor = 0
End if
Destroy(lu_sdk)

is_inquire_window_name = 'w_cattle_characteristics_inq'

iu_rmt001 = Create u_rmt001
iu_ws_pas4 = Create u_ws_pas4

wf_retrieve()

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
End Choose


end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288

integer li_x = 37
integer li_y = 190

//dw_hot_box.width	= newwidth - (30 + li_x)
//dw_hot_box.height	= newheight - (30 + li_y)

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

dw_hot_box.width	= newwidth - li_x
dw_hot_box.height	= newheight - li_y
end event

type dw_hot_box from u_base_dw_ext within w_hot_box_perpetual
event ue_dwndropdown pbm_dwndropdown
integer y = 100
integer width = 2880
integer height = 1280
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_hot_box_perpetual"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;//is_selection = '1'
end event

event itemchanged;call super::itemchanged;//DataWindowChild 		ldwc_child
//DataStore 				lds_Tmp
//Long 						ll_count,ll_row,ll_find
//String					ls_temp
//Decimal					ld_temp
//u_string_functions 	u_string
//
//CHOOSE CASE dwo.name
//	CASE 'desc'
//		If u_string.nf_countoccurrences(data,'~r~n') > 0 Then
//			iw_frame.SetMicroHelp("You can not enter a carrage return(Enter) in Long Description")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'short_desc'
//		If u_string.nf_countoccurrences(data,'~r~n') > 0 Then
//			iw_frame.SetMicroHelp("You can not enter a carrage return(Enter) in Short Description")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//		ll_find = dw_cattle.Find ( "short_desc = '" + data + "'", 1, dw_cattle.RowCount() )
//		If ll_find > 0 Then
//			iw_frame.SetMicroHelp("This Short Description has already been used -- please choose another")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'grade'
//		dw_cattle.GetChild("grade", ldwc_child)
//		ll_find = ldwc_child.Find ( "grade = '" + data + "'", 1, ldwc_child.RowCount() )
//		If ll_find = 0 then
//			iw_frame.SetMicroHelp("The Grade is not a valid grade")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'min_wght'
//		IF Not IsNumber(data) Then
//			iw_frame.SetMicroHelp("Min Weight Range must be numeric")
//			This.SelectText(1, 100)
//			Return 1
//		End IF	
//		ld_temp =  dw_cattle.GetItemDecimal(row,'max_wght')
//		If dec(data) > ld_temp and ld_temp > 0 Then
//			iw_frame.SetMicroHelp("Min Weight Range can not be greater then the Max Weight Range")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'max_wght'
//		IF Not IsNumber(data) Then
//			iw_frame.SetMicroHelp("Max Weight Range must be numeric")
//			This.SelectText(1, 100)
//			Return 1
//		End IF	
//		ld_temp =  dw_cattle.GetItemDecimal(row,'min_wght')
//		If dec(data) < ld_temp Then
//			iw_frame.SetMicroHelp("Max Weight Range can not be less then the Min Weight Range")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'min_grade'
//		ld_temp =  dw_cattle.GetItemDecimal(row,'max_grade')
//		If dec(data) > ld_temp and ld_temp > 0 Then
//			iw_frame.SetMicroHelp("Min Yield Grade can not be greater then the Max Yield Grade")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'max_grade'
//		ld_temp =  dw_cattle.GetItemDecimal(row,'min_grade')
//		If dec(data) < ld_temp Then
//			iw_frame.SetMicroHelp("Max Yield Grade can not be less then the Min Yield Grade")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'destination'
//		dw_cattle.GetChild("destination", ldwc_child)
//		ll_find = ldwc_child.Find ( "destination = '" + data + "'", 1, ldwc_child.RowCount() )
//		If ll_find = 0 then
//			iw_frame.SetMicroHelp("The Destination is not a valid destination")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'sex'
//		dw_cattle.GetChild("sex", ldwc_child)
//		ll_find = ldwc_child.Find ( "sex = '" + data + "'", 1, ldwc_child.RowCount() )
//		If ll_find = 0 then
//			iw_frame.SetMicroHelp("The Sex is not a valid sex")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//END CHOOSE
//
//iw_frame.SetMicroHelp("Ready")
Return 0
end event

event itemerror;call super::itemerror;//Return 2

end event

type dw_plant from u_plant within w_hot_box_perpetual
integer taborder = 20
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;disable()
end event

