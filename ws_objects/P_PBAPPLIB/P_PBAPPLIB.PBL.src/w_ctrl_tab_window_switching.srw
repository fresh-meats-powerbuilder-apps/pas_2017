﻿$PBExportHeader$w_ctrl_tab_window_switching.srw
forward
global type w_ctrl_tab_window_switching from window
end type
type sle_1 from statictext within w_ctrl_tab_window_switching
end type
type p_1 from picture within w_ctrl_tab_window_switching
end type
end forward

global type w_ctrl_tab_window_switching from window
integer x = 480
integer y = 640
integer width = 2016
integer height = 312
windowtype windowtype = response!
long backcolor = 12632256
event ue_keyup pbm_keyup
sle_1 sle_1
p_1 p_1
end type
global w_ctrl_tab_window_switching w_ctrl_tab_window_switching

type variables
Window	iw_ActiveSheet,&
			iw_LastSheet
			
String 	is_new_code			
end variables

forward prototypes
public function boolean wf_pooled_window (window w_window)
end prototypes

event ue_keyup;IF Not KeyDown(KeyControl!) Then
	IF IsValid (iw_ActiveSheet) Then
		iw_ActiveSheet.SetFocus()
		Close(This)
	end if
END IF

end event

public function boolean wf_pooled_window (window w_window);ClassDefinition 		lu_ClassDef
w_netwise_sheet_ole 	w_ole_sheet

if not IsValid(w_window) then
	return true	
end if

lu_ClassDef = w_window.classdefinition
do while IsValid(lu_ClassDef)
	if lu_ClassDef.name = "w_netwise_sheet_ole" then
		w_ole_sheet = iw_ActiveSheet
		if w_ole_sheet.ib_pooled then
			return true	
		end if
		
		return false
	end if
	
	lu_ClassDef = lu_ClassDef.Ancestor		
loop

return false
end function

event key;Window					lw_LastSheet

IF KeyDown( KeyControl!) And KeyDown( KeyTab!) THEN
	IF KeyDown( KeyShift!) THEN
		DO 
			iw_LastSheet = iw_ActiveSheet
			iw_ActiveSheet = gw_base_frame.GetFirstSheet()

			DO 
				IF NOT wf_pooled_window(iw_ActiveSheet) THEN
					lw_LastSheet = iw_ActiveSheet
				END IF
				iw_ActiveSheet = gw_base_Frame.GetNextSheet( iw_ActiveSheet)
			LOOP While IsValid( iw_ActiveSheet) AND iw_ActiveSheet<> iw_LastSheet
		
			iw_LastSheet = lw_LastSheet
			iw_ActiveSheet = lw_LastSheet
		LOOP UNTIL IsValid( iw_ActiveSheet) 

	ELSE
		DO While IsValid( iw_ActiveSheet)
			iw_ActiveSheet = gw_base_frame.GetNextSheet( iw_ActiveSheet)
			
			IF IsValid( iw_ActiveSheet) THEN				
				IF NOT wf_pooled_window(iw_ActiveSheet) THEN
					exit
				END IF
			ELSE
				iw_ActiveSheet = gw_base_frame.GetFirstSheet()
				IF IsValid( iw_ActiveSheet) THEN				
					IF NOT wf_pooled_window(iw_ActiveSheet) THEN
						exit
					END IF
				END IF
			END IF
		LOOP 
	END IF


	IF IsValid(Iw_ActiveSheet) THEN	
		sle_1.Text = Trim(iw_ActiveSheet.Title)
	ELSE
		Close( This)
	END IF
END IF
end event

event open;Window					lw_LastSheet

p_1.PictureName	=	Message.nf_Get_app_id() + '.BMP'

iw_activesheet = gw_base_frame.GetFirstSheet()


IF KeyDown(KeyShift!) THEN
	DO While IsValid( iw_ActiveSheet)
		IF NOT wf_pooled_window(iw_ActiveSheet) THEN
			lw_LastSheet = iw_ActiveSheet		
		END IF
		
		iw_ActiveSheet = gw_base_frame.GetNextSheet( iw_ActiveSheet)
	Loop 
	
	iw_ActiveSheet = lw_LastSheet
	
ELSE
	DO While IsValid( iw_ActiveSheet)
		iw_activesheet = gw_base_frame.GetNextSHeet( iw_ActiveSheet)
	
		IF IsValid( iw_ActiveSheet) THEN
			IF NOT wf_pooled_window(iw_ActiveSheet) THEN
				exit
			END IF
		END IF
	LOOP
END IF

IF IsValid ( Iw_ActiveSheet) and iw_activesheet <> gw_base_frame.GetFirstSheet() THEN	
	sle_1.Text = Trim(iw_ActiveSheet.Title)
ELSE
	Close( This)
END IF
end event

on w_ctrl_tab_window_switching.create
this.sle_1=create sle_1
this.p_1=create p_1
this.Control[]={this.sle_1,&
this.p_1}
end on

on w_ctrl_tab_window_switching.destroy
destroy(this.sle_1)
destroy(this.p_1)
end on

type sle_1 from statictext within w_ctrl_tab_window_switching
integer x = 320
integer y = 92
integer width = 1458
integer height = 100
integer textsize = -11
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
string text = "none"
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type p_1 from picture within w_ctrl_tab_window_switching
integer x = 151
integer y = 80
integer width = 146
integer height = 120
boolean focusrectangle = false
end type

