﻿$PBExportHeader$w_yield_tree_copy_popup.srw
forward
global type w_yield_tree_copy_popup from Window
end type
type dw_2 from u_product_code within w_yield_tree_copy_popup
end type
type cb_cancel from commandbutton within w_yield_tree_copy_popup
end type
type cb_ok from commandbutton within w_yield_tree_copy_popup
end type
type dw_1 from datawindow within w_yield_tree_copy_popup
end type
end forward

global type w_yield_tree_copy_popup from Window
int X=823
int Y=360
int Width=2171
int Height=616
boolean TitleBar=true
string Title="Compute Yields"
long BackColor=79741120
boolean ControlMenu=true
WindowType WindowType=response!
event ue_postopen ( )
dw_2 dw_2
cb_cancel cb_cancel
cb_ok cb_ok
dw_1 dw_1
end type
global w_yield_tree_copy_popup w_yield_tree_copy_popup

type variables
Boolean		ib_oktocompute

w_base_sheet	iw_parentwindow
end variables

event ue_postopen;DataWindowChild			ldwc_temp, &
								ldwc_fromchar, &
								ldwc_tochar

dw_1.InsertRow(0)
dw_2.InsertRow(0)

iw_parentwindow.Event ue_get_data('compute_product_code')
dw_2.uf_SetProductCode(Message.StringParm)

iw_parentwindow.Event ue_get_data('compute_product_desc')
dw_2.uf_SetProductDesc(Message.StringParm)

iw_parentwindow.Event ue_get_data('dwc_char')
ldwc_temp = message.PowerObjectParm
dw_1.GetChild('characteristic', ldwc_fromchar)
ldwc_temp.ShareData(ldwc_fromchar)
dw_1.GetChild('to_characteristic', ldwc_tochar)
ldwc_temp.ShareData(ldwc_tochar)


iw_parentwindow.Event ue_get_data('characteristics')
dw_1.SetItem(1, 'characteristic', Integer(Message.StringParm))
dw_1.SetItem(1, 'to_characteristic', Integer(Message.StringParm))

end event

on w_yield_tree_copy_popup.create
this.dw_2=create dw_2
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.dw_1=create dw_1
this.Control[]={this.dw_2,&
this.cb_cancel,&
this.cb_ok,&
this.dw_1}
end on

on w_yield_tree_copy_popup.destroy
destroy(this.dw_2)
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.dw_1)
end on

event open;iw_parentwindow = Message.PowerObjectParm

ib_OkToCompute = False

This.Event Post ue_postopen()
end event

event close;If Not ib_oktocompute Then
	CloseWithReturn(This, 'Cancel')
Else
	CloseWithReturn(This, '')
End If
end event

type dw_2 from u_product_code within w_yield_tree_copy_popup
int X=23
int Y=16
int Width=1577
int Height=100
int TabOrder=10
end type

event constructor;call super::constructor;This.uf_Disable()
end event

type cb_cancel from commandbutton within w_yield_tree_copy_popup
int X=1093
int Y=364
int Width=247
int Height=108
int TabOrder=30
string Text="Cancel"
boolean Cancel=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;Close(Parent)
end event

type cb_ok from commandbutton within w_yield_tree_copy_popup
int X=823
int Y=364
int Width=247
int Height=108
int TabOrder=20
string Text="&Ok"
boolean Default=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;iw_parentwindow.Event ue_set_data('ComputeFromChar', &
		String(dw_1.GetItemNumber(1, 'characteristic')))

ib_OkToCompute = True
Close(Parent)
end event

type dw_1 from datawindow within w_yield_tree_copy_popup
int X=9
int Y=168
int Width=2112
int Height=120
int TabOrder=10
string DataObject="d_yield_copy_popup"
boolean Border=false
boolean LiveScroll=true
end type

