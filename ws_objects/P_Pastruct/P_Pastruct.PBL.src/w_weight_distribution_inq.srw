﻿$PBExportHeader$w_weight_distribution_inq.srw
forward
global type w_weight_distribution_inq from w_base_response_ext
end type
type dw_begin_end_date from u_base_dw_ext within w_weight_distribution_inq
end type
type dw_plant from u_plant within w_weight_distribution_inq
end type
type dw_fab_product_code from u_fab_product_code within w_weight_distribution_inq
end type
end forward

global type w_weight_distribution_inq from w_base_response_ext
integer x = 87
integer y = 276
integer width = 1650
integer height = 716
string title = "Weight Distribution Inquiry"
long backcolor = 67108864
dw_begin_end_date dw_begin_end_date
dw_plant dw_plant
dw_fab_product_code dw_fab_product_code
end type
global w_weight_distribution_inq w_weight_distribution_inq

type variables
w_weight_distribution  iw_Parent_Window

datawindowchild         idwc_master, &
                                 idwc_slave

Integer			ii_PARange
end variables

event open;call super::open;iw_Parent_Window = Message.PowerObjectParm
If Not IsValid(iw_Parent_Window) Then
	Close(This)
	return
End if

This.Title = iw_Parent_Window.Title + " Inquire"

iw_frame.SetMicroHelp("Ready")


end event

event ue_postopen;call super::ue_postopen;String			ls_PARange, &
					ls_header_string,&
					ls_plant,&
					ls_product,&
					ls_product_desc,&
					ls_product_state, &
					ls_product_status, &
					ls_product_state_desc, &
					ls_product_status_desc, &
					ls_BeginDate

Date				ldt_temp

If Message.ReturnValue = -1 Then Close(This)

// get the number of valid days for PA
If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "FUTURE", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "Future PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
ii_PARange = Integer(ls_PARange)

iw_ParentWindow.TriggerEvent("ue_getdata", 0, "header")
ls_header_string = Message.StringParm

If NOT iw_frame.iu_string.nf_IsEmpty(ls_header_string) Then
	ls_plant						= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product					= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_desc			= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_state			= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_state_desc	= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_status			= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_status_desc	= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_BeginDate				= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
End If

if len(trim(ls_plant)) > 0 then 	dw_plant.uf_set_plant_code(ls_plant)
dw_fab_product_code.uf_importstring(ls_product, ls_product_desc, &
												ls_product_state, ls_product_state_desc, &
												ls_product_status, ls_product_status_desc, True)
			
ldt_temp = Date(ls_BeginDate)
If ldt_temp < Today() Then ldt_temp = Today()
dw_begin_end_date.SetItem( 1, "begin_date", ldt_temp)

dw_plant.event getfocus( )


end event

on w_weight_distribution_inq.create
int iCurrent
call super::create
this.dw_begin_end_date=create dw_begin_end_date
this.dw_plant=create dw_plant
this.dw_fab_product_code=create dw_fab_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_begin_end_date
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_fab_product_code
end on

on w_weight_distribution_inq.destroy
call super::destroy
destroy(this.dw_begin_end_date)
destroy(this.dw_plant)
destroy(this.dw_fab_product_code)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;String	ls_plant, &
			ls_product

date	ld_begin_date, &
		ld_end_date

IF (dw_plant.AcceptText() = -1) OR(dw_begin_end_date.AcceptText() = -1) THEN return

ls_plant = dw_plant.GetText()
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End if

If Not dw_fab_product_code.uf_validate( ) Then Return
ls_product = dw_fab_product_code.uf_exportstring( )

ld_begin_Date	= date(dw_begin_end_date.Gettext())
If iw_frame.iu_string.nf_IsEmpty(string(ld_begin_Date)) Then
	iw_frame.SetMicroHelp("Begin Date is a required field")
	dw_begin_end_date.SetFocus()
	return
End if
		
CloseWithReturn(This, ls_plant + "~t" + ls_product + "~t" + &
							 String(ld_begin_Date) + "~t" + String(ld_end_date))

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_weight_distribution_inq
integer x = 1312
integer y = 488
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_weight_distribution_inq
integer x = 1006
integer y = 488
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_weight_distribution_inq
integer x = 699
integer y = 488
integer taborder = 40
end type

type dw_begin_end_date from u_base_dw_ext within w_weight_distribution_inq
integer x = 73
integer y = 384
integer width = 681
integer height = 88
integer taborder = 30
string dataobject = "d_source_begin_end_date"
boolean border = false
boolean ib_updateable = true
end type

event itemchanged;call super::itemchanged;string		ls_gettext

ls_gettext = GetText()

CHOOSE CASE GetColumnName() 
	CASE "begin_date"
		IF Date(ls_GetText) < TODAY() THEN
			MessageBox( "Begin Date", "Begin Date must be greater than Today." )
			This.SetFocus()
			This.SelectText(1,Len(ls_GetText))
			Return 1
		END IF	
	CASE "end_date"
		IF Date(ls_GetText) < GetItemDate(1, "begin_date") THEN
			MessageBox( "End Date", "End Date must be greater then Begin Date." )
			This.SetFocus()
			This.SelectText(1,Len(ls_GetText))
			Return 1
		END IF	
END CHOOSE

If DaysAfter(Today(), Date(ls_gettext)) > ii_PARange Then
	MessageBox("Date", "Date must be within " + String(ii_PARange) + &
					" days of today.")
	Return 1
End if
end event

event itemerror;call super::itemerror;Return 1
end event

event constructor;call super::constructor;This.InsertRow(0)
Modify("begin_date.Protect='0' begin_date.Background.Color='16777215'")
dw_begin_end_date.SetTabOrder ( "begin_date", 30 )

end event

on getfocus;call u_base_dw_ext::getfocus;This.SelectText(1,Len(GetText()))
end on

type dw_plant from u_plant within w_weight_distribution_inq
integer x = 192
integer y = 8
integer width = 1495
integer taborder = 10
end type

type dw_fab_product_code from u_fab_product_code within w_weight_distribution_inq
integer y = 96
integer width = 1609
integer height = 288
integer taborder = 20
boolean bringtotop = true
end type

