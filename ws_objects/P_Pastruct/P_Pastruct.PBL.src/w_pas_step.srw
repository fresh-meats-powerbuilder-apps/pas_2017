﻿$PBExportHeader$w_pas_step.srw
forward
global type w_pas_step from w_base_sheet_ext
end type
type dw_step_header from u_base_dw_ext within w_pas_step
end type
type dw_step_detail from u_base_dw_ext within w_pas_step
end type
type cb_copystep from commandbutton within w_pas_step
end type
end forward

global type w_pas_step from w_base_sheet_ext
integer x = 0
integer y = 0
integer width = 2683
integer height = 1436
string title = "Manufacturing Steps"
long backcolor = 67108864
event ue_getdata pbm_custom01
dw_step_header dw_step_header
dw_step_detail dw_step_detail
cb_copystep cb_copystep
end type
global w_pas_step w_pas_step

type variables
Private:
Int			ii_ShiftDuration
String		is_last_step_type
u_pas201		iu_pas201
s_error		istr_error_info
u_ws_pas1	iu_ws_pas1

datawindowchild 			idddw_child_state, &
								idddw_child_status
								
//nvuo_fab_product_code	invuo_fab_product_code

u_sect_functions		iu_sect_functions

end variables

forward prototypes
public subroutine wf_infomessage (string as_message)
public subroutine wf_infomessage (string as_message, string as_edit_name)
public function boolean wf_update ()
public function string wf_default_state ()
public function boolean wf_update_header (ref string as_header_string)
public function boolean wf_select_tpasmfgs (string as_fab_product_code, string as_product_state, string as_product_status)
public function string wf_default_status ()
public function long wf_find_duplicate (string as_product_code, string as_product_state, string as_product_status, long al_row)
public function boolean wf_deleterow ()
public subroutine wf_delete ()
public function boolean wf_addrow ()
public function boolean wf_update_detail (ref string as_detail_string)
public function boolean wf_retrieve ()
public subroutine wf_filenew ()
public function boolean wf_freezing_step ()
end prototypes

event ue_getdata;call super::ue_getdata;DataWindowChild	ldwc_steps


Choose Case Lower(String(Message.LongParm, "address"))
Case "header"
	Message.StringParm = dw_step_header.Object.DataWindow.Data
Case "steplist"
	dw_step_header.GetChild('mfg_step_sequence', ldwc_steps)
	Message.StringParm = ldwc_steps.Describe("DataWindow.Data")
End Choose
end event

public subroutine wf_infomessage (string as_message);iw_frame.SetMicroHelp(as_message)

end subroutine

public subroutine wf_infomessage (string as_message, string as_edit_name);//iw_frame.SetMicroHelp(as_message)

wf_infomessage(as_message)

//IBDKEEM DEBUG for edit checks.
//messagebox(as_edit_name,as_message)

end subroutine

public function boolean wf_update ();Boolean				lb_ret

DataWindowChild	dwc_dddw

Int					li_counter, &
						li_new_step_sequence

Long					ll_row

String				as_header_string, &
						as_detail_string


// make sure the step is ready to be saved
If dw_step_header.AcceptText() = -1 then return false
If dw_step_detail.AcceptText() = -1 then return false

If Not wf_update_header(as_header_string) Then Return False

If Not wf_update_detail(as_detail_string) Then
	Return False
End IF

SetPointer(HourGlass!)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "nf_pasp06br"
istr_error_info.se_message = Space(70)

//lb_ret = iu_pas201.nf_pasp06br(istr_error_info, &
//										as_header_string, &
//										as_detail_string, &
//										li_new_step_sequence)	

lb_ret = iu_ws_pas1.nf_pasp06fr(istr_error_info, &
										as_header_string, &
										as_detail_string, &
										li_new_step_sequence)	


If lb_ret Then 
	This.SetRedraw(False)
	dw_step_header.GetChild("mfg_step_sequence", dwc_dddw)

	If dw_step_header.GetItemStatus(1, 0, Primary!) = NewModified! Then
		SetMicroHelp("Add Successful")
		ll_row = dwc_dddw.InsertRow(0)	
		dwc_dddw.SetItem(ll_row, "mfg_step_sequence", li_new_step_sequence)
		dwc_dddw.SetItem(ll_row, "mfg_step_descr", &
								dw_step_header.GetItemString(1, "mfg_step_description"))
	Else
		SetMicroHelp("Modifications Successful")
		dwc_dddw.SetItem(dwc_dddw.Find('mfg_step_sequence = ' + &
					String(dw_Step_header.GetItemNumber(1, "mfg_step_sequence")), &
					1, dwc_dddw.RowCount()), "mfg_step_descr", &
					dw_step_header.GetItemString(1, "mfg_Step_description"))
	End if
	dw_step_header.SetItem(1, "mfg_step_sequence", li_new_step_sequence)

	dw_step_header.ResetUpdate()
	dw_step_detail.ResetUpdate()
	dw_step_header.SetFocus()
	
	
//	If dw_step_header.GetColumnName() = 'fab_product_code' Then
	If dw_step_header.GetColumnName() = 'mfg_step_description' Then
		dw_step_header.SetColumn('mfg_step_description')
	End If
	This.SetRedraw(True)

End if

return true
end function

public function string wf_default_state ();return '1'

end function

public function boolean wf_update_header (ref string as_header_string);DataWindowChild		dwc_dddw

Boolean					lb_need_new_seq

Int						li_counter, &
							li_temp, &
							li_mfg_step_sequence

Long						ll_row_count, &
							ll_row, &
							ll_new_row

String					ls_temp, &
							ls_header_string, &
							ls_mfg_step_type, &
							ls_fab_product_code, &
							ls_product_state, &
							ls_product_status, &
							ls_comment


ll_row_count = dw_step_detail.RowCount()
ls_temp = dw_step_header.GetItemString(1, "fab_product_code")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	wf_infomessage("Product Code is a required field", "wf_update_header")
	dw_step_header.SetColumn("fab_product_code")
	dw_step_header.SetFocus()
	Return False
End if

ls_fab_product_code = trim(ls_temp)
ls_header_string = ls_temp + '~t'

ls_temp = dw_step_header.GetItemString(1, "product_state")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	wf_infomessage("Product State is a required field", "wf_update_header")
	dw_step_header.SetColumn("product_state")
	dw_step_header.SetFocus()
	Return False
End if
ls_product_state = trim(ls_temp)
ls_header_string += ls_temp + '~t'

ls_temp = dw_step_header.GetItemString(1, "product_status")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	wf_infomessage("Product Status is a required field", "wf_update_header")
	dw_step_header.SetColumn("product_status")
	dw_step_header.SetFocus()
	Return False
End if

ls_product_status = trim(ls_temp)
ls_header_string += ls_temp + '~t'

ls_mfg_step_type = Trim(dw_step_header.GetItemString(1, 'mfg_step_type'))
If iw_frame.iu_string.nf_isempty(ls_mfg_step_type) Then
	wf_infomessage("Step Type is a required field", "wf_update_header")
	dw_step_header.SetColumn("mfg_step_type")
	dw_step_header.SetFocus()
	Return False
End If

For ll_row = 1 to dw_step_detail.RowCount()
	If Trim(dw_step_detail.GetItemString(ll_row, 'fab_product_code')) = ls_fab_product_code &
	 and Trim(dw_step_detail.GetItemString(ll_row, 'product_state')) = ls_product_state &
	 and Trim(dw_step_detail.GetItemString(ll_row, 'product_status')) = ls_product_status Then
		dw_step_detail.setrow(ll_row)
		wf_infomessage(ls_fab_product_code + " is an output product.  This will cause a cycle.", "dw_step_header.itemchanged")
		Return False
	End If
Next
		
Choose Case ls_mfg_step_type
Case 'T'
	// Transfer Step
	If ll_row_count <> 0 Then
		wf_infomessage("Transfer Steps cannot have output products", "wf_update_header")
		Return False
	End if	
	
	If dw_step_header.GetItemNumber(1, 'shift_duration') <> 0 Then
		wf_infomessage("Shift Duration must be 0 for Transfer Steps", "wf_update_header")
		dw_step_header.SetColumn( 'shift_duration')
		dw_step_header.SetFocus()
		return False
	End if
Case 'F'
	// Freezing Step
	If ll_row_count <> 1 Then
		wf_infomessage("Freezing Steps can only have one output product which must be " +	&
									Trim(dw_step_header.GetItemString(1, "fab_product_code")) + " and State 2", "wf_update_header")
		If ll_row_count = 0 Then 
			This.SetRedraw(False)
			ll_new_row = dw_step_detail.InsertRow(0)
			dw_step_detail.SetRow(ll_new_row)
			dw_step_detail.SetColumn("fab_product_code")
			This.SetRedraw(True)
			dw_step_detail.SetFocus()
		Else
			This.SetRedraw(False)
			dw_step_detail.SetRow(0)
			dw_step_detail.SetColumn("fab_product_code")
			This.SetRedraw(True)
			dw_step_detail.SetFocus()
		End IF	
		Return False
	End if

//	If Trim(dw_step_header.GetItemString(1, "fab_product_code")) = Trim(dw_step_detail.GetItemString(1, 'fab_product_code')) AND &
//		dw_step_header.GetItemString(1, "product_state") = dw_step_detail.GetItemString(1, 'product_state') Then
//		wf_infomessage(ls_fab_product_code + " is an output product.  This will cause a cycle.", "dw_step_header.itemchanged")
//		This.SetRedraw(False)
//		dw_step_detail.SetRow(0)
//		dw_step_detail.SetColumn("fab_product_code")
//		This.SetRedraw(True)
//		dw_step_detail.SetFocus()
//		Return False
//	End if


	If Trim(dw_step_detail.GetItemString(1, 'fab_product_code')) <> &
		Trim(dw_step_header.GetItemString(1, "fab_product_code")) or &
		dw_step_header.GetItemString(1, "product_state") <> '1' or &
		dw_step_detail.GetItemString(1, 'product_state') <> '2' Then
		
		wf_infomessage("Freezing Steps can only have one output product which must be " +	&
									Trim(dw_step_header.GetItemString(1, "fab_product_code")) + " and State 2", "wf_update_header")
		This.SetRedraw(False)
		dw_step_detail.SetRow(0)
		dw_step_detail.SetColumn("fab_product_code")
		This.SetRedraw(True)
		dw_step_detail.SetFocus()
		Return False
	End if
		
Case 'W'
	// Weight Step
	For li_counter = 1 to ll_row_count
		If dw_step_detail.GetItemString(li_counter, "fab_type") <> "WT" Then
			wf_infomessage("All output products for a weight step must be of type Weight", "wf_update_header")
			Return False
		End if
	Next
	
Case Else   // 'S', 'L', 'C', 'P' and any other defined in tutltype
	// Standard or Labeling Step
	li_temp = dw_step_detail.Find("Len(fab_product_code) > 0", 1, ll_row_count)
	If li_temp = 0 Then
		wf_infomessage("Steps must have at least one output product", "wf_update_header")
		If ll_row_count = 0 Then 
			This.SetRedraw(False)
			dw_step_detail.SetFocus()
			ll_new_row = dw_step_detail.InsertRow(0)
			dw_step_detail.SetRow(ll_new_row)
			dw_step_detail.SetColumn("fab_product_code")
			This.SetRedraw(True)
		Else
			This.SetRedraw(False)
			dw_step_detail.SetFocus()
			dw_step_detail.SetRow(0)
			dw_step_detail.SetColumn("fab_product_code")
			This.SetRedraw(True)
		End IF	
		Return False
	End if
End Choose

li_mfg_step_sequence = dw_step_header.GetItemNumber(1, "mfg_step_sequence")
If IsNull(li_mfg_step_sequence) Or li_mfg_step_sequence < 0 Then	
	If Len(Trim(dw_step_header.GetItemString(1, "mfg_step_description"))) = 0 Then
		wf_infomessage("Step Description is a required field", "wf_update_header")
		dw_Step_header.SetColumn("mfg_step_description")
		dw_step_header.SetFocus()
		Return False
	End if
	li_mfg_step_sequence = 0
	lb_need_new_seq = True
End if
ls_header_string += String(li_mfg_step_sequence) + '~t'

ls_temp = dw_step_header.GetItemString(1, "mfg_step_description")
dw_step_header.GetChild("mfg_step_sequence", dwc_dddw)
li_temp = dwc_dddw.Find("Trim(mfg_step_descr) = '" + Trim(ls_temp) + "'", 1, dwc_dddw.RowCount())
If li_temp > 0 And dw_step_header.GetItemStatus &
		(1, "mfg_step_description", Primary!) <> NotModified! Then
	wf_infomessage(Trim(ls_temp) + " is already a step description.", "wf_update_header")
	Return False
End if
If iw_frame.iu_string.nf_IsEmpty(ls_temp) then
	wf_infomessage("Step Description is a required field", "wf_update_header")
	dw_step_header.SetColumn("mfg_step_description")
	dw_step_header.SetFocus()
	Return False
End if

ls_comment = iu_sect_functions.uf_condense(dw_step_header.GetItemString(1,'mfg_step_description'), ls_comment)
dw_step_header.SetItem(1, 'mfg_step_description', ls_comment)
ls_temp = ls_comment

ls_header_string += ls_temp + '~t'

li_temp = dw_step_header.GetItemNumber(1, "shift_duration")
If IsNull(li_temp) Then 
	wf_infomessage("Step Duration is a required field", "wf_update_header")
	dw_step_header.SetColumn("shift_duration")
	dw_step_header.SetFocus()
	Return False
End if
ls_header_string += String(li_temp) + '~t'
// already validated step type above
ls_header_string += ls_mfg_step_type + '~t'

Choose Case dw_step_header.GetItemStatus(1, 0, Primary!)
Case NotModified!, New!
	// should never be new! and get to this point, but just in case...
	ls_header_string += ' ~r~n'
	
Case NewModified!
	ls_header_string += 'A~r~n'

Case DataModified!
	ls_header_string += 'M~r~n'

End Choose

as_header_string = ls_header_string

Return (True)
end function

public function boolean wf_select_tpasmfgs (string as_fab_product_code, string as_product_state, string as_product_status);DataWindowChild	dwc_dddw

Boolean	lb_ret

Char	lc_fab_product_descr[30], &
		lc_fab_group[2]

String	ls_mfg_step_string, &
			ls_header_string, &
			ls_product_status,&
			ls_fab_product_descr,&
			ls_fab_group
			

SetPointer(HourGlass!)
//lc_fab_product_code = Trim(as_Fab_product_code)
//lc_product_state = Trim(as_product_state)

ls_header_string = as_fab_product_code + '~t' + as_product_state + '~t' + as_product_status + '~t'
		
istr_error_info.se_event_name = "ItemChanged"
istr_error_info.se_procedure_name = "nf_pasp04ar"
istr_error_info.se_message = Space(70)

ls_fab_product_descr = lc_fab_product_descr
ls_fab_group = lc_fab_group

//lb_ret = iu_pas201.nf_pasp04br(istr_error_info, &
//										ls_header_string, &
//										lc_fab_product_descr, &
//										lc_fab_group, &
//										ls_mfg_step_string)
lb_ret = iu_ws_pas1.nf_pasp04fr(istr_error_info, &
										ls_header_string, &
										ls_fab_product_descr, &
										ls_fab_group, &
										ls_mfg_step_string)
										
lc_fab_product_descr = ls_fab_product_descr
lc_fab_group = ls_fab_group

If Not lb_ret Then 
	return false
End if

dw_step_header.SetItem(1, "fab_product_descr", lc_fab_product_descr)
dw_step_header.SetItem(1, "fab_group", lc_fab_group)
dw_step_header.GetChild("mfg_step_sequence", dwc_dddw)
dwc_dddw.Reset()
dwc_dddw.ImportString(ls_mfg_step_string)

return true
end function

public function string wf_default_status ();return 'G'

end function

public function long wf_find_duplicate (string as_product_code, string as_product_state, string as_product_status, long al_row);Long		ll_result, &
			ll_row_count


ll_row_count = dw_step_detail.RowCount()
If al_row < 1 or al_row > ll_row_count Then return 0

ll_result = dw_step_detail.Find("Trim(fab_product_code) = Trim('" + as_product_code + "') AND " + &
											"product_state = '" + as_product_state + "' AND " + &
											"product_status = '" + as_product_status + "'" , &
											1, al_row - 1)
											
If ll_result <> 0 Then 
	return ll_result
ElseIf al_row = ll_row_count Then 
	return 0
Else
	return dw_step_detail.Find("Trim(fab_product_code) = Trim('" + as_product_code + "') AND " + &
										"product_state = '" + as_product_state + "' AND " + &
										"product_status = '" + as_product_status + "'" , &
										al_row + 1, ll_row_count)
End If


end function

public function boolean wf_deleterow ();Long		ll_row, &
			ll_rowcount, ll_rtn

String	ls_header_product, &
			ls_header_state, &
			ls_header_status, &
			ls_detail_product, &
			ls_detail_state, &
			ls_detail_status, &
			ls_detail_product_descr

DataWindow		ldw_focus

GraphicObject	lg_control


ls_header_product = Trim(dw_step_header.GetItemString(1, "fab_product_code"))
ls_header_state = Trim(dw_step_header.GetItemString(1, "product_state"))
ls_header_status = Trim(dw_step_header.GetItemString(1, "product_status"))

ll_row = dw_step_detail.GetRow()
ls_detail_product = Trim(dw_step_detail.GetItemString (ll_row, "fab_product_code"))
ls_detail_state = Trim(dw_step_detail.GetItemString(ll_row, "product_state"))
ls_detail_status = Trim(dw_step_detail.GetItemString(ll_row, "product_status"))
ls_detail_product_descr = Trim(dw_step_detail.GetItemString (ll_row, "fab_product_descr"))

//IBDKEEM ** 09/09/2002 ** Product State ** BUS RULES
// If Freeze Step, The only valid output is the frozen product. Must have only one output
If Trim(dw_step_header.GetItemString(1, "mfg_step_type")) = 'F' &
 		And dw_step_detail.RowCount() > 0 Then
	
	If ls_header_product = ls_detail_product &
  	 		AND '1' = ls_header_state AND '2' = ls_detail_state Then 
		wf_infomessage( ls_header_product + ' State 2 cannot be deleted', "wf_deleterow")
		return false
	End if
End if

If dw_step_detail.GetItemStatus(dw_step_detail.GetRow(), 0, Primary!) <> New! Then
	If MessageBox("Delete Row", "Are you sure you want to delete " + &
			ls_detail_product + " " + ls_detail_product_descr + "?", &
			Question!, YesNo!, 2) = 1 Then
		dw_step_detail.DeleteRow(0)
	End if
Else
	dw_step_detail.DeleteRow(0)
End if

ll_rowcount = dw_step_detail.RowCount()
If ll_row > ll_rowcount Then ll_row = ll_rowcount
ll_rtn = dw_step_detail.SetRow(ll_row)
If ll_row < ll_rowcount then dw_step_detail.TriggerEvent(rowfocuschanged!)
dw_step_detail.SetFocus()

return true


end function

public subroutine wf_delete ();Int	li_temp

String	ls_header_string, &
			ls_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_step_description, &
			ls_temp

ls_product_code = Trim(dw_step_header.GetItemString(1, "fab_product_code"))
If iw_frame.iu_string.nf_IsEmpty(ls_product_code) Then 
	wf_infomessage("Product Code is required for Delete", "wf_delete")
	return
End if

ls_product_state = Trim(dw_step_header.GetItemString(1, "product_state"))
If iw_frame.iu_string.nf_IsEmpty(ls_product_state) Then 
	wf_infomessage("Product State is required for Delete", "wf_delete")
	return
End if

ls_product_status = Trim(dw_step_header.GetItemString(1, "product_status"))
If iw_frame.iu_string.nf_IsEmpty(ls_product_status) Then 
	wf_infomessage("Product Status is required for Delete", "wf_delete")
	return
End if

ls_step_description = Trim(dw_step_header.GetItemString(1, "mfg_step_description"))
If iw_frame.iu_string.nf_IsEmpty(ls_step_description) Then 
	wf_infomessage("Step Description is required for Delete", "wf_delete")
	return
End if

If MessageBox(ls_product_code + " : " + ls_step_description, &
					"Are you sure you want to delete Step " + ls_step_description + &
					" from Product " + ls_product_code + &
					" State " + ls_product_state + & 
					" Status " + ls_product_status + & 
					"?", Question!, YesNo!, 2) = 2 Then return

If dw_step_header.GetItemStatus(1, 0, Primary!) = NewModified! Then 
	This.SetRedraw(False)
	dw_step_header.Reset()
	dw_step_header.InsertRow(0)
	dw_step_detail.Reset()
	dw_step_detail.InsertRow(0)
	
	dw_step_header.SetFocus()
	dw_step_header.SetColumn("fab_product_code")
	
	This.SetRedraw(True)
	
	return
End if

li_temp = dw_step_header.GetItemNumber(1, "mfg_step_sequence")
If li_temp < 0 or IsNull(li_temp) Then 
	wf_infomessage("Trouble getting Step Sequence, Please try Delete Again", "wf_delete")
	return
End if

ls_header_string = ls_product_code + '~t' +  ls_product_state + '~t' +  ls_product_status + '~t' + String(li_temp) + '~t~t~t~tD~r~n'

istr_error_info.se_event_name = "wf_delete"
istr_error_info.se_procedure_name = "nf_pasp06br"
istr_error_info.se_message = Space(70)
//
//If iu_pas201.nf_pasp06br(istr_error_info, &
//										ls_header_string, &
//										"", &
//										li_temp)	Then

If iu_ws_pas1.nf_pasp06fr(istr_error_info, &
										ls_header_string, &
										"", &
										li_temp)	Then

	wf_infomessage("Delete Successful")
	This.PostEvent("ue_ClearWindow")
End if

return
end subroutine

public function boolean wf_addrow ();Long			ll_row

If dw_step_detail.accepttext( ) <> 1 then return false

Choose Case Trim(dw_step_header.GetItemString(1, "mfg_step_type"))
Case 'F'
	If dw_step_detail.RowCount() > 0 Then 
		wf_infomessage("Freezing Steps can only have one output product", "wf_addrow")
		return False
	End if

Case 'T'
	wf_infomessage("Transfer Steps cannot have output products", "wf_addrow")
	return false
End Choose
This.SetRedraw(False)

// Super::wf_addrow()
ll_row = dw_step_detail.InsertRow(0)
dw_step_detail.ScrollToRow(ll_row)
dw_step_detail.PostEvent("ue_set_detail_focus")
dw_step_detail.setitem(ll_row,"product_state", wf_default_state())
dw_step_detail.setitem(ll_row,"product_status", wf_default_status())

return true


end function

public function boolean wf_update_detail (ref string as_detail_string);DWItemStatus		lis_Status

Integer				li_counter

Long					ll_row_count, &
						ll_keyprod, &
						ll_deleted_count, &
						ll_modified_row

String				ls_detail_string, &
						ls_ind

IF dw_step_header.GetItemString(1,"mfg_Step_type") <> "T" Then
	ll_row_count = dw_step_detail.RowCount()
	ll_keyprod = dw_step_detail.Find("key_product = 'Y'", &
			1, ll_row_count)
	If ll_keyprod < ll_row_count Then
		ll_keyprod = dw_step_detail.Find("key_product = 'Y'", &
				ll_keyprod + 1, ll_row_count)
		If ll_keyprod > 0 Then
			dw_step_detail.ScrollToRow(1)
			dw_step_detail.SetColumn("key_product")			
			dw_step_detail.SetFocus()
			wf_infomessage("There must be only one Key Product","wf_update_detail")
			Return False
		End If
	End If
End If

ls_detail_string = ""

ll_deleted_Count = dw_step_detail.DeletedCount()
for li_counter = 1 to ll_deleted_count
	ls_detail_string += Trim(dw_step_detail.GetItemString(li_counter, &
								"fab_product_code", Delete!, False)) + "~t" + &
								Trim(dw_step_detail.GetItemString(li_counter, &
								"product_state", Delete!, False)) + "~t" + &
								Trim(dw_step_detail.GetItemString(li_counter, &
								"product_status", Delete!, False)) + "~t" + &
								dw_step_detail.GetItemString( li_counter, &
								"key_product", Delete!, False) + "~tD~r~n"
Next

ll_modified_row = dw_step_detail.GetNextModified(0, Primary!)

Do While ll_modified_row > 0
	If Not iw_frame.iu_string.nf_IsEmpty(dw_step_detail.GetItemString(ll_modified_row,"fab_product_code")) Then 
		If iw_frame.iu_string.nf_isempty(dw_step_detail.GetItemString( ll_modified_row, "product_state")) then 
				dw_step_detail.setrow( ll_modified_row)
				dw_step_detail.setcolumn("product_state") 
				dw_step_detail.SetFocus()
				wf_infomessage("Product State is a required field","wf_update_detail")
				return false
		End If	
		
		If iw_frame.iu_string.nf_isempty(dw_step_detail.GetItemString( ll_modified_row, "product_status")) then 
				dw_step_detail.setrow( ll_modified_row)
				dw_step_detail.setcolumn("product_status") 
				dw_step_detail.SetFocus()
				wf_infomessage("Product Status is a required field","wf_update_detail")
				return false
		End If	
	
		lis_status = dw_step_detail.GetItemStatus(ll_modified_row, 0, Primary!)
		CHOOSE CASE lis_status
			CASE NewModified!
				ls_ind = 'A'
			CASE DataModified!
				ls_ind = 'M'
			CASE ELSE
				CONTINUE
		END CHOOSE
		ls_detail_string += Trim(dw_step_detail.GetItemString( &
										ll_modified_row, "fab_product_code")) + "~t" + &
									Trim(dw_step_detail.GetItemString( &
										ll_modified_row, "product_state")) + "~t" + &
									Trim(dw_step_detail.GetItemString( &
										ll_modified_row, "product_status")) + "~t" + &										
									dw_step_detail.GetItemString( &
										ll_modified_row, "key_product") + "~t"+ ls_ind + "~r~n"
	End If
	ll_modified_row = dw_step_detail.GetNextModified(ll_modified_row, Primary!)
Loop

as_detail_string = ls_detail_string

Return(True)
end function

public function boolean wf_retrieve ();Boolean	lb_ret

Char	lc_step_type

DataWindowChild	ldwc_steps

Int	li_mfg_step_sequence, &
		li_shift_duration, &
		li_counter, &
		li_temp

Long	ll_row_count

String	ls_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_output_string, &
			ls_ReturnString, &
			ls_Step, &
			ls_header_string,ls_step_type
			

Call w_base_sheet::closequery

If Message.ReturnValue = 1 Then return false

OpenWithParm(w_pas_product_step_inq, This)
ls_ReturnString = Message.StringParm 

SetPointer(Hourglass!)

If iw_frame.iu_string.nf_IsEmpty(ls_ReturnString) Then return False

iw_frame.iu_string.nf_ParseLeftRight(ls_ReturnString, '~r~n', ls_ReturnString, ls_Step)

dw_step_header.GetChild('mfg_step_sequence', ldwc_steps)
ldwc_steps.Reset()
ldwc_steps.ImportString(ls_step)

dw_step_header.Reset()
dw_step_header.ImportString(ls_ReturnString)

li_temp = ldwc_steps.Find('mfg_step_sequence = ' + &
				String(dw_step_header.GetItemNumber(1, "mfg_step_sequence")), &
				1, 1000)
If li_temp > 0 Then
	dw_step_header.SetItem(1, "mfg_step_description", &
				ldwc_steps.GetItemString(li_temp, "mfg_step_descr"))
Else
	MessageBox("Step Description", "There was a problem getting the " + &
					"Step Description, Please Contact Applications")				
	return False
End if

dw_step_header.uf_ChangeRowStatus(1, NotModified!)

ls_product_code = dw_step_header.GetItemString(1, "fab_product_code")
ls_product_state = dw_step_header.GetItemString(1, "product_state")
ls_product_status = dw_step_header.GetItemString(1, "product_status")

If iw_frame.iu_string.nf_IsEmpty(ls_product_code) Then return False
If iw_frame.iu_string.nf_IsEmpty(ls_product_state) Then return False

li_mfg_step_sequence = dw_step_header.GetItemNumber(1, "mfg_step_sequence")

wf_infomessage("Wait.. Inquiring Database")
istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp05ar"
istr_error_info.se_message = Space(70)

ls_header_string = ls_product_code + '~t' + ls_product_state + '~t' + &
						+ ls_product_status + '~t' + string(li_mfg_step_sequence) + '~t'

ls_step_type = lc_step_type
//lb_ret = iu_pas201.nf_pasp05br(istr_error_info, &
//										ls_header_string, &
//										li_shift_duration, &
//										lc_step_type, &
//										ls_output_string)
lb_ret = iu_ws_pas1.nf_pasp05fr(istr_error_info, &
										ls_header_string, &
										li_shift_duration, &
										ls_step_type, &
										ls_output_string)


lc_step_type = ls_step_type

If Not lb_ret then 
	return false
End if

This.SetRedraw(False)

dw_step_detail.Reset()
dw_step_detail.ImportString(ls_output_string)

dw_step_header.Modify("product_status.Enabled='No'")
dw_step_header.SetItem(1, "shift_duration", li_shift_duration)
dw_step_header.SetItem(1, "mfg_step_type", lc_step_type)
is_last_step_type = dw_step_header.GetItemString(1, "mfg_step_type")

dw_step_detail.ResetUpdate()
dw_step_detail.SelectRow(0, False)

dw_step_header.ResetUpdate()
dw_Step_header.SetColumn("mfg_step_description")
dw_step_header.SetFocus()
wf_infomessage("Inquire Successful")

This.SetRedraw(True)


return true
end function

public subroutine wf_filenew ();DataWindowChild	ldwc_steps

Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return

This.SetRedraw(False)
dw_step_header.Reset()
dw_step_header.InsertRow(0)
dw_step_detail.Reset()
dw_step_detail.InsertRow(0)
dw_step_detail.setitem(1,"product_state", wf_default_state())
dw_step_detail.setitem(1,"product_status", wf_default_status())

// dw_step_detail.SetFocus()

dw_step_header.SetFocus()
dw_step_header.SetColumn("fab_product_code")
dw_step_header.SetItem(1,"product_state","1")
dw_step_header.SetItem(1,"product_state_descr","FRESH")
dw_step_header.SetItem(1,"product_status","G")
dw_step_header.SetItem(1,"product_status_descr","GOOD")

dw_step_header.GetChild('mfg_step_sequence', ldwc_steps)
ldwc_steps.Reset()
	
This.SetRedraw(True)

end subroutine

public function boolean wf_freezing_step ();// This function will validate the existance of the freezing 
// product of the input product.  If that product does not exist,
// it will add it.  This will also add the row to the output dw

Char	lc_header_product[10], &
		lc_dummy, &
		lc_product_status  // lc_product status is product status (active/inactive from tskuprod
		                   // not the product status from dw_step_header

Int	li_tab, &
		li_4thtab, &
		li_5thtab

Long	ll_row

String	ls_temp, &
			ls_fab, &
			ls_update_fab, &
			ls_fab_product_descr, &
			ls_fab_type, &
			ls_pieces_per_box,&
			ls_header_product,ls_dummy,ls_product_status
			
ls_temp = dw_step_header.GetItemString(1, "fab_product_code")
If iw_frame.iu_string.nf_isempty(ls_temp) Then 
	This.SetRedraw(True)
	return false
End If

lc_header_product = Trim(ls_temp)

istr_error_info.se_event_name = "wf_freezing_step, Call 1"
istr_error_info.se_procedure_name = "nf_pasp00br"
istr_error_info.se_message = Space(70)

//If iu_pas201.nf_pasp00br(istr_error_info, &
//							lc_header_product, &
//							'2',&
//							lc_dummy, &
//							lc_product_status, &
//							ls_fab)  <> 0 Then
	
//ls_header_product = lc_header_product[10]
ls_header_product = ls_temp
ls_dummy = lc_dummy
ls_product_status = lc_product_status
If iu_ws_pas1.nf_pasp00fr(istr_error_info, &
							ls_header_product, &
							'2',&
							ls_dummy, &
							ls_product_status, &
							ls_fab)  <> 0 Then	
							
	
	lc_header_product[10] = ls_header_product
lc_dummy = ls_dummy
lc_product_status = ls_product_status
	
	
	// freezing product does not exist or error occured
	If Trim(istr_error_info.se_return_code) <> '100' Then 
		This.SetRedraw(True)
		return False
	End if
	
	//TODO SET MOUSE TO HOURGLASS
	wf_infomessage("Inserting Product " + lc_header_product + " State 2", "wf_freezing_step")
	ls_dummy = lc_dummy
	ls_product_status = lc_product_status
	// get fab record for input product
	istr_error_info.se_event_name = "wf_freezing_step, Call 2"
//	If iu_pas201.nf_pasp00br(istr_error_info, &
//								lc_header_product, &
//								'1',&
//								lc_dummy, &
//								lc_product_status, &
//								ls_fab) <> 0 Then 
	If iu_ws_pas1.nf_pasp00fr(istr_error_info, &
								lc_header_product, &
								'1',&
								ls_dummy, &
								ls_product_status, &
								ls_fab) <> 0 Then 
		This.SetRedraw(True)
		Return false
	End if
	lc_dummy = ls_dummy
	lc_product_status = ls_product_status
	li_tab = Pos(ls_fab, "~r~n", 1)
	// for some reason, the CR/LF might not be there
	If li_tab = 0 then li_tab = Len(ls_fab) + 1
	ls_fab = Left(ls_fab, li_tab - 1)
	
/*	
PASFAB-FAB-PRODUCT-CODE   1
PASFAB-FAB-PRODUCT-DESCR  2
PASFAB-PRODUCT-STATE      3
PASFAB-FAB-TYPE           4
PASFAB-FAB-GROUP          5
PASFAB-YIELD-GROUP        6
PASFAB-FAB-UOM            7
PASFAB-SKU-PRODUCT-CODE   8
IN-CHAR-KEY               9
IN-PIECES-PER-BOX         10
IN-REV-PIECES-PER-BOX     11
PASFAB-REV-PIECE-EFF-DATE 12
IN-DBFUNC.               
*/
	ls_fab_product_descr = iw_frame.iu_string.nf_gettoken(ls_fab, '~t') 	//1
	ls_update_fab = Trim(lc_header_product) + "~t" + ls_fab_product_descr + "~t2~t" + ls_fab + "~tA~r~n"

	istr_error_info.se_event_name = "wf_freezing_step"
	istr_error_info.se_procedure_name = "nf_pasp01br"

//	If iu_pas201.nf_pasp01br(istr_error_info, ls_update_fab) Then 
If iu_ws_pas1.nf_pasp01fr(istr_error_info, ls_update_fab) Then 
		wf_infomessage("Product Code " + Trim(lc_header_product) + " State 2 successfully inserted", "wf_freezing_step")
	End if
End if

If lc_product_status = 'I' Then
	wf_infomessage(Trim(lc_header_product) + " is an inactive product", "wf_freezing_step")
	This.SetRedraw(True)
	return False
End if

ll_row = dw_step_detail.InsertRow(0)
			
/*
PASFAB-FAB-PRODUCT-DESCR + '~t' // 1
PASFAB-FAB-TYPE          + '~t' // 2
PASFAB-FAB-GROUP         + '~t' // 3
PASFAB-YIELD-GROUP       + '~t' // 4
PASFAB-FAB-UOM           + '~t' // 5
PASFAB-SKU-PRODUCT-CODE  + '~t' // 6
WS-CHAR-KEY              + '~t' // 7
WS-PIECES-PER-BOX        + '~t' // 8
WS-REV-PIECES-PER-BOX    + '~t' // 9
WS-REV-PIECE-EFF-DATE    + '~t' // 0
*/

ls_fab_product_descr = iw_frame.iu_string.nf_gettoken(ls_fab, '~t') 	//1
ls_fab_type = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')				//2
ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//3
ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//4
ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//5
ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//6
ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//7
ls_pieces_per_box = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')		//8

dw_step_detail.SetItem(ll_row, "fab_product_code", lc_header_product)
dw_step_detail.SetItem(ll_row, "fab_product_descr", ls_fab_product_descr)
dw_step_detail.SetItem(ll_row, "product_state", '2')
dw_step_detail.SetItem(ll_row, "product_status", dw_step_header.GetItemString(1, "product_status"))
dw_step_detail.SetItem(ll_row, "fab_type", ls_fab_type)
dw_step_detail.SetItem(ll_row, "pieces_per_box", Dec(ls_pieces_per_box))
dw_step_detail.SetItem(ll_row, "yield", 0)
dw_step_detail.SetItem(ll_row, "key_product", 'Y')

Return true
end function

on open;iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then Close(This)
dw_step_header.InsertRow(0)
//dw_step_header.uf_ChangeRowStatus(1, NotModified!)
Call Super::Open
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_deleterow')

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event ue_postopen;call super::ue_postopen;DataWindowChild	dwc_type, &
						dwc_fabtype

String				ls_ShiftDuration

iu_ws_pas1 = Create u_ws_pas1
dw_step_header.GetChild("mfg_step_type", dwc_type)
dw_step_detail.GetChild("fab_type", dwc_fabtype)

dwc_type.SetTransObject(SQLCA)
dwc_type.Retrieve("STEPTYPE")
dwc_fabtype.SetTransObject(SQLCA)
dwc_fabtype.Retrieve("FABTYPE")
//iw_frame.iu_netwise_data.nf_GetTypeDesc('STEPTYPE', dwc_type)
//iw_frame.iu_netwise_data.nf_GetTypeDesc('FABTYPE', dwc_fabtype)

dwc_type.Sort()

istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "Mfg Steps"
istr_error_info.se_user_id 		= sqlca.userid


If iw_frame.iu_netwise_data.nf_GetShortDesc("SHIFTDUR", "MAXIMUM", ls_ShiftDuration) = -1 Then
	// Row not found
	MessageBox("Shift Duration", "Shift Duration not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
ii_ShiftDuration = Integer(ls_ShiftDuration)

dw_step_header.Object.shift_duration.EditMask.SpinRange = '0~~' + ls_ShiftDuration


This.PostEvent("ue_query")

end event

event resize;call super::resize;//integer li_x		= 1
//integer li_y		= 345

integer li_x		= 51
integer li_y		= 470

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


//dw_step_detail.width	= width - (50 + li_x)
//dw_step_detail.height = height - (125 + li_y)

dw_step_detail.width	= width - (li_x)
dw_step_detail.height = height - (li_y)



end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_enable('m_deleterow')

end on

on ue_clearwindow;call w_base_sheet_ext::ue_clearwindow;This.SetRedraw(False)
dw_step_detail.Reset()
dw_step_header.uf_ChangeRowStatus(1, NotModified!)
dw_step_header.SetColumn("mfg_step_description")
dw_step_header.SetFocus()
This.SetRedraw(True)

end on

event close;call super::close;Destroy iu_pas201
Destroy iu_ws_pas1
end event

on w_pas_step.create
int iCurrent
call super::create
this.dw_step_header=create dw_step_header
this.dw_step_detail=create dw_step_detail
this.cb_copystep=create cb_copystep
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_step_header
this.Control[iCurrent+2]=this.dw_step_detail
this.Control[iCurrent+3]=this.cb_copystep
end on

on w_pas_step.destroy
call super::destroy
destroy(this.dw_step_header)
destroy(this.dw_step_detail)
destroy(this.cb_copystep)
end on

event ue_fileprint;call super::ue_fileprint;DataWindowChild	ldwc_header, &
						ldwc_detail

DataStore			lds_print

If not ib_print_ok Then Return

lds_print = Create u_print_datastore
lds_print.DataObject = "d_pas_step_print"
						
lds_print.GetChild("d_pas_step_header",ldwc_header)
lds_print.GetChild("d_pas_step_detail",ldwc_detail)

dw_step_header.ShareData(ldwc_header)
dw_step_detail.ShareData(ldwc_detail)

lds_print.Print(True)

Destroy lds_print


end event

type dw_step_header from u_base_dw_ext within w_pas_step
integer width = 1755
integer height = 416
integer taborder = 10
string dataobject = "d_pas_step_header"
boolean border = false
end type

event itemchanged;call super::itemchanged;DataWindowChild	dwc_dddw

Int	li_counter, &
		li_temp

Long	ll_row_count, &
		ll_row, ll_rtn, ll_count

String	ls_temp, &
			ls_product,&
			ls_product_state, &
			ls_product_status

ll_row_count = dw_step_detail.RowCount()

Choose Case This.GetColumnName()

	Case "fab_product_code"
		If iw_frame.iu_string.nf_IsEmpty(data) Then 
			This.SetItem(1, "fab_product_descr", "")
			return 
		End if
		
		//ibdkeem ** 09/09/2002 ** Check for cycle.
		ls_product_state = GetItemString( 1, "product_state" )
		ls_product_status = GetItemString( 1, "product_status" )		
		IF IsNull( ls_product_state ) THEN ls_product_state = ""
		IF IsNull( ls_product_status ) THEN ls_product_status = ""
		
		For ll_count = 1 to dw_step_detail.RowCount()
			If Trim(dw_step_detail.GetItemString(ll_count, 'fab_product_code')) = Trim(data) &
			 and Trim(dw_step_detail.GetItemString(ll_count, 'product_state')) = Trim(ls_product_state) &
			 and Trim(dw_step_detail.GetItemString(ll_count, 'product_status')) = Trim(ls_product_status) Then
				This.SelectText(1, Len(data))
				wf_infomessage(Trim(data) + " is an output product.  This will cause a cycle.", "dw_step_header.itemchanged")
				Return 1
			End If
		Next
		
		If (Len(ls_product_state) > 0) and (Len(ls_product_status) > 0) Then
			ls_product = data
			If Not wf_select_tpasmfgs(ls_product, ls_product_state, ls_product_status) Then 
				This.SelectText(1, Len(data))
				return 1
			End if
		End If
		
		
	Case "product_state"
		ll_row = idddw_child_state.Find('type_code = "' + Trim(data) + '"', 1, idddw_child_state.RowCount())
		If ll_row <= 0 Then
			wf_infomessage(data + " is an Invalid Product State", "dw_step_header.itemchanged")

			This.SetFocus()
			This.SelectText(1, Len(data))
			
			return 1
		Else
			ls_product = GetItemString( 1, "fab_product_code")
			ls_product_status = GetItemString( 1, "product_status")
			IF IsNull( ls_product ) THEN ls_product = ""
			
			If (Len(ls_product) > 0) and (Len(ls_product_status) > 0) Then
				ls_product_state = data
				If Not wf_select_tpasmfgs(ls_product,ls_product_state, ls_product_status) Then 
					This.SelectText(1, Len(data))
					dw_step_header.SetColumn('fab_product_code')
					dw_step_header.setItem(1, "product_state", ' ')
					This.SetFocus()
					return 1
				End if
			End If
		
			SetItem( 1, "product_state_descr", idddw_child_state.GetItemString(ll_row, 'type_short_desc'))
			This.SetFocus()
			This.SelectText(1, Len(data))
		End if
		
	Case "product_status"
		ll_row = idddw_child_status.Find('type_code = "' + Trim(data) + '"', 1, idddw_child_status.RowCount())
		If ll_row <= 0 Then
			wf_infomessage(data + " is an Invalid Product Status", "dw_step_header.itemchanged")

			This.SetFocus()
			This.SelectText(1, Len(data))
			
			return 1
		Else
			ls_product = GetItemString( 1, "fab_product_code")
			ls_product_state = GetItemString( 1, "product_state")
			IF IsNull( ls_product ) THEN ls_product = ""
			
			If (Len(ls_product) > 0) and (Len(ls_product_state) > 0) Then
				ls_product_status = data
				If Not wf_select_tpasmfgs(ls_product,ls_product_state, ls_product_status) Then 
					This.SelectText(1, Len(data))
					dw_step_header.SetColumn('fab_product_code')
					dw_step_header.setItem(1, "product_status", ' ')
					This.SetFocus()
					return 1
				End if
			End If
		
			SetItem( 1, "product_status_descr", idddw_child_status.GetItemString(ll_row, 'type_short_desc'))
			This.SetFocus()
			This.SelectText(1, Len(data))
		End if
			
		
	Case "mfg_step_description"
		This.GetChild("mfg_step_sequence", dwc_dddw)
		li_temp = dwc_dddw.Find("Trim(mfg_step_descr) = '" + Trim(data) + "'", 1, dwc_dddw.RowCount())
		If li_temp > 0 Then
			wf_infomessage(Trim(data) + " is already a step description", "dw_step_header.itemchanged")
			This.SelectText(1,Len(data))
			return 1
		End if
		
	Case "mfg_step_type"
		is_last_step_type = This.GetItemString( 1, "mfg_step_type")
		Choose Case Trim(data)
		Case "F"
			// check to see that the rows are really filled in
			// Freezing step, only one product possible (frozen input)
			Choose Case ll_row_count 
				Case 0
					wf_freezing_step()
	
				Case 1
					ls_product = Trim(dw_step_detail.GetItemString(1, &
											"fab_product_code"))
					If Pos(ls_product, "*", 1) > 0 Then Return
					If len(ls_product) > 0 Then
						If MessageBox("Freezing Step", "Freezing Steps can " + &
								"only have one output product which must be " + &
								Trim(dw_step_header.GetItemString(1, &
								"fab_product_code")) + &
								"*.  Would you like to continue by deleting " + &
								"current outputs and inserting" + &
								" frozen product?", Question!, YesNo!, 2) = 2 Then
							// clicked on no
							Return 1
						End If			
					End If
					Parent.SetRedraw(False)
					dw_step_detail.DeleteRow(1)
					wf_freezing_step()
					Parent.SetRedraw(True)
					
				Case Is > 1
					If MessageBox("Freezing Step", "Freezing Steps can " + &
							"only have one output product which must be " + &
							Trim(dw_step_header.GetItemString(1, &
							"fab_product_code")) + &
							"*.  Would you like to continue by deleting " + &
							"current outputs and inserting" + &
							" frozen product?", Question!, YesNo!, 2) = 2 Then
						// clicked on no
						Return 1
					End If			
					Parent.SetRedraw(False)
					Do While dw_step_detail.RowCount() > 0
						dw_step_detail.DeleteRow(1)
					Loop
					wf_freezing_step()
					Parent.SetRedraw(True)
				End Choose
	
		Case "T"
			// Transfer Step,  No outputs possible
			If ll_row_count > 0 Then
				// check to see that the rows are really filled in
	//			For li_counter = 1 to ll_row_count
					If Len(Trim(dw_step_detail.GetItemString(1, "fab_product_code"))) &
							> 0 Then
						If MessageBox("Transfer Product", "Transfer Steps cannot have output products.  " + &
								"Would you like to delete all output products now?", &
								Question!, YesNo!, 2) = 2 Then
							// clicked on no
							Return 1
						End If
	//				Else
	//					Exit
					End If			
	//			Next
				Parent.SetRedraw(False)
				Do While dw_step_detail.RowCount() > 0
					dw_step_detail.DeleteRow(1)
				Loop					
				Parent.SetRedraw(True)
			End if
		
		Case "W"
			// Weight Step, All outputs must be of type weight
			For li_counter = 1 to ll_row_count
				If dw_step_detail.GetItemString(li_counter, "fab_type") <> "WT" Then
					If MessageBox("Weight Step", "All output products for a Weight Step " + &
											"must be of type Weight.  Would you like to delete " + &
											"all non Weight Steps?", Question!, YesNo!, 2) = 1 Then
						Parent.SetRedraw(False)
						For li_counter = 1 to dw_step_detail.RowCount()
							If dw_step_detail.GetItemString(li_counter, "fab_type") <> "WT" Then
								dw_step_detail.DeleteRow(li_counter)
								li_counter --
							End if
						Next
						Parent.SetRedraw(True)
					Else
						// clicked on no
						Return 1
					End if
					Exit
				End if
			Next
	
		Case "L", "S"
			// Make sure all products are of the same fab group
			ls_temp = "fab_group <> '" + dw_step_header.GetItemString(1, 'fab_group') + "'"
			ll_row = dw_step_detail.Find(ls_temp, 1, dw_step_detail.RowCount())
				
			If ll_Row > 0 Then	
				wf_infomessage( "Fab group " + dw_step_detail.GetItemString( &
					ll_Row, 'fab_group') + " of product " + dw_step_detail.GetItemString( &
					ll_Row, 'fab_product_code') + " does not match fab group " + &
					dw_step_header.GetItemString(1, "fab_group") + " of parent product " + &
					dw_step_header.GetItemString(1, "fab_product_code"), "dw_step_header.itemchanged")
				dw_step_detail.SetRow(ll_Row)
				Return 1
			End if
			
		Case "C"
			// Labeling or Standard, or Conversion no restrictions here
		End Choose		
	
	Case "mfg_step_sequence"
		This.GetChild("mfg_step_sequence", dwc_dddw)
		li_temp = dwc_dddw.Find('mfg_step_sequence = ' + &
								String(This.GetItemNumber(1, "mfg_step_sequence")), &
								1, dwc_dddw.RowCount())
		This.SetItem(1, "mfg_step_descr", dwc_dddw.GetItemString(li_temp, "mfg_step_descr"))
	
	Case "shift_duration"
		If Not IsNumber(data) Or Integer(data) < 0 Or &
				Integer(data) > ii_shiftduration Then
			wf_infomessage( "Shift Duration must be a number between 0 and " + String(ii_shiftduration), "dw_step_header.itemchanged")
			This.SelectText(1, Len(data))
			Return 1
		End if

End Choose
end event

on getfocus;call u_base_dw_ext::getfocus;//This.SetColumn("mfg_step_description")
iw_frame.im_menu.mf_disable('m_deleterow')

end on

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;This.SelectText(1, Len(GetText()))
end on

event itemerror;call super::itemerror;Choose Case dwo.Name
Case "mfg_step_description", "shift_duration", "fab_product_code" , "product_state" , "product_status"
	Return 1
Case "mfg_step_type"
	This.SetItem(1, "mfg_step_type", is_last_step_type)
	Return 1
End Choose
end event

event constructor;call super::constructor;This.GetChild("product_state", idddw_child_state)

idddw_child_state.SetTransObject(SQLCA)
idddw_child_state.Retrieve("PRDSTATE")
idddw_child_state.SetSort("type_code")
idddw_child_state.Sort()

This.GetChild("product_status", idddw_child_status)

idddw_child_status.SetTransObject(SQLCA)
idddw_child_status.Retrieve("PRODSTAT")
idddw_child_status.SetSort("type_code")
idddw_child_status.Sort()

This.InsertRow(0)
end event

type dw_step_detail from u_base_dw_ext within w_pas_step
event ue_set_detail_focus pbm_custom24
integer y = 416
integer width = 2560
integer height = 832
integer taborder = 20
string dataobject = "d_pas_step_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

on ue_set_detail_focus;call u_base_dw_ext::ue_set_detail_focus;This.SetRow(This.RowCount())
This.SetColumn("fab_product_code")
This.SetFocus()
Parent.SetRedraw(True)

end on

on getfocus;call u_base_dw_ext::getfocus;iw_frame.im_menu.mf_enable('m_deleterow')
This.TriggerEvent(RowFocusChanged!)

end on

event itemchanged;call super::itemchanged;Char		lc_dummy, &
			lc_product_state, &
			lc_product_status

Int		li_tab, &
			li_ret, &
			li_temp
			
Long		ll_keyprod, &
			ll_row_count

String	ls_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_fab, &
			ls_fab_group, &
			ls_fab_product_descr, &
			ls_fab_type, &
			ls_pieces_per_box, & 
			ls_StepType,&
			ls_temp,&		
			ls_dummy

If Dwo.name = "key_product" Then
	ll_row_count = This.RowCount()

	If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(row, "fab_product_code"))Then 
		Return 2
	End If

	CHOOSE CASE row 

		CASE 1
			ll_keyprod = dw_step_detail.Find ( "key_product = 'Y'", row + 1, ll_row_count)
				
		CASE 2 to (ll_row_count - 1)
			ll_keyprod = dw_step_detail.Find ( "key_product = 'Y'", row - 1, 1)
			If ll_keyprod = 0 Then ll_keyprod = dw_step_detail.Find ("key_product = 'Y'", row + 1, ll_row_count)

		CASE ll_row_count 
			ll_keyprod = dw_step_detail.Find ( "key_product = 'Y'", row - 1, 1)

	END CHOOSE	

	If ll_keyprod <> 0 Then
		This.SetItem(ll_keyprod, "key_product", "N")
	End If
end if

If Dwo.name = "fab_product_code" or &
		Dwo.name = "product_state" or &
		Dwo.name = "product_status" then
		
		If Dwo.name = "fab_product_code" Then
			ls_product_code = data
			ls_product_state = GetItemString( row, "product_state")
			ls_product_status = GetItemString( row, "product_status")
		Else
			If Dwo.name = "product_state" Then
				ls_product_code = GetItemString( row, "fab_product_code")
				ls_product_state = data
				ls_product_status = GetItemString( row, "product_status")
			Else
				ls_product_code = GetItemString( row, "fab_product_code")
				ls_product_state = GetItemString( row, "product_state")
				ls_product_status = data
			End If
		End If
		
		If iw_frame.iu_string.nf_isempty(ls_product_code) then return
		If iw_frame.iu_string.nf_isempty(ls_product_state) then return 
		If iw_frame.iu_string.nf_isempty(ls_product_status) then return
		
		If Trim(ls_product_code) = Trim(dw_step_header.GetItemString(1, "fab_product_code")) and &
			Trim(ls_product_state) = Trim(dw_step_header.GetItemString(1, "product_state")) and &
			Trim(ls_product_status) = Trim(dw_step_header.GetItemString(1, "product_status")) Then
			wf_infomessage("The output product of a step cannot be the same as the input product.", "dw_step_detail.itemchanged")
			
			this.setitem(row,"fab_product_code", "")
			this.setitem(row,"product_state", "")
			this.setitem(row,"product_status", "")			
			this.setcolumn("fab_product_code")
			return 1
		End if
	
		If wf_find_duplicate(ls_product_code, ls_product_state, ls_product_status, row) <> 0 Then
			wf_infomessage("Product " + ls_product_code + " is already an output product in this step.", "dw_step_detail.itemchanged")

			this.setitem(row,"fab_product_code", "")
			this.setitem(row,"product_state", "")
			this.setitem(row,"product_status", "")
			this.setcolumn("fab_product_code")			
			return 1
		End if
		
		istr_error_info.se_event_name = "Itemchanged for dw_step_detail"
		istr_error_info.se_procedure_name = "nf_pasp00br"
		istr_error_info.se_message = Space(70)
	
		lc_product_state = ls_product_state
		
		
		
		ls_dummy = lc_dummy
		ls_product_status = lc_product_status
		
//		li_ret = iu_pas201.nf_pasp00br(istr_error_info, &
//												ls_product_code, &
//												lc_product_state,&
//												lc_dummy, &
//												lc_product_status, &
//												ls_fab)
     		li_ret = iu_ws_pas1.nf_pasp00fr(istr_error_info, &
												ls_product_code, &
												lc_product_state,&
												ls_dummy, &
												ls_product_status, &
												ls_fab)


		If li_ret <> 0 then 
//			This.SetFocus()
//			This.SelectText(1,len(ls_product_code))

			this.setitem(row,"fab_product_code", "")
			this.setitem(row,"product_state", "")
			this.setitem(row,"product_status", "")
			this.setcolumn("fab_product_code")
			return 1
		End if
	
		If lc_product_status = 'I' Then
			wf_infomessage(Trim(ls_product_code) + " is an inactive product, please choose another product.", "dw_step_detail.itemchanged")
//			This.SelectText(1, Len(ls_product_code))

			this.setitem(row,"fab_product_code", "")
			this.setitem(row,"product_state", "")
			this.setitem(row,"product_status", "")
			this.setcolumn("fab_product_code")
			return 1
		End if
					
		/*
		PASFAB-FAB-PRODUCT-DESCR + '~t' // 1
		PASFAB-FAB-TYPE          + '~t' // 2
		PASFAB-FAB-GROUP         + '~t' // 3
		PASFAB-YIELD-GROUP       + '~t' // 4
		PASFAB-FAB-UOM           + '~t' // 5
		PASFAB-SKU-PRODUCT-CODE  + '~t' // 6
		WS-CHAR-KEY              + '~t' // 7
		WS-PIECES-PER-BOX        + '~t' // 8
		WS-REV-PIECES-PER-BOX    + '~t' // 9
		WS-REV-PIECE-EFF-DATE    + '~t' // 0
		*/
		
		ls_fab_product_descr = iw_frame.iu_string.nf_gettoken(ls_fab, '~t') 	//1
		ls_fab_type = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')				//2
		ls_fab_group = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')				//3
		ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//4
		ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//5
		ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//6
		ls_temp = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')					//7
		ls_pieces_per_box = iw_frame.iu_string.nf_gettoken(ls_fab, '~t')		//8

		ls_StepType = Trim(dw_step_header.GetItemString(1, "mfg_step_type"))
		Choose Case ls_StepType
			Case	"W"
			// Weight Step, All outputs must be of fab type weight
				If ls_fab_type <> "WT" Then
					wf_infomessage("All output products of a Weight Step must be of type Weight.", "dw_step_detail.itemchanged")

					this.setitem(row,"fab_product_code", "")
					this.setitem(row,"product_state", "")
					this.setitem(row,"product_status", "")
					this.setcolumn("fab_product_code")	
					return 1
				End if
		
			Case "T"
				If This.RowCount() > 0 Then
					If MessageBox("Invalid Output Product", "Transfer Steps cannot have output products." + &
										"  Do you wish to delete all output products?", &
										Question!, YesNo!, 2) = 1 Then
						This.SetRedraw(False)
						Do while This.RowCount() > 0
							This.DeleteRow(1)
						Loop
						This.SetRedraw(True)	
					End if
				End if
					
			Case "F"

				If ls_product_code <> Trim(dw_step_header.GetItemString(1, "fab_product_code")) and &
					ls_product_state <> '2' Then
					wf_infomessage("Freezing Steps can only have one output product which must be " + &
									Trim(dw_step_header.GetItemString(1, "fab_product_code")) + " and State 2", "dw_step_detail.itemchanged")

					this.setitem(row,"fab_product_code", "")
					this.setitem(row,"product_state", "")
					this.setitem(row,"product_status", "")
					this.setcolumn("fab_product_code")
					Return 1
				End if
	
		End Choose
	
		// If step type is conversion, then it is ok to go between fab groups
		If Trim(ls_fab_group) <> Trim(dw_step_header.GetItemString(1, "fab_group")) And &
			ls_StepType <> "C" Then
			wf_infomessage("Fab group " + ls_fab_group + " of product " + &
							ls_product_code + " does not match fab group " + &
							dw_step_header.GetItemString(1, "fab_group") + " of parent product " + &
							dw_step_header.GetItemString(1, "fab_product_code"), "dw_step_detail.itemchanged")

			this.setitem(row,"fab_product_code", "")
			this.setitem(row,"product_state", "")
			this.setitem(row,"product_status", "")
			this.setcolumn("fab_product_code")
			return 1
		End if	
		
		dw_step_detail.SetItem(row, "fab_product_descr", ls_fab_product_descr)
		dw_step_detail.SetItem(row, "fab_type", ls_fab_type)
		dw_step_detail.SetItem(row, "pieces_per_box", Dec(ls_pieces_per_box))
		dw_step_detail.SetItem(row, "yield", 0)
		dw_step_detail.SetItem(row, "key_product", 'N')
		
		ls_StepType = Trim(dw_step_header.GetItemString(1, "mfg_step_type"))
		IF ls_StepType <>  'F' Then
			IF row > 0 and This.Rowcount() = row Then
				IF Not iw_frame.iu_string.nf_IsEmpty(ls_product_code) &
						AND Not iw_frame.iu_string.nf_IsEmpty(ls_product_state) &
						AND Not iw_frame.iu_string.nf_IsEmpty(ls_product_status) Then 
					parent.postevent("ue_addrow")
				End IF
			End IF
		End IF
END IF

return 0
end event

on losefocus;call u_base_dw_ext::losefocus;This.SelectRow(0, False)

end on

event itemerror;call super::itemerror;Choose Case This.GetColumnName()
Case "fab_product_code", "product_state", "product_status", "key_product"
	// itemchanged will handle all errors for this column
	Return 1
End Choose


end event

event rowfocuschanged;// SELECTION PROFILES
// 0 = No rows should be selected
// 1 = select only one row at a a time
// 2 = select multiple rows
// 3 = select multiple rows with support for Control and Shift keys  

integer	li_retval

// Update the il_dwrow with the current row
il_dwrow = GetRow()

// Depending on is_selection, highlight the proper row(s)
Choose Case is_selection
	Case "1"
		SelectRow(0,false)
		IF il_dwrow > 0 Then
			SelectRow(il_dwrow, TRUE)
		ELSE
			SelectRow( This.Rowcount(),True)
		END IF
	Case "2"
		//SelectRow(il_dwrow, TRUE)
	Case "3"
End Choose

//integer	li_retval
//Long		ll_dwrow
//
//// Update the ll_dwrow with the current row
//// ll_dwrow = This.GetRow()
//
//SelectRow(0,false)
//SelectRow(This.GetRow(), TRUE)
//
end event

event constructor;call super::constructor;DataWindowChild	ldwc_child1, ldwc_child2

is_selection = '1'

This.GetChild("product_state", ldwc_child1)

ldwc_child1.SetTransObject(SQLCA)
ldwc_child1.Retrieve('PRDSTATE')

This.GetChild("product_status", ldwc_child2)

ldwc_child2.SetTransObject(SQLCA)
ldwc_child2.Retrieve('PRODSTAT')

end event

type cb_copystep from commandbutton within w_pas_step
integer x = 1810
integer y = 12
integer width = 297
integer height = 108
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Copy Step"
end type

event clicked;Long		ll_row

dwItemStatus	lis_status


dw_step_header.SetItem(1, "fab_product_code", '')
//dw_step_header.SetItem(1, "product_state", '')
//dw_step_header.SetItem(1, "product_status", '')
//JXR 9/10/18 - set step sequence to 0 when copying step
dw_step_header.SetItem(1,"mfg_step_sequence",0)
dw_step_header.uf_changerowstatus(1, new!)
dw_step_header.SetFocus()
dw_step_header.SetColumn("fab_product_code")

ll_row = 1
lis_status = dw_step_detail.GetItemStatus(ll_row, 0, Primary!)
Do While lis_status = notmodified! and ll_row <= dw_step_detail.RowCount()
	dw_step_detail.uf_changerowstatus(ll_row, newmodified!)
	ll_row ++
	lis_status = dw_step_detail.GetItemStatus(ll_row, 0, Primary!)
Loop

Parent.SetRedraw(True)
end event

