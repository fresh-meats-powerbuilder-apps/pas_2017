﻿$PBExportHeader$w_pas_yields_tree.srw
forward
global type w_pas_yields_tree from w_base_sheet_ext
end type
type dw_fab_product_code from u_fab_product_code within w_pas_yields_tree
end type
type tv_tree from treeview within w_pas_yields_tree
end type
type dw_header from u_base_dw_ext within w_pas_yields_tree
end type
type dw_yields from u_base_dw_ext within w_pas_yields_tree
end type
end forward

global type w_pas_yields_tree from w_base_sheet_ext
integer x = 9
integer y = 60
integer width = 3680
integer height = 1676
string title = "Yield Tree"
long backcolor = 67108864
event ue_computeyields ( )
event ue_postcomputeyields ( )
event ue_commit_copy_ylds ( )
event ue_print_copy_yields_rpt ( )
dw_fab_product_code dw_fab_product_code
tv_tree tv_tree
dw_header dw_header
dw_yields dw_yields
end type
global w_pas_yields_tree w_pas_yields_tree

type variables
s_error	istr_error_info
u_pas203	iu_pas203
u_pas201	iu_pas201
u_ws_pas3	iu_ws_pas3

Integer	ii_parent_step, ii_old_width

String	is_parent_string, &
			is_dates_string, &
			is_background_color, &
			is_protect, &
			is_SaveAnyway
										
DataStore	ids_tree, &
				ids_compute_datastore, &
				ids_expand_datastore

Boolean 	ib_collapsing, &
			ib_StopScroll
			
Boolean	ib_first_time, &
			ib_expand_all, &
			ib_set_redraw = False, &
			ib_update_successful, &
			ib_inquire_required, &
			ib_endoftree, &
			ib_compute_yields, &
			ib_first_compute

Long		il_row, &
			il_item_count, &
			il_current_row, &
			il_computefromchar, &
			il_selected_row, &
			il_StopNodeIdx
end variables
forward prototypes
public function boolean wf_update ()
public function decimal wf_get_modified_total (long al_products_parent_row)
public function boolean wf_get_next_level (long al_row)
public function boolean wf_check_against_children_yield (long al_row, ref decimal adc_difference)
public function boolean wf_check_against_parent_yield (long al_next_modified, ref decimal adc_difference, ref string as_parent_product)
public function boolean wf_deleterow ()
public subroutine wf_expand_next_level (long al_handle)
public function boolean wf_get_yields (long al_row, long al_characteristic, ref decimal adc_yield, ref decimal adc_piece_ratio)
public function long wf_getdwchildproductrow (long al_row)
public function decimal wf_calculate_to_child (long al_row, long al_parent_row)
public function long wf_getdwparentproductrow (long al_handle)
public function boolean wf_calculate_all_yields (long al_handle)
public function boolean wf_is_highest_level (long al_dw_row)
public function long wf_getparentrow (long al_row)
public function boolean wf_build_copy_yield_string (ref string as_copy_yield_string, long al_next_modified, decimal adc_yield, decimal adc_piece_ratio)
public function boolean wf_set_selected_row ()
public subroutine wf_popup_menu ()
public function boolean wf_collapse (integer al_handle)
public function boolean wf_collapse_yield (long al_handle)
public function boolean wf_expand_yields (long al_handle)
public function boolean wf_get_compute_yields (long al_row, long al_characteristic)
public subroutine wf_post_update ()
public function boolean wf_retrieve ()
public function boolean wf_scroll_tree (long al_first_row_on_page)
end prototypes

event ue_computeyields;Decimal				ldc_to_yield

Long					ll_treerow, &
						ll_handle

w_yield_tree_copy_popup		lw_computeyields


If Not wf_set_selected_row() Then Return

IF il_selected_row < 2 Then
	iw_frame.SetMicrohelp('No Row Selected for the Compute Yields')
	Return
End If

If dw_yields.GetColumnName() = 'yield' Then
	IF dw_yields.AcceptText() < 0 Then Return
End If

Openwithparm(lw_computeyields, This, This)

If Message.StringParm = 'Cancel' Then Return

If dw_yields.GetItemStatus(il_selected_row, 'yield', primary!) = NotModified! And &
		dw_header.GetItemNumber(1, 'characteristic') = il_computefromchar Then
	iw_frame.SetMicrohelp('Nothing to compute')
	Return
End If
This.SetRedraw(False)

ll_handle = dw_yields.GetItemNumber(il_selected_row, 'tree_row')

If Not IsValid(ids_compute_datastore) Then
	ids_compute_datastore = create Datastore
	ids_compute_datastore.DataObject = 'd_yields_tree'
Else
	ids_compute_datastore.Reset()
End If
ib_first_compute = True
ib_compute_yields = True
wf_calculate_all_yields(ll_handle)
This.Event Post ue_postcomputeyields()

//MessageBox('Time', String(now()))
tv_tree.SetRedraw(True)
dw_yields.SetRedraw(True)
This.SetRedraw(True)
end event

event ue_postcomputeyields;ib_compute_yields = False

end event

event ue_commit_copy_ylds();String			ls_input_string

Long				ll_parent_row

Integer			li_ret


If Not wf_set_selected_row() Then Return

ll_parent_row = wf_getparentrow(il_selected_row)

ls_input_string = String(dw_header.GetItemDate(1, 'update_begin_date'), 'yyyy-mm-dd') &
			+ "~t" + String(dw_header.GetItemDate(1, 'update_end_date'), 'yyyy-mm-dd') &
			+ "~t" + dw_yields.GetItemString(ll_parent_row, 'input_fab_product') &
			+ "~t" + String(dw_yields.GetItemNumber(ll_parent_row, 'mfg_step_sequence')) &
			+ "~t" + dw_yields.GetItemString(il_selected_row, 'input_fab_product') &
			+ "~t" + dw_yields.GetItemString(il_selected_row, 'output_fab_product') &
			+ "~t" + String(dw_yields.GetItemNumber(il_selected_row, 'mfg_step_sequence')) &
			+ "~r~n"

//li_ret = iu_pas201.nf_pasp99br_upd_commit_copy_ylds(istr_error_info, &
//															ls_input_string)

li_ret = iu_ws_pas3.uf_pasp99fr(istr_error_info, &
											ls_input_string)

If li_ret <> 0 Then
	Return
End If

iw_frame.setmicrohelp("Commit Successful")

Return
end event

event ue_print_copy_yields_rpt();String			ls_input_string

Long				ll_parent_row

Integer			li_ret


ls_input_string = String(dw_header.GetItemDate(1, 'update_begin_date'), 'yyyy-mm-dd') &
			+ "~t" + String(dw_header.GetItemDate(1, 'update_end_date'), 'yyyy-mm-dd') &
			+ "~t" + "" &
			+ "~t" + "" &
			+ "~t" + "" &
			+ "~t" + "" &
			+ "~t" + "" &
			+ "~r~n"

//li_ret = iu_pas201.nf_pasp99br_upd_commit_copy_ylds(istr_error_info, &
//															ls_input_string)

li_ret = iu_ws_pas3.uf_pasp99fr(istr_error_info, &
											ls_input_string)

If li_ret <> 0 Then
	Return
End If

iw_frame.setmicrohelp("Copy Yields Report Created Successful")


Return
end event

public function boolean wf_update ();Integer	li_ret
Long		ll_handle, &
			ll_next_modified
String	ls_input_header, &
			ls_input_detail
			
If dw_yields.AcceptText() < 1 Then Return False

SetPointer(HourGlass!)

ib_update_successful = True
ib_expand_all = True

il_current_row = dw_yields.GetRow()
This.SetRedraw(False)
ll_handle = tv_tree.FindItem(RootTreeItem!, 0)
This.wf_expand_next_level(ll_handle)

This.Function Post wf_post_update()
Yield()

If ib_update_successful = False Then
	Return False
End If

Return True

end function

public function decimal wf_get_modified_total (long al_products_parent_row);Boolean			lb_more_rows

Long				ll_product_row, &
					ll_rowcount
					
Decimal			ldc_total_products_yield, &
					ldc_yield


lb_more_rows = True

ldc_total_products_yield = 0

ll_rowcount = dw_yields.RowCount()
ll_product_row = dw_yields.Find('parent_tree_row = ' + String(al_products_parent_row), &
			1, ll_rowcount + 1)

Do While ll_product_row > 0 
	If IsNull(dw_yields.GetItemNumber(ll_product_row, 'yield')) Then
		ldc_yield = 0.00
	Else
		ldc_yield = dw_yields.GetItemDecimal(ll_product_row, 'yield')
	End IF
	ldc_total_products_yield += ldc_yield
	ll_product_row ++
	ll_product_row = dw_yields.Find('parent_tree_row = ' + String(al_products_parent_row), &
				ll_product_row, ll_rowcount + 1)
Loop

Return ldc_total_products_yield
end function

public function boolean wf_get_next_level (long al_row);Long						ll_pos, &
							ll_pos2, &
							ll_pos3
							
Decimal					ldc_yield, &
							ldc_piece_ratio

String					ls_header_string, &
							ls_output_product, &
							ls_tree_products, &
							ls_temp
Integer					li_ret

u_string_functions	lu_strings


If dw_header.GetItemNumber(1, 'characteristic') = 0 Then
	dw_yields.SetItem(al_row, 'yield', 0.00)
	dw_yields.SetItem(al_row, 'piece_ratio', 1.00)
	dw_yields.SetItem(al_row, 'char_key', 0)
Else
	If dw_yields.GetItemNumber(al_row, 'Char_key') = 0 Then
		MessageBox('Delete Row', "It is already using the Standard Yield.")
		Return False
	End IF
	If Not wf_Get_Yields(al_row, 0, ldc_yield, ldc_piece_ratio) Then Return False
	
	dw_yields.SetItem(al_row, 'yield', ldc_yield)
	dw_yields.SetItem(al_row, 'piece_ratio', ldc_piece_ratio)
	dw_yields.SetItem(al_row, 'char_key', 0)
	dw_yields.SetItem(al_row, 'delete_ind', 'Y')

	If dw_yields.GetItemString(al_row, 'fab_type') <> 'BY' &
		Or dw_yields.GetItemString(al_row, 'sku_product_ind') = 'N' Then
	tv_tree.ExpandItem(dw_yields.GetItemNumber(al_row, 'tree_row'))
End If


End If

ls_output_product = dw_yields.GetItemString(al_row, 'output_fab_product')

If ldc_yield > 0 Then
	iw_frame.SetMicroHelp(Trim(ls_output_product) + ' was deleted.  Replaced by the Standard yield.')
Else
	iw_frame.SetMicroHelp(Trim(ls_output_product) + ' was deleted.')
End If

Return True

end function

public function boolean wf_check_against_children_yield (long al_row, ref decimal adc_difference);Boolean			lb_product_found

String			ls_parent_product

Long				ll_parents_product_tree_row, &
					ll_step_row, &
					ll_product_row, &
					ll_steps_tree_row, &
					ll_parent_product_row, &
					ll_rowcount

Decimal			ldc_products_yield, &
					ldc_parent_yield, &
					ldc_yield
					

If IsNull(dw_yields.GetItemNumber(al_row, 'yield')) Then
	ldc_yield = 0.00
Else
	ldc_yield = dw_yields.GetItemDecimal(al_row, 'yield')
End IF
ldc_parent_yield += ldc_yield

ll_rowcount = dw_yields.RowCount()
ll_step_row = al_row
ll_parents_product_tree_row = dw_yields.GetItemNumber(ll_step_row, 'tree_row')
ll_step_row = dw_yields.Find('parent_tree_row = ' + String(ll_parents_product_tree_row), &
			ll_step_row, ll_rowcount + 1)
If ll_step_row < 1 Then
	Return False
End If
Do While ll_step_row > 0
	lb_product_found = False
	ll_steps_tree_row = dw_yields.GetItemNumber(ll_step_row, 'tree_row')
	ll_product_row = ll_step_row
	ldc_products_yield = 0
	ll_product_row = dw_yields.Find('parent_tree_row = ' + String(ll_steps_tree_row), &
			ll_product_row, ll_rowcount + 1)
	Do While ll_product_row > 0
		If IsNull(dw_yields.GetItemNumber(ll_product_row, 'yield')) Then
			ldc_yield = 0.00
		Else
			ldc_yield = dw_yields.GetItemDecimal(ll_product_row, 'yield')
		End IF
		ldc_products_yield += ldc_yield
		ll_product_row ++
		ll_product_row = dw_yields.Find('parent_tree_row = ' + String(ll_steps_tree_row), &
				ll_product_row, ll_rowcount + 1)
		lb_product_found = True
	Loop
	adc_difference = ldc_parent_yield - ldc_products_yield
	If adc_difference <> 0.00 and lb_product_found Then
		Return True
	End If
	adc_difference = 0.00
	ll_step_row ++
	ll_step_row = dw_yields.Find('parent_tree_row = ' + String(ll_parents_product_tree_row), &
			ll_step_row, ll_rowcount + 1)
Loop

Return True

end function

public function boolean wf_check_against_parent_yield (long al_next_modified, ref decimal adc_difference, ref string as_parent_product);String			ls_parent_product

Long				ll_products_parent_row, &
					ll_step_row, &
					ll_parent_row, &
					ll_steps_parent_row, &
					ll_parent_product_row, &
					ll_rowcount

Decimal			ldc_total_modified, &
					ldc_parent_yield, &
					ldc_yield
					

ll_rowcount = dw_yields.RowCount()
ll_products_parent_row = dw_yields.GetItemNumber(al_next_modified, 'parent_tree_row')
ll_step_row = dw_yields.Find('tree_row = ' + String(ll_products_parent_row), 1, ll_rowcount + 1)
If ll_step_row < 1 Then
	Return False
End If

ll_steps_parent_row = dw_yields.GetItemNumber(ll_step_row, 'parent_tree_row')
ll_parent_product_row = dw_yields.Find('tree_row = ' + String(ll_steps_parent_row), 1, ll_rowcount + 1)
If ll_parent_product_row < 1 Then
	Return False
End If

//If the parent row doesn't have a parent it is at the top of the tree.  don't do the check.
If Len(Trim(dw_yields.GetItemString(ll_parent_product_row, 'input_fab_product'))) = 0 Then
	adc_difference = 0.00
	Return True
End If

If IsNull(dw_yields.GetItemNumber(ll_parent_product_row, 'yield')) Then
	ldc_yield = 0.00
Else
	ldc_yield = dw_yields.GetItemDecimal(ll_parent_product_row, 'yield')
End IF
ldc_parent_yield += ldc_yield
as_parent_product = dw_yields.GetItemString(ll_parent_product_row, 'output_fab_product')

adc_difference = ldc_parent_yield - wf_get_modified_total(ll_products_parent_row)

Return True


end function

public function boolean wf_deleterow ();Long				ll_row
String			ls_product

ll_row = dw_yields.GetRow()
If ll_row <= 0 Then
	MessageBox('Delete Row', 'You must select a row to delete.')
	Return False
End If

dw_yields.SelectRow(ll_row, True)
If MessageBox("Delete Yield","Are you sure you want to delete the selected yield?", &
	Question!,YesNo!) = 2 Then
	dw_yields.SelectRow(ll_row, False)
	Return False
End If
dw_yields.SelectRow(ll_row, False)

This.wf_get_next_level(ll_row)

Return True


end function

public subroutine wf_expand_next_level (long al_handle);Long				ll_childhandle, &
					ll_parent_handle, &
					ll_handle, &
					ll_item_level
String			ls_temp
TreeViewItem	lti_item

ll_handle = al_handle

Do While ll_handle > 0
  tv_tree.GetItem(ll_handle,lti_item)
	If Right(lti_item.data, 1) <> 'N' &
			and lti_item.children Then
		If Not lti_item.Expanded Then
			tv_tree.ExpandItem(ll_handle)
		End If
		wf_expand_next_level(tv_tree.FindItem(ChildTreeItem!,ll_handle))
	End If
  ll_handle = tv_tree.FindItem(NextTreeItem!, ll_handle)
Loop


end subroutine

public function boolean wf_get_yields (long al_row, long al_characteristic, ref decimal adc_yield, ref decimal adc_piece_ratio);Long						ll_pos, &
							ll_pos2, &
							ll_pos3
							
Decimal					ldc_yield, &
							ldc_piece_ratio

String					ls_header_string, &
							ls_output_product, &
							ls_output_product_state, &
							ls_output_product_status, &
							ls_tree_products, &
							ls_temp
Integer					li_ret

dwItemStatus			lis_temp

u_string_functions	lu_strings


ls_output_product = dw_yields.GetItemString(al_row, 'output_fab_product')
ls_output_product_state = dw_yields.GetItemString(al_row, 'output_product_state')
ls_output_product_status = dw_yields.GetItemString(al_row, 'output_product_status')

lis_temp = dw_yields.GetItemStatus(al_row, 0, primary!)

ls_header_string = dw_yields.GetItemString(al_row, 'input_fab_product') + '~t' + &
							String(dw_yields.GetItemNumber(al_row, 'mfg_step_sequence')) + '~t' + &
							String(al_characteristic) + '~t' + &
							String(dw_header.GetItemDate(1, 'effective_begin_date'), 'yyyy-mm-dd') + '~t' + &
							String(dw_header.GetItemDate(1, 'effective_end_date'), 'yyyy-mm-dd') + '~t' + &
							dw_header.GetItemString(1, 'effective_pending_ind') + '~t' + &
							ls_output_product_state + '~t' + ls_output_product_status + '~t' + &
							'C'

istr_error_info.se_event_name = "dw_yields Itemchanged"
	
//li_ret = iu_pas203.nf_pasp67br_inq_pas_yields_tree(istr_error_info, &
//								ls_output_product, &
//								ls_header_string, &
//								ls_tree_products)

li_ret = iu_ws_pas3.uf_pasp67fr(istr_error_info, &
								ls_output_product, &
								ls_header_string, &
								ls_tree_products)

If li_ret <> 0 Then 
	return False
End If

ll_pos = lu_strings.nf_npos(ls_tree_products, '~t', 1, 8)
ll_pos2 = Pos(ls_tree_products, '~t', ll_pos + 1)
ll_pos3 = Pos(ls_tree_products, '~t', ll_pos2 + 1)

If ll_pos3 = 0 Then
	ll_pos3 = Len(ls_tree_products)
End If

ls_temp = Mid(ls_tree_products, ll_pos2 + 1, ll_pos3 - (ll_pos2 + 1))

adc_yield = Double(Mid(ls_tree_products, ll_pos + 1, ll_pos2 - (ll_pos + 1)))
adc_piece_ratio = Double(Mid(ls_tree_products, ll_pos2 + 1, ll_pos3 - (ll_pos2 + 1)))

Return True

end function

public function long wf_getdwchildproductrow (long al_row);Long				ll_rowcount, &
					ll_tree_row, &
					ll_child_row
					
					
ll_rowcount = dw_yields.RowCount()

ll_tree_row = dw_yields.GetItemNumber(al_row, 'tree_row')
ll_child_row = dw_yields.Find('parent_tree_row = ' + String(ll_tree_row), &
			al_row, ll_rowcount + 1)
If ll_child_row < 1 Then
	Return -1
End If

If dw_yields.GetItemString(ll_child_row, 'step_product_ind') = 'S' Then
	ll_tree_row = dw_yields.GetItemNumber(ll_child_row, 'tree_row')
	ll_child_row = dw_yields.Find('parent_tree_row = ' + String(ll_tree_row), &
				al_row, ll_rowcount + 1)
	If ll_child_row < 1 Then
		Return -1
	End If
End If

Return ll_child_row
end function

public function decimal wf_calculate_to_child (long al_row, long al_parent_row);String			ls_output_product

Long				ll_to_characteristic, &
					ll_parent_row

Decimal			ldc_to_parent_yield, &
					ldc_from_parent_yield, &
					ldc_from_yield, &
					ldc_to_yield, &
					ldc_temp
					

ldc_to_yield = 0



return ldc_to_yield
end function

public function long wf_getdwparentproductrow (long al_handle);Long				ll_parent_handle, &
					ll_parent_row, &
					ll_rowcount, &
					ll_row


ll_rowcount = dw_yields.RowCount()

ll_row = dw_yields.Find('tree_row = ' + String(al_handle), 1, dw_yields.RowCount() + 1)
ll_parent_handle = dw_yields.GetItemNumber(ll_row, 'parent_tree_row')
ll_parent_row = dw_yields.Find('tree_row = ' + String(ll_parent_handle), 1, &
		ll_rowcount + 1)
If ll_parent_row < 1 Then Return -1

//If the parent is a step get its parent.
If dw_yields.GetItemString(ll_parent_row, 'step_product_ind') = 'S' Then
	ll_parent_handle = dw_yields.GetItemNumber(ll_parent_row, 'parent_tree_row')
	ll_parent_row = dw_yields.Find('tree_row = ' + String(ll_parent_handle), 1, &
			ll_rowcount + 1)
	If ll_parent_row < 1 Then Return -1
End If

Return ll_parent_row


end function

public function boolean wf_calculate_all_yields (long al_handle);Decimal						ldc_parent_yield, &
								ldc_parent_yield_hold, &
								ldc_child_yield, &
								ldc_from_yield, &
								ldc_piece_ratio, &
								ldc_from_parent_yield, &
								ldc_from_this_yield, &
								ldc_to_parent_yield
								

Long							ll_childhandle, &
								ll_childhandle_2, &
								ll_parent_handle, &
								ll_item_level, &
								ll_pos, &
								ll_child_row, &
								ll_to_characteristic, &
								ll_parent_row, &
								ll_datastore_child_row, &
								ll_datastore_parent_row, &
								ll_this_row
					
String						ls_temp, &
								ls_data, &
								ls_step_prod_ind, &
								ls_product, &
								ls_parent_input_product, &
								ls_parent_output_product, &
								ls_parent_step
					
TreeViewItem				lti_item, &
								lti_temp

u_string_functions		lu_string


//We are calculating the last child to equal the remainder because the children have to
//add up to parent.  I was told this couldn't be accomplished in the equation(sp). REM

If al_handle < 1 Then Return True

tv_tree.GetItem(al_handle,lti_item)
If lti_item.Children Then
	If Not lti_item.Expanded Then
		tv_tree.ExpandItem(al_handle)
	End If
	ll_parent_row = wf_GetdwParentProductRow(al_handle)
	ll_this_row = dw_yields.Find("tree_row = " + String(al_handle), 1, &
				dw_yields.RowCount() + 1)
	If ib_first_compute Then
//Get the first Parent Yields to be used in the Ratio.
		IF Not wf_Get_Yields(ll_parent_row, il_computefromchar, ldc_from_parent_yield, &
				ldc_piece_ratio) Then Return False
//Get the this first Yields to be used in the Ratio.
		If Not wf_Get_Yields(ll_this_row, il_computefromchar, ldc_from_this_yield, &
				ldc_piece_ratio) Then Return False
		ids_compute_datastore.InsertRow(0)
		ids_compute_datastore.SetItem(1, 'mfg_step_sequence', &
				dw_yields.GetItemNumber(ll_this_row, 'mfg_step_sequence'))
		ids_compute_datastore.SetItem(1, 'input_fab_product', &
				dw_yields.GetItemString(ll_this_row, 'input_fab_product'))
		ids_compute_datastore.SetItem(1, 'output_fab_product', &
				dw_yields.GetItemString(ll_this_row, 'output_fab_product'))
		ids_compute_datastore.SetItem(1, 'yield', ldc_from_this_yield)
		ll_datastore_parent_row = 1
		ib_first_compute = False
	Else
		ll_datastore_parent_row = ids_compute_datastore.find("mfg_step_sequence = " + &
				String(dw_yields.GetItemNumber(ll_parent_row, 'mfg_step_sequence')) + &
				" And input_fab_product = '" + &
				dw_yields.GetItemString(ll_parent_row, 'input_fab_product') + &
				"' And output_fab_product = '" + &
				dw_yields.GetItemString(ll_parent_row, 'output_fab_product') + "'", 1, ids_compute_datastore.RowCount() + 1)
		IF Not ll_datastore_parent_row > 0 Then
			MessageBox('Program Error', 'The parent row could not be found in the datastore.  ' + &
			  'Please call Applications.')
			Return False
		End If
		ldc_from_parent_yield = ids_compute_datastore.GetItemDecimal(ll_datastore_parent_row, 'yield')
	End If
	ldc_to_parent_yield = dw_yields.GetItemDecimal(ll_parent_row, 'yield')

//Store off the parent yield so you can replace when you come to a new branch of the tree.
	ldc_parent_yield_hold = ldc_to_parent_yield
	ldc_parent_yield = ldc_parent_yield_hold

	ll_to_characteristic = dw_header.GetItemNumber(1, 'characteristic')
	
	ll_childhandle = tv_tree.FindItem(ChildTreeItem!,al_handle)
	Do While ll_childhandle > 0
//Check if the you are at the last child if so you need to let that child = the remainder.
		ll_childhandle_2 = tv_tree.FindItem(NextTreeItem!,ll_childhandle)
		tv_tree.GetItem(ll_childhandle,lti_temp)
		ls_data = lti_temp.Data
		ll_pos = lu_string.nf_npos(ls_data, '~t', 1, 3) + 1
		If Mid(ls_data, ll_pos, 1) = 'N' or Mid(ls_data, ll_pos, 1) = 'P' Then
			ll_child_row = dw_yields.Find("tree_row = " + String(ll_childhandle), 1, &
					dw_yields.RowCount() + 1)
			ll_datastore_child_row = ids_compute_datastore.find("mfg_step_sequence = " + &
						String(dw_yields.GetItemNumber(ll_child_row, 'mfg_step_sequence')) + &
						" And input_fab_product = '" + &
						dw_yields.GetItemString(ll_child_row, 'input_fab_product') + &
						"' And output_fab_product = '" + &
						dw_yields.GetItemString(ll_child_row, 'output_fab_product') + "'", 1, ids_compute_datastore.RowCount() + 1)							
			If Not ll_datastore_child_row > 0 Then
				If Not wf_Get_Compute_Yields(ll_parent_row, il_computefromchar) Then Return False
				ll_datastore_child_row = ids_compute_datastore.find("mfg_step_sequence = " + &
							String(dw_yields.GetItemNumber(ll_child_row, 'mfg_step_sequence')) + &
							" And input_fab_product = '" + &
							dw_yields.GetItemString(ll_child_row, 'input_fab_product') + &
							"' And output_fab_product = '" + &
							dw_yields.GetItemString(ll_child_row, 'output_fab_product') + "'", 1, ids_compute_datastore.RowCount() + 1)							
				If Not ll_datastore_child_row > 0 Then
					MessageBox('Structure Change', 'The structure has changed since you ' + &
							'last inquired.  Please Reinquire and try your compute again.')
					Return False
				End If
			End If
			IF ll_childhandle_2 > 0 Then
				ldc_from_yield = ids_compute_datastore.GetItemDecimal(ll_datastore_child_row, 'yield')
				If ldc_from_parent_yield > 0 Then
					ldc_child_yield = (ldc_from_yield * ldc_to_parent_yield) / ldc_from_parent_yield
				Else
					ldc_child_yield = 0.00
				End If
				ldc_parent_yield -= Round(ldc_child_yield, 3)
				If ldc_parent_yield < 0.00 Then ldc_parent_yield = 0.00
			Else
				ldc_child_yield = ldc_parent_yield
			End If
			dw_yields.SetItem(ll_child_row, 'yield', ldc_child_yield)
			dw_yields.SetItem(ll_child_row, 'char_key', ll_to_characteristic)
		Else
			ldc_parent_yield = ldc_parent_yield_hold
		End If
		If Not wf_calculate_all_yields(ll_childhandle) Then Return False
		ll_childhandle = ll_childhandle_2
	Loop
	
End if

Return True
end function

public function boolean wf_is_highest_level (long al_dw_row);TreeViewItem	ltvi_item
Long ll_parent_handle, &
		ll_parent_tree_row


ll_parent_tree_row = wf_getparentrow(al_dw_row)
if ll_parent_tree_row > 0 Then
	If dw_yields.GetItemStatus(ll_parent_tree_row, 0, primary!) = DataModified! Then
		Return False
	Else
		Return wf_is_highest_level(ll_parent_tree_row)
	End if
End if

Return True

end function

public function long wf_getparentrow (long al_row);Long				ll_parent_handle, &
					ll_parent_row, &
					ll_rowcount


ll_rowcount = dw_yields.RowCount()

ll_parent_handle = dw_yields.GetItemNumber(al_row, 'parent_tree_row')
ll_parent_row = dw_yields.Find('tree_row = ' + String(ll_parent_handle), 1, &
		ll_rowcount + 1)
If ll_parent_row < 1 Then Return -1

//If the parent is a step get its parent.
If dw_yields.GetItemString(ll_parent_row, 'step_product_ind') = 'S' Then
	ll_parent_handle = dw_yields.GetItemNumber(ll_parent_row, 'parent_tree_row')
	ll_parent_row = dw_yields.Find('tree_row = ' + String(ll_parent_handle), 1, &
			ll_rowcount + 1)
	If ll_parent_row < 1 Then Return -1
End If

Return ll_parent_row


end function

public function boolean wf_build_copy_yield_string (ref string as_copy_yield_string, long al_next_modified, decimal adc_yield, decimal adc_piece_ratio);String			ls_copy_yield_string
Long				ll_parent_tree_row


IF wf_is_highest_level(al_next_modified) Then
	ll_parent_tree_row = wf_getparentrow(al_next_modified)
	if not ll_parent_tree_row > 0 Then
		messagebox("Program Error","Program Error.  This product doesn't have a parent.  Please call Applications.")
		Return False
	end If
	as_copy_yield_string += dw_yields.GetItemString(ll_parent_tree_row, 'input_fab_product') &
			+ "~t" + String(dw_yields.GetItemNumber(ll_parent_tree_row, 'mfg_step_sequence')) &
			+ "~t" + dw_yields.GetItemString(al_next_modified, 'input_fab_product') &
			+ "~t" + dw_yields.GetItemString(al_next_modified, 'output_fab_product') &
			+ "~t" + String(dw_yields.GetItemNumber(al_next_modified, 'mfg_step_sequence')) &
			+ "~t" + dw_yields.GetItemString(al_next_modified, 'fab_type') &
			+ "~r~n"
End If

Return True
end function

public function boolean wf_set_selected_row ();Long					ll_treerow, &
						ll_handle, &
						ll_row

String				ls_visible


If Typeof(GetFocus()) = datawindow! Then
	ll_row = dw_yields.GetRow()
	If ll_row < 1 Then 
		iw_frame.SetMicrohelp('No yield selected')
		Return False
	End If
Else
	ll_handle = tv_tree.FindItem(currenttreeitem!, 0)
	ll_row = dw_yields.Find('tree_row = ' + String(ll_handle), 1, dw_yields.RowCount() + 1)
	If dw_yields.GetItemString(ll_row, 'step_product_ind') = 'S' Then
		iw_frame.SetMicrohelp('Nothing to compute')
		Return False
	End If
	dw_yields.SetRedraw(False)
	ls_visible = dw_yields.object.yield.visible
	dw_yields.object.yield.visible = 1
	dw_yields.ScrollToRow(ll_row)
	dw_yields.object.yield.visible = ls_visible
	dw_yields.SetRedraw(True)
End If
		
il_selected_row = ll_row

Return True
end function

public subroutine wf_popup_menu ();String			ls_copy_ind, &
					ls_record_type, &
					ls_fab_group

m_yieldtree_popup		lm_popmenu


lm_popmenu = Create m_yieldtree_popup

IF dw_header.GetItemString(1, 'effective_pending_ind') = 'E' Then
	lm_popmenu.m_yieldtree.m_computeyields.Enabled = False
End If

ls_record_type = 'YLDSPECS'
ls_fab_group = dw_header.GetItemString(1, 'fab_product_group') 

  SELECT rtrim(tutltypes.type_desc)  
    INTO :ls_copy_ind  
    FROM tutltypes  
   WHERE ( tutltypes.record_type = :ls_record_type ) AND  
         ( tutltypes.type_code = :ls_fab_group )   ;

IF ls_copy_ind <> 'YES' Then
	lm_popmenu.m_yieldtree.m_commitcopyyields.Enabled = False
	lm_popmenu.m_yieldtree.m_printcopyyieldsreport.Enabled = False
End If

lm_popmenu.m_yieldtree.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())


end subroutine

public function boolean wf_collapse (integer al_handle);// This function will dig into the children of the called node, close them then close itself.

Long				ll_NextSibling, &
					ll_NextVisibleHandle

TreeViewItem	ltvi_next

ll_NextSibling = tv_tree.FindItem(NextTreeItem!, al_handle)
ll_NextVisibleHandle = tv_tree.FindItem(NextVisibleTreeItem!, al_handle)

	// ** Debuging code to trace the collapse of the nodes. **
		//		string			ls_data 
		//		TreeViewItem	ltvi_child
		//		
		//		tv_Tree.GetItem(al_handle,ltvi_child)
		//		ls_data = String(ltvi_child.Data)
		//		ls_data = "il_StopNodeIdx Node = " + STRING(il_StopNodeIdx) + "~nll_NextSibling Node = " + STRING(ll_NextSibling) + "~nCollapsing Node = " + string(al_handle) + "~n" + ls_data 
		//		messagebox("wf_collapse",ls_data)
	// ** END Debugging Code **

Do until ll_NextVisibleHandle < 1 &
		or ll_NextVisibleHandle = ll_NextSibling &
			or ll_NextVisibleHandle = il_StopNodeIdx
			
	tv_tree.GetItem(ll_NextVisibleHandle, ltvi_next)
	If ltvi_next.expanded Then
			wf_collapse(ll_NextVisibleHandle)
	End If
	
	ll_NextVisibleHandle = tv_tree.FindItem(NextVisibleTreeItem!, ll_NextVisibleHandle)
Loop 

wf_collapse_yield(al_handle) 		//Remove items from the datawindow
tv_tree.CollapseItem(al_handle)	//Collapse itself

Return True
end function

public function boolean wf_collapse_yield (long al_handle);Boolean				lb_more_rows

Integer				li_step_sequence

String				ls_data, &
						ls_input_product, &
						ls_input_product_state, &
						ls_input_product_status, &
						ls_output_product_state, &
						ls_output_product_status, &
						ls_output_product, &
						ls_find_string
						
Long					ll_count, &
						ll_row, &
						ll_nexthandle, &
						ll_parent_tree_row

TreeViewItem		ltvi_temp

u_string_functions	lu_string


lb_more_rows = True
tv_tree.GetItem(al_handle,ltvi_temp)
ls_data = String(ltvi_temp.Data)

ls_input_product = lu_string.nf_GetToken(ls_data, "~t")
ls_input_product_state = lu_string.nf_GetToken(ls_data, "~t")
ls_input_product_status = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product_state = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product_status = lu_string.nf_GetToken(ls_data, "~t")
li_step_sequence = Integer(lu_string.nf_GetToken(ls_data, "~t"))

ls_find_string = "tree_row = " + String(al_handle)
ll_count = dw_yields.Find(ls_find_string, 1, dw_yields.RowCount() + 1)
IF ll_count < 1 Then 
	MessageBox('Programming Error', "It didn't find a matching row.  Please call applications." + &
					'Find String = ' + ls_find_string)
	Return False
End If

If dw_yields.GetItemString(ll_count, 'step_product_ind') = 'P' Then
	ll_nexthandle = tv_tree.FindItem(ChildTreeItem!, al_handle)
	ll_row = ll_count + 1
	Do While ll_nexthandle > 0 
		dw_yields.DeleteRow(ll_row)
		ll_nexthandle = tv_tree.FindItem(NextTreeItem!, ll_nexthandle)
	Loop
Else
	ll_count ++
	if ll_count <= dw_yields.RowCount() then
		ll_parent_tree_row = dw_yields.GetItemNumber(ll_count, 'parent_tree_row')
		
		Do While ll_parent_tree_row = al_handle &
					and lb_more_rows
			dw_yields.RowsMove(ll_count, ll_count, Primary!, dw_yields, 10000, Filter!)
			If ll_count <= dw_yields.RowCount() Then
				ll_parent_tree_row = dw_yields.GetItemNumber(ll_count, 'parent_tree_row')
			Else
				lb_more_rows = False
			End If
		Loop
	end if
End If

Return True

end function

public function boolean wf_expand_yields (long al_handle);Boolean				lb_more_rows

Integer				li_step_sequence

String				ls_data, &
						ls_input_product, &
						ls_output_product, &
						ls_input_product_state, &
						ls_input_product_status, &
						ls_output_product_state, &
						ls_output_product_status, &
						ls_filter_input_product, &
						ls_find_string, &
						ls_parent
						
Long					ll_count, &
						ll_insert_row, &
						ll_row, &
						ll_nexthandle, &
						ll_parent_tree_row

TreeViewItem		ltvi_temp

u_string_functions	lu_string


lb_more_rows = True

tv_tree.GetItem(al_handle,ltvi_temp)
ls_data = String(ltvi_temp.Data)

ls_input_product = lu_string.nf_GetToken(ls_data, "~t")
ls_input_product_state = lu_string.nf_GetToken(ls_data, "~t")
ls_input_product_status = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product_state = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product_status = lu_string.nf_GetToken(ls_data, "~t")
li_step_sequence = Integer(lu_string.nf_GetToken(ls_data, "~t"))
ls_Parent = lu_string.nf_GetToken(ls_data, "~t")

ls_find_string = "tree_row = " + String(al_handle)

ll_insert_row = dw_yields.Find(ls_find_string, 1, dw_yields.RowCount() + 1)

IF ll_insert_row < 1 Then 
	MessageBox('Programming Error In Expand Yields', &
			"It didn't find a matching row.  Please call applications.~r~n" + &
			'handle = ' + String(al_handle))
	Return False
End If

If dw_yields.GetItemString(ll_insert_row, 'step_product_ind') = 'P' Then
	ll_row = ll_insert_row + 1
	ll_nexthandle = tv_tree.FindItem(ChildTreeItem!, al_handle)
	Do While ll_nexthandle > 0 
		dw_yields.InsertRow(ll_row)
		dw_yields.SetItem(ll_row, 'step_product_ind', 'S')
		dw_yields.SetItem(ll_row, 'tree_row', ll_nexthandle)
		dw_yields.SetItem(ll_row, 'parent_tree_row', al_handle)
		dw_yields.SetItemStatus(ll_row, 0, Primary!, NotModified!)
		ll_row ++
		ll_nexthandle = tv_tree.FindItem(NextTreeItem!, ll_nexthandle)
	Loop
Else
	For ll_count = 1 to dw_yields.FilteredCount()
			ll_parent_tree_row = dw_yields.GetItemNumber(ll_count, 'parent_tree_row', Filter!, False)
		IF ll_parent_tree_row = al_handle Then
			Do While ll_parent_tree_row = al_handle &
					and lb_more_rows
				ll_insert_row ++
				dw_yields.RowsMove(ll_count, ll_count, Filter!, dw_yields, ll_insert_row, Primary!)
				If ll_count <= dw_yields.FilteredCount() Then
					ll_parent_tree_row = dw_yields.GetItemNumber(ll_count, 'parent_tree_row', Filter!, False)
				Else
					lb_more_rows = False
				End If
			Loop
			Exit
		End If
	Next
End If

Return True

end function

public function boolean wf_get_compute_yields (long al_row, long al_characteristic);Long						ll_row_count
							
String					ls_header_string, &
							ls_output_product, &
							ls_output_product_state, &
							ls_output_product_status, &
							ls_tree_products, &
							ls_temp
Integer					li_ret

u_string_functions	lu_strings


ls_output_product = dw_yields.GetItemString(al_row, 'output_fab_product')
ls_output_product_state = dw_yields.GetItemString(al_row, 'output_product_state')
ls_output_product_status = dw_yields.GetItemString(al_row, 'output_product_status')
ls_header_string = '~t~t' + &
							String(al_characteristic) + '~t' + &
							String(dw_header.GetItemDate(1, 'effective_begin_date'), 'yyyy-mm-dd') + '~t' + &
							String(dw_header.GetItemDate(1, 'effective_end_date'), 'yyyy-mm-dd') + '~t' + &
							dw_header.GetItemString(1, 'effective_pending_ind') + '~t' + &
							ls_output_product_state + '~t' + ls_output_product_status + '~t' + &
							'Y'

istr_error_info.se_event_name = "get yields"
	
//li_ret = iu_pas203.nf_pasp67br_inq_pas_yields_tree(istr_error_info, &
//								ls_output_product, &
//								ls_header_string, &
//								ls_tree_products)

li_ret = iu_ws_pas3.uf_pasp67fr(istr_error_info, &
								ls_output_product, &
								ls_header_string, &
								ls_tree_products)

if li_ret <> 0 And Left(istr_error_info.se_message, 6) = 'PAS008' Then
	return False
End If

If li_ret <> 0 Then 
	return False
End If

ll_row_count = ids_compute_datastore.ImportString(ls_tree_products)

Return True

end function

public subroutine wf_post_update ();Integer	li_ret
Long		ll_handle, &
			ll_next_modified, &
			ll_next_tree_handle, &
			ll_characteristic
String	ls_input_header, &
			ls_input_String, &
			ls_parent, &
			ls_product, &
			ls_message, &
			ls_copy_yield_string, &
			ls_record_type, &
			ls_copy_ind, &
			ls_fab_group
Decimal	ldc_difference, &
			ldc_yield, &
			ldc_piece_ratio

ll_next_modified = dw_yields.GetNextModified(0, Primary!)
If ll_next_modified < 1 Then
	iw_frame.SetMicroHelp('No Update Necessary')
	ib_expand_all = False
	tv_tree.Event ue_post_expanded()
	This.SetRedraw(True)
	ib_update_successful = True
	Return
End If

ls_record_type = 'YLDSPECS'
ls_fab_group = dw_header.GetItemString(1, 'fab_product_group') 

  SELECT rtrim(tutltypes.type_desc)
    INTO :ls_copy_ind  
    FROM tutltypes  
   WHERE ( tutltypes.record_type = :ls_record_type ) AND  
         ( tutltypes.type_code = :ls_fab_group )   ;


ll_characteristic = dw_header.GetItemNumber(1, 'characteristic')
Do
	ls_product = dw_yields.GetItemString(ll_next_modified, 'output_fab_product')
	If Not This.wf_check_against_parent_yield(ll_next_modified, ldc_difference, ls_parent) Then
		MessageBox("Error in check against parent", "Please call Applications.")
		dw_yields.SetFocus()
		dw_yields.ScrollToRow(ll_next_modified)
		This.wf_scroll_tree(ll_next_modified)
		dw_yields.SetColumn('yield')
		dw_yields.SelectText(1,100)
		ib_expand_all = False
		tv_tree.Event ue_post_expanded()
		This.SetRedraw(True)
		ib_update_successful = False
		Return
	Else 
		If ldc_difference <> 0.00 Then
			ls_message = "Yield Violation ~t The difference between " + Trim(ls_product) + &
					"'s yield and " + Trim(ls_parent) + "'s yield is " + String(ldc_difference) + &
					"." + "~t" + is_SaveAnyway
			OpenWithParm(w_update_message_box, ls_message)
			If Message.StringParm <> 'SaveAnyway' Then
				dw_yields.SetFocus()
				dw_yields.ScrollToRow(ll_next_modified)
				This.wf_scroll_tree(ll_next_modified)
				dw_yields.SetColumn('yield')
				dw_yields.SelectText(1,100)
				ib_expand_all = False
				tv_tree.Event ue_post_expanded()
				This.SetRedraw(True)
				ib_update_successful = False
				Return
			End If
		End If	
	End If	
	This.wf_check_against_children_yield(ll_next_modified, ldc_difference)
		If ldc_difference <> 0.00 Then
		ls_message = "Yield Violation ~t The difference between " + Trim(ls_product) + &
					"'s yield and its children is " + String(ldc_difference) + &
					"." + "~t" + is_SaveAnyway
		OpenWithParm(w_update_message_box, ls_message)
		If Message.StringParm <> 'SaveAnyway' Then
			dw_yields.SetFocus()
			dw_yields.ScrollToRow(ll_next_modified)
			This.wf_scroll_tree(ll_next_modified)
			dw_yields.SetColumn('yield')
			dw_yields.SelectText(1,100)
			ib_expand_all = False
			tv_tree.Event ue_post_expanded()
			This.SetRedraw(True)
			ib_update_successful = False
			Return
		End If
	End If

	If IsNull(dw_yields.GetItemNumber(ll_next_modified, 'yield')) Then
		ldc_yield = 0.00
	Else 
		ldc_yield = dw_yields.GetItemDecimal(ll_next_modified, 'yield')
	End If

	If IsNull(dw_yields.GetItemNumber(ll_next_modified, 'piece_ratio')) Then
		ldc_yield = 0.00
	Else 
		ldc_piece_ratio = dw_yields.GetItemDecimal(ll_next_modified, 'piece_ratio')
	End If

	
	ls_input_string += dw_yields.GetItemString(ll_next_modified, 'input_fab_product') &
				+ "~t" + dw_yields.GetItemString(ll_next_modified, 'output_fab_product') &
				+ "~t" + String(dw_yields.GetItemNumber(ll_next_modified, 'mfg_step_sequence')) &
				+ "~t" + String(dw_yields.GetItemNumber(ll_next_modified, 'char_key')) &
				+ "~t" + String(ldc_yield) &
				+ "~t" + String(ldc_piece_ratio) &
				+ "~t" + dw_yields.GetItemString(ll_next_modified, 'input_product_state') &
				+ "~t" + dw_yields.GetItemString(ll_next_modified, 'input_product_status') &				
				+ "~t" + dw_yields.GetItemString(ll_next_modified, 'output_product_state') &
				+ "~t" + dw_yields.GetItemString(ll_next_modified, 'output_product_status') &				
				+ "~t" + dw_yields.GetItemString(ll_next_modified, 'delete_ind') &
				+ "~r~n"

	IF ll_characteristic = 0 and ls_copy_ind = 'YES' Then
		If Not wf_build_copy_yield_string(ls_copy_yield_string, &
													ll_next_modified, &
													ldc_yield, &
													ldc_piece_ratio) Then Return
	End If

	ll_next_modified = dw_yields.GetNextModified(ll_next_modified, Primary!)
	
Loop While ll_next_modified > 0

ls_input_header = String(dw_header.GetItemNumber(1, 'characteristic')) + "~t" + &
						String(dw_header.GetItemDate(1, 'update_begin_date'), 'yyyy-mm-dd') + "~t" + &
						String(dw_header.GetItemDate(1, 'update_end_date'), 'yyyy-mm-dd')


//li_ret = iu_pas203.nf_pasp68br_upd_pas_yields_tree(istr_error_info, &
//														ls_input_header, &
//														ls_input_string)

li_ret = iu_ws_pas3.uf_pasp68fr(istr_error_info, &
											ls_input_header, &
											ls_input_string)

If li_ret <> 0 Then
	ib_expand_all = False
	tv_tree.SetRedraw(True)
	dw_yields.SetRedraw(True)
	This.wf_scroll_tree(il_current_row)
	This.SetRedraw(True)
	ib_update_successful = False
	Return
End If

ib_expand_all = False
tv_tree.SetRedraw(True)
dw_yields.SetRedraw(True)
//This.wf_scroll_tree(il_current_row)
This.SetRedraw(True)

dw_yields.ResetUpdate()
iw_frame.SetMicroHelp("Modification Successful")

ib_inquire_required = True
ib_update_successful = True 

ls_input_header = Mid(ls_input_header, Pos(ls_input_header, '~t') + 1)

IF ll_characteristic = 0 and ls_copy_ind = 'YES' Then
//	iu_pas201.nf_pasp98br_upd_automate_cpy_yld(istr_error_info, &
//															ls_input_header, &
//															ls_copy_yield_string)
	iu_ws_pas3.uf_pasp98fr(istr_error_info, &
									ls_input_header, &
									ls_copy_yield_string)
End If

return

end subroutine

public function boolean wf_retrieve ();Boolean	lb_first_time = True

Int		li_mfg_step_sequence, &
			li_ret
			
Long		ll_row, &
			ll_yield_row, &
			ll_row_count, &
			ll_tvroot, &
			ll_counter, &
			ll_find_row, &
			ll_tvnextstep = 0, &
			ll_tvnextproduct = 0, &
			ll_treerow

TreeViewItem	ltvi_temp

String	ls_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_input_product_state, &
			ls_input_product_status, &
			ls_output_product_state, &
			ls_output_product_status, &
			ls_product_type, &
			ls_input_product, &
			ls_output_product, &
			ls_header_string, &
			ls_tree_products, &
			ls_product_descr, &
			ls_parent_product, &
			ls_parent_step
			
DataWindowChild		ldwc_parent_product

u_string_functions		lu_string


IF Not Super::wf_retrieve() Then Return False

This.SetRedraw(False)

If dw_header.GetItemString(1, 'effective_pending_ind') = 'E' Then
	dw_yields.Object.yield.Background.color = '12632256'
	dw_yields.Object.piece_ratio.Background.color = '12632256'
	dw_yields.Object.yield.protect = '1'
	dw_yields.Object.piece_ratio.protect = '1'
	iw_frame.im_menu.mf_Disable('m_save')
	iw_frame.im_menu.mf_Disable('m_deleterow')
Else
	dw_yields.Object.yield.Background.color = is_background_color
	dw_yields.Object.piece_ratio.Background.color = is_background_color
	dw_yields.Object.yield.protect = is_protect
	dw_yields.Object.piece_ratio.protect = is_protect
	iw_frame.im_menu.mf_Enable('m_save')
	iw_frame.im_menu.mf_Enable('m_deleterow')
End If

dw_header.GetChild('parent_fab_product', ldwc_parent_product)
ldwc_parent_product.Reset()
ldwc_parent_product.ImportString(is_parent_string)

ls_parent_product = dw_header.GetItemString(1, 'parent_fab_product')
ls_parent_product = lu_string.nf_GetToken(ls_parent_product, ' ')

ib_first_time = True
il_row = 0
SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

ll_tvroot = 0
Do while ll_tvroot <> -1
	ll_tvroot = tv_tree.FindItem(RootTreeItem!,0)
	If ll_tvroot <> -1 Then tv_tree.DeleteItem(ll_tvroot)
Loop

ids_tree.Reset()
dw_yields.Reset()
If Not IsValid(ids_expand_datastore) Then
	ids_expand_datastore = create Datastore
	ids_expand_datastore.DataObject = 'd_yields_tree'
Else
	ids_expand_datastore.Reset()
End If

ls_product_code = dw_fab_product_code.uf_get_product_code()
ls_product_state = dw_fab_product_code.uf_get_product_state()
ls_product_status = dw_fab_product_code.uf_get_product_status()
ls_header_string = ls_parent_product + '~t' + &
							String(ii_parent_step) + '~t' + &
							String(dw_header.GetItemNumber(1, 'characteristic')) + '~t' + &
							String(dw_header.GetItemDate(1, 'effective_begin_date'), 'yyyy-mm-dd') + '~t' + &
							String(dw_header.GetItemDate(1, 'effective_end_date'), 'yyyy-mm-dd') + '~t' + &
							dw_header.GetItemString(1, 'effective_pending_ind') + '~t' + &
						  	ls_product_state + '~t' + ls_product_status + '~t' + &	
							'I'
					
istr_error_info.se_event_name = "wf_retrieve"

//li_ret = iu_pas203.nf_pasp67br_inq_pas_yields_tree(istr_error_info, &
//								ls_product_code, &
//								ls_header_string, &
//								ls_tree_products)

li_ret = iu_ws_pas3.uf_pasp67fr(istr_error_info, &
								ls_product_code, &
								ls_header_string, &
								ls_tree_products)

If li_ret <> 0 And Left(istr_error_info.se_message, 6) <> 'PAS008' Then
	This.PostEvent("ue_query")
	This.SetRedraw(True)
	return True
End if
ls_header_string = Trim(ls_header_string)
dw_header.SetItem(1, "fab_product_group", ls_header_string)
ll_row_count = dw_yields.ImportString(ls_tree_products)
If ll_row_count < 1 Then 
	This.SetRedraw(True)
	Return False
End If

ls_input_product = dw_yields.GetItemString(1,"input_fab_product")
ls_output_product = dw_yields.GetItemString(1,"output_fab_product")
ls_product_type = dw_yields.GetItemString(1,"fab_type")
ls_input_product_state = dw_yields.GetItemString(1,"input_product_state")
ls_input_product_status = dw_yields.GetItemString(1,"input_product_status")
ls_output_product_state = dw_yields.GetItemString(1,"output_product_state")
ls_output_product_status = dw_yields.GetItemString(1,"output_product_status")

ltvi_temp.Data = ls_input_product + "~t" + &
					ls_output_product + "~t" + &
					String(dw_yields.GetItemNumber(1,"mfg_step_sequence")) + "~tP"

ltvi_temp.Label = trim(ls_output_product + " " + &
	ls_output_product_state + " " + &
	ls_output_product_status + " " + &	
	ls_product_type + " " + &
	dw_yields.GetItemString(1,"fab_product_description") + &
	Fill(" ", 100))

ltvi_temp.PictureIndex = 1
ltvi_temp.SelectedPictureIndex = 1
ltvi_temp.Children = True

If ls_product_type = 'BY' &
		or dw_yields.GetItemString(1, 'sku_product_ind') = 'Y' Then
	ltvi_temp.Children = False
	ll_tvroot = tv_tree.InsertItemFirst(0,ltvi_temp)

	dw_yields.SetItem(1,"tree_row", ll_tvroot)
	dw_yields.SetItem(1,"Parent_tree_row", 0)
	dw_yields.ResetUpdate()
	This.SetRedraw(True)
	This.SetMicroHelp("Ready")
	Return True
End If

ltvi_temp.Children = True
ll_tvroot = tv_tree.InsertItemFirst(0,ltvi_temp)
dw_yields.SetItem(1,"tree_row", ll_tvroot)
dw_yields.SetItem(1,"Parent_tree_row", 0)

il_item_count = 1

If Not IsNull(ls_tree_products) Then
	For ll_counter = 2 to ll_row_count		
		il_item_count ++
		ltvi_temp.Children = True
		ls_input_product = dw_yields.GetItemString(ll_counter,"input_fab_product")
		ls_input_product_state = dw_yields.GetItemString(ll_counter,"input_product_state")
		ls_input_product_status = dw_yields.GetItemString(ll_counter,"input_product_status")		
		ls_product_descr = dw_yields.GetItemString(ll_counter,"fab_product_description")
		ls_output_product = dw_yields.GetItemString(ll_counter,"output_fab_product")
		ls_output_product_state = dw_yields.GetItemString(ll_counter,"output_product_state")
		ls_output_product_status = dw_yields.GetItemString(ll_counter,"output_product_status")		
		li_mfg_step_sequence = dw_yields.GetItemNumber(ll_counter,"mfg_step_sequence")
		If dw_yields.GetItemString(ll_counter, 'step_product_ind') = 'S' Then
			ll_tvnextproduct = 0
			ltvi_temp.Label = trim(ls_product_descr)
			ltvi_temp.PictureIndex = 2
			ltvi_temp.SelectedPictureIndex = 2
			ltvi_temp.Data = ls_input_product + "~t" + &
									ls_input_product_state + "~t" + &
									ls_input_product_status + "~t" + &									
									ls_output_product + "~t" + &
									ls_output_product_state + "~t" + &
									ls_output_product_status + "~t" + &
									String(li_mfg_step_sequence) + "~tS"
			If ll_tvnextstep = 0 Then
				ll_tvnextstep = tv_tree.InsertItemFirst(ll_tvroot,ltvi_temp)
			Else
				ll_tvnextstep = tv_tree.InsertItem(ll_tvroot,ll_tvnextstep,ltvi_temp)
			End If
			dw_yields.SetItem(ll_counter,"tree_row", ll_tvnextstep)
			dw_yields.SetItem(ll_counter,"parent_tree_row", ll_tvroot)
		Else
			ls_product_type = dw_yields.GetItemString(ll_counter,"fab_type")
			ltvi_temp.Label = trim(ls_output_product + " " + &
				ls_output_product_state + " " + ls_output_product_status + " " + &
				ls_product_type + " " + &
				ls_product_descr + fill(' ', 150))
			ltvi_temp.Data = ls_input_product + "~t" + &
									ls_input_product_state + "~t" + &
									ls_input_product_status + "~t" + &
									ls_output_product + "~t" + &
									ls_output_product_state + "~t" + &
									ls_output_product_status + "~t" + &
									String(li_mfg_step_sequence) + "~tN"
			ltvi_temp.PictureIndex = 1
			ltvi_temp.SelectedPictureIndex = 1
			If ls_product_type = 'BY' &
					or dw_yields.GetItemString(ll_counter, 'sku_product_ind') = 'Y' Then
				ltvi_temp.Children = False
			End If
			If ll_tvnextstep = 0 Then
				ll_tvnextstep = tv_tree.InsertItemFirst(ll_tvroot,ltvi_temp)
				dw_yields.SetItem(ll_counter,"tree_row", ll_tvnextstep)
				dw_yields.SetItem(ll_counter,"parent_tree_row", ll_tvroot)
			Else
				If ll_tvnextproduct = 0 then
					ll_tvnextproduct = tv_tree.InsertItemFirst(ll_tvnextstep,ltvi_temp)
				Else
					ll_tvnextproduct = tv_tree.InsertItem(ll_tvnextstep,ll_tvnextproduct,ltvi_temp)
				End If
			End If
			dw_yields.SetItem(ll_counter,"tree_row", ll_tvnextproduct)
			dw_yields.SetItem(ll_counter,"parent_tree_row", ll_tvnextstep)
			dw_yields.RowsMove(ll_counter, ll_counter, Primary!, dw_yields, 10000, Filter!)
			ll_counter --
			ll_row_count --
		End If
	Next
End If
tv_tree.ExpandItem(ll_tvroot)
dw_yields.ResetUpdate()

tv_tree.SetFocus()

This.SetRedraw(True)
This.SetMicroHelp("Ready")


return true

end function

public function boolean wf_scroll_tree (long al_first_row_on_page);Long					ll_tree_row

ll_tree_row = dw_yields.GetItemNumber(al_first_row_on_page, 'tree_row')
tv_tree.SetFirstVisible(ll_tree_row)

Return True
end function

on w_pas_yields_tree.create
int iCurrent
call super::create
this.dw_fab_product_code=create dw_fab_product_code
this.tv_tree=create tv_tree
this.dw_header=create dw_header
this.dw_yields=create dw_yields
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fab_product_code
this.Control[iCurrent+2]=this.tv_tree
this.Control[iCurrent+3]=this.dw_header
this.Control[iCurrent+4]=this.dw_yields
end on

on w_pas_yields_tree.destroy
call super::destroy
destroy(this.dw_fab_product_code)
destroy(this.tv_tree)
destroy(this.dw_header)
destroy(this.dw_yields)
end on

event close;call super::close;If IsValid(iu_pas203) Then Destroy iu_pas203
If IsValid(ids_tree) Then Destroy ids_tree
If IsValid(iu_ws_pas3) Then Destroy iu_ws_pas3
end event

event ue_postopen;call super::ue_postopen;String			ls_char, &
					ls_group_id, &
					ls_modify_auth

DataWindowChild	ldwc_char


ib_inquire_required = True

ls_group_id = iw_frame.iu_netwise_data.is_groupid

  SELECT group_profile.modify_auth
    INTO :ls_modify_auth 
    FROM group_profile  
   WHERE ( group_profile.appid = 'PRD' ) AND  
         ( group_profile.group_id = :ls_group_id ) AND  
         ( group_profile.window_name = 'W_UPDATE_MESSAGE_BOX' ) AND  
         ( group_profile.menuid = 'W_PAS_YIELD_TREE' )   ;

If ls_modify_auth = 'Y' Then
	is_SaveAnyway = 'Y'
Else
	is_SaveAnyway ='N'
End If

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Yields Tree"
istr_error_info.se_user_id 		= sqlca.userid

is_inquire_window_name = 'w_pas_yields_tree_inq'

iu_pas203 = Create u_pas203
iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3

ids_tree = Create DataStore
ids_tree.DataObject = 'd_yields_tree'

istr_error_info.se_event_name = "ue_postopen"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

//If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
If iu_ws_pas3.uf_pasp61fr(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_header.GetChild("characteristic", ldwc_char)
ldwc_char.ImportString(ls_char)

dw_yields.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)

wf_retrieve()
end event

event ue_get_data;call super::ue_get_data;DataWindowChild		ldwc_char

Choose Case as_value
	Case 'fab_product_code'
		Message.StringParm = dw_fab_product_code.uf_get_product_code()
	Case 'fab_product_desc'
		Message.StringParm = dw_fab_product_code.uf_get_product_desc()
	Case 'product_state'
		Message.StringParm = dw_fab_product_code.uf_get_product_state()	
	Case 'product_status'
		Message.StringParm = dw_fab_product_code.uf_get_product_status()		
	Case 'product_state_description'
		Message.StringParm = dw_fab_product_code.GetItemString(1, 'product_state_description')
	Case 'product_status_description'
		Message.StringParm = dw_fab_product_code.GetItemString(1, 'product_status_description')		
	Case 'compute_product_code'
		Message.StringParm = dw_yields.GetItemString(il_selected_row, 'output_fab_product')
	Case 'compute_product_desc'
		Message.StringParm = dw_yields.GetItemString(il_selected_row, 'fab_product_description')
	Case 'parent_fab_product'
		Message.StringParm = dw_header.GetItemString(1, 'parent_fab_product')
	Case 'effective_begin_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'effective_begin_date'), 'mm/dd/yyyy')
	Case 'effective_end_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'effective_end_date'), 'mm/dd/yyyy')
	Case 'update_begin_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'update_begin_date'), 'mm/dd/yyyy')
	Case 'update_end_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'update_end_date'), 'mm/dd/yyyy')
	Case 'characteristics'
		Message.StringParm = String(dw_header.GetItemNumber(1, 'characteristic'))
	Case 'effective_pending_ind'
		Message.StringParm = dw_header.GetItemString(1, 'effective_pending_ind')
	Case 'parent_string'
		Message.StringParm = is_parent_string
	Case 'dates_string'
		Message.StringParm = is_dates_string
	Case 'dwc_char'
		dw_header.GetChild('characteristic', ldwc_char)
		Message.PowerObjectParm = ldwc_char
	Case 'ib_inquire_required'
		Message.StringParm = String(ib_inquire_required)
End Choose

end event

event ue_set_data;call super::ue_set_data;u_conversion_functions		lu_conversions

Choose Case as_data_item
	Case 'fab_product_code'
		dw_fab_product_code.uf_set_product_code(as_value)	
	Case 'product_state'
		dw_fab_product_code.uf_set_product_state(as_value)	
	Case 'product_status'
		dw_fab_product_code.uf_set_product_status(as_value)	
	Case 'product_state_description'
		dw_fab_product_code.SetItem(1, 'product_state_description', as_value)
	Case 'product_status_description'
		dw_fab_product_code.SetItem(1, 'product_status_description', as_value)
	Case 'fab_product_desc'
		dw_fab_product_code.uf_set_product_desc(as_value)
	Case 'parent_fab_product'
		dw_header.SetItem(1, 'parent_fab_product', as_value)
	Case 'ii_step'
		ii_parent_step = Integer(as_value)
	Case 'effective_begin_date'
		dw_header.SetItem(1, 'effective_begin_date', Date(as_value))
	Case 'effective_end_date'
		dw_header.SetItem(1, 'effective_end_date', Date(as_value))
	Case 'update_begin_date'
		dw_header.SetItem(1, 'update_begin_date', Date(as_value))
	Case 'update_end_date'
		dw_header.SetItem(1, 'update_end_date', Date(as_value))
	Case 'characteristics'
		dw_header.SetItem(1, 'characteristic', Integer(as_value))
	Case 'effective_pending_ind'
		dw_header.SetItem(1, 'effective_pending_ind', as_value)
	Case 'parent_string'
		is_parent_string = as_value
	Case 'dates_string'
		is_dates_string = as_value
	Case 'ComputeFromChar'
		il_computefromchar = Long(as_value)
	Case 'ib_inquire_required'
		ib_inquire_required = lu_conversions.nf_Boolean(as_value)
End Choose

end event

event resize;call super::resize;constant integer li_MinWidth = 2903 
constant integer li_MinHeigth = 1568

integer 				li_tree_x, &
						li_dw_x, &
						li_dw_yields_width, li_diff
constant integer li_y		= 121

long				ll_row

//// pjm 09/15/2014 - I got the resizing working

//// scs 11/04/2014 - Fixed resize, tree view is sized such that the scroll bar is hidden behind
//                    the dw_yields datawindow.
//IF newwidth > li_MinWidth AND ii_old_width > 0 THEN 
IF newwidth > li_MinWidth THEN	
	dw_yields.width = 1152
	dw_yields.x = newwidth - dw_yields.width 
	tv_tree.width = newwidth - dw_yields.width + 75
END IF

IF newheight > li_MinHeigth THEN 
//	tv_tree.height = newheight - tv_tree.y - 108
//	dw_yields.height = newheight - dw_yields.y - 108
	tv_tree.height = newheight - tv_tree.y
	dw_yields.height = newheight - dw_yields.y
END IF
//ii_old_width = newwidth

ll_row = Long(dw_yields.Object.DataWindow.FirstRowOnPage)

If ll_row > 0 Then
	wf_scroll_tree(ll_row)
End If

end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_enable('m_computeyields')

If dw_header.GetItemString(1, 'effective_pending_ind') = 'P' Then
	iw_frame.im_menu.mf_Enable('m_save')
	iw_frame.im_menu.mf_Enable('m_deleterow')
Else
	iw_frame.im_menu.mf_Disable('m_save')
	iw_frame.im_menu.mf_Disable('m_delete_row')
End If
	

end event

event deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

iw_frame.im_menu.mf_Disable('m_computeyields')

end event

event ue_fileprint;call super::ue_fileprint;DataStore		lds_print

DataWindowChild	ldwc_temp, &
						ldwc_print_temp

Decimal			ldc_yields, &
					ldc_ratio
					
String			ls_step_product_ind, &
					ls_temp

Long				ll_count, &
					ll_characteristics, &
					ll_tree_row, &
					ll_findrow

TreeViewItem	lti_item


If Not ib_print_ok Then Return

lds_print = Create u_print_datastore
lds_print.DataObject = 'd_yields_tree_prt'

dw_yields.GetChild('char_key', ldwc_temp)
lds_print.GetChild('characteristics', ldwc_print_temp)
ldwc_temp.ShareData(ldwc_print_temp)

For ll_count = 1 to dw_yields.RowCount() 
	lds_print.InsertRow(0)
	ls_step_product_ind = dw_yields.GetItemString(ll_count, 'step_product_ind')
	If ls_step_product_ind = 'P' Then
		ldc_yields = dw_yields.GetItemdecimal(ll_count, 'yield')
		lds_print.SetItem(ll_count, 'yields', ldc_yields)
		ldc_ratio = dw_yields.GetItemdecimal(ll_count, 'piece_ratio')
		lds_print.SetItem(ll_count, 'ratio', ldc_ratio)
		ll_characteristics = dw_yields.GetItemNumber(ll_count, 'char_key')
		lds_print.SetItem(ll_count, 'characteristics', ll_characteristics)
	End If
	ll_tree_row = dw_yields.GetItemNumber(ll_count, 'tree_row')
	tv_tree.GetItem(ll_tree_row, lti_item)
	lds_print.SetItem(ll_count, 'tree_label', lti_item.label)
	lds_print.SetItem(ll_count, 'level_number', lti_item.Level)
Next

ls_temp = dw_fab_product_code.uf_get_product_code( )
lds_print.object.product_code_t.text = ls_temp

ls_temp = dw_fab_product_code.uf_get_product_desc( )
lds_print.object.product_descr_t.text = ls_temp

ls_temp = dw_fab_product_code.uf_get_product_state( )
lds_print.object.product_state_t.text = ls_temp

ls_temp = dw_fab_product_code.uf_get_product_status( )
lds_print.object.product_status_t.text = ls_temp

ls_temp = dw_fab_product_code.uf_get_product_state_desc( )
lds_print.object.product_state_descr_t.text = ls_temp

ls_temp = dw_fab_product_code.uf_get_product_status_desc( )
lds_print.object.product_status_descr_t.text = ls_temp

ls_temp = dw_header.GetItemString(1, 'parent_fab_product')
lds_print.object.parent_product_code_t.text = ls_temp

dw_header.GetChild('parent_fab_product_descr', ldwc_temp)
ll_findrow = ldwc_temp.Find('parent_product = "' + ls_temp + '"', 1, ldwc_temp.RowCount() + 1)
If ll_findrow > 0 Then
	ls_temp = ldwc_temp.GetItemString(ll_findrow, 'parent_product_descr')
	lds_print.object.parent_product_descr_t.text = ls_temp
	ls_temp = ldwc_temp.GetItemString(ll_findrow, 'mfg_step_descr')
	lds_print.object.parent_mfg_step_t.text = ls_temp
Else
	lds_print.object.parent_product_descr_t.text = ' '
	lds_print.object.parent_mfg_step_t.text = ' '
End If

ls_temp = String(dw_header.GetItemNumber(1, 'characteristic'))
dw_header.GetChild('characteristic', ldwc_temp)
ll_findrow = ldwc_temp.Find('char_key = ' + ls_temp, 1, ldwc_temp.RowCount() + 1)
If ll_findrow > 0 Then
	ls_temp = ldwc_temp.GetItemString(ll_findrow, 'display_value')
	lds_print.object.characteristic_t.text = ls_temp
Else
	lds_print.object.characteristic_t.text = ' '
End If

ls_temp = String(dw_header.GetItemDate(1, 'effective_begin_date'), 'mm/dd/yyyy')
lds_print.object.effective_begin_date_t.text = ls_temp

ls_temp = String(dw_header.GetItemDate(1, 'effective_end_date'), 'mm/dd/yyyy')
lds_print.object.effective_end_date_t.text = ls_temp

ls_temp = String(dw_header.GetItemDate(1, 'update_begin_date'), 'mm/dd/yyyy')
lds_print.object.update_begin_date_t.text = ls_temp

ls_temp = String(dw_header.GetItemDate(1, 'update_end_date'), 'mm/dd/yyyy')
lds_print.object.update_end_date_t.text = ls_temp


lds_print.print()
end event

event open;call super::open;ib_compute_yields = False
// pjm 09/15/2014 added for resizing
ii_old_width = this.Width
end event

type dw_fab_product_code from u_fab_product_code within w_pas_yields_tree
integer x = 27
integer y = 4
integer height = 272
integer taborder = 30
end type

event constructor;call super::constructor;this.uf_disable( )

end event

type tv_tree from treeview within w_pas_yields_tree
event ue_vscroll pbm_vscroll
event ue_post_expanded ( )
event ue_post_scroll ( )
integer x = 9
integer y = 548
integer width = 2450
integer height = 972
integer taborder = 30
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean linesatroot = true
string picturename[] = {"dotsgray.bmp","step!","Custom028!","Custom029!","Custom030!","Custom031!"}
integer pictureheight = 16
long picturemaskcolor = 553648127
integer statepictureheight = 32
long statepicturemaskcolor = 553648127
end type

event ue_vscroll;//** IBDKEEM ** 04/04/2002 ***********************
//Tried to get this working for the PB8 Migration.
//Never worked in the 6.5 build so i gave up.
//i was tired to trying to fix Muckey's Code.
//************************************************

//if not ib_StopScroll then
//	This.PostEvent('ue_post_scroll')
//end if

end event

event ue_post_expanded();
IF Not ib_compute_yields Then
	This.SetRedraw(True)
	dw_yields.SetRedraw(True)
End If

Parent.wf_scroll_tree(Long(dw_yields.Object.DataWindow.FirstRowOnPage))


end event

event ue_post_scroll();//** IBDKEEM ** 04/04/2002 ***********************
//Tried to get this working for the PB8 Migration.
//Never worked in the 6.5 build so i gave up.
//i was tired to trying to fix Muckey's Code.
//************************************************
// When the TreeView scrolls then position the Datawindow the same

//Long						ll_find_row		// Row Needed Position
//Long						ll_first_row	// Current First Row of Data Window
//Long						ll_handle		// Handle to First Visiable Treeview Item
//String					ls_visible
//Long						ll_test
//
//
//ib_StopScroll = true
//Parent.SetRedraw(False)
//this.SetRedraw(False)
//dw_yields.SetRedraw(False)	
//
//ll_handle = This.FindItem(FirstVisibleTreeItem!, 0)
//ll_find_row = dw_yields.Find('tree_row = ' + String(ll_handle), 1, dw_yields.RowCount() + 1)
//
//dw_yields.ScrollToRow(ll_find_row)		
//ll_first_row = Long(dw_yields.Object.DataWindow.FirstRowOnPage)
//messagebox("ll_first_row", ll_first_row)
//
//if ll_find_row <> ll_first_row then
//	dw_yields.scrollpriorpage( )
//	dw_yields.ScrollToRow(ll_find_row)		
//	
//	messagebox("ll_first_row", ll_first_row)
//	
//	if ll_find_row <> ll_first_row then
//		dw_yields.scrollnextpage( )
//		dw_yields.ScrollToRow(ll_find_row)		
//	end if
//	
//	messagebox("ll_first_row", ll_first_row)
//	
//	if ll_find_row <> ll_first_row then
//		dw_yields.scrollnextpage( )
//		dw_yields.ScrollToRow(ll_find_row)		
//	end if
//	
//	messagebox("ll_first_row", ll_first_row)
//end if
//
//
//Parent.SetRedraw(True)
//this.SetRedraw(True)
//dw_yields.SetRedraw(True)	
//ib_StopScroll = false
end event

event itemexpanding;Boolean			lb_first_product

String			ls_product_code, &
					ls_input_product, &
					ls_output_product, &
					ls_input_product_state, &
					ls_input_product_status, &
					ls_output_product_state, &
					ls_output_product_status, &
					ls_tree_products, &
					ls_header_string, &
					ls_step_descr, &
					ls_parent, &
					ls_product_type, &
					ls_data, &
					ls_find_string, &
					ls_temp

Integer			li_step_sequence, &
					li_ret
					
Long				ll_counter, &
					ll_tvnext, &
					ll_tvnextproduct, &
					ll_row_count, &
					ll_insert_row, &
					ll_findrow, &
					ll_datastore_row, &
					ll_datastore_row_2

TreeViewItem	ltvi_child

u_string_functions			lu_string


If ib_first_time then 
	ib_first_time = False
	Return 0
End If

This.SetRedraw(False)
dw_yields.SetRedraw(false)

This.GetItem(handle,ltvi_child)
ls_data = String(ltvi_child.Data)

ls_parent = Right(ls_data, 1)
If ls_parent = "S" or ls_parent = "P" Then
	wf_expand_yields(handle)
	Return 
end if

ls_input_product = lu_string.nf_GetToken(ls_data, "~t")
ls_input_product_state = lu_string.nf_GetToken(ls_data, "~t")
ls_input_product_status = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product_state = lu_string.nf_GetToken(ls_data, "~t")
ls_output_product_status = lu_string.nf_GetToken(ls_data, "~t")
li_step_sequence = Integer(lu_string.nf_GetToken(ls_data, "~t"))

ls_find_string = "tree_row = " + String(handle)

ll_insert_row = dw_yields.Find(ls_find_string, 1, dw_yields.RowCount() + 1)

IF ll_insert_row < 1 Then 
	MessageBox('Programming Error In ItemExpanding', &
			"It didn't find a matching row.  Please call applications.")
	IF Not ib_compute_yields Then
		This.SetRedraw(True)
		dw_yields.SetRedraw(True)
	End If
	Return
End If

ids_tree.Reset()

ls_temp = ids_expand_datastore.object.datawindow.data
ll_datastore_row = ids_expand_datastore.Find("mfg_step_sequence = " + &
			String(0) + " And input_fab_product = '" + &
			Space(Len(ls_input_product)) + "' And output_fab_product = '" + &
			ls_output_product  + "' And output_product_state = '" + &
			ls_output_product_state  + "' And output_product_status = '" + &			
			ls_output_product_status + "'",  1, ids_expand_datastore.RowCount() + 1)
//				ls_output_product + "'", 1, ids_expand_datastore.RowCount() + 1)
IF ll_datastore_row > 0 Then
	ll_datastore_row_2 = ids_expand_datastore.Find("mfg_step_sequence = " + &
			String(0) + " And input_fab_product = '" + &
			Space(Len(ls_input_product)) + "'", ll_datastore_row + 1, ids_expand_datastore.RowCount() + 1)
	If Not ll_datastore_row_2 > 0 Then
//I added one here so I can subtract 1.
		ll_datastore_row_2 = ids_expand_datastore.RowCount() + 1
	End If
	ll_datastore_row_2 --		
	ids_expand_datastore.RowsCopy(ll_datastore_row, ll_datastore_row_2, Primary!, ids_tree, 100000, Primary! )
	ll_row_count = ids_tree.RowCount()
Else
	ls_header_string = '~t~t' + &
								String(dw_header.GetItemNumber(1, 'characteristic')) + '~t' + &
								String(dw_header.GetItemDate(1, 'effective_begin_date'), 'yyyy-mm-dd') + '~t' + &
								String(dw_header.GetItemDate(1, 'effective_end_date'), 'yyyy-mm-dd') + '~t' + &
								dw_header.GetItemString(1, 'effective_pending_ind') + '~t' + &
								ls_output_product_state + '~t' + ls_output_product_status + '~t' + &
								'I'
	
	istr_error_info.se_event_name = "tv_itemexpanding"
		
//	li_ret = iu_pas203.nf_pasp67br_inq_pas_yields_tree(istr_error_info, &
//									ls_output_product, &
//									ls_header_string, &
//									ls_tree_products)

	li_ret = iu_ws_pas3.uf_pasp67fr(istr_error_info, &
									ls_output_product, &
									ls_header_string, &
									ls_tree_products)
	
	if li_ret <> 0 And Left(istr_error_info.se_message, 6) = 'PAS008' Then
		ltvi_child.Children = False
		This.SetItem(handle,ltvi_child)
		ll_datastore_row = ids_expand_datastore.InsertRow(100000)
		ids_expand_datastore.SetItem(ll_datastore_row, "mfg_step_sequence", 0)
		ids_expand_datastore.SetItem(ll_datastore_row, "input_fab_product", Space(Len(ls_input_product)))
		ids_expand_datastore.SetItem(ll_datastore_row, "output_fab_product", ls_output_product)
		ids_expand_datastore.SetItem(ll_datastore_row, "output_product_state", ls_output_product_state)
		ids_expand_datastore.SetItem(ll_datastore_row, "output_product_status", ls_output_product_status)		
		ids_expand_datastore.SetItem(ll_datastore_row, "input_product_state", ls_input_product_state)
		ids_expand_datastore.SetItem(ll_datastore_row, "input_product_status", ls_input_product_status)		
		IF Not ib_compute_yields Then
			This.SetRedraw(True)
			dw_yields.SetRedraw(True)
		End If
		return
	End If
	
	If li_ret <> 0 Then 
		IF Not ib_compute_yields Then
			This.SetRedraw(True)
			dw_yields.SetRedraw(True)
		End If
		return 1
	End If
	ll_row_count = ids_tree.ImportString(ls_tree_products)
	ids_expand_datastore.ImportString(ls_tree_products)
End If

ls_data = String(ltvi_child.Data)
ltvi_child.Data = Replace(ls_data, Len(ls_data), 1, "P")
This.SetItem(handle,ltvi_child)

If ll_row_count > 0 Then
	lb_first_product = True
	For ll_counter = 2 to ll_row_count		
		il_item_count ++
		ltvi_child.Children = True
		ls_step_descr = ids_tree.GetItemString(ll_counter,"fab_product_description")
		ls_input_product = ids_tree.GetItemString(ll_counter,"input_fab_product")
		ls_input_product_state = ids_tree.GetItemString(ll_counter,"input_product_state")
		ls_input_product_status = ids_tree.GetItemString(ll_counter,"input_product_status")
		ls_output_product = ids_tree.GetItemString(ll_counter,"output_fab_product")
		ls_output_product_state = ids_tree.GetItemString(ll_counter,"output_product_state")
		ls_output_product_status = ids_tree.GetItemString(ll_counter,"output_product_status")
		li_step_sequence = ids_tree.GetItemNumber(ll_counter,"mfg_step_sequence")
		If ids_tree.GetItemString(ll_counter, 'step_product_ind') = 'S' Then
			ll_insert_row ++
			ll_tvnextproduct = 0
			ltvi_child.Label = trim(ls_step_descr)
			ltvi_child.PictureIndex = 2
			ltvi_child.SelectedPictureIndex = 2
			ltvi_child.Data = ls_input_product + "~t" + &
									ls_input_product_state + "~t" + &
									ls_input_product_status + "~t" + &
									ls_output_product + "~t" + &
									ls_output_product_state + "~t" + &
									ls_output_product_status + "~t" + &
									String(li_step_sequence) + "~tS"
			If lb_first_product Then
				ll_tvnext = This.InsertItemFirst(handle,ltvi_child)
			Else
				ll_tvnext = This.InsertItem(handle, ll_tvnext,ltvi_child)
			End If
			ids_tree.SetItem(ll_counter, 'tree_row', ll_tvnext)
			ids_tree.SetItem(ll_counter, 'parent_tree_row', handle)
			ids_tree.RowsMove(ll_counter, ll_counter, Primary!, dw_yields, ll_insert_row, Primary!)
			dw_yields.SetItemStatus(ll_insert_row, 0, Primary!, NotModified!)
			ll_counter --
			ll_row_count --
		Else
			ls_product_type = ids_tree.GetItemString(ll_counter,"fab_type")
			ltvi_child.Data = ls_input_product + "~t" + &
									ls_input_product_state + "~t" + &
									ls_input_product_status + "~t" + &
									ls_output_product + "~t" + &
									ls_output_product_state + "~t" + &
									ls_output_product_status + "~t" + &
									String(li_step_sequence) + "~tN"
			ltvi_child.Label = trim(ls_output_product + " " + &
				ls_output_product_state + " " + ls_output_product_status + " " + &
				ls_product_type + " " + &
				ids_tree.GetItemString(ll_counter,"fab_product_description") + &
				fill(' ', 150))

			ltvi_child.PictureIndex = 1
			ltvi_child.SelectedPictureIndex = 1
			If ls_product_type = 'BY' & 
					or ids_tree.GetItemString(ll_counter, 'sku_product_ind') = 'Y' Then
				ltvi_child.Children = False
			End If
			If lb_first_product Then
				ll_insert_row ++
				ll_tvnext = This.InsertItemFirst(handle,ltvi_child)
				ids_tree.SetItem(ll_counter, 'tree_row', ll_tvnext)
				ids_tree.SetItem(ll_counter, 'parent_tree_row', handle)
				ids_tree.RowsMove(ll_counter, ll_counter, Primary!, dw_yields, ll_insert_row, Primary!)
				dw_yields.SetItemStatus(ll_insert_row, 0, Primary!, NotModified!)
				ll_counter --
				ll_row_count --
			Else
				If ll_tvnextproduct = 0 then
					ll_tvnextproduct = This.InsertItemFirst(ll_tvnext,ltvi_child)
				Else
					ll_tvnextproduct = This.InsertItem(ll_tvnext, ll_tvnextproduct,ltvi_child)
				End If
				ids_tree.SetItem(ll_counter, 'tree_row', ll_tvnextproduct)
				ids_tree.SetItem(ll_counter, 'parent_tree_row', ll_tvnext)
				ids_tree.RowsMove(ll_counter, ll_counter, Primary!, dw_yields, 10000, Filter!)
				dw_yields.SetItemStatus(dw_yields.FilteredCount(), 0, Filter!, NotModified!)
				ll_counter --
				ll_row_count --
			End If
		End If
		lb_first_product = False
	Next
End If

IF ib_compute_yields Then
	This.Event itemexpanded(handle)
End If

SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

Return 0

end event

event selectionchanged;TreeViewItem		ltvi_temp
Long					ll_findrow
String				ls_visible

//This.GetItem(newhandle,ltvi_temp)
dw_yields.SetRedraw(False)
ls_visible = dw_yields.object.yield.visible
dw_yields.object.yield.visible = 1
dw_yields.ScrollToRow(ll_findrow)
dw_yields.object.yield.visible = ls_visible
dw_yields.SetRedraw(True)

end event

event itemcollapsing;IF NOT ib_collapsing THEN
	SetPointer(Hourglass!)
	
	Parent.SetRedraw(False)
	this.SetRedraw(False)
	dw_yields.SetRedraw(False)	
	ib_collapsing = true //this will stop this event from firing when parent.wf_collapse(handle) is called recursivly.
	
	// This is the Stop Node. When it is reached do not collapse any further
	il_StopNodeIdx = tv_tree.FindItem(NextTreeItem!, handle)
	
	// ** Debuging code to trace the collapse of the nodes. **
		//		string			ls_data 
		//		TreeViewItem	ltvi_child
		//		
		//		This.GetItem(handle,ltvi_child)
		//		ls_data = String(ltvi_child.Data)
		//		ls_data = "Stop Node = " + STRING(il_StopNodeIdx) + "~nCollapsing Node = " + string(handle) + "~n" + ls_data 
		//		messagebox("ue_post_collapsed",ls_data)
	// ** END Debugging Code **
	
	parent.wf_collapse(handle)
	//Parent.wf_scroll_tree(Long(dw_yields.Object.DataWindow.FirstRowOnPage))
	
	il_StopNodeIdx = -1
	
	ib_collapsing = false
	this.SetRedraw(True)
	dw_yields.SetRedraw(True)
	Parent.SetRedraw(True)
	
	SetPointer(Arrow!)
END IF

//Return 1 to stop the control from collapsing. 
//The function parent.wf_collapse(handle) will perform this action instead.
Return 1 
end event

event itemexpanded;Long				ll_findrow


IF Not ib_expand_all Then
	This.PostEvent('ue_post_expanded')
End If
end event

event getfocus;dw_yields.AcceptText()
end event

event rightclicked;wf_popup_menu()
end event

event constructor;tv_tree.indent = -1
end event

type dw_header from u_base_dw_ext within w_pas_yields_tree
integer y = 160
integer width = 2779
integer height = 352
integer taborder = 40
string dataobject = "d_yields_tree_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = false

This.InsertRow(0)


end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild		ldwc_char, &
							ldwc_parent_product, &
							ldwc_parent_step, &
							ldwc_parent_descr
								

dw_header.GetChild('parent_fab_product', ldwc_parent_product)
dw_header.GetChild('parent_fab_product_descr', ldwc_parent_descr)
ldwc_parent_product.ShareData(ldwc_parent_descr)

dw_header.GetChild('parent_step_descr', ldwc_parent_step)
ldwc_parent_product.ShareData(ldwc_parent_step)

This.GetChild('characteristic', ldwc_char)

This.SetItem(1, 'characteristic', 0)
This.SetItem(1, 'effective_pending_ind', 'P')

end event

type dw_yields from u_base_dw_ext within w_pas_yields_tree
event ue_sink_with_tree ( )
event ue_vscroll pbm_vscroll
event ue_post_itemchanged ( )
integer x = 2459
integer y = 480
integer width = 1152
integer height = 1040
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_yields_tree"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_vscroll;call super::ue_vscroll;Long					ll_row


ll_row = Long(dw_yields.Object.DataWindow.FirstRowOnPage)

If ll_row > 0 Then
	wf_scroll_tree(ll_row)
End If
end event

event ue_post_itemchanged;call super::ue_post_itemchanged;Long		ll_row

ll_row = This.GetRow()

If This.GetItemNumber(ll_row, 'piece_ratio') = 0 Then
	This.SetItem(ll_row, 'piece_ratio', 1.00)
End If
end event

event constructor;call super::constructor;//This.Modify("yield.editmask.mask = ~"~'0~'~tIf(step_product_ind = ~'P~', ~'###,###.00~', ~'#~')~"" + &
//				"piece_ratio.editmask.mask = ~"~'0~'~tIf(step_product_ind = ~'P~', ~'###,###.00~', ~'#~')~"")

is_background_color = dw_yields.Object.yield.Background.color
is_protect = dw_yields.Object.yield.protect

end event

event itemchanged;call super::itemchanged;Long				ll_treerow

Decimal			ldc_data, &
					ldc_yield, &
					ldc_piece_ratio
					
String 			ls_temp

Choose Case dwo.name
	Case 'yield'
		If Not IsNumber(data) Then
			iw_frame.SetMicroHelp('Yields must be a number')
			This.SelectText(1, 100)
			Return 1
		End If

		ldc_data = Dec(data)
	
		If ldc_data < 0.00 or ldc_data > 100.00 Then
			iw_frame.SetMicroHelp('Yields must be greater than 0.00 and less than 100.00')
			This.SelectText(1, 100)
			Return 1
		End If
		If IsNull(dw_yields.GetItemNumber(row, 'piece_ratio')) Then
			ldc_piece_ratio = 0.00
		Else
			ldc_piece_ratio = dw_yields.GetItemDecimal(row, 'piece_ratio')
		End IF
		If ldc_data = 0.00 and ldc_piece_ratio = 0.00 Then
			Parent.post wf_get_next_level(row)
			This.Event post ue_post_itemchanged()
		Else
			This.SetItem(row, 'char_key', dw_header.GetItemNumber(1, 'characteristic'))
			This.SetItem(row, 'delete_ind', 'N')
			If This.GetItemString(row, 'fab_type') <> 'BY' &
					Or This.GetItemString(row, 'sku_product_ind') = 'N' Then
				tv_tree.ExpandItem(This.GetItemNumber(row, 'tree_row'))
			End If
		End If			
			
	Case 'piece_ratio'

		ldc_data = Dec(data)
		
		If ldc_data < 0.00 or ldc_data > 100.00 Then
			iw_frame.SetMicroHelp('Piece Ratio must be greater than 0.00 and less than 100.00')
			This.SelectText(1, 100)
			Return 1
		End If
		If IsNull(This.GetItemNumber(row, 'yield')) Then
			ldc_yield = 0.00
		Else
			ldc_yield = This.GetItemDecimal(row, 'yield')
		End IF
		If ldc_data = 0.00 and ldc_yield = 0.00 Then
			Parent.post wf_get_next_level(row) 
			This.Event post ue_post_itemchanged()
		Else
			This.SetItem(row, 'char_key', dw_header.GetItemNumber(1, 'characteristic'))
			This.SetItem(row, 'delete_ind', 'N')
		End If			
End Choose

		
end event

event itemerror;call super::itemerror;Choose Case dwo.name
	Case 'yield'
		If Not IsNumber(data) Then
			iw_frame.SetMicroHelp('Yields must be a number')
			This.SelectText(1, 100)
			Return 1
		End If
End Choose
Return 1
end event

event rowfocuschanged;call super::rowfocuschanged;Long					ll_treerow

String				ls_parent

Decimal				ldc_difference


If Not currentrow > 1 Then Return

If Not Parent.wf_check_against_parent_yield(currentrow, ldc_difference, ls_parent) Then
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	Return
End IF
If ldc_difference <> 0.00 Then
	iw_frame.SetMicroHelp('The difference between the children and ' + Trim(ls_parent) + &
			' is ' + String(ldc_difference, '###.000'))
Else
	If Not Parent.wf_check_against_children_yield(currentrow, ldc_difference) Then
		iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
		Return
	End If
	If ldc_difference <> 0.00 Then
		iw_frame.SetMicroHelp('The difference between ' + &
				Trim(dw_yields.GetItemString(currentrow, 'output_fab_product')) + &
				"'s and its children is " + String(ldc_difference, '###.000'))
	Else
		iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	End If
End If


end event

event scrollvertical;call super::scrollvertical;Long					ll_row

ll_row = Long(dw_yields.Object.DataWindow.FirstRowOnPage)
If ll_row > 0 Then
	wf_scroll_tree(ll_row)
End If
	
//** IBDKEEM ** 04/04/2002 **
//if not ib_StopScroll then
//	ib_StopScroll = true
//	Parent.SetRedraw(False)
//	this.SetRedraw(False)
//	tv_tree.SetRedraw(False)	
//	
//	If ll_row > 0 Then
//		wf_scroll_tree(ll_row)
//	End If
//
//	Parent.SetRedraw(True)
//	this.SetRedraw(True)
//	tv_tree.SetRedraw(True)	
//	ib_StopScroll = false
//end if
end event

event rbuttondown;call super::rbuttondown;wf_popup_menu()
end event

event clicked;call super::clicked;If row > 0 Then
	this.SetRow(row)
End If
end event

