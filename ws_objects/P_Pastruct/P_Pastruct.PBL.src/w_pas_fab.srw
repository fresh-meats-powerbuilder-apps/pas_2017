﻿$PBExportHeader$w_pas_fab.srw
forward
global type w_pas_fab from w_base_sheet_ext
end type
type dw_pas_fab from u_base_dw_ext within w_pas_fab
end type
end forward

global type w_pas_fab from w_base_sheet_ext
integer x = 0
integer y = 0
integer width = 2505
integer height = 880
string title = "Fabrication Products"
long backcolor = 67108864
boolean center = true
event ue_getdata pbm_custom01
dw_pas_fab dw_pas_fab
end type
global w_pas_fab w_pas_fab

type variables
Private:
s_error				istr_error_info
u_pas201				iu_pas201
u_pas203				iu_pas203
datawindowchild 	idddw_child
Character			ic_projections_exist
u_ws_pas1	iu_ws_pas1
u_ws_pas3	iu_ws_pas3


end variables

forward prototypes
public subroutine wf_filenew ()
public subroutine wf_print ()
public subroutine wf_infomessage (string as_function, string as_message)
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_delete ()
end prototypes

event ue_getdata;String ls_fab_product

Choose Case Lower(String(Message.LongParm, "product"))
	Case 'product'
		ls_fab_product = dw_pas_fab.GetItemString(1, 'fab_product_code') + "~t" + &
								dw_pas_fab.GetItemString(1, 'fab_product_description') + "~t" 
								
		Message.StringParm = ls_fab_product
End Choose
end event

public subroutine wf_filenew ();Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return

dw_pas_fab.Reset()
dw_pas_fab.InsertRow(0)
//dw_pas_fab.SetFocus()
ic_projections_exist = ' '
return
end subroutine

public subroutine wf_print ();dw_pas_fab.Print()
end subroutine

public subroutine wf_infomessage (string as_function, string as_message);iw_frame.SetMicroHelp(as_message)

//IBDKEEM DEBUG for edit checks.
//messagebox(as_edit_name,as_message)
end subroutine

public function boolean wf_retrieve ();Char		lc_product_status

String	ls_tpasfab, &
			ls_fab_product,&
			ls_product_code, &
			ls_product_desc, &
			ls_temp,ls_product_status, ls_projections_exist
	
Integer	li_rtn, len_status


ls_temp = dw_pas_fab.GetItemString(1, "fab_product_code")
If not IsNull(ls_temp) and Len(Trim(ls_temp)) <> 0 then
	Call w_base_sheet::closequery
	If Message.ReturnValue = 1 Then return false
End if

 
OpenWithParm(w_pas_product_inq, This)
ls_fab_product = Message.StringParm

If iw_frame.iu_string.nf_IsEmpty(ls_fab_product) Then return false

ls_product_code			= iw_frame.iu_string.nf_gettoken(ls_fab_product,"~t")
ls_product_desc			= iw_frame.iu_string.nf_gettoken(ls_fab_product,"~t")
			
ls_tpasfab = Space(71)

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp00br"
istr_error_info.se_message = Space(71)

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

//li_rtn = iu_pas201.nf_pasp00br(istr_error_info, &
//							ls_product_code, &
//							'1', &
//							ic_projections_exist, &
//							lc_product_status, &
//							ls_tpasfab)
						
ls_projections_exist = ic_projections_exist						
li_rtn = iu_ws_pas1.nf_pasp00fr(istr_error_info, &
							ls_product_code, &
							'1', &
							ls_projections_exist, &
							ls_product_status, &
							ls_tpasfab)							
							
	ic_projections_exist = ls_projections_exist						

If Not iw_frame.iu_string.nf_IsEmpty(ls_tpasfab) Then						
	ls_tpasfab = 	Trim(ls_product_code) + "~t" + ls_tpasfab

	dw_pas_fab.SetRedraw(False)
	dw_pas_fab.Reset()
	dw_pas_fab.ImportString(ls_tpasfab)	
	
ElseIf li_rtn > 0 Then
	This.Post wf_Retrieve()
	dw_pas_fab.SetRedraw(True)
	return False
End if

dw_pas_fab.uf_ChangeRowStatus(1, NotModified!)

ls_product_status = Trim(ls_product_status)
 Len_status =  Len(ls_product_status)


If Len(ls_product_status) > 0 Then
	dw_pas_fab.object.sku_product_code.protect = 1
	dw_pas_fab.object.sku_product_code.BackGround.Color = 67108864
Else
	dw_pas_fab.object.sku_product_code.protect = 0
	dw_pas_fab.object.sku_product_code.BackGround.Color = 16777215
End If

ls_temp = dw_pas_fab.Describe("Evaluate('LookUpDisplay(quantity_uom)',1)")
ls_temp = iw_frame.iu_string.nf_wordcap(ls_temp)
dw_pas_fab.Modify("pcs_per_box_t.Text='" + ls_temp + " / Box'")
dw_pas_fab.Modify("rev_pcs_per_box_t.Text='Rev " + ls_temp + " / Box'")

dw_pas_fab.SetColumn("fab_product_description")
dw_pas_fab.SetRedraw(True)
dw_pas_fab.ResetUpdate()

SetMicroHelp("Inquire Successful")
return True
end function

public function boolean wf_update ();String	ls_upd_tpasfab, &
			ls_temp, &
			ls_update_message

Date		ldt_temp


If dw_pas_fab.AcceptText() = -1 Then Return False
   
ls_temp = dw_pas_fab.GetItemString(1, "fab_product_code")
If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
	wf_infomessage("Warning", "Fab Code is a required field.")
	dw_pas_fab.SetColumn("fab_product_code")
	dw_pas_fab.SetFocus()
	return False
End if
ls_upd_tpasfab = ls_temp + '~t'

ls_temp = dw_pas_fab.GetItemString(1, "fab_product_description")
If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
	wf_infomessage("Warning", "Fab Description is a required field.")
	dw_pas_fab.SetColumn("fab_product_description")
	dw_pas_fab.SetFocus()
	return False
End if
ls_upd_tpasfab += ls_temp + '~t1~t'

ls_temp = dw_pas_fab.GetItemString(1, "fab_type")
If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
	wf_infomessage("Warning", "Fab Type is a required field.")
	dw_pas_fab.SetColumn("fab_type")
	dw_pas_fab.SetFocus()
	return False
End if
ls_upd_tpasfab += ls_temp + '~t'

ls_temp = dw_pas_fab.GetItemString(1, "fab_group")
If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
	wf_infomessage("Warning", "Fab Group is a required field.")
	dw_pas_fab.SetColumn("fab_group")
	dw_pas_fab.SetFocus()
	return False
End if
ls_upd_tpasfab += ls_temp + '~t'

ls_temp = dw_pas_fab.GetItemString(1, "yield_group")
If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
	ls_upd_tpasfab += '~t'
Else	
	ls_upd_tpasfab += ls_temp + '~t'
End if

ls_temp = dw_pas_fab.GetItemString(1, "quantity_uom")
If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
	wf_infomessage("Warning", "Quantity UOM is a required field.")
	dw_pas_fab.SetColumn("quantity_uom")
	dw_pas_fab.SetFocus()
	return False
End if
ls_upd_tpasfab += ls_temp + '~t'

ls_temp = dw_pas_fab.GetItemString(1, "sku_product_code")
If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
	ls_upd_tpasfab += '~t'
Else	
	ls_upd_tpasfab += ls_temp + '~t'
End if

ls_temp = String(dw_pas_fab.GetItemNumber(1, "characteristics"))
If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
	ls_upd_tpasfab += '0' + '~t'
Else	
	ls_upd_tpasfab += ls_temp + '~t'
End if

Choose Case dw_pas_fab.GetItemStatus(1, 0, Primary!)
Case DataModified!
	ls_temp = String(dw_pas_fab.GetItemNumber(1, "pcs_per_box"), "0000")
	If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
		ls_upd_tpasfab += '0000~t'
	Else	
		ls_upd_tpasfab += ls_temp + '~t'
	End if

	ls_temp = String(dw_pas_fab.GetItemNumber(1, "rev_pcs_per_box"))
	If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
		wf_infomessage("Warning", "Rev Pieces per Box is a required field.")
		dw_pas_fab.SetColumn("rev_pcs_per_box")
		dw_pas_fab.SetFocus()
		return False
	End if
	ls_upd_tpasfab += ls_temp + '~t'

	ldt_temp = dw_pas_fab.GetItemDate(1, "rev_piece_eff_date")
	If Not IsDate(String(ldt_temp)) Then
		wf_infomessage("Warning", "Rev Piece Effective Date is a required field and must be greater than or equal to today.")
		dw_pas_fab.SetColumn("rev_piece_eff_date")
		dw_pas_fab.SetFocus()
		return False
	End if
	
	If dw_pas_fab.GetItemStatus(1, "rev_piece_eff_date", primary!) <> NotModified! Then
		If IsNull(ldt_temp) or ldt_temp < Today() Then
			wf_infomessage("Warning", "Rev Piece Effective Date is a required field and must be greater than or equal to today.")
			dw_pas_fab.SetColumn("rev_piece_eff_date")
			dw_pas_fab.SetFocus()
			return False
		End if
	End If
	
	If dw_pas_fab.GetItemStatus(1, "rev_pcs_per_box", primary!) <> NotModified! Then	
		If ldt_temp > RelativeDate(Today(),30) Then
			If MessageBox("Effective Date Warning", "Effective Date is more than 30 days out, click OK to accept",Exclamation!,OKCancel!,2) = 2 Then
				return False
			End If
		End If
	End If
	
	ls_upd_tpasfab += String(ldt_temp, "yyyy-mm-dd") + '~t'

	ls_upd_tpasfab += 'M~r~n'
	ls_update_message = "Modification Successful"		

Case NewModified!

	ls_temp = String(dw_pas_fab.GetItemNumber(1, "pcs_per_box"), "0000")
	If IsNull(ls_temp) or Len(Trim(ls_temp)) = 0 Then
		wf_infomessage("Warning", "Pieces per Box is a required field.")
		dw_pas_fab.SetColumn("pcs_per_box")
		dw_pas_fab.SetFocus()
		return False
	End if
	ls_upd_tpasfab += ls_temp + '~t'

	ls_upd_tpasfab += '0000~t2999-12-31~tA~r~n'
	dw_pas_fab.SetItem(1, "rev_piece_eff_date", Date("2999-12-31"))
	ls_update_message = "Add Successful"

Case Else
	return True
End Choose

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp01br"
istr_error_info.se_message = Space(71)

SetPointer(HourGlass!)

//If Not iu_pas201.nf_pasp01br(istr_error_info, ls_upd_tpasfab) Then Return False

If Not iu_ws_pas1.nf_pasp01fr(istr_error_info, ls_upd_tpasfab) Then Return False

This.SetRedraw(False)
dw_pas_fab.uf_ChangeRowStatus(1, NotModified!)
dw_pas_fab.SetColumn("fab_product_description")
This.SetRedraw(True)
SetMicroHelp(ls_update_message)

return true
end function

public subroutine wf_delete ();String	ls_temp, &
			ls_tpasfab, &
			ls_fab_product

If dw_pas_fab.GetItemStatus(1, 0, Primary!) = New! or &
	dw_pas_fab.GetItemStatus(1, 0, Primary!) = NewModified! then return
	
If dw_pas_fab.AcceptText() = -1 then Return

ls_fab_product = dw_pas_fab.GetItemString(1, "fab_product_code") 

If IsNull(ls_fab_product) or Len(Trim(ls_fab_product)) = 0 then
	SetMicroHelp("No Product to Delete")
	dw_pas_fab.SetColumn("fab_product_code")
	return
End if					

If MessageBox("Warning", "Are you sure you wish to delete the Fab " + Trim(ls_temp) + &
					"?", Exclamation!, YesNo!) <> 1 then return
					
ls_tpasfab = ls_fab_product + "~t~t1~t~t~t~t~t~t~t~t~t~tD"
/*
	PASFAB-FAB-PRODUCT-CODE    + "~t"
	PASFAB-FAB-PRODUCT-DESCR   + "~t"
	PASFAB-PRODUCT-STATE       + "~t"
	PASFAB-FAB-TYPE            + "~t"
	PASFAB-FAB-GROUP           + "~t"
	PASFAB-YIELD-GROUP         + "~t"
	PASFAB-FAB-UOM             + "~t"
	PASFAB-SKU-PRODUCT-CODE    + "~t"
	IN-CHAR-KEY                + "~t"
	IN-PIECES-PER-BOX          + "~t"
	IN-REV-PIECES-PER-BOX      + "~t" 
	PASFAB-REV-PIECE-EFF-DATE  + "~t"
	IN-DBFUNC.                
*/

istr_error_info.se_event_name = "wf_delete"
istr_error_info.se_procedure_name = "u_pas021.nf_pasp01br"
istr_error_info.se_message = Space(71)

//If Not iu_pas201.nf_pasp01br(istr_error_info, ls_tpasfab) Then Return
If Not iu_ws_pas1.nf_pasp01fr(istr_error_info, ls_tpasfab) Then Return

This.SetRedraw(False)
dw_pas_fab.Reset()
dw_pas_fab.InsertRow(0)
dw_pas_fab.uf_ChangeRowStatus(1, NotModified!)
dw_pas_fab.SetColumn("fab_product_description")
This.SetRedraw(True)
SetMicroHelp(ls_fab_product + " Successfully Deleted")



end subroutine

event ue_postopen;call super::ue_postopen;DataWindowChild	dwc_type, &
						dwc_group, &
						dwc_uom, &
						dwc_char
						
String				ls_char						

iu_pas201 = Create u_pas201
iu_pas203 = Create u_pas203
iu_ws_pas1 = Create u_ws_pas1
iu_ws_pas3 = Create u_ws_pas3

dw_pas_fab.GetChild("fab_type", dwc_type)
dw_pas_fab.GetChild("fab_group", dwc_group)
dw_pas_fab.GetChild("quantity_uom", dwc_uom)

dwc_group.SetTransObject(SQLCA)
dwc_group.Retrieve("FABGROUP")
dwc_type.SetTransObject(SQLCA)
dwc_type.Retrieve("FABTYPE")
dwc_uom.SetTransObject(SQLCA)
dwc_uom.Retrieve("FABUOM")

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Fab"
istr_error_info.se_user_id 		= sqlca.userid

istr_error_info.se_event_name = "ue_postopen"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"
istr_error_info.se_message = Space(71)

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

//If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
If iu_ws_pas3.uf_pasp61fr(istr_error_info, ls_char) <> 0 Then
	wf_infomessage('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_pas_fab.GetChild("characteristics", dwc_char)
dwc_char.ImportString(ls_char)

ic_projections_exist = " "
wf_retrieve()
end event

on w_pas_fab.create
int iCurrent
call super::create
this.dw_pas_fab=create dw_pas_fab
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_pas_fab
end on

on w_pas_fab.destroy
call super::destroy
destroy(this.dw_pas_fab)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_find')

end event

event close;call super::close;Destroy iu_pas201
Destroy iu_pas203
Destroy iu_ws_pas1
Destroy iu_ws_pas3
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_find')

end event

event open;call super::open;dw_pas_fab.InsertRow(0)
dw_pas_fab.uf_ChangeRowStatus(1, NotModified!)
dw_pas_fab.SetColumn("fab_product_description")


end event

event ue_filenew;call super::ue_filenew;This.wf_FileNew()

end event

event ue_get_data;call super::ue_get_data;String ls_fab_product

Choose Case as_value
	Case 'product'
		ls_fab_product = dw_pas_fab.GetItemString(1, 'fab_product_code') + "~t" + &
								dw_pas_fab.GetItemString(1, 'fab_product_description') + "~t" + &
								dw_pas_fab.GetItemString(1, 'product_state') + "~t" + &
								dw_pas_fab.GetItemString(1, 'product_state_description')
								
		Message.StringParm = ls_fab_product
End Choose
end event

type dw_pas_fab from u_base_dw_ext within w_pas_fab
integer y = 19
integer width = 2417
integer height = 746
string dataobject = "d_pas_fab"
boolean border = false
end type

event itemchanged;call super::itemchanged;String	ls_text, &
			ls_temp, &
			ls_sku_product_code

Long 		ll_row

u_string_functions		lu_string

Choose Case dwo.Name
	Case "fab_type"
		If ic_projections_exist = "Y" Then
			wf_infomessage("Warning", "Fab Type cannot be changed because " + &
							Trim(This.GetItemString(1, "fab_product_code")) + &
							" has projection records.")
			Return 2
		End if
	Case "quantity_uom"
		dw_pas_fab.SetItem(1,"quantity_uom",data)
		ls_temp = dw_pas_fab.Describe("Evaluate('LookUpDisplay(quantity_uom)',1)")
		ls_temp = iw_frame.iu_string.nf_wordcap(ls_temp)
		dw_pas_fab.Modify("pcs_per_box_t.Text='" + ls_temp + " / Box'")
		dw_pas_fab.Modify("rev_pcs_per_box_t.Text='Rev " + ls_temp + " / Box'")
	Case 'sku_product_code'
		If lu_string.nf_IsEmpty(data) Then Return
		If not iu_pas203.nf_inq_sku_product(istr_error_info, data, ls_sku_product_code) then 
			This.SelectText(1, 100)
			Return 1
		End If
		ls_temp = Left(Right(ls_sku_product_code,3), 1) 
		If Left(Right(ls_sku_product_code,3), 1) = 'I' Then
			This.SelectText(1, 100)
			iw_frame.SetMicroHelp(data + ' -- Sku Product Code is inactive. Please enter a active ' + &
					'Sku Product Code.')
			Return 1
		End If
	Case 'pcs_per_box'
		If long(data) < 0 Then
			iw_frame.SetMicrohelp("Pieces Per Box must be a positive number")
			This.SelectText(1, 100)
			Return 1
		End If
	Case 'rev_piece_eff_date'
		IF date(data) < today() Then
			iw_frame.SetMicrohelp("Effective Date must be greater than or equal to today")
			This.SelectText(1, 100)
			Return 1
		End If
	// Don't allow spaces in the fab_product_code ibdkdld	
	Case 'fab_product_code'
		IF Pos(RightTrim(data),' ') > 0 Then
			iw_frame.SetMicrohelp("You can not have spaces in the product code")
			This.SelectText(1, 100)
			Return 1
		End If
End Choose
end event

event itemfocuschanged;call super::itemfocuschanged;String	ls_protected


Choose Case This.GetColumnName()
Case "fab_product_code"
	ls_protected = This.Describe("fab_product_code.Protect")
	If Not IsNumber(ls_protected) Then
		// then it is an expression, evaluate it
		// first char is default value, then tab, then expression
		// the whole thing is surrounded by quotes
		ls_protected = This.Describe('Evaluate("' + Mid(ls_protected, &
										Pos(ls_protected, '~t') + 1, &
										Len(ls_protected) - Pos(ls_protected, '~t') - 1) + &
										'", 1)')
	End if
	If ls_protected = '1' Then
		This.SetColumn("fab_product_code")
	End if
Case Else
	This.SelectText(1,Len(This.GetText()))
End Choose
end event

event itemerror;call super::itemerror;Choose Case dwo.name 
	// Don't allow spaces ibdkdld	
	Case 'sku_product_code'
		Return 1
	Case 'fab_product_code'
		Return 1
	Case 'rev_piece_eff_date'
		Return 1
	Case 'pcs_per_box'
		This.SelectText(1,100)
		Return 0
End Choose
end event

event constructor;call super::constructor;This.GetChild("product_state", idddw_child)

idddw_child.SetTransObject(SQLCA)
idddw_child.Retrieve("PRDSTATE")
end event

