﻿$PBExportHeader$w_pas_output_products.srw
$PBExportComments$Child Window to display output products of a step
forward
global type w_pas_output_products from w_base_response_ext
end type
type dw_outputs from u_base_dw_ext within w_pas_output_products
end type
end forward

global type w_pas_output_products from w_base_response_ext
integer x = 443
integer y = 488
integer width = 2702
integer height = 1296
string title = "Output Products"
boolean controlmenu = false
boolean resizable = true
windowtype windowtype = child!
long backcolor = 67108864
dw_outputs dw_outputs
end type
global w_pas_output_products w_pas_output_products

type variables
u_pas201		iu_pas201
u_ws_pas1		iu_ws_pas1

w_pas_instances	iw_parent_window
end variables

forward prototypes
public subroutine wf_print ()
end prototypes

public subroutine wf_print ();dw_outputs.Print()
end subroutine

event open;iw_parent_Window = Message.PowerObjectParm
iu_pas201 = Create u_pas201
iu_ws_pas1 = Create u_ws_pas1

//This.width += 50
This.Move(iw_frame.Width - This.Width - 50, This.Y)
This.PostEvent("ue_postOpen")
end event

event ue_postopen;call super::ue_postopen;s_error	lstr_error_info

Char	lc_dummy

Int	li_step_seq, &
		li_dummy

String	ls_outputs, &
			ls_fab_product, &
			ls_product_state, &
			ls_product_status, &
			ls_header_string, ls_dummy


iw_parent_window.wf_Get_Step(ls_fab_product, li_step_Seq, ls_product_state, ls_product_status)
dw_outputs.Reset()
lstr_error_info.se_message = Space(70)

ls_header_string = ls_fab_product + '~t' + ls_product_state + '~t' + &
						+ ls_product_status + '~t' + string(li_step_Seq) + '~t' 

//iu_pas201.nf_pasp05br(lstr_error_info, &
//							ls_header_string, &
//							li_dummy, &
//							lc_dummy, &
//							ls_outputs)
							
iu_ws_pas1.nf_pasp05fr(lstr_error_info, &
							ls_header_string, &
							li_dummy, &
							ls_dummy, &
							ls_outputs)							

lc_dummy = ls_dummy

If not iw_frame.iu_string.nf_isEmpty(ls_Outputs) then
	dw_outputs.ImportString(ls_outputs)
End if
end event

event close;call super::close;iw_parent_window.wf_unselect_rows()
Destroy iu_pas201
Destroy iu_ws_pas1

end event

on w_pas_output_products.create
int iCurrent
call super::create
this.dw_outputs=create dw_outputs
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_outputs
end on

on w_pas_output_products.destroy
call super::destroy
destroy(this.dw_outputs)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event resize;call super::resize;if this.width >= 2600 then 
	dw_outputs.width = 2578 
else
	dw_outputs.width = this.width - 22
end if

dw_outputs.height = this.height - 400

cb_base_cancel.y = dw_outputs.height + 100
cb_base_help.y = dw_outputs.height + 100


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_output_products
integer x = 1746
integer y = 936
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_output_products
integer x = 1445
integer y = 936
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_output_products
boolean visible = false
end type

type dw_outputs from u_base_dw_ext within w_pas_output_products
integer y = 32
integer width = 2578
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_pas_step_detail"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

