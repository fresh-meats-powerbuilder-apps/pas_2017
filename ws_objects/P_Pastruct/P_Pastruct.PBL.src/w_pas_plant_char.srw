﻿$PBExportHeader$w_pas_plant_char.srw
forward
global type w_pas_plant_char from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_pas_plant_char
end type
type dw_chars from u_base_dw_ext within w_pas_plant_char
end type
type dw_plant_import from u_base_dw_ext within w_pas_plant_char
end type
type dw_plants from u_base_dw_ext within w_pas_plant_char
end type
end forward

global type w_pas_plant_char from w_base_sheet_ext
integer x = 0
integer y = 0
integer width = 2926
integer height = 1616
string title = "Step Plant/Characteristics"
long backcolor = 67108864
event ue_getdata pbm_custom01
dw_header dw_header
dw_chars dw_chars
dw_plant_import dw_plant_import
dw_plants dw_plants
end type
global w_pas_plant_char w_pas_plant_char

type variables
Boolean	ib_modified

Long	il_plant_clicked_row, &
	il_char_clicked_row

s_error		istr_error_info

u_pas201		iu_pas201
u_ws_pas1	iu_ws_pas1
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

event ue_getdata;call super::ue_getdata;DataWindowChild	ldwc_steps


Choose Case Lower(String(Message.LongParm, "address"))
Case "header"
	Message.StringParm = dw_header.Object.DataWindow.Data
Case "steplist"
	dw_header.GetChild('mfg_step_sequence', ldwc_steps)
	Message.StringParm = ldwc_steps.Describe("DataWindow.Data")
End Choose
end event

public function boolean wf_retrieve ();Char	lc_fab_group[2]

DataWindowChild	ldwc_steps

Int	li_mfg_Step_sequence, &
		li_counter

Long	ll_row, &
		ll_row_count, &
		ll_plant_row_count

String	ls_plant, &
			ls_char, &
			ls_temp, &
			ls_Header, &
			ls_Steps, &
			ls_fab_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_header_string,&
			ls_fab_group


If ib_modified Then
	This.TriggerEvent("closequery")
	If Message.ReturnValue = 1 Then return false
End if

OpenWithParm(w_pas_product_step_inq, This)

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

ls_Header = Message.StringParm
If iw_frame.iu_string.nf_IsEmpty(ls_Header) Then return False

iw_frame.iu_string.nf_ParseLeftRight(ls_Header, '~r~n', ls_Header, ls_Steps)

dw_header.GetChild('mfg_step_sequence', ldwc_steps)
ldwc_steps.Reset()
ldwc_Steps.ImportString(ls_Steps)

dw_header.Reset()
dw_header.ImportString(ls_Header)


This.SetRedraw(False)
dw_plants.SetFilter("")
dw_plants.Filter()
ll_row_count = dw_plants.RowCount()
for li_counter = 1 to ll_row_count
	dw_plants.SetItem(li_counter, "original_row", 'N')
Next
dw_plants.SelectRow(0, False)
dw_plants.SetFilter("location_code = ''")
dw_plants.Filter()
dw_chars.Reset()

This.SetRedraw(True)

ls_temp = dw_header.GetItemString(1, "fab_product_code")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then return False
ls_fab_product_code = ls_temp

ls_temp = dw_header.GetItemString(1, "product_state") 
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then return False
ls_product_state = ls_temp

ls_temp = dw_header.GetItemString(1, "product_status") 
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then return False
ls_product_status = ls_temp

li_mfg_step_sequence = dw_header.GetItemNumber(1, "mfg_step_sequence")
If li_mfg_step_sequence < 1 Then
	MessageBox("Step Description", "Error getting Step Description")
	return False
End if

ls_header_string = ls_fab_product_code + '~t' + ls_product_state + '~t' &
						+ ls_product_status + '~t' + string (li_mfg_step_sequence) + '~t' 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp07br"
istr_error_info.se_message = Space(70)
ls_fab_group = lc_fab_group

//If Not iu_pas201.nf_pasp07br(istr_error_info, &
//							ls_header_string, &
//							lc_fab_group, &
//							ls_plant, &
//							ls_char) Then 
							
If Not iu_ws_pas1.nf_pasp07fr(istr_error_info, &
							ls_header_string, &
							ls_fab_group, &
							ls_plant, &
							ls_char) Then
							
	Return False
End if
lc_fab_group = ls_fab_group
This.SetRedraw(False)

String	ls_filter, &
			ls_temp_codes

SELECT type_short_desc 
	into :ls_temp_codes
	from tutltypes
	where record_type = 'PLTCHAR'
	and type_code = :lc_fab_group;
	
If SQLCA.SQLCode = 0 Then
	For li_counter = 1 to Len(Trim(ls_temp_codes))
		If li_counter = 1 Then 
			ls_filter = "location_type = '" + Mid(ls_temp_codes,li_counter,1) + "'"
		Else
			ls_filter += " or location_type = '" + Mid(ls_temp_codes,li_counter,1) + "'"
		End If
	Next
	dw_plants.SetFilter(ls_filter)
Else
	dw_plants.SetFilter("")
End If

dw_plants.Filter()
dw_plants.Sort()


dw_plant_import.Reset()
dw_plant_import.ImportString(ls_plant)
ll_row_count = dw_plant_import.RowCount()
ll_plant_row_count = dw_plants.RowCount()

For li_counter = 1 to ll_row_count
		dw_plants.SetItem(ll_row, "original_row", 'N')
Next
	
For li_counter = 1 to ll_row_count
	ll_row = dw_plants.Find("location_code = '" + &
							dw_plant_import.GetItemString(li_counter, "plant_code") + &
							"'", 1, ll_plant_row_count)
	If ll_row <> 0 Then
		dw_plants.SetItem(ll_row, "original_row", 'Y')
		dw_plants.SelectRow(ll_row, True)
	End if
Next

If Not iw_frame.iu_string.nf_IsEmpty(ls_char) Then
	dw_chars.ImportString(ls_char)
End if

dw_chars.Sort()

ll_row = 0
ll_row_count = dw_chars.RowCount()
Do 
	ll_row = dw_chars.Find("original_row = 'Y'", ll_row + 1, ll_row_count)
	If ll_row <> 0 Then
		dw_chars.SelectRow(ll_row, True)
	End if
Loop While ll_row > 0 And ll_row < ll_row_count

dw_plants.ResetUpdate()
dw_chars.ResetUpdate()
dw_chars.Sort()
This.SetRedraw(True)
SetMicroHelp("Ready")
return true
end function

public function boolean wf_update ();Boolean	lb_ret

Int	li_mfg_step_sequence, &
		li_counter

Long	ll_row_count, &
		ll_row

String 	ls_filter, &
			ls_plant_update, &
			ls_char_update, &
			ls_temp, &
			ls_fab_product_code, &
			ls_product_state, &
			ls_product_status, &
			ls_header_string


ls_plant_update = ""
ls_char_update = ""

ls_temp = dw_header.GetItemString(1, "fab_product_code")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	MessageBox("Product Code", "Product Code is a required field")
	return false
End if
ls_fab_product_code = ls_temp


ls_temp = dw_header.GetItemString(1, "product_state")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	MessageBox("Product State", "Product State is a required field")
	return false
End if
ls_product_state = ls_temp

ls_temp = dw_header.GetItemString(1, "product_status")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	MessageBox("Product Status", "Product Status is a required field")
	return false
End if
ls_product_status = ls_temp

li_mfg_step_sequence = dw_header.GetItemNumber(1, "mfg_step_sequence")
If IsNull(li_mfg_step_Sequence) Or li_mfg_step_sequence < 1 Then
	MessageBox("Step Description", "Step Description is a required field")
	return false
End if

This.SetRedraw(False)
ls_filter = dw_plants.Describe('DataWindow.Table.Filter')
dw_plants.SetFilter("original_row = 'N' And IsSelected()")
dw_plants.Filter()

ll_row_count = dw_plants.RowCount()
For ll_Row = 1 to ll_Row_count
	ls_plant_update += dw_plants.GetItemString(ll_row, "location_code") + '~tA~r~n'
Next

dw_plants.SetFilter("")
dw_plants.Filter()
dw_plants.SetFilter("original_row = 'Y' And Not IsSelected()")
dw_plants.Filter()

ll_row_count = dw_plants.RowCount()
For ll_Row = 1 to ll_Row_count
	ls_plant_update += dw_plants.GetItemString(ll_row, "location_code") + '~tD~r~n'
Next

dw_plants.SetFilter("")
dw_plants.Filter()
dw_plants.SetFilter(ls_filter)
dw_plants.Filter()


dw_chars.SetFilter("original_row = 'N' And IsSelected()")
dw_chars.Filter()

ll_row_count = dw_chars.RowCount()
For ll_Row = 1 to ll_Row_count
	ls_char_update += String(dw_chars.GetItemNumber(ll_row, "char_key"), '0000') + '~tA~r~n'
Next

dw_chars.SetFilter("")
dw_chars.Filter()
dw_chars.SetFilter("original_row = 'Y' And Not IsSelected()")
dw_chars.Filter()

ll_row_count = dw_chars.RowCount()
For ll_Row = 1 to ll_Row_count
	ls_char_update += String(dw_chars.GetItemNumber(ll_row, "char_key"), '0000') + '~tD~r~n'
Next

dw_chars.SetFilter("")
dw_chars.Filter()
dw_chars.Sort()

ls_header_string = ls_fab_product_code + '~t' + ls_product_state + '~t' + & 
						ls_product_status + '~t' + string(li_mfg_step_sequence) + '~t'

This.SetRedraw(True)

//lb_ret = iu_pas201.nf_pasp08br(istr_error_info, &
//										ls_header_string, &
//										ls_plant_update, &
//										ls_char_update)
										
										
lb_ret = iu_ws_pas1.nf_pasp08fr(istr_error_info, &
										ls_header_string, &
										ls_plant_update, &
										ls_char_update)
										
If lb_ret Then
	ll_row_count = dw_plants.RowCount()
	For li_counter = 1 to ll_row_count
		If dw_plants.IsSelected(li_counter) Then
			dw_plants.SetItem(li_counter, "original_row", 'Y')
		Else
			dw_plants.SetItem(li_counter, "original_row", 'N')
		End if
	Next
	
	ll_row_count = dw_chars.RowCount()
	For li_counter = 1 to ll_row_count
		If dw_chars.IsSelected(li_counter) Then
			dw_chars.SetItem(li_counter, "original_row", 'Y')
		Else
			dw_chars.SetItem(li_counter, "original_row", 'N')
		End if
	Next
	
	dw_plants.ResetUpdate()
	dw_chars.ResetUpdate()
	ib_modified = False
	SetMicroHelp("Update Successful")
End if
return true
end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy iu_pas201
Destroy iu_ws_pas1
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_print')
end event

on closequery;call w_base_sheet_ext::closequery;If ib_modified Then
		// Changes were made to one of the datawindows
	CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
	CASE 1	// Save Changes
		If This.wf_update() = FALSE Then
			Message.ReturnValue = 1	 // Update failed - do not close window
		Else
			Return
		End If

	CASE 2	// Do not save changes

	CASE 3	// Cancel the closing of window
		Message.ReturnValue = 1
		Return
	END CHOOSE
End if
end on

event ue_postopen;call super::ue_postopen;This.SetRedraw(False)

//iw_frame.iu_netwise_data.nf_GetLocations_DW( dw_plants)
DataStore lds_locations
lds_locations = Create DataStore
lds_locations.DataObject = 'd_locations'
lds_locations.SetTransObject(SQLCA)
lds_locations.Retrieve()
dw_plants.ImportString(lds_locations.Describe("DataWindow.Data"))
Destroy lds_locations

dw_plants.SetFilter("location_code = ''")
dw_plants.Filter()

istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "Plt/Char"
istr_error_info.se_user_id 		= sqlca.userid

dw_plants.ResetUpdate()
dw_chars.ResetUpdate()
dw_chars.Sort()
This.SetRedraw(True)

iu_pas201 = Create u_pas201
iu_ws_pas1	= Create u_ws_pas1

This.PostEvent("ue_query")
end event

on ue_clearwindow;This.SetRedraw(False)

dw_header.Reset()
dw_header.InsertRow(0)

dw_chars.Reset()
dw_chars.InsertRow(0)

dw_plants.SelectRow(0, False)
dw_plants.SetFilter('location_code = ""')
dw_plants.Filter()
This.SetRedraw(True)
end on

event ue_query;call super::ue_query;wf_retrieve()
end event

on open;call w_base_sheet_ext::open;dw_header.InsertRow(0)

end on

on w_pas_plant_char.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_chars=create dw_chars
this.dw_plant_import=create dw_plant_import
this.dw_plants=create dw_plants
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_chars
this.Control[iCurrent+3]=this.dw_plant_import
this.Control[iCurrent+4]=this.dw_plants
end on

on w_pas_plant_char.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_chars)
destroy(this.dw_plant_import)
destroy(this.dw_plants)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_print')

end event

event resize;call super::resize;//constant integer li_x		= 1376
constant integer li_y		= 340

dw_plants.width = 1330
dw_chars.width = 1381

//dw_manage_inventory_detail_fresh.width	= newwidth - li_x
dw_plants.height	= newheight - li_y - 100
dw_chars.height	= newheight - li_y - 100


end event

type dw_header from u_base_dw_ext within w_pas_plant_char
integer x = 402
integer y = 32
integer width = 1669
integer height = 392
integer taborder = 0
string dataobject = "d_pas_exception_header"
boolean border = false
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False

end on

type dw_chars from u_base_dw_ext within w_pas_plant_char
integer x = 1376
integer y = 428
integer width = 1381
integer height = 924
integer taborder = 30
string dataobject = "d_pas_pltchr_char"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event clicked;call super::clicked;Int	li_idx

if row = 0 then return

ib_modified = True

If KeyDown(KeyShift!) Then
	SetRedraw(FALSE)
	If il_char_clicked_row > row Then
		For li_idx = il_char_clicked_row To row STEP -1
			SelectRow(li_idx, TRUE)	
		End For
	Else
		For li_idx = il_char_clicked_row To row
			SelectRow(li_idx, TRUE)	
		End For
	End If
	SetRedraw(TRUE)
	Return
Else
	If IsSelected(row) then
		This.SelectRow(row, False)
	Else
		This.SelectRow(row, True)
	End if
End If

il_char_clicked_row = row	
end event

type dw_plant_import from u_base_dw_ext within w_pas_plant_char
boolean visible = false
integer x = 215
integer y = 332
integer width = 311
integer height = 740
integer taborder = 10
string dataobject = "d_pas_pltchr_plant_import"
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False

end on

type dw_plants from u_base_dw_ext within w_pas_plant_char
integer x = 27
integer y = 428
integer width = 1330
integer height = 924
integer taborder = 20
string dataobject = "d_pas_pltchr_plant"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event clicked;call super::clicked;Int	li_idx

if row = 0 then return

ib_modified = True

If KeyDown(KeyShift!) Then
	SetRedraw(FALSE)
	If il_plant_clicked_row > row Then
		For li_idx = il_plant_clicked_row To row STEP -1
			SelectRow(li_idx, TRUE)	
		End For
	Else
		For li_idx = il_plant_clicked_row To row
			SelectRow(li_idx, TRUE)	
		End For
	End If
	SetRedraw(TRUE)
	Return
Else
	If IsSelected(row) then
		This.SelectRow(row, False)
	Else
		This.SelectRow(row, True)
	End if
End If

il_plant_clicked_row = row	
end event

