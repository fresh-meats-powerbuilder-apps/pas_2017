﻿$PBExportHeader$w_pas_tree_inq.srw
forward
global type w_pas_tree_inq from w_base_response_ext
end type
type dw_fab_product_code from u_fab_product_code within w_pas_tree_inq
end type
end forward

global type w_pas_tree_inq from w_base_response_ext
integer width = 1659
integer height = 568
string title = "Fab Inquire"
long backcolor = 67108864
dw_fab_product_code dw_fab_product_code
end type
global w_pas_tree_inq w_pas_tree_inq

type variables
Boolean	IsValidReturn

//window		iw_ParentWindow
end variables

event open;call super::open;iw_parentwindow = Message.PowerObjectParm
If Not IsValid(iw_parentwindow) Then
	MessageBox("Warning", "Error getting handle to parent window, Please try again.")
	CloseWithReturn(This, "")
End if
This.Title = iw_ParentWindow.Title + " Inquire"

end event

event ue_postopen;call super::ue_postopen;string ls_prod

iw_parentwindow.TriggerEvent("ue_getdata", 0, "product")
ls_prod =  Message.StringParm

If Not iw_frame.iu_string.nf_IsEmpty(ls_prod) Then 
	dw_fab_product_code.Reset()
	dw_fab_product_code.uf_importstring(ls_prod, TRUE)
End If


end event

on close;call w_base_response_ext::close;If Not IsValidReturn Then
	Message.StringParm = ""
End if
end on

on w_pas_tree_inq.create
int iCurrent
call super::create
this.dw_fab_product_code=create dw_fab_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fab_product_code
end on

on w_pas_tree_inq.destroy
call super::destroy
destroy(this.dw_fab_product_code)
end on

event ue_base_cancel;call super::ue_base_cancel;IsValidReturn = True
CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;String	ls_product

// IBDKEEM ** 08/08/2002 ** Product State
If not dw_fab_product_code.uf_Validate() Then
	return
End if

IsValidReturn = True
CloseWithReturn(This, dw_fab_product_code.uf_exportstring( ))

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_tree_inq
integer x = 1353
integer y = 352
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_tree_inq
integer x = 1061
integer y = 352
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_tree_inq
integer x = 768
integer y = 352
integer taborder = 20
end type

type dw_fab_product_code from u_fab_product_code within w_pas_tree_inq
integer x = 73
integer width = 1531
integer taborder = 10
boolean bringtotop = true
end type

