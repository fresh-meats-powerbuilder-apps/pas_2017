﻿$PBExportHeader$w_weight_distribution.srw
forward
global type w_weight_distribution from w_base_sheet_ext
end type
type dw_fab_product_code from u_fab_product_code within w_weight_distribution
end type
type dw_begin_end_date from u_source_begin_end_date within w_weight_distribution
end type
type dw_weight_distribution from u_base_dw_ext within w_weight_distribution
end type
type dw_plant from u_plant within w_weight_distribution
end type
end forward

global type w_weight_distribution from w_base_sheet_ext
integer x = 0
integer y = 0
integer width = 2610
integer height = 1716
string title = "Weight Distribution"
long backcolor = 67108864
event ue_getdata pbm_custom01
dw_fab_product_code dw_fab_product_code
dw_begin_end_date dw_begin_end_date
dw_weight_distribution dw_weight_distribution
dw_plant dw_plant
end type
global w_weight_distribution w_weight_distribution

type variables
Boolean                 ib_re_inquire, &
		ib_updating, &
		ib_Inquiring

Integer                  ii_update_rec_occurs
		
DataWindowChild   idwc_species, &
                            idwc_grade, &
                            idwc_weight_range, &
                            idwc_yield_grade, &
                            idwc_hormone_free, &
                            idwc_sex

string                    is_update_source

s_error                  istr_error_info

u_pas203              iu_pas203
//u_pas201              iu_pas201
u_ws_pas1	iu_ws_pas1


end variables

forward prototypes
public function boolean wf_deleterow ()
public subroutine wf_delete ()
public subroutine wf_print ()
public subroutine wf_product (character ac_product[10])
public subroutine wf_sort ()
public function boolean wf_checkpercenttotals ()
public function boolean wf_upd_wt_dist (long al_row, string as_header, string as_tpassrce_ind)
public subroutine wf_species_filter (string as_sex)
public function boolean wf_validate (long as_row)
public function boolean wf_validate_end (ref long al_row)
public subroutine wf_filenew ()
public function boolean wf_fixenddates (long al_row)
public function boolean wf_addrow ()
public function boolean wf_update ()
public function boolean wf_retrieve ()
end prototypes

event ue_getdata;String	ls_plant, &
			ls_product

date		ld_begin_date, &
			ld_end_date
		
Choose Case Lower(String(Message.LongParm, "header"))
	Case 'header'
		ls_plant = dw_plant.uf_get_plant_code( )
		ls_product = dw_fab_product_code.uf_exportstring( )
		ld_begin_Date	= date(dw_begin_end_date.uf_getbegindate( ))
		
		Message.StringParm = ls_plant + "~t" + ls_product + "~t" + &
									String(ld_begin_Date) + "~t" + String(ld_end_date)
End Choose
end event

public function boolean wf_deleterow ();dw_weight_distribution.SetFocus()
return(super::wf_deleterow())

end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public subroutine wf_print ();dw_weight_distribution.Print()
end subroutine

public subroutine wf_product (character ac_product[10]);//boolean			lb_return
//
//string			ls_tab = "~t"
//
//character		lc_product_desc[30] = Space(30)
//
//iw_frame.SetMicroHelp("Wait.. Inquiring Database")
//
//istr_error_info.se_event_name			= "wf_product"
//istr_error_info.se_procedure_name	= "nf_pasp03br"
//istr_error_info.se_message				= Space(70)
//
////f_debug("")
////f_debug("Before Create of iu_pas201")
//IF Not IsValid( iu_pas201 ) THEN
//	iu_pas201	=  CREATE u_pas201
//END IF
//
////f_debug("Before pasp03br")
//lb_return	= iu_pas201.nf_pasp03br( istr_Error_Info, ac_product, lc_product_desc )
////f_debug("After pasp03br")
//
//IF lb_return THEN
//	dw_weight_distribution.SetItem( dw_weight_distribution.GetRow(), "fab_product_descr", lc_product_desc )
//ELSE
//	MessageBox( "Product Code", ac_product + " is Not a Valid Product Code.", stopsign!, ok! )
//END IF
//
//Return( lb_return )
end subroutine

public subroutine wf_sort ();dw_weight_distribution.SetReDraw( False )
dw_weight_distribution.Sort()
dw_weight_distribution.GroupCalc()
dw_weight_distribution.SetReDraw( True )

end subroutine

public function boolean wf_checkpercenttotals ();Boolean			lb_Return

date				ld_begin_date, &
					ld_end_date, &
					ld_old_begin_date, &
					ld_old_end_date

decimal{2}		ld_percent

Integer			li_sequence, &
					li_old_sequence

long				ll_NbrRows, &
					ll_Row

string			ls_char_key, &
					ls_species, &
					ls_grade, &
					ls_weight_range, &
					ls_yield_grade, &
					ls_hormone_free, &
					ls_sex, &
					ls_old_char_key, &
					ls_old_species, &
					ls_old_grade, &
					ls_old_weight_range, &
					ls_old_yield_grade, &
					ls_old_hormone_free, &
					ls_old_sex
					
DWItemStatus	ldwis_status
					
lb_Return	= True
SetPointer( HourGlass! )
SetReDraw( False )
ll_NbrRows	= dw_weight_distribution.RowCount()
If ll_NbrRows > 0 Then wf_sort()
li_old_sequence = 0

FOR ll_Row = 1 TO ll_NbrRows
	ld_percent = dw_weight_distribution.GetItemNumber(ll_Row, "percent_total" )
	IF ld_percent <> 100 THEN
		SetPointer( Arrow! )
		SetReDraw( True)
		MessageBox( "Total Percent", "Weight Distribution Percent must Total 100%." )
		dw_weight_distribution.ScrollToRow( ll_Row )
		lb_Return = False
		EXIT
	END IF

	li_sequence				= dw_weight_distribution.GetItemNumber( ll_Row, "sequence")
	ls_char_key 			= dw_weight_distribution.GetItemString( ll_Row, "char_key" )
	ls_species				= dw_weight_distribution.GetItemString( ll_Row, "species")
	ls_grade					= dw_weight_distribution.GetItemString( ll_Row, "grade")
	ls_weight_range		= dw_weight_distribution.GetItemString( ll_Row, "weight_range")
	ls_yield_grade			= dw_weight_distribution.GetItemString( ll_Row, "yield_grade")
	ls_hormone_free		= dw_weight_distribution.GetItemString( ll_Row, "hormone_free")
	ls_sex					= dw_weight_distribution.GetItemString( ll_Row, "sex")
	ld_begin_date			= Date(dw_weight_distribution.GetItemDateTime( ll_Row, "begin_date"))
	ld_end_date				= Date(dw_weight_distribution.GetItemDateTime( ll_Row, "end_date"))

	// change end date to = group end date
	IF (ls_species			= ls_old_species) AND &
		(ls_grade			= ls_old_grade) AND &
		(ls_weight_range	= ls_old_weight_range) AND &
		(ls_hormone_free	= ls_old_hormone_free) AND &
		(ls_yield_grade	= ls_old_yield_grade) AND &
		(ls_sex				= ls_old_sex) AND &
		(ld_begin_date		= ld_old_begin_date) AND &
		(ld_end_date		<> ld_old_end_date) AND &
		(li_sequence		= li_old_sequence) Then
		ldwis_status = dw_weight_distribution.GetItemStatus(ll_row, 0, primary!)
		dw_weight_distribution.SetItem( ll_Row, "end_date", ld_old_end_date )
	END IF
	// add sequence number if none exists
	IF (ls_species			= ls_old_species) AND &
		(ls_grade			= ls_old_grade) AND &
		(ls_weight_range	= ls_old_weight_range) AND &
		(ls_hormone_free	= ls_old_hormone_free) AND &
		(ls_yield_grade	= ls_old_yield_grade) AND &
		(ls_sex				= ls_old_sex) AND &
		(ld_begin_date		= ld_old_begin_date) AND &
		(ld_end_date		= ld_old_end_date) AND &
		(li_sequence		= li_old_sequence) Then
		li_old_sequence++
		li_sequence = li_old_sequence
	END IF
	ldwis_status = dw_weight_distribution.GetItemStatus(ll_row, 0, primary!)
	dw_weight_distribution.SetItem( ll_Row, "sequence", li_sequence )
	If ldwis_status = NotModified! Then
		dw_weight_distribution.uf_changerowstatus(ll_row, NotModified!)
	End If

	ls_old_char_key		= ls_char_key
	li_old_sequence		= li_sequence
	ls_old_species			= ls_species			
	ls_old_grade			= ls_grade			
	ls_old_weight_range	= ls_weight_range
	ls_old_hormone_free	= ls_hormone_free
	ls_old_yield_grade	= ls_yield_grade
	ls_old_sex				= ls_sex
	ld_old_begin_date		= ld_begin_date
	ld_old_end_date		= ld_end_date
	
	wf_fixenddates (ll_row)
NEXT

// ***** for testing *******
//		Return( False )
// *************************

SetPointer( Arrow! )
SetReDraw( True)
Return( lb_Return )
end function

public function boolean wf_upd_wt_dist (long al_row, string as_header, string as_tpassrce_ind);boolean		lb_return

date			ld_begin_date, &
				ld_end_date

decimal{2}  ldc_percent, &
				ldc_avg_weight

dwbuffer		ldwb_buffer

integer		li_sequence

character	lc_char_key[4], &
				lc_species[1], &
				lc_grade[1], &
				lc_weight_range[1], &
				lc_yield_grade[1], &
				lc_hormone_free[1], &
				lc_sex[1]
				
string		ls_tab, &
				ls_cr

IF as_tpassrce_ind = "D" THEN
	ldwb_buffer = delete!
ELSE
	ldwb_buffer = primary!
END IF

li_sequence				= dw_weight_distribution.GetItemNumber( al_Row, "sequence", ldwb_buffer, False  )
lc_char_key				= dw_weight_distribution.GetItemString( al_Row, "char_key", ldwb_buffer, False  )
lc_species				= dw_weight_distribution.GetItemString( al_Row, "species", ldwb_buffer, False  )
lc_grade					= dw_weight_distribution.GetItemString( al_Row, "grade", ldwb_buffer, False  )
lc_weight_range		= dw_weight_distribution.GetItemString( al_Row, "weight_range", ldwb_buffer, False  )
lc_yield_grade			= dw_weight_distribution.GetItemString( al_Row, "yield_grade", ldwb_buffer, False  )
lc_hormone_free		= dw_weight_distribution.GetItemString( al_Row, "hormone_free", ldwb_buffer, False  )
lc_sex					= dw_weight_distribution.GetItemString( al_Row, "sex", ldwb_buffer, False  )
ld_begin_date			= Date(dw_weight_distribution.GetItemDateTime( al_Row, "begin_date", ldwb_buffer, False  ))
ld_end_date				= Date(dw_weight_distribution.GetItemDateTime( al_Row, "end_date", ldwb_buffer, False  ))
ldc_percent				= dw_weight_distribution.GetItemNumber( al_Row, "percent", ldwb_buffer, False  )
ldc_avg_weight			= dw_weight_distribution.GetItemNumber( al_Row, "avg_weight", ldwb_buffer, False  )
ls_tab					= "~t"
ls_cr						= "~r~n"

is_update_source	+=	string(li_sequence, "0000") + ls_tab + lc_char_key + ls_tab + &
						lc_species + ls_tab + lc_grade + ls_tab + lc_weight_range + ls_tab + &
						lc_yield_grade + ls_tab + lc_hormone_free + ls_tab + lc_sex + ls_tab + &
						string(ld_begin_date, "yyyy-mm-dd" )+ ls_tab + &
						string(ld_end_date, "yyyy-mm-dd") + ls_tab + &
						string(ldc_percent ) + ls_tab + &
						string(ldc_avg_weight ) + ls_tab + &
						as_tpassrce_ind + ls_cr

ii_update_rec_occurs ++

If ii_update_rec_occurs = 100 Then
//	If Not iu_pas203.nf_upd_wt_dist ( istr_error_info, as_header, is_update_source ) THEN Return( False )
	If Not iu_ws_pas1.nf_pasp26fr ( istr_error_info, as_header, is_update_source ) THEN Return( False )
	ii_update_rec_occurs = 0
	is_update_source = ""
End if

Return( True )
end function

public subroutine wf_species_filter (string as_sex);IF as_sex = 'B' THEN		
	idwc_sex.SetFilter ( "record_type='BEEFSEX'" )
ELSEIF as_sex = 'P' THEN
	idwc_sex.SetFilter ( "record_type='PORKSEX'" )
ELSE
	idwc_sex.SetFilter ( "record_type=''" )
END IF

idwc_sex.Filter ( )
// TJB 01/15/96 Added Sort
idwc_sex.Sort()
end subroutine

public function boolean wf_validate (long as_row);Decimal			ld_percent


ld_percent = dw_weight_distribution.GetItemDecimal &
				(as_row, "percent")

If dw_weight_distribution.AcceptText() < 0 Then Return False

If iw_frame.iu_string.nf_IsEmpty(dw_weight_distribution.GetItemString &
				(as_row, "species")) Then
	dw_weight_distribution.ScrollToRow(as_row)
	dw_weight_distribution.SetColumn("species")
	dw_weight_distribution.SetFocus()
	dw_weight_distribution.TriggerEvent(ItemError!)
	Return False
End If

If iw_frame.iu_string.nf_IsEmpty(dw_weight_distribution.GetItemString &
					(as_row, "hormone_free")) Then
	dw_weight_distribution.ScrollToRow(as_row)
	dw_weight_distribution.SetColumn("hormone_free")
	dw_weight_distribution.SetFocus()
	dw_weight_distribution.TriggerEvent(ItemError!)
	Return False
End If

If IsNull(dw_weight_distribution.GetItemDecimal &
				(as_row, "avg_weight")) Then
	dw_weight_distribution.ScrollToRow(as_row)
	dw_weight_distribution.SetColumn("avg_weight")
	dw_weight_distribution.SetFocus()
	dw_weight_distribution.TriggerEvent(ItemError!)
	Return False
End If

If IsNull(ld_percent) or ld_percent <= 0 Then
	dw_weight_distribution.ScrollToRow(as_row)
	dw_weight_distribution.SetColumn("percent")
	dw_weight_distribution.SetFocus()
	dw_weight_distribution.TriggerEvent(ItemError!)
	Return False
End If

Return True
end function

public function boolean wf_validate_end (ref long al_row);Date					ldt_end_date, &
						ldt_new_end_date

Integer				li_sequence

Long					ll_group_break, &
						ll_rtn, &
						ll_mrtn, &
						ll_rowcount, &
						ll_row, &
						ll_newrow, &
						ll_groupcount

String				ls_species, &
						ls_grade, &
						ls_weight_range, &
						ls_yield_grade, &
						ls_hormone_free, &
						ls_sex, &
						ls_searchstring, &
						ls_begin_date, &
						ls_char_key, &
						ls_new_char_key

ll_rowcount = dw_weight_distribution.RowCount()

If ll_rowcount < 2 Then Return True

DO WHILE ll_Row <= ll_rowcount 
	ll_Row = dw_weight_distribution.GetNextModified(ll_Row, Primary!)
	
	IF ll_Row > 0 THEN 
		ls_species = dw_weight_distribution.GetItemString(ll_row, "species")
		ls_grade = dw_weight_distribution.GetItemString(ll_row, "grade")
		ls_weight_range = dw_weight_distribution.GetItemString &
															(ll_row, "weight_range")
		ls_yield_grade = dw_weight_distribution.GetItemString &
															(ll_row, "yield_grade")
		ls_hormone_free = dw_weight_distribution.GetItemString &
															(ll_row, "hormone_free")
		ls_sex = dw_weight_distribution.GetItemString(ll_row, "sex")
		ls_char_key = dw_weight_distribution.GetItemString(ll_row, "char_key")
		ls_begin_date = String(Date(dw_weight_distribution.GetItemDateTime &
															(ll_row, "begin_date")))
		ldt_end_date = Date(dw_weight_distribution.GetItemDateTime &
															(ll_row, "end_date"))

		ls_SearchString	= "species = '" + ls_species + &
									"' and grade = '" + ls_grade + &
									"' and weight_range = '" + ls_weight_range + &
									"' and yield_grade = '" + ls_yield_grade + &
									"' and hormone_free = '" + ls_hormone_free + &
									"' and sex = '" + ls_sex + &
									"' and Date(begin_date) = Date('" + ls_begin_date + "')"
	
	// Find a matching row excluding the current row.
	
		ll_rtn = 0
		ll_group_break = 0
	
		Do 
			ll_group_break = dw_weight_distribution.FindGroupChange &
									(ll_group_break, 1)
			If ll_group_break = 0 Then Exit
			If ll_group_break <> ll_row Then ll_rtn =  dw_weight_distribution.Find &
					(ls_searchstring, ll_group_break, ll_group_break)
			If ll_rtn <= 0 Then ll_group_break ++
		LOOP WHILE ll_rtn <= 0

		If ll_rtn > 0 Then
			
			ls_new_char_key = dw_weight_distribution.GetItemString &	
											(ll_group_break, "char_key")
			If ls_new_char_key <> ls_char_key Then
				dw_weight_distribution.SetRedraw(False)
				ll_groupcount = dw_weight_distribution.GetItemNumber &	
													(ll_group_break, "group_count") 
				li_sequence = dw_weight_distribution.GetItemNumber &	
													(ll_group_break, "group_count")
				ldt_new_end_date = Date(dw_weight_distribution.GetItemDateTime &
														(ll_group_break, "end_date"))
				If ldt_end_date <> ldt_new_end_date Then
					dw_weight_distribution.SetItem(ll_row, "end_date", ldt_new_end_date)
				End If	
				dw_weight_distribution.SetItem(ll_row, "char_key", ls_new_char_key)
				dw_weight_distribution.SetItem(ll_row, "sequence", li_sequence+ 1)
				dw_weight_distribution.sort()
				dw_weight_distribution.groupcalc()
				ll_newrow = ll_group_break + ll_groupcount
				dw_weight_distribution.Setcolumn("percent")
				dw_weight_distribution.SetRow(ll_row)				
				dw_weight_distribution.SetRedraw(True)
				al_row = ll_row
				Return False
			End If
		End If
	Else
		ll_row = ll_rowcount + 1
	End If
LOOP

Return True
end function

public subroutine wf_filenew ();DateTime				ldt_date

long					ll_NewRow


If iw_frame.iu_string.nf_IsEmpty(dw_plant.uf_get_plant_code()) or &
		iw_frame.iu_string.nf_IsEmpty(dw_fab_product_code.uf_get_product_code()) or &
		iw_frame.iu_string.nf_IsEmpty(dw_fab_product_code.uf_get_product_state()) or &
		iw_frame.iu_string.nf_IsEmpty(dw_fab_product_code.uf_get_product_state()) Then
	iw_frame.SetMicroHelp("Please reinquire and enter a Plant" + &
					", Product, State, and Status before adding a row")
	Return 
End If

ldt_date = DateTime(Today())

dw_weight_distribution.SetReDraw( False )
ll_NewRow = dw_weight_distribution.InsertRow(0)
dw_weight_distribution.SetItem( ll_NewRow, "char_key", '0' )
dw_weight_distribution.SetItem( ll_NewRow, "begin_date", ldt_date)
dw_weight_distribution.SetItem( ll_NewRow, "sequence", 1 )
dw_weight_distribution.SetReDraw( True )

dw_weight_distribution.ScrollToRow( ll_NewRow )
dw_weight_distribution.SetFocus()
dw_weight_distribution.SetColumn( "species" )

Return 
end subroutine

public function boolean wf_fixenddates (long al_row);String	ls_searchstring
Long		ll_rowcount, &
			ll_findrow
Date		ldt_Value

ls_searchstring = "species = '" + dw_weight_distribution.GetItemString( al_row, "species" ) + &
						"' and grade = '" + dw_weight_distribution.GetItemString( al_row, "grade" ) + &
						"' and weight_range = '" + dw_weight_distribution.GetItemString( al_row, "weight_range" ) + &
						"' and yield_grade = '" + dw_weight_distribution.GetItemString( al_row, "yield_grade" ) + &
						"' and hormone_free = '" + dw_weight_distribution.GetItemString( al_row, "hormone_free" ) + &
						"' and sex = '" + dw_weight_distribution.GetItemString( al_row, "sex" ) + &
						"' and begin_date > DateTime('" + String(dw_weight_distribution.GetItemDateTime( al_row, "begin_date" )) + &
						"') and begin_date < DateTime('" + String(dw_weight_distribution.GetItemDateTime( al_row, "end_date" )) + "')"
ll_rowcount = dw_weight_distribution.RowCount()
ll_findrow = dw_weight_distribution.Find(ls_searchstring, 0, ll_rowcount + 1)

IF ll_findrow > 0 Then
	ldt_value = RelativeDate(Date(String(dw_weight_distribution.GetItemDateTime(ll_findrow, "begin_date"), "yyyy-mm-dd")), -1)
	dw_weight_distribution.SetItem(al_row,"end_date", ldt_value)
End If

Return True

end function

public function boolean wf_addrow ();DateTime				ldt_begin_date, &
						ldt_end_date

Integer				li_sequence

long					ll_OldRow, &
						ll_NewRow, &
						ll_rowcount, &
						ll_findrow

string				ls_species, &
						ls_grade, &
						ls_weight_range, &
						ls_yield_grade, &
						ls_hormone_free, &
						ls_sex, &
						ls_char_key, &
						ls_SearchString

If iw_frame.iu_string.nf_IsEmpty(dw_plant.uf_get_plant_code()) or &
		iw_frame.iu_string.nf_IsEmpty(dw_fab_product_code.uf_get_product_code()) or &
		iw_frame.iu_string.nf_IsEmpty(dw_fab_product_code.uf_get_product_state()) or &
		iw_frame.iu_string.nf_IsEmpty(dw_fab_product_code.uf_get_product_status()) Then
	iw_frame.SetMicroHelp("Please reinquire and enter a Plant" + &
					", Product, State, and Status before adding a row")
	Return False
End If

// Changed to fix error if no rows present rem

ll_OldRow = dw_weight_distribution.GetRow()
If ll_OldRow > 0 Then
	// find the last row in the group
	ls_species			=	dw_weight_distribution.GetItemString( ll_OldRow, "species" )
	ls_grade				=	dw_weight_distribution.GetItemString( ll_OldRow, "grade" )
	ls_weight_range	=	dw_weight_distribution.GetItemString( ll_OldRow, "weight_range" )
	ls_yield_grade		=	dw_weight_distribution.GetItemString( ll_OldRow, "yield_grade" )
	ls_hormone_free	=	dw_weight_distribution.GetItemString( ll_OldRow, "hormone_free" )
	ls_sex				=	dw_weight_distribution.GetItemString( ll_OldRow, "sex" )
	ldt_begin_date		=	dw_weight_distribution.GetItemDateTime( ll_OldRow, "begin_date" )
	ldt_end_date		=	dw_weight_distribution.GetItemDateTime( ll_OldRow, "end_date" )
	ls_char_key			=	dw_weight_distribution.GetItemString( ll_OldRow, "char_key" )
	li_sequence			=	dw_weight_distribution.GetItemNumber( ll_OldRow, "group_count" )		

	ls_searchstring = "species = '" + ls_species + &
							"' and grade = '" + ls_grade + &
							"' and weight_range = '" + ls_weight_range + &
							"' and yield_grade = '" + ls_yield_grade + &
							"' and hormone_free = '" + ls_hormone_free + &
							"' and sex = '" + ls_sex + &
							"' and begin_date = DateTime('" + String(ldt_begin_date) + &
							"') and end_date = DateTime('" + String(ldt_end_date) + &
							"') and char_key = '" + ls_char_key + "'"
	
	ll_rowcount = dw_weight_distribution.RowCount()
	ll_findrow = dw_weight_distribution.FIND(ls_searchstring, 1, ll_rowcount + 1)
	
	Do While ll_findrow > 0
		ll_OldRow = ll_findrow
		ll_findrow = dw_weight_distribution.FIND(ls_searchstring, ll_findrow + 1, ll_rowcount + 1)
	loop
End If

dw_weight_distribution.SetReDraw( False )
ll_NewRow = dw_weight_distribution.InsertRow( ll_OldRow + 1 )
If ll_OldRow > 0 Then
	dw_weight_distribution.SetItem( ll_NewRow, "species", ls_species )
	dw_weight_distribution.SetItem( ll_NewRow, "grade", ls_grade )
	dw_weight_distribution.SetItem( ll_NewRow, "weight_range", ls_weight_range )
	dw_weight_distribution.SetItem( ll_NewRow, "yield_grade", ls_yield_grade )
	dw_weight_distribution.SetItem( ll_NewRow, "hormone_free", ls_hormone_free )
	dw_weight_distribution.SetItem( ll_NewRow, "sex", ls_sex )
	dw_weight_distribution.SetItem( ll_NewRow, "char_key", ls_char_key )
	dw_weight_distribution.SetItem( ll_NewRow, "sequence", li_sequence + 1 )
	dw_weight_distribution.SetColumn('avg_weight')
Else
	dw_weight_distribution.SetColumn('species')
End if

// Added to initialize the dates
If ll_newrow = 1 Then
	ldt_begin_date = DateTime(Today())
	ldt_end_date = DateTime(Date("2999-12-31"))
End If

dw_weight_distribution.SetItem( ll_NewRow, "begin_date", ldt_begin_date )
dw_weight_distribution.SetItem( ll_NewRow, "end_date", ldt_end_date )
dw_weight_distribution.SetReDraw( True )

dw_weight_distribution.ScrollToRow( ll_NewRow )
dw_weight_distribution.SetFocus()

Return( true )
end function

public function boolean wf_update ();integer			li_rc

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					al_row 

string			ls_tpassrce_ind , &
					ls_header, &
					ls_temp, &
					ls_tab = "~t", &
					ls_cr	= "~r~n", &
					ls_plant, &
					ls_product_code, &
					ls_product_state, &
					ls_product_status


dwItemStatus	lis_status

IF dw_weight_distribution.AcceptText() = -1 THEN Return( False )
IF dw_weight_distribution.ModifiedCount() + dw_weight_distribution.DeletedCount() <= 0 THEN Return( False )

ls_plant				=	dw_plant.uf_get_plant_code ( )
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	Return False
End If

ls_product_code	=	dw_fab_product_code.uf_get_product_code( )
If iw_frame.iu_string.nf_IsEmpty(ls_product_code) Then 
	iw_frame.SetMicroHelp("Product is a required field")
	Return False
End If

ls_product_state	=	dw_fab_product_code.uf_get_product_state( )
If iw_frame.iu_string.nf_IsEmpty(ls_product_state) Then 
	iw_frame.SetMicroHelp("Product State is a required field")
	Return False
End If

ls_product_status	=	dw_fab_product_code.uf_get_product_status( )
If iw_frame.iu_string.nf_IsEmpty(ls_product_status) Then 
	iw_frame.SetMicroHelp("Product Status is a required field")
	Return False
End If

ls_header			=	ls_plant + ls_tab + ls_product_code + ls_tab + ls_product_state + &
							ls_tab + ls_product_status + ls_cr
ll_NbrRows 			=	dw_weight_distribution.RowCount( )

ib_updating = True

If Not wf_validate_end(al_row) Then 
	MessageBox("Duplicate Weight Distributions", + &
			"Characteristics for each weight distribution must be unique.") 
	dw_weight_distribution.SetRow(al_row)				
	dw_weight_distribution.Setcolumn("percent")
	Return False
End If
IF NOT wf_checkpercenttotals ( ) THEN Return( False )

iw_frame.SetMicroHelp("Wait... Updating the Database")

is_update_source = ""
ii_update_rec_occurs = 0

IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF

// UPDATE THE DELETED ROWS
ll_Deleted_Count = dw_weight_distribution.DeletedCount()
FOR ll_Row = 1 to ll_Deleted_Count
	IF NOT wf_upd_wt_dist ( ll_Row, ls_header, 'D' ) THEN
		Return( False )
	END IF
Next

// UPDATE THE Rest of the ROWS

ll_row = 0
DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_weight_distribution.GetNextModified(ll_Row, Primary!)

	IF ll_Row > 0 THEN 
//  Added next 2 line to validate the data
		If Not wf_validate(ll_row) Then Return False
		lis_status = dw_weight_distribution.GetItemStatus( ll_Row, 0, Primary! )
		CHOOSE CASE lis_status
			CASE NewModified!
				ls_tpassrce_ind = 'A'
			CASE DataModified!
				ls_tpassrce_ind = 'M'
			CASE ELSE
				CONTINUE
		END CHOOSE		
		IF NOT wf_upd_wt_dist ( ll_Row, ls_header, ls_tpassrce_ind ) THEN
			Return( False )
		END IF
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

// RESET THE DATAWINDOWS STATUSES

IF ii_update_rec_occurs > 0 THEN
	dw_weight_distribution.SetRedraw(False)
//	IF iu_pas203.nf_upd_wt_dist(istr_error_info, ls_header, is_update_source) THEN
	IF iu_ws_pas1.nf_pasp26fr(istr_error_info, ls_header, is_update_source) THEN
		dw_weight_distribution.ResetUpdate()
		ib_re_inquire = True
		wf_retrieve()		
		iw_frame.SetMicroHelp("Modification Successful")
	END IF
	dw_weight_distribution.SetRedraw(True)
END IF

Return( True )
end function

public function boolean wf_retrieve ();int				li_rc

long				ll_row, &
					ll_rowcount, &
					ll_value 

boolean			lb_return

String			ls_header_string,&
					ls_plant,&
					ls_product,&
					ls_product_desc,&
					ls_product_state,&
					ls_product_state_desc,&
					ls_product_status,&
					ls_product_status_desc,&					
					ls_BeginDate
					
string			ls_input, &
					ls_source

Call w_base_sheet::closequery

IF NOT ib_re_inquire THEN
	OpenWithParm(w_weight_distribution_inq, This)
	ls_header_string = Message.StringParm 
	
	If iw_frame.iu_string.nf_IsEmpty(ls_header_string) Then
		return false
	End If
	
	ls_plant						= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product					= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_desc			= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_state			= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_state_desc	= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_status			= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_product_status_desc	= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	ls_BeginDate				= iw_frame.iu_string.nf_gettoken(ls_header_string, "~t")
	
	dw_plant.uf_set_plant_code(ls_plant)
	dw_fab_product_code.uf_importstring(ls_product, ls_product_desc, &
													ls_product_state, ls_product_state_desc, &
													ls_product_status, ls_product_status_desc, True)
				
	dw_begin_end_date.SetItem( 1, "begin_date", Date(ls_BeginDate))
Else
	ls_plant 			= dw_plant.uf_get_plant_code( )
	ls_product			= dw_fab_product_code.uf_get_product_code( )
	ls_product_state	= dw_fab_product_code.uf_get_product_state( )
	ls_product_status	= dw_fab_product_code.uf_get_product_status( )	
	ls_BeginDate		= string(dw_begin_end_date.getItemDate( 1, "begin_date"))
END IF
				
iw_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(Hourglass!)
	
//dw_weight_distribution.uf_ChangeRowStatus(1, NotModified!)

istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_pasp25br"
istr_error_info.se_message				=  Space(70)

ls_input		= Space(27)
ls_source	= Space(6101)
ls_input		= ls_plant + "~t" + ls_product + "~t" + &
				  ls_product_state + "~t" + ls_product_status + "~t" + ls_BeginDate + "~t"
				  
//lb_return	= iu_pas203.nf_inq_wt_dist( istr_Error_Info, ls_input, ls_source )
lb_return	= iu_ws_pas1.nf_pasp25fr( istr_Error_Info, ls_input, ls_source )
 
IF ib_re_inquire THEN 	ib_re_inquire = False

IF NOT lb_return THEN 	Return( False )

This.SetRedraw( False )
ib_Inquiring = True
dw_weight_distribution.Reset()
ll_value = dw_weight_distribution.ImportString(ls_source)
dw_weight_distribution.ResetUpdate()
IF dw_weight_distribution.RowCount() > 0 THEN
	wf_sort()
	dw_weight_distribution.SetFocus()
	dw_weight_distribution.ScrollToRow(1)
	dw_weight_distribution.TriggerEvent("RowFocusChanged")
	iw_frame.SetMicroHelp(String(ll_value) + " rows retrieved") // iw_frame. added rem
END IF

IF ll_value <= 0 Then iw_frame.SetMicroHelp("0" + " rows retrieved") // line added rem
ib_Inquiring = False
This.SetRedraw( True )
Return( True )
 
end function

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

Destroy iu_ws_pas1
end event

event ue_postopen;call super::ue_postopen;integer		li_rc

Long			ll_rtn


IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF

If Message.ReturnValue = -1 Then
	Close(This)
	Return
End If

iu_ws_pas1 = Create u_ws_pas1

istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "w_weight_distribution"
istr_error_info.se_user_id 		=  sqlca.userid

li_rc	=	dw_weight_distribution.GetChild( "species", idwc_species )
li_rc	=	dw_weight_distribution.GetChild( "grade", idwc_grade )
li_rc	=	dw_weight_distribution.GetChild( "weight_range", idwc_weight_range )
li_rc	=	dw_weight_distribution.GetChild( "yield_grade", idwc_yield_grade )
li_rc	=	dw_weight_distribution.GetChild( "hormone_free", idwc_hormone_free )
li_rc	=	dw_weight_distribution.GetChild( "sex", idwc_sex )

idwc_species.SetTransObject(SQLCA)
idwc_species.Retrieve("SPECIES")
idwc_grade.SetTransObject(SQLCA)
idwc_grade.Retrieve("GRADE")
idwc_weight_range.SetTransObject(SQLCA)
idwc_weight_range.Retrieve("WGTRANGE")
idwc_yield_grade.SetTransObject(SQLCA)
idwc_yield_grade.Retrieve("YLDGRADE")
idwc_hormone_free.SetTransObject(SQLCA)
idwc_hormone_free.Retrieve("HORMONE")

DataStore	lds_sex
lds_sex = Create DataStore
lds_sex.DataObject = "d_tutltype_dddw"
lds_sex.SetTransObject(SQLCA)
lds_sex.Retrieve("BEEFSEX")
idwc_sex.ImportString(lds_sex.Describe("DataWindow.Data"))
lds_sex.Retrieve("PORKSEX")
idwc_sex.ImportString(lds_sex.Describe("DataWindow.Data"))
Destroy lds_sex

//idwc_sex.SetTransObject(SQLCA)
//idwc_sex.Retrieve("BEEFSEX")
//idwc_sex.Retrieve("PORKSEX")

//Added this code to add a space if type_code is blank

ll_rtn = idwc_grade.Find('Len(type_code) = 0 or IsNull(type_code)', &
				1, idwc_grade.RowCount())
If ll_rtn > 0 Then idwc_grade.SetItem(ll_rtn, "type_code", " ")

ll_rtn = idwc_weight_range.Find('Len(type_code) = 0 or IsNull(type_code)', &
				1, idwc_weight_range.RowCount())
If ll_rtn > 0 Then idwc_weight_range.SetItem(ll_rtn, "type_code", " ")

ll_rtn = idwc_yield_grade.Find('Len(type_code) = 0 or IsNull(type_code)', &
				1, idwc_yield_grade.RowCount())
If ll_rtn > 0 Then idwc_yield_grade.SetItem(ll_rtn, "type_code", " ")

ll_rtn = idwc_sex.Find('Len(type_code) = 0 or IsNull(type_code)', &
				1, idwc_sex.RowCount())
If ll_rtn > 0 Then idwc_sex.SetItem(ll_rtn, "type_code", " ")


// TJB 01/15/96  Added Sorts so the dddws will always be in the same order
idwc_species.Sort()
idwc_grade.Sort()
idwc_weight_range.Sort()
idwc_yield_grade.Sort()
idwc_hormone_free.Sort()

wf_species_filter( "" )

This.PostEvent("ue_query")

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event resize;call super::resize;integer li_x		= 40
integer li_y		= 430


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


dw_weight_distribution.width	= width - li_x
dw_weight_distribution.height	= height - li_y


end event

on w_weight_distribution.create
int iCurrent
call super::create
this.dw_fab_product_code=create dw_fab_product_code
this.dw_begin_end_date=create dw_begin_end_date
this.dw_weight_distribution=create dw_weight_distribution
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fab_product_code
this.Control[iCurrent+2]=this.dw_begin_end_date
this.Control[iCurrent+3]=this.dw_weight_distribution
this.Control[iCurrent+4]=this.dw_plant
end on

on w_weight_distribution.destroy
call super::destroy
destroy(this.dw_fab_product_code)
destroy(this.dw_begin_end_date)
destroy(this.dw_weight_distribution)
destroy(this.dw_plant)
end on

type dw_fab_product_code from u_fab_product_code within w_weight_distribution
integer x = 23
integer y = 96
integer height = 288
integer taborder = 30
end type

event constructor;call super::constructor;this.uf_enable(False)
end event

type dw_begin_end_date from u_source_begin_end_date within w_weight_distribution
integer x = 1797
integer y = 16
integer width = 677
integer height = 88
integer taborder = 0
end type

on rowfocuschanged;call u_source_begin_end_date::rowfocuschanged;This.ResetUpdate()

end on

event constructor;call super::constructor;ib_Updateable = False
TriggerEvent( "ue_insertrow" )
uf_setbegindate ( today() )
uf_setenddate ( RelativeDate(Today(), 28))
This.ResetUpdate()

Modify("begin_date.Protect='0' begin_date.Background.Color='67108864'")
Modify("end_date.Protect='0' end_date.Background.Color='67108864'")
end event

type dw_weight_distribution from u_base_dw_ext within w_weight_distribution
integer y = 388
integer width = 2523
integer height = 1176
integer taborder = 30
string dataobject = "d_weight_distribution"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemerror;call super::itemerror;String		ls_text, &
				ls_error_msg, &
				ls_error_ttl
Long			ll_row
Date			ldt_value, ldt_end


ls_text = GetText()
ll_row = GetRow()

Choose Case GetColumnName()

	Case "species"
		ls_error_msg = "Species is a Required Field"
		ls_error_ttl = "Species"

	Case "hormone_free"
		ls_error_msg = "Hormone Free is a Required Field"
		ls_error_ttl = "Hormone Free"

	Case "begin_date"
		ldt_value = Date(left(ls_text,pos(ls_text, " ")))
		If ldt_value < Today() Then
			ls_error_msg = "Begin Date must be" + &
				" greater than or equal to today" 
			ls_error_ttl = "Begin Date"
		Else
			If ldt_value > Date(This.GetItemDateTime(ll_row, "end_date")) Then
				ls_error_msg = "Begin Date must be less than" + &
					" the End Date"
				ls_error_ttl = "Begin Date"
			End IF
		End If

	Case "avg_weight"
		If IsNull(GetItemDecimal(ll_row, "avg_weight")) Then
			ls_error_msg = "Average Weight is a Required Field"
		Else
			ls_error_msg = "Average Weight must be a positive number"
		End If
		ls_error_ttl = "Average Weight"

	Case "percent"
		ls_error_msg = "Percent must be greater than zero"
		ls_error_ttl = "Percent"

End Choose

If ib_updating Then
	MessageBox(ls_error_ttl, ls_error_msg + '.')
	ib_updating = False
Else
	iw_frame.SetMicroHelp(ls_error_msg) 
End if
Return 1


end event

event itemchanged;call super::itemchanged;Boolean			lb_lastrow
Date				ldt_value

Decimal			ld_value

Long				ll_findrow, &
					ll_rowcount

String			ls_searchstring
DWItemStatus	ldwis_status	


CHOOSE CASE dwo.Name
	CASE "species"
// Added this line to initialize char_key.
		This.SetItem(row, "char_key", "0000")
		//************  TJB 01/15/96  Added the Trim()  
		wf_species_filter( Trim(Data))

// Added from here on down to validate fields

	Case "grade", "weight_range", "yield_grade", "hormone_free", "sex"
		This.SetItem(row, "char_key", "0000")

	Case "begin_date"
		ldt_value = Date(left(Data,pos(Data, " ")))
		If ldt_value < Today() Then
			Return 1
		Else
			If ldt_value > Date(This.GetItemDateTime(row, "end_date")) Then
				Return 1
			End IF
		End If
	
// rem							"') and end_date = DateTime('" + String(This.GetItemDateTime( row, "end_date" )) + &
//		ls_searchstring = "species = '" + This.GetItemString( row, "species" ) + &
//								"' and grade = '" + This.GetItemString( row, "grade" ) + &
//								"' and weight_range = '" + This.GetItemString( row, "weight_range" ) + &
//								"' and yield_grade = '" + This.GetItemString( row, "yield_grade" ) + &
//								"' and hormone_free = '" + This.GetItemString( row, "hormone_free" ) + &
//								"' and sex = '" + This.GetItemString( row, "sex" ) + &
//								"' and begin_date = DateTime('" + String(ldt_value) + &
//								"') and char_key = '" + This.GetItemString( row, "char_key" ) + "'"
//		ll_rowcount = This.RowCount()
//		ll_findrow = This.Find(ls_searchstring, 1, ll_rowcount + 1)
//		lb_lastrow = false
//		
//		Do while ll_findrow > 0
//			If ll_findrow <> row Then
//				ldwis_status = GetItemStatus(ll_findrow, 0, primary!)
//		 		This.SetItem(ll_findrow, "end_date", RelativeDate(ldt_value, -1))
//				This.uf_changerowstatus(ll_findrow, DataModified!)
////				This.SetItem(row, "end_date", This.GetItemDateTime( ll_findrow, "end_date" ))
//			End If
//			ll_findrow = This.FIND(ls_searchstring, ll_findrow + 1, ll_rowcount + 2)
//			If ll_findrow = ll_rowcount then
//				IF lb_lastrow = true then
//					ll_findrow = 0
//				Else
//					lb_lastrow = true
//				End If
//			End If
//		loop
//
	Case "avg_weight"

		If Dec(Data) < 0 Then
			Return 1
		End If
	Case "percent"
		If Dec(Data) < 0 Then
			Return 1
		End If
		
END CHOOSE

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
end event

on constructor;call u_base_dw_ext::constructor;is_selection = '1'
ib_updating = False
end on

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

event itemfocuschanged;call super::itemfocuschanged;Long		ll_FoundRow

String	ls_SearchString


This.SelectText(1,Len(GetText()))

If ib_Inquiring Then return

If This.GetItemStatus(row, 0, Primary!) <> NewModified! Then return

ls_SearchString	= "species = '" + This.GetItemString(row, "species") + &
							"' and grade = '" + This.GetItemString(row, "grade") + &
							"' and weight_range = '" + This.GetItemString(row, "weight_range") + &
							"' and yield_grade = '" + This.GetItemString(row, "yield_grade") + &
							"' and hormone_free = '" + This.GetItemString(row, "hormone_free") + &
							"' and sex = '" + This.GetItemString(row, "sex") + &
							"' and Date(begin_date) = Date('" + String(Date(This.GetItemDateTime &
													(row, "begin_date"))) + "')"

// the last line has the greatest sequence, so search from the end for matching chars
If row < This.RowCount() Then
	ll_FoundRow = This.Find(ls_SearchString, 10000, row + 1)
End if

if row > 1 And ll_FoundRow < 1 Then
	ll_FoundRow = This.Find(ls_SearchString, row - 1, 1)
End if

If ll_FoundRow > 0 Then
	This.SetItem(row, 'char_key', This.GetItemString(ll_FoundRow, 'char_key'))
	This.SetItem(row, 'sequence', This.GetItemNumber(ll_FoundRow, 'sequence') + 1)
End if
			

end event

event rowfocuschanged;call super::rowfocuschanged;long ll_Row

ll_Row = il_dwrow 

IF ll_Row > 0 THEN wf_species_filter( GetItemString( ll_Row, "species" ) )

//This.GroupCalc()
end event

type dw_plant from u_plant within w_weight_distribution
integer x = 201
integer y = 16
integer width = 1495
integer taborder = 10
end type

event constructor;call super::constructor;ib_Updateable = False
ib_protected = True
TriggerEvent( "ue_insertrow" )


end event

