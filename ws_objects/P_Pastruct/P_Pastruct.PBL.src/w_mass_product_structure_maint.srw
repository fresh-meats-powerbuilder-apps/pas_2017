﻿$PBExportHeader$w_mass_product_structure_maint.srw
forward
global type w_mass_product_structure_maint from w_base_sheet_ext
end type
type dw_begin_end_dates from u_base_dw_ext within w_mass_product_structure_maint
end type
type dw_to_shift from u_shift_all within w_mass_product_structure_maint
end type
type dw_from_shift from u_shift_all within w_mass_product_structure_maint
end type
type dw_delete_char from u_base_dw_ext within w_mass_product_structure_maint
end type
type dw_to_char from u_base_dw_ext within w_mass_product_structure_maint
end type
type to_t from statictext within w_mass_product_structure_maint
end type
type from_plant_t from statictext within w_mass_product_structure_maint
end type
type dw_to_plant from u_plant within w_mass_product_structure_maint
end type
type dw_from_plant from u_plant within w_mass_product_structure_maint
end type
type cb_delete from commandbutton within w_mass_product_structure_maint
end type
type cb_copy from commandbutton within w_mass_product_structure_maint
end type
type dw_from_char from u_base_dw_ext within w_mass_product_structure_maint
end type
type ln_1 from line within w_mass_product_structure_maint
end type
end forward

global type w_mass_product_structure_maint from w_base_sheet_ext
integer x = 41
integer y = 268
integer width = 3689
integer height = 1252
string title = "Mass Product Structure Maintenance"
boolean minbox = false
boolean maxbox = false
long backcolor = 67108864
boolean clientedge = true
event ue_postitemchanged pbm_custom01
event ue_copy pbm_custom70
event ue_delete pbm_custom70
dw_begin_end_dates dw_begin_end_dates
dw_to_shift dw_to_shift
dw_from_shift dw_from_shift
dw_delete_char dw_delete_char
dw_to_char dw_to_char
to_t to_t
from_plant_t from_plant_t
dw_to_plant dw_to_plant
dw_from_plant dw_from_plant
cb_delete cb_delete
cb_copy cb_copy
dw_from_char dw_from_char
ln_1 ln_1
end type
global w_mass_product_structure_maint w_mass_product_structure_maint

type variables
Private:

Int		ii_async_commhandle
						
s_error     		istr_error_info
u_pas203          iu_pas203
u_pas201				iu_pas201
u_ws_pas3		iu_ws_pas3
w_production_pt_parameters		iw_parent


nvuo_fab_product_code invuo_fab_product_code

date        		id_date

Time					it_ChangedTime

Long					il_ChangedRow

String				is_ChangedColumnName, &
						is_inquire_flag

INTEGER				ii_Query_Retry_Count  



end variables

forward prototypes
public function boolean wf_validate (long al_row)
public function boolean wf_validate_copy (long al_row)
public function boolean wf_delete_instance ()
public function boolean wf_validate_delete (long al_row)
public function boolean wf_copy_instance ()
end prototypes

event ue_postitemchanged;//If il_ChangedRow > 0 And il_ChangedRow <= dw_one_box_transfer_maint.RowCount() &
//		And Not iw_frame.iu_string.nf_IsEmpty(is_ChangedColumnName) Then
//	dw_one_box_transfer_maint.SetItem(il_ChangedRow, is_ChangedColumnName, it_ChangedTime)
//End if
////
end event

event ue_copy;CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, OKCancel!)
	CASE 1	// Save Changes
			wf_copy_instance()
		CASE 2	// Do not save changes
			Return
	END CHOOSE
//wf_copy_instance()								
end event

event ue_delete;CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, OKCancel!)
	CASE 1	// Save Changes
			wf_delete_instance()	
		CASE 2	// Do not save changes
			Return
	END CHOOSE




							
end event

public function boolean wf_validate (long al_row);Date					ldt_start_date, &
						ldt_end_date, &
						ldt_found_start_date, &
						ldt_found_end_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows

String				ls_fab_product, &
						ls_ship_plant, &
						ls_recv_plant, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag


//ll_nbrrows = dw_mass_product_structure_maint.RowCount()
//
//ls_temp = dw_mass_product_structure_maint.GetItemString(al_row, "ship_plant")
//IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
//	MessageBox("Ship plant", "Please enter a ship plant.  Ship plant cannot be left blank.")
//	This.SetRedraw(False)
//	dw_mass_product_structure_maint.ScrollToRow(al_row)
//	dw_mass_product_structure_maint.SetColumn("ship_plant")
//	dw_mass_product_structure_maint.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If
//
//ls_temp = dw_mass_product_structure_maint.GetItemString(al_row, "recv_plant")
//IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
//	MessageBox("Recv plant", "Please enter a recv plant.  Recv plant cannot be left blank.")
//	This.SetRedraw(False)
//	dw_mass_product_structure_maint.ScrollToRow(al_row)
//	dw_mass_product_structure_maint.SetColumn("recv_plant")
//	dw_mass_product_structure_maint.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If
//
//// Check that the start date has a value and is > current date and < end date.
//ls_update_flag = dw_mass_product_structure_maint.GetItemString(al_row, "update_flag")
//
//// Check that the end date has a value and is > current date and start date.
//
//If ll_nbrrows < 2 Then Return True
//
//IF al_row < 0 THEN Return True
//
//ls_ship_plant = dw_mass_product_structure_maint.GetItemString(al_row, "ship_plant")
//ls_recv_plant = dw_mass_product_structure_maint.GetItemString(al_row, "recv_plant")
//
//ls_SearchString	= "ship_plant = '" + ls_ship_plant + &
//						"' and recv_plant = '" + ls_recv_plant + "'" 
//// Find a matching row excluding the current row.
//
//CHOOSE CASE al_row 
//	CASE 1
//		ll_rtn = dw_mass_product_structure_maint.Find  &
//				( ls_SearchString, al_row + 1, ll_nbrrows)
//	CASE 2 to (ll_nbrrows - 1)
//		ll_rtn = dw_mass_product_structure_maint.Find ( ls_SearchString, al_row - 1, 1)
//		If ll_rtn = 0 Then ll_rtn = dw_mass_product_structure_maint.Find  &
//			(ls_SearchString, al_row + 1, ll_nbrrows)
//	CASE ll_nbrrows 
//		ll_rtn = dw_mass_product_structure_maint.Find ( ls_SearchString, al_row - 1, 1)
//END CHOOSE
////
//
//If ll_rtn > 0 Then
////		MessageBox ("There are duplicate rows with the" + &
////  					" same Ship Plant and Receiving Plant.") 
//		iw_Frame.SetMicroHelp("There are duplicate rows with the" + &
//  					" same Ship Plant and Receiving Plant.") 
//		dw_mass_product_structure_maint.SetRedraw(False)
//		dw_mass_product_structure_maint.ScrollToRow(al_row)
//		dw_mass_product_structure_maint.SetColumn("ship_plant")
//		dw_mass_product_structure_maint.SetRow(al_row)
//		dw_mass_product_structure_maint.SelectRow(ll_rtn, True)
//		dw_mass_product_structure_maint.SelectRow(al_row, True)
//		dw_mass_product_structure_maint.SetRedraw(True)
//		Return False
//End if
////
Return True
end function

public function boolean wf_validate_copy (long al_row);Long					ll_rtn, &
						ll_nbrrows
						
integer				li_char_key

String				ls_searchstring, &
						ls_temp, &
						ls_from_shift, &
						ls_to_shift
						
						
ls_temp = dw_from_plant.GetItemString(al_row, "location_code")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	iw_frame.SetMicroHelp("Please enter a From Plant.  From plant cannot be left blank.")
	This.SetRedraw(False)
	dw_from_plant.ScrollToRow(al_row)
	dw_from_plant.SetColumn("location_code")
	dw_from_plant.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_from_shift = dw_from_shift.GetItemString(al_row, "shift")
IF IsNull(ls_from_shift) or Len(trim(ls_from_shift)) = 0 Then 
	iw_frame.SetMicroHelp("Please enter a From Shift.  From shift cannot be left blank.")
	This.SetRedraw(False)
	dw_from_shift.ScrollToRow(al_row)
	dw_from_shift.SetColumn("shift")
	dw_from_shift.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = string(dw_from_char.GetItemNumber(al_row, "char_key"))
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	iw_frame.SetMicroHelp("Please enter a From Characteristic.  From characteristic cannot be left blank.")
	This.SetRedraw(False)
	dw_from_char.ScrollToRow(al_row)
	dw_from_char.SetColumn("char_key")
	dw_from_char.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_to_plant.GetItemString(al_row, "location_code")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	iw_frame.SetMicroHelp("Please enter a From Plant.  From plant cannot be left blank.")
	This.SetRedraw(False)
	dw_to_plant.ScrollToRow(al_row)
	dw_to_plant.SetColumn("location_code")
	dw_to_plant.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_to_shift = dw_to_shift.GetItemString(al_row, "shift")
IF IsNull(ls_to_shift) or Len(trim(ls_to_shift)) = 0 Then 
	iw_frame.SetMicroHelp("Please enter a From Shift.  From shift cannot be left blank.")
	This.SetRedraw(False)
	dw_to_shift.ScrollToRow(al_row)
	dw_to_shift.SetColumn("shift")
	dw_to_shift.SetFocus()
	This.SetRedraw(True)
	Return False
End If
ls_temp = string(dw_to_char.GetItemNumber(al_row, "char_key"))
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	iw_frame.SetMicroHelp("Please enter a From Characteristic.  From characteristic cannot be left blank.")
	This.SetRedraw(False)
	dw_to_char.ScrollToRow(al_row)
	dw_to_char.SetColumn("char_key")
	dw_to_char.SetFocus()
	This.SetRedraw(True)
	Return False
End If

if ls_from_shift = 'ALL' then
	if ls_to_shift = 'ALL' then
	else
		iw_frame.SetMicroHelp("If from shift is ALL then to shift must also be ALL.")
		This.SetRedraw(False)
		dw_to_shift.ScrollToRow(al_row)
		dw_to_shift.SetColumn("shift")
		dw_to_shift.SetFocus()
		This.SetRedraw(True)
		Return False
	end if	
else		
	IF ls_to_shift =  'ALL' THEN
		IF ls_from_shift = 'ALL' THEN
		else	
			iw_frame.SetMicroHelp("If to shift is ALL then from shift must also be ALL.")
			This.SetRedraw(False)
			dw_from_shift.ScrollToRow(al_row)
			dw_from_shift.SetColumn("shift")
			dw_from_shift.SetFocus()
			This.SetRedraw(True)
			Return False
		end if
	end if
end if
	


Return True
end function

public function boolean wf_delete_instance ();Integer		li_counter

Long			ll_rowcount, &
				ll_delete_char_key
		
String		ls_update_string, &
				ls_from_plant
				
boolean		lb_return
				
				
dw_from_Plant.SetReDraw(False)

If Not wf_validate_delete(1) Then
			dw_from_plant.SetReDraw(True)
	Return False
End If
//
ll_delete_char_key = dw_delete_char.GetItemNumber(1, 'char_key')
ls_update_string = ' '+ '~t' + &
			' '+ '~t' + &
			string(dw_delete_char.GetItemNumber(1, 'char_key'))+ '~t' + &
			' '+ '~t' + &
			' '+ '~t' + &
			' '+ '~t' + &
			string(dw_begin_end_dates.GetItemDate(1, "begin_date"),"yyyy-mm-dd") + '~t' + &
			string(dw_begin_end_dates.GetItemDate(1, "end_date"),"yyyy-mm-dd") + '~t' + &			
			'D~r~n'

istr_error_info.se_event_name = "wf_copy_instance"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp69cr_upd_mass_product_structure"
istr_error_info.se_message = Space(71)

//lb_return = iu_pas201.nf_pasp69cr_upd_mass_product_structure(istr_error_info, ls_Update_string)
lb_return = iu_ws_pas3.uf_pasp69gr(istr_error_info, ls_Update_string)
//If not iu_pas201.nf_pasp69cr_upd_mass_product_structure(istr_error_info, ls_Update_string) Then 
//	dw_from_plant.SetReDraw(True)
//	Return False
//End If
//
iw_frame.SetMicroHelp("Delete Successful")

//ib_ReInquire = True
//wf_retrieve()

dw_from_plant.SetFocus()
dw_from_plant.SetReDraw(True)

Return( True )

end function

public function boolean wf_validate_delete (long al_row);Long					ll_rtn, &
						ll_nbrrows
						
integer				li_char_key

String				ls_searchstring, &
						ls_temp, &
						ls_from_shift, &
						ls_to_shift
						
						
ls_temp = string(dw_delete_char.GetItemNumber(al_row, "char_key"))
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	iw_frame.SetMicroHelp("Please enter a valid Characteristic to delete.")
	This.SetRedraw(False)
	dw_delete_char.ScrollToRow(al_row)
	dw_delete_char.SetColumn("char_key")
	dw_delete_char.SetFocus()
	This.SetRedraw(True)
	Return False
End If

Return True
end function

public function boolean wf_copy_instance ();Integer		li_counter

Long			ll_rowcount, &
				ll_from_char_key
		
String		ls_update_string, &
				ls_from_plant
				
boolean		lb_return
				
				
dw_from_Plant.SetReDraw(False)

If Not wf_validate_copy(1) Then
			dw_from_plant.SetReDraw(True)
	Return False
End If

If dw_begin_end_dates.AcceptText() = -1 Then Return False
 

ls_from_plant = dw_from_plant.GetItemString(1, 'location_code')
ll_from_char_key = dw_to_char.GetItemNumber(1, 'char_key')
ls_update_string = dw_from_plant.GetItemString(1, 'location_code')+ '~t' + &
			dw_from_shift.GetItemString(1, 'shift')+ '~t' + &
			string(dw_from_char.GetItemNumber(1, 'char_key'))+ '~t' + &
			dw_to_plant.GetItemString(1, 'location_code')+ '~t' + &
			dw_to_shift.GetItemString(1, 'shift')+ '~t' + &
			string(dw_to_char.GetItemNumber(1, 'char_key'))+ '~t' + &
			string(dw_begin_end_dates.GetItemDate(1, "begin_date"),"yyyy-mm-dd") + '~t' + &
			string(dw_begin_end_dates.GetItemDate(1, "end_date"),"yyyy-mm-dd") + '~t' + &
			'C~r~n'

//MessageBox ("From char key", "From char key = " + string(dw_from_char.GetItemNumber(1, 'char_key')))
//MessageBox ("To char key", "To char key = " + string(dw_to_char.GetItemNumber(1, 'char_key')))

istr_error_info.se_event_name = "wf_copy_instance"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp69cr_upd_mass_product_structure"
istr_error_info.se_message = Space(71)

//lb_return = iu_pas201.nf_pasp69cr_upd_mass_product_structure(istr_error_info, ls_Update_string)
lb_return = iu_ws_pas3.uf_pasp69gr(istr_error_info, ls_Update_string)
//If not iu_pas201.nf_pasp69cr_upd_mass_product_structure(istr_error_info, ls_Update_string) Then 
//	dw_from_plant.SetReDraw(True)
//	Return False
//End If
//
iw_frame.SetMicroHelp("Copy Successful")

//ib_ReInquire = True
//wf_retrieve()

dw_from_plant.SetFocus()
dw_from_plant.SetReDraw(True)

Return( True )

end function

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

IF IsValid( iu_pas201 ) THEN
	DESTROY iu_pas201
END IF

IF IsValid( iu_ws_pas3 ) THEN
	DESTROY iu_ws_pas3
END IF

//dw_mass_product_structure_maint.ShareDataOff ( )


end event

event ue_postopen;call super::ue_postopen;iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3

This.PostEvent("ue_query")




end event

event deactivate;call super::deactivate;//iw_frame.im_menu.mf_Disable('m_graph')
iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_inquire')
iw_frame.im_menu.mf_enable('m_save')

end event

event ue_query;call super::ue_query;String						ls_char, &
								st_allrow, &
								ls_filter

DataWindowChild			ldwc_char

iu_pas203 = Create u_pas203

If dw_begin_end_dates.RowCount() = 0 Then
	dw_begin_end_dates.InsertRow(0)
End If

dw_begin_end_dates.SetItem(1, "begin_date", (Today()))
dw_begin_end_dates.SetItem(1, "end_date", Date("2999/12/31"))

dw_from_plant.SetFocus()

istr_error_info.se_event_name = "ue_postconstructor"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

//If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
If iu_ws_pas3.uf_pasp61fr(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If


dw_from_char.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)
ldwc_char.Sort()
						
							
dw_to_char.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)
ldwc_char.Sort()

dw_delete_char.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)
ldwc_char.Sort()






								
end event

event resize;call super::resize;//integer li_x		
//integer li_y		
//
//li_x = (dw_mass_product_structure_maint.x * 2) + 30 
//li_y = dw_mass_product_structure_maint.y + 115
//
//if width > li_x Then
//	dw_mass_product_structure_maint.width	= width - li_x
//end if
//
//if height > li_y then
//	dw_mass_product_structure_maint.height	= height - li_y
//end if
end event

on w_mass_product_structure_maint.create
int iCurrent
call super::create
this.dw_begin_end_dates=create dw_begin_end_dates
this.dw_to_shift=create dw_to_shift
this.dw_from_shift=create dw_from_shift
this.dw_delete_char=create dw_delete_char
this.dw_to_char=create dw_to_char
this.to_t=create to_t
this.from_plant_t=create from_plant_t
this.dw_to_plant=create dw_to_plant
this.dw_from_plant=create dw_from_plant
this.cb_delete=create cb_delete
this.cb_copy=create cb_copy
this.dw_from_char=create dw_from_char
this.ln_1=create ln_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_begin_end_dates
this.Control[iCurrent+2]=this.dw_to_shift
this.Control[iCurrent+3]=this.dw_from_shift
this.Control[iCurrent+4]=this.dw_delete_char
this.Control[iCurrent+5]=this.dw_to_char
this.Control[iCurrent+6]=this.to_t
this.Control[iCurrent+7]=this.from_plant_t
this.Control[iCurrent+8]=this.dw_to_plant
this.Control[iCurrent+9]=this.dw_from_plant
this.Control[iCurrent+10]=this.cb_delete
this.Control[iCurrent+11]=this.cb_copy
this.Control[iCurrent+12]=this.dw_from_char
this.Control[iCurrent+13]=this.ln_1
end on

on w_mass_product_structure_maint.destroy
call super::destroy
destroy(this.dw_begin_end_dates)
destroy(this.dw_to_shift)
destroy(this.dw_from_shift)
destroy(this.dw_delete_char)
destroy(this.dw_to_char)
destroy(this.to_t)
destroy(this.from_plant_t)
destroy(this.dw_to_plant)
destroy(this.dw_from_plant)
destroy(this.cb_delete)
destroy(this.cb_copy)
destroy(this.dw_from_char)
destroy(this.ln_1)
end on

event activate;call super::activate;//iw_frame.im_menu.mf_Enable('m_graph')
iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_save')

end event

event ue_get_data;call super::ue_get_data;//Choose Case as_value
//	Case 'plant'
////		message.StringParm = dw_production_pt_parameters.uf_get_plant_code()
//	Case 'product' , 'fab_product'
////		message.StringParm = dw_production_pt_parameters.uf_get_product_code()
//	Case 'product_desc'
////		message.StringParm = dw_production_pt_parameters.uf_get_product_desc()
//End Choose

	
end event

event ue_set_data;call super::ue_set_data;//Choose Case as_data_item
//	case 'ib_inquire'
//		is_inquire_flag = as_value
//End Choose

end event

type dw_begin_end_dates from u_base_dw_ext within w_mass_product_structure_maint
integer x = 37
integer y = 384
integer width = 695
integer height = 160
integer taborder = 60
string dataobject = "d_mass_struct_begin_end_dates"
boolean border = false
end type

type dw_to_shift from u_shift_all within w_mass_product_structure_maint
integer x = 1696
integer y = 268
integer taborder = 50
end type

event constructor;call super::constructor;this.insertrow(0)
end event

type dw_from_shift from u_shift_all within w_mass_product_structure_maint
integer x = 1696
integer y = 96
integer taborder = 20
end type

event constructor;call super::constructor;this.insertrow(0)
end event

type dw_delete_char from u_base_dw_ext within w_mass_product_structure_maint
event ue_dnwdropdown pbm_dwndropdown
event test pbm_dwntabout
integer x = 1019
integer y = 736
integer width = 1467
integer height = 140
integer taborder = 80
string dataobject = "d_char"
boolean controlmenu = true
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event ue_dnwdropdown;//Long			ll_row
//string		ls_ColName, &
//				ls_ship_plant, &
//				ls_dest_plant, &
//				ls_product, &
//				ls_state, &
//				ls_division
//
//date			ld_start_date, &
//				ld_end_date
//
//ls_ColName = GetColumnName()
//
//IF ls_ColName = "fab_product_code" THEN
//	GetChild ( "fab_product_code", idwc_fab_product_code )
//	ll_row = GetRow()
//	ls_ship_plant = GetItemString( ll_row, "ship_plant" )
//	ls_dest_plant = GetItemString( ll_row, "dest_plant" )
//	ls_product = GetItemString( ll_row, "fab_product_code" )
//	ls_state = GetItemString( ll_row, "product_state" )
//	
//	ld_start_date = GetItemDate( ll_row, "start_date" )
//	ld_end_date = GetItemDate( ll_row, "end_date" )
//	
//	IF iw_frame.iu_string.nf_IsEmpty(ls_ship_plant) or &
//		iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) or &
//			iw_frame.iu_string.nf_IsEmpty(ls_product) OR &
//				iw_frame.iu_string.nf_IsEmpty(ls_state) or &		
//					NOT IsDate( string(ld_start_Date)) Or IsNull(ld_start_Date) or &
//					NOT IsDate( string(ld_end_Date)) Or IsNull(ld_end_Date) THEN
//		MessageBox( "", "Enter a Valid Plant, Product and Date First.", stopsign!, ok! )
//		Return 1
//	END IF
//	
//	// check to see if you have all the 3 fields needed
//	
//	This.SetItem(This.GetRow(), "fab_product_code", "")
//END IF
end event

event constructor;call super::constructor;ib_updateable = True
//is_selection = '1'
This.InsertRow(0)
//ib_storedatawindowsyntax = TRUE


end event

event getfocus;call super::getfocus;If This.rowcount() > 0 then 
	This.SelectText(1, Len(GetText()))
end if
end event

event itemchanged;call super::itemchanged;int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_prpt_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_GetText, &
				ls_temp, &
				ls_product, &
				ls_product_info 
Date			ldt_temp	

dwItemStatus	le_RowStatus
nvuo_pa_business_rules	u_rule
 
//is_ColName    = GetColumnName()
ls_GetText    = data
ll_prpt_row   = GetRow()
il_ChangedRow = 0

//CHOOSE CASE is_ColName
//	CASE "ship_plant"
//		ll_plant_row = idwc_ship_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_ship_plant.RowCount()) 
//		IF ll_plant_row = 0 THEN
//			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
//			This.selecttext(1,100)
//			Return 1
//		END IF
//		
//		This.SetItem(ll_PRPT_row, 'ship_plant', ls_GetText)
//	
//	CASE "recv_plant"
//		ll_plant_row = idwc_recv_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_recv_plant.RowCount()) 
//		IF ll_plant_row = 0 THEN
//			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
//			This.selecttext(1,100)
//			Return 1
//		END IF
//		
//		
//END CHOOSE
//	
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_prpt_row > 0 AND ll_prpt_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_PRPT_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_PRPT_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_PRPT_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_PRPT_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;//String	ls_column_name, &
//			ls_GetText
//
//
//ls_GetText = This.GetText()
//
//Choose Case This.GetColumnName()
//	
//Case "product_code","ship_plant", "recv_plant", &
//		"start_date", "end_date"  
//	This.SelectText(1, Len(ls_gettext))
//	return 1
//End Choose
end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

on ue_graph;//w_graph		w_grp_child
Window			lw_response

//Message.PowerObjectParm = This
//Message.StringParm = 'd_source_weekly_graph'
OpenWithParm(lw_response ,  This, 'w_source_graph_select')
end on

type dw_to_char from u_base_dw_ext within w_mass_product_structure_maint
event ue_dnwdropdown pbm_dwndropdown
event test pbm_dwntabout
integer x = 2190
integer y = 220
integer width = 1467
integer height = 140
integer taborder = 60
string dataobject = "d_char"
boolean controlmenu = true
boolean border = false
borderstyle borderstyle = stylelowered!
end type

event ue_dnwdropdown;//Long			ll_row
//string		ls_ColName, &
//				ls_ship_plant, &
//				ls_dest_plant, &
//				ls_product, &
//				ls_state, &
//				ls_division
//
//date			ld_start_date, &
//				ld_end_date
//
//ls_ColName = GetColumnName()
//
//IF ls_ColName = "fab_product_code" THEN
//	GetChild ( "fab_product_code", idwc_fab_product_code )
//	ll_row = GetRow()
//	ls_ship_plant = GetItemString( ll_row, "ship_plant" )
//	ls_dest_plant = GetItemString( ll_row, "dest_plant" )
//	ls_product = GetItemString( ll_row, "fab_product_code" )
//	ls_state = GetItemString( ll_row, "product_state" )
//	
//	ld_start_date = GetItemDate( ll_row, "start_date" )
//	ld_end_date = GetItemDate( ll_row, "end_date" )
//	
//	IF iw_frame.iu_string.nf_IsEmpty(ls_ship_plant) or &
//		iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) or &
//			iw_frame.iu_string.nf_IsEmpty(ls_product) OR &
//				iw_frame.iu_string.nf_IsEmpty(ls_state) or &		
//					NOT IsDate( string(ld_start_Date)) Or IsNull(ld_start_Date) or &
//					NOT IsDate( string(ld_end_Date)) Or IsNull(ld_end_Date) THEN
//		MessageBox( "", "Enter a Valid Plant, Product and Date First.", stopsign!, ok! )
//		Return 1
//	END IF
//	
//	// check to see if you have all the 3 fields needed
//	
//	This.SetItem(This.GetRow(), "fab_product_code", "")
//END IF
end event

event constructor;call super::constructor;ib_updateable = True
//is_selection = '1'
This.InsertRow(0)
//ib_storedatawindowsyntax = TRUE


end event

event getfocus;call super::getfocus;If This.rowcount() > 0 then 
	This.SelectText(1, Len(GetText()))
end if
end event

event itemchanged;call super::itemchanged;int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_prpt_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_GetText, &
				ls_temp, &
				ls_product, &
				ls_product_info 
Date			ldt_temp	

dwItemStatus	le_RowStatus
nvuo_pa_business_rules	u_rule
 
//is_ColName    = GetColumnName()
ls_GetText    = data
ll_prpt_row   = GetRow()
il_ChangedRow = 0

//CHOOSE CASE is_ColName
//	CASE "ship_plant"
//		ll_plant_row = idwc_ship_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_ship_plant.RowCount()) 
//		IF ll_plant_row = 0 THEN
//			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
//			This.selecttext(1,100)
//			Return 1
//		END IF
//		
//		This.SetItem(ll_PRPT_row, 'ship_plant', ls_GetText)
//	
//	CASE "recv_plant"
//		ll_plant_row = idwc_recv_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_recv_plant.RowCount()) 
//		IF ll_plant_row = 0 THEN
//			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
//			This.selecttext(1,100)
//			Return 1
//		END IF
//		
//		
//END CHOOSE
//	
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_prpt_row > 0 AND ll_prpt_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_PRPT_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_PRPT_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_PRPT_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_PRPT_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;//String	ls_column_name, &
//			ls_GetText
//
//
//ls_GetText = This.GetText()
//
//Choose Case This.GetColumnName()
//	
//Case "product_code","ship_plant", "recv_plant", &
//		"start_date", "end_date"  
//	This.SelectText(1, Len(ls_gettext))
//	return 1
//End Choose
end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

on ue_graph;//w_graph		w_grp_child
Window			lw_response

//Message.PowerObjectParm = This
//Message.StringParm = 'd_source_weekly_graph'
OpenWithParm(lw_response ,  This, 'w_source_graph_select')
end on

type to_t from statictext within w_mass_product_structure_maint
integer x = 23
integer y = 276
integer width = 174
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "To"
alignment alignment = right!
boolean focusrectangle = false
end type

type from_plant_t from statictext within w_mass_product_structure_maint
integer x = 18
integer y = 88
integer width = 174
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "From"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_to_plant from u_plant within w_mass_product_structure_maint
integer x = 187
integer y = 268
integer taborder = 40
end type

type dw_from_plant from u_plant within w_mass_product_structure_maint
integer x = 183
integer y = 76
integer taborder = 10
end type

type cb_delete from commandbutton within w_mass_product_structure_maint
integer x = 1440
integer y = 936
integer width = 923
integer height = 92
integer taborder = 100
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Delete Instances/Step Plant Char/Yields"
end type

event clicked;dw_delete_char.AcceptText()


Parent.PostEvent("ue_delete")
end event

type cb_copy from commandbutton within w_mass_product_structure_maint
integer x = 1408
integer y = 496
integer width = 741
integer height = 92
integer taborder = 90
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Copy Instances/Step Plant Char"
end type

event clicked;dw_from_plant.AcceptText()
dw_from_shift.AcceptText()
dw_from_char.AcceptText()

dw_to_plant.AcceptText()
dw_to_shift.AcceptText()
dw_to_char.AcceptText()


Parent.PostEvent("ue_copy")
end event

type dw_from_char from u_base_dw_ext within w_mass_product_structure_maint
event ue_dnwdropdown pbm_dwndropdown
event test pbm_dwntabout
integer x = 2181
integer y = 52
integer width = 1467
integer height = 140
integer taborder = 30
string dataobject = "d_char"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dnwdropdown;//Long			ll_row
//string		ls_ColName, &
//				ls_ship_plant, &
//				ls_dest_plant, &
//				ls_product, &
//				ls_state, &
//				ls_division
//
//date			ld_start_date, &
//				ld_end_date
//
//ls_ColName = GetColumnName()
//
//IF ls_ColName = "fab_product_code" THEN
//	GetChild ( "fab_product_code", idwc_fab_product_code )
//	ll_row = GetRow()
//	ls_ship_plant = GetItemString( ll_row, "ship_plant" )
//	ls_dest_plant = GetItemString( ll_row, "dest_plant" )
//	ls_product = GetItemString( ll_row, "fab_product_code" )
//	ls_state = GetItemString( ll_row, "product_state" )
//	
//	ld_start_date = GetItemDate( ll_row, "start_date" )
//	ld_end_date = GetItemDate( ll_row, "end_date" )
//	
//	IF iw_frame.iu_string.nf_IsEmpty(ls_ship_plant) or &
//		iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) or &
//			iw_frame.iu_string.nf_IsEmpty(ls_product) OR &
//				iw_frame.iu_string.nf_IsEmpty(ls_state) or &		
//					NOT IsDate( string(ld_start_Date)) Or IsNull(ld_start_Date) or &
//					NOT IsDate( string(ld_end_Date)) Or IsNull(ld_end_Date) THEN
//		MessageBox( "", "Enter a Valid Plant, Product and Date First.", stopsign!, ok! )
//		Return 1
//	END IF
//	
//	// check to see if you have all the 3 fields needed
//	
//	This.SetItem(This.GetRow(), "fab_product_code", "")
//END IF
end event

event itemchanged;call super::itemchanged;//int			li_rc, &
//				li_seconds,li_temp
//
//long			ll_char_row, &
//				ll_prpt_row, &
//				ll_plant_row, &
//				ll_RowCount, &
//				ll_find_row
//
//string		ls_GetText, &
//				ls_temp, &
//				ls_product, &
//				ls_product_info 
//Date			ldt_temp	
//
////dwItemStatus	le_RowStatus
////nvuo_pa_business_rules	u_rule
//// 
////
////ls_GetText    = data
////ll_prpt_row   = GetRow()
////il_ChangedRow = 0
////
////CHOOSE CASE is_ColName
////	CASE "ship_plant"
////		ll_plant_row = idwc_ship_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_ship_plant.RowCount()) 
////		IF ll_plant_row = 0 THEN
////			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
////			This.selecttext(1,100)
////			Return 1
////		END IF
////		
////		This.SetItem(ll_PRPT_row, 'ship_plant', ls_GetText)
////	
////	CASE "recv_plant"
////		ll_plant_row = idwc_recv_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_recv_plant.RowCount()) 
////		IF ll_plant_row = 0 THEN
////			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
////			This.selecttext(1,100)
////			Return 1
////		END IF
////		
////		
////END CHOOSE
////	
//// Update the Update_Flag column so the RPC will know how to update the row
////IF ll_prpt_row > 0 AND ll_prpt_row <= This.RowCount() THEN
////	IF Long(This.Describe("update_flag.id")) > 0 THEN
////		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_PRPT_row, "update_flag")) Then
////			le_RowStatus = This.GetItemStatus(ll_PRPT_row, 0, Primary!)
////			CHOOSE CASE le_RowStatus
////			CASE NewModified!, New!
////				This.SetItem(ll_PRPT_row, "update_flag", "A")
////			CASE DataModified!, NotModified!
////				This.SetItem(ll_PRPT_row, "update_flag", "U")
////			END CHOOSE		
////		End if
////	END IF
////END IF
//
//parent.PostEvent("ue_postitemchanged")
//iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
//
return 0
end event

event itemerror;call super::itemerror;//String	ls_column_name, &
//			ls_GetText
//
//
//ls_GetText = This.GetText()
//
//Choose Case This.GetColumnName()
//	
//Case "product_code","ship_plant", "recv_plant", &
//		"start_date", "end_date"  
//	This.SelectText(1, Len(ls_gettext))
//	return 1
//End Choose
end event

event ue_graph;//w_graph		w_grp_child
//Window			lw_response
//
////Message.PowerObjectParm = This
////Message.StringParm = 'd_source_weekly_graph'
//OpenWithParm(lw_response ,  This, 'w_source_graph_select')
end event

event constructor;call super::constructor;ib_updateable = True
//is_selection = '1'
This.InsertRow(0)
//ib_storedatawindowsyntax = TRUE


end event

event getfocus;call super::getfocus;If This.rowcount() > 0 then 
	This.SelectText(1, Len(GetText()))
end if
end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

type ln_1 from line within w_mass_product_structure_maint
integer linethickness = 7
integer beginx = 32
integer beginy = 636
integer endx = 3538
integer endy = 636
end type

