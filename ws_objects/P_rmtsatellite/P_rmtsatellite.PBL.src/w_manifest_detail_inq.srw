﻿$PBExportHeader$w_manifest_detail_inq.srw
$PBExportComments$IBDKDLD
forward
global type w_manifest_detail_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_manifest_detail_inq
end type
type dw_date from u_effective_date within w_manifest_detail_inq
end type
type dw_manifest_num from u_manifest_num within w_manifest_detail_inq
end type
end forward

global type w_manifest_detail_inq from w_base_response_ext
integer width = 1669
integer height = 480
long backcolor = 67108864
dw_plant dw_plant
dw_date dw_date
dw_manifest_num dw_manifest_num
end type
global w_manifest_detail_inq w_manifest_detail_inq

type variables
Boolean			ib_inquire
end variables

on w_manifest_detail_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_date=create dw_date
this.dw_manifest_num=create dw_manifest_num
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_date
this.Control[iCurrent+3]=this.dw_manifest_num
end on

on w_manifest_detail_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_date)
destroy(this.dw_manifest_num)
end on

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
 				ls_manifest_num, &
				ls_date
				
u_string_functions		lu_strings


If dw_plant.AcceptText() = -1 or &
	dw_date.AcceptText() = -1 or &
	dw_manifest_num.AcceptText() = -1 Then return 
	
ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End If

ls_date = String(dw_date.uf_get_effective_date(), 'YYYY-mm-dd')
If lu_strings.nf_IsEmpty(ls_date) Then
	iw_frame.SetMicroHelp("Date is a required field")
	dw_date.SetFocus()
	Return
End If

ls_manifest_num = dw_manifest_num.uf_get_manifest_num()
If lu_strings.nf_IsEmpty(ls_manifest_num) Then
	iw_frame.SetMicroHelp("Manifest Number is a required field")
	dw_manifest_num.SetFocus()
	Return
End If


iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('effective_date',ls_date)
iw_parentwindow.Event ue_set_data('manifest_num',ls_manifest_num)

ib_inquire = True


//ib_ok_to_close = True

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('effective_date')
dw_date.uf_set_effective_date(date(Message.StringParm))

iw_parentwindow.Event ue_get_data('manifest_num')
dw_manifest_num.uf_set_manifest_num(Message.StringParm)

dw_date.uf_initilize('Manifest Date:',' ')




end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_manifest_detail_inq
integer x = 1371
integer y = 268
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_manifest_detail_inq
integer x = 1083
integer y = 268
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_manifest_detail_inq
integer x = 795
integer y = 268
integer taborder = 40
end type

type dw_plant from u_plant within w_manifest_detail_inq
integer x = 229
integer y = 12
integer taborder = 10
boolean bringtotop = true
end type

type dw_date from u_effective_date within w_manifest_detail_inq
integer x = 9
integer y = 104
integer taborder = 20
boolean bringtotop = true
end type

type dw_manifest_num from u_manifest_num within w_manifest_detail_inq
integer y = 196
integer taborder = 30
boolean bringtotop = true
end type

