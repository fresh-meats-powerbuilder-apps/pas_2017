﻿$PBExportHeader$u_last_userid.sru
$PBExportComments$ibdkdld
forward
global type u_last_userid from datawindow
end type
end forward

global type u_last_userid from datawindow
int Width=814
int Height=84
int TabOrder=10
string DataObject="d_last_userid"
boolean Border=false
boolean LiveScroll=true
end type
global u_last_userid u_last_userid

forward prototypes
public subroutine uf_set_user (string as_user)
public function integer uf_modified ()
end prototypes

public subroutine uf_set_user (string as_user);This.SetItem(1,"userid",as_user)
end subroutine

public function integer uf_modified (); Return 0
end function

event constructor;This.InsertRow(0)
end event

