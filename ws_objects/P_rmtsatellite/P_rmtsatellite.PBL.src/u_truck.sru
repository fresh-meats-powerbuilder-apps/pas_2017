﻿$PBExportHeader$u_truck.sru
$PBExportComments$IBDKDLD
forward
global type u_truck from u_base_dw_ext
end type
end forward

global type u_truck from u_base_dw_ext
integer width = 827
integer height = 72
string dataobject = "d_truck"
boolean border = false
end type
global u_truck u_truck

forward prototypes
public function string uf_get_truck ()
public subroutine uf_set_truck (string as_truck)
public subroutine uf_enable (boolean ab_enable)
end prototypes

public function string uf_get_truck ();Return This.GetItemString(1,'truck')
end function

public subroutine uf_set_truck (string as_truck);This.SetItem(1,"truck",as_truck)
end subroutine

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("truck.Protect = 0 " + &
				"truck.Pointer = 'Arrow!'")
		This.object.truck.Background.Color = 16777215
	Case False
		This.Modify("truck.Protect = 1 " + &
				"truck.Pointer = 'Beam!'")
		This.object.truck.Background.Color = 67108864
		ib_updateable = False
END CHOOSE

end subroutine

event constructor;call super::constructor;This.InsertRow(0)
end event

on u_truck.create
end on

on u_truck.destroy
end on

