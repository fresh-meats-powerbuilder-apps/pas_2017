﻿$PBExportHeader$u_last_updated.sru
$PBExportComments$ibdkdld
forward
global type u_last_updated from datawindow
end type
end forward

global type u_last_updated from datawindow
int Width=905
int Height=80
int TabOrder=10
string DataObject="d_last_updated"
boolean Border=false
boolean LiveScroll=true
end type
global u_last_updated u_last_updated

forward prototypes
public subroutine uf_set_last_updated (datetime adt_datetime)
public function integer uf_modified ()
end prototypes

public subroutine uf_set_last_updated (datetime adt_datetime);This.SetItem(1,"last_update",adt_datetime)
end subroutine

public function integer uf_modified (); Return 0
end function

event constructor;This.InsertRow(0)
end event

