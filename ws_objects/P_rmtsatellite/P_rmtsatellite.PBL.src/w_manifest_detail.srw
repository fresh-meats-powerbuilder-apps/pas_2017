﻿$PBExportHeader$w_manifest_detail.srw
$PBExportComments$IBDKDLD
forward
global type w_manifest_detail from w_base_sheet_ext
end type
type dw_manifest from u_base_dw_ext within w_manifest_detail
end type
type dw_plant from u_plant within w_manifest_detail
end type
type dw_effective_date from u_effective_date within w_manifest_detail
end type
type dw_manifest_num from u_manifest_num within w_manifest_detail
end type
type dw_ship_plant from u_ship_plant within w_manifest_detail
end type
type dw_carrier from u_carrier within w_manifest_detail
end type
type dw_truck from u_truck within w_manifest_detail
end type
end forward

global type w_manifest_detail from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 2875
integer height = 1516
long backcolor = 67108864
dw_manifest dw_manifest
dw_plant dw_plant
dw_effective_date dw_effective_date
dw_manifest_num dw_manifest_num
dw_ship_plant dw_ship_plant
dw_carrier dw_carrier
dw_truck dw_truck
end type
global w_manifest_detail w_manifest_detail

type variables
u_rmt001		iu_rmt001

s_error		istr_error_info

String		is_update_string

Long		il_rec_count

Boolean		ib_no_inquire

Window		iw_parentwindow
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_update_modify (long al_row, string as_header, string as_column_name, boolean ab_original_value)
public subroutine wf_filenew ()
public subroutine wf_delete ()
end prototypes

public function boolean wf_retrieve ();Long						ll_rec_count, &
							ll_row, &
							ll_count, &
							ll_column = 1
							
String					ls_plant, &
							ls_column, & 
							ls_header, &
							ls_column_headings, &
							ls_detail, &
							ls_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_describe
							
u_string_functions 	lu_string				
	
If dw_plant.AcceptText() = -1 Then Return False
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

If Not ib_no_inquire Then
	OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
Else
	ib_no_inquire = False
	ib_inquire = True
End If

If ib_inquire = False Then Return False
SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 
// clear out prev values reset the datawindow
ls_describe = "col_" + String(ll_column) + ".Visible"
Do While dw_manifest.Describe(ls_describe) > '0'
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 0 " + &
		" col_" + String(ll_column) + ".Visible = 0 " + &
		" co_col_" + String(ll_column) + ".Visible = 0 "
	ll_column ++
	ls_describe = "col_" + String(ll_column) + ".Visible"
Loop
dw_manifest.Modify ( ls_TitleModify )
ll_column = 1	
ls_TitleModify = ''

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr06mr_inq_manifest_detail"
iu_rmt001 = Create u_rmt001

ls_header  = dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd') + '~t'
ls_header += dw_manifest_num.uf_get_manifest_num() + '~r~n' 

//MessageBox("header",ls_header)

dw_manifest.reset()

//If Not iu_rmt001.uf_rmtr08mr_inq_mainifest_detail(istr_error_info, & 
//										ls_column_headings, &
//										ls_detail, &
//										ls_header) Then 
//										This.SetRedraw(True) 
//										Return False
//End If			
//

//MessageBox("Return header",ls_header)
//MessageBox("Column headings",ls_column_headings)
//MessageBox("Detail",ls_detail)


//ls_header = '033~tLakeside~tKBTA~tC 148 E PC~t'
dw_ship_plant.uf_set_ship_plant(lu_string.nf_gettoken(ls_header,"~t"))
dw_ship_plant.uf_set_ship_plant_desc(lu_string.nf_gettoken(ls_header,"~t"))
dw_carrier.uf_set_carrier(lu_string.nf_gettoken(ls_header,"~t"))
dw_truck.uf_set_truck(lu_string.nf_gettoken(ls_header,"~t"))


//ls_column_headings = 'NOROLL~tNO ROLL~tDAVE 3~tDAVE 4~tDAVE 5~tDAVE 6~tDAVE 7~tDAVE 8~tDAVE 9~tDAVE 10~tDAVE 11~tDAVE 12~t'
DO WHILE Not lu_string.nf_amiempty(ls_column_headings) or ll_column > 50
	ls_temp = lu_string.nf_gettoken(ls_column_headings,"~t")
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 1 " + &
		" col_" + String(ll_column) + ".Visible = 1 " + &
		" co_col_" + String(ll_column) + ".Visible = 1 "
	ll_column ++
LOOP
dw_manifest.Modify ( ls_TitleModify )


//ls_detail = &
//'1999-05-23~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1.55~t0~t0~t0~t0~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'1999-05-23~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' 
//
//MessageBox("ls_detail",ls_detail)
//
ll_rec_count = dw_manifest.ImportString(ls_detail)

If ll_rec_count > 0 Then 
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if


// Must do because of the reinquire
dw_manifest.Object.DataWindow.HorizontalScrollSplit = '0'
dw_manifest.Object.DataWindow.HorizontalScrollPosition2 =  '0'
dw_manifest.Object.DataWindow.HorizontalScrollSplit = '500'
dw_manifest.Object.DataWindow.HorizontalScrollPosition2 =  '500'

This.SetRedraw(True) 

dw_manifest.ResetUpdate()
dw_manifest.SetFocus()
 
Return True

end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name
dwItemStatus	ldwis_status			
u_string_functions u_string



IF dw_manifest.AcceptText() = -1 Then 
	Return False
End if

ls_header = ''
ls_header  = dw_manifest_num.uf_get_manifest_num() + '~t'
ls_header += String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd') + '~t'
ls_header += dw_plant.uf_get_plant_code() + '~t'
ls_header += Upper(sqlca.userid) + '~t'
ls_header += String(Today(),'yyyy-mm-dd')	+ "~r~n"

ll_modrows = dw_manifest.ModifiedCount()

IF ll_modrows <= 0 Then 
	SetMicroHelp('No Update Necessary')
	Return False
Else
 	SetMicroHelp("Wait... Updating Database")
	SetPointer(HourGlass!)
End if

This.SetRedraw(False)


is_update_string = ""
ll_row = 0
dw_manifest.SelectRow(0,False)

For ll_count = 1 To ll_modrows
	ll_Row = dw_manifest.GetNextModified(ll_Row, Primary!) 
	ldwis_status = dw_manifest.GetItemStatus(ll_row,"desc", Primary!) 
	ll_column_num = 1
	ls_describe = "col_" + String(ll_column_num) + ".Visible"
	Do While dw_manifest.Describe(ls_describe) > '0'
		ls_column_name = "col_" + String(ll_column_num)
		If dw_manifest.GetItemStatus(ll_row,ls_column_name, Primary!) = DataModified! Then
			wf_update_modify(ll_row, ls_header,ls_column_name,False)  
		Else	
			If ldwis_status = Datamodified! Then
				wf_update_modify(ll_row, ls_header,ls_column_name,False)  
				wf_update_modify(ll_row, ls_header,ls_column_name,True)  
			End If
		End If
		ll_column_num ++
		ls_describe = "col_" + String(ll_column_num) + ".Visible"
	Loop
Next


//If il_rec_count > 0 Then
//	IF Not iu_rmt001.uf_rmtr09mr_upd_manifest_detail(istr_error_info, ls_header, &
//												is_update_string) THEN 
//		This.SetRedraw(True)
//		Return False
//	End if
//End If


SetMicroHelp("Modification Successful")
dw_manifest.ResetUpdate()
This.SetRedraw(True)

Return( True )
//////////////////////////////////////////////////////
//Boolean			lb_Return
//
//Integer			li_rc, &
//					li_IncreaseAmount
//
//String			ls_input, &
//					ls_temp, &
//					ls_header
//Long           ll_modrows,ll_count,ll_row
//
//IF dw_manifest.AcceptText() = -1 THEN Return( False )
//
////ls_Tab				= "~t"
//
//iw_frame.SetMicroHelp("Wait... Updating Database")
//
//istr_error_info.se_function_name		= "wf_update"
//istr_error_info.se_event_name			= ""
//
//istr_error_info.se_procedure_name	= "rmtr09mr"
//istr_error_info.se_message				= Space(71)
//
//
//ll_modrows = dw_manifest.ModifiedCount()
//
//IF ll_modrows <= 0 Then 
////	ib_updating = False
//	iw_frame.SetMicroHelp('No Update Necessary')
//	Return False
//End if
//
//iw_frame.SetMicroHelp("Wait... Updating Database")
//SetPointer(HourGlass!)
//
//This.SetRedraw(False)
//
//ls_header  = dw_manifest_num.uf_get_manifest_num() + '~t'
//ls_header += String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd') + '~t'
//ls_header  = dw_plant.uf_get_plant_code() + '~r~n'
//
////ls_header += dw_plant_rb.uf_get_sort() + '~t'
////ls_header += String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd') + '~t'
////ls_header += dw_date_rb.uf_get_date_radio() + '~r~n'
//
////lc_status_ind = 'M'
//For ll_count = 1 To ll_modrows
//	ll_Row = dw_manifest.GetNextModified(ll_Row, Primary!) 
//	If Not This.wf_update_modify(ll_row, ls_header) Then 
//		This.SetRedraw(True)
//		Return False
//	End If
//Next
//
//messagebox('update data', is_update_string)
//
//iu_rmt001.uf_rmtr09mr_upd_manifest_detail(istr_error_info, ls_header, &
//			is_update_string)
//
//iw_frame.SetMicroHelp("Modification Successful")
//dw_manifest.ResetUpdate()
////dw_cattle.SetColumn("end_date")
////dw_plant_constr.SetRedraw(True)
//This.SetRedraw(True)
//
//Return( True )
end function

public function boolean wf_update_modify (long al_row, string as_header, string as_column_name, boolean ab_original_value);String						ls_describe

DWBuffer						ldwb_buffer


ldwb_buffer = Primary!

ls_describe = as_column_name + "_t" + ".text"	

is_update_string += dw_manifest.Describe(ls_describe) + "~t"
is_update_string += &
	String(dw_manifest.GetItemDate(al_row,"desc",ldwb_buffer, ab_original_value), 'yyyy-mm-dd') + "~t" 
If ab_original_value = True Then
	is_update_string += &
	String(0)+ "~r~n"	//dw_manifest.GetItemDecimal(al_row,as_column_name,ldwb_buffer, ab_original_value)) + "~r~n"	 
Else
	is_update_string += &
	String(dw_manifest.GetItemDecimal(al_row,as_column_name,ldwb_buffer, ab_original_value)) + "~r~n"	 
End If	
il_rec_count ++

//If il_rec_count = 100 Then
//	IF Not iu_rmt001.uf_rmtr09mr_upd_manifest_detail(istr_error_info, as_header, &
//			is_update_string) THEN Return False
//	il_rec_count = 0
//	is_update_string = ""
//END IF

Return True
end function

public subroutine wf_filenew ();wf_addrow()
end subroutine

public subroutine wf_delete ();wf_deleteRow()
end subroutine

on w_manifest_detail.create
int iCurrent
call super::create
this.dw_manifest=create dw_manifest
this.dw_plant=create dw_plant
this.dw_effective_date=create dw_effective_date
this.dw_manifest_num=create dw_manifest_num
this.dw_ship_plant=create dw_ship_plant
this.dw_carrier=create dw_carrier
this.dw_truck=create dw_truck
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_manifest
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_effective_date
this.Control[iCurrent+4]=this.dw_manifest_num
this.Control[iCurrent+5]=this.dw_ship_plant
this.Control[iCurrent+6]=this.dw_carrier
this.Control[iCurrent+7]=this.dw_truck
end on

on w_manifest_detail.destroy
call super::destroy
destroy(this.dw_manifest)
destroy(this.dw_plant)
destroy(this.dw_effective_date)
destroy(this.dw_manifest_num)
destroy(this.dw_ship_plant)
destroy(this.dw_carrier)
destroy(this.dw_truck)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;String 					ls_temp
u_string_functions 	lu_string


ls_temp = Message.StringParm	

This.Title = 'Manifest Detail'
dw_truck.uf_enable(False)
dw_ship_plant.uf_enable(False)
dw_carrier.uf_enable(False)
dw_effective_date.uf_enable(false)
dw_manifest_num.uf_enable(False)
dw_plant.disable()

dw_effective_date.uf_set_text('Manifest Date:')
dw_ship_plant.uf_set_text('Dest Plant:')	

If lu_string.nf_IsEmpty(ls_temp) or lower(ls_temp) = 'w_manifest_detail' Then 
	ib_no_inquire = False
Else
	ib_no_inquire = True
	dw_plant.uf_set_plant_code(lu_string.nf_gettoken(ls_temp,"~t"))
	dw_effective_date.uf_set_effective_date(date(lu_string.nf_gettoken(ls_temp,"~t")))
	dw_manifest_num.uf_set_manifest_num(lu_string.nf_gettoken(ls_temp,"~t"))
End If


end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'effective_date'
		message.StringParm = String(dw_effective_date.uf_get_effective_date(), 'YYYY-MM-dd')
	Case 'manifest_num'
		message.StringParm = dw_manifest_num.uf_get_manifest_num()
End choose

end event

event ue_postopen;call super::ue_postopen;This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "mandetl"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_manifest_detail_inq'

iu_rmt001 = Create u_rmt001

wf_retrieve()


end event

event ue_set_data;call super::ue_set_data;
Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date( Date(as_value ))
	Case 'manifest_num'
		dw_manifest_num.uf_set_manifest_num(as_value)
End Choose


end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288

integer li_x		= 57
integer li_y		= 190
  
  
if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

//dw_manifest.width	= newwidth - (30 + li_x)
//dw_manifest.height	= newheight - (30 + li_y)
//
dw_manifest.width	= newwidth - (li_x)
dw_manifest.height	= newheight - (li_y)

// Must do because of the split horizonal bar 
dw_manifest.Object.DataWindow.HorizontalScrollSplit = '0'
dw_manifest.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_manifest.Object.DataWindow.HorizontalScrollSplit = '500'
dw_manifest.Object.DataWindow.HorizontalScrollPosition2 =  '500'


end event

type dw_manifest from u_base_dw_ext within w_manifest_detail
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
integer y = 188
integer width = 2738
integer height = 1536
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_manifest_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event ue_post_itemchanged;//String				ls_original, &
//						ls_new
//
//
//ls_original = This.GetItemString(al_row, 'status', primary!, True)
//ls_new = This.GetItemString(al_row, 'status')
//	
//If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
//	This.SetItemStatus(al_row, 'status', primary!, notmodified!)
//End If
//
end event

event itemchanged;call super::itemchanged;String ls_Column_name
			

ls_column_name = Left(Dwo.Name, 4)

CHOOSE CASE ls_column_name
	CASE 'col_'
		If Not IsNumber(data) Then
			iw_frame.SetMicroHelp("All Columns must be numeric")
			This.SelectText(1, 100)
			Return 1
		End If
		IF Sign(Integer(data)) = -1 Then
			iw_frame.SetMicroHelp("The Number must be Positive")
			This.SelectText(1, 100)
			Return 1
		End If
	Case 'desc'
		If Not IsDate(data) Then
			iw_frame.SetMicroHelp("Kill Date must be a Valid Date")
			This.SelectText(1, 100)
			Return 1
		End IF
END CHOOSE


end event

event constructor;call super::constructor;iw_parentwindow = Parent
end event

event itemerror;call super::itemerror;Return 2
end event

type dw_plant from u_plant within w_manifest_detail
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()

end event

type dw_effective_date from u_effective_date within w_manifest_detail
integer x = 1449
integer width = 759
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;uf_set_effective_date(Today())
uf_enable(False)
end event

type dw_manifest_num from u_manifest_num within w_manifest_detail
integer x = 2176
integer y = 8
integer taborder = 50
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;uf_enable(False)
end event

type dw_ship_plant from u_ship_plant within w_manifest_detail
integer x = 64
integer y = 96
integer width = 1385
integer height = 68
integer taborder = 40
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;uf_enable(False)
end event

type dw_carrier from u_carrier within w_manifest_detail
integer x = 1449
integer y = 96
integer height = 96
integer taborder = 30
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;uf_enable(False)
end event

type dw_truck from u_truck within w_manifest_detail
integer x = 1874
integer y = 96
integer taborder = 20
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;uf_enable(False)
end event

