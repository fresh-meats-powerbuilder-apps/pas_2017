﻿$PBExportHeader$u_manifest_num.sru
$PBExportComments$IBDKDLD
forward
global type u_manifest_num from u_base_dw_ext
end type
end forward

global type u_manifest_num from u_base_dw_ext
integer width = 521
integer height = 72
string dataobject = "d_manifest_num"
boolean border = false
end type
global u_manifest_num u_manifest_num

type variables

end variables

forward prototypes
public function string uf_get_manifest_num ()
public subroutine uf_set_manifest_num (string as_number)
public subroutine uf_enable (boolean ab_enable)
end prototypes

public function string uf_get_manifest_num ();//Return String(u_manifest_num.Object.DataWindow.Data)

Return This.GetItemString(1,'manifest_num')
end function

public subroutine uf_set_manifest_num (string as_number);This.SetItem(1,"manifest_num",as_number)
end subroutine

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("manifest_num.Protect = 0 " + &
				"manifest_num.Pointer = 'Arrow!'")
			This.object.manifest_num.Background.Color = 16777215
	Case False
		This.Modify("manifest_num.Protect = 1 " + &
				"manifest_num.Pointer = 'Beam!'")
		This.object.manifest_num.Background.Color = 67108864
		ib_updateable = False
END CHOOSE

end subroutine

event constructor;call super::constructor;This.InsertRow(0)
end event

event getfocus;If This.GetColumnName() = "manifest_num" Then
	This.SelectText(1, Len(This.GetText()))
End if
end event

event itemchanged;call super::itemchanged;If Not IsNumber(data) Then
	iw_Frame.SetMicroHelp("Manifest Number must be numeric")
	This.SetFocus()
	This.SelectText(1, Len(data))
	Return 1
End If
end event

event itemerror;call super::itemerror;Return 1
end event

on u_manifest_num.create
end on

on u_manifest_num.destroy
end on

