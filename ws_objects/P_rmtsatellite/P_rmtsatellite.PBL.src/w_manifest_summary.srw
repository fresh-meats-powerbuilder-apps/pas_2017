﻿$PBExportHeader$w_manifest_summary.srw
$PBExportComments$IBDKDLD WINDOW FOR MANIFEST SUMMARY
forward
global type w_manifest_summary from w_base_sheet_ext
end type
type dw_manifest from u_base_dw_ext within w_manifest_summary
end type
type dw_plant from u_plant within w_manifest_summary
end type
type dw_date_rb from u_date_radio within w_manifest_summary
end type
type dw_plant_rb from u_sort_radio2 within w_manifest_summary
end type
type dw_effective_date from u_effective_date within w_manifest_summary
end type
end forward

global type w_manifest_summary from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 2875
integer height = 1520
long backcolor = 67108864
dw_manifest dw_manifest
dw_plant dw_plant
dw_date_rb dw_date_rb
dw_plant_rb dw_plant_rb
dw_effective_date dw_effective_date
end type
global w_manifest_summary w_manifest_summary

type variables
u_rmt001		iu_rmt001

s_error		istr_error_info

String		is_update_string

Long		il_rec_count
end variables

forward prototypes
public function integer wf_modified ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_update_modify (long al_row, string as_header)
end prototypes

public function integer wf_modified ();return 0
end function

public function boolean wf_retrieve ();Long			ll_rec_count,ll_row
String		ls_plant, &
				ls_plant_rb, & 
				ls_header, &
				ls_detail, &
				ls_date, ls_date_rb
				
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr01mr_inq_cattle_characteristics"
iu_rmt001 = Create u_rmt001

ls_plant = dw_plant.uf_get_plant_code()
//ls_plant_desc = dw_plant.uf_get_plant_descr() 
ls_plant_rb = dw_plant_rb.uf_get_sort()
ls_date = String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd')
ls_date_rb = dw_date_rb.uf_get_date_radio()


ls_header  = ls_plant + '~t'
ls_header += ls_plant_rb + '~t'
ls_header += ls_date + '~t'
ls_header += ls_date_rb + '~r~n' 

//MessageBox("header",ls_header)

dw_manifest.reset()

//If iu_rmt001.uf_rmtr04mr_inq_manifest_summary(istr_error_info, & 
//										ls_header, &
//										ls_detail) = -1 Then
//										This.SetRedraw(True) 
//										Return False
//End If			


//ls_shift_header = '08:00~t08:00~t12:00~t15:00~t'
//ls_detail = &
//'033~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tL~r~n' + &
//'061~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tL~r~n' + &
//'041~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tL~r~n' + &
//'044~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tL~r~n' + &
//'034~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tL~r~n' + &
//'035~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tL~r~n' + &
//'036~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tL~r~n' + &
//'037~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tU~r~n' + &
//'038~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tU~r~n' + &
//'039~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tU~r~n' + &
//'097~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tU~r~n' + &
//'349~t349~tKBT~tCATTLETHEIBPGOD~t1999-05-19~t1999-05-17~t100.50~tU~r~n' 
//
//MessageBox("ls_detail",ls_detail)

ll_rec_count = dw_manifest.ImportString(ls_detail)

If ll_rec_count > 0 Then 
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True) 

dw_manifest.ResetUpdate()
dw_manifest.SetFocus()
 
Return True

end function

public function boolean wf_update ();Boolean			lb_Return

Integer			li_rc, &
					li_IncreaseAmount

String			ls_input, &
					ls_temp, &
					ls_header
Long           ll_modrows,ll_count,ll_row

IF dw_manifest.AcceptText() = -1 THEN Return( False )

//ls_Tab				= "~t"

iw_frame.SetMicroHelp("Wait... Updating Database")

istr_error_info.se_function_name		= "wf_update"
istr_error_info.se_event_name			= ""

istr_error_info.se_procedure_name	= "rmtr05mr"
istr_error_info.se_message				= Space(71)


ll_modrows = dw_manifest.ModifiedCount()

IF ll_modrows <= 0 Then 
//	ib_updating = False
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

iw_frame.SetMicroHelp("Wait... Updating Database")
SetPointer(HourGlass!)
This.SetRedraw(False)
ls_header  = dw_plant.uf_get_plant_code() + '~t'
ls_header += dw_plant_rb.uf_get_sort() + '~t'
ls_header += String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd') + '~t'
ls_header += dw_date_rb.uf_get_date_radio() + '~r~n'

//lc_status_ind = 'M'
For ll_count = 1 To ll_modrows
	ll_Row = dw_manifest.GetNextModified(ll_Row, Primary!) 
	If Not This.wf_update_modify(ll_row, ls_header) Then 
		This.SetRedraw(True)
		Return False
	End If
Next

//messagebox('update data', is_update_string)
//
//iu_rmt001.uf_rmtr05mr_upd_manifest_summary(istr_error_info, ls_header, &
//			is_update_string)
//
iw_frame.SetMicroHelp("Modification Successful")
is_update_string = ""
dw_manifest.ResetUpdate()
//dw_cattle.SetColumn("end_date")
//dw_plant_constr.SetRedraw(True)
This.SetRedraw(True)

Return( True )
end function

public function boolean wf_update_modify (long al_row, string as_header);String						ls_group_code,ls_user_id,ls_today,ls_temp,ls_temp1

Char							lc_status_ind

DWBuffer						ldwb_buffer


ldwb_buffer = Primary!

If dw_plant_rb.uf_get_sort() = 'S' Then	
	is_update_string += dw_plant.uf_get_plant_code()+ "~t" 
Else
	is_update_string += dw_manifest.GetItemString(al_row,"plant",ldwb_buffer, False) + "~t" 
End If

is_update_string += &
	dw_manifest.GetItemString(al_row,"manifest_num",ldwb_buffer, False) + "~t" 

If dw_date_rb.uf_get_date_radio() = 'S' Then	
	is_update_string += String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd')+ "~t" 
Else
	is_update_string += String(dw_manifest.GetItemDate(al_row,"ship_date",ldwb_buffer, False), 'yyyy-mm-dd') + "~t" 
End If

is_update_string += &
	dw_manifest.GetItemString (al_row,"status",ldwb_buffer, False) + "~r~n"	

il_rec_count ++

//If il_rec_count = 100 Then
//	IF Not iu_rmt001.uf_rmtr05mr_upd_manifest_summary(istr_error_info, as_header, &
//			is_update_string) THEN Return False
//	il_rec_count = 0
//	is_update_string = ""
//END IF

Return True
end function

on w_manifest_summary.create
int iCurrent
call super::create
this.dw_manifest=create dw_manifest
this.dw_plant=create dw_plant
this.dw_date_rb=create dw_date_rb
this.dw_plant_rb=create dw_plant_rb
this.dw_effective_date=create dw_effective_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_manifest
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_date_rb
this.Control[iCurrent+4]=this.dw_plant_rb
this.Control[iCurrent+5]=this.dw_effective_date
end on

on w_manifest_summary.destroy
call super::destroy
destroy(this.dw_manifest)
destroy(this.dw_plant)
destroy(this.dw_date_rb)
destroy(this.dw_plant_rb)
destroy(this.dw_effective_date)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;This.Title = 'Manifest Summary'
dw_date_rb.uf_enable(False)
dw_plant_rb.uf_enable(False)
dw_effective_date.uf_enable(false)
dw_plant.disable()
dw_effective_date.uf_set_text('Date:')

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'effective_date'
		message.StringParm = String(dw_effective_date.uf_get_effective_date(), 'YYYY-MM-dd')
	Case 'choice'
		message.StringParm = dw_plant_rb.uf_get_sort()
	Case 'date_rb'
		message.StringParm = dw_date_rb.uf_get_date_radio()
End choose

end event

event ue_postopen;call super::ue_postopen;This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "mansumm"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_manifest_summary_inq'

iu_rmt001 = Create u_rmt001

wf_retrieve()


end event

event ue_set_data;call super::ue_set_data;String	ls_temp
Window	lw_temp

Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date( Date(as_value ))
	Case 'date_rb'
		dw_date_rb.uf_set_date_radio(as_value)
		CHOOSE CASE as_value
			CASE 'S'
				dw_manifest.Object.date_t.Text = 'Arrival Date'
			CASE 'A'
				dw_manifest.Object.date_t.Text = 'Ship Date'
		END CHOOSE
	Case 'choice'
		dw_plant_rb.uf_set_sort(as_value)
		CHOOSE CASE as_value
			CASE 'S'
				dw_manifest.Object.plant_t.Text = 'Dest Plant'
			CASE 'D'
				dw_manifest.Object.plant_t.Text = 'Ship Plant'
		END CHOOSE
	Case 'manifest detail'
		If dw_plant_rb.uf_get_sort() = 'S' Then
	      ls_temp  = dw_plant.uf_get_plant_code() + '~t' 
		Else
			ls_temp  = dw_manifest.GetItemString(dw_manifest.GetRow ( ),"plant") + "~t" 
		End If

		//ls_temp  = dw_plant.uf_get_plant_code() + '~t'
		If dw_date_rb.uf_get_date_radio() = 'S' Then 
			ls_temp += String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd') + '~t'
		Else
			ls_temp += String(dw_manifest.GetItemDate(dw_manifest.GetRow(),"ship_date"),'yyyy-mm-dd') + "~t"
		End If
		ls_temp += dw_manifest.GetItemString(dw_manifest.GetRow( ), 'manifest_num') 
		//messageBox('ls_temp',ls_temp)
		OpenSheetWithParm(lw_temp, ls_temp, "w_manifest_detail",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
End Choose


end event

type dw_manifest from u_base_dw_ext within w_manifest_summary
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
integer x = 32
integer y = 268
integer width = 2734
integer height = 1096
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_manifest_summary"
boolean vscrollbar = true
boolean border = false
end type

event ue_post_itemchanged;String				ls_original, &
						ls_new


ls_original = This.GetItemString(al_row, 'status', primary!, True)
ls_new = This.GetItemString(al_row, 'status')
	
If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
	This.SetItemStatus(al_row, 'status', primary!, notmodified!)
End If

end event

event itemchanged;call super::itemchanged;IF dwo.name = 'status' Then
	This.EVENT POST ue_post_itemchanged(row, dwo)
End if
end event

event constructor;call super::constructor;is_selection = '1'

end event

event rbuttondown;call super::rbuttondown;m_manifest_detail	lm_popup

IF row > 0 Then
	dw_manifest.SetRow ( row )
	dw_manifest.SelectRow ( row, True )
	lm_popup = Create m_manifest_detail
	lm_popup.m_manifestdetail.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())
	Destroy lm_popup
End If


end event

type dw_plant from u_plant within w_manifest_summary
integer x = 219
integer y = 28
integer taborder = 0
boolean bringtotop = true
end type

type dw_date_rb from u_date_radio within w_manifest_summary
integer x = 2277
integer y = 12
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;uf_set_groupbox_text('Display')
end event

type dw_plant_rb from u_sort_radio2 within w_manifest_summary
integer x = 1691
integer y = 20
integer taborder = 0
boolean bringtotop = true
string dataobject = "d_sort_radio"
end type

event constructor;call super::constructor;uf_set_box_text('Display')
end event

type dw_effective_date from u_effective_date within w_manifest_summary
integer y = 120
integer width = 759
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;uf_set_effective_date(Today())
end event

