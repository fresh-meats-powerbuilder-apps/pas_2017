﻿$PBExportHeader$w_manifest_summary_inq.srw
$PBExportComments$IBDKDLD WINDOW FOR MANIFEST SUMMARY INQ
forward
global type w_manifest_summary_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_manifest_summary_inq
end type
type dw_date from u_effective_date within w_manifest_summary_inq
end type
type dw_plant_rb from u_sort_radio2 within w_manifest_summary_inq
end type
type dw_date_rb from u_date_radio within w_manifest_summary_inq
end type
end forward

global type w_manifest_summary_inq from w_base_response_ext
integer width = 1669
integer height = 676
long backcolor = 67108864
dw_plant dw_plant
dw_date dw_date
dw_plant_rb dw_plant_rb
dw_date_rb dw_date_rb
end type
global w_manifest_summary_inq w_manifest_summary_inq

on w_manifest_summary_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_date=create dw_date
this.dw_plant_rb=create dw_plant_rb
this.dw_date_rb=create dw_date_rb
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_date
this.Control[iCurrent+3]=this.dw_plant_rb
this.Control[iCurrent+4]=this.dw_date_rb
end on

on w_manifest_summary_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_date)
destroy(this.dw_plant_rb)
destroy(this.dw_date_rb)
end on

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
 				ls_plant_desc, &
				ls_date, &
				ls_date_rb,ls_plant_rb
				
Int			li_pa_range				

u_string_functions		lu_strings


If dw_plant.AcceptText() = -1 or &
	dw_date.AcceptText() = -1 or &
	dw_date_rb.AcceptText() = -1 or &
	dw_plant_rb.AcceptText() = -1 Then return 
	
ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End If

ls_date = String(dw_date.uf_get_effective_date(), 'YYYY-mm-dd')
If lu_strings.nf_IsEmpty(ls_date) Then
	iw_frame.SetMicroHelp("Date is a required field")
	dw_date.SetFocus()
	Return
End If

ls_date_rb = dw_date_rb.uf_get_date_radio()
ls_plant_rb = dw_plant_rb.uf_get_sort()


iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('effective_date',ls_date)
iw_parentwindow.Event ue_set_data('choice',ls_plant_rb)
iw_parentwindow.Event ue_set_data('date_rb',ls_date_rb)


ib_ok_to_close = True

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('effective_date')
dw_date.uf_set_effective_date(date(Message.StringParm))

iw_parentwindow.Event ue_get_data('choice')
dw_plant_rb.uf_set_sort(Message.StringParm)

iw_parentwindow.Event ue_get_data('date_rb')
dw_date_rb.uf_set_date_radio(Message.StringParm)

dw_date.uf_initilize('Date:',' ')




end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_manifest_summary_inq
integer x = 1376
integer y = 472
integer taborder = 70
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_manifest_summary_inq
integer x = 1088
integer y = 472
integer taborder = 60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_manifest_summary_inq
integer x = 800
integer y = 472
integer taborder = 50
end type

type dw_plant from u_plant within w_manifest_summary_inq
integer x = 219
integer y = 12
integer taborder = 10
boolean bringtotop = true
end type

type dw_date from u_effective_date within w_manifest_summary_inq
integer y = 104
integer taborder = 20
boolean bringtotop = true
end type

type dw_plant_rb from u_sort_radio2 within w_manifest_summary_inq
integer x = 389
integer y = 208
integer width = 603
integer height = 212
integer taborder = 30
boolean bringtotop = true
end type

event constructor;call super::constructor;uf_set_box_text('Display')
This.uf_enable(True)
end event

type dw_date_rb from u_date_radio within w_manifest_summary_inq
integer x = 1006
integer y = 200
integer taborder = 40
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_set_groupbox_text('Display')
This.uf_enable(True)
end event

