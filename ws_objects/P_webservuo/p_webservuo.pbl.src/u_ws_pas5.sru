﻿$PBExportHeader$u_ws_pas5.sru
forward
global type u_ws_pas5 from u_pas_webservice
end type
end forward

global type u_ws_pas5 from u_pas_webservice
end type
global u_ws_pas5 u_ws_pas5

forward prototypes
public function integer nf_pasp19fr (ref s_error astr_error_info, string as_input, ref string as_output)
public function integer nf_pasp57fr (ref s_error astr_error_info, ref string as_input_header, ref string as_input_detail)
public function integer nf_pasp78gr (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string)
public function integer nf_pasp79gr (ref s_error astr_error_info, string as_detail_string_in, ref string as_detail_string_out, string as_header_string)
public function integer nf_pasp84gr (ref s_error astr_error_info, string as_input_string)
public function integer nf_pasp92gr (ref s_error astr_error_info, string as_input_string)
public function integer nf_pasp93gr (ref s_error astr_error_info, ref string as_input_string, ref string as_output_string)
public function integer nf_pasp94gr (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function integer nf_pasp78fr (ref s_error astr_error_info, string as_input_string, ref string as_output_maint, ref string as_output_except, ref string as_output_load_totals)
public function integer nf_pasp79fr (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string)
public function integer nf_pasp80fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp81fr (ref s_error astr_error_info, string as_input_string, string as_detail_in, ref string as_detail_out, string as_exception_in, ref string as_exception_out)
public function integer nf_pasp82fr (ref s_error astr_error_info, string as_input_string)
public function integer nf_pasp88fr (ref s_error astr_error_info, ref string as_header_string, ref string as_prod, ref string as_ship, ref string as_tran, ref string as_rec, ref string as_plan_tfr)
public function integer nf_pasp89fr (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function integer nf_pasp97fr (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring)
public function integer nf_pasp73gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp51gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp52gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp53gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp54gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp55gr (ref s_error astr_error_info, string as_input_string, ref string as_area_string, ref string as_sect_string)
public function integer nf_pasp56gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp57gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp58gr (ref s_error astr_error_info, string as_input_string, ref string as_detail_string, ref string as_chain_string)
public function boolean nf_pasp59gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp60gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp61gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp62gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp63gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function integer nf_pasp64gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer uf_rmtr18nr (ref s_error astr_error_info, ref string as_header, ref string as_detail)
public function boolean nf_rmtr19nr (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string)
public function integer nf_pasp16gr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_output_string)
public function integer nf_pasp18gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, ref string as_ship_plant, ref string as_ship_date)
public function integer nf_pasp19gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, string as_orders)
public function integer nf_pasp20gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer nf_pasp56fr (ref s_error astr_error_info, string as_input_string, ref string as_header_string, ref string as_output_string, ref string as_output_invt_string)
end prototypes

public function integer nf_pasp19fr (ref s_error astr_error_info, string as_input, ref string as_output);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'PASP19FR'
ls_tran_id = 'P19F'

ls_input_string = as_input

li_rval = uf_rpccall(ls_input_string, as_output, astr_error_info, ls_program_name, ls_tran_id, 1)

return li_rval
end function

public function integer nf_pasp57fr (ref s_error astr_error_info, ref string as_input_header, ref string as_input_detail);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval

u_String_Functions lu_string
ls_program_name = 'PASP57FR'
ls_tran_id = 'P57F'

ls_input_string = as_input_detail

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp78gr (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval

u_String_Functions lu_string

ls_program_name = 'PASP78GR'
ls_tran_id = 'P78G'

ls_input_string = as_header_string_in

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	ls_stringind = Mid(ls_outputstring, 2,1)
	ls_outputstring = Mid(ls_outputstring, 3)
	Do while Len(Trim(ls_outputstring))>0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
		Choose Case ls_stringind
			Case 'H'
				as_header_string_out += ls_output
			Case 'D'
				as_output_string += ls_output
		End Choose
		ls_stringind = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring,2)
	Loop
end if
return li_rval


end function

public function integer nf_pasp79gr (ref s_error astr_error_info, string as_detail_string_in, ref string as_detail_string_out, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval
u_String_Functions lu_string

ls_program_name = 'PASP79GR'
ls_tran_id = 'P79G'

ls_input_string = as_header_string + '~h7F' + as_detail_string_in

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval


end function

public function integer nf_pasp84gr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id
integer li_rval
u_String_Functions lu_string

ls_program_name = 'PASP84GR'
ls_tran_id = 'P84G'

li_rval = uf_rpcupd(as_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp92gr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id
integer li_rval

u_String_Functions lu_string

ls_program_name = 'PASP92GR'
ls_tran_id = 'P92G'

li_rval = uf_rpcupd(as_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp93gr (ref s_error astr_error_info, ref string as_input_string, ref string as_output_string);String ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'PASP93GR'
ls_tran_id = 'P93G'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp94gr (ref s_error astr_error_info, string as_header_string, string as_input_string);String ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'PASP94GR'
ls_tran_id = 'P94G'

ls_input_string = as_header_string + '~h7F' + as_input_string + '~h7F'

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp78fr (ref s_error astr_error_info, string as_input_string, ref string as_output_maint, ref string as_output_except, ref string as_output_load_totals);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name = 'PASP78FR'
ls_tran_id = 'P78F'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id, 1)

//if li_rval = 0 then
if ls_outputstring <> "" then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	
	Do While Len(Trim(ls_outputstring))>0
		ls_output =  lu_string.nf_GetToken(ls_outputstring, '~h7F')
		Choose case ls_stringind
			Case 'M'
				as_output_maint += ls_output
			Case 'E'
				as_output_except += ls_output
			Case 'T'
				as_output_load_totals += ls_output
		End Choose
		ls_stringInd = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop
end if

return li_rval
end function

public function integer nf_pasp79fr (ref s_error astr_error_info, string as_header_string_in, ref string as_header_string_out, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string
ls_program_name= 'PASP79FR'
ls_tran_id = 'P79F'

ls_input_string = as_header_string_in

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if ls_outputstring <> "" then
	ls_stringInd = Mid(ls_outputstring, 2,1)
	ls_outputstring = Mid(ls_outputstring, 3)
	
	Do While Len(Trim(ls_outputstring))>0
		ls_output = lu_string.nf_gettoken(ls_outputstring, '~h7F')
		Choose Case ls_stringInd
			Case 'H'
				as_header_string_out += ls_output
			Case 'D' 
				as_output_string += ls_output
		End Choose
		ls_stringInd = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop
End if

return li_rval
end function

public function integer nf_pasp80fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind, ls_header, ls_plant
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP80FR'
ls_tran_id = 'P80F'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
				Case 'H'
					ls_header += trim(ls_output)
				Case 'P'		
					ls_plant += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

as_output_string = ls_header + ls_plant

return li_rval

end function

public function integer nf_pasp81fr (ref s_error astr_error_info, string as_input_string, string as_detail_in, ref string as_detail_out, string as_exception_in, ref string as_exception_out);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP81FR'
ls_tran_id = 'P81F'

ls_input_string = as_input_string + '~h7F' + as_detail_in + '~h7F' + as_exception_in + '~h7F'

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

//if li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'E'
				as_exception_out += ls_output
			Case 'D'		
				as_detail_out += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
return li_rval

end function

public function integer nf_pasp82fr (ref s_error astr_error_info, string as_input_string);string ls_program_name,  ls_tran_id, ls_input_string, ls_outputstring
integer li_rval 
u_String_Functions lu_string

ls_program_name = 'PASP82FR'
ls_tran_id = 'P82F'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp88fr (ref s_error astr_error_info, ref string as_header_string, ref string as_prod, ref string as_ship, ref string as_tran, ref string as_rec, ref string as_plan_tfr);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP88FR'
ls_tran_id = 'P88F'


ls_input_string = as_header_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
				Case 'P'
					as_prod += ls_output
				Case 'S'
					as_ship += ls_output
				Case 'T'
					as_tran += ls_output
				Case 'R'
					as_rec += ls_output
				Case 'X'
					as_plan_tfr += ls_output
			End Choose
			ls_stringInd = Left(ls_OutputString, 1)
			ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval


	


end function

public function integer nf_pasp89fr (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval

u_String_Functions lu_string
ls_program_name = 'PASP89FR'
ls_tran_id = 'P89F'


ls_input_string = as_header_string + '~h7F' + as_input_string + '~h7F'

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp97fr (ref s_error astr_error_info, string as_inputstring, ref string as_outputstring);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP97FR'
ls_tran_id = 'P97F'


ls_input_string = as_inputstring

li_rval = uf_rpccall(ls_input_string, as_outputstring, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp73gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP73GR'
ls_tran_id = 'P73G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp51gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP51GR'
ls_tran_id = 'P51G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function boolean nf_pasp52gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP52GR'
ls_tran_id = 'P52G'


ls_input_string = as_header_string + '~h7F' + as_update_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)
if li_rval = 0 then 
	return true
end if

end function

public function integer nf_pasp53gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP53GR'
ls_tran_id = 'P53G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id, 1)

return li_rval

end function

public function boolean nf_pasp54gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP54GR'
ls_tran_id = 'P54G'


ls_input_string =  as_header_string + '~h7F' + as_update_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id, 1)


if li_rval = 0 then
	return true
end if
end function

public function integer nf_pasp55gr (ref s_error astr_error_info, string as_input_string, ref string as_area_string, ref string as_sect_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP55GR'
ls_tran_id = 'P55G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'A'
				as_area_String += ls_output
			Case 'S'
				as_Sect_String += ls_output
		End Choose
		
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

end function

public function integer nf_pasp56gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring

integer li_rval 
u_String_Functions lu_string

ls_program_name = 'PASP56GR'
ls_tran_id = 'P56G'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean nf_pasp57gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval 
u_String_Functions lu_string

ls_program_name = 'PASP57GR'
ls_tran_id = 'P57G'

ls_input_string = as_header_string + '~h7F' + as_update_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)
if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer nf_pasp58gr (ref s_error astr_error_info, string as_input_string, ref string as_detail_string, ref string as_chain_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_string_Id, ls_output, ls_stringind 
integer li_rval 
u_String_Functions lu_string 

ls_program_name = 'PASP58GR'
ls_tran_id = 'P58G'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	Do While Len(trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
		
		Choose Case ls_stringInd
			Case 'D'
				as_detail_string += ls_output
			Case 'C'
				as_chain_string += ls_output
		End Choose
		
		ls_stringInd = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop
end if

return li_rval
	





end function

public function boolean nf_pasp59gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval 
u_String_Functions lu_string

ls_program_name = 'PASP59GR'
ls_tran_id = 'P59G'

ls_input_string = as_header_string + '~h7F' + as_update_string
li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else 
	return false
end if
end function

public function integer nf_pasp60gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval
u_String_Functions lu_string

ls_program_name = 'PASP60GR'
ls_tran_id = 'P60G'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean nf_pasp61gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval 
u_String_Functions lu_string

ls_program_name = 'PASP61GR'
ls_tran_id = 'P61G'

ls_input_string = as_header_string + '~h7F' + as_update_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function integer nf_pasp62gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval
u_String_Functions lu_string

ls_program_name = 'PASP62GR'
ls_tran_id = 'P62G'

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean nf_pasp63gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval

u_String_Functions lu_string

ls_program_name = 'PASP63GR'
ls_tran_id = 'P63G'

ls_input_string = as_header_string + '~h7F' + as_update_string	

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer nf_pasp64gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string
integer li_rval, li_program_version
u_String_Functions lu_string

ls_program_name = 'PASP64GR'
ls_tran_id = 'P64G'
li_program_version = 1

ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id,li_program_version)

return li_rval
end function

public function integer uf_rmtr18nr (ref s_error astr_error_info, ref string as_header, ref string as_detail);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'RMTR18NR'
ls_tran_id = 'R18N'


ls_input_string = as_header

as_header = ""


li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'H'
				as_header += ls_output
			Case 'D'		
				as_detail += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	
return li_rval

end function

public function boolean nf_rmtr19nr (ref s_error astr_error_info, ref string as_header_string, ref string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'RMTR19NR'
ls_tran_id = 'R19N'


ls_input_string = as_header_string + '~h7F' + as_input_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else 
	return false
end if

end function

public function integer nf_pasp16gr (ref s_error astr_error_info, string as_header_string, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP16GR'
ls_tran_id = 'P16G'


ls_input_string = as_header_string + '~h7F' + as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp18gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, ref string as_ship_plant, ref string as_ship_date);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP18GR'
ls_tran_id = 'P18G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)
if li_rval = 0 then 
	ls_stringind = mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	Do while Len(Trim(ls_outputstring)) > 0 
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
		Choose Case ls_stringind
			Case 'P'
				as_ship_plant += trim(ls_output)
			Case 'S'
				as_ship_date += trim(ls_output)
			Case 'D' 
				as_output_string += trim(ls_output)
		end Choose
		ls_stringind = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop
end if


return li_rval

end function

public function integer nf_pasp19gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string, string as_orders);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP19GR'
ls_tran_id = 'P19G'


ls_input_string = as_input_string + '~h7F'  + as_orders

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp20gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP20GR'
ls_tran_id = 'P20G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id, 1)

return li_rval

end function

public function integer nf_pasp56fr (ref s_error astr_error_info, string as_input_string, ref string as_header_string, ref string as_output_string, ref string as_output_invt_string);string ls_program_name, ls_tran_id, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval, li_version_number

u_String_Functions lu_string

ls_program_name = 'PASP56FR'
ls_tran_id = 'P56F'

li_version_number = 1

li_rval = uf_rpccall(as_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id, li_version_number)

if li_rval = 0 then
	ls_stringind = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	
	Do While Len(Trim(ls_outputstring)) > 0 
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
		Choose  Case ls_stringind
			Case 'H'
				as_header_string +=ls_output
			Case 'D'
				as_output_string += ls_output
			Case 'I'
				as_output_invt_string += ls_output				
		End Choose
		ls_stringind = Left(ls_outputstring,1)
		ls_outputstring = Mid(ls_outputstring,2)
	Loop
end if
return li_rval

end function

on u_ws_pas5.create
call super::create
end on

on u_ws_pas5.destroy
call super::destroy
end on

