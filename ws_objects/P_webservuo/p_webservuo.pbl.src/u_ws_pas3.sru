﻿$PBExportHeader$u_ws_pas3.sru
forward
global type u_ws_pas3 from u_pas_webservice
end type
end forward

global type u_ws_pas3 from u_pas_webservice
end type
global u_ws_pas3 u_ws_pas3

forward prototypes
public function boolean uf_pasp41fr (ref s_error astr_error_info, ref string as_input, ref string as_plant, ref string as_char, ref string as_exception)
public function boolean uf_pasp42fr (s_error astr_error_info, string as_header, string as_exception)
public function boolean uf_pasp50fr (ref s_error astr_error_info, ref string as_input_string, ref datawindow adw_target)
public function boolean uf_pasp52fr (ref s_error astr_error_info, string as_division, string as_prodtran, string as_window_name, ref string as_trans_data)
public function integer uf_pasp61fr (ref s_error astr_error_info, ref string as_char_string)
public function integer uf_pasp72fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer uf_pasp67fr (ref s_error astr_error_info, readonly string as_product_code, ref string as_input_header, ref string as_output_string)
public function integer uf_pasp69fr (ref s_error astr_error_info, string as_input_string, ref string as_input_dates_string, ref string as_parents_string)
public function boolean uf_pasp53fr (ref s_error astr_error_info, ref string as_input, ref string as_trans_data, string as_window_name)
public function boolean uf_pasp69gr (ref s_error astr_error_info, ref string as_update_string)
public function integer uf_pasp99fr (ref s_error astr_error_info, string as_input_string)
public function integer uf_pasp68fr (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function integer uf_pasp98fr (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function integer uf_pasp66fr (ref s_error astr_error_info, readonly string as_input_string, readonly string as_input_parameters)
public function integer uf_pasp59fr (ref s_error astr_error_info, ref string as_output_string)
public function integer uf_pasp60fr (ref s_error astr_error_info, string as_input_string)
public function integer uf_pasp70fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer uf_pasp71fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean uf_pasp86fr (ref s_error astr_error_info, string as_input_string, ref string as_header_a, ref string as_shift_a, ref string as_header_b, ref string as_shift_b, ref string as_header_c, ref string as_shift_c, ref string as_header_d, ref string as_shift_d)
public function boolean uf_pasp87fr (ref s_error astr_error_info, string as_header_string, string as_input_string)
public function integer uf_pasp28gr (ref s_error astr_error_info, string as_input_string)
public function integer uf_pasp49gr (ref s_error astr_error_info, ref string as_output_string)
public function boolean uf_pasp50gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string)
public function boolean uf_pasp67gr (ref s_error astr_error_info, ref string as_output_string)
public function boolean uf_pasp68gr (ref s_error astr_error_info, ref string as_update_string)
public function boolean uf_pasp74gr (ref s_error astr_error_info, ref string as_input_string)
public function integer uf_pasp75gr (ref s_error astr_error_info, ref string as_output_string, string as_header_string, string as_input_string)
public function integer uf_pasp80gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean uf_pasp27gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function integer uf_pasp02hr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean uf_pasp03hr (ref s_error astr_error_info, ref string as_input_string)
public function integer uf_pasp13gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean uf_pasp05hr (ref s_error astr_error_info, string as_header_string, ref string as_detail_string)
public function boolean uf_pasp06hr (ref s_error astr_error_info, string as_header_string, ref string as_detail_string)
end prototypes

public function boolean uf_pasp41fr (ref s_error astr_error_info, ref string as_input, ref string as_plant, ref string as_char, ref string as_exception);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP41FR'
ls_tran_id = 'P41F'


ls_input_string = as_input

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_plant = ''
as_char = ''
as_exception = ''

//if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'P'
				as_plant += ls_output
			Case 'C'
				as_char += ls_output
			Case 'E'
				as_exception += ls_output
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
if li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean uf_pasp42fr (s_error astr_error_info, string as_header, string as_exception);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP42FR'
ls_tran_id = 'P42F'


ls_input_string = as_header + '~h7F'  + as_exception


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean uf_pasp50fr (ref s_error astr_error_info, ref string as_input_string, ref datawindow adw_target);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval,li_version


u_String_Functions  lu_string

ls_program_name= 'PASP50FR'
ls_tran_id = 'P50F'


ls_input_string = as_input_string

li_version = 1
li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id,li_version)

if li_rval = 0 then
	adw_target.importstring(ls_outputstring)
	return true
else
	return false
end if
end function

public function boolean uf_pasp52fr (ref s_error astr_error_info, string as_division, string as_prodtran, string as_window_name, ref string as_trans_data);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP52FR'
ls_tran_id = 'P52F'


ls_input_string = as_division + '~t' + as_prodtran + '~t' + as_window_name


li_rval = uf_rpccall(ls_input_string, as_trans_data, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer uf_pasp61fr (ref s_error astr_error_info, ref string as_char_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP61FR'
ls_tran_id = 'P61F'


ls_input_string = ''


li_rval = uf_rpccall(ls_input_string, as_char_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp72fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP72FR'
ls_tran_id = 'P72F'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp67fr (ref s_error astr_error_info, readonly string as_product_code, ref string as_input_header, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP67FR'
ls_tran_id = 'P67F'


ls_input_string = as_product_code + '~h7F'  + as_input_header


li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'H'
				as_input_header = ls_output
			Case 'T'
				as_output_string += ls_output
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
end if	

return li_rval
end function

public function integer uf_pasp69fr (ref s_error astr_error_info, string as_input_string, ref string as_input_dates_string, ref string as_parents_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP69FR'
ls_tran_id = 'P69F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_input_dates_string = ''
as_parents_string = ''

//if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'I'
				as_input_dates_string += ls_output
			Case 'P'
				as_parents_string += ls_output

		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
return li_rval
end function

public function boolean uf_pasp53fr (ref s_error astr_error_info, ref string as_input, ref string as_trans_data, string as_window_name);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP53FR'
ls_tran_id = 'P53F'


ls_input_string = as_input + '~h7F'  + as_window_name + '~h7F'  + as_trans_data


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean uf_pasp69gr (ref s_error astr_error_info, ref string as_update_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP69GR'
ls_tran_id = 'P69G'


ls_input_string = as_update_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer uf_pasp99fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP99FR'
ls_tran_id = 'P99F'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp68fr (ref s_error astr_error_info, string as_header_string, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP68FR'
ls_tran_id = 'P68F'


ls_input_string = as_header_string + '~h7F' + as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp98fr (ref s_error astr_error_info, string as_header_string, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP98FR'
ls_tran_id = 'P98F'


ls_input_string = as_header_string + '~h7F' + as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp66fr (ref s_error astr_error_info, readonly string as_input_string, readonly string as_input_parameters);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP66FR'
ls_tran_id = 'P66F'


ls_input_string = as_input_string + '~h7F' + as_input_parameters


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp59fr (ref s_error astr_error_info, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP59FR'
ls_tran_id = 'P59F'


ls_input_string = ''


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp60fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP60FR'
ls_tran_id = 'P60F'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp70fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP70FR'
ls_tran_id = 'P70F'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp71fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP71FR'
ls_tran_id = 'P71F'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean uf_pasp86fr (ref s_error astr_error_info, string as_input_string, ref string as_header_a, ref string as_shift_a, ref string as_header_b, ref string as_shift_b, ref string as_header_c, ref string as_shift_c, ref string as_header_d, ref string as_shift_d);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP86FR'
ls_tran_id = 'P86F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_header_a		= ''
as_shift_a     = ''
as_header_b		= ''
as_shift_b     = ''
as_header_c		= ''
as_shift_c     = ''
as_header_d		= ''
as_shift_d		= ''

//if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case '1'
				as_header_a += ls_output
			Case '2'
				as_header_b += ls_output
			Case '3'
				as_header_c += ls_output
			Case '4'
				as_header_d += ls_output
			Case 'A'
				as_shift_a += ls_output
			Case 'B'
				as_shift_b += ls_output
			Case 'C'
				as_shift_c += ls_output
			Case 'D'
				as_shift_d += ls_output					
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	
if li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean uf_pasp87fr (ref s_error astr_error_info, string as_header_string, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP87FR'
ls_tran_id = 'P87F'


ls_input_string = as_header_string + '~h7F' + as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer uf_pasp28gr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP28GR'
ls_tran_id = 'P28G'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp49gr (ref s_error astr_error_info, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP49GR'
ls_tran_id = 'P49G'


ls_input_string = ''


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean uf_pasp50gr (ref s_error astr_error_info, string as_update_string, ref string as_output_string, string as_header_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP50GR'
ls_tran_id = 'P50G'


ls_input_string = as_update_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean uf_pasp67gr (ref s_error astr_error_info, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP67GR'
ls_tran_id = 'P67G'


ls_input_string = ''


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean uf_pasp68gr (ref s_error astr_error_info, ref string as_update_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP68GR'
ls_tran_id = 'P68G'


ls_input_string = as_update_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean uf_pasp74gr (ref s_error astr_error_info, ref string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP74GR'
ls_tran_id = 'P74G'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer uf_pasp75gr (ref s_error astr_error_info, ref string as_output_string, string as_header_string, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP75GR'
ls_tran_id = 'P75G'


ls_input_string = as_header_string + '~h7F' + as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer uf_pasp80gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP80GR'
ls_tran_id = 'P80G'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean uf_pasp27gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP27GR'
ls_tran_id = 'P27G'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer uf_pasp02hr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP02HR'
ls_tran_id = 'P02H'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean uf_pasp03hr (ref s_error astr_error_info, ref string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP03HR'
ls_tran_id = 'P03H'


ls_input_string = as_input_string


li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer uf_pasp13gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP13GR'
ls_tran_id = 'P13G'


ls_input_string = as_input_string


li_rval = uf_rpccall(ls_input_string, as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean uf_pasp05hr (ref s_error astr_error_info, string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_input_string, ls_output_string
integer li_rval, li_version


u_String_Functions  lu_string

ls_program_name= 'PASP05HR'
ls_tran_id = 'P05H'


ls_input_string = as_header_string + '~h7F' + as_detail_string + '~h7F' 
li_version = 1
li_rval = uf_rpccall(ls_input_string, ls_output_string, astr_error_info, ls_program_name, ls_tran_id,li_version)

as_detail_string = ls_output_string

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean uf_pasp06hr (ref s_error astr_error_info, string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_input_string, ls_output_string
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP06HR'
ls_tran_id = 'P06H'


ls_input_string = as_header_string + '~h7F' + as_detail_string + '~h7F' 

li_rval = uf_rpccall(ls_input_string, ls_output_string, astr_error_info, ls_program_name, ls_tran_id)

as_detail_string = ls_output_string

If li_rval = 0 Then
	return true
else
	return false
End If
end function

on u_ws_pas3.create
call super::create
end on

on u_ws_pas3.destroy
call super::destroy
end on

