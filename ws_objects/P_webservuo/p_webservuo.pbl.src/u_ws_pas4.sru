﻿$PBExportHeader$u_ws_pas4.sru
forward
global type u_ws_pas4 from u_pas_webservice
end type
end forward

global type u_ws_pas4 from u_pas_webservice
end type
global u_ws_pas4 u_ws_pas4

forward prototypes
public function integer nf_rmtr01nr (ref s_error astr_error_info, string as_header_string, ref string as_output_string)
public function integer nf_pasp90fr (s_error astr_error_info, ref string as_output_string)
public function integer nf_rmtr23nr (s_error astr_error_info, string as_header_string, ref string as_output_string)
public function boolean nf_rmtr14nr (s_error astr_error_info, ref string as_header_input, ref string as_header_columns, ref string as_detail, ref string as_formula)
public function integer nf_rmtr16nr (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string)
public function boolean nf_rmtr20nr (s_error astr_error_info, ref string as_header_columns, ref string as_detail_string, ref string as_header_input)
public function boolean nf_rmtr28nr (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string)
public function boolean nf_rmtr07nr (s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function boolean nf_rmtr24nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function boolean nf_rmtr15nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function boolean nf_rmtr31nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function boolean nf_rmtr17nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function boolean nf_rmtr21nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function boolean nf_rmtr29nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function boolean nf_rmtr30nr (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string)
public function boolean nf_pasp91fr (s_error astr_error_info, ref string as_input_string)
public function boolean nf_rmtr02nr (ref s_error astr_error_info, ref string as_update_string)
public function boolean nf_rmtr03nr (ref s_error astr_error_info, ref string as_grade, ref string as_yields, ref string as_desc, ref string as_program, ref string as_cool_code, ref string as_chr)
public function integer nf_rmtr06nr (ref s_error astr_error_info, ref string as_header, ref string as_detail)
public function integer nf_pasp81gr (ref s_error astr_error_info, string as_header_string_in, ref string as_output_hot_box, ref string as_output_temp_cooler, ref string as_output_plant_info)
public function integer nf_rmtr25nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string)
public function boolean nf_rmtr26nr (s_error astr_error_info, ref string as_header_columns, ref string as_detail_string, ref string as_header_input)
public function integer nf_pasp96fr (ref s_error astr_error_info, character in_option, string as_input_string, ref string as_output_string)
public function integer nf_pasp63fr (ref s_error astr_error_info, ref string as_output_string)
public function integer nf_pasp64fr (ref s_error astr_error_info, string as_input_string)
public function integer nf_pasp74fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp30gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp93fr (ref s_error astr_error_info, string as_input_string)
public function boolean nf_pasp31gr (ref s_error astr_error_info, string as_input_string)
public function boolean nf_pasp75fr (ref s_error astr_error_info, string as_header_string, string as_detail_string)
public function boolean nf_pasp46fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string)
public function boolean nf_pasp82gr (ref s_error astr_error_info, string as_hot_box_info, string as_temp_cooler_info, string as_header_string)
public function integer nf_pasp01gr (ref s_error astr_error_info, string as_ind_string, string as_detail_string, ref string as_output_string)
public function boolean nf_pasp47fr (ref s_error astr_error_info, string as_division_code, string as_update)
public function integer nf_pasp92fr (ref s_error astr_error_info, string as_input_string, ref string as_combined_string)
end prototypes

public function integer nf_rmtr01nr (ref s_error astr_error_info, string as_header_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'RMTR01NR'
ls_tran_id = 'R01N'


ls_input_string = as_header_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp90fr (s_error astr_error_info, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval, li_program_version


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'PASP90FR'
ls_tran_id = 'P90F'

li_program_version = 2


li_rval = uf_rpccall(' ',as_output_string, astr_error_info, ls_program_name, ls_tran_id, li_program_version)

return li_rval

end function

public function integer nf_rmtr23nr (s_error astr_error_info, string as_header_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'RMTR23NR'
ls_tran_id = 'R23N'


ls_input_string = as_header_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function boolean nf_rmtr14nr (s_error astr_error_info, ref string as_header_input, ref string as_header_columns, ref string as_detail, ref string as_formula);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output,ls_string_ind

u_String_Functions  lu_string

integer li_rval

ls_program_name='RMTR14NR'
ls_tran_id ='R14N'

ls_inputstring = as_header_input

li_rval = uf_rpccall(ls_inputstring,ls_outputstring,astr_error_info,ls_program_name,ls_tran_id)

as_header_columns = ''
as_detail = ''
as_formula = ''

IF li_rval = 0 Then
	ls_string_ind = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
		Choose Case ls_string_ind
			Case 'C'
				as_header_columns += ls_output
			Case 'D'
				as_detail			+= ls_output
			Case 'F'
				as_formula			+= ls_output
		End Choose
		ls_string_ind = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop 
END IF	

if li_rval = 0 then

return true

else
	return false
end if
end function

public function integer nf_rmtr16nr (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring, ls_stringId, ls_output, ls_stringind
integer li_rval

u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'RMTR16NR'
ls_tran_id = 'R16N'


ls_input_string = as_header_string

li_rval = uf_rpccall(ls_input_string, ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_header_string		= ''
as_output_string     = ''

//if  li_rval = 0 then
	ls_stringInd = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)

	Do While Len(Trim(ls_outputstring)) > 0
		
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
	
		Choose Case ls_StringInd
			Case 'H'
				as_header_string += ls_output
			Case 'D'
				as_output_string += ls_output
		End Choose
		ls_stringInd = Left(ls_OutputString, 1)
		ls_outputstring = Mid(ls_OutputString, 2)
	Loop
//end if	

return li_rval

end function

public function boolean nf_rmtr20nr (s_error astr_error_info, ref string as_header_columns, ref string as_detail_string, ref string as_header_input);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output,ls_string_ind
integer li_rval


u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'RMTR20NR'
ls_tran_id = 'R20N'


ls_inputstring = as_header_input

li_rval = uf_rpccall(ls_inputstring,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_header_columns = ''
as_detail_string = ''

IF li_rval = 0 Then
	ls_string_ind = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
		Choose Case ls_string_ind
			Case 'C'
				as_header_columns += ls_output
			Case 'D'
				as_detail_string			+= ls_output
		End Choose
		ls_string_ind = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop 
END IF	


if li_rval = 0 then
	return true
else
	return false
end if


end function

public function boolean nf_rmtr28nr (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'RMTR28NR'
ls_tran_id = 'R28N'


ls_input_string = as_header_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean nf_rmtr07nr (s_error astr_error_info, ref string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='RMTR07NR'
ls_tran_id ='R07N'

ls_inputstring = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr24nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='RMTR24NR'
ls_tran_id ='R24N'

ls_inputstring = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr15nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='RMTR15NR'
ls_tran_id ='R15N'

ls_inputstring = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr31nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='RMTR31NR'
ls_tran_id ='R31N'

ls_inputstring = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr17nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='RMTR17NR'
ls_tran_id ='R17N'

ls_inputstring = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr21nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='RMTR21NR'
ls_tran_id ='R21N'

ls_inputstring = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr29nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='RMTR29NR'
ls_tran_id ='R29N'

ls_inputstring = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr30nr (ref s_error astr_error_info, ref string as_header_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'RMTR30NR'
ls_tran_id = 'R30N'


ls_input_string = as_header_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 then
	return true
else
	return false
end if
end function

public function boolean nf_pasp91fr (s_error astr_error_info, ref string as_input_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP91FR'
ls_tran_id ='P91F'

ls_inputstring = as_input_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr02nr (ref s_error astr_error_info, ref string as_update_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='RMTR02NR'
ls_tran_id ='R02N'

ls_inputstring = as_update_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_rmtr03nr (ref s_error astr_error_info, ref string as_grade, ref string as_yields, ref string as_desc, ref string as_program, ref string as_cool_code, ref string as_chr);string ls_program_name, ls_tran_id, ls_input_string, ls_output_string,ls_string_ind,ls_output
integer li_rval

u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'RMTR03NR'
ls_tran_id = 'R03N'

li_rval = uf_rpccall('',ls_output_string, astr_error_info, ls_program_name, ls_tran_id)

as_grade = ''
as_yields = ''
as_desc = ''
as_chr = ''
as_program = ''
as_cool_code = ''

If li_rval = 0 Then
	ls_string_ind = Mid(ls_output_string, 2, 1)
	ls_output_string = Mid(ls_output_string, 3)
		Do While Len(Trim(ls_output_string)) > 0
			ls_output = lu_string.nf_GetToken(ls_output_string, '~h7F')
		
			Choose Case ls_string_ind
				Case 'G'
					as_grade += ls_output
				Case 'Y'
					as_yields += ls_output
				Case 'X'
					as_desc += ls_output
				Case 'H'
					as_chr += ls_output
				Case 'P'
					as_program += ls_output
				Case 'O'
					as_cool_code += ls_output
			End Choose
			ls_string_ind = Left(ls_output_string, 1)
			ls_output_string = Mid(ls_output_string, 2)
		Loop 
End If

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function integer nf_rmtr06nr (ref s_error astr_error_info, ref string as_header, ref string as_detail);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


ls_program_name= 'RMTR06NR'
ls_tran_id = 'R06N'


li_rval = uf_rpccall(as_header,as_detail, astr_error_info, ls_program_name, ls_tran_id)

return li_rval

end function

public function integer nf_pasp81gr (ref s_error astr_error_info, string as_header_string_in, ref string as_output_hot_box, ref string as_output_temp_cooler, ref string as_output_plant_info);string ls_program_name, ls_tran_id, ls_input_string, ls_output_string,ls_string_ind,ls_output
integer li_rval

u_String_Functions  lu_string

ls_program_name= 'PASP81GR'
ls_tran_id = 'P81G'

li_rval = uf_rpccall(as_header_string_in,ls_output_string, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 Then
ls_string_ind = Mid(ls_output_string, 2, 1)
		ls_output_string = Mid(ls_output_string, 3)
		Do While Len(Trim(ls_output_string)) > 0
			ls_output = lu_string.nf_GetToken(ls_output_string, '~h7F')
		
			Choose Case ls_string_ind
				Case 'H'
					as_output_hot_box += ls_output
				Case 'T'
					as_output_temp_cooler += ls_output
				Case 'P'
					as_output_plant_info += ls_output
			End Choose
			ls_string_ind = Left(ls_output_string, 1)
			ls_output_string = Mid(ls_output_string, 2)
		Loop 
End If

return li_rval

end function

public function integer nf_rmtr25nr (ref s_error astr_error_info, ref string as_header_string, ref string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output,ls_string_ind
integer li_rval


u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'RMTR25NR'
ls_tran_id = 'R25N'


ls_inputstring = as_header_string

li_rval = uf_rpccall(ls_inputstring,ls_outputstring, astr_error_info, ls_program_name, ls_tran_id)

as_header_string = ''
as_detail_string = ''

IF li_rval = 0 Then
	ls_string_ind = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
		Choose Case ls_string_ind
			Case 'H'
				as_header_string += ls_output
			Case 'D'
				as_detail_string			+= ls_output
		End Choose
		ls_string_ind = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop 
END IF	

If li_rval = 0 Then
	return 1
else
	return -1
End If
end function

public function boolean nf_rmtr26nr (s_error astr_error_info, ref string as_header_columns, ref string as_detail_string, ref string as_header_input);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output,ls_string_ind

u_String_Functions  lu_string

integer li_rval

ls_program_name='RMTR26NR'
ls_tran_id ='R26N'

ls_inputstring = as_header_input

li_rval = uf_rpccall(ls_inputstring,ls_outputstring,astr_error_info,ls_program_name,ls_tran_id)

as_header_columns = ''
as_detail_string = ''

IF li_rval = 0 Then
	ls_string_ind = Mid(ls_outputstring, 2, 1)
	ls_outputstring = Mid(ls_outputstring, 3)
	Do While Len(Trim(ls_outputstring)) > 0
		ls_output = lu_string.nf_GetToken(ls_outputstring, '~h7F')
		Choose Case ls_string_ind
			Case 'C'
				as_header_columns += ls_output
			Case 'D'
				as_detail_string			+= ls_output
		End Choose
		ls_string_ind = Left(ls_outputstring, 1)
		ls_outputstring = Mid(ls_outputstring, 2)
	Loop 
END IF	

if li_rval = 0 then
	return true
else
	return false
end if
end function

public function integer nf_pasp96fr (ref s_error astr_error_info, character in_option, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP96FR'
ls_tran_id ='P96F'

ls_inputstring = as_input_string + '~h7F' + in_option

li_rval = uf_rpccall(ls_inputstring,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function integer nf_pasp63fr (ref s_error astr_error_info, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'PASP63FR'
ls_tran_id = 'P63F'


ls_input_string = as_output_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id, 1)

return li_rval

end function

public function integer nf_pasp64fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'PASP64FR'
ls_tran_id = 'P64F'


ls_input_string = as_input_string

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id, 1)

return li_rval

end function

public function integer nf_pasp74fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'PASP74FR'
ls_tran_id = 'P74F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean nf_pasp30gr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'PASP30GR'
ls_tran_id = 'P30G'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_pasp93fr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP93FR'
ls_tran_id ='P93F'

ls_inputstring = as_input_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_pasp31gr (ref s_error astr_error_info, string as_input_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP31GR'
ls_tran_id ='P31G'

ls_inputstring = as_input_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_pasp75fr (ref s_error astr_error_info, string as_header_string, string as_detail_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP75FR'
ls_tran_id ='P75F'

ls_inputstring = as_header_string + '~h7F' + as_detail_string

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_pasp46fr (ref s_error astr_error_info, string as_input_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'PASP46FR'
ls_tran_id = 'P46F'


ls_input_string = as_input_string

li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

If li_rval = 0 Then
	return true
else
	return false
End If
end function

public function boolean nf_pasp82gr (ref s_error astr_error_info, string as_hot_box_info, string as_temp_cooler_info, string as_header_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_outputstring,ls_stringind,ls_output

integer li_rval

ls_program_name='PASP82GR'
ls_tran_id ='P82G'



ls_inputstring = as_header_string +'~h7F'+ as_hot_box_info+'~h7F'+ as_temp_cooler_info

li_rval = uf_rpcupd(ls_inputstring,astr_error_info,ls_program_name,ls_tran_id)
return true
//If li_rval = 0 Then
//	return true
//else
//	return false
//End If
end function

public function integer nf_pasp01gr (ref s_error astr_error_info, string as_ind_string, string as_detail_string, ref string as_output_string);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


//u_String_Functions  lu_string
//Only use it for multi-strings methods

ls_program_name= 'PASP01GR'
ls_tran_id = 'P01G'

ls_input_string = as_ind_string + '~h7F' + as_detail_string 
li_rval = uf_rpccall(ls_input_string,as_output_string, astr_error_info, ls_program_name, ls_tran_id)

return li_rval
end function

public function boolean nf_pasp47fr (ref s_error astr_error_info, string as_division_code, string as_update);string ls_program_name, ls_tran_id, ls_input_string, ls_outputstring
integer li_rval


u_String_Functions  lu_string

ls_program_name= 'PASP47FR'
ls_tran_id = 'P47F'


ls_input_string = as_division_code+'~h7F'+as_update

li_rval = uf_rpcupd(ls_input_string, astr_error_info, ls_program_name, ls_tran_id)

if li_rval = 0 then
	return true
else
	return false
end if

end function

public function integer nf_pasp92fr (ref s_error astr_error_info, string as_input_string, ref string as_combined_string);string ls_program_name, ls_tran_id, ls_inputstring, ls_stringind,ls_output,ls_string_ind

//u_String_Functions  lu_string

integer li_rval, li_pos

long	ll_counter
ls_program_name='PASP92FR'
ls_tran_id ='P92F'

ls_inputstring = as_input_string

li_rval = uf_rpccall(ls_inputstring,as_combined_string,astr_error_info,ls_program_name,ls_tran_id, 1)

return li_rval
end function

on u_ws_pas4.create
call super::create
end on

on u_ws_pas4.destroy
call super::destroy
end on

