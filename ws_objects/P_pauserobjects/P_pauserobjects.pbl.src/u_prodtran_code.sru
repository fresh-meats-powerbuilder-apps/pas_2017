﻿$PBExportHeader$u_prodtran_code.sru
forward
global type u_prodtran_code from u_netwise_dw
end type
end forward

global type u_prodtran_code from u_netwise_dw
integer width = 1595
integer height = 88
string dataobject = "d_prodtran_code"
boolean border = false
event ue_accept ( )
end type
global u_prodtran_code u_prodtran_code

type variables
DataWindowChild	idwc_child

Boolean		ib_required
end variables

forward prototypes
public subroutine disable ()
public subroutine enable ()
public function string uf_get_prodtran ()
public subroutine uf_set_prodtran (string as_prodtran)
public function string uf_get_prodtran_descr ()
end prototypes

event ue_accept;call super::ue_accept;This.AcceptText()

end event

public subroutine disable ();This.Modify("prodtran_code.Background.Mode = 1 prodtran_code.Protect = 1 " + &
				"prodtran_code.Pointer = 'Arrow!'")

end subroutine

public subroutine enable ();This.Modify("prodtran_code.Background.Mode = 0 prodtran_code.Protect = 0" + &
				"prodtran_code.Pointer = 'Beam!'")

end subroutine

public function string uf_get_prodtran ();return Trim(This.GetItemString(1, "prodtran_code"))
end function

public subroutine uf_set_prodtran (string as_prodtran);Long	ll_row, &
		ll_row_count

String	ls_text, &
			ls_description


This.SetItem(1, "prodtran_code", as_prodtran)

This.GetChild("prodtran_code",idwc_child)

ls_text = 'Trim(type_code) = "' + as_prodtran + '"'
ll_row_count = idwc_child.RowCount()
ll_row = idwc_child.Find(ls_text, 1, ll_row_count)
If ll_row <= 0 Then
	If ib_required OR as_prodtran <> "  " Then
		gw_netwise_Frame.SetMicroHelp(as_prodtran + " is an Invalid Product Translation Code")
		SetFocus()
		This.SelectText(1, Len(ls_text))
	End If
	ls_description = ""
Else
	SetFocus()
	This.SelectText(1, Len(ls_text))
	ls_Description = idwc_child.GetItemString(ll_row, "type_desc")
	gw_netwise_frame.SetMicroHelp("Ready")
End if

This.SetItem(1, "prodtran_description", ls_Description)
end subroutine

public function string uf_get_prodtran_descr ();return Trim(This.GetItemString(1, "prodtran_description"))
end function

event destructor;call super::destructor;SetProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "Lastprodtran", &
			This.GetItemString(1, 'prodtran_code'))

end event

on getfocus;call u_netwise_dw::getfocus;This.SelectText(1, Len(This.GetText()))
end on

event itemchanged;call super::itemchanged;Long	ll_row

String	ls_text, &
			ls_description


ls_text = This.GetText()
If Len(Trim(ls_text)) = 0 Then 
	This.SetItem(1, "prodtran_description", "")	
	return
End if
ll_row = idwc_child.Find('Trim(type_code) = "' + ls_text + '"', 1, idwc_child.RowCount())
If ll_row <= 0 Then
	gw_netwise_Frame.SetMicroHelp(ls_text + " is an Invalid Product Translation Code")
//	ls_description = ""
//	This.SetItem(1, "prodtran_description", ls_Description)
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	return 1
Else
//	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	ls_Description = idwc_child.GetItemString(ll_row, "type_desc")
	gw_netwise_frame.SetMicroHelp("Ready")
End if

This.SetItem(1, "prodtran_description", ls_Description)
end event

event itemerror;call super::itemerror;return 1
end event

on itemfocuschanged;call u_netwise_dw::itemfocuschanged;This.SelectText(1, Len(This.GetText()))
end on

event constructor;call super::constructor;ib_updateable = False
ib_required = True

String	ls_prodtran

This.GetChild("prodtran_code", idwc_child)

idwc_child.SetTransObject(SQLCA)
idwc_child.Retrieve("DIVCODE")

//idwc_child.Sort()

If This.Describe("prodtran_code.Protect") <> '1' Then
	ls_prodtran = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), "Lastprodtran", "  ")
	This.Reset()
	This.SetItem(This.InsertRow(0), 'prodtran_code', ls_prodtran)
	This.TriggerEvent(ItemChanged!)
End if

end event

event losefocus;call super::losefocus;This.TriggerEvent('ue_Accept')

end event

on u_prodtran_code.create
end on

on u_prodtran_code.destroy
end on

