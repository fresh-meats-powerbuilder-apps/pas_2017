﻿$PBExportHeader$w_selection_percent.srw
$PBExportComments$ibdkdld
forward
global type w_selection_percent from w_base_sheet_ext
end type
type dw_plant from u_plant within w_selection_percent
end type
type dw_shift from u_shift within w_selection_percent
end type
type dw_effective_date from u_effective_date within w_selection_percent
end type
type dw_select_time from u_selection within w_selection_percent
end type
type dw_1 from u_base_dw_ext within w_selection_percent
end type
end forward

global type w_selection_percent from w_base_sheet_ext
integer width = 2666
integer height = 691
string title = "Selection Percent"
long backcolor = 67108864
dw_plant dw_plant
dw_shift dw_shift
dw_effective_date dw_effective_date
dw_select_time dw_select_time
dw_1 dw_1
end type
global w_selection_percent w_selection_percent

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
u_rmt001		iu_rmt001
u_ws_pas4	iu_ws_pas4
s_error		istr_error_info

Long		il_color,&
		il_SelectedColor,&
		il_SelectedTextColor

Boolean		ib_reinquire, &
		ib_close_this

Datastore		ids_report

String		is_select_time,&
		is_update_string
		

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_update_modify ()
end prototypes

public function boolean wf_retrieve ();long			ll_rec_count, &
				ll_row,&
				ll_count,&
				ll_max = 7,ll_total_count

string		ls_plant, &
				ls_plant_desc, & 
				ls_shift,&
				ls_production_date, &
				ls_weekly_data, &
				ls_temp, &
				ls_header,&
				ls_detail

Int			il_temp

date			ldt_week_end_date

u_string_functions	lu_string


If dw_shift.AcceptText() = -1 Then return False
If dw_plant.AcceptText() = -1 Then return False
If dw_effective_date.AcceptText() = -1 Then return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 
dw_1.Reset ( )

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_rmtr23mr_inq_sel_percent"
istr_error_info.se_message = Space(71)

ls_plant = dw_plant.uf_get_plant_code()


ls_shift = dw_shift.uf_get_shift()
ls_production_date = string(dw_effective_date.uf_get_effective_date(), "yyyy-mm-dd" )
If is_select_time = 'S' Then
	dw_1.object.date_dt.expression = ls_production_date
	dw_effective_date.Show()
Else
	dw_1.object.date_dt.expression = '0001-01-01'
	dw_effective_date.Hide()
End If

ls_header = ls_plant + '~t' + &
				is_select_time + '~t' + &
				ls_production_date + '~t' + &
				ls_shift + '~r~n' 

//MessageBox('Header Data',ls_header)				

/*If iu_rmt001.uf_rmtr23mr_inq_sel_percent(istr_error_info, & 
										ls_header, &
										ls_detail) < 0 Then
										This.SetRedraw(True) 
										Return False
End If									
*/

If iu_ws_pas4.NF_RMTR23NR(istr_error_info, & 
										ls_header, &
										ls_detail) < 0 Then
										This.SetRedraw(True) 
										Return False
End If									

//messagebox('temp',String(il_temp))
//ls_detail = '11.00~t1100.00~t22.00~t2200.00~t33.00~t3300.00~t44.00~t4400.00~t55.00~t5500.00~t66.00~t6600.00~t0.00~t0.00~r~n' 
					  
//MessageBox('Return Data',ls_detail)

ll_total_count = dw_1.ImportString(ls_detail)
	
If ll_total_count > 0 Then
	SetMicroHelp(String(ll_total_count) + " Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

//This.SetRedraw(True) 

dw_1.ResetUpdate()
This.SetRedraw(True) 
dw_1.SetFocus()
 
Return True

end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num

Char				lc_status_ind					

string			ls_plant, &
					ls_production_date,&
					ls_shift,&
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name
dwItemStatus	ldwis_status			
u_string_functions u_string



IF dw_1.AcceptText() = -1 Then 
	Return False
End if

ls_header = ''
ls_plant = dw_plant.uf_get_plant_code()
ls_shift = dw_shift.uf_get_shift()
ls_production_date = string(dw_effective_date.uf_get_effective_date(), "yyyy-mm-dd" )
If is_select_time = 'S' Then
	dw_1.object.date_dt.expression = ls_production_date
//	dw_effective_date.Show()
Else
	dw_1.object.date_dt.expression = '0001-01-01'
//	dw_effective_date.Hide()
End If

ls_header = ls_plant + '~t' + &
				is_select_time + '~t' + &
				ls_production_date + '~t' + &
				ls_shift + '~r~n' 

ll_modrows = dw_1.ModifiedCount()

IF ll_modrows <= 0 Then 
	SetMicroHelp('No Update Necessary')
	Return False
Else
 	SetMicroHelp("Wait... Updating Database")
	SetPointer(HourGlass!)
End if

This.SetRedraw(False)


is_update_string = ""

wf_update_modify()  

//messageBox('update', is_update_string)

/*IF Not iu_rmt001.uf_rmtr24mr_upd_sel_percent(istr_error_info, ls_header, &
											is_update_string) THEN 
	This.SetRedraw(True)
	Return False
End If
*/
IF Not iu_ws_pas4.NF_RMTR24NR(istr_error_info, ls_header, &
											is_update_string) THEN 
	This.SetRedraw(True)
	Return False
End If

SetMicroHelp("Modification Successful")
dw_1.ResetUpdate()
This.SetRedraw(True) 
Return( True )

end function

public subroutine wf_update_modify ();String						ls_hot_box_name,ls_selection_name
long							ll_count				
DWBuffer						ldwb_buffer


ldwb_buffer = Primary!

FOR ll_count = 1 TO 7
	ls_selection_name = 'selection_' + string(ll_count)	
	ls_hot_box_name   = 'hot_box_' 	+ string(ll_count)	
	is_update_string += &
		String(dw_1.GetItemDecimal(1,ls_selection_name,ldwb_buffer,False)) + "~t"	 
	is_update_string += &
		String(dw_1.GetItemDecimal(1,ls_hot_box_name,ldwb_buffer,False)) + "~t"	 
NEXT

end subroutine

event deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_new')

end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')



end event

event ue_postopen;call super::ue_postopen;Environment	le_env

//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "selperc"
istr_error_info.se_user_id 		= sqlca.userid

GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if


// open inquire window
is_inquire_window_name = 'w_selection_percent_inq'

iu_rmt001 = Create u_rmt001
iu_ws_pas4	= Create u_ws_pas4

wf_retrieve()

end event

on w_selection_percent.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_shift=create dw_shift
this.dw_effective_date=create dw_effective_date
this.dw_select_time=create dw_select_time
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_shift
this.Control[iCurrent+3]=this.dw_effective_date
this.Control[iCurrent+4]=this.dw_select_time
this.Control[iCurrent+5]=this.dw_1
end on

on w_selection_percent.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_shift)
destroy(this.dw_effective_date)
destroy(this.dw_select_time)
destroy(this.dw_1)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant' 
		Message.StringParm = dw_plant.uf_get_plant_code()
	Case 'shift'
		Message.StringParm = dw_shift.uf_get_shift()
	Case 'time'
		Message.StringParm = dw_select_time.uf_get_select_time()
		Message.StringParm = is_select_time
	Case 'effective_date'
		Message.StringParm = String(dw_effective_date.uf_get_effective_date())
End Choose






end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant' 
		dw_plant.uf_set_plant_code(as_value)
	Case 'shift' 
		dw_shift.uf_set_shift(as_value)
	Case 'time'
		dw_select_time.uf_set_select_time(as_value)
		is_select_time = as_value
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date(Date(as_value))
End Choose




end event

type dw_plant from u_plant within w_selection_percent
integer x = 219
integer y = 3
integer width = 1503
integer height = 90
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;Disable()
end event

type dw_shift from u_shift within w_selection_percent
integer x = 234
integer y = 93
integer width = 333
integer height = 74
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_enable(False)
end event

type dw_effective_date from u_effective_date within w_selection_percent
boolean visible = false
integer y = 160
integer width = 728
integer height = 93
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_initilize('Production Date:','')
This.uf_set_effective_date(Today())
This.uf_enable(False)
end event

type dw_select_time from u_selection within w_selection_percent
integer x = 1843
integer height = 256
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_enable(False)
end event

type dw_1 from u_base_dw_ext within w_selection_percent
integer y = 256
integer width = 2575
integer height = 320
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_selection_percent"
boolean border = false
end type

event constructor;call super::constructor;is_select_time = 'D'
end event

event itemerror;call super::itemerror;Return 2
end event

event itemchanged;call super::itemchanged;String 	ls_name
Dec   	ld_100_percent = 100.00

// I did this to make the code more generic
ls_name = dwo.name
ls_name = Left(ls_name, Len(ls_name) - 2)

CHOOSE CASE ls_name
	Case 'hot_box'
		IF Not IsNumber(data) Then
			iw_frame.SetMicroHelp("Hot Box Capacity must be numeric")
			This.SelectText(1, 100)
			Return 1
		End IF	
		If Dec(data) < 0 Then
			iw_frame.SetMicroHelp("Hot Box Capacity may not be less than zero")
			This.SelectText(1, 100)
			Return 1
		End IF	
	Case 'selection'
		IF Not IsNumber(data) Then
			iw_frame.SetMicroHelp("Selection Percent must be numeric")
			This.SelectText(1, 100)
			Return 1
		End IF	
		If Dec(data) > ld_100_percent Then
			iw_frame.SetMicroHelp("Selection Percent may not be greater than 100.00")
			This.SelectText(1, 100)
			Return 1
		End IF	
		If Dec(data) < 0 Then
			iw_frame.SetMicroHelp("Selection Percent may not be less than zero")
			This.SelectText(1, 100)
			Return 1
		End IF	
End Choose
end event

