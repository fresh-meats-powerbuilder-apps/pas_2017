﻿$PBExportHeader$w_cattle_characteristics.srw
$PBExportComments$IBDKDLD window for maintaining cattle characteristics
forward
global type w_cattle_characteristics from w_base_sheet_ext
end type
type dw_cattle from u_base_dw_ext within w_cattle_characteristics
end type
end forward

global type w_cattle_characteristics from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 2904
integer height = 1517
long backcolor = 67108864
event ue_keydown pbm_dwnkey
dw_cattle dw_cattle
end type
global w_cattle_characteristics w_cattle_characteristics

type variables
s_error			istr_error_info

datastore			ids_tree,&
						ids_desc, &
						ids_program_type, &
						ids_grade, &
						ids_cool_code, &
						ids_chr

u_rmt001			iu_rmt001
u_pas203			iu_pas203
u_ws_pas4	iu_ws_pas4

Boolean			ib_updating,&
			ib_char_key

Long			il_char_count, &
			il_rec_count

String			is_update_string,&  
					is_desc, &
					is_column_name
Window			iw_temp

u_sect_functions		iu_sect_functions
end variables

forward prototypes
public function boolean wf_deleterow ()
public subroutine wf_delete ()
public subroutine wf_filenew ()
public function boolean wf_update ()
public subroutine wf_setup_dropdown ()
public subroutine wf_setup_dddws (string as_grade, string as_yields, string as_program, string as_cool_code, string as_chr)
public function boolean wf_retrieve ()
public function boolean wf_validate (long al_row)
public function string wf_setup_listboxes (string as_char_type, long al_row, string as_return_type)
public function boolean wf_update_modify (long al_row, character ac_status_ind, string as_header)
public function boolean wf_addrow ()
end prototypes

public function boolean wf_deleterow ();Boolean		lb_ret

dw_cattle.SetFocus()
lb_ret = super::wf_deleterow()

dw_cattle.SelectRow(0,False)
dw_cattle.SelectRow(dw_cattle.GetRow(),True)

return lb_ret


end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_delrows, 	ll_count

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_comment
					
u_string_functions u_string

SetPointer(HourGlass!)

ib_updating = True

IF dw_cattle.AcceptText() = -1 Then 
	ib_updating = False
	Return False
End if

//ls_plant = dw_plant.uf_get_plant_code()
//
//If u_string.nf_IsEmpty(ls_Plant) Then 
//	iw_frame.SetMicroHelp("Please enter a Plant before updating.")
//	ib_updating = False
//	Return False
//End If
//
//ls_header = ls_plant	+ "~r~n"

ll_modrows = dw_cattle.ModifiedCount()
ll_delrows = dw_cattle.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0 Then 
	ib_updating = False
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

This.SetRedraw(False)


is_update_string = ""
//il_rec_count = 0
ll_row = 0
dw_cattle.SelectRow(0,False)

For ll_count = 1 To ll_delrows 

	lc_status_ind = 'D'
	If Not This.wf_update_modify(ll_count, lc_status_ind, ls_header) Then 
		ib_updating = False
		This.SetRedraw(True)
		Return False
	End if

Next

lc_status_ind = ' '
For ll_count = 1 To ll_modrows
	ll_Row = dw_cattle.GetNextModified(ll_Row, Primary!) 
	If Not wf_validate(ll_row) Then 
		This.SetRedraw(True)
		Return False
	End If
	
	ls_comment = iu_sect_functions.uf_condense(dw_cattle.GetItemString(ll_row,'desc'), ls_comment)
	dw_cattle.SetItem(ll_row, 'desc', ls_comment)
	ls_comment = iu_sect_functions.uf_condense(dw_cattle.GetItemString(ll_row,'short_desc'), ls_comment)
	dw_cattle.SetItem(ll_row, 'short_desc', ls_comment)
	
	Choose Case dw_cattle.GetItemStatus(ll_row, 0, Primary!)
		CASE NewModified!
			lc_status_ind = 'A'
		CASE DataModified!
			lc_status_ind = 'U'
	END CHOOSE	

	If Not This.wf_update_modify(ll_row, lc_status_ind, ls_header) Then 
		This.SetRedraw(True)
		Return False
	End If
Next


SetMicroHelp("Wait, Updating ...")
SetPointer(HourGlass!)

//dw_plant_constr.SetRedraw(False)
If il_rec_count > 0 Then
	IF Not iu_ws_pas4.NF_RMTR02NR(istr_error_info, &
			is_update_string)  THEN 
		ib_updating = False
		This.SetRedraw(True)
		Return False
	End if
End If

// leave for now
//FOR ll_count = 1 TO dw_cattle.RowCount()
//	dw_cattle.SetItem(ll_count,'unprotect_char_key',"N")
//	dw_cattle.SetItem(ll_count,'protect',"Y")
//NEXT

iw_frame.SetMicroHelp("Modification Successful")
dw_cattle.ResetUpdate()
This.SetRedraw(True)

ib_updating = False
Return( True )
end function

public subroutine wf_setup_dropdown ();
end subroutine

public subroutine wf_setup_dddws (string as_grade, string as_yields, string as_program, string as_cool_code, string as_chr);						
DataWindowChild	ldwc_min_yield,&
						ldwc_max_yield,&
						ldwc_destination,&
						ldwc_program


ids_program_type =  Create DataStore
ids_program_type.DataObject = "d_rmt_program_dddw"
ids_program_type.Reset()
ids_program_type.ImportString(as_program)
ids_program_type.Sort()

ids_grade =  Create DataStore
ids_grade.DataObject = "d_rmt_grade_dddw"
ids_grade.Reset()
ids_grade.ImportString(as_grade)
ids_grade.Sort()

ids_cool_code =  Create DataStore
ids_cool_code.DataObject = "d_rmt_cool_code_dddw"
ids_cool_code.Reset()
ids_cool_code.ImportString(as_cool_code)
ids_cool_code.Sort()

ids_chr =  Create DataStore
ids_chr.DataObject = "d_rmt_chr_dddw"
ids_chr.Reset()
ids_chr.ImportString(as_chr)
ids_chr.Sort()

// import min yield grades
dw_cattle.GetChild("min_grade", ldwc_min_yield)
ldwc_min_yield.ImportString(as_yields)
ldwc_min_yield.Sort()
// import max yield grades
dw_cattle.GetChild("max_grade", ldwc_max_yield)
ldwc_max_yield.ImportString(as_yields)
ldwc_max_yield.Sort()


end subroutine

public function boolean wf_retrieve ();Long					ll_row,ll_rec_count_rmt

String				ls_char, &
						ls_grade,&
						ls_yields,&
						ls_desc,&
						ls_chr, &
						ls_program,&
						ls_cool_code
						
				

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)

istr_error_info.se_procedure_name = "u_rmt001.nf_multi_dddw"
iu_rmt001 = Create u_rmt001

dw_cattle.reset()

// retrieve what is in the database
// Rev#01 added ls_program , Removed Sex & Destination
If Not iu_ws_pas4.NF_RMTR03NR(istr_error_info, ls_grade,ls_yields,ls_desc,ls_program,ls_cool_code, ls_chr)  Then
	MessageBox('DropDown Datawindows', 'There is a problem getting the multiple dddw items. Please call Applications.')
end If

wf_setup_dddws(ls_grade,ls_yields,ls_program,ls_cool_code, ls_chr)

ll_rec_count_rmt = dw_cattle.ImportString(ls_desc)

FOR ll_row = 1 TO ll_rec_count_rmt
	dw_cattle.SetItem(ll_row, 'short_desc',RightTrim(dw_cattle.GetItemString(ll_row,'short_desc')))
	dw_cattle.SetItem(ll_row, 'desc',RightTrim(dw_cattle.GetItemString(ll_row,'desc')))

	dw_cattle.object.program[ll_row] = wf_setup_listboxes("PGM", ll_row, 'desc')
	dw_cattle.object.grade[ll_row] = wf_setup_listboxes("GRD", ll_row, 'desc')
	dw_cattle.object.cool_code[ll_row] = wf_setup_listboxes("COOL", ll_row, 'desc')
NEXT

If ll_rec_count_rmt > 0 Then 
	SetMicroHelp(String(ll_rec_count_rmt) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

dw_cattle.ResetUpdate()
This.SetRedraw(True) 
dw_cattle.SetFocus()


Return True

end function

public function boolean wf_validate (long al_row);String						ls_temp
u_string_functions		lu_string



ls_temp = dw_cattle.GetItemString(al_row, "desc")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("desc")
	iw_frame.SetMicroHelp('Long Description is a required field')
	Return False
End If

ls_temp = dw_cattle.GetItemString(al_row, "short_desc")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("short_desc")
	iw_frame.SetMicroHelp('Short Description is a required field')
	Return False
End If

ls_temp = dw_cattle.GetItemString(al_row, "grade")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("grade")
	iw_frame.SetMicroHelp('Grade is a required field')
	Return False
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "min_wght"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_wght")
	iw_frame.SetMicroHelp('Min Weight Range is a required field')
	Return False
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "max_wght"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("max_wght")
	iw_frame.SetMicroHelp('Max Weight Range is a required field')
	Return False
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "min_grade"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_grade")
	iw_frame.SetMicroHelp('Min Yield Grade is a required field')
	Return False
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "max_grade"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("max_grade")
	iw_frame.SetMicroHelp('Max Yield Grade is a required field')
	Return False
End If
ls_temp = dw_cattle.GetItemString(al_row, "program")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("program")
	iw_frame.SetMicroHelp('Program is a required field')
	Return False
End If

If dw_cattle.GetItemDecimal(al_row,"max_wght") < &
		dw_cattle.GetItemDecimal(al_row,"min_wght") Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_wght")
	iw_frame.SetMicroHelp('The Min Weight Range must be less then or equal to Max Weight Range')
	Return False
End If

If dw_cattle.GetItemDecimal(al_row,"max_grade") < &
		dw_cattle.GetItemDecimal(al_row,"min_grade") Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_grade")
	iw_frame.SetMicroHelp('The Min Yield Grade must be less then or equal to Max Yield Grade')
	Return False
End If

Return True
end function

public function string wf_setup_listboxes (string as_char_type, long al_row, string as_return_type);datastore 			lds_dddw

Long					ll_row,ll_find,ll_count
				
String				ls_temp, &
						ls_chr,&
						ls_char_num,&
						ls_find_str, &
						ls_filter, &
						ls_DataObject, &
						ls_desc, &
						ls_char_desc

String				ls_char_code []


Choose Case as_char_type
	Case 'PGM'
		ls_desc = "program_desc"
		lds_dddw = ids_program_type
		ls_char_desc = "program"
	Case 'GRD'
		lds_dddw = ids_grade
		ls_desc = "grade_desc"
		ls_char_desc = "grade"
	Case 'COOL'
		lds_dddw = ids_cool_code
		ls_desc = "cool_code_desc"
		ls_char_desc = "code"
	Case Else
		return ""
End Choose		


ids_chr.SetFilter("")
ls_filter = "char_name = '" + dw_cattle.GetItemString(al_row, "short_desc") + &
		"' and char_type = '" + as_char_type + "'" 
ids_chr.SetFilter(ls_filter)
ids_chr.Filter()

if ids_chr.rowcount() > 0 then
	if as_char_type = "PGM" Then
		ls_char_code = ids_chr.Object.char_num.primary
	else
		ls_char_code = ids_chr.Object.char_code.primary
	end if
	ls_temp = ""
	For ll_count = 1 to UpperBound(ls_char_code)
		if as_return_type = "desc" then
			ls_find_str = ls_char_desc + " = '" + String(ls_char_code[ll_count]) + "'"
			
			ll_find = lds_dddw.Find ( ls_find_str, 1, lds_dddw.RowCount() )
			if ll_find > 0 then
				ls_temp += lds_dddw.GetItemString(ll_find, ls_desc) + '~r~n'
			else 
				ls_temp += "No matching description found" + '~r~n'
			end if
		else 
			ls_temp += String(ls_char_code[ll_count]) + '~t'
		end if
	Next
end if
return ls_temp


end function

public function boolean wf_update_modify (long al_row, character ac_status_ind, string as_header);String						ls_group_code,ls_user_id,ls_today,ls_temp,ls_temp1, ls_filter

Long							ll_row

Char							lc_status_ind

DWBuffer						ldwb_buffer


ls_user_id = Upper(sqlca.userid)
ls_today   = String(Today(),'yyyy-mm-dd')

IF ac_status_ind = "D" THEN
	ldwb_buffer = delete!
ELSE
	ldwb_buffer = primary!
End If

is_update_string += 'H' + '~t'
is_update_string +=  &
	dw_cattle.GetItemString(al_row,"desc",ldwb_buffer, False) + "~t" 
is_update_string += &
	dw_cattle.GetItemString(al_row,"short_desc",ldwb_buffer, False) + "~t" 
is_update_string += &
	String(dw_cattle.GetItemDecimal (al_row,"min_wght",ldwb_buffer, False)) + "~t" 
is_update_string += &
	String(dw_cattle.GetItemDecimal (al_row,"max_wght",ldwb_buffer, False)) + "~t" 
is_update_string += &
	String(dw_cattle.GetItemDecimal (al_row,"min_grade",ldwb_buffer, False)) + "~t" 
is_update_string += &
	String(dw_cattle.GetItemDecimal (al_row,"max_grade",ldwb_buffer, False)) + "~t" 
is_update_string += &
	dw_cattle.GetItemString(al_row,"desc_group",ldwb_buffer, False) + "~t" 
is_update_string += &
	dw_cattle.GetItemString(al_row,"comment",ldwb_buffer, False) + "~t" 
is_update_string += &
	ls_today + "~t" 
is_update_string += &
	ls_user_id + "~t" 	
is_update_string += ac_status_ind + "~r~n"	

ids_chr.SetFilter("")
ls_filter = "char_name = '" + dw_cattle.GetItemString(al_row, "short_desc") + "'" 
ids_chr.SetFilter(ls_filter)
ids_chr.Filter()

if ac_status_ind <> "D" then
	for ll_row = 1 to ids_chr.RowCount()
		is_update_string += 'D' + '~t'
		is_update_string += ids_chr.GetItemString(ll_row, 'char_name') + '~t'
		is_update_string += ids_chr.GetItemString(ll_row, 'char_type') + '~t'
		is_update_string += ids_chr.GetItemString(ll_row, 'char_code') + '~t'
		is_update_string += ids_chr.GetItemString(ll_row, 'char_num') + '~r~n'
	Next
End If

il_rec_count ++

/*If il_rec_count = 70 Then
	IF Not iu_rmt001.uf_rmtr02mr_upd_cattle_char(istr_error_info, as_header, &
			is_update_string) THEN Return False
	il_rec_count = 0
	is_update_string = ""
END IF
*/
If il_rec_count = 70 Then
	IF Not iu_ws_pas4.NF_RMTR02NR(istr_error_info, &
			is_update_string) THEN Return False
	il_rec_count = 0
	is_update_string = ""
END IF


Return True
end function

public function boolean wf_addrow ();Long 	ll_row

Super::wf_addrow()

ll_row = dw_cattle.GetRow ( )
If ll_row = 0 Then
	ll_row = 1
End If
// Rev#01 added	
dw_cattle.setitem(ll_row,'program','000')
// Rev#01 removed	
//dw_cattle.SetItem(ll_row,'sex',"0")
//dw_cattle.SetItem(ll_row,'destination',"0")
//rev #02 added cool code
dw_cattle.setitem(ll_row,'cool_code',' ')
dw_cattle.setitem(ll_row,'comment', ' ')
dw_cattle.ScrollToRow (ll_row)
dw_cattle.SetColumn ( 'desc' )
dw_cattle.SetFocus()

Return (True)


end function

on w_cattle_characteristics.create
int iCurrent
call super::create
this.dw_cattle=create dw_cattle
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cattle
end on

on w_cattle_characteristics.destroy
call super::destroy
destroy(this.dw_cattle)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

end event

event open;call super::open;This.Title = 'Cattle Characteristics'

end event

event ue_get_data;call super::ue_get_data;String 			ls_char_type

Choose Case is_column_name
	Case 'program'
		ls_char_type = 'PGM'
	Case 'grade'
		ls_char_type = 'GRD'
	Case 'cool_code'
		ls_char_type = 'COOL'
End Choose


Choose Case as_value
	Case 'PGM'
		message.StringParm = ids_program_type.object.datawindow.data
	Case 'GRD'
		message.StringParm = ids_grade.object.datawindow.data
	Case 'COOL'
		message.StringParm = ids_cool_code.object.datawindow.data
	Case 'Char'
		message.StringParm = wf_setup_listboxes(ls_char_type, dw_cattle.GetRow(), 'code')
	Case 'Type'
		message.StringParm = ls_char_type
	Case 'Characteristic'
		message.StringParm = dw_cattle.GetItemString(dw_cattle.GetRow(), 'desc')
End choose

end event

event ue_postopen;call super::ue_postopen;This.PostEvent('ue_query')//  structure for calls to the rpc's

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "catchar"
istr_error_info.se_user_id 		= sqlca.userid
iu_ws_pas4	= Create u_ws_pas4

wf_retrieve()

end event

event ue_set_data;call super::ue_set_data;
u_string_functions	lu_string_functions

String					ls_item, &
							ls_char_type

Long						ll_row, &
							ll_newrow
							
							
Choose Case as_data_item
	Case 'Selected'
		ls_char_type = lu_string_functions.nf_gettoken(as_value, '~t')
		
		Do While ids_chr.rowcount() > 0
			ids_chr.DeleteRow(1)
		Loop
		ll_row = dw_cattle.GetRow()
		Do While Len(Trim(as_value)) > 0
			ls_item = lu_string_functions.nf_gettoken(as_value, '~t')
			ll_newrow = ids_chr.InsertRow(0)
			ids_chr.SetItem(ll_newrow, 'char_name', dw_cattle.GetItemString(ll_row, "short_desc"))
			ids_chr.SetItem(ll_newrow, 'char_type', ls_char_type)
			if ls_char_type = 'PGM' then
				ids_chr.SetItem(ll_newrow, 'char_code', ' ')
				ids_chr.SetItem(ll_newrow, 'char_num', ls_item)
			else
				ids_chr.SetItem(ll_newrow, 'char_code', ls_item)
				ids_chr.SetItem(ll_newrow, 'char_num', '0')
			end if
		Loop
		
		Choose Case ls_char_type 
			Case 'PGM'
				dw_cattle.object.program[ll_row] = ""
				dw_cattle.object.program[ll_row] = wf_setup_listboxes(ls_char_type, ll_row, 'desc')
			Case 'GRD'
				dw_cattle.object.grade[ll_row] = ""
				dw_cattle.object.grade[ll_row] = wf_setup_listboxes(ls_char_type, ll_row, 'desc')
			Case 'COOL'
				dw_cattle.object.cool_code[ll_row] = ""
				dw_cattle.object.cool_code[ll_row] = wf_setup_listboxes(ls_char_type, ll_row, 'desc')
		End Choose
End Choose


end event

event close;call super::close;//If isvalid(iw_temp) Then
//	Close(iw_temp)
//End If
end event

event ue_revisions;call super::ue_revisions;/*****************************************************************
**   REVISION NUMBER: Rev#01
**   PROJECT NUMBER:  Support
**   DATE:				 January 2000            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Redesign of Window
**                    Remove Sex & Destination, Added Program Type 
******************************************************************
**   REVISION NUMBER: Rev #02
**   PROJECT NUMBER: SR7595	 
**   DATE:				November 2008            
**   PROGRAMMER: 		Donna Keairns     
**   PURPOSE:  		Added cool code logic       
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

event resize;call super::resize;//constant integer li_x		= 40 //0
//constant integer li_y		= 26 //96

integer li_x		= 70 
integer li_y		= 56 


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

  
//dw_cattle.width	= newwidth - (30 + li_x)
//dw_cattle.height	= newheight - (30 + li_y)
  
dw_cattle.width	= newwidth - (li_x)
dw_cattle.height	= newheight - (li_y)
end event

type dw_cattle from u_base_dw_ext within w_cattle_characteristics
event ue_dwndropdown pbm_dwndropdown
integer x = 33
integer y = 26
integer width = 2798
integer height = 1347
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_cattle_characteristics"
boolean minbox = true
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event constructor;call super::constructor;is_selection = '1'


end event

event itemchanged;call super::itemchanged;DataWindowChild 		ldwc_child
DataStore 				lds_Tmp
Long 						ll_count,ll_row,ll_find
String					ls_temp
Decimal					ld_temp
u_string_functions 	u_string

CHOOSE CASE dwo.name
//	CASE 'desc'
//		If u_string.nf_countoccurrences(data,'~r~n') > 0 Then
//			iw_frame.SetMicroHelp("You can't enter a carriage return(Enter) in Long Description")
//			This.SelectText(1, 100)
//			Return 1
//		End If
	CASE 'short_desc'
//		If u_string.nf_countoccurrences(data,'~r~n') > 0 Then
//			iw_frame.SetMicroHelp("You can't enter a carriage return(Enter) in Short Description")
//			This.SelectText(1, 100)
//			Return 1
//		End If
		ll_find = dw_cattle.Find ( "short_desc = '" + data + "'", 1, dw_cattle.RowCount() )
		If ll_find > 0 Then
			iw_frame.SetMicroHelp("This Short Description has already been used -- please choose another")
			This.SelectText(1, 100)
			Return 1
		End If
		// Set the group the same as the data
		dw_cattle.SetItem(row,'desc_group',data)
	CASE 'min_wght'
		IF Not IsNumber(data) Then
			iw_frame.SetMicroHelp("Min Weight Range must be numeric")
			This.SelectText(1, 100)
			Return 1
		End IF	
		IF sign(Integer(data)) = -1  Then
			iw_frame.SetMicroHelp("Min Weight can not be a negative")
			This.SelectText(1, 100)
			Return 1
		End IF	
		ld_temp =  dw_cattle.GetItemDecimal(row,'max_wght')
		If dec(data) > ld_temp and ld_temp > 0 Then
			iw_frame.SetMicroHelp("Min Weight Range can not be greater than the Max Weight Range")
			This.SelectText(1, 100)
			Return 1
		End If
	CASE 'max_wght'
		IF Not IsNumber(data) Then
			iw_frame.SetMicroHelp("Max Weight Range must be numeric")
			This.SelectText(1, 100)
			Return 1
		End IF	
		IF sign(Integer(data)) = -1 Then
			iw_frame.SetMicroHelp("Max Weight Range can not be a negative")
			This.SelectText(1, 100)
			Return 1
		End IF	
		ld_temp =  dw_cattle.GetItemDecimal(row,'min_wght')
		If dec(data) < ld_temp Then
			iw_frame.SetMicroHelp("Max Weight Range can not be less than the Min Weight Range")
			This.SelectText(1, 100)
			Return 1
		End If
	CASE 'min_grade'
		ld_temp =  dw_cattle.GetItemDecimal(row,'max_grade')
		If dec(data) > ld_temp and ld_temp > 0 Then
			iw_frame.SetMicroHelp("Min Yield Grade can not be greater than the Max Yield Grade")
			This.SelectText(1, 100)
			Return 1
		End If
	CASE 'max_grade'
		ld_temp =  dw_cattle.GetItemDecimal(row,'min_grade')
		If dec(data) < ld_temp Then
			iw_frame.SetMicroHelp("Max Yield Grade can not be less than the Min Yield Grade")
			This.SelectText(1, 100)
			Return 1
		End If
// Rev#01 removed	
//	CASE 'destination'
//		dw_cattle.GetChild("destination", ldwc_child)
//		ll_find = ldwc_child.Find ( "dest_id = '" + data + "'", 1, ldwc_child.RowCount() )
//		If ll_find = 0 then
//			iw_frame.SetMicroHelp("The Destination is not a valid destination")
//			This.SelectText(1, 100)
//			Return 1
//		End If
//	CASE 'sex'
//		dw_cattle.GetChild("sex", ldwc_child)
//		ll_find = ldwc_child.Find ( "sex = '" + data + "'", 1, ldwc_child.RowCount() )
//		If ll_find = 0 then
//			iw_frame.SetMicroHelp("The Sex is not a valid sex")
//			This.SelectText(1, 100)
//			Return 1
//		End If
// Rev#01 added 	
//rev #02 add cool code
END CHOOSE

iw_frame.SetMicroHelp("Ready")
Return 0
end event

event itemerror;call super::itemerror;Return 2

end event

event clicked;call super::clicked;String ls_filter

is_column_name = dwo.name

if is_column_name = 'program' or is_column_name = 'grade' or is_column_name = 'cool_code' then
	OpenWithParm(w_cattle_characteristics_selection, iw_parent)
	this.SetRedraw(false)
	this.TriggerEvent("RowFocusChanged")
	this.SelectRow(this.GetRow(), true)
	this.SetRedraw(true)
End if


end event

