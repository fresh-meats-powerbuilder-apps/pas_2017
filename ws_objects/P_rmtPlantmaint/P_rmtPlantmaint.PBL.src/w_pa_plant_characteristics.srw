﻿$PBExportHeader$w_pa_plant_characteristics.srw
$PBExportComments$IBDKDLD window for maintaining pa/plant characteristics
forward
global type w_pa_plant_characteristics from w_base_sheet_ext
end type
type dw_plant from u_plant within w_pa_plant_characteristics
end type
type dw_cattle from u_base_dw_ext within w_pa_plant_characteristics
end type
end forward

global type w_pa_plant_characteristics from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 2922
integer height = 1552
long backcolor = 67108864
event ue_keydown pbm_dwnkey
dw_plant dw_plant
dw_cattle dw_cattle
end type
global w_pa_plant_characteristics w_pa_plant_characteristics

type variables
s_error			istr_error_info

datastore		ids_tree,&
					ids_desc, &
					ids_chr

u_rmt001			iu_rmt001
u_pas203			iu_pas203
u_ws_pas3			iu_ws_pas3
u_ws_pas4	iu_ws_pas4

Boolean			ib_updating,&
					ib_char_key

Long				il_char_count, &
					il_rec_count

String			is_update_string,&  
					is_desc
Window			iw_temp

DWItemStatus	idw_status
end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_validate (long al_row)
public subroutine wf_delete ()
public function boolean wf_validate_last_row (long al_row, ref string as_new_data)
public subroutine wf_filenew ()
public function boolean wf_check_datastore ()
public function boolean wf_ellipsis ()
public function boolean wf_deleterow ()
public subroutine wf_import_desc (string as_desc)
public function boolean wf_remove_used_desc ()
public function boolean wf_addrow ()
public function boolean wf_update_modify (long al_row, character ac_status_ind, string as_header)
public function boolean wf_retrieve ()
public function boolean wf_char_dddw ()
public subroutine wf_set_dddw (long al_row, string as_char_type)
public subroutine wf_changeItemStatus (string as_column_name, long as_row)
public subroutine wf_change_item_status (string as_column_name, long al_row)
end prototypes

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_delrows, 	ll_count

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header
u_string_functions u_string

SetPointer(HourGlass!)

ib_updating = True

IF dw_cattle.AcceptText() = -1 Then 
	ib_updating = False
	Return False
End if

ls_header = dw_plant.uf_get_plant_code()	+ "~r~n"

ll_modrows = dw_cattle.ModifiedCount()
ll_delrows = dw_cattle.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0 Then 
	ib_updating = False
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

This.SetRedraw(False)


is_update_string = ""
ll_row = 0
dw_cattle.SelectRow(0,False)

For ll_count = 1 To ll_delrows 

	lc_status_ind = 'D'

	If Not This.wf_update_modify(ll_count, lc_status_ind, ls_header) Then 
		ib_updating = False
		This.SetRedraw(True)
		Return False
	End if

Next

For ll_count = 1 To ll_modrows
	ll_Row = dw_cattle.GetNextModified(ll_Row, Primary!) 
	If Not wf_validate(ll_row) Then 
		This.SetRedraw(True)
		Return False
	End If
	
	Choose Case dw_cattle.GetItemStatus(ll_row, 0, Primary!)
		CASE NewModified!
			lc_status_ind = 'A'
		CASE DataModified!
			lc_status_ind = 'M'
	END CHOOSE	

	If Not This.wf_update_modify(ll_row, lc_status_ind, ls_header) Then 
		This.SetRedraw(True)
		Return False
	End If
Next


SetMicroHelp("Wait, Updating ...")
SetPointer(HourGlass!)

/*If il_rec_count > 0 Then
	IF Not iu_rmt001.uf_rmtr07mr_upd_pa_cattle_char(istr_error_info, ls_header, &
												is_update_string) THEN 
		ib_updating = False
		This.SetRedraw(True)
		Return False
	End if
End If
*/
If il_rec_count > 0 Then
	IF Not iu_ws_pas4.NF_RMTR07NR(istr_error_info, ls_header, &
												is_update_string) THEN 
		ib_updating = False
		This.SetRedraw(True)
		Return False
	End if
End If



FOR ll_count = 1 TO dw_cattle.RowCount()
	dw_cattle.SetItem(ll_count,'unprotect_char_key',"N")
	dw_cattle.SetItem(ll_count,'protect',"Y")
NEXT

iw_frame.SetMicroHelp("Modification Successful")
dw_cattle.ResetUpdate()
This.SetRedraw(True)

ib_updating = False
Return( True )
end function

public function boolean wf_validate (long al_row);String						ls_temp
u_string_functions		lu_string


ls_temp = String(dw_cattle.GetItemNumber(al_row, "char_key"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("char_key")
	iw_frame.SetMicroHelp('Characteristics is a required field')
	Return False
End If

ls_temp = dw_cattle.GetItemString(al_row, "short_desc")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("short_desc")
	iw_frame.SetMicroHelp('Short Description is a required field')
	Return False
End If

ls_temp = dw_cattle.GetItemString(al_row, "grade")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("grade")
	iw_frame.SetMicroHelp('Grade is a required field')
	Return False
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "min_wght"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_wght")
	iw_frame.SetMicroHelp('Min Weight Range is a required field')
	Return False
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "max_wght"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("max_wght")
	iw_frame.SetMicroHelp('Max Weight Range is a required field')
	Return False
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "min_grade"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_grade")
	iw_frame.SetMicroHelp('Min Yield Grade is a required field')
	Return False
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "max_grade"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("max_grade")
	iw_frame.SetMicroHelp('Max Yield Grade is a required field')
	Return False
End If
//Rev#01
//ls_temp = dw_cattle.GetItemString(al_row, "destination")
//If lu_string.nf_IsEmpty(ls_temp) Then
//	dw_cattle.ScrollToRow(al_row)
//	dw_cattle.SetColumn("destination")
//	iw_frame.SetMicroHelp('Destination is a required field')
//	Return False
//End If
//
//ls_temp = dw_cattle.GetItemString(al_row, "sex")
//If lu_string.nf_IsEmpty(ls_temp) Then
//	dw_cattle.ScrollToRow(al_row)
//	dw_cattle.SetColumn("sex")
//	iw_frame.SetMicroHelp('Sex is a required field')
//	Return False
//End If

ls_temp = dw_cattle.GetItemString(al_row, "program")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("program")
	iw_frame.SetMicroHelp('Program is a required field')
	Return False
End If

If dw_cattle.GetItemDecimal(al_row,"max_wght") < &
		dw_cattle.GetItemDecimal(al_row,"min_wght") Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_wght")
	iw_frame.SetMicroHelp('The Min Weight Range must be less then or equal to Max Weight Range')
	Return False
End If

If dw_cattle.GetItemDecimal(al_row,"max_grade") < &
		dw_cattle.GetItemDecimal(al_row,"min_grade") Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_grade")
	iw_frame.SetMicroHelp('The Min Yield Grade must be less then or equal to Max Yield Grade')
	Return False
End If

ls_temp = dw_cattle.GetItemString(al_row, "slaughter")
If ls_temp = 'N' Then
	ls_temp = dw_cattle.GetItemString(al_row, "break")
	If ls_temp = 'N' Then
		dw_cattle.ScrollToRow(al_row)
		dw_cattle.SetColumn("slaughter")
		iw_frame.SetMicroHelp('Slaughter and/or Break is a required field')
		Return False
	End If
End If

Return True

end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_validate_last_row (long al_row, ref string as_new_data);String						ls_temp
u_string_functions		lu_string


ls_temp = dw_cattle.GetItemString(al_row, "desc")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("desc")
	MessageBox('Required Field', 'Description is a required field.  Please enter a value for Description.')
	Return False
Else
	as_new_data = ls_temp + '~t'
End If

ls_temp = dw_cattle.GetItemString(al_row, "grade")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("grade")
	MessageBox('Required Field', 'Grade is a required field.  Please enter a value for Grade.')
	Return False
Else
	as_new_data += ls_temp + '~t'
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "min_wght"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_wght")
	MessageBox('Required Field', 'Min Weight Range is a required field.  Please enter a value for Min Weight Range.')
	Return False
Else
	as_new_data += ls_temp + '~t'
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "max_wght"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("max_wght")
	MessageBox('Required Field', 'Max Weight Range is a required field.  Please enter a value for Max Weight Range.')
	Return False
Else
	as_new_data += ls_temp + '~t'
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "min_grade"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("min_grade")
	MessageBox('Required Field', 'Min Yield Grade is a required field.  Please enter a value for Min Yield Grade.')
	Return False
Else
	as_new_data += ls_temp + '~t'
End If

ls_temp = String(dw_cattle.GetItemDecimal(al_row, "max_grade"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("max_grade")
	MessageBox('Required Field', 'Max Yield Grade is a required field.  Please enter a value for Max Yield Grade.')
	Return False
Else
	as_new_data += ls_temp + '~t'
End If
//Rev#01
//ls_temp = dw_cattle.GetItemString(al_row, "destination")
//If lu_string.nf_IsEmpty(ls_temp) Then
//	dw_cattle.ScrollToRow(al_row)
//	dw_cattle.SetColumn("destination")
//	MessageBox('Required Field', 'Destination is a required field.  Please enter a value for Destination.')
//	Return False
//Else
//	as_new_data += ls_temp + '~t'
//End If
//
//ls_temp = dw_cattle.GetItemString(al_row, "sex")
//If lu_string.nf_IsEmpty(ls_temp) Then
//	dw_cattle.ScrollToRow(al_row)
//	dw_cattle.SetColumn("sex")
//	MessageBox('Required Field', 'Sex is a required field.  Please enter a value for Sex.')
//	Return False
//Else
//	as_new_data += ls_temp 
//End If

ls_temp = dw_cattle.GetItemString(al_row, "program")
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_cattle.ScrollToRow(al_row)
	dw_cattle.SetColumn("program")
	MessageBox('Required Field', 'Program is a required field.  Please enter a value for Program.')
	Return False
Else
	as_new_data += ls_temp 
End If

Return True
end function

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_check_datastore ();Window			lw_Temp,lw_sheet	
Long				ll_ret
boolean 			lb_valid


If ids_desc.RowCount() = 0 Then
	ll_ret = MessageBox("Add Row Result", "You have used all the unique descriptions.  To add new descriptions press Yes to go to the Cattle Characteristics window.", &
				Question!, YesNO!, 1)
	IF ll_ret = 1 THEN
 		// Process Yes.
		// If the Cattle Characteristics window is open bring it to the top
		// else open it
		SetPointer(HourGlass!)
		lw_sheet = iw_frame.GetFirstSheet()
		IF IsValid(lw_sheet) THEN
			DO
				lw_sheet = iw_frame.GetNextSheet(lw_sheet)
				lb_valid = IsValid (lw_sheet)
				IF lb_valid Then
					If lw_sheet.Title = 'Cattle Characteristics' THEN 
						lw_sheet.BringToTop = TRUE
						Return False
					End IF
				End If
			LOOP WHILE lb_valid
		END IF
		OpenSheet(lw_temp, "w_cattle_characteristics", iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen/*Original!*/ )
		Return False
	ELSE
 		// Process No.
		Return False
	END IF
Else
	ids_desc.Sort()
	Return True
End If
end function

public function boolean wf_ellipsis ();If Not wf_check_datastore() Then
	Return False
End IF

ids_desc.Sort()
OpenWithParm ( iw_Temp, This , 'w_cattle_characteristics_response' )

Return True



end function

public function boolean wf_deleterow ();Boolean		lb_ret
Long			ll_row,ll_row_count,ll_loop,ll_counter,ll_find,ll_temp
String		ls_temp,ls_temp1


dw_cattle.SetFocus()
ll_row = dw_cattle.GetRow()

If ll_row < 1 Then Return FAlse 


dw_cattle.SelectRow(ll_row,True)
// IF row is newmod then 
If dw_cattle.GetItemStatus(ll_row,0, Primary!) = NewModified! Then
	//If wf_validate_last_row(ll_row,ls_new_data)  Then
	ls_temp = dw_cattle.GetItemString (ll_row, 'desc')
	ll_find = ids_desc.Find ( "desc_child = '" + ls_temp + "'", 1, ids_desc.RowCount() )
	If ll_find > 0 Then
		ll_row_count = dw_cattle.RowCount()
		FOR ll_loop = 1 TO ll_row_count
			ls_temp1 = dw_cattle.GetItemString (ll_loop, 'desc')
			If ls_temp = ls_temp1 Then
				ll_counter ++
			End If
		NEXT
		If ll_counter = 1 Then
			ids_desc.RowsDiscard(ll_find, ll_find, Primary! )
			ids_desc.Sort()
		End If
	End If
Else
	// If the row is not new modified then import back into the datastore
	ls_temp  = dw_cattle.GetItemString (ll_Row, 'desc') + '~t'
	ls_temp += dw_cattle.GetItemString (ll_Row, 'grade') + '~t'
	ls_temp += String(dw_cattle.GetItemDecimal (ll_Row, 'min_wght')) + '~t'
	ls_temp += String(dw_cattle.GetItemDecimal (ll_Row, 'max_wght')) + '~t'
	ls_temp += String(dw_cattle.GetItemDecimal (ll_Row, 'min_grade')) + '~t'
	ls_temp += String(dw_cattle.GetItemDecimal (ll_Row, 'max_grade')) + '~t'
//Rev#01
	ls_temp += dw_cattle.GetItemString ( ll_Row, 'program') + '~t'
//	ls_temp += dw_cattle.GetItemString ( ll_Row, 'destination') + '~t'
//	ls_temp += dw_cattle.GetItemString ( ll_Row, 'sex') 
//rev#02
	ls_temp += dw_cattle.GetItemSTring (ll_row, 'cool_code') + '~t'
	ll_temp = ids_desc.ImportString(ls_temp) 
	ids_desc.Sort()
End if

lb_ret = super::wf_deleterow()
return lb_ret

end function

public subroutine wf_import_desc (string as_desc);u_string_functions 	u_string
String 					ls_temp
Long						ll_loop = 1,ll_row,ll_find


ll_row = dw_cattle.GetSelectedRow ( 0 )
dw_cattle.SetRedraw ( False )
//rev#01
//DO WHILE ll_loop < 10
DO WHILE ll_loop < 10
	CHOOSE CASE ll_loop
		CASE 1
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
			dw_cattle.SetItem ( ll_Row, 'short_desc', ls_temp)
		CASE 2
			// This part of the string not used
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
		CASE 3
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
			dw_cattle.SetItem ( ll_Row, 'grade', ls_temp)
		CASE 4
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
			dw_cattle.SetItem ( ll_Row, 'min_wght', Dec(ls_temp))
		CASE 5
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
			dw_cattle.SetItem ( ll_Row, 'max_wght', Dec(ls_temp))
		CASE 6
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
			dw_cattle.SetItem ( ll_Row, 'min_grade', Dec(ls_temp))
		CASE 7
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
			dw_cattle.SetItem ( ll_Row, 'max_grade', Dec(ls_temp))
//rev#01
		CASE 8
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
			dw_cattle.SetItem ( ll_Row, 'program', ls_temp)
//rev#02
		CASE 9
			ls_temp = u_string.nf_gettoken(as_desc,'~t')
			dw_cattle.SetItem ( ll_Row, 'cool_code', ls_temp)
//			dw_cattle.SetColumn ( 'cool_code' )
//			dw_cattle.Accepttext()

//		CASE 8
//			ls_temp = u_string.nf_gettoken(as_desc,'~t')
//			dw_cattle.SetItem ( ll_Row, 'destination', ls_temp)
//		CASE 9
//			ls_temp = u_string.nf_gettoken(as_desc,'~t')
//			dw_cattle.SetItem ( ll_Row, 'sex', ls_temp)
	END CHOOSE
	ll_loop ++
LOOP

dw_cattle.SetItem ( ll_Row, 'update_date', Today())
dw_cattle.SetItem ( ll_Row, 'update_user', String(Upper(sqlca.userid)))
dw_cattle.SetItem ( ll_Row, 'protect', 'Y')
dw_cattle.SetItem ( ll_Row, 'unprotect_char_key', 'Y')

dw_cattle.SetColumn ( 'char_key' )
dw_cattle.SetFocus()
dw_cattle.SetRedraw ( True )

end subroutine

public function boolean wf_remove_used_desc ();Long 		ll_row,ll_rec_count,ll_find
String	ls_temp,ls_grade,ls_yields,ls_desc,ls_program,ls_cool_code



//Inquire on descriptions
istr_error_info.se_procedure_name = "u_rmt001.nf_multi_dddw"
iu_rmt001 = Create u_rmt001

// Get the multi dddw
// Rev#01
string ls_char
/*If Not iu_rmt001.uf_rmtr03mr_inq_dddw(istr_error_info, ls_grade,ls_yields,ls_desc,ls_program,ls_cool_code, ls_char)  Then
	MessageBox('DropDown Datawindows', 'There is a problem getting the multiple dddw items. Please call Applications.')
end If
*/
If Not iu_ws_pas4.NF_RMTR03NR(istr_error_info, ls_grade,ls_yields,ls_desc,ls_program,ls_cool_code, ls_char)  Then
	MessageBox('DropDown Datawindows', 'There is a problem getting the multiple dddw items. Please call Applications.')
end If



// import descriptions
ids_desc = Create DataStore
ids_desc.DataObject = 'd_cattle_characteristics_child'
ll_rec_count = ids_desc.ImportString(ls_desc)
//messagebox('# of imported rows',string(ll_rec_count))
FOR ll_row = 1 TO ll_rec_count
	// strip the trailing spaces from the child desc
	ids_desc.SetItem(ll_row, 'desc_child',RightTrim(ids_desc.GetItemString(ll_row,'desc_child')))
NEXT

//////////
ll_rec_count = dw_cattle.RowCount()
FOR ll_row = 1 TO ll_rec_count
	// strip the trailing spaces from desc
	dw_cattle.SetItem(ll_row, 'short_desc',RightTrim(dw_cattle.GetItemString(ll_row,'short_desc')))
	// throw out the used desc from the datastore
	ls_temp = dw_cattle.GetItemString(ll_row, 'short_desc')
	ll_find = ids_desc.Find ("desc_child = '" + ls_temp + "'", 1, ids_desc.RowCount())
	If ll_find > 0 Then
		ids_desc.RowsDiscard(ll_find,ll_find, Primary!)
	End If
NEXT

ids_desc.Sort()
Return True
end function

public function boolean wf_addrow ();Window			lw_Temp	
Long				ll_new_row,ll_ret

If Not wf_remove_used_desc() Then
	Return False
End If

If Not wf_check_datastore() Then
	Return False
End IF

//If ids_desc.RowCount() = 0 Then
//	ll_ret = MessageBox("Add Row Result", "You have used all the unique descriptions, to add new descriptions press Yes to go to the Cattle Characteristics window.", &
//				Question!, YesNO!, 1)
//	IF ll_ret = 1 THEN
// 		// Process Yes.
//		SetPointer(HourGlass!)
//		OpenSheet(lw_temp, "w_cattle_characteristics", iw_frame, 0, Original! )
//		Return False
//	ELSE
// 		// Process No.
//		Return False
//	END IF
//End If
////	///
//	iw_frame.SetMicroHelp("You have used all the unique descriptions - to add new descriptions go to the Cattle Characteristics window")
//	Return False
//End If
Super:: wf_addrow()
 
ll_new_row = dw_cattle.GetRow ( )
if ll_new_row = 0 then ll_new_row = 1
dw_cattle.SetItem(ll_new_row,'protect',"N")
dw_cattle.SetItem(ll_new_row,'unprotect_char_key',"Y")
dw_cattle.SetItem(ll_new_row,'slaughter',"Y")
dw_cattle.SetItem(ll_new_row,'break',"Y")
//Rev#01
//dw_cattle.SetItem(ll_new_row,'sex',"0")
//dw_cattle.SetItem(ll_new_row,'destination',"0")
dw_cattle.SetItem(ll_new_row,'program',"000")


IF Not wf_char_dddw() Then
	Return False
End if

dw_cattle.SetColumn ( 'char_key' )
dw_cattle.SetFocus()
Return True


end function

public function boolean wf_update_modify (long al_row, character ac_status_ind, string as_header);String						ls_group_code,ls_user_id,ls_today,ls_temp,ls_temp1

Char							lc_status_ind

DWBuffer						ldwb_buffer


ls_user_id = Upper(sqlca.userid)
ls_today   = String(Today(),'yyyy-mm-dd')

IF ac_status_ind = "D" THEN
	ldwb_buffer = delete!
ELSE
	ldwb_buffer = primary!
End If

is_update_string += &
	String(dw_cattle.GetItemNumber(al_row,"char_key",ldwb_buffer, False)) + "~t" 
is_update_string += &
	dw_cattle.GetItemString(al_row,"short_desc",ldwb_buffer, False) + "~t" 
is_update_string += &
	ls_today + "~t" 
is_update_string += &
	ls_user_id + "~t"
is_update_string += &
	dw_cattle.GetItemString(al_row,"slaughter",ldwb_buffer, False) + "~t" 
is_update_string += &
	dw_cattle.GetItemString(al_row,"break",ldwb_buffer, False) + "~t" 
is_update_string += ac_status_ind + "~r~n"	


il_rec_count ++

If il_rec_count = 100 Then
	IF Not  iu_ws_pas4.NF_RMTR07NR(istr_error_info, as_header, &
			is_update_string) THEN Return False
	il_rec_count = 0
	is_update_string = ""
END IF

Return True
end function

public function boolean wf_retrieve ();Long			ll_rec_count_pas,ll_row,ll_find,ll_rec_count_rmt
				
String		ls_plant, &
				ls_plant_desc, & 
				ls_header, &
				ls_detail, &
				ls_temp,&
				ls_char

DataWindowChild	ldwc_char


This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"


// Get the characteristics for the dddw
//If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
If iu_ws_pas3.uf_pasp61fr(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If
dw_cattle.GetChild("char_key", ldwc_char)
ldwc_char.Reset()
il_char_count = ldwc_char.ImportString(ls_char)


////////////////////////////////////////
wf_remove_used_desc()
///////////////////////////////////////////

istr_error_info.se_procedure_name = "nf_rmtr01mr_inq_cattle_characteristics"

ls_plant = dw_plant.uf_get_plant_code()
ls_plant_desc = dw_plant.uf_get_plant_descr()  

ls_header = ls_plant + '~r~n' 

dw_cattle.reset()

/*If iu_rmt001.uf_rmtr01mr_inq_cattle_char(istr_error_info, & 
										ls_header, &
										ls_detail) = -1 Then
										This.SetRedraw(True) 
										Return False
End If			
*/
If iu_ws_pas4.NF_RMTR01NR(istr_error_info, & 
										ls_header, &
										ls_detail) = -1 Then
										This.SetRedraw(True) 
										Return False
End If			



//ls_shift_header = '08:00~t08:00~t12:00~t15:00~t'
//ls_detail = &
//'33~tcattle theibpgod~tA~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'61~tcattle theibpgod~tB~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'41~tcattle theibpgod~tC~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'44~tcattle theibpgod~tD~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'34~tcattle theibpgod~tE~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'35~tcattle theibpgod~tF~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'36~tcattle theibpgod~tG~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'37~tcattle theibpgod~tH~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'38~tcattle theibpgod~tI~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'39~tcattle theibpgod~tJ~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'97~tcattle theibpgod~tA~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' + &
//'349~tcattle theibpgod~tB~t100.50~t1300.50~t1.5~t5.5~t1~t1~t1999-05-19~tibdkdld~r~n' 

//MessageBox("ls_detail",ls_detail)

ll_rec_count_pas = dw_cattle.ImportString(ls_detail)

wf_char_dddw()

//FOR ll_row = 1 TO ll_rec_count_pas 
//	dw_cattle.SetItem(ll_row, 'grade_dwn', dw_cattle.GetItemString(ll_row, 'grade'))
//NEXT

If ll_rec_count_pas > 0 Then 
	SetMicroHelp(String(ll_rec_count_pas) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True) 

//dw_cattle.SetRow (1)
//dw_cattle.SelectRow (1,True)
//dw_cattle.SetFocus()
//
dw_cattle.SelectRow(0, FALSE)
dw_cattle.ScrolltoRow(1)
dw_cattle.SelectRow(1, TRUE)
dw_cattle.SetFocus()



dw_cattle.ResetUpdate()
 
Return True

end function

public function boolean wf_char_dddw ();Long 					ll_rec_count,ll_row,ll_char,ll_child_char,ll_child_row,ll_find
String 				ls_temp 
DataWindowChild 	ldwc_char

dw_cattle.GetChild ( "char_key",  ldwc_char )

ll_rec_count =  dw_cattle.RowCount()
integer multiplier = 75
FOR ll_row = 1 TO ll_rec_count 
	ll_char = dw_cattle.GetItemNumber(ll_row, 'char_key') 
	
	//Replace with a find
	ll_find = ldwc_char.Find ( "char_key = " + String(ll_char), 1, ldwc_char.RowCount() )
	If ll_find > 0 Then
		ldwc_char.setitem(ll_find,'display','Y')
	End If
	
	ll_find = ldwc_char.Find ( "char_key = 0 " , 1, ldwc_char.RowCount() )
		If ll_find > 0 Then
		ldwc_char.RowsDiscard (ll_find, ll_find, Primary! )
	End If
	
NEXT
//dw_cattle.modify("grade.protect = 0")
//sort the child 
ldwc_char.Sort ( )

Return True

end function

public subroutine wf_set_dddw (long al_row, string as_char_type);DataWindowChild	ldwc_dddw

Long					ll_row,ll_find,ll_count
				
String				ls_temp, &
						ls_chr,&
						ls_char_num,&
						ls_find_str, &
						ls_filter, &
						ls_DataObject, &
						ls_desc, &
						ls_char_desc, &
						ls_char_code_col

String				ls_char_code []


Choose Case as_char_type
	Case 'PGM'
		ls_char_code_col = "program"
		ls_desc = "program_dwn"
	Case 'GRD'
		ls_char_code_col = "grade"
		ls_desc = "grade_dwn"
	Case 'COOL'
		ls_char_code_col = "code"
		ls_desc = "cool_code_dwn"
	Case Else
		return 
End Choose		


ids_chr.SetFilter("")
ls_filter = "char_name = '" + dw_cattle.GetItemString(al_row, "short_desc") + &
		"' and char_type = '" + as_char_type + "'" 
ids_chr.SetFilter(ls_filter)
ids_chr.Filter()

if ids_chr.rowcount() > 0 then
	if as_char_type = "PGM" Then
		ls_char_code = ids_chr.Object.char_num.primary
	else
		ls_char_code = ids_chr.Object.char_code.primary
	end if
	ls_temp = ""
	For ll_count = 1 to UpperBound(ls_char_code)
		ls_temp += "'" + String(ls_char_code[ll_count]) + "', "
	Next
	ls_temp = Left(ls_temp, Len(ls_temp) - 2)
end if

dw_cattle.GetChild(ls_desc, ldwc_dddw)

ldwc_dddw.SetFilter("")
ls_filter = "(" + ls_char_code_col + " IN (" + ls_temp + "))" 
ldwc_dddw.SetFilter(ls_filter)
ldwc_dddw.Filter()

end subroutine

public subroutine wf_changeItemStatus (string as_column_name, long as_row);
end subroutine

public subroutine wf_change_item_status (string as_column_name, long al_row);Choose Case as_column_name
	Case 'grade_dwn'
		dw_cattle.SetItemStatus(al_row, 0, Primary!, idw_status)
	Case 'program_dwn'
		dw_cattle.SetItemStatus(al_row, 0, Primary!, idw_status)
	Case 'cool_code_dwn'
		dw_cattle.SetItemStatus(al_row, 0, Primary!, idw_status)
end Choose
end subroutine

on w_pa_plant_characteristics.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_cattle=create dw_cattle
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_cattle
end on

on w_pa_plant_characteristics.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_cattle)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

end event

event open;call super::open;This.Title = 'PA/Plant Characteristics'

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'ellipsis'
		message.StringParm = ids_desc.object.datawindow.data
		//messagebox('ellipsis get data', string(message.stringparm))
End choose

end event

event ue_postopen;call super::ue_postopen;String				ls_char, &
						ls_grade,&
						ls_yields,&
						ls_program,&
						ls_desc,&
						ls_group_id, &
						ls_modify_auth, &
						ls_cool_code, ls_chr

DataWindowChild	ldwc_char,&
						ldwc_grade,&
						ldwc_grade_dwn,&
						ldwc_min_yield,&
						ldwc_program,&
						ldwc_program_dwn,&
						ldwc_max_yield,&
						ldwc_desc, &
						ldwc_cool_code,&
						ldwc_cool_code_dwn
						
Long					ll_rec_count,ll_row

This.PostEvent('ue_query')//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "catchar"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_cattle_characteristics_inq'

iu_rmt001 = Create u_rmt001
iu_pas203 = Create u_pas203
iu_ws_pas3 = Create u_ws_pas3
iu_ws_pas4	= Create u_ws_pas4


ids_tree = Create DataStore
ids_tree.DataObject = 'd_yields_tree'

istr_error_info.se_event_name = "ue_postopen"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

istr_error_info.se_procedure_name = "u_rmt001.nf_multi_dddw"
// Get the multi dddw
// Rev#01
 

/*If Not iu_rmt001.uf_rmtr03mr_inq_dddw(istr_error_info, ls_grade,ls_yields,ls_desc,ls_program,ls_cool_code, ls_chr)  Then
	MessageBox('DropDown Datawindows', 'There is a problem getting the multiple dddw items. Please call Applications.')
end If
*/
If Not iu_ws_pas4.NF_RMTR03NR(istr_error_info, ls_grade,ls_yields,ls_desc,ls_program,ls_cool_code, ls_chr)  Then
	MessageBox('DropDown Datawindows', 'There is a problem getting the multiple dddw items. Please call Applications.')
end If

// import grades
dw_cattle.GetChild("grade", ldwc_grade)
ldwc_grade.ImportString(ls_grade)
dw_cattle.GetChild("grade_dwn", ldwc_grade_dwn)
ldwc_grade_dwn.ImportString(ls_grade)

// import min yield grades
dw_cattle.GetChild("min_grade", ldwc_min_yield)
ldwc_min_yield.ImportString(ls_yields)

// import max yield grades
dw_cattle.GetChild("max_grade", ldwc_max_yield)
ldwc_max_yield.ImportString(ls_yields)

// these two no longer used
//// import destination
//dw_cattle.GetChild("destination", ldwc_destination)
//ldwc_destination.ImportString(ls_destination)
//// import sex
//dw_cattle.GetChild("sex", ldwc_sex)
//ldwc_sex.ImportString(ls_sex)

// import programs
dw_cattle.GetChild("program", ldwc_program)
ldwc_program.ImportString(ls_program)
dw_cattle.GetChild("program_dwn", ldwc_program_dwn)
ldwc_program_dwn.ImportString(ls_program)

//rev#02 added cool code
dw_cattle.GetChild("cool_code", ldwc_cool_code)
ldwc_cool_code.ImportString(ls_cool_code)
dw_cattle.GetChild("cool_code_dwn", ldwc_cool_code_dwn)
ldwc_cool_code_dwn.ImportString(ls_cool_code)

ids_chr =  Create DataStore
ids_chr.DataObject = "d_rmt_chr_dddw"
ids_chr.Reset()
ids_chr.ImportString(ls_chr)
ids_chr.Sort()

wf_retrieve()

end event

event ue_set_data;call super::ue_set_data;
Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'closechild'
		dw_cattle.SetColumn ( 'desc' )
		dw_cattle.SetFocus()
	Case 'selectrow'
		wf_import_desc(as_value)
		//messagebox('selectrow set data',string(as_value))
End Choose


end event

event close;call super::close;//If isvalid(iw_temp) Then
//	Close(iw_temp)
//End If
end event

event ue_revisions;call super::ue_revisions;/*****************************************************************
**   REVISION NUMBER: Rev#01
**   PROJECT NUMBER:  Support
**   DATE:				 January 2000            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Added Program, Removed Sex & Destination
**                    
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

event resize;call super::resize;//constant integer li_x		= 0
//constant integer li_y		= 99
//
integer li_x		= 30
integer li_y		= 129

 
//dw_cattle.width	= newwidth - (30 + li_x)
//dw_cattle.height	= newheight - (30 + li_y)

dw_cattle.width	= newwidth - (li_x)
dw_cattle.height	= newheight - (li_y)
end event

type dw_plant from u_plant within w_pa_plant_characteristics
integer width = 1503
integer height = 90
integer taborder = 20
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()
end event

type dw_cattle from u_base_dw_ext within w_pa_plant_characteristics
event ue_dwndropdown pbm_dwndropdown
integer y = 96
integer width = 2798
integer height = 1261
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_pa_plant_characteristics"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event ue_dwndropdown;u_string_functions	lu_string

IF dw_cattle.GetColumnName() = 'char_key' Then
	wf_char_dddw()
End IF

Return 0


end event

event clicked;call super::clicked;DataWindowChild ldwc_char
string colName 
colname = string(dwo.name)

CHOOSE CASE colName
	CASE 'bo_desc'
		//If /*dw_cattle.GetItemString ( row, 'protect') = 'N' and &*/
		If dw_cattle.GetItemString ( row, 'unprotect_char_key') = 'Y' Then
			wf_ellipsis()
		End IF
	case 'grade_dwn'
		wf_set_dddw(row, 'GRD')
		idw_status = this.GetItemStatus(row, 0, Primary!)
	case 'program_dwn'
		wf_set_dddw(row, 'PGM')
		idw_status = this.GetItemStatus(row, 0, Primary!)
	case 'cool_code_dwn'
		wf_set_dddw(row, 'COOL')
		idw_status = this.GetItemStatus(row, 0, Primary!)
END CHOOSE



end event

event constructor;call super::constructor;is_selection = '1'
end event

event itemchanged;call super::itemchanged;DataWindowChild 	ldwc_char
DataStore 			lds_Tmp
Long 					ll_count,ll_row,ll_find
String				ls_temp

CHOOSE CASE dwo.name
	CASE 'short_desc'
		ll_find = dw_cattle.Find ( "short_desc = '" + data + "'", 1, dw_cattle.RowCount() )
		If ll_find > 0 Then
			iw_frame.SetMicroHelp("This Short Description has already been used for another Characteristic")
			This.SelectText(1, 100)
			//dw_cattle.SetFocus()
			Return 1
		End If
		ll_find = ids_desc.Find ( "desc_child = '" + data + "'", 1, ids_desc.RowCount() )
		If ll_find > 0 Then
			//import data
			ids_desc.SelectRow ( ll_find,	True )
			lds_Tmp = Create DataStore
			lds_Tmp.DataObject = 'd_cattle_characteristics_child'
			lds_tmp.Object.Data = ids_desc.Object.Data.Selected
			ls_temp = lds_tmp.Object.Datawindow.data
			wf_import_desc(ls_temp)
		Else 
			IF ll_find = 0 Then
				iw_frame.SetMicroHelp("This Short Description has not been set up -- please try another")
				This.SelectText(1, 100)
				//dw_cattle.SetFocus()
				Return 1
			End If
		End If
	CASE 'char_key'
		dw_cattle.GetChild ( "char_key",  ldwc_char )
		If ldwc_char.GetItemString ( ldwc_char.getrow(), 'display') = 'Y'  Then
			//ldwc_char.ScrollPriorRow ( )
			ll_count = ldwc_char.RowCount ( )
			DO WHILE ll_count > ll_row 
				If ldwc_char.GetItemString ( ll_count, 'display') = 'Y'  Then
					ll_count --
				Else
					ldwc_char.ScrollToRow ( ll_count )
					dw_cattle.SetItem(row,'char_key',ldwc_char.GetitemNumber(ldwc_char.getrow(),'char_key') ) 
					Return 2
				End If	
			LOOP
		End If
END CHOOSE

Parent.Post wf_change_item_status(dwo.name, row)
Return 0
end event

event ue_keydown;call super::ue_keydown;If Keydown(KeyControl!) and Keydown(KeyDownArrow!) Then
	wf_ellipsis ()
End IF

end event

event rowfocuschanging;wf_remove_used_desc()


end event

event itemerror;call super::itemerror;Return 2
end event

