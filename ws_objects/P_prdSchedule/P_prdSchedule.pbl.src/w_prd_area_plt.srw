﻿$PBExportHeader$w_prd_area_plt.srw
forward
global type w_prd_area_plt from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_prd_area_plt
end type
type dw_seq_sect_dtl from u_base_dw_ext within w_prd_area_plt
end type
end forward

global type w_prd_area_plt from w_base_sheet_ext
integer width = 2377
integer height = 1340
string title = "Schedule Section "
long backcolor = 67108864
dw_header dw_header
dw_seq_sect_dtl dw_seq_sect_dtl
end type
global w_prd_area_plt w_prd_area_plt

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product	

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

u_pas201		iu_pas201

String		is_input, &
				is_debug, &
				is_name_string
	
DataWindowChild		idwc_sect_desc, idddw_child_status, dwc_temp


u_sect_functions		iu_sect_functions
u_ws_pas5				iu_ws_pas5
end variables

forward prototypes
public subroutine wf_delete ()
public function boolean wf_validate (long al_row)
public subroutine wf_set_filter (string as_ind)
public function boolean wf_addrow ()
public function boolean wf_deleterow ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_get_sect_desc ()
public function boolean wf_check_desc (long al_row)
end prototypes

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_validate (long al_row);Integer				li_temp

Long					ll_rtn, &
						ll_nbrrows, &
						ll_ModifiedRow
						
String				ls_searchstring, &
						ls_temp, &
						ls_update_flag, &
						ls_sched_type, &
						ls_sequence
											
						
IF dw_seq_sect_dtl.AcceptText() = -1 THEN Return( False )

ll_nbrrows = dw_seq_sect_dtl.RowCount()

ls_temp = dw_seq_sect_dtl.GetItemString(al_row, "name_code")
IF IsNull(ls_temp) Then 
	iw_frame.SetMicroHelp("Please choose a section description.")
	This.SetRedraw(False)
	dw_seq_sect_dtl.ScrollToRow(al_row)
	dw_seq_sect_dtl.SetColumn("name_code")
	dw_seq_sect_dtl.SetFocus()
	This.SetRedraw(True)
	Return False
End If

li_temp = dw_seq_sect_dtl.GetItemNumber(al_row, "sequence")
IF li_temp > 0 Then 
	//do nothing
Else
	iw_frame.SetMicroHelp("Please enter a sequence number.")
	dw_seq_sect_dtl.ScrollToRow(al_row)
	dw_seq_sect_dtl.SetColumn("sequence")
	dw_seq_sect_dtl.SetFocus()
	Return False
End If

If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

ls_Searchstring = "sequence = "
ls_Searchstring += String(dw_seq_sect_dtl.GetItemNumber(al_row, "sequence"))

CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_seq_sect_dtl.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_seq_sect_dtl.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_seq_sect_dtl.Find  &
			(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_seq_sect_dtl.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE

If ll_rtn > 0 Then
	iw_Frame.SetMicroHelp( "There are duplicate sequence numbers")
	dw_seq_sect_dtl.ScrollToRow(al_row)
	dw_seq_sect_dtl.SetColumn("sequence")
	dw_seq_sect_dtl.SetRow(al_row)
	dw_seq_sect_dtl.SelectRow(ll_rtn, True)
	dw_seq_sect_dtl.SelectRow(al_row, True)
	Return False
End if

Return True
end function

public subroutine wf_set_filter (string as_ind);String 	ls_filter


ls_filter = "available_ind = '" + as_ind + "'"
idwc_sect_desc.SetFilter(ls_filter)
idwc_sect_desc.Filter()


end subroutine

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row
				
String		ls_ind, &
				ls_filter

If dw_seq_sect_dtl.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_row = dw_seq_sect_dtl.InsertRow(0)	

//wf_set_filter('N')

dw_seq_sect_dtl.ScrollToRow(ll_row)
dw_seq_sect_dtl.SetItem(ll_row, "update_flag", 'A')
dw_seq_sect_dtl.SetItem(ll_row, "sequence", '0')
dw_seq_sect_dtl.SetColumn("sequence")
dw_seq_sect_dtl.SetFocus()
This.SetRedraw(True)

return true



end function

public function boolean wf_deleterow ();Boolean		lb_ret

Long			ll_row

String		ls_ind, &
				ls_filter, &
				ls_name_code

This.SetRedraw( False )
ll_row = dw_seq_sect_dtl.GetRow()

If ll_row < 1 Then
	Return True
End If

//if dw_seq_sect_dtl.GetItemString(ll_row, "update_flag") = 'A' Then
//	ls_name_code = dw_seq_sect_dtl.GetItemString(ll_row, "name_code")
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code,'Y') 
//End if

dw_seq_sect_dtl.SetItem(ll_row, "update_flag", 'D')

//wf_set_filter('N')

dw_seq_sect_dtl.SetFocus()
lb_ret = super::wf_deleterow()

dw_seq_sect_dtl.SelectRow(0,False)

This.SetRedraw( True )
return lb_ret
end function

public function boolean wf_retrieve ();Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_output_values, ls_temp, &
			ls_name_code
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_prd_area_plt_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")

wf_get_sect_desc()

ls_input = This.dw_header.GetItemString(1, 'plant_code') + '~t' + &
				This.dw_header.GetItemString(1, 'name_code') + '~t'

			
is_input = ls_input

//li_ret = iu_pas201.nf_pasp53cr_inq_sched_sections(istr_error_info, &
//									is_input, &
//									ls_output_values) 
						
li_ret = iu_ws_pas5.nf_pasp53gr(istr_error_info, &
									is_input, &
									ls_output_values) 						

This.dw_seq_sect_dtl.Reset()

If li_ret = 0 Then
	This.dw_seq_sect_dtl.ImportString(trim(ls_output_values))
End If

ll_value = dw_seq_sect_dtl.RowCount()
If ll_value < 0 Then ll_value = 0

li_row_count = dw_seq_sect_dtl.RowCount()
//For li_counter = 1 to li_Row_count
//	ls_name_code = dw_Seq_sect_dtl.GetItemString(li_counter, "name_code")
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code, 'N') 
//Next	
	
dw_seq_sect_dtl.ResetUpdate()

IF ll_value > 0 THEN
	dw_seq_sect_dtl.SetFocus()
	dw_seq_sect_dtl.ScrollToRow(1)
	dw_seq_sect_dtl.SetColumn( "sequence" )
	dw_seq_sect_dtl.TriggerEvent("RowFocusChanged")
END IF

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )

dw_seq_sect_dtl.ResetUpdate()

Return True

end function

public function boolean wf_update ();integer			li_Counter

					
long				ll_Row, &
					ll_RowCount, &
					ll_NbrRows, &
					ll_temp

string			ls_Update_string, &
					ls_header_string, &
					ls_output_string, &
					ls_ind, &
					ls_filter, & 
					ls_name_code
					

IF dw_seq_sect_dtl.AcceptText() = -1 THEN Return( False )
ll_temp = dw_seq_sect_dtl.DeletedCount()
IF dw_seq_sect_dtl.ModifiedCount() + dw_seq_sect_dtl.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_seq_sect_dtl.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_seq_sect_dtl.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then
//			dw_seq_sect_dtl.SetReDraw(False)
//			wf_set_filter('N')
//			dw_seq_sect_dtl.SetReDraw(True)
			Return False
		End If
		If Not wf_check_desc(ll_row) Then
			Return False
		End If
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_header_string = is_input
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_seq_sect_dtl)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp54cr_upd_sched_sections"
istr_error_info.se_message = Space(71)

//If not iu_pas201.nf_pasp54cr_upd_sched_sections(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then  Return False
If not iu_ws_pas5.nf_pasp54gr(istr_error_info, ls_Update_string,ls_output_string, ls_header_string) Then  Return False

dw_seq_sect_dtl.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = idwc_sect_desc.RowCount()
For li_counter = 1 to ll_RowCount
	idwc_sect_desc.SetItem(li_Counter, 'available_ind', 'Y')
Next	

ll_RowCount = dw_seq_sect_dtl.RowCount()	
For li_Counter = 1 to ll_RowCount
//	ls_name_code = dw_Seq_sect_dtl.GetItemString(li_counter, "name_code")
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code, 'N') 
	dw_seq_sect_dtl.SetItem(li_Counter, 'update_flag', ' ')
Next

//wf_set_filter('N')
dw_seq_sect_dtl.ResetUpdate()
dw_seq_sect_dtl.Sort()
dw_seq_sect_dtl.SetFocus()
dw_seq_sect_dtl.SetReDraw(True)

Return( True )
end function

public function boolean wf_get_sect_desc ();Integer	li_ret, &
			li_row_Count, &
			li_rtn, &
			li_counter
			
String	ls_input, &
			ls_output_values, &
			ls_temp, &
			ls_name_string, &
			ls_ind, &
			ls_filter, &
			ls_name_code
			
Long		ll_value, &
			ll_rtn
	
u_string_functions u_string

DataWindowChild	ldwc_temp


idwc_sect_desc.Reset()

ls_input = 'SECTION~r~n'

ls_output_values = iu_sect_functions.uf_get_names(ls_input)

li_row_count = idwc_sect_desc.ImportString(ls_output_values)
idwc_sect_desc.SetSort("type_descr")
idwc_sect_desc.Sort()
//For li_counter = 1 to li_Row_count
//	idwc_sect_desc.SetItem(li_Counter, 'available_ind', 'Y')
//Next	

Return True

end function

public function boolean wf_check_desc (long al_row);Integer				li_counter

Long					ll_rtn, &
						ll_nbrrows

String				ls_SearchString, &
						ls_name_code

dw_seq_sect_dtl.AcceptText()
ls_name_code = dw_seq_sect_dtl.GetItemString(al_row, 'name_code')
ll_nbrrows = dw_seq_sect_dtl.RowCount()

if ll_nbrrows = 1 Then Return True

For li_counter = 1 to ll_nbrrows
	dw_seq_sect_dtl.SelectRow(li_counter, False)
Next	
	
ls_SearchString = 	"name_code = '"+ ls_name_code + "'"
	
CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_seq_sect_dtl.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_seq_sect_dtl.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_seq_sect_dtl.Find  &
				(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_seq_sect_dtl.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE
		
If ll_rtn > 0 Then
	iw_Frame.SetMicroHelp( "Section Description has already been" + &
  						" assigned a sequence number.")
	dw_seq_sect_dtl.SetRedraw(False)
	dw_seq_sect_dtl.ScrollToRow(al_row)
	dw_seq_sect_dtl.SetRow(ll_rtn)
	dw_seq_sect_dtl.SelectRow(ll_rtn, True)
	dw_seq_sect_dtl.SelectRow(al_row, True)
	dw_seq_sect_dtl.SetRedraw(True)
	Return False
End If

Return True
end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)
If Isvalid(iu_ws_pas5) then destroy(iu_ws_pas5)



end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;

wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

wf_get_sect_desc()
end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_seq_sect_dtl.InsertRow(0)


end event

event ue_postopen;call super::ue_postopen;Environment		le_env
iu_ws_pas5 	= create u_ws_pas5

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if



istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Schedule Section By Plant"
istr_error_info.se_user_id = sqlca.userid

idwc_sect_desc.Reset()
dw_seq_sect_dtl.GetChild("name_code", idwc_sect_desc)

//idddw_child_status.SetTransObject(SQLCA)
//idddw_child_status.Retrieve("PPSRPTCD")
//idddw_child_status.SetSort("type_code")
//idddw_child_status.Sort()

dw_seq_sect_dtl.GetChild('sched_rep', dwc_temp)
dwc_temp.SetTransObject(SQLCA)
dwc_temp.Retrieve("PPSRPTCD")

This.PostEvent("ue_query")

end event

on w_prd_area_plt.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_seq_sect_dtl=create dw_seq_sect_dtl
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_seq_sect_dtl
end on

on w_prd_area_plt.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_seq_sect_dtl)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Name Type'
		Message.StringParm = dw_header.GetItemString(1, 'name_type')
	Case 'Name Code'
		Message.StringParm = dw_header.GetItemString(1, 'name_code')
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Name Type'
		dw_header.SetItem(1, 'name_type', as_value)
	Case 'Name Code'
		dw_header.SetItem(1, 'name_code', as_value)
	Case 'Type Descr'
		dw_header.SetItem(1, 'type_descr', as_value)
End Choose


end event

event resize;call super::resize;integer li_x		
integer li_y	
integer ly_height = 115

li_x = (dw_seq_sect_dtl.x * 2) + 30 
//li_y = dw_seq_sect_dtl.y + 115
//

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > ly_height Then
                ly_height = il_BorderPaddingHeight
End If

li_y = dw_seq_sect_dtl.y + ly_height

if width > li_x Then
	dw_seq_sect_dtl.width	= width - li_x
end if

if height > li_y then
	dw_seq_sect_dtl.height	= height - li_y
end if
end event

type dw_header from u_base_dw_ext within w_prd_area_plt
integer y = 4
integer width = 1371
integer height = 172
integer taborder = 0
boolean enabled = false
string dataobject = "d_prd_area_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False

end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_seq_sect_dtl from u_base_dw_ext within w_prd_area_plt
integer y = 256
integer width = 2249
integer height = 772
integer taborder = 20
string dataobject = "d_sched_seq_sect"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;Long		ll_rtn
String	ls_temp, &
			ls_ColumnName, &
			ls_avail_ind, &
			ls_SearchString, &
			ls_ind, &
			ls_filter
			
Boolean 	lb_color

//dw_seq_sect_dtl.SetReDraw(True)

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

//dw_seq_sect_dtl.SetReDraw(False)
//If ls_ColumnName = "name_code" Then
//	//	wf_set_filter('Y')
////Else 
////	wf_set_filter('N')
//End If
//
//dw_seq_sect_dtl.SetReDraw(True)





end event

event constructor;call super::constructor;This.ib_updateable = True

end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

long			ll_source_row, &
				ll_RowCount, &
				ll_rtn

String		ls_ColumnName, &
				ls_SearchString, &
				ls_avail_ind, &
				ls_name_code								

nvuo_pa_business_rules	u_rule


ll_source_row	= GetRow()
il_ChangedRow = 0

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

If ls_ColumnName = "name_code" Then
	If Not wf_check_desc(row) Then
//		iw_Frame.SetMicroHelp( "Section Description has already been" + &
//						  " assigned a sequence number.")
//		  dw_seq_sect_dtl.SetRedraw(False)
//		dw_seq_sect_dtl.ScrollToRow(row)
//		dw_seq_sect_dtl.SetRow(row)
//		dw_seq_sect_dtl.SelectRow(row, True)
//		dw_seq_sect_dtl.SetRedraw(True)
		Return 
	End If
//	This.selecttext(1,100)
//	ls_name_code = String(data)
//	idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code,'N') 
//	If This.GetItemString(row, "update_flag") = 'A' Then
//		ls_name_code = This.GetItemString(row, "name_code")
//		//wf_set_filter('N')
//		//idwc_sect_desc = iu_sect_functions.uf_set_visible_ind(idwc_sect_desc, ls_name_code,'Y') 
//		//wf_set_filter('Y')
//	End If
//	dw_seq_sect_dtl.SetReDraw(True)
End If
	
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

//wf_set_filter('Y')


return 0
end event

event itemerror;call super::itemerror;return (1)
end event

event itemfocuschanged;call super::itemfocuschanged;dw_seq_sect_dtl.SetReDraw(True)
end event

