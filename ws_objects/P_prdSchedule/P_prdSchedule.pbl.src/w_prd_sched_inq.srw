﻿$PBExportHeader$w_prd_sched_inq.srw
forward
global type w_prd_sched_inq from w_base_response
end type
type dw_plant from u_plant within w_prd_sched_inq
end type
type dw_area_sect from u_sched_area_sect within w_prd_sched_inq
end type
type dw_sched_date from u_sched_date within w_prd_sched_inq
end type
type dw_shift from u_shift within w_prd_sched_inq
end type
type dw_row_choice from u_row_choice within w_prd_sched_inq
end type
type dw_dtl_sum_choice from u_base_dw_ext within w_prd_sched_inq
end type
end forward

global type w_prd_sched_inq from w_base_response
integer y = 485
integer width = 1602
integer height = 1270
string title = "Update Schedule Inquire"
long backcolor = 67108864
event ue_query pbm_custom70
dw_plant dw_plant
dw_area_sect dw_area_sect
dw_sched_date dw_sched_date
dw_shift dw_shift
dw_row_choice dw_row_choice
dw_dtl_sum_choice dw_dtl_sum_choice
end type
global w_prd_sched_inq w_prd_sched_inq

type variables
Boolean		ib_valid_return, &
				ib_NewPlant
				
w_base_sheet	iw_parent

u_pas201		iu_pas201

s_error		istr_error_info

DataWindowChild	idwc_temp

u_sect_functions	iu_sect_functions
end variables

on w_prd_sched_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_area_sect=create dw_area_sect
this.dw_sched_date=create dw_sched_date
this.dw_shift=create dw_shift
this.dw_row_choice=create dw_row_choice
this.dw_dtl_sum_choice=create dw_dtl_sum_choice
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_area_sect
this.Control[iCurrent+3]=this.dw_sched_date
this.Control[iCurrent+4]=this.dw_shift
this.Control[iCurrent+5]=this.dw_row_choice
this.Control[iCurrent+6]=this.dw_dtl_sum_choice
end on

on w_prd_sched_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_area_sect)
destroy(this.dw_sched_date)
destroy(this.dw_shift)
destroy(this.dw_row_choice)
destroy(this.dw_dtl_sum_choice)
end on

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

//This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
//			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

Window      lw_parent 

lw_parent = this.parentwindow()

This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
	This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

If dw_area_sect.RowCount() = 0 Then
	dw_area_sect.InsertRow(0)
End If

If dw_shift.RowCount() = 0 Then
	dw_shift.InsertRow(0)
End If

If dw_sched_date.RowCount() = 0 Then
	dw_sched_date.InsertRow(0)
End If

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date		ldt_temp

String	ls_temp, &
			ls_SearchString
Long 		ll_rtn
Integer	li_rtn, &
			li_temp

If dw_plant.AcceptText() = -1 or &
	dw_area_sect .AcceptText() = -1 or &
	dw_sched_date .AcceptText() = -1 Then
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_area_sect.GetItemString(1, "area_name_code")) Then
	iw_frame.SetMicroHelp("Area is a required field")
	dw_area_sect.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_area_sect.GetItemString(1, "sect_name_code")) Then
	iw_frame.SetMicroHelp("Section is a required field")
	dw_area_sect.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(String(dw_sched_date.GetItemDate(1, "sched_date"))) Then
	iw_frame.SetMicroHelp("Date is a required field")
	dw_sched_date.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_shift.GetItemString(1, "shift")) Then
	iw_frame.SetMicroHelp("Shift is a required field")
	dw_shift.SetFocus()
	return
End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant Desc', ls_temp)

ls_temp = dw_area_sect.uf_get_area_code()
iw_parent.Event ue_Set_Data('Area Name Code', ls_temp)

ls_temp = dw_area_sect.uf_get_area_descr()
iw_parent.Event ue_Set_Data('Area Descr', ls_temp)

ls_temp = dw_area_sect.uf_get_sect_code()
iw_parent.Event ue_Set_Data('Sect Name Code', ls_temp)

ls_temp = dw_area_sect.uf_get_sect_descr()
iw_parent.Event ue_Set_Data('Sect Descr', ls_temp)

ldt_temp = dw_sched_date.GetItemDate(1, "sched_date")

iw_parent.Event ue_set_data('sched_date', &
		String(ldt_temp, 'yyyy-mm-dd'))

ls_temp = dw_shift.uf_get_shift( )
iw_parent.Event ue_Set_Data('shift', ls_temp)

ls_temp = dw_row_choice.GetItemString(1, "choice")
iw_parent.Event ue_Set_Data('Row Option', ls_temp)

ls_temp = dw_dtl_sum_choice.GetItemString(1, "choice")
iw_parent.Event ue_Set_Data('Detail Sum Option', ls_temp)

ib_valid_return = True
Close(This)
end event

event ue_postopen;call super::ue_postopen;Int							li_pos, &
								li_ret
								
String						ls_sect_name, &
								ls_temp, &
								ls_shift, &
								ls_date, &
								ls_option

u_string_functions		lu_string

Environment					le_env


iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
	dw_area_sect.SetItem(1,"plant_code", ls_temp)
	dw_area_sect.uf_get_plt_codes(ls_temp)
	dw_area_sect.AcceptText()
End If

iw_parent.Event ue_Get_Data('Area Name Code')
ls_sect_name = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_sect_name) Then 
	dw_area_sect.SetItem(1,"area_name_code",ls_sect_name)
End If

iw_parent.Event ue_Get_Data('Sect Name Code')
ls_sect_name = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_sect_name) Then 
	dw_area_sect.SetItem(1,"sect_name_code",ls_sect_name)
End If

iw_parent.Event ue_get_data('sched_date')
ls_date = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_date) Then dw_sched_date.uf_set_sched_date(date(ls_date))

iw_parent.Event ue_Get_Data('shift')
ls_shift = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_shift) Then 
	dw_shift.SetItem(1,"shift",ls_shift)
End If

iw_parent.Event ue_Get_Data('Row Option')
ls_option = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_option) Then 
	dw_row_choice.SetItem(1,"choice",ls_option)
End If

iw_parent.Event ue_Get_Data('Detail Sum Option')
ls_option = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_option) Then 
	dw_dtl_sum_choice.SetItem(1,"choice",ls_option)
End If

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Update Schedule Inquire"
istr_error_info.se_user_id = sqlca.userid




end event

type cb_base_help from w_base_response`cb_base_help within w_prd_sched_inq
boolean visible = false
integer x = 1401
integer y = 867
integer height = 112
integer taborder = 0
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_prd_sched_inq
integer x = 633
integer y = 1021
integer taborder = 80
end type

type cb_base_ok from w_base_response`cb_base_ok within w_prd_sched_inq
integer x = 183
integer y = 1018
integer taborder = 70
end type

type dw_plant from u_plant within w_prd_sched_inq
integer x = 80
integer y = 48
integer width = 1503
integer height = 90
integer taborder = 10
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;String		ls_ColName

ls_ColName = GetColumnName()

CHOOSE CASE ls_ColName
	CASE "location_code"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid location")
			This.selecttext(1,100)
			return 1
		Else
			ib_NewPlant = True
			dw_area_sect.Reset()
			dw_area_sect.InsertRow(0)
		End If
End Choose
end event

event losefocus;call super::losefocus;String	ls_PlantCode

ls_PlantCode = This.GetItemString(1,"location_code")

//set the hidden plant code on the next window to 
//be used to retrieve the area name codes.  
if dw_area_sect.GetItemString(1,"plant_code") = ls_PlantCode Then
	//do nothing
Else
	dw_area_sect.SetItem(1,"plant_code", ls_PlantCode)
	dw_area_sect.AcceptText()

//set this to true for the clicked event on the area window
	ib_NewPlant = True
End If
end event

type dw_area_sect from u_sched_area_sect within w_prd_sched_inq
integer y = 128
integer width = 1020
integer height = 170
integer taborder = 20
boolean bringtotop = true
end type

event clicked;call super::clicked;String		ls_ColName, &
				ls_plant_code

ls_ColName = GetColumnName()
dw_plant.AcceptText()

//ib_NewPlant is set to true when the plant window loses focus
If ib_NewPlant Then
	CHOOSE CASE ls_ColName
		CASE "area_name_code"
			ls_plant_code = dw_plant.GetItemString(1,"location_code")
			If isnull(ls_plant_code) Then
				iw_frame.SetMicroHelp("This is not a valid plant")
				return 1
			Else
				This.SetRedraw( False )
				super::uf_get_plt_codes(ls_plant_code)
				This.AcceptText()
				This.SetRedraw( True )
			End If
	End Choose
	ib_NewPlant = False
End If
return 0

end event

event itemchanged;call super::itemchanged;String		ls_ColName

ls_ColName = GetColumnName()
dw_plant.AcceptText()

CHOOSE CASE ls_ColName
	CASE "area_name_code"
		dw_area_sect.SetItem(1,"sect_name_code", '')
END CHOOSE
end event

type dw_sched_date from u_sched_date within w_prd_sched_inq
integer x = 22
integer y = 298
integer width = 622
integer height = 93
integer taborder = 30
boolean bringtotop = true
end type

type dw_shift from u_shift within w_prd_sched_inq
integer x = 95
integer y = 387
integer width = 333
integer height = 74
integer taborder = 40
boolean bringtotop = true
end type

type dw_row_choice from u_row_choice within w_prd_sched_inq
integer x = 146
integer y = 742
integer width = 666
integer taborder = 60
boolean bringtotop = true
end type

type dw_dtl_sum_choice from u_base_dw_ext within w_prd_sched_inq
integer x = 146
integer y = 470
integer width = 666
integer height = 224
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_dtl_sum_choice"
boolean border = false
end type

event constructor;call super::constructor;if this.rowcount() = 0 then
	this.insertrow(0)
end if
end event

