﻿$PBExportHeader$u_source_begin_end_date.sru
forward
global type u_source_begin_end_date from u_base_dw_ext
end type
end forward

global type u_source_begin_end_date from u_base_dw_ext
integer width = 654
integer height = 172
string dataobject = "d_source_begin_end_date"
boolean border = false
end type
global u_source_begin_end_date u_source_begin_end_date

forward prototypes
public function string uf_getbegindate ()
public function string uf_getenddate ()
public function boolean uf_setbegindate (date ad_date)
public function boolean uf_setenddate (date ad_date)
public subroutine uf_enable (boolean ab_enable)
end prototypes

public function string uf_getbegindate ();If This.AcceptText() = -1 Then return ''

Return String(GetItemDate( 1, "Begin_Date" ), 'yyyy-mm-dd')


end function

public function string uf_getenddate ();If This.AcceptText() = -1 Then return ''

Return String(GetItemDate( 1, "End_Date" ), 'yyyy-mm-dd')



end function

public function boolean uf_setbegindate (date ad_date);integer		li_rc

li_rc	= SetItem( 1, "begin_date", ad_date )

IF li_rc = 1 THEN
	Return( True )
ELSE
	Return( False )
END IF

end function

public function boolean uf_setenddate (date ad_date);integer		li_rc

li_rc	= SetItem( 1, "end_date", ad_date )

IF li_rc = 1 THEN
	Return( True )
ELSE
	Return( False )
END IF

end function

public subroutine uf_enable (boolean ab_enable);//16777215, 67108864

ib_Updateable = ab_enable 

If ab_enable Then
	Modify("begin_date.Protect='1' begin_date.Background.Color='16777215'")
	Modify("end_date.Protect='1' end_date.Background.Color='16777215'")
Else
	Modify("begin_date.Protect='0' begin_date.Background.Color='67108864'")
	Modify("end_date.Protect='0' end_date.Background.Color='67108864'")
End If

return
end subroutine

event itemchanged;call super::itemchanged;date		ld_date

string		ls_GetText, &
				ls_ColumnName

ls_GetText		= GetText()
ls_ColumnName	= GetColumnName()

CHOOSE CASE ls_ColumnName 
	CASE "begin_date"
		IF Date( ls_GetText ) < Today() THEN
			MessageBox( "Begin Date", "The Begin Date cannot be less than today." )
			return 2
		END IF
	CASE "end_date"
		IF Date( ls_GetText ) < GetItemDate( GetRow(), "Begin_Date" ) THEN
			MessageBox( "End Date", "The End Date cannot be less than the Begin Date." )
			return 2
		END IF
END CHOOSE


end event

on u_source_begin_end_date.create
end on

on u_source_begin_end_date.destroy
end on

