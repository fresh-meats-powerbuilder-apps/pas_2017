﻿$PBExportHeader$w_source_sequence_inq.srw
$PBExportComments$Source seq # inquire
forward
global type w_source_sequence_inq from w_base_response_ext
end type
type dw_effective_date from u_effective_date within w_source_sequence_inq
end type
type dw_plant from u_plant within w_source_sequence_inq
end type
end forward

global type w_source_sequence_inq from w_base_response_ext
integer width = 1723
integer height = 464
long backcolor = 67108864
dw_effective_date dw_effective_date
dw_plant dw_plant
end type
global w_source_sequence_inq w_source_sequence_inq

on w_source_sequence_inq.create
int iCurrent
call super::create
this.dw_effective_date=create dw_effective_date
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_effective_date
this.Control[iCurrent+2]=this.dw_plant
end on

on w_source_sequence_inq.destroy
call super::destroy
destroy(this.dw_effective_date)
destroy(this.dw_plant)
end on

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
 				ls_plant_desc, &
				ls_date
				
Int			li_pa_range				

u_string_functions		lu_strings


If dw_plant.AcceptText() = -1 &
	or dw_effective_date.AcceptText() = -1  Then return 
	
ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End if

ls_date = string(dw_effective_date.uf_get_effective_date())
	
iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('effective_date',ls_date)

ib_ok_to_close = True

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('effective_date')
dw_effective_date.uf_set_effective_date(date(Message.StringParm))

dw_effective_date.uf_initilize('Production Date:','PA')





end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_source_sequence_inq
integer x = 1147
integer y = 252
integer taborder = 4
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_source_sequence_inq
integer x = 859
integer y = 252
integer taborder = 3
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_source_sequence_inq
integer x = 571
integer y = 252
integer taborder = 2
end type

type dw_effective_date from u_effective_date within w_source_sequence_inq
integer x = 27
integer y = 104
boolean bringtotop = true
end type

type dw_plant from u_plant within w_source_sequence_inq
integer x = 247
integer y = 12
integer taborder = 5
boolean bringtotop = true
end type

