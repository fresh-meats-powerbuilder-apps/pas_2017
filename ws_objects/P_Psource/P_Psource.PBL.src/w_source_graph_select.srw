﻿$PBExportHeader$w_source_graph_select.srw
forward
global type w_source_graph_select from w_base_response_ext
end type
type lb_shift from listbox within w_source_graph_select
end type
type st_shift from statictext within w_source_graph_select
end type
type st_4 from statictext within w_source_graph_select
end type
type st_5 from statictext within w_source_graph_select
end type
type em_quantity from editmask within w_source_graph_select
end type
type dw_source from datawindow within w_source_graph_select
end type
type dw_graph from u_base_dw_ext within w_source_graph_select
end type
type dw_chars from u_base_dw_ext within w_source_graph_select
end type
end forward

global type w_source_graph_select from w_base_response_ext
int X=133
int Y=249
int Width=2149
int Height=817
boolean TitleBar=true
string Title="Source Graph Selection"
long BackColor=12632256
lb_shift lb_shift
st_shift st_shift
st_4 st_4
st_5 st_5
em_quantity em_quantity
dw_source dw_source
dw_graph dw_graph
dw_chars dw_chars
end type
global w_source_graph_select w_source_graph_select

event ue_postopen;call super::ue_postopen;Decimal	ldc_estimated, &
			ldc_estimated1, &
			ldc_estimated2, &
			ldc_actual, &
			ldc_actual1, &
			ldc_actual2

Int		li_Counter

Long	 	ll_RowCount, &
			ll_Row

String	ls_FindString, &
			ls_CharKey



SetPointer(HourGlass!)
ll_RowCount = dw_source.RowCount()
If ll_RowCount < 1 Then return

iw_frame.SetMicroHelp("Finding unique Characteristics ...")
ls_CharKey = String(dw_source.GetItemNumber(1, 'char_key'))
dw_chars.ImportString(ls_CharKey + '~t' + dw_source.GetItemString(1, 'characteristics'))

ls_FindString = "char_key <> " + ls_CharKey 

Do
	ll_Row = dw_source.Find(ls_FindString, ll_Row + 1, ll_RowCount)
	If ll_Row < 1 Then Exit
	ls_CharKey = String(dw_source.GetItemNumber(ll_Row, 'char_key'))
	dw_chars.ImportString(ls_CharKey + '~t' + &
				dw_source.GetItemString(ll_Row, 'characteristics'))
	ls_FindString += " and char_key <> " + ls_CharKey
	
Loop while ll_Row > 0 And ll_Row < ll_RowCount

// Calculate Percentages
dw_source.GroupCalc()

iw_frame.SetMicroHelp("Calculating Percentages ...")

For li_Counter = 1 to ll_RowCount
 	ldc_estimated1 = dw_source.GetItemNumber(li_Counter, 'estimated_1')
	ldc_estimated2 = dw_source.GetItemNumber(li_Counter, 'estimated_2')

 	If ldc_estimated1 = 0 Or IsNull(ldc_Estimated1) Then
		ldc_estimated = 0
	Else
		ldc_estimated = ldc_estimated2 / ldc_estimated1
	End if
	dw_source.SetItem(li_Counter, 'estimated_pct', ldc_estimated)

	ldc_actual1 = dw_source.GetItemNumber(li_Counter, 'actual_1')
	ldc_actual2 = dw_source.GetItemNumber(li_Counter, 'actual_2')

 	If ldc_actual1 = 0 Or IsNull(ldc_actual1) Then
		ldc_actual = 0
	Else
		ldc_actual = ldc_actual2 / ldc_actual1
	End if
	dw_source.SetItem(li_Counter, 'actual_pct', ldc_actual)
Next

iw_Frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
end event

on open;call w_base_response_ext::open;DataWindow	ldw_arg

If TypeOf(Message.PowerObjectParm) = DataWindow! Then
	ldw_arg = Message.PowerObjectParm
	dw_source.ImportString(ldw_arg.Describe("DataWindow.Data"))
Else
	Close(This)
End if


end on

on w_source_graph_select.create
int iCurrent
call w_base_response_ext::create
this.lb_shift=create lb_shift
this.st_shift=create st_shift
this.st_4=create st_4
this.st_5=create st_5
this.em_quantity=create em_quantity
this.dw_source=create dw_source
this.dw_graph=create dw_graph
this.dw_chars=create dw_chars
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=lb_shift
this.Control[iCurrent+2]=st_shift
this.Control[iCurrent+3]=st_4
this.Control[iCurrent+4]=st_5
this.Control[iCurrent+5]=em_quantity
this.Control[iCurrent+6]=dw_source
this.Control[iCurrent+7]=dw_graph
this.Control[iCurrent+8]=dw_chars
end on

on w_source_graph_select.destroy
call w_base_response_ext::destroy
destroy(this.lb_shift)
destroy(this.st_shift)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.em_quantity)
destroy(this.dw_source)
destroy(this.dw_graph)
destroy(this.dw_chars)
end on

event doubleclicked;call super::doubleclicked;If KeyDown(KeyT!) And KeyDown(KeyJ!) And KeyDown(KeyB!) Then
	dw_source.Show()
End if
end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;Int		li_Counter

Long		ll_Row, &
			ll_RowCount

String	ls_Shift, &
			ls_GraphName, &
			ls_FilterQty, &
			ls_describe

Window	lw_Graph


dw_Source.SetFilter('')
dw_Source.Filter()


// Filter out the correct shift
ls_Shift = lb_Shift.SelectedItem()
If Len(Trim(ls_Shift)) = 1 Then
	If dw_Source.SetFilter("shift = '" + ls_Shift + "'") = -1 Then
		MessageBox("SetFilter", "shift = '" + ls_Shift + "'")
	End if
	dw_Source.Filter()
	dw_Source.RowsDiscard(1, dw_Source.FilteredCount(), Filter!)
End if


// Filter Out the correct Characteristics
// If none are selected, then don't filter out any
ll_RowCount = dw_chars.RowCount()
ll_Row = dw_chars.GetSelectedRow(0)

If ll_Row > 0 Then
	// at least one is selected, now filter out all that aren't
	For li_Counter = 1 to ll_RowCount
		If Not dw_chars.IsSelected(li_Counter) Then
			If dw_Source.SetFilter("char_key <> " + dw_chars.GetItemString(li_Counter, &
								'char_key')) = -1 Then
				MessageBox("SetFilter", "char_key <> " + dw_chars.GetItemString(li_Counter, &
 								'char_key'))
			End if
			dw_Source.Filter()
			dw_Source.RowsDiscard(1, dw_Source.FilteredCount(), Filter!)
		End if
	Next
End if

ll_Row = dw_graph.GetSelectedRow(0)
If ll_Row < 1 Then
	iw_Frame.SetMicroHelp("Please select a graph to display")
	dw_graph.SetFocus()
End if

ls_GraphName = dw_graph.GetItemString(ll_Row, 'graph_name')

ls_FilterQty = em_quantity.Text
If Not IsNumber(ls_FilterQty) Or iw_frame.iu_string.nf_IsEmpty(ls_FilterQty) Then ls_FilterQty = '0'
If ls_GraphName = 'd_source_graph_actual' Then
	If dw_Source.SetFilter("actual_pct >= " + ls_FilterQty + "/100") = -1 Then
		MessageBox("SetFilter", "actual_pct >= " + ls_FilterQty + "/100")
	End if
	dw_Source.Filter()
Else
	If dw_Source.SetFilter("estimated_pct >= " + ls_FilterQty + "/100") = -1 Then
		MessageBox("SetFilter", "estimated_pct >= " + ls_FilterQty + "/100")
	End if
	dw_Source.Filter()
End if
dw_Source.RowsDiscard(1, dw_Source.FilteredCount(), Filter!)

//ls_Describe = dw_source.Object.DataWindow.Data

If dw_source.RowCount() < 1 Then
	MessageBox("Source Graph","There are no records matching the criteria.")
	return
End If

Message.StringParm = ls_GraphName
Message.PowerObjectParm = dw_source
OpenSheet(lw_Graph, 'w_graph_ext', iw_Frame, 0, Layered!)

Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_source_graph_select
int X=1797
int Y=333
int TabOrder=90
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_source_graph_select
int X=1797
int Y=209
int TabOrder=80
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_source_graph_select
int X=1797
int Y=85
int TabOrder=70
end type

type lb_shift from listbox within w_source_graph_select
int X=1450
int Y=85
int Width=289
int Height=181
int TabOrder=50
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean Sorted=false
long TextColor=33554432
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
int Accelerator=115
string Item[]={"All Shifts",&
"A",&
"B"}
end type

on constructor;This.SelectItem(1)
end on

type st_shift from statictext within w_source_graph_select
int X=1454
int Y=13
int Width=247
int Height=73
boolean Enabled=false
string Text="Shift"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_source_graph_select
int X=238
int Y=581
int Width=787
int Height=73
boolean Enabled=false
string Text="Filter Sources with less than "
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_source_graph_select
int X=1317
int Y=581
int Width=124
int Height=73
boolean Enabled=false
string Text="% "
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=33554432
long BackColor=12632256
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type em_quantity from editmask within w_source_graph_select
int X=1047
int Y=573
int Width=247
int Height=93
int TabOrder=60
Alignment Alignment=Right!
BorderStyle BorderStyle=StyleLowered!
string Mask="###,##0"
boolean Spin=true
double Increment=1
string MinMax="0~~100"
long TextColor=33554432
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_source from datawindow within w_source_graph_select
int X=1454
int Y=333
int Width=650
int Height=361
int TabOrder=80
boolean Visible=false
string DataObject="d_source_graph_data"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

on clicked;MessageBox("Chars", This.GetItemString(1, 'characteristics'))
end on

type dw_graph from u_base_dw_ext within w_source_graph_select
int X=10
int Y=13
int Width=828
int Height=529
int TabOrder=30
string DataObject="d_graph_select"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on constructor;call u_base_dw_ext::constructor;is_selection = '1'

This.SelectRow(1, True)
end on

type dw_chars from u_base_dw_ext within w_source_graph_select
int X=855
int Y=9
int Width=567
int Height=537
int TabOrder=40
string DataObject="d_source_graph_chars"
BorderStyle BorderStyle=StyleLowered!
end type

on constructor;call u_base_dw_ext::constructor;is_selection = '3'
end on

