﻿$PBExportHeader$w_source_sequence.srw
$PBExportComments$window to maintain source seq #
forward
global type w_source_sequence from w_base_sheet_ext
end type
type st_d from statictext within w_source_sequence
end type
type dw_d from u_base_dw_ext within w_source_sequence
end type
type st_1 from statictext within w_source_sequence
end type
type st_2 from statictext within w_source_sequence
end type
type st_a from statictext within w_source_sequence
end type
type st_b from statictext within w_source_sequence
end type
type st_c from statictext within w_source_sequence
end type
type dw_plant from u_plant within w_source_sequence
end type
type dw_effective_date from u_effective_date within w_source_sequence
end type
type dw_a from u_base_dw_ext within w_source_sequence
end type
type dw_b from u_base_dw_ext within w_source_sequence
end type
type dw_c from u_base_dw_ext within w_source_sequence
end type
type uo_1 from u_spin_up_down within w_source_sequence
end type
end forward

global type w_source_sequence from w_base_sheet_ext
integer x = 5
integer y = 180
integer width = 6647
integer height = 1728
boolean hscrollbar = true
long backcolor = 67108864
st_d st_d
dw_d dw_d
st_1 st_1
st_2 st_2
st_a st_a
st_b st_b
st_c st_c
dw_plant dw_plant
dw_effective_date dw_effective_date
dw_a dw_a
dw_b dw_b
dw_c dw_c
uo_1 uo_1
end type
global w_source_sequence w_source_sequence

type variables
u_pas201		iu_pas201
u_pas203		iu_pas203
u_ws_pas3		iu_ws_pas3

datastore		ids_tree
s_error		istr_error_info

Long		il_color, &
     		il_SelectedColor, &
		il_SelectedTextColor,&
		il_rec_count_dw_a,&
		il_rec_count_dw_b,&
		il_rec_count_dw_c, &
		il_rec_count_dw_d

Boolean		ib_reinquire

Char		 ic_fetch_direction

Integer		ii_max_page_number		

String		is_selection, &
		is_scroll
		
Datawindow	idw_datawindow
//w_netwise_response	iw_inquirewindow, &
//			iw_parentwindow
end variables

forward prototypes
public function boolean wf_next ()
public function boolean wf_previous ()
public function boolean wf_retrieve ()
public function integer wf_process (string as_scroll_ind)
public function boolean wf_update ()
end prototypes

public function boolean wf_next ();This.wf_process('f')

Return True
end function

public function boolean wf_previous ();This.SetRedraw(False)  
This.wf_process('b')
This.SetRedraw(True)

Return True
end function

public function boolean wf_retrieve ();long			ll_rec_count, &
				ll_rec_count_a, &
				ll_rec_count_b, &
				ll_rec_count_c, &
				ll_rec_count_d, &
				ll_row,&
				li_ret

Boolean		lb_dw_a_empty, &
				lb_dw_b_empty, &
				lb_dw_c_empty

string		ls_plant, &
				ls_plant_desc, & 
				ls_temp, &
				ls_effective_date, &
				ls_data, &
				ls_shift_a, &
				ls_shift_b, &
				ls_shift_c, &
				ls_shift_d, &
				ls_header_a, &
				ls_header_b, &
				ls_header_c, &
				ls_header_d, &
				ls_input

date			ldt_week_end_date

u_string_functions	lu_string

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp86br_inq_source_sequence"
istr_error_info.se_message = Space(71)

ls_plant = dw_plant.uf_get_plant_code()
ls_plant_desc = dw_plant.uf_get_plant_descr()  

ls_effective_date = string(dw_effective_date.uf_get_effective_date(), "yyyy-mm-dd" )

ls_input = ls_plant + '~t' + &
			  ls_effective_date + '~r~n' 

dw_a.reset()
dw_b.reset()
dw_c.reset()
dw_d.reset()

//If Not iu_pas201.nf_pasp86br_inq_source_sequence(istr_error_info, & 
If Not iu_ws_pas3.uf_pasp86fr(istr_error_info, & 
										ls_input, &
										ls_header_a, &
										ls_shift_a, &
										ls_header_b, &
										ls_shift_b, &
										ls_header_c, &
										ls_shift_c, &
										ls_header_d, &
										ls_shift_d)  Then
										This.SetRedraw(True) 
										Return False
End If			
//ls_shift_header = '08:00~t08:00~t12:00~t15:00~t'
//ls_shift_a = 	'08:00~t08:00~tc1200~t31~t1000~t08:00:00~t00~r~n' + &
//					'c1200~t32~t1000~t08:00:00~t00~r~n' + &
//					'c1200~t33~t1000~t08:00:00~t00~r~n' + &
//					'c1200~t34~t1000~t08:00:00~t00~r~n' + &
//					'c1200~t35~t1000~t08:00:00~t00~r~n' + &
//					'c1200~t36~t1000~t08:00:00~t00~r~n' //+ &
////					'c1200~t37~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t38~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t39~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t40~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t41~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t42~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t43~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t44~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t45~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t46~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t47~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t48~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t49~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t50~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t51~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t52~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t53~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t54~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t55~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t56~t1000~t08:00:00~t00~r~n' 
//					
//ls_shift_b = 	'13:00~t08:00~tc1200~t31~t1000~t08:00:00~t00~r~n' + &
//					'c1200~t32~t1000~t08:00:00~t00~r~n'// + &
////					'c1200~t33~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t34~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t35~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t36~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t37~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t38~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t39~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t40~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t41~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t42~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t43~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t44~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t45~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t46~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t47~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t48~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t49~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t50~t1000~t08:00:00~t00~r~n' + &
////					'c1200~t51~t1000~t08:00:00~t00~r~n' 

// fix defect 535 need to initilize instance var
il_rec_count_dw_a = 0
il_rec_count_dw_b = 0
il_rec_count_dw_c = 0
il_rec_count_dw_d = 0


If not 	lu_string.nf_IsEmpty(ls_shift_a) Then
	dw_a.Show()
	st_a.Show()
 	ls_temp = lu_string.nf_gettoken(ls_header_a,'~t')
	dw_a.object.start_time_t.text = ls_temp
	ls_temp = lu_string.nf_gettoken(ls_header_a,'~r~n')
	dw_a.object.prod_hours_t.text = ls_temp
	ll_rec_count_a = dw_a.ImportString(ls_shift_a)
	il_rec_count_dw_a	= ll_rec_count_a 
	ll_rec_count += ll_rec_count_a
Else
	lb_dw_a_empty = True
	dw_a.hide()
	st_a.hide()
End If

If not 	lu_string.nf_IsEmpty(ls_shift_b) Then
	dw_b.Show()
	st_b.Show()
	ls_temp = lu_string.nf_gettoken(ls_header_b,'~t')
	dw_b.object.start_time_t.text = ls_temp
	ls_temp = lu_string.nf_gettoken(ls_header_b,'~r~n')
	dw_b.object.prod_hours_t.text = ls_temp
	ll_rec_count_b = dw_b.ImportString(ls_shift_b)
	il_rec_count_dw_b	= ll_rec_count_b 
	ll_rec_count += ll_rec_count_b
Else
	dw_b.hide()
	st_b.hide()
	lb_dw_b_empty = True
End If		
	
If not 	lu_string.nf_IsEmpty(ls_shift_c) Then
	dw_c.Show()
	st_c.Show()
	ls_temp = lu_string.nf_gettoken(ls_header_c,'~t')
	dw_c.object.start_time_t.text = ls_temp
	ls_temp = lu_string.nf_gettoken(ls_header_c,'~r~n')
	dw_c.object.prod_hours_t.text = ls_temp
	ll_rec_count_c = dw_c.ImportString(ls_shift_c)	
	il_rec_count_dw_c	= ll_rec_count_c 
	ll_rec_count += ll_rec_count_c
Else
	dw_c.hide()
	st_c.hide()
	lb_dw_c_empty = True
End If		

If not 	lu_string.nf_IsEmpty(ls_shift_d) Then
	dw_d.Show()
	st_d.Show()
	ls_temp = lu_string.nf_gettoken(ls_header_d,'~t')
	dw_d.object.start_time_t.text = ls_temp
	ls_temp = lu_string.nf_gettoken(ls_header_d,'~r~n')
	dw_d.object.prod_hours_t.text = ls_temp
	ll_rec_count_d = dw_d.ImportString(ls_shift_d)	
	il_rec_count_dw_d	= ll_rec_count_d 
	ll_rec_count += ll_rec_count_d
Else
	dw_d.hide()
	st_d.hide()
End If	

	
	
If ll_rec_count > 0 Then 
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
	iw_frame.im_menu.mf_enable('m_save')
	iw_frame.im_menu.mf_enable('m_next')
	iw_frame.im_menu.mf_enable('m_previous')
	uo_1.Show()
	st_1.Show()
	st_2.Show()
	If lb_dw_a_empty Then
		If lb_dw_b_empty Then
			If lb_dw_c_empty Then
				dw_d.SetFocus()
			Else
				dw_c.SetFocus()
				dw_d.SelectRow(0,False)
			End If
		Else
			dw_b.SetFocus()
			dw_c.SelectRow(0,False)
			dw_d.SelectRow(0,False)
		End If
	Else
		dw_a.SetFocus()
		dw_b.SelectRow(0,False)
		dw_c.SelectRow(0,False)
		dw_d.SelectRow(0,False)
	End IF
Else
	uo_1.Hide()
	st_1.Hide()
	st_2.Hide()
	iw_frame.im_menu.mf_disable('m_next')
	iw_frame.im_menu.mf_disable('m_previous')
	iw_frame.im_menu.mf_disable('m_save')
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True) 

dw_a.ResetUpdate()
dw_b.ResetUpdate()
dw_c.ResetUpdate()
dw_d.ResetUpdate()
 
Return True

end function

public function integer wf_process (string as_scroll_ind);String 	ls_scroll
				
Long		ll_row,&
			ll_last_row



ll_row = idw_datawindow.GetRow()
//MessageBox ('ll_row',string(ll_row))

CHOOSE CASE idw_datawindow
	CASE dw_a
		ll_last_row = il_rec_count_dw_a
	CASE dw_b
		ll_last_row = il_rec_count_dw_b
	CASE dw_c
		ll_last_row = il_rec_count_dw_c
	CASE dw_d
		ll_last_row = il_rec_count_dw_d		
	CASE ELSE
		MessageBox ('Invalid DataWindow','Contact Applications.')
		idw_datawindow.SetFocus()
		Return 1
END CHOOSE
		
IF ll_row = 0 or IsNull(ll_row) Then
	SetMicroHelp('You must have a source selected')
	idw_datawindow.SetFocus()	
	Return 1
else
	ls_scroll = as_scroll_ind
	If ls_scroll = 'b' Then
		If ll_row = 1 Then
			SetMicroHelp('This row is already at the top ')
			idw_datawindow.SelectRow(0,False)
			idw_datawindow.ScrollToRow ( ll_row)
			idw_datawindow.SelectRow(ll_row,True)
			idw_datawindow.SetFocus()	
			Return 1
		else
			idw_datawindow.RowsMove(ll_row, ll_row, Primary!, idw_datawindow, ll_row - 1,Primary!)
//			idw_datawindow.SelectRow ( ll_row, False)
			ll_row = ll_row - 1
		End If
	Else
		If ll_row = ll_last_row Then
			SetMicroHelp('This row is already at the bottom')
			idw_datawindow.SelectRow(0,False)
			idw_datawindow.ScrollToRow ( ll_row)
			idw_datawindow.SelectRow(ll_row,True)
			idw_datawindow.SetFocus()	
			Return 1
		else	
			idw_datawindow.RowsMove(ll_row, ll_row, Primary!, idw_datawindow, ll_row + 2,Primary!)
//			idw_datawindow.SelectRow ( ll_row - 1, False)
			ll_row = ll_row + 1
		End If
	End If
End if

SetMicroHelp('Ready')
idw_datawindow.SetItemStatus ( ll_row, 0, Primary!, DataModified! )
// Find and highlight the current row.
idw_datawindow.SelectRow(0, False)
idw_datawindow.ScrollToRow ( ll_row)
idw_datawindow.SelectRow(ll_row,True)
idw_datawindow.SetFocus()	
Return 0
end function

public function boolean wf_update ();String			ls_input_string,&
					ls_header_string

Long				ll_row,&
					ll_row_end
					
SetPointer(HourGlass!)
			
ls_header_string = ''
ls_header_string += 	String(dw_plant.uf_get_plant_code()) + '~t' 
ls_header_string +=	String(dw_effective_date.uf_get_effective_date(),'yyyy-mm-dd') + '~r~n'

ls_input_string = ''	

IF il_rec_count_dw_a > 0 Then
	ll_row = 1
	ll_row_end = il_rec_count_dw_a
	DO UNTIL ll_row > ll_row_end
		ls_input_string += dw_a.GetItemString(ll_row, 'product') + '~t'
		ls_input_string += dw_a.GetItemString(ll_row, 'product_state') + '~t'
		ls_input_string += String(dw_a.GetItemTime(ll_row, 'begin_time'),'hh:mm:ss') + '~t'
		dw_a.SetItem ( ll_row, 'seq_num', ll_row )
		ls_input_string += String(ll_row) + '~t'
		ls_input_string += dw_a.GetItemString(ll_row, 'production_plant') + '~t'
		ls_input_string += string(dw_a.GetItemdate(ll_row, 'process_date')) + '~r~n'
		ll_row ++
	LOOP
End If

IF il_rec_count_dw_b > 0 Then
	ll_row = 1
	ll_row_end = il_rec_count_dw_b
	DO UNTIL ll_row > ll_row_end
		ls_input_string += dw_b.GetItemString(ll_row, 'product') + '~t'
		ls_input_string += dw_b.GetItemString(ll_row, 'product_state') + '~t'
		ls_input_string += String(dw_b.GetItemTime(ll_row, 'begin_time'),'hh:mm:ss') + '~t'
		dw_b.SetItem ( ll_row, 'seq_num', ll_row )
		ls_input_string += String(ll_row) + '~t'
		ls_input_string += dw_b.GetItemString(ll_row, 'production_plant') + '~t'
		ls_input_string += string(dw_b.GetItemdate(ll_row, 'process_date')) + '~r~n'
		ll_row ++
	LOOP
End If

IF il_rec_count_dw_c > 0 Then
	ll_row = 1
	ll_row_end = il_rec_count_dw_c
	DO UNTIL ll_row > ll_row_end
		ls_input_string += dw_c.GetItemString(ll_row, 'product') + '~t'
		ls_input_string += dw_c.GetItemString(ll_row, 'product_state') + '~t'
		ls_input_string += String(dw_c.GetItemTime(ll_row, 'begin_time'),'hh:mm:ss') + '~t'
		dw_c.SetItem ( ll_row, 'seq_num', ll_row )
		ls_input_string += String(ll_row) + '~t'
		ls_input_string += dw_c.GetItemString(ll_row, 'production_plant') + '~t'
		ls_input_string += string(dw_c.GetItemdate(ll_row, 'process_date')) + '~r~n'
		ll_row ++
	LOOP
End If

IF il_rec_count_dw_d > 0 Then
	ll_row = 1
	ll_row_end = il_rec_count_dw_d
	DO UNTIL ll_row > ll_row_end
		ls_input_string += dw_d.GetItemString(ll_row, 'product') + '~t'
		ls_input_string += dw_d.GetItemString(ll_row, 'product_state') + '~t'
		ls_input_string += String(dw_d.GetItemTime(ll_row, 'begin_time'),'hh:mm:ss') + '~t'
		dw_d.SetItem ( ll_row, 'seq_num', ll_row )
		ls_input_string += String(ll_row) + '~t'
		ls_input_string += dw_d.GetItemString(ll_row, 'production_plant') + '~t'
		ls_input_string += string(dw_d.GetItemdate(ll_row, 'process_date')) + '~r~n'
		ll_row ++
	LOOP
End If

//MessageBox('Header Data', ls_header_string)
//MessageBox('DataWindow Data', ls_input_string)

//If Not iu_pas201.nf_pasp87br_upd_source_sequence(istr_error_info, &
//																ls_header_string, &
//																ls_input_string) Then	
If Not iu_ws_pas3.uf_pasp87fr(istr_error_info, &
										ls_header_string, &
										ls_input_string) Then	
										Return False
End If

iw_frame.SetMicroHelp('Modification Successful')
dw_a.ResetUpdate()
dw_b.ResetUpdate()
dw_c.ResetUpdate()
dw_d.ResetUpdate()
Return True
end function

on w_source_sequence.create
int iCurrent
call super::create
this.st_d=create st_d
this.dw_d=create dw_d
this.st_1=create st_1
this.st_2=create st_2
this.st_a=create st_a
this.st_b=create st_b
this.st_c=create st_c
this.dw_plant=create dw_plant
this.dw_effective_date=create dw_effective_date
this.dw_a=create dw_a
this.dw_b=create dw_b
this.dw_c=create dw_c
this.uo_1=create uo_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_d
this.Control[iCurrent+2]=this.dw_d
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_a
this.Control[iCurrent+6]=this.st_b
this.Control[iCurrent+7]=this.st_c
this.Control[iCurrent+8]=this.dw_plant
this.Control[iCurrent+9]=this.dw_effective_date
this.Control[iCurrent+10]=this.dw_a
this.Control[iCurrent+11]=this.dw_b
this.Control[iCurrent+12]=this.dw_c
this.Control[iCurrent+13]=this.uo_1
end on

on w_source_sequence.destroy
call super::destroy
destroy(this.st_d)
destroy(this.dw_d)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_a)
destroy(this.st_b)
destroy(this.st_c)
destroy(this.dw_plant)
destroy(this.dw_effective_date)
destroy(this.dw_a)
destroy(this.dw_b)
destroy(this.dw_c)
destroy(this.uo_1)
end on

event activate;call super::activate;// moved to wf_retrieve 
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')


iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_generatesales')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')

end event

event deactivate;iw_frame.im_menu.mf_disable('m_save')
iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')


iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_generatesales')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')

end event

event ue_get_data;call super::ue_get_data;date		ldt_date	

Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'effective_date'
		ldt_date = dw_effective_date.uf_get_effective_date()
		message.StringParm = string(ldt_date) 
End choose

end event

event ue_postopen;call super::ue_postopen;String			ls_char, &
					ls_group_id, &
					ls_modify_auth

DataWindowChild	ldwc_char

// set the properities on u_effective_date
dw_effective_date.uf_set_text('Production Date:')


This.PostEvent('ue_query')//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "srceseq"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_source_sequence_inq'

iu_pas201 = Create u_pas201
iu_pas203 = Create u_pas203
iu_ws_pas3 = Create u_ws_pas3

ids_tree = Create DataStore
ids_tree.DataObject = 'd_yields_tree'

istr_error_info.se_event_name = "ue_postopen"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

//If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
If iu_ws_pas3.uf_pasp61fr(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_a.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)

dw_b.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)

dw_c.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)

dw_d.GetChild("char_key", ldwc_char)
ldwc_char.ImportString(ls_char)

wf_retrieve()

end event

event ue_set_data;call super::ue_set_data;u_conversion_functions		lu_conv


Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date(Date(as_value))
	Case 'ib_inquire'
		ib_inquire = lu_conv.nf_Boolean(as_value)
End Choose
end event

event open;call super::open;This.Title = 'Source Sequence'
end event

type st_d from statictext within w_source_sequence
integer x = 5010
integer y = 128
integer width = 197
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Shift D"
boolean focusrectangle = false
end type

event constructor;This.Hide()
end event

type dw_d from u_base_dw_ext within w_source_sequence
integer x = 4718
integer y = 192
integer width = 1440
integer height = 1228
integer taborder = 30
string dataobject = "d_source_sequence_detail"
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;
IF row > 0 THEN
	dw_d.SelectRow(row,True)
Else
	dw_d.SelectRow(dw_d.getrow(),True)
End If


dw_a.SelectRow(0,False)
dw_b.SelectRow(0,False)
dw_c.SelectRow(0,False)
end event

event getfocus;call super::getfocus;// Set instance datawindow to this control.
idw_datawindow = This

end event

event constructor;call super::constructor;is_selection = '1'
This.Hide()
end event

event ue_keydown;call super::ue_keydown;Choose Case key
	Case KeyTab!
		If il_rec_count_dw_a > 0 Then
			dw_a.SelectRow(dw_a.GetRow(), True)
			dw_b.SelectRow(0,False)
			dw_c.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			Return 0
		End If
		If il_rec_count_dw_b > 0 Then
			dw_b.SelectRow(dw_b.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_c.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			return 0
		End If
		If il_rec_count_dw_c > 0 Then
			dw_c.SelectRow(dw_c.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_b.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			return 0
		End If		
End Choose

end event

type st_1 from statictext within w_source_sequence
integer x = 1559
integer y = 436
integer width = 69
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Up"
boolean focusrectangle = false
end type

event constructor;This.Hide()
end event

type st_2 from statictext within w_source_sequence
integer x = 1559
integer y = 544
integer width = 133
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Down"
boolean focusrectangle = false
end type

event constructor;This.Hide()
end event

type st_a from statictext within w_source_sequence
integer x = 544
integer y = 128
integer width = 155
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Shift A"
boolean focusrectangle = false
end type

event constructor;This.Hide()
end event

type st_b from statictext within w_source_sequence
integer x = 2066
integer y = 128
integer width = 160
integer height = 64
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Shift B"
boolean focusrectangle = false
end type

event constructor;This.Hide()
end event

type st_c from statictext within w_source_sequence
integer x = 3483
integer y = 128
integer width = 165
integer height = 60
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Shift C"
boolean focusrectangle = false
end type

event constructor;This.Hide()
end event

type dw_plant from u_plant within w_source_sequence
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()
end event

type dw_effective_date from u_effective_date within w_source_sequence
integer x = 1435
integer width = 786
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_Enable(False)
This.uf_set_effective_date(Today())
end event

type dw_a from u_base_dw_ext within w_source_sequence
integer x = 14
integer y = 188
integer width = 1399
integer height = 1216
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_source_sequence_detail"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;is_selection = '1'
This.Hide()
end event

event getfocus;// Set instance datawindow to this control.
idw_datawindow = This

end event

event clicked;call super::clicked;// When the control gets focus unhighlight the other datawindows row.

IF row > 0 THEN
	dw_a.SelectRow(row,True)
Else
	dw_a.SelectRow(dw_a.getrow(),True)
End If


dw_b.SelectRow(0,False)
dw_c.SelectRow(0,False)
dw_d.SelectRow(0,False)

end event

event ue_keydown;call super::ue_keydown;Choose Case key
	Case KeyTab!
		If il_rec_count_dw_b > 0 Then
			dw_b.SelectRow(dw_b.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_c.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			Return 0
		End If
		If il_rec_count_dw_c > 0 Then
			dw_c.SelectRow(dw_c.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_b.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			return 0
		End If
		If il_rec_count_dw_d > 0 Then
			dw_d.SelectRow(dw_d.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_b.SelectRow(0,False)
			dw_c.SelectRow(0,False)
			return 0
		End If		
End Choose
end event

type dw_b from u_base_dw_ext within w_source_sequence
integer x = 1710
integer y = 188
integer width = 1440
integer height = 1228
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_source_sequence_detail"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;is_selection = '1'
This.Hide()
end event

event getfocus;// Set instance datawindow to this control.
idw_datawindow = This

end event

event ue_keydown;call super::ue_keydown;Choose Case key
	Case KeyTab!
		If il_rec_count_dw_c > 0 Then
			dw_c.SelectRow(dw_c.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_b.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			Return 0
		End If
		If il_rec_count_dw_d > 0 Then
			dw_d.SelectRow(dw_d.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_b.SelectRow(0,False)
			dw_c.SelectRow(0,False)
			return 0
		End If		
		If il_rec_count_dw_a > 0 Then
			dw_a.SelectRow(dw_a.GetRow(), True)
			dw_b.SelectRow(0,False)
			dw_c.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			return 0
		End If
		
End Choose

end event

event clicked;call super::clicked;
IF row > 0 THEN
	dw_b.SelectRow(row,True)
Else
	dw_b.SelectRow(dw_b.getrow(),True)
End If


dw_a.SelectRow(0,False)
dw_c.SelectRow(0,False)
dw_d.SelectRow(0,False)

end event

type dw_c from u_base_dw_ext within w_source_sequence
integer x = 3195
integer y = 184
integer width = 1440
integer height = 1228
integer taborder = 25
boolean bringtotop = true
string dataobject = "d_source_sequence_detail"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;is_selection = '1'
This.Hide()
end event

event getfocus;// Set instance datawindow to this control.
idw_datawindow = This

end event

event ue_keydown;call super::ue_keydown;Choose Case key
	Case KeyTab!
		If il_rec_count_dw_d > 0 Then
			dw_d.SelectRow(dw_d.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_b.SelectRow(0,False)
			dw_c.SelectRow(0,False)
			Return 0
		End If
		If il_rec_count_dw_a > 0 Then
			dw_a.SelectRow(dw_a.GetRow(), True)
			dw_c.SelectRow(0,False)
			dw_b.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			Return 0
		End If
		If il_rec_count_dw_b > 0 Then
			dw_b.SelectRow(dw_b.GetRow(), True)
			dw_a.SelectRow(0,False)
			dw_c.SelectRow(0,False)
			dw_d.SelectRow(0,False)
			return 0
		End If
End Choose

end event

event clicked;call super::clicked;
IF row > 0 THEN
	dw_c.SelectRow(row,True)
Else
	dw_c.SelectRow(dw_c.getrow(),True)
End If


dw_b.SelectRow(0,False)
dw_a.SelectRow(0,False)
dw_d.SelectRow(0,False)
end event

type uo_1 from u_spin_up_down within w_source_sequence
integer x = 1413
integer y = 412
integer width = 146
integer height = 224
integer taborder = 50
boolean bringtotop = true
long backcolor = 67108864
end type

on uo_1.destroy
call u_spin_up_down::destroy
end on

event constructor;call super::constructor;This.Hide()
end event

