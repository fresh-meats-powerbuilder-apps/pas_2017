﻿$PBExportHeader$w_pas_actual_prod_update_mb.srw
forward
global type w_pas_actual_prod_update_mb from w_base_response_ext
end type
type st_1 from statictext within w_pas_actual_prod_update_mb
end type
type cb_all from commandbutton within w_pas_actual_prod_update_mb
end type
type cb_selected from commandbutton within w_pas_actual_prod_update_mb
end type
type p_1 from picture within w_pas_actual_prod_update_mb
end type
end forward

global type w_pas_actual_prod_update_mb from w_base_response_ext
int X=188
int Y=401
int Width=1921
int Height=525
boolean TitleBar=true
string Title="Update"
st_1 st_1
cb_all cb_all
cb_selected cb_selected
p_1 p_1
end type
global w_pas_actual_prod_update_mb w_pas_actual_prod_update_mb

on close;call w_base_response_ext::close;If Len(Trim(Message.StringParm)) = 0 Then
	Message.StringParm = "cancel"
End if
end on

on w_pas_actual_prod_update_mb.create
int iCurrent
call w_base_response_ext::create
this.st_1=create st_1
this.cb_all=create cb_all
this.cb_selected=create cb_selected
this.p_1=create p_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=st_1
this.Control[iCurrent+2]=cb_all
this.Control[iCurrent+3]=cb_selected
this.Control[iCurrent+4]=p_1
end on

on w_pas_actual_prod_update_mb.destroy
call w_base_response_ext::destroy
destroy(this.st_1)
destroy(this.cb_all)
destroy(this.cb_selected)
destroy(this.p_1)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "cancel")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_actual_prod_update_mb
int X=595
int Y=677
int TabOrder=10
boolean Visible=false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_actual_prod_update_mb
int X=1271
int Y=261
int Width=481
int TabOrder=40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_actual_prod_update_mb
int X=595
int Y=429
int TabOrder=20
boolean Visible=false
end type

type st_1 from statictext within w_pas_actual_prod_update_mb
int X=257
int Y=113
int Width=1175
int Height=77
boolean Enabled=false
string Text="Do you wish to update all rows, or only selected rows?"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=16777215
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_all from commandbutton within w_pas_actual_prod_update_mb
int X=243
int Y=261
int Width=481
int Height=109
int TabOrder=30
string Text="Update All"
boolean Default=true
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;CloseWithReturn(Parent, "all")
end on

type cb_selected from commandbutton within w_pas_actual_prod_update_mb
int X=764
int Y=261
int Width=481
int Height=109
int TabOrder=50
string Text="Update Selected"
int TextSize=-9
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;CloseWithReturn(Parent, "selected")
end on

type p_1 from picture within w_pas_actual_prod_update_mb
int X=60
int Y=73
int Width=183
int Height=161
string PictureName="question.bmp"
boolean FocusRectangle=false
end type

