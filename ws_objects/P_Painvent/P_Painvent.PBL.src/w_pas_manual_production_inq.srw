﻿$PBExportHeader$w_pas_manual_production_inq.srw
forward
global type w_pas_manual_production_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_pas_manual_production_inq
end type
type dw_prod_inq from u_base_dw_ext within w_pas_manual_production_inq
end type
type dw_prod_ind from u_base_dw_ext within w_pas_manual_production_inq
end type
type dw_area from u_sched_area_sect within w_pas_manual_production_inq
end type
type uo_prod_group from u_pa_prod_grp within w_pas_manual_production_inq
end type
type dw_compare_retrieve from u_base_dw_ext within w_pas_manual_production_inq
end type
end forward

global type w_pas_manual_production_inq from w_base_response_ext
integer width = 1760
integer height = 1964
long backcolor = 67108864
dw_plant dw_plant
dw_prod_inq dw_prod_inq
dw_prod_ind dw_prod_ind
dw_area dw_area
uo_prod_group uo_prod_group
dw_compare_retrieve dw_compare_retrieve
end type
global w_pas_manual_production_inq w_pas_manual_production_inq

type variables
Boolean			ib_valid_to_close
w_pas_manual_production	iw_parent
u_pas203			iu_pas203		
u_ws_pas3			iu_ws_pas3
s_error		istr_error_info
end variables

forward prototypes
public subroutine wf_get_end_of_shift ()
end prototypes

public subroutine wf_get_end_of_shift ();String	ls_plant, &
			ls_begin_date, &
			ls_shift, &
			ls_data, &
			ls_output, ls_fromTime, ls_toTime
			
integer rtncode
			
DataWindowChild	dwc_end_of_shift_flag

ls_fromTime = STRING(dw_prod_inq.GetItemTime(1, "from_time"))
ls_toTime = string(dw_prod_inq.GetItemTime(1, "to_time"))
			
// dmk sr4031
// Set the transaction object for the child
dw_prod_inq.GetChild('yld_end_of_shift_flag', dwc_end_of_shift_flag)
//	



CONNECT USING SQLCA;	
dwc_end_of_shift_flag.SetTransObject(SQLCA)
dwc_end_of_shift_flag.Retrieve()


dw_plant.AcceptText() 
ls_plant = dw_plant.GetItemString(1,"location_code")

dw_prod_inq.AcceptText()
//ls_begin_date = String(dw_prod_inq.GetItemDate(1,"begin_date"),"mm/dd/yyyy")
ls_shift = dw_prod_inq.GetItemString(1,"shift")

if (ls_plant > '   ') and (ls_shift > ' ') then
	ls_data = ls_plant + '~t' + dw_prod_inq.Describe("DataWindow.Data")
	istr_error_info.se_event_name = "wf_get_end_of_shift"
//	iu_pas203.nf_pasp27cr_inq_actual_production(istr_error_info, &
//									ls_data, &
//									ls_output)
	iu_ws_pas3.uf_pasp27gr(istr_error_info, &
									ls_data, &
									ls_output)
//strip off plant and 1st tab since that belongs to different data window
	ls_output = mid(ls_output,5)
	//ls_output += '~t' + ls_fromTime + '~t' + ls_toTime + '~t'
	dw_prod_inq.Reset()
	dw_prod_inq.ImportString(ls_output)								
end if

dw_prod_inq.SetItem(1, "from_time", TIME(ls_fromTime))
dw_prod_inq.SetItem(1, "to_time", TIME(ls_toTime))

//return true




end subroutine

event ue_postopen;call super::ue_postopen;Int		li_pos

String	ls_data, &
			ls_temp, &
			ls_begin_date, &
			ls_output, &
			ls_test_plant, &
			ls_test_shift, ls_sect_name, ls_plant, &
			ls_check_box_val
			 
u_string_functions	lu_string_functions	

dw_prod_ind.SetItem(1, "prod_ind", "L")
dw_area.Object.sect_name_code.Visible = false

dw_area.visible = false
uo_prod_group.visible = false
dw_prod_inq.Object.from_time.visible = false
dw_prod_inq.Object.to_time.visible = false
dw_prod_inq.Object.t_2.visible = false
dw_prod_inq.Object.t_3.visible = false
dw_prod_inq.SetItem(1, "production_ind", "Y")

dw_plant.AcceptText() 
ls_plant = dw_plant.GetItemString(1,"location_code")
dw_area.uf_get_plt_codes(ls_plant)

ls_data = iw_parent.wf_GetHeaderData()


If iw_frame.iu_string.nf_isEmpty(ls_data) Or (Left(ls_data, 1) = '~t') Then return

li_pos = iw_frame.iu_string.nf_npos(ls_data, '~t', 1, 2)

dw_plant.Reset()
dw_plant.ImportString(ls_data)

ls_data = Right(ls_data, Len(ls_data) - li_pos)

ls_begin_date = lu_string_functions.nf_GetToken(ls_data, '~t')

If Len(ls_begin_date) > 0 Then
	ls_begin_date = String(Date(ls_begin_date), 'mm/dd/yyyy')
Else
	ls_begin_date = String(Today(), 'mm/dd/yyyy')
End if


dw_prod_inq.SetItem(1,"begin_date",ls_begin_date)

dw_prod_inq.SetItem(1,"shift",lu_string_functions.nf_GetToken(ls_data, '~t'))

string ls_production_ind
ls_production_ind = lu_string_functions.nf_GetToken(ls_data, '~t')
dw_prod_inq.SetItem(1,"production_ind", ls_production_ind)


string ls_productInd, ls_areaOrGroup, ls_fromTime, ls_toTime
ls_productInd = lu_string_functions.nf_GetToken(ls_data, '~t') // strip off UOM
ls_productInd = lu_string_functions.nf_GetToken(ls_data, '~t') // strip off end of shift
ls_productInd = lu_string_functions.nf_GetToken(ls_data, '~t') // strip off END OF SHIFT DATE
ls_productInd = lu_string_functions.nf_GetToken(ls_data, '~t') // strip off END OF SHIFT TIME



ls_fromTime = lu_string_functions.nf_GetToken(ls_data, '~t')
ls_toTime = lu_string_functions.nf_GetToken(ls_data, '~t')
ls_productInd = lu_string_functions.nf_GetToken(ls_data, '~t')

ls_areaOrGroup = lu_string_functions.nf_GetToken(ls_data, '~t')

//messagebox(ls_productInd, ls_areaOrGroup)


dw_prod_ind.SetItem(1, "prod_ind", ls_productInd)
dw_prod_ind.PostEvent("itemchanged")
choose case ls_productInd
	case 'A'
		dw_area.SetItem(1, "area_name_code", ls_areaOrGroup)
		dw_area.visible = true

	case 'G'
		// cant set group
		uo_prod_group.visible = true
end choose


if ls_production_ind = 'T' then
	
	dw_prod_inq.Object.from_time.visible = true
	dw_prod_inq.Object.to_time.visible = true
	dw_prod_inq.Object.t_2.visible = true
	dw_prod_inq.Object.t_3.visible = true
	dw_prod_inq.SetItem(1, "from_time", TIME(ls_fromTime))
	dw_prod_inq.SetItem(1, "to_time", TIME(ls_toTime))
end if
	
If dw_prod_inq.GetItemString(1,"production_ind") = 'C' Then
	dw_compare_retrieve.Visible = True
	ls_check_box_val = lu_string_functions.nf_GetToken(ls_data, '~t')
	dw_compare_retrieve.SetItem(1, "pa_projection", ls_check_box_val)
	ls_check_box_val = lu_string_functions.nf_GetToken(ls_data, '~t')
	dw_compare_retrieve.SetItem(1, "yields", ls_check_box_val)
	ls_check_box_val = lu_string_functions.nf_GetToken(ls_data, '~t')
	dw_compare_retrieve.SetItem(1, "pa_actual", ls_check_box_val)
	ls_check_box_val = lu_string_functions.nf_GetToken(ls_data, '~t')
	dw_compare_retrieve.SetItem(1, "material_handling", ls_check_box_val)		
Else
	dw_compare_retrieve.Visible = False
End if	

wf_get_end_of_shift()


end event

event open;call super::open;iw_parent = Message.PowerObjectParm
If Not IsValid(iw_parent) Then 
	Close(This)
	return
End if

If Not IsValid(iu_pas203) Then
	iu_pas203 = Create u_pas203
	If Message.ReturnValue = -1 Then 
		Close(This)
		Return
	End if
End if

If Not IsValid(iu_ws_pas3) Then
	iu_ws_pas3 = Create u_ws_pas3
	If Message.ReturnValue = -1 Then 
		Close(This)
		Return
	End if
End if

This.Title = iw_parent.Title + " Inquire"
uo_prod_group.Visible = False
end event

event close;call super::close;If Not ib_valid_to_close Then Message.StringParm = ""

If IsValid(iu_pas203) Then
	Destroy iu_pas203
End if

If IsValid(iu_ws_pas3) Then
	Destroy iu_ws_pas3
End if
end event

on w_pas_manual_production_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_prod_inq=create dw_prod_inq
this.dw_prod_ind=create dw_prod_ind
this.dw_area=create dw_area
this.uo_prod_group=create uo_prod_group
this.dw_compare_retrieve=create dw_compare_retrieve
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_prod_inq
this.Control[iCurrent+3]=this.dw_prod_ind
this.Control[iCurrent+4]=this.dw_area
this.Control[iCurrent+5]=this.uo_prod_group
this.Control[iCurrent+6]=this.dw_compare_retrieve
end on

on w_pas_manual_production_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_prod_inq)
destroy(this.dw_prod_ind)
destroy(this.dw_area)
destroy(this.uo_prod_group)
destroy(this.dw_compare_retrieve)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;Date		ldt_temp

Int		li_DaysAfter, li_compare_checked_count

String	ls_temp, &
			ls_return,  ls_prodInd, ls_products, ls_times


If dw_plant.AcceptText() = -1 Then return
If dw_prod_inq.AcceptText() = -1 Then return

ls_temp = dw_plant.nf_Get_Plant_Code()
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End if
ls_return = ls_temp + '~t' + dw_plant.uf_get_plant_descr( ) + '~t'

// we can assume that it is a valid date now
ldt_temp = Date(dw_prod_inq.GetItemString(1, "begin_date"))
li_daysAfter = DaysAfter(Today(), ldt_temp)
// valid dates are today, yesterday, and the day before
If li_DaysAfter < -4 or li_DaysAfter > 0 Then
	iw_frame.SetMicroHelp("Date must be " + String(RelativeDate(Today(), -4), "mm/dd/yyyy") + &
									" to " + String(Today(), "mm/dd/yyyy"))
	dw_prod_inq.SetColumn("begin_date")
	dw_prod_inq.SetFocus()
	return
End if

ls_return += String(ldt_temp, "yyyy-mm-dd") + '~t'

ls_temp = dw_prod_inq.GetItemString(1, "shift")
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	iw_Frame.SetMicroHelp("Shift is a required field")
	dw_prod_inq.SetColumn("shift")
	dw_prod_inq.SetFocus()
	return
End if
ls_return += ls_temp + '~t'

ls_prodInd = dw_prod_inq.GetItemString(1, "production_ind")
ls_temp = ls_prodInd
If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	iw_Frame.SetMicroHelp("Production is a required radio button")
	dw_prod_inq.SetColumn("production_ind")
	dw_prod_inq.SetFocus()
	return
End if

If ls_prodINd = 'C' Then
	li_compare_checked_count = 0
	If dw_compare_retrieve.GetItemString(1, "pa_projection") = 'Y' Then 
		li_compare_checked_count ++
	End If
	If dw_compare_retrieve.GetItemString(1, "yields") = 'Y' Then 
		li_compare_checked_count ++
	End If
	If dw_compare_retrieve.GetItemString(1, "pa_actual") = 'Y' Then 
		li_compare_checked_count ++
	End If
	If dw_compare_retrieve.GetItemString(1, "material_handling") = 'Y' Then 
		li_compare_checked_count ++
	End If
	If li_compare_checked_count < 2 Then
		iw_frame.SetMicroHelp("At least 2 retrieve check boxes must be checked")
		Return
	End If
End If	
	
ls_return += ls_temp + '~t'

//ls_temp = dw_prod_inq.GetItemString(1, "uom_ind")
//If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
//	iw_Frame.SetMicroHelp("Unit of Measure is a required radio button")
//	dw_prod_inq.SetColumn("uom_ind")
//	dw_prod_inq.SetFocus()
//	return
//End if

//default unit of measure to 'B'
ls_temp = 'B'
ls_return += ls_temp + '~t'

ls_times = '' + '~t' + '' + '~t' // default to space
CHOOSE CASE ls_prodInd
	CASE 'M'
		ls_return += dw_prod_inq.GetItemString(1,"mh_end_of_shift_flag")  + '~t' &
		 			 +  dw_prod_inq.GetItemString(1,"mh_end_of_shift_date")  + '~t' & 
		 			 +  dw_prod_inq.GetItemString(1,"mh_end_of_shift_time")  + '~t' /*&
					 +  dw_prod_inq.GetItemString(1,"yld_end_of_shift_flag") + '~t'*/
	CASE 'Y'
		ls_return += dw_prod_inq.GetItemString(1,"yld_end_of_shift_flag") + '~t' &
		 			 +  dw_prod_inq.GetItemString(1,"yld_end_of_shift_date") + '~t' &
		 			 +  dw_prod_inq.GetItemString(1,"yld_end_of_shift_time") + '~t' /*&
					 +  '' + '~t'*/
	CASE 'T' // time
		ls_return += '' + '~t' + '' + '~t' + '' + '~t' /*+  '' + '~t'*/
		
		
		ls_times  =  STRING(dw_prod_inq.GetItemTime(1, "from_time")) + '~t' &
					 +	 STRING(dw_prod_inq.GetItemTime(1, "to_time"))   + '~t'
	CASE 'U' // compare consumption
		ls_return += '' + '~t' + '' + '~t' + '' + '~t' /*+  '' + '~t'*/
		
   CASE ELSE
		ls_return += '' + '~t' + dw_prod_inq.GetItemString(1, "last_update_info") + '~t' + '' + '~t' //+  '' + '~t'
END CHOOSE

ls_return += ls_times

ls_products = dw_prod_ind.GetItemString(1, "prod_ind")
ls_return += ls_products + '~t'
CHOOSE CASE ls_products
	CASE 'G' // group
		string grpSelDesc, grpSel
		uo_prod_group.uf_get_sel_id(grpSel)
		grpSelDesc = uo_prod_group.uf_get_sel_desc()
		ls_return += grpSel + ',' + grpSelDesc + '~t'
	CASE 'A' // area		
		ls_return += dw_area.GetItemString(1, "area_name_code") + '~t'
	CASE ELSE // All
		ls_return += "" + '~t'
END CHOOSE

If ls_prodINd = 'C' Then
	ls_return += dw_compare_retrieve.GetItemString(1, "pa_projection") + '~t'
	ls_return += dw_compare_retrieve.GetItemString(1, "yields") + '~t'
	ls_return += dw_compare_retrieve.GetItemString(1, "pa_actual") + '~t'
	ls_return += dw_compare_retrieve.GetItemString(1, "material_handling") + '~t'
Else
	ls_return += "N" + '~t' + "N" + '~t' + "N" + '~t' + "N" + '~t' 
End IF
	

/****************************************************/
// NEED TO HANDLE THE PASSING OF PRODUCT CODE STILL...
/****************************************************/

ib_valid_to_close = True
CloseWithReturn(This, ls_return)


end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_manual_production_inq
integer x = 1440
integer y = 1768
integer taborder = 80
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_manual_production_inq
integer x = 1125
integer y = 1768
integer taborder = 70
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_manual_production_inq
integer x = 809
integer y = 1768
integer taborder = 60
end type

type dw_plant from u_plant within w_pas_manual_production_inq
integer y = 4
integer width = 1426
integer taborder = 10
end type

event itemchanged;call super::itemchanged;
Parent.wf_get_end_of_shift()

dw_area.uf_get_plt_codes(data)
end event

type dw_prod_inq from u_base_dw_ext within w_pas_manual_production_inq
integer y = 96
integer width = 1682
integer height = 768
integer taborder = 20
string dataobject = "d_pas_man_prod_inq"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild	dwc_date, &
                  ldwc_type
Date					ldt_today
Int					li_ret
String				ls_import


ldt_today = Today()
This.InsertRow(0)
dw_prod_inq.GetChild("begin_date", dwc_date)
ls_Import = String(RelativeDate(ldt_today, -4), 'mm/dd/yyyy') + '~r~n' + &
							String(RelativeDate(ldt_today, -3), 'mm/dd/yyyy') + '~r~n' + &
							String(RelativeDate(ldt_today, -2), 'mm/dd/yyyy') + '~r~n' + &
							String(RelativeDate(ldt_today, -1), 'mm/dd/yyyy') + '~r~n' + &
							String(ldt_today, 'mm/dd/yyyy')
li_ret = dwc_date.ImportString(ls_Import)

// Plant is selected so don't enable weight to be selected
//This.SetItem(1, "uom_ind", "B")
//This.Modify("uom_ind.Protect = 1")

This.SetItem(1, 'begin_date', String(ldt_today, 'mm/dd/yyyy'))

		


This.GetChild("shift", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("shift")
This.InsertRow(0)

end event

event itemerror;call super::itemerror;String	ls_GetText

ls_GetText = This.GetText()

Choose Case This.GetColumnName()
Case "begin_date"
	If Not IsDate(ls_GetText) Then
		iw_Frame.SetMicroHelp("Begin Date Must be a valid date")
		Return 1
	End if
	
case "from_time", "to_time"
	if not IsTime(ls_GetText) then
		iw_Frame.SetMicroHelp("Time Must be a valid time")
		Return 1
	End if
case else
	iw_Frame.SetMicroHelp("")
End Choose
end event

event itemchanged;call super::itemchanged;//String	ls_GetText
//
//ls_GetText = This.GetText()
boolean lb_showTime


Choose Case this.GetColumnName()
//Case "production_ind"
//	If ls_GetText = 'P' Then
//		This.SetItem(1, "uom_ind", "B")
//		This.Modify("uom_ind.Protect = 1")
//	Else
//		This.Modify("uom_ind.Protect = 0")
//	End if
Case "shift"
	Parent.wf_get_end_of_shift()
Case "begin_date"
	Parent.wf_get_end_of_shift()
Case "production_ind"
	
	lb_showTime = (data = 'T')
	this.Object.from_time.visible = lb_showTime
	this.Object.to_time.visible = lb_showTime
	this.Object.t_2.visible = lb_showTime
	this.Object.t_3.visible = lb_showTime
	if(not lb_showTime) then
		this.SetItem(1, "from_time", "")
		this.SetItem(1, "to_time", "")
		this.Object.shift_t.Text = "Shift"
	else
		this.Object.shift_t.Text = "PA Shift"
	end if
	
	If data = 'C' Then
		dw_compare_retrieve.Visible = True
		dw_compare_retrieve.SetItem(1, "yields", "Y")
		dw_compare_retrieve.SetITem(1, "pa_actual", "Y")
		dw_compare_retrieve.SetItem(1, "pa_projection", "N")
		dw_compare_retrieve.SetItem(1, "material_handling", "N")
	Else
		dw_compare_retrieve.Visible = False
	End If
	
End Choose

		
end event

type dw_prod_ind from u_base_dw_ext within w_pas_manual_production_inq
integer x = 32
integer y = 1068
integer width = 448
integer height = 400
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_product_choice"
boolean border = false
end type

event constructor;call super::constructor;this.InsertRow(0)
end event

event itemchanged;call super::itemchanged;boolean lb_showArea, lb_showGroup

lb_showArea = (data = 'A')
lb_showGroup = (data = 'G')
dw_area.visible = lb_showArea
uo_prod_group.visible = lb_showGroup

//If ls_choice = 'N' Then
//	dw_sched_date.Enabled = True
//	dw_sched_date.visible = True
//	dw_shift.Enabled = True
//	dw_shift.visible = True
//Else
//	dw_sched_date.Enabled = False
//	dw_sched_date.visible = False
//	dw_shift.Enabled = False
//	dw_shift.visible = False
//End If
end event

type dw_area from u_sched_area_sect within w_pas_manual_production_inq
integer x = 590
integer y = 1084
integer width = 1038
integer height = 84
integer taborder = 40
boolean bringtotop = true
end type

event constructor;call super::constructor;this.InsertRow(0)
end event

type uo_prod_group from u_pa_prod_grp within w_pas_manual_production_inq
integer x = 453
integer y = 1180
integer height = 548
integer taborder = 50
boolean bringtotop = true
end type

on uo_prod_group.destroy
call u_pa_prod_grp::destroy
end on

type dw_compare_retrieve from u_base_dw_ext within w_pas_manual_production_inq
boolean visible = false
integer x = 37
integer y = 864
integer width = 1682
integer height = 192
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_manual_production_retrieve"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

