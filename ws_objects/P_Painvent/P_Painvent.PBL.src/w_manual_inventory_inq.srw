﻿$PBExportHeader$w_manual_inventory_inq.srw
forward
global type w_manual_inventory_inq from w_base_response_ext
end type
type dw_pas_plant from u_plant within w_manual_inventory_inq
end type
type dw_date_and_state from datawindow within w_manual_inventory_inq
end type
type dw_fab_product_code from u_fab_product_code within w_manual_inventory_inq
end type
end forward

global type w_manual_inventory_inq from w_base_response_ext
integer x = 421
integer y = 648
integer width = 2030
integer height = 640
string title = "Manual Inventory Inquire"
boolean controlmenu = false
long backcolor = 67108864
dw_pas_plant dw_pas_plant
dw_date_and_state dw_date_and_state
dw_fab_product_code dw_fab_product_code
end type
global w_manual_inventory_inq w_manual_inventory_inq

type variables
u_pas201 iu_pas201
end variables

event ue_postopen;call super::ue_postopen;dw_fab_product_code.uf_enable_status(false)
dw_fab_product_code.uf_set_product_status('G')

If Not iw_frame.iu_string.nf_IsEmpty(dw_fab_product_code.uf_get_product_code( )) Then
	dw_fab_product_code.SetFocus()
	dw_fab_product_code.PostEvent(GetFocus!)
End if
end event

event open;call super::open;String ls_message_StringParm,&
		 ls_plant,&
		 ls_plant_desc,&
		 ls_product_code,&
		 ls_product_desc,&
		 ls_product_state,&
		 ls_fresh_frozen, &
 		 ls_next_on_save,&
		 ls_show_change, &
		 ls_temp

Int	li_pos

Window	lw_parent


ls_message_StringParm  = Message.StringParm

lw_parent = This.ParentWindow()
If IsValid(lw_parent) Then
	This.X = lw_parent.X + (lw_parent.Width / 2) - (This.Width / 2)
	This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)
End if

ls_plant = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")
ls_plant_desc = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")
ls_product_code = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")
ls_product_desc = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")
ls_temp = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")
ls_fresh_frozen = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")
ls_next_on_save = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")
ls_show_change = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")
ls_product_state = iw_frame.iu_string.nf_gettoken(ls_message_StringParm, "~t")


dw_date_and_state.InsertRow(0)
dw_Date_and_State.SetItem( 1, "inq_date", Today())

IF Len( Trim(ls_fresh_frozen)) > 0 Then
	dw_fab_product_code.uf_set_product_code(ls_product_code,ls_product_desc)
END IF

IF Len( Trim(ls_fresh_frozen)) > 0 Then
	dw_fab_product_code.uf_set_product_state(ls_product_state)
END IF

IF Len( Trim(ls_fresh_frozen)) > 0 Then
	dw_date_and_state.SetItem( 1, "fresh_frozen_ind", ls_fresh_frozen)
ELSE
	dw_date_and_state.SetItem( 1, "fresh_frozen_ind", 'Z')
END IF

If Len( Trim(ls_show_change)) > 0 Then
	dw_date_and_state.SetItem( 1, "total_change_message", ls_show_change)
Else
	dw_date_and_state.SetItem( 1, "total_change_message", 'N')
End If

If Len( Trim(ls_next_on_save)) > 0 Then
	dw_date_and_state.SetItem( 1, "next_on_save", ls_show_change)
Else
	dw_date_and_state.SetItem( 1, "next_on_save", 'N')
End If

If dw_pas_plant.RowCount() < 1 Then dw_pas_plant.InsertRow(0)

IF LEN( Trim(Ls_Plant)) > 0 THEN
	dw_pas_plant.uf_set_plant_code(ls_plant)
   dw_pas_plant.AcceptText()


Else
	dw_pas_plant.SetFocus()
END IF

end event

on close;call w_base_response_ext::close;DESTROY( iu_pas201)
end on

on w_manual_inventory_inq.create
int iCurrent
call super::create
this.dw_pas_plant=create dw_pas_plant
this.dw_date_and_state=create dw_date_and_state
this.dw_fab_product_code=create dw_fab_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_pas_plant
this.Control[iCurrent+2]=this.dw_date_and_state
this.Control[iCurrent+3]=this.dw_fab_product_code
end on

on w_manual_inventory_inq.destroy
call super::destroy
destroy(this.dw_pas_plant)
destroy(this.dw_date_and_state)
destroy(this.dw_fab_product_code)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"Cancel")
end event

event ue_base_ok;call super::ue_base_ok;String ls_return_String,&
		 Ls_Temp_String
 

If dw_pas_plant.AcceptText() = -1 Then 
	dw_pas_plant.SetFocus()
	return
End if

Ls_Temp_String = dw_pas_plant.GetItemString( 1, "location_Code") 
IF iw_frame.iu_string.nf_isEmpty(ls_Temp_String) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_pas_plant.SetFocus()
	dw_pas_plant.SetColumn( "location_Code")
	Return
END IF

If not dw_fab_product_code.uf_validate( ) Then 
	return
End if

ls_Return_String = dw_pas_plant.Describe( "DataWindow.Data")+"~t"
ls_Return_String += dw_fab_product_code.uf_get_product_code( ) + "~t"
ls_Return_String += dw_fab_product_code.uf_get_product_desc( ) + "~t"
ls_return_String += dw_date_and_state.Describe( "DataWindow.Data") + "~t"
ls_Return_String += dw_fab_product_code.uf_get_product_state( )

CloseWithReturn(This, ls_return_String)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_manual_inventory_inq
integer x = 1701
integer y = 276
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_manual_inventory_inq
integer x = 1701
integer y = 152
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_manual_inventory_inq
integer x = 1701
integer y = 28
integer taborder = 40
end type

type dw_pas_plant from u_plant within w_manual_inventory_inq
integer x = 178
integer y = 28
integer width = 1435
integer taborder = 10
end type

type dw_date_and_state from datawindow within w_manual_inventory_inq
integer x = 37
integer y = 288
integer width = 1467
integer height = 236
integer taborder = 30
string dataobject = "d_manual_inventory_date_and_state"
boolean border = false
boolean livescroll = true
end type

type dw_fab_product_code from u_fab_product_code within w_manual_inventory_inq
integer y = 96
integer width = 1499
integer height = 195
integer taborder = 20
boolean bringtotop = true
end type

