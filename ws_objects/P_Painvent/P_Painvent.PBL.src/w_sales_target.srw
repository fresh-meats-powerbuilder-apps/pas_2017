﻿$PBExportHeader$w_sales_target.srw
$PBExportComments$Not used any more
forward
global type w_sales_target from w_netwise_sheet
end type
type dw_sales_target from u_base_dw within w_sales_target
end type
end forward

global type w_sales_target from w_netwise_sheet
integer x = 0
integer y = 0
integer width = 2816
integer height = 1404
string title = "Sales Target"
long backcolor = 67108864
dw_sales_target dw_sales_target
end type
global w_sales_target w_sales_target

type variables
Boolean			ib_m_print_Status, &
					ib_inquire
					
String			is_plant_type

U_pas203			iu_pas203

u_ws_pas3			iu_ws_pas3

s_error			istr_error_info


end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();Int			li_ret

String		ls_division, &
				ls_target_string

Long			ll_rec_count

Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return false

OpenWithParm(w_plant_type_inq, This)

If not ib_inquire then return false

dw_sales_target.Reset()

istr_error_info.se_event_name = "wf_retrieve"
SetPointer(HourGlass!)
dw_sales_target.SetRedraw(false)

//If Not iu_pas203.nf_pasp02dr_inq_sales_targets(istr_error_info, &
//										is_plant_type, &
//										ls_target_string) = 0 Then Return False

If Not iu_ws_pas3.uf_pasp02hr(istr_error_info, &
										is_plant_type, &
										ls_target_string) = 0 Then Return False

//ls_target_string = &
//		"004" + "~t" + "1" + "~t" + "99" + "~t" + &
//							"2" + "~t" + "99" + "~t" + & 
//							"3" + "~t" + "99" + "~t" + & 
//							"4" + "~t" + "99" + "~t" + & 
//							"5" + "~t" + "99" + "~t" + & 
//							"6" + "~t" + "99" + "~t" + & 
//							"7" + "~t" + "99" + "~r~n" + & 
//		"018" + "~t" + "99999" + "~t" + "99" + "~t" + &
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~r~n" + & 
//		"026" + "~t" + "99999" + "~t" + "99" + "~t" + &
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~t" + & 
//							"99999" + "~t" + "99" + "~r~n" 
//
If Not iw_frame.iu_string.nf_IsEmpty(ls_target_string) Then
	ll_rec_count = dw_sales_target.importstring(ls_target_string)
	If ll_rec_count > 0 then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

dw_sales_target.ResetUpdate()
dw_sales_target.SetRedraw(true) 
dw_sales_target.SetFocus()

Return True
end function

public function boolean wf_update ();int				li_count, &
					li_col

long				ll_Row, &
					ll_NbrRows, &
					ll_col_count
					

string			ls_update_string, &
					ls_division, &
					ls_tab, &
					ls_col_name, &
					ls_day_of_week, &
					ls_uom_code
					
dwitemstatus	lis_status
					
ls_tab = "~t"

IF dw_sales_target.AcceptText() = -1 THEN Return( False )

IF dw_sales_target.ModifiedCount() <= 0 THEN 
	iw_frame.setmicrohelp("No update necessary")
	Return( False )
end if

this.setredraw(False)

ll_NbrRows = dw_sales_target.ModifiedCount()
For li_count = 1 To ll_NbrRows 
	ll_Row = dw_sales_target.GetNextModified(ll_Row, Primary!) 
	ll_col_count = Long(dw_sales_target.object.datawindow.column.count)
	for li_col = 1 to ll_col_count
		lis_status = dw_sales_target.getitemstatus( ll_row, li_col, Primary!)
		if lis_status = datamodified! then
			
			ls_col_name = dw_sales_target.describe("#" + String(li_col) + ".Name")
			ls_uom_code = Upper(left(ls_col_name, 3))
			if ls_uom_code = "BOX" or ls_uom_code = "COM" or ls_uom_code = "WGT" Then
				ls_day_of_week = Right(ls_col_name, 1)
				ls_update_string += dw_sales_target.GetItemString(ll_row, &
													"plant_code") + ls_tab + &
									ls_day_of_week + ls_tab + &
									ls_uom_code + ls_tab + &
									String(dw_sales_target.GetItemDecimal &
									(ll_row,li_col)) + "~r~n"
			End If 
		end if	
	Next				
Next

//IF iu_pas203.nf_pasp03dr_upd_sales_targets(istr_error_info, ls_update_string) THEN
IF iu_ws_pas3.uf_pasp03hr(istr_error_info, ls_update_string) THEN
	dw_sales_target.ResetUpdate()
	SetMicroHelp("Modification Successful")
Else 	
	SetMicroHelp("Modification Unsuccessful")
END IF

this.setredraw(True)

Return( True )
end function

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

end event

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name		= "Salestargt"
istr_error_info.se_user_id				= sqlca.userid

//is_inquire_window_name = 'w_plant_type_inq'

iu_pas203 = Create u_pas203
iu_ws_pas3 = Create u_ws_pas3

wf_retrieve()
end event

on w_sales_target.create
int iCurrent
call super::create
this.dw_sales_target=create dw_sales_target
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sales_target
end on

on w_sales_target.destroy
call super::destroy
destroy(this.dw_sales_target)
end on

event resize;call super::resize;dw_sales_target.Resize( This.Width - dw_sales_target.X - 75, This.Height - dw_sales_target.y - 125)
end event

event ue_fileprint;call super::ue_fileprint;dw_sales_target.print()


end event

event ue_get_data(string as_value);call super::ue_get_data;Choose Case as_value
	Case 'plant_type'
		message.StringParm = is_plant_type
	Case 'title'
		message.StringParm = This.title
End Choose
end event

event ue_set_data(string as_data_item, string as_value);call super::ue_set_data;u_conversion_functions		lu_conv


Choose Case as_data_item
	Case 'plant_type'
		is_plant_type = as_value
	Case 'ib_inquire'
		ib_inquire = lu_conv.nf_Boolean(as_value)
End Choose
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')

iw_frame.im_menu.mf_Disable('m_deleterow')

end event

type dw_sales_target from u_base_dw within w_sales_target
integer y = 24
integer width = 2697
integer height = 1256
integer taborder = 10
string dataobject = "d_sales_target"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event constructor;call super::constructor;ib_updateable = True


end event

event itemfocuschanged;call super::itemfocuschanged;SelectText(1, Len(GetText())+ 10)
end event

event getfocus;call super::getfocus;SelectText(1, Len(GetText())+ 10)
end event

event itemchanged;call super::itemchanged;choose case Upper(Left(dwo.name, 3) )
	Case "BOX", "COM"
		IF double(data) < 0 then
			Return 1
		end IF
End Choose
iw_frame.SetMicroHelp("Ready")

end event

event itemerror;call super::itemerror;iw_frame.SetMicroHelp("Targets must be numeric")
Return 1
end event

