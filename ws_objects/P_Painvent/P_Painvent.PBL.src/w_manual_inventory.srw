﻿$PBExportHeader$w_manual_inventory.srw
forward
global type w_manual_inventory from w_base_sheet_ext
end type
type dw_manage_inventory_detail_fresh from u_base_dw_ext within w_manual_inventory
end type
type uo_1 from u_spin_big within w_manual_inventory
end type
type st_2 from statictext within w_manual_inventory
end type
type st_1 from statictext within w_manual_inventory
end type
type gb_1 from groupbox within w_manual_inventory
end type
type dw_manage_inventory_header from u_base_dw_ext within w_manual_inventory
end type
end forward

global type w_manual_inventory from w_base_sheet_ext
integer x = 0
integer y = 0
integer width = 2545
integer height = 1610
string title = "Manual Inventory"
long backcolor = 67108864
dw_manage_inventory_detail_fresh dw_manage_inventory_detail_fresh
uo_1 uo_1
st_2 st_2
st_1 st_1
gb_1 gb_1
dw_manage_inventory_header dw_manage_inventory_header
end type
global w_manual_inventory w_manual_inventory

type variables
u_pas202 iu_pas202
u_ws_pas1	iu_ws_pas1

s_error istr_Error_Info

String  is_Format_Date_String

Char ic_fetch_direction

Boolean		ib_good_product,&
		ib_plant_live, &
		ib_no_inquire

Long		il_orig_qty

INTEGER ii_PostOpen_Retry_Count
// pjm 09/12/2014 - addded to allow for negative rows
Boolean ib_allow_negatives

end variables

forward prototypes
public function boolean wf_next ()
public function boolean wf_previous ()
public subroutine wf_delete ()
public subroutine wf_filenew ()
public function boolean wf_addrow ()
public function boolean wf_update ()
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_next ();iw_Frame.SetMicroHelp("Inquiring on Next Product ...")
// F means page forward
ic_fetch_direction = 'F'

This.PostEvent("ue_query")

Return True
end function

public function boolean wf_previous ();iw_Frame.SetMicroHelp("Inquiring on Previous Product ...")
// b means page backwards
ic_fetch_direction = 'B'


This.PostEvent("ue_query")

Return TRUE
end function

public subroutine wf_delete ();wf_deleteRow()
end subroutine

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_addrow ();Int li_Days_Delay,&
	 li_row
	 
Long	ll_row_count, &
		ll_wgt, ll_qty, &
	 ll_total_wgt, ll_total_qty


String ls_days_Delay,&
		 ls_zero_str,&
		 ls_Find_String,&
		 ls_plant
		 
nvuo_pa_business_rules 	nvuo_pa		 

//removed days delay ibdkdld
//If dw_manage_inventory_detail_fresh.RowCount() < 1 Then
//	ls_Days_Delay = '0001'
//Else
//	ls_Days_Delay = dw_manage_inventory_detail_fresh.GetItemString( dw_manage_inventory_detail_fresh.RowCount(), "age_code")
//	li_Days_Delay = Integer( ls_Days_Delay)
//	li_days_delay++
//	ls_Zero_str = Fill("0", 4 - Len(String(li_days_Delay)))
//	ls_days_delay = ls_Zero_str + String( li_days_delay)
//	ls_Find_String = "age_code = '" +ls_Days_Delay+"'"
//	li_row = dw_manage_inventory_detail_fresh.Find( ls_Find_String, 1,dw_manage_inventory_detail_fresh.RowCount() )
//
//	DO while li_Row > 0 
//		li_days_delay++
//		ls_Zero_str = Fill("0", 4 - Len(String(li_days_Delay)))
//		ls_days_delay = ls_Zero_str + String( li_days_delay)
//		ls_Find_String = "age_code = '" +ls_Days_Delay+"'"
//		li_row = dw_manage_inventory_detail_fresh.Find( ls_Find_String, 1,dw_manage_inventory_detail_fresh.RowCount())
//	Loop
//End if
dw_manage_inventory_detail_fresh.InsertRow(0)
ll_row_count = dw_manage_inventory_detail_fresh.RowCount()
//ibdkdld added production plant made age code a date
ls_plant = dw_manage_inventory_Header.GetItemString( 1, "plant")
dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "production_plant",ls_plant)
If ib_plant_live Then //s_nvuo_pa.uf_check_pasldtyp(ls_plant) Then
	dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "protect_plant", 'L')
End If
//dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "age_code", ls_days_Delay)
dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "age_code", String(RelativeDate(Today(),-1),'mm/dd/yyyy'))
dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "quantity", 0)
dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "avail_to_ship", RelativeDate(Today(),-1))
dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "update_flag", ' ')
//sr 8171
dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "inventory_wgt", 0)
dw_manage_inventory_detail_fresh.SetItem( ll_row_count, "product_status", 'G')

dw_Manage_Inventory_Detail_Fresh.ScrollToRow( ll_row_count)
dw_Manage_Inventory_Detail_Fresh.uf_ChangeRowStatus(ll_row_count, New!)
dw_manage_Inventory_detail_fresh.SetColumn("age_code")

//sr 8171
ll_row_count = dw_manage_inventory_detail_fresh.RowCount()
ll_total_qty = 0
ll_total_wgt = 0
FOR li_row = 1 TO ll_Row_count - 1
	ll_qty = dw_manage_Inventory_detail_fresh.GetItemNumber(li_row, "quantity")
	ll_wgt = dw_manage_inventory_detail_fresh.GetItemNumber(li_row, "inventory_wgt")
	ll_total_qty = ll_total_qty + ll_qty
	ll_total_wgt = ll_total_wgt + ll_wgt
Next
if ll_total_qty > 0 Then
	dw_manage_inventory_detail_fresh.SetItem(ll_row_count, "prev_avg_wgt", ll_total_wgt/ll_total_qty )
Else
	dw_manage_inventory_detail_fresh.SetItem(ll_row_count, "prev_avg_wgt", 0 )	
end if

Return TRUE
end function

public function boolean wf_update ();Boolean  lb_ok,lb_error

Int		li_colnbr,li_row_count,li_loop, &
			li_check			
			
Long		ll_row, &
			ll_row_count,ll_find,ll_len, &
			ll_ModifiedRow, &
			ll_qty, ll_wgt, &
			ll_prod_avg_wgt, & 
		 	ll_sales_avg_wgt, &
			ll_avg_wgt, ll_prev_avg_wgt

String ls_Header_String,&
		 ls_detail_String, &
			ls_colname, &
			ls_old_mask,ls_old_mask1	, &
			ls_frz_ind,ls_mod,ls_prod_plant, &
			ls_find_string,ls_avail_date,ls_age_code, ls_ind, &
			ls_product_status, &
			ls_hold_code, &
			ls_hold_plant, &
			ls_temp_status, &
			ls_find_status_string
			
Date		ldt_prod_date,&
			ldt_avail_date
			
dwItemStatus l_status
			


If dw_manage_inventory_detail_fresh.AcceptText() = -1 Then return False
If dw_manage_inventory_header.AcceptText() = -1 Then return False


// validate here
ll_row_count = dw_manage_inventory_detail_fresh.RowCount()

ll_row = dw_manage_inventory_detail_fresh.Find("avail_to_ship < Date('" + &
				String(relativeDate(Today(), -1)) + "') And update_flag = 'A'", 1, &
				ll_row_count)
If ll_row > 0 Then
	MessageBox("Available To Ship", "Date must be greater than or equal to " + &
					String(RelativeDate(Today(), -1), "mm/dd/yyyy")+".")
	dw_manage_inventory_detail_fresh.SetRow(ll_row)
	dw_manage_inventory_detail_fresh.ScrollToRow(ll_row)
	dw_manage_inventory_detail_fresh.SetColumn("avail_to_ship")
	dw_manage_inventory_detail_fresh.SetFocus()
	return false
End if
// ibdkdld Beg

FOR ll_row = 1 TO ll_row_count
	If dw_manage_inventory_header.GetItemString(1,"fresh_frozen_indicator") <> 'Z' Then
		Exit
	Else	
		ls_age_code = dw_manage_inventory_detail_fresh.GetItemString(ll_row,'age_code')
		ll_len = len(ls_age_code)
		If ll_len < 10 Then
			SetMicroHelp("The Production Date is not valid")
			dw_manage_inventory_detail_fresh.ScrollToRow(ll_row)
			dw_manage_inventory_detail_fresh.SetColumn("age_code")
			dw_manage_inventory_detail_fresh.SetFocus()
			lb_error = True
			Exit
		Else				
			ldt_prod_date = Date(ls_age_code)
			l_status	= dw_manage_inventory_detail_fresh.GetItemStatus ( ll_row, 0,Primary! ) 
			If l_status = NewModified! Then
				ldt_avail_date = dw_manage_inventory_detail_fresh.GetItemDate(ll_row,'avail_to_ship')
				If ldt_prod_date > ldt_avail_date Then
				//	dw_manage_inventory_detail_fresh.SetRow(ll_row)
					dw_manage_inventory_detail_fresh.ScrollToRow(ll_row)
					dw_manage_inventory_detail_fresh.SetColumn("age_code")
					dw_manage_inventory_detail_fresh.SetFocus()
					SetMicroHelp("The Production Date can not be greater than the Available to Ship Date")
					lb_error = True
					Exit
				Else 
					ls_prod_plant = dw_manage_inventory_detail_fresh.GetItemString(ll_row,'production_plant')
					ls_avail_date = String(ldt_avail_date)
					//dmk sr8171, add product status to find string and error messages
					ls_product_status = dw_manage_inventory_detail_fresh.GetItemString(ll_row,'product_status')
					// if only one row, can't be duplicate
					If ll_Row_Count < 2 Then 
						//can't be dup
					Else
						//dmk sr8171 add product status to string
						ls_Find_String = 	"age_Code = '"+ ls_age_code +&
													"' and avail_to_ship = date('" + ls_avail_date+ "')" +& 
													" and production_plant = '" + ls_prod_plant + "'" +&
												  	" and product_status = '" + ls_product_status + "'"
						ll_find = dw_manage_inventory_detail_fresh.Find( ls_find_String, 1, ll_row - 1 )
						CHOOSE CASE ll_row
							CASE 1
								ll_find = dw_manage_inventory_detail_fresh.Find( ls_find_String, 2, ll_row_count )
									IF ll_find > 0 Then
										MessageBox("Duplicate Row", "Production Date, Avail Ship Dates, Production Plant and Product Status must be a unique combination.")
										dw_manage_inventory_detail_fresh.ScrollToRow ( ll_row )						
										dw_manage_inventory_detail_fresh.SetColumn("age_code")
										dw_manage_inventory_detail_fresh.SelectText(1, 100)
										Return False
									END IF
							CASE 2 to ll_row_count - 1
								ll_find = dw_manage_inventory_detail_fresh.Find( ls_find_String, 1, ll_row -1 )
								IF ll_find > 0 Then
									MessageBox("Duplicate Row", "Production Date, Avail Ship Dates, Production Plant and Product Status must be a unique combination." )//+ ls_find_string)
									dw_manage_inventory_detail_fresh.ScrollToRow ( ll_row )						
									dw_manage_inventory_detail_fresh.SetColumn("age_code")
									dw_manage_inventory_detail_fresh.SelectText(1, 100)
									Return False
								ELSE
									ll_find = dw_manage_inventory_detail_fresh.Find( ls_find_String, ll_row + 1, ll_row_count )
									IF ll_find > 0 Then
										MessageBox("Duplicate Row", "Production Date, Avail Ship Dates, Production Plant and Product Status must be a unique combination.")
										dw_manage_inventory_detail_fresh.ScrollToRow ( ll_row )						
										dw_manage_inventory_detail_fresh.SetColumn("age_code")
										dw_manage_inventory_detail_fresh.SelectText(1, 100)
										Return False
									END IF
								End If
							CASE ll_row_count
								ll_find = dw_manage_inventory_detail_fresh.Find( ls_find_String, 1, ll_row_count -1 )
									IF ll_find > 0 Then
										MessageBox("Duplicate Row", "Production Date, Avail Ship Dates, Production Plant and Product Status must be a unique combination.")
										dw_manage_inventory_detail_fresh.ScrollToRow ( ll_row )						
										dw_manage_inventory_detail_fresh.SetColumn("age_code")
										dw_manage_inventory_detail_fresh.SelectText(1, 100)
										Return False
									END IF
						END CHOOSE
					End If
				End If
			End If
		End If
	End if
NEXT
//end ibdkdld

If lb_error Then Return False

ll_row = dw_manage_inventory_detail_fresh.Find("avail_to_ship < Date('" + &
				String(relativeDate(Today(), -1)) + "') And update_flag = 'A'", 1, &
				ll_row_count)
If ll_row > 0 Then
	MessageBox("Available To Ship", "Date must be greater than or equal to " + &
					String(RelativeDate(Today(), -1), "mm/dd/yyyy")+".")
	dw_manage_inventory_detail_fresh.SetRow(ll_row)
	dw_manage_inventory_detail_fresh.ScrollToRow(ll_row)
	dw_manage_inventory_detail_fresh.SetColumn("avail_to_ship")
	dw_manage_inventory_detail_fresh.SetFocus()
	return false
End if
//dmk- March 2009 - check that the qty_lb_ind = 'B', signifying
// both fields have changed.  Check that both are negative or positive
ll_row = 0
Do
	ll_ModifiedRow = ll_row

	ll_ModifiedRow = dw_manage_inventory_detail_fresh.GetNextModified(ll_row, Primary!)
	ll_row = ll_ModifiedRow

	If ll_row > 0 Then
		ls_ind = dw_manage_inventory_detail_fresh.GetItemString(ll_row,'qty_lb_ind') 
		If dw_manage_inventory_detail_fresh.GetItemString(ll_row,'qty_lb_ind') <> 'B' Then
			If dw_manage_inventory_detail_fresh.GetItemString(ll_row,'qty_lb_ind') = 'Q' Then
				ll_prev_avg_wgt = dw_manage_inventory_detail_fresh.GetItemNumber(ll_row, "prev_avg_wgt")
				ll_qty = dw_manage_inventory_detail_fresh.GetItemNumber(ll_row, "quantity")
				ll_wgt = dw_manage_inventory_detail_fresh.GetItemNumber(ll_row, "inventory_wgt") 
				If ll_qty = 0 Then
					ll_avg_wgt = 0
				Else
					if ll_wgt = 0 and (ll_prev_avg_wgt = 0 or IsNull(ll_prev_avg_wgt)) Then
					
						ls_hold_code = dw_manage_inventory_header.GetItemString(1,'product_code')
						ls_hold_plant = dw_manage_inventory_detail_fresh.GetItemString(ll_row, 'production_plant')
						CONNECT USING SQLCA;	
						SELECT plt.production_avg_wt
								,plt.sales_avg_weight
  						INTO :ll_prod_avg_wgt 
		 				 	 ,:ll_sales_avg_wgt
  				//		FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
						FROM sku_plant plt
  						WHERE ( plt.sku_product_code = :ls_hold_code ) AND  
         					( plt.plant_code  = :ls_hold_plant)   ;
					
						If SQLCA.SQLCode = 00 Then
							If ll_prod_avg_wgt > 0 Then
								ll_avg_wgt = ll_prod_avg_wgt
							Else
								ll_avg_wgt = ll_sales_avg_wgt 
							End if	
						Else
							ll_avg_wgt = 0
						End If
					Else
						ll_avg_wgt = ll_prev_avg_wgt
					End If
				End If
				dw_manage_inventory_detail_fresh.SetItem(ll_row, "inventory_wgt", ll_qty * ll_avg_wgt)
			End if	
		End if
		ll_qty = dw_manage_inventory_detail_fresh.GetItemNumber(ll_row, "quantity")
		ll_wgt = dw_manage_inventory_detail_fresh.GetItemNumber(ll_row, "inventory_wgt") 

		If ((ll_qty < 0) And (ll_wgt > 0)) or &
		    ((ll_qty > 0) And (ll_wgt < 0)) Then
			SetMicroHelp("Quantity and Pounds must be both negative or both positive.")
			dw_manage_inventory_detail_fresh.SetRow(ll_row)
			dw_manage_inventory_detail_fresh.ScrollToRow(ll_row)
			dw_manage_inventory_detail_fresh.SetColumn("quantity")
			dw_manage_inventory_detail_fresh.SetFocus()
			return false
		End If
		If (ll_qty = 0) And (ll_wgt <> 0) Then
			SetMicroHelp("Pounds must be zero if quantity is zero.")
			dw_manage_inventory_detail_fresh.SetRow(ll_row)
			dw_manage_inventory_detail_fresh.ScrollToRow(ll_row)
			dw_manage_inventory_detail_fresh.SetColumn("quantity")
			dw_manage_inventory_detail_fresh.SetFocus()
			return false
		End If
	End If
Loop while ll_row > 0 

// If Any Values are less than zero, then none can be greater than zero, also vice versa
ll_row = dw_manage_inventory_detail_fresh.Find("quantity < 0", 1, ll_row_count)

If ll_row > 0 AND ib_allow_negatives = FALSE Then  // PJM changed 09/12/2014
	ls_temp_status = dw_manage_inventory_detail_fresh.GetItemString(ll_row, "product_status")
	ls_find_status_string = "quantity > 0 and product_status = " + "'" + ls_temp_status + "'"
	If dw_manage_inventory_detail_fresh.Find(ls_find_status_string, 1, ll_row_count) > 0 Then
		// error here
		MessageBox("Positive and Negative Quantity", "A quantity cannot be both " + &
					"greater than zero and less than zero for the same status.")
		dw_manage_inventory_detail_fresh.SetRow(ll_row)
		Return False
	End if
End if		

//Show message if user has checked the box and total has changed
Long ll_new_qty
If dw_manage_inventory_header.GetItemString(1, "total_change_message") = 'Y' Then 
	ll_new_qty = Long(dw_manage_inventory_detail_fresh.Describe("Evaluate('c_total_qty',1)"))
	If ll_new_qty <> il_orig_qty Then
		MessageBox("Total has changed","The quantity total has changed from " + &
			String(il_orig_qty) + " to " + String(ll_new_qty) + &
			".  Please check your changes.",exclamation!,ok!) 
		Return False
	End If
End If

ls_header_String = dw_manage_inventory_header.GetItemString( 1, "plant")+"~t"
ls_header_String += dw_manage_Inventory_Header.getItemString( 1, "product_code")+"~t"
ls_header_String += dw_manage_Inventory_Header.getItemString( 1, "product_state")+"~t"
ls_header_String += String(dw_manage_Inventory_Header.GetItemDate( 1, "inventory_date"),"yyyy-mm-dd")+"~t"
ls_frz_ind = dw_manage_inventory_Header.GetItemString( 1, "fresh_frozen_indicator")

ls_header_String += ls_frz_ind

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait...Updating database")
dw_manage_inventory_detail_fresh.SetRedraw( False)

dw_manage_inventory_detail_fresh.SetFilter( "update_flag = 'A' or update_flag = 'M' or update_flag = 'D'")
dw_manage_inventory_detail_fresh.Filter()

ls_old_mask = dw_manage_inventory_detail_fresh.Describe("avail_to_ship.EditMask.Mask")
dw_manage_inventory_detail_fresh.Modify("avail_to_ship.Edit.Format = 'yyyy-mm-dd'")
//IF not ls_frz_ind = 'F' Then
//	ls_old_mask1 = dw_manage_inventory_detail_fresh.Describe("age_code.EditMask.Mask")
//	dw_manage_inventory_detail_fresh.Modify("age_code.Edit.Format = 'yyyy-mm-dd'")
//End If

ls_detail_String = dw_manage_inventory_detail_fresh.Describe( "DataWindow.Data")

dw_manage_inventory_detail_fresh.Modify("avail_to_ship.EditMask.Mask = '" + ls_old_mask + "'")
//IF not ls_frz_ind = 'F' Then
//	dw_manage_inventory_detail_fresh.Modify("age_code.EditMask.Mask = '" + ls_old_mask + "'")
//End If

dw_manage_inventory_detail_fresh.SetFilter( "")
dw_manage_inventory_detail_fresh.Filter()
// added if ibdkdld
IF ls_frz_ind = 'F' Then 
	dw_manage_inventory_detail_fresh.Sort()
Else
// dmk sr8171, changed sort
//	dw_manage_inventory_detail_fresh.setsort('seq_date D')
	dw_manage_inventory_detail_fresh.SetSort("seq_date D, avail_to_ship_date D, production_plant D, product_status D")
	dw_manage_inventory_detail_fresh.Sort()
End If
dw_manage_inventory_detail_fresh.SetRedraw(True)

If Len(ls_detail_string) > 0 Then
//	lb_ok = iu_pas202.nf_upd_inventory( istr_error_info, ls_header_String, ls_detail_String)
	lb_ok = iu_ws_pas1.nf_pasp13fr( istr_error_info, ls_header_String, ls_detail_String)
Else
	// If no data, don't call RPC
	lb_ok = True
End if

IF lb_ok then 
	il_orig_qty = Long(dw_manage_inventory_detail_fresh.Describe("Evaluate('c_total_qty',1)"))
	If dw_manage_inventory_header.GetItemString(1, "next_on_save") = 'Y' and &
		Len(Trim(ic_fetch_direction)) = 0 Then
		wf_next()
	End if
	SetMicroHelp("Modify Successful")
	// added if ibdkdld
	IF ls_frz_ind = 'F' Then 
		dw_manage_inventory_detail_fresh.Sort()
	End If
	dw_manage_inventory_detail_fresh.ResetUpdate()
	dw_manage_inventory_detail_fresh.SetRedraw(True)
Else
	SetMicroHelp("Ready")
End if
//message.stringparm = 'I'
//wf_retrieve()
Return True


end function

public function boolean wf_retrieve ();Boolean lb_return
Date ld_date

Int	li_row_Count,li_loop
		

Long	ll_first_tab, &
		ll_second_tab, ll_rtn, &
		ll_row_count, ll_row, &
		ll_wgt, ll_qty
		

String ls_Header_in,&
		 ls_Header_out,&
		 ls_return_String,ls_temp,ls_inq,ls_type_desc,ls_plant

u_string_functions 	lu_string				



Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

// next or previous has not been chosen  
IF LEN( Trim(ic_fetch_direction) ) = 0  &
 		AND (ib_no_inquire = False) Then 

	ls_header_in = dw_manage_inventory_header.Describe("DataWindow.Data")

//	ls_header_in = dw_manage_inventory_Header.GetItemstring(1,"plant") + '~t'
//	ls_header_in += dw_manage_inventory_Header.GetItemstring(1,"plant_desc") + '~t'
//	ls_header_in += dw_manage_inventory_Header.GetItemstring(1,"Product_code") + '~t'
//	ls_header_in += dw_manage_inventory_Header.GetItemstring(1,"Product_desc") + '~t'
//	ls_header_in += string(dw_manage_inventory_Header.GetItemDate(1,"Inventory_date")) + '~t'
//	ls_header_in += dw_manage_inventory_Header.GetItemstring(1,"Fresh_frozen_indicator") + '~t'
//	ls_header_in += dw_manage_inventory_Header.GetItemstring(1,"next_on_save") + '~t'
//	ls_header_in += dw_manage_inventory_Header.GetItemstring(1,"total_change_message") + '~t'
//	ls_header_in += dw_manage_inventory_Header.GetItemstring(1,"product_state")

	OpenWithParm( w_manual_inventory_inq,ls_header_in, This)
	ls_Header_in = Message.StringParm
	IF Upper(ls_Header_in) = 'CANCEL' THEN
		RETURN FALSE
	ELSE
		dw_manage_inventory_header.Reset()
	 	li_Row_count = dw_manage_inventory_header.ImportString( ls_Header_in)
		dw_manage_inventory_detail_fresh.Reset()
	END IF
ELSE
	dw_manage_inventory_detail_fresh.Reset()
	ls_Header_out = Space(43)
END IF

ib_no_inquire = False 

ls_Header_in  = dw_manage_inventory_Header.GetItemString( 1, "plant") + "~t"
ls_Header_in += trim(dw_Manage_Inventory_Header.GetItemString( 1, "product_code")) + "~t"
ls_header_in += dw_manage_inventory_Header.GetItemstring(1,"product_state") + "~t"
ls_Header_in += String( dw_manage_Inventory_Header.GetItemDate( 1, "inventory_date"),"YYYY-MM-DD")+"~t"
ls_Header_in += dw_manage_Inventory_Header.GetItemString( 1, "fresh_frozen_indicator")+"~t" + ic_fetch_direction//+"~r~n"


IF dw_manage_Inventory_Header.GetItemString( 1, "fresh_frozen_indicator") = 'F' Then
	dw_manage_inventory_detail_fresh.SetSort("age_code A")
	dw_manage_inventory_detail_fresh.Sort( )
	dw_manage_inventory_detail_fresh.Modify("Age_code_t.Text='Age Code' Fresh_Frozen_text.Text = 'Age by Code:' ")
	iw_frame.im_menu.mf_disable('m_new')
	iw_frame.im_menu.mf_disable('m_addrow')
ELSE
// ibdkdld removed two lines
//	dw_manage_inventory_detail_fresh.SetSort("age_code D")
//dmk sr8171
	dw_manage_inventory_detail_fresh.SetSort("seq_date D, avail_to_ship_date D, production_plant D, product_status D")
	dw_manage_inventory_detail_fresh.Sort( )
	dw_manage_inventory_detail_fresh.Modify("Age_code_t.Text='Production Date' Fresh_Frozen_text.Text = 'Age by Day:' ")
	iw_frame.im_menu.mf_enable('m_new')
	iw_frame.im_menu.mf_enable('m_addrow')
END IF

//lb_return = iu_pas202.nf_inq_inventory( istr_error_info, ls_Header_in, ls_Header_out, ls_return_String)
lb_return = iu_ws_pas1.nf_pasp12fr( istr_error_info, ls_Header_in, ls_Header_out, ls_return_String)

dw_manage_inventory_detail_fresh.SetRedraw( False)

IF lb_Return  Then
	ib_good_product = True
	iw_frame.im_menu.mf_enable('m_save')
	//ibdkdld beg
	ls_temp = lu_string.nf_gettoken(ls_Return_String,"~t")
	ls_return_string = RightTrim ( ls_return_string )
	If ls_temp  = 'L' Then 
		ib_plant_live = True
	Else
		ib_plant_live = False
	End If
	li_Row_count = dw_manage_inventory_detail_fresh.ImportString( ls_Return_String)
	IF dw_manage_Inventory_Header.GetItemString( 1, "fresh_frozen_indicator") = 'Z' Then
		FOR li_loop = 1 TO li_Row_count
			ld_date = date(dw_manage_inventory_detail_fresh.getitemString(li_loop,'age_code'))
			dw_manage_inventory_detail_fresh.SetItem( li_loop, "seq_date", ld_date)
		NEXT
	End If
	//ibdkdld end
	dw_manage_Inventory_Detail_Fresh.ResetUpdate()
	SetMicroHelp("Inquire Successful")
Else
	ib_plant_live = False
	ib_good_product = False
	ll_rtn = iw_frame.im_menu.mf_disable('m_save')
	ll_rtn = iw_frame.im_menu.mf_disable('m_new')
	ll_rtn = iw_frame.im_menu.mf_disable('m_addrow')
END IF

IF LEN( Trim( ic_fetch_direction)) > 0 Then
	ll_first_tab = POS( ls_Header_out,"~t")
	ll_second_tab = Pos(ls_Header_out, "~t", ll_first_tab + 1)
	dw_manage_inventory_header.SetItem( 1, "product_code", Left( ls_Header_out, ll_first_tab - 1))
	dw_manage_inventory_header.SetItem( 1, "product_desc", Mid( ls_Header_out, ll_first_tab + 1, ll_second_tab - ll_first_tab - 1))
END IF

il_orig_qty = Long(dw_manage_inventory_detail_fresh.Describe("Evaluate('c_total_qty',1)"))
ic_fetch_direction = ' '
dw_manage_Inventory_detail_fresh.SetColumn("quantity")
dw_manage_inventory_detail_fresh.SetRedraw( True)
//sr8171   dmk set the prev_quantity field to be used later
li_Row_count = dw_manage_inventory_detail_fresh.RowCount()

ll_row = 1
If li_Row_count > 0 Then
	Do
		ll_qty = dw_manage_Inventory_detail_fresh.GetItemNumber(ll_row, "quantity")
		ll_wgt = dw_manage_inventory_detail_fresh.GetItemNumber(ll_row, "inventory_wgt")
		if ll_qty > 0 Then
			dw_manage_inventory_detail_fresh.SetItem(ll_row, "prev_avg_wgt", ll_wgt/ll_qty )
		end if
		ll_row = ll_row + 1
	Loop While ll_row <= li_row_count
Else
	dw_manage_inventory_detail_fresh.SetItem(ll_row, "prev_avg_wgt", 0 )
End If

// pjm 09/12/2014 - addded to allow for negative rows
ls_plant = dw_manage_inventory_header.GetItemString( 1, "Plant")

  SELECT tutltypes.type_desc  
    INTO :ls_type_desc  
    FROM tutltypes  
   WHERE (tutltypes.type_code = :ls_plant)	AND 
         (tutltypes.record_type = 'PASLDTYP');

ib_allow_negatives = FALSE  // default
If sqlca.sqlcode = 0  and Not IsNull(ls_type_desc) Then
	If Upper(Mid(ls_type_desc,3,1)) = 'Y' Then
		ib_allow_negatives = TRUE
	End If
End If
	 
						
dw_manage_inventory_detail_fresh.ResetUpdate()
// removed ibdkdld
//dw_manage_inventory_detail_fresh.Sort()
dw_manage_inventory_detail_fresh.SetRedraw( True)
Return TRUE
end function

event activate;call super::activate;iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')

If dw_manage_Inventory_Header.RowCount() > 0 Then
	IF dw_manage_Inventory_Header.GetItemString( 1, "fresh_frozen_indicator") = 'F' Then
		iw_frame.im_menu.mf_disable('m_new')
		iw_frame.im_menu.mf_disable('m_addrow')
		//dw_manage_inventory_detail_fresh.SetMask(NumericMask!, "#####")
	Else
		iw_frame.im_menu.mf_enable('m_new')
		iw_frame.im_menu.mf_enable('m_addrow')
	End if
End if

If not ib_good_product Then
	iw_frame.im_menu.mf_disable('m_new')
	iw_frame.im_menu.mf_disable('m_addrow')
	iw_frame.im_menu.mf_disable('m_save')
End If
	
end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_addrow')

end on

event open;call super::open;String	ls_inquire
Long		ll_ret

iu_pas202 = Create u_pas202
iu_ws_pas1 = Create u_ws_pas1

dw_manage_inventory_header.ib_Updateable=False

ib_good_product = False



ls_inquire = Message.StringParm

If iw_frame.iu_string.nf_IsEmpty(ls_inquire) Then 
	ib_no_inquire = False
Else
	ib_no_inquire = True
	dw_manage_inventory_header.Reset()
	ll_ret = dw_manage_inventory_header.ImportString(ls_inquire)
End If
	

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_Retrieve()
end on

event ue_postopen;call super::ue_postopen;DataWindowChild	ldwc_SourcePlant, &
						ldwc_plant
integer		li_rc
String		ls_PARange,ls_group_id
nvuo_pa_business_rules 	nvuo_pa



ls_group_id = 'GROUP' + iw_frame.iu_netwise_data.is_groupid 
nvuo_pa.uf_check_user_group(ls_group_id)
dw_manage_inventory_detail_fresh.GetChild('production_plant', ldwc_plant)
ldwc_plant.SetTransObject(SQLCA)
if (sqlca.sqldbcode = 10005 or sqlca.sqldbcode = 10025) &
		and ii_PostOpen_Retry_Count < 2 then
		
	ii_PostOpen_Retry_Count += 1
	SQLCA.postevent("ue_reconnect")
	this.postevent("ue_postopen")
else
	ldwc_plant.Retrieve(ls_group_id)
	
	// Add a blank row at the end of the table
	ldwc_plant.InsertRow(0)


	istr_error_info.se_app_name 		= "Pas"
	istr_error_info.se_window_name 	= "Man Inv"
	istr_error_info.se_user_id 		= sqlca.userid

	This.PostEvent("ue_query")
end if
end event

on w_manual_inventory.create
int iCurrent
call super::create
this.dw_manage_inventory_detail_fresh=create dw_manage_inventory_detail_fresh
this.uo_1=create uo_1
this.st_2=create st_2
this.st_1=create st_1
this.gb_1=create gb_1
this.dw_manage_inventory_header=create dw_manage_inventory_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_manage_inventory_detail_fresh
this.Control[iCurrent+2]=this.uo_1
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.gb_1
this.Control[iCurrent+6]=this.dw_manage_inventory_header
end on

on w_manual_inventory.destroy
call super::destroy
destroy(this.dw_manage_inventory_detail_fresh)
destroy(this.uo_1)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.gb_1)
destroy(this.dw_manage_inventory_header)
end on

event ue_fileprint;call super::ue_fileprint;Long   JobHandle

String ls_PrintJob,&
		 ls_Data			


If not ib_print_ok Then return

ls_PrintJob = "Manual Inventory"
JobHandle = PrintOpen( ls_PrintJob)
ls_Data = "Plant "+ dw_manage_inventory_header.GetItemString( 1, "Plant")+" "+dw_manage_inventory_header.GetItemString( 1, "plant_desc")+"~r~n"

ls_Data += "Product "+ dw_manage_inventory_header.GetItemString( 1, "product_code") &
			 +" "+ dw_manage_inventory_header.GetItemString( 1, "product_desc")+"~r~n"

ls_Data += "State "+ dw_manage_inventory_header.GetItemString( 1, "product_state") + "~r~n"
			 
ls_Data += "Inventory Date "+String(Today())+"~r~n"

IF dw_manage_inventory_header.GetItemString( 1, "fresh_frozen_indicator") = 'F' Then
	ls_Data += "Age by Code:~r~n"
	ls_DATA += "Age~tQuantity~tAvail to ship~r~n"
ELSE
	ls_Data += "Age by Day:~r~n"
		ls_DATA += "Days Delay~tQuantity~tAvail to ship~r~n"
END IF			 
ls_data+= dw_manage_inventory_detail_fresh.Describe( "DataWindow.Data")
//dw_manage_inventory_header.Print( JobHandle, 1, 1)
//dw_manage_inventory_detail_fresh.Print( JobHandle, 1, 1250)
Print( JobHandle, ls_Data)
PrintClose( JobHandle)
end event

event resize;call super::resize;////constant integer li_x		= 554
//constant integer li_y		= 450
integer li_y		= 450


if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

//dw_manage_inventory_detail_fresh.width	= newwidth - li_x
dw_manage_inventory_detail_fresh.width = 2450
dw_manage_inventory_detail_fresh.height	= newheight - li_y


end event

event close;call super::close;Destroy iu_ws_pas1
end event

type dw_manage_inventory_detail_fresh from u_base_dw_ext within w_manual_inventory
integer y = 448
integer width = 2414
integer height = 960
integer taborder = 10
string dataobject = "d_manual_inventory_detail_fresh"
boolean vscrollbar = true
boolean border = false
end type

event itemchanged;call super::itemchanged;Int li_row

Long	 ll_quantity_old,&
		 ll_quantity_new,&
		 ll_find, &
		 ll_row, &
		 ll_ModifiedRow, &
		 ll_ModifiedRowCount 
		 

String ls_Column_Name,&
		 ls_temp_string,&
		 ls_find_String, &
		 ls_update_flag, &
			ls_GetText
DataWindowChild 		ldwc_child
u_string_functions	u_string


li_row = This.GetRow()
ls_Column_Name = dwo.name //This.GetColumnName()
ls_GetText = This.GetText()

Choose Case ls_Column_Name
	Case 'production_plant'
		dw_manage_inventory_detail_fresh.GetChild("production_plant", ldwc_child)
		ll_find = ldwc_child.Find ( "location_code = '" + data + "'", 1, ldwc_child.RowCount() )
		If ll_find = 0 then
			iw_frame.SetMicroHelp("The Production Plant is not a valid plant")
			This.SelectText(1, 100)
			Return 1
		End If
	Case 'age_code'
		IF dw_manage_inventory_header.GetItemString(  1, "fresh_frozen_indicator") = 'Z' Then
			If len(data) = 10 Then
				IF IsDate ( data ) and date(data) > date('01/01/1900') Then
					If date(data) > Today()  Then
						This.SelectText(1,100)
						SetMicroHelp("The Production Date can not be greater than today")
						Return 1
					Else
						If u_string.nf_CountOccurrences(data,'/') > 2 Then
							This.SelectText(1,100)
							SetMicroHelp("The Production Date is not a valid date")
							Return 1
						End If
					End If
				Else
					This.SelectText(1,100)
					SetMicroHelp("The Production Date is not a valid date")
					Return 1
				End IF
			Else
				This.SelectText(1,100)
				SetMicroHelp("The Production Date must be a 10 character date")
				Return 1
			End IF
		End If
		dw_manage_inventory_detail_fresh.SetItem(row,'seq_date',date(data))
			
//			// removed no longer needed ibdkdld

//			// if only one row, can't be duplicate
//			If This.RowCount() < 2 Then return
//
//			ls_temp_string = Fill( "0", 4 - LEN( Trim(String( ls_GetText ))))			
//			ls_temp_String+= Trim(ls_GetText)
//			ls_Find_String = 	"age_Code = '"+ Ls_Temp_String
//			ls_find_String+=	"' and avail_to_ship = date('"
//			ls_Find_String+=	String(This.GetItemDate( li_row, "avail_to_ship"),"MM/DD/YY")+"')"
// 
//			IF This.Find( ls_find_String, 1, li_row -1 ) > 0 Then
//				MessageBox("Duplicate Row", "Days Delay and Avail Ship Dates must be a unique combination.")
//				This.SelectText(1, Len(ls_GetText))
//				Return 1
//			else
//				IF This.RowCount() <> li_row Then
//					IF This.Find( ls_find_String, li_row +1, This.RowCount() ) > 0 Then
//						MessageBox("Duplicate Row", "Days Delay and Avail Ship Dates must be a unique combination.")
//						This.SelectText(1, Len(ls_GetText))
//						Return 1
//					ELSE
//						This.SetItem( li_row, "age_code", ls_temp_String)
//						//This.SetText()
//						Return 2
//					END IF
//				ELSE	
//					This.SetItem( li_row, "age_code", ls_temp_String)
//					//This.SetText()
//					Return 2
//				END IF
//			END IF
//		END IF
	Case 'quantity'
		ll_quantity_Old = This.GetItemNumber( li_row, "quantity", Primary!, TRUE)

		ll_quantity_New = Long(ls_GetText)

		IF ll_quantity_Old = 0 Then
			IF ll_quantity_new <> 0 Then
				This.SetItem( li_row, "update_flag", 'A')
			END IF
		ELSE
			IF ll_quantity_new = 0 Then
				This.SetItem( li_row, "update_flag", 'D')
			ELSE
				If This.GetItemString(li_row, "update_flag") = 'A' Then
					// do nothing  - changing the  quantity on new line was setting flag to 'M' 
				Else	
					This.SetItem( li_row, "update_flag", 'M')
				End IF
			END IF
		END IF
	//dmk March 2009  Set the qty_lb_ind, 'B', both qty and lbs fields 
	// have changed, if ''P', pounds have changed, if 'Q', quantity has
	// changed.  wf_update will check that both have changed
		IF This.GetItemString(li_row, "qty_lb_ind") = 'B' Then
			//do nothing
		ELSE
			IF This.GetItemString(li_row, "qty_lb_ind") = 'P' THEN
				This.SetItem( li_row, "qty_lb_ind", 'B')
			ELSE
				This.SetItem( li_row, "qty_lb_ind", 'Q')
			END IF
		END IF
	//	END IF
		
	Case 'avail_to_ship'
			If Date(ls_GetText) < RelativeDate(Today(), -1) Then
				MessageBox("Available To Ship", "Date must be greater or equal to " + &
								String(RelativeDate(Today(), -1), "mm/dd/yyyy")+".")
				This.SelectText(1, Len(ls_GetText))
				Return 1
			End if
			
//dmk March 2009 - add logic for lbs field, only can change when
//sorted by production date.
	Case 'inventory_wgt'
		ls_update_flag = This.GetItemString( li_row, "update_flag", Primary!, TRUE)

		IF (ls_update_flag = 'A') or (ls_update_flag = 'D') or (ls_update_flag = 'M') Then
			// do nothing
		ELSE
			This.SetItem( li_row, "update_flag", 'M')
		END IF
		
		IF This.GetItemString(li_row, "qty_lb_ind") = 'B' Then
			//do nothing
		ELSE
			IF This.GetItemString(li_row, "qty_lb_ind") = 'Q' THEN
				This.SetItem( li_row, "qty_lb_ind", 'B')
			ELSE
				This.SetItem( li_row, "qty_lb_ind", 'P')
			END IF
  		END IF
//			// if only one row, can't be duplicate
//			If This.RowCount() < 2 Then return
//			ls_Find_String = 	"age_Code = '"+ This.GetItemString( li_Row,"age_Code")+&
//									"' and avail_to_ship = date('" + ls_GetText+"')"
// 
//			IF This.Find( ls_find_String, 1, li_row -1 ) > 0 Then
//				MessageBox("Duplicate Row", "Production Date and Avail Ship Dates must be a unique combination.")
//				This.SelectText(1, Len(ls_GetText))
//				Return 1
//			ELSE
//				IF This.RowCount() <> li_row Then
//					IF This.Find( ls_find_String, li_row -1, This.RowCount() ) > 0 Then	
//						MessageBox("Duplicate Row", "Production Date and Avail Ship Dates must be a unique combination.")
//						Return 1		
//					END IF
//				END IF
//			END IF

//dmk SR8171
	Case 'product_status'
//		If ls_GetText = 'G' or ls_GetText = 'R' Then
//			//do nothing
//		Else
//			MessageBox("Invalid Status", "Product status must be G or R.")
//		End If
		if (ls_getText = 'R') or (ls_getText = 'Q')  Then
			dw_manage_inventory_detail_fresh.SetItem(row,'avail_to_ship',date("12/31/2999"))
		end if	
	Case Else
		MessageBox("Error", "Programming Error.  Contact Programmer.")
END Choose

ll_row = 0
ll_ModifiedRowCount = 0
Do
	ll_ModifiedRow = dw_manage_inventory_detail_fresh.GetNextModified(ll_row, Primary!)
	If ll_ModifiedRow > 0 Then
		ll_ModifiedRowCount ++
		If ll_ModifiedRowCount >= 100	 Then
			MessageBox("Maximum Updates", "Maximum number of updated lines reached, click Save before proceeding")
		End If
	End If	
	ll_row = ll_ModifiedRow

Loop while ll_row > 0 


end event

event itemerror;call super::itemerror;Choose Case This.GetColumnName()
Case "quantity"
	If Not IsNumber(This.GetText()) Then
		iw_frame.SetMicroHelp("Quantity must be a number")
		Return 1
	End if
Case "inventory_wgt"
	If Not IsNumber(This.GetText()) Then
		iw_frame.SetMicroHelp("Pounds must be a number")
		Return 1
	End if	
Case Else
	 Return 1
End Choose

This.SelectText(1, Len(This.GetText()))
end event

on constructor;call u_base_dw_ext::constructor;// QA Didn't like that it did this.  If it will, then Cut & Paste should be made to work too
This.Modify("DataWindow.Selected.Mouse = No")
end on

event getfocus;call super::getfocus;This.SelectText(1,Len(This.GetText()))
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1,Len(This.GetText()))
end event

event doubleclicked;call super::doubleclicked;String ls_date,ls_col_name
str_parms	lstr_parms

// if frozen ind = frozen age_code will contain a date ibdkdld
ls_col_name = dwo.name
If ls_col_name = 'age_code' Then
	If This.GetItemStatus ( row, 0, Primary!  ) = New! OR This.GetItemStatus ( row, 0, Primary!  ) = NewModified!Then
		If dw_manage_inventory_header.GetItemString(1,"fresh_frozen_indicator") = 'Z' Then
			// Get the X and Y coordinates of the place that was clicked
			lstr_parms.integer_arg[1] = gw_base_Frame.PointerX() + gw_base_Frame.WorkSpaceX() - 50
			CHOOSE CASE	lstr_parms.integer_arg[1]
			CASE IS > 2253
				lstr_parms.integer_arg[1] = 2253
			CASE is < 113
				lstr_parms.integer_arg[1] = 113
			END CHOOSE
			
			lstr_parms.integer_arg[2] = gw_base_Frame.PointerY() + gw_base_Frame.WorkSpaceY() - 500
			CHOOSE CASE	lstr_parms.integer_arg[2]
			CASE IS > 1053
				lstr_parms.integer_arg[2] = 1053
			CASE is < 0
				lstr_parms.integer_arg[2] = 0
			END CHOOSE
			lstr_parms.date_arg[1] = Date(This.GetItemString(Row, ls_col_name ))
			OpenWithParm(w_Calendar, lstr_parms)
			// Get the return string (date) from the calendar window
			// If an empty string do not do anything
			ls_date = Message.StringParm
			If ls_date <> "" Then
				SetText(ls_Date)
			Else
				Return
			End If
		End If
	End IF
End If

end event

event ue_keydown;call super::ue_keydown;String 	ls_date,ls_col_name,ls_temp
Long 		ll_currentRow,ll_currentcolumn,ll_len
str_parms	lstr_parms

// Get the current row and column that has focus
ll_CurrentRow = This.GetRow()
ll_CurrentColumn = This.GetColumn()
// Get the current column name
ls_Col_Name = GetColumnName()

If	Keydown(KeyControl!) and Keydown(KeyDownArrow!) Then
	If ls_col_name = 'age_code' Then
		If This.GetItemStatus ( ll_CurrentRow, 0, Primary!  ) = New! OR This.GetItemStatus ( ll_CurrentRow, 0, Primary!  ) = NewModified!Then
		// if frozen ind = frozen age_code will contain a date ibdkdld
			If dw_manage_inventory_header.GetItemString(1,"fresh_frozen_indicator") = 'Z' Then
				// Get the X and Y coordinates of the place that was clicked
				lstr_parms.integer_arg[1] = gw_base_Frame.PointerX() + gw_base_Frame.WorkSpaceX() - 50
				CHOOSE CASE	lstr_parms.integer_arg[1]
					CASE IS > 2253
						lstr_parms.integer_arg[1] = 2253
					CASE is < 113
						lstr_parms.integer_arg[1] = 113
					END CHOOSE
					
					lstr_parms.integer_arg[2] = gw_base_Frame.PointerY() + gw_base_Frame.WorkSpaceY() - 500
					CHOOSE CASE	lstr_parms.integer_arg[2]
					CASE IS > 1053
						lstr_parms.integer_arg[2] = 1053
					CASE is < 0
						lstr_parms.integer_arg[2] = 0
				END CHOOSE
				lstr_parms.date_arg[1] = Date(This.GetItemString(ll_CurrentRow, ls_col_name ))
				OpenWithParm(w_Calendar, lstr_parms)
				// Get the return string (date) from the calendar window
				// If an empty string do not do anything
				ls_date = Message.StringParm
				If ls_date <> "" Then
					SetText(ls_Date)
				Else
					Return
				End If
			End If
		End If
	End IF
End If










//
//CASE	Keydown(KeyControl!) and Keydown(KeyDownArrow!)
//	IF This.Describe("DataWindow.ReadOnly") = 'yes' THEN RETURN 
//	
//	// Get the current row and column that has focus
//			ll_CurrentRow = This.GetRow()
//			ll_CurrentColumn = This.GetColumn()
//			
//	// Get the current column name
//			is_ColumnName = GetColumnName()
//
//	// Get the column type of the clicked field
//			ls_ColumnType = Lower(This.Describe("#" + String( &
//												ll_CurrentColumn) + ".ColType"))
//
//	// Get the X and Y coordinates of the place that was clicked
//		lstr_parms.integer_arg[1] = long(This.Describe(is_ColumnName + &
//									".X")) + iw_parent.X + 200
//	CHOOSE CASE	lstr_parms.integer_arg[1]
//		CASE IS > 2253
//			lstr_parms.integer_arg[1] = 2253
//		CASE is < 113
//			lstr_parms.integer_arg[1] = 113
//	END CHOOSE
//
//	lstr_parms.integer_arg[2] = long(This.Describe(is_ColumnName + &
//								".Y")) + iw_parent.Y - 400
//	CHOOSE CASE	lstr_parms.integer_arg[2]
//		CASE IS > 1053
//			lstr_parms.integer_arg[2] = 1053
//		CASE is < 0
//			lstr_parms.integer_arg[2] = 0
//	END CHOOSE
//	
//	IF ls_ColumnType = "date" or &
//		ls_ColumnType = "datetime" THEN
//			
//			Choose Case ls_ColumnType
//				Case "date"
//					lstr_parms.date_arg[1] = This.GetItemDate(ll_CurrentRow, &
//																	ll_CurrentColumn)
//					IF IsNull(lstr_parms.date_arg[1]) THEN
//						lstr_parms.date_arg[1] = Today()
//					END IF
//					OpenWithParm(w_Calendar, lstr_parms)
//					// Get the return string (date) from the calendar window
//					// If an empty string do not do anything
//					ls_date = Message.StringParm
//
//
//
//
//
end event

event editchanged;Long						ll_len
String 					ls_temp
//u_string_functions	lu_string
//Boolean					lb_numeric
//
//
dwItemStatus l_status

If dwo.name = 'age_code' Then
	If dw_manage_inventory_header.GetItemString(1,"fresh_frozen_indicator") = 'Z' Then
		ll_len = len(RightTrim (data))
		ls_temp = Right(data,1)
		IF Not IsNumber(ls_temp) and Not ls_temp = '/' Then 
			This.SelectText ( ll_len + 1 ,1)
			This.Setfocus()
		//	Beep (1)
		//	iw_frame.SetMicroHelp("The Production Date has to be a number or a forward slash")
		Else
			If ll_len = 2 or ll_len = 5 Then
				
//				l_status = this.getitemstatus(  row, 0, Primary! )
//				CHOOSE CASE l_status
//					CASE new!
//						messagebox('Row is new','')
//					CASE NewModified!	
//						messagebox('Row is NewModified','')
//				END CHOOSE
//				
//				This.setItemStatus ( row, 'age_code', Primary!, NotModified!	)
//				l_status = this.getitemstatus( row, 'age_code', Primary! )
//				CHOOSE CASE l_status
//					CASE datamodified!
//						messagebox('Column is datamodified','')
//					CASE NotModified!	
//						messagebox('Column is notmodified','')
//				END CHOOSE
//				
				This.SetItem ( Row, 'age_code', data + '/')
			 
//			 	This.setItemStatus ( row, 0, Primary!, NotModified!	)
//				l_status = this.getitemstatus(  row, 0, Primary! )
//				CHOOSE CASE l_status
//					CASE new!
//						messagebox('Row is new','')
//					CASE NewModified!	
//						messagebox('Row is NewModified','')
//				END CHOOSE
//				
//				This.setItemStatus ( row, 'age_code', Primary!, datamodified!	)
//				l_status = this.getitemstatus( row, 'age_code', Primary! )
//				CHOOSE CASE l_status
//					CASE datamodified!
//						messagebox('Column is datamodified','')
//					CASE NotModified!	
//						messagebox('Column is notmodified','')
//				END CHOOSE
//
//				
				This.SelectText ( ll_len + 2, 0 )
//			Else
//				l_status = this.getitemstatus(  row, 0, Primary! )
//				CHOOSE CASE l_status
//					CASE new!
//						messagebox('Row is new','')
//					CASE NewModified!	
//						messagebox('Row is NewModified','')
//				END CHOOSE
//				
//				l_status = this.getitemstatus( row, 'age_code', Primary! )
//				CHOOSE CASE l_status
//					CASE datamodified!
//						messagebox('Column is datamodified','')
//					CASE NotModified!	
//						messagebox('Column is notmodified','')
//				END CHOOSE
			End IF
		End If
	End If
End If	




























//ls_temp = Right(data,1)
//If Not lu_string.nf_IsEmpty(ls_temp) Then
//	CHOOSE CASE dwo.name
//		CASE 'chain_speed_1'
//			IF Not IsNumber(ls_temp) THEN
//				lb_numeric = True
//			End if
//		CASE 'chain_speed_2'
//			IF Not IsNumber(ls_temp) THEN
//				lb_numeric = True
//			End if
//		CASE 'chain_speed_3'
//			IF Not IsNumber(ls_temp) THEN
//				lb_numeric = True
//			End if
//		CASE 'chain_speed_4'
//			IF Not IsNumber(ls_temp) THEN
//				lb_numeric = True
//			End if
//		CASE 'chain_speed_5'
//			IF Not IsNumber(ls_temp) THEN
//				lb_numeric = True
//			End if
//		CASE 'chain_speed_6'
//			IF Not IsNumber(ls_temp) THEN
//				lb_numeric = True
//			End if
//		CASE 'chain_speed_7'
//			IF Not IsNumber(ls_temp) THEN
//				lb_numeric = True
//			End if
//		CASE ELSE
//			IF Not IsNumber(ls_temp) THEN
//				If Len(Data) > 1 Then
//					If lu_string.nf_IsEmpty(ls_temp) or ls_temp = ':' Then
//						//do no not
//					Else
//						wf_error_handling('','severe','duration','Must be numeric, spaces, or a Colon')
//						beep(1)
//						idw_active_dw.SelectText ( Len(data) , 1)
//						Return  1
//					End if
//				Else
//					If not lu_string.nf_IsEmpty(ls_temp) Then
//						If ls_temp = ':' Then 
//							wf_error_handling('','severe','duration','Must have a hour(number) before the colon')
//						Else	
//							wf_error_handling('','severe','duration','Must be numeric, spaces, or a colon')
//						End If
//						beep(1)
//						idw_active_dw.SelectText ( 1, 1)
//						Return  1
//				End if
//			End If
//		END IF
//	END CHOOSE
//End If
//
//IF lb_numeric then
//   wf_error_handling('','severe','duration','Must be numeric')
//	beep(1)
//	idw_active_dw.SelectText ( Len(data) , 1)
//	Return  1
//End IF
//
//Return 0
end event

event rowfocuschanging;//Long		ll_row, &
//			ll_row_count,ll_find
//
//String ls_Header_String,&
//		 ls_detail_String, &
//			ls_colname, &
//			ls_old_mask,&
//			ls_frz_ind,ls_mod,ls_prod_plant,ls_find_string,ls_avail_date
//Date		ldt_prod_date,&
//			ldt_avail_date
//			
//dwItemStatus l_status
//			
//
//
//l_status	= dw_manage_inventory_detail_fresh.GetItemStatus ( currentrow, 0,Primary! ) 
//If l_status = NewModified! Then
//	ldt_prod_date = Date(dw_manage_inventory_detail_fresh.GetItemString(currentrow,'age_code'))
//	ldt_avail_date = dw_manage_inventory_detail_fresh.GetItemDate(currentrow,'avail_to_ship')
//	If ldt_prod_date > ldt_avail_date Then
//	//	dw_manage_inventory_detail_fresh.SetRow(currentrow)
//		dw_manage_inventory_detail_fresh.ScrollToRow(currentrow)
//		dw_manage_inventory_detail_fresh.SetColumn("age_code")
//		dw_manage_inventory_detail_fresh.SetFocus()
//		dw_manage_inventory_detail_fresh.SelectText(1, 100)
//		SetMicroHelp("The Production Date can not be greater than the Avilable to Ship Date")
//		Return 1
//	Else 
//		ls_prod_plant = dw_manage_inventory_detail_fresh.GetItemString(currentrow,'production_plant')
//		ls_avail_date = String(ldt_avail_date)
//		ls_Find_String = 	"age_Code = '"+ String(ldt_prod_date)+&
//									"' and avail_to_ship = date('" + ls_avail_date+ "')" +& 
//									" and production_plant = '" + ls_prod_plant + "'"
//		ll_find = dw_manage_inventory_detail_fresh.Find( ls_find_String, 1, currentrow - 1 )
//		IF ll_find > 0 Then
//				MessageBox("Duplicate Row", "Production Date, Avail Ship Dates, and Production Plant must be a unique combination." )//+ ls_find_string)
//				dw_manage_inventory_detail_fresh.ScrollToRow ( currentrow )						
//				dw_manage_inventory_detail_fresh.SetColumn("age_code")
//				dw_manage_inventory_detail_fresh.SetFocus()
//				dw_manage_inventory_detail_fresh.SelectText(1, 100)
//				Return 1 //False
//		END IF
//	End If
//End If
//
Return 0
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_temp


This.GetChild('product_status', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve('PRODSTAT')
end event

type uo_1 from u_spin_big within w_manual_inventory
integer x = 2041
integer y = 96
integer width = 165
integer height = 227
end type

on uo_1.destroy
call u_spin_big::destroy
end on

type st_2 from statictext within w_manual_inventory
integer x = 1774
integer y = 138
integer width = 249
integer height = 61
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Next"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_manual_inventory
integer x = 1770
integer y = 237
integer width = 249
integer height = 77
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Previous"
alignment alignment = right!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_manual_inventory
integer x = 1741
integer y = 35
integer width = 494
integer height = 330
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Product"
end type

type dw_manage_inventory_header from u_base_dw_ext within w_manual_inventory
integer x = 4
integer y = 13
integer width = 1660
integer height = 394
integer taborder = 20
boolean enabled = false
string dataobject = "d_manual__inventory_header"
boolean border = false
end type

on constructor;call u_base_dw_ext::constructor;This.InsertRow(0)
end on

