$PBExportHeader$u_sched_area_sect.sru
forward
global type u_sched_area_sect from datawindow
end type
end forward

global type u_sched_area_sect from datawindow
integer width = 1019
integer height = 172
integer taborder = 10
string dataobject = "d_prd_area_sect"
boolean maxbox = true
boolean border = false
boolean livescroll = true
end type
global u_sched_area_sect u_sched_area_sect

type variables
String		is_area_string, &
				is_sect_string
				
u_pas201		iu_pas201

s_error		istr_error_info

DataWindowChild		idwc_area, &
							idwc_sect
end variables

forward prototypes
public function integer uf_enable (boolean ab_enable)
public function integer uf_modified ()
public function string uf_get_sect_code ()
public function string uf_get_area_code ()
public function string uf_get_sect_descr ()
public function string uf_get_area_descr ()
public function boolean uf_get_plt_codes (ref string as_plant)
end prototypes

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.shift.Background.Color = 16777215
	This.object.shift.Protect = 0
Else
	This.object.shift.Background.Color = 12632256
	This.object.shift.Protect = 1
End If

Return 1
end function

public function integer uf_modified ();return 0
end function

public function string uf_get_sect_code ();return This.GetItemString(1, "sect_name_code")
end function

public function string uf_get_area_code ();return This.GetItemString(1, "area_name_code")
end function

public function string uf_get_sect_descr ();Long			ll_rtn

String		ls_SearchString, &
				ls_temp

ls_Searchstring = "sect_name_code = "
ls_Searchstring += "'" + This.GetItemString(1, "sect_name_code") + "'"
								
ll_rtn = idwc_sect.Find  &
				( ls_SearchString, 1, idwc_sect.RowCount())
				
ls_temp = idwc_sect.GetItemString(ll_rtn, "type_descr")

Return ls_temp
end function

public function string uf_get_area_descr ();Long			ll_rtn

String		ls_SearchString, &
				ls_temp

ls_Searchstring = "name_code = "
ls_Searchstring += "'" + This.GetItemString(1, "area_name_code") + "'"
								
ll_rtn = idwc_area.Find  &
				( ls_SearchString, 1, idwc_area.RowCount())
				
ls_temp = idwc_area.GetItemString(ll_rtn, "type_descr")

Return ls_temp
end function

public function boolean uf_get_plt_codes (ref string as_plant);Integer		li_ret, &
				li_rtn
String		ls_input, &
				ls_area_string, &
				ls_sect_string
				

iu_pas201 = Create u_pas201
ls_area_string = ''
ls_sect_string = ''

ls_input = as_plant + '~r~n'

li_ret = iu_pas201.nf_pasp55cr_inq_all_area_sects(istr_error_info, &
									ls_input, &
									ls_area_string, &
									ls_sect_string)
									

If IsValid(iu_pas201) Then Destroy(iu_pas201)

li_rtn = This.GetChild('area_name_code', idwc_area)
idwc_area.Reset()
li_rtn = idwc_area.ImportString(ls_area_string)
idwc_area.SetSort("type_descr")
idwc_area.Sort()

li_rtn = This.GetChild('sect_name_code', idwc_sect)
idwc_sect.Reset()
li_rtn = idwc_sect.ImportString(ls_sect_string)

This.InsertRow(0)

is_area_string = ls_area_string
is_sect_string = ls_sect_string

Return True

end function

on u_sched_area_sect.create
end on

on u_sched_area_sect.destroy
end on

event getfocus;If This.GetColumnName() = "name_code" Then
	This.SelectText(1, Len(This.GetText()))
End if
end event

event itemerror;Return 1
end event

event clicked;Integer	li_rtn, &
			li_start_pos

Long		ll_find, &
			ll_rtn

String	ls_ColumnName, &
			ls_filter, &
			ls_area_code

This.SetRedraw(False)

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

If ls_ColumnName = "sect_name_code" Then
	ls_area_code = This.GetItemString(1, 'area_name_code')
	If Not isnull(ls_area_code) Then 
		ls_filter = "area_name_code = '" + ls_area_code + "'"
		idwc_sect.SetFilter(ls_filter)
		idwc_sect.Filter()
		idwc_sect.SetSort("type_descr")
		idwc_sect.Sort()
	Else
		ls_area_code = '999999999'
		ls_filter = "area_name_code = '" + ls_area_code + "'"
		idwc_sect.SetFilter(ls_filter)
		idwc_sect.Filter()
		iw_frame.SetMicroHelp("Select an area before selecting a section") 
	End if 
End If

This.SetRedraw(True)

end event

