HA$PBExportHeader$u_sort_radio2.sru
$PBExportComments$ibdkcjr
forward
global type u_sort_radio2 from datawindow
end type
end forward

global type u_sort_radio2 from datawindow
int Width=594
int Height=208
int TabOrder=10
string DataObject="d_sort_radio2"
boolean Border=false
boolean LiveScroll=true
end type
global u_sort_radio2 u_sort_radio2

forward prototypes
public function string uf_get_sort ()
public subroutine uf_set_sort (string as_choice)
public subroutine uf_set_box_text (string as_title)
public subroutine uf_enable (boolean ab_enable)
end prototypes

public function string uf_get_sort ();return This.GetItemString(1, "choice")
end function

public subroutine uf_set_sort (string as_choice);This.SetItem(1,"choice",as_choice)
end subroutine

public subroutine uf_set_box_text (string as_title);this.object.display.text = as_title
end subroutine

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("choice.Protect = 0 " + &
				"choice.Pointer = 'Arrow!'")
	Case False
		This.Modify("choice.Protect = 1 " + &
				"choice.Pointer = 'Beam!'")
END CHOOSE

end subroutine

event constructor;This.InsertRow(0)
end event

