HA$PBExportHeader$u_product_code.sru
$PBExportComments$UO to get product description from product code
forward
global type u_product_code from u_base_dw_ext
end type
end forward

global type u_product_code from u_base_dw_ext
integer width = 1495
integer height = 92
string dataobject = "d_product_code"
boolean border = false
end type
global u_product_code u_product_code

type variables
Long			il_old_color
Boolean		ib_required = TRUE
Boolean		ib_error_occurred = FALSE
s_error     istr_error_info

Private:
nvuo_fab_product_code	invuo_fab_product_code

end variables

forward prototypes
public function string uf_getproductcode ()
public function string uf_getproductdesc ()
public function boolean uf_setproductdesc (character ac_product_desc[30])
public subroutine uf_disable ()
public subroutine uf_enable ()
public function boolean uf_setproductcode (string as_product_code, string as_product_descr)
public function boolean uf_setproductcode (character ac_product_code[10])
public function string uf_product (character ac_product[10])
end prototypes

public function string uf_getproductcode ();string		ls_product_code

AcceptText()

ls_product_code = GetItemString( 1, "fab_product_code" )

IF IsNull( ls_product_code ) THEN ls_product_code = ""

Return( ls_product_code )
end function

public function string uf_getproductdesc ();string		ls_product_desc

AcceptText()

ls_product_desc = GetItemString( 1, "fab_product_description" )

IF IsNull( ls_product_desc ) THEN ls_product_desc = ""

Return( ls_product_desc )
end function

public function boolean uf_setproductdesc (character ac_product_desc[30]);integer		li_rc

li_rc = SetItem( 1, "fab_product_description", ac_product_desc)

IF li_rc = 1 THEN
	Return( true )
ELSE
	Return( False )
END IF
end function

public subroutine uf_disable ();il_old_color = Long(This.Describe("fab_product_code.BackColor"))

This.Modify("fab_product_code.BackGround.Color = 12632256")
This.Modify("fab_product_code.Protect = 1")
return
end subroutine

public subroutine uf_enable ();This.Modify("fab_product_code.BackGround.Color = " + String(il_old_color))
This.Modify("fab_product_code.Protect = 0")
return
end subroutine

public function boolean uf_setproductcode (string as_product_code, string as_product_descr);integer		li_rc
String	ls_product_code


li_rc = SetItem( 1, "fab_product_code", as_product_code )

If is_ColumnName <> "fab_product_code" Then return False

ls_product_code = GetText()

IF Len(Trim(ls_product_code)) <> 0 THEN
	SetItem(1, "fab_product_description", as_product_descr )
	IF Len(Trim(as_product_descr)) = 0  THEN
		This.SelectText(1, Len(ls_product_Code))
		If Not ib_error_occurred Then
			iw_frame.SetMicroHelp(ls_product_code + " is an invalid Product Code")
		End If
		Return False
	END IF
END IF

Return( true )

end function

public function boolean uf_setproductcode (character ac_product_code[10]);integer		li_rc
String	ls_product_code, &
			ls_product_desc


li_rc = SetItem( 1, "fab_product_code", ac_product_code )

If is_ColumnName <> "fab_product_code" Then return False

ls_product_code = ac_product_code

IF Len(Trim(ls_product_code)) <> 0 THEN
	ls_product_desc = uf_product( ls_product_code ) 
	SetItem(1, "fab_product_description", ls_product_desc )
	IF Len(Trim(ls_product_desc)) = 0  THEN
		This.SelectText(1, Len(ls_product_Code))
		If Not ib_error_occurred Then
			iw_frame.SetMicroHelp(ls_product_code + " is an invalid Product Code")
		End If
		Return False
	END IF
END IF

Return( true )

end function

public function string uf_product (character ac_product[10]);Boolean			lb_return

If not invuo_fab_product_code.uf_check_product(ac_product,' ') then
	ib_error_occurred = invuo_fab_product_code.ib_error_occurred
	If	ib_error_occurred then
		iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
	Else
		iw_frame.SetMicroHelp(ac_product + " is an invalid Product Code")
	End If
End If
	
Return invuo_fab_product_code.uf_get_product_description( )


//Boolean			lb_return
//String			ls_product_desc = Space(42)
//
//iw_frame.SetMicroHelp("Wait.. Inquiring Database")
//
//istr_error_info.se_event_name			= "wf_product"
//istr_error_info.se_procedure_name	= "nf_pasp03br"
//istr_error_info.se_message				= Space(70)
//
//IF Not IsValid( iu_pas201 ) THEN
//	iu_pas201	=  CREATE u_pas201
//END IF
//
//lb_return	= iu_pas201.nf_pasp03br( istr_Error_Info, ac_product, ls_product_desc )
// 
//IF Not lb_return THEN		
//	ib_error_occurred = TRUE
//	ls_product_desc = ''
//	If Left(Upper(istr_error_info.se_message),6) = "PAS002" Then
//		// Allow us to display "So-and-so is an invalid Product Code" instead of
//		// displaying "PAS002 FAB PRODUCT CODE SO-AND-SO NOT FOUND" which is ugly
//		ib_error_occurred = FALSE
//	End If
//END IF
//
//Return( iw_frame.iu_string.nf_gettoken(ls_product_desc,'~t') )
//
end function

on getfocus;call u_base_dw_ext::getfocus;String	ls_GetText 

ls_GetText = Trim(This.GetText())
If Right(ls_GetText, 1) = '*' Then
	This.SelectText(1, Len(ls_GetText) - 1)
Else
	This.SelectText(1, Len(ls_GetText))
End if

end on

event ue_postconstructor;call super::ue_postconstructor;If Not ib_updateable Then
	Modify("fab_product_code.Protect='1' fab_product_code.Background.Color='12632256'")
End If
end event

event itemchanged;call super::itemchanged;String	ls_product_code, &
			ls_product_desc

If is_ColumnName <> "fab_product_code" Then return

ls_product_code = GetText()

IF Len(Trim(ls_product_code)) <> 0 THEN
	ls_product_desc = uf_product( ls_product_code ) 
	SetItem(1, "fab_product_description", ls_product_desc )
	IF Len(Trim(ls_product_desc)) = 0  THEN
		This.SelectText(1, Len(ls_product_Code))
		If Not ib_error_occurred Then
			iw_frame.SetMicroHelp(ls_product_code + " is an invalid Product Code")
		End If
		Return 1
	END IF
	If Not ib_error_occurred Then
		iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	End If
Else
	SetItem(1, "fab_product_description", "" )
	This.SelectText(1, Len(ls_product_Code))
	If ib_required Then
		iw_frame.SetMicroHelp("Product Code is a required field")
		Return 1
	End If
END IF
Return 0

end event

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;String	ls_GetText 

ls_GetText = Trim(This.GetText())
If Right(ls_GetText, 1) = '*' Then
	This.SelectText(1, Len(ls_GetText) - 1)
Else
	This.SelectText(1, Len(ls_GetText))
End if

end on

event itemerror;call super::itemerror;Return 1
end event

on u_product_code.create
end on

on u_product_code.destroy
end on

