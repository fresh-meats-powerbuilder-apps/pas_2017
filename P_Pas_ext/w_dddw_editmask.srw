HA$PBExportHeader$w_dddw_editmask.srw
forward
global type w_dddw_editmask from w_base_sheet_ext
end type
type dw_data from u_base_dw_ext within w_dddw_editmask
end type
end forward

global type w_dddw_editmask from w_base_sheet_ext
integer width = 987
integer height = 586
boolean titlebar = false
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = popup!
dw_data dw_data
end type
global w_dddw_editmask w_dddw_editmask

type prototypes
Function UINT GetSystemMetrics(UINT aIndex) Library "user.exe"

Function Int SetWindowPos(int hWin, int hWndPos, int x1, int y1, &
			int cx, int cy, int swp) library 'user.exe'
end prototypes

type variables
Private:
u_dddw_editmask		iu_parent
end variables

on close;call w_base_sheet_ext::close;iu_parent.SetFocus()

end on

on deactivate;call w_base_sheet_ext::deactivate;Close(This)

end on

event open;call super::open;Int	li_x, &
		li_y

iu_parent = Message.PowerObjectParm
This.dw_data.DataObject = iu_parent.nf_GetDataObject()
iu_parent.dw_Data.ShareData(This.dw_Data)

//SetWindowPos(Handle(This), -1, 0, 0, 0, 0, 3)

iu_parent.nf_GetDddwXY(li_x, li_y)
This.Move(li_x, li_y)

This.dw_Data.Width = iu_Parent.nf_GetDDDWWidth()
This.Width = This.dw_Data.Width

end event

on closequery;// comment
end on

on w_dddw_editmask.create
int iCurrent
call super::create
this.dw_data=create dw_data
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_data
end on

on w_dddw_editmask.destroy
call super::destroy
destroy(this.dw_data)
end on

type dw_data from u_base_dw_ext within w_dddw_editmask
integer width = 965
integer height = 576
string dataobject = "d_pas_plant_dddw"
boolean border = false
end type

event clicked;call super::clicked;

If row = 0 Then return

iu_Parent.TriggerEvent('ue_dddw_select', 0, Row)
Close(Parent)
end event

on rowfocuschanged;call u_base_dw_ext::rowfocuschanged;This.SelectRow(0, False)
This.SelectRow(This.GetRow(), True)
end on

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

