HA$PBExportHeader$u_production_date.sru
forward
global type u_production_date from datawindow
end type
end forward

global type u_production_date from datawindow
integer width = 882
integer height = 92
integer taborder = 1
string dataobject = "d_production_date"
boolean border = false
boolean livescroll = true
event ue_keydown pbm_dwnkey
event datechange ( )
end type
global u_production_date u_production_date

type variables
Window		iw_parentwindow

//Type of date function
String	is_date_type, &
			is_date
	
// Datawindow Row Selection (0,1,2,3)
// 	0 - No rows selected (default)
//	1 - One row selected
//	2 - Multiple rows selected
//	3 - Multiple rows with CTRL and ALT support
string	is_selection= "0"

// Does datawindow have update capabilities
boolean	ib_Updateable

// Reset Datawindow on insert of new row
//	TRUE - Reset datawindow on Insert
// 	FALSE- Do not Reset DW on Insert (default)
boolean	ib_ResetOnInsert

//used to set the top row in datawindow as current row
boolean	ib_ScrollSetsCurrentRow
// Parent window established in constructor event
window	iw_parent

// Contains the row number of the current row
long	il_dwRow

// Holds the number of the last clicked row
// Used for selecting rows with Shift Key
long	il_last_clicked_row

// Draggable Datawindow
// Need to issue Drag(Begin!) in clicked event
// ue_lbuttonup automatically turns Drag Off
boolean ib_draggable

// Holds the name of the object at pointer the user clicked on
string	is_ObjectAtPointer

// Holds the name of the band the user clicked in
string	is_BandAtPointer

// Holds the name of the current column
string	is_ColumnName

// Set this variable to Move the focus to the first column 
// in next row when enter is hit
boolean	ib_firstcolumnonnextrow

// Enable scrolling in the datawindow
// FALSE	- Do not allow scrolling
// TRUE	- Allow scrolling (default)
boolean	ib_scrollable = TRUE

//nonvisual used for find replace dialog boxes
u_findreplace	inv_find

end variables

forward prototypes
public function integer uf_enable (boolean ab_enable)
public function date uf_get_effective_date ()
public function integer uf_set_effective_date (date adt_effective_date)
public function integer uf_modified ()
public function integer uf_set_text (string as_text)
public function integer uf_initilize (string as_text_name, string as_date_type)
public function integer uf_validate (string as_date, ref string as_message)
end prototypes

event ue_keydown;INTEGER		li_Column, &
				li_ColumnCount


long			ll_CurrentRow, &
				ll_CurrentColumn
				
String		ls_date, &
				ls_ColumnType

str_parms	lstr_parms

CHOOSE CASE TRUE
CASE	KeyDown(KeyEnter!)
	IF NOT ib_firstcolumnonnextrow THEN RETURN
	IF il_dwrow < This.RowCount() THEN
		li_ColumnCount = INTEGER(This.Describe("DataWindow.Column.Count"))
		FOR li_Column = 1 TO li_ColumnCount
			IF This.Describe("#" + STRING(li_Column) + ".TabSequence") = "10" THEN
				This.SetColumn(li_Column)
			END IF
		NEXT
	END IF
CASE	Keydown(KeyControl!) and Keydown(KeyDownArrow!)
	IF This.Describe("DataWindow.ReadOnly") = 'yes' THEN RETURN 
	
	// Get the current row and column that has focus
			ll_CurrentRow = This.GetRow()
			ll_CurrentColumn = This.GetColumn()
			
	// Get the current column name
			is_ColumnName = GetColumnName()

	// Get the column type of the clicked field
			ls_ColumnType = Lower(This.Describe("#" + String( &
												ll_CurrentColumn) + ".ColType"))

//	// Get the X and Y coordinates of the place that was clicked
lstr_parms.integer_arg[1] = iw_parentwindow.PointerX() + iw_parentwindow.WorkSpaceX() - 50
CHOOSE CASE	lstr_parms.integer_arg[1]
CASE IS > 2253
	lstr_parms.integer_arg[1] = 2253
CASE is < 113
	lstr_parms.integer_arg[1] = 113
END CHOOSE

lstr_parms.integer_arg[2] = iw_parentwindow.PointerY() + iw_parentwindow.WorkSpaceY() - 500
CHOOSE CASE	lstr_parms.integer_arg[2]
CASE IS > 1053
	lstr_parms.integer_arg[2] = 1053
CASE is < 0
	lstr_parms.integer_arg[2] = 0
END CHOOSE

	IF ls_ColumnType = "date" or &
		ls_ColumnType = "datetime" THEN
			
			Choose Case ls_ColumnType
				Case "date"
					lstr_parms.date_arg[1] = This.GetItemDate(ll_CurrentRow, &
																	ll_CurrentColumn)
					IF IsNull(lstr_parms.date_arg[1]) THEN
						lstr_parms.date_arg[1] = Today()
					END IF
					OpenWithParm(w_Calendar, lstr_parms)
					// Get the return string (date) from the calendar window
					// If an empty string do not do anything
					ls_date = Message.StringParm
// If you hit cntl downarrow the calendar pops up.  When the datawindow gets
// focus it scrolls to the next row.
// This bug has been fixed, however, it was determined to be more trouble than it was
// worth.  So, If it has been decided to implement, uncomment the Down arrow bug
// below.
//downarrow bug					This.SetRedraw(False)
					If ls_date <> "" Then
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						SetText(ls_Date)
					Else
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						Return
					End If
					
				Case "datetime"
					lstr_parms.date_arg[1] = Date(This.GetItemDateTime(ll_CurrentRow, &
																		ll_CurrentColumn))
					OpenWithParm(w_Calendar, lstr_parms)
					// Get the return string (date) from the calendar window
					// If an empty string do not do anything
					ls_date = Message.StringParm
						This.SetRedraw(False)
					If ls_date <> "" Then
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						SetText(ls_Date)
					Else
//downarrow bug						Message.StringParm = String(ll_currentrow)
//downarrow bug						This.PostEvent('ue_post_keydown')
						Return
					End If
				End Choose
	END IF
CASE ELSE

	// Used to not allow scrolling of a datawindow
	// If scroll ins allowed then return out of script
	If ib_scrollable Then Return

	// Turn the vertical scroll bar off
	vscrollbar=False

		IF (KeyDown(KeyTab!))			or &
			(KeyDown(KeyEnter!))			or &
			(KeyDown(KeyDownArrow!))	or &
		 	(KeyDown(KeyUpArrow!))		or &
		 	(KeyDown(KeyPageDown!))		or &
		 	(KeyDown(KeyPageUp!))			THEN
// ibdkdld 07/30/2002 production problem	remove code		 
//			 MessageBox("","Im here")
//				SetRedraw(False)				
				PostEvent("ue_no_scroll")
		END IF
END CHOOSE

end event

event datechange();If is_date > "" Then This.setitem(1,"effective_date",date(is_date))

end event

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.effective_date.Background.Color = 16777215
	This.object.effective_date.Protect = 0
Else
	This.object.effective_date.Background.Color = 12632256
	This.object.effective_date.Protect = 1
End If

Return 1
end function

public function date uf_get_effective_date ();Date				ldt_effective_date

If This.AcceptText() = -1 then 
	This.SetFocus()
	SetNull(ldt_effective_date)
	return ldt_effective_date
End if

ldt_effective_date = This.GetItemDate(1, 'effective_date')

Return ldt_effective_date
end function

public function integer uf_set_effective_date (date adt_effective_date);Long	ll_row, &
		ll_row_count

String	ls_text

This.SetItem(1,"effective_date",Date(String(adt_effective_date, 'yyyy-mm-dd')))
This.SetFocus()
This.SelectText(1, Len(String(adt_effective_date)) + 5)

return 0
end function

public function integer uf_modified ();Return 0
end function

public function integer uf_set_text (string as_text);This.object.effective_date_t.Text = as_text
Return 1
end function

public function integer uf_initilize (string as_text_name, string as_date_type);// Set the text on the user object.
This.uf_set_text(as_text_name)
// Set a instance var to be used in the uf_validate.
is_date_type = as_date_type

Return 0
end function

public function integer uf_validate (string as_date, ref string as_message);String						ls_temp,&
								ls_temp1
Long ll_rtn								
								
// changed the whole process ibdkdld 07/30/02
nvuo_pa_business_rules 	nvuo_pa


CHOOSE CASE is_date_type
	CASE 'PA' 
		ls_temp1 = ''
		If Not nvuo_pa.uf_check_pa_date(Date(as_date),ls_temp1) Then
  			ls_temp = 'You must select a date between '+ String(today(),'mm/dd/yyyy') + ' and '
			ls_temp += ls_temp1
			as_message = ''
			as_message = ls_temp
			This.SetFocus()
			This.SelectText ( 1, 15 )
			Return 1
		End If
	CASE 'EX' 
		ls_temp1 = ''
		ll_rtn = nvuo_pa.uf_check_pa_date_ext(Date(as_date),ls_temp1)
		
		Choose case ll_rtn
			Case 0 
				// Valid PA Range
			Case 1	
				// Valid Ext PA WeekEnd date
			Case 2
				// Set valid Ext PA WeekEnd date 
	  			ls_temp = 'The date of ' + String(date(as_date),'mm/dd/yyyy') + ' is beyond the daily PA range -- ' & 
				  + 'the date has been changed to the next Sunday date'
				as_message = ls_temp
				is_date = ls_temp1
				This.PostEvent("datechange")
				Return 1
			Case 3
				// Invalid Ext PA WeekEnd date/PA Range date
	  			ls_temp = 'You must select a date between '+ String(today(),'mm/dd/yyyy') + ' and ' &
						+ ls_temp1 + ' or a Sunday date beyond ' + ls_temp1  
				as_message = ls_temp
				This.SetFocus()
				This.SelectText ( 1, 15 )
				Return 1
			Case 4
	  			ls_temp = 'The date of ' + String(date(as_date),'mm/dd/yyyy') + ' is beyond the extended PA weekend date -- ' & 
				  + 'the date has been changed to the max Sunday date'
				as_message = ls_temp
				is_date = ls_temp1
				This.PostEvent("datechange")
				return 1
				// ibdkdld change business rules
			Case 5 
	  			ls_temp = 'The date of ' + String(date(as_date),'mm/dd/yyyy') + ' is beyond the Daily PA date range -- ' & 
				  + 'the date has been changed to the max Daily PA Date'
				as_message = ls_temp
				is_date = ls_temp1
				This.PostEvent("datechange")
				return 1
		end choose		
	CASE ELSE
		Return 0
END CHOOSE

Return 0

				
//		removed ibdkdld		
//		If Not nvuo_pa.uf_check_pa_date(Date(as_date),ls_temp1) Then
//  			ls_temp = 'You must select a date between '+ String(today(),'mm/dd/yyyy') + ' and '
//			ls_temp += ls_temp1
//			as_message = ''
//			as_message = ls_temp
//			This.SetFocus()
//			This.SelectText ( 1, 15 )
//			Return 1
//				
				

end function

event doubleclicked;String		ls_date

str_parms	lstr_parms


If This.object.effective_date.Protect = '1' Then Return -1

// Get the X and Y coordinates of the place that was clicked
lstr_parms.integer_arg[1] = iw_parentwindow.PointerX() + iw_parentwindow.WorkSpaceX() - 50
CHOOSE CASE	lstr_parms.integer_arg[1]
CASE IS > 2253
	lstr_parms.integer_arg[1] = 2253
CASE is < 113
	lstr_parms.integer_arg[1] = 113
END CHOOSE

lstr_parms.integer_arg[2] = iw_parentwindow.PointerY() + iw_parentwindow.WorkSpaceY() - 500
CHOOSE CASE	lstr_parms.integer_arg[2]
CASE IS > 1053
	lstr_parms.integer_arg[2] = 1053
CASE is < 0
	lstr_parms.integer_arg[2] = 0
END CHOOSE

lstr_parms.date_arg[1] = This.GetItemDate(1, 'effective_date')
OpenWithParm(w_Calendar, lstr_parms)
// Get the return string (date) from the calendar window
// If an empty string do not do anything
ls_date = Message.StringParm
If ls_date <> "" Then
	SetText(ls_Date)
Else
	Return
End If

end event

event constructor;iw_parentwindow = Parent
This.InsertRow(0)
end event

event itemchanged;String					ls_temp

//u_string_functions	lu_strings


//If lu_strings.nf_IsEmpty(data) Then
//	iw_frame.SetMicroHelp("Production Date is a required field")
//	This.SetFocus()
//	Return 1
//End if
//
If This.uf_validate(data,ls_temp) = 1 Then
   iw_frame.SetMicroHelp(ls_temp)
	This.SetFocus()
	Return 1
End If	

Return 0
end event

event itemerror;Return 1
end event

on u_production_date.create
end on

on u_production_date.destroy
end on

