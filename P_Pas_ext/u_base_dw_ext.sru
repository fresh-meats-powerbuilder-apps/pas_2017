HA$PBExportHeader$u_base_dw_ext.sru
forward
global type u_base_dw_ext from u_netwise_dw
end type
end forward

global type u_base_dw_ext from u_netwise_dw
event ue_graph pbm_custom01
event ue_graph_type pbm_custom02
event ue_graph_name pbm_custom03
event ue_graph_data pbm_custom04
event ue_revisions ( )
end type
global u_base_dw_ext u_base_dw_ext

on ue_graph;call u_netwise_dw::ue_graph;//w_graph		w_grp_child

//OpenSheetWithParm( w_grp_child, this, iw_frame, 0, original! )
end on

event ue_graph_type;call super::ue_graph_type;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_type, lst_graph_axis.dw, iw_frame )
end event

event ue_graph_name;call super::ue_graph_name;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_name, lst_graph_axis, iw_frame )
end event

event ue_graph_data;call super::ue_graph_data;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_data, lst_graph_axis, iw_frame )
end event

event ue_revisions;/*****************************************************************
**   REVISION NUMBER: rev#01
**   PROJECT NUMBER:  support
**   DATE:				 july 99            
**   PROGRAMMER:      David Deal
**   PURPOSE:         added code in the constructor and ue_keydown
**                    event to write and check the ibpuser.ini
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

event constructor;call super::constructor;String ls_value


ib_Updateable = True

//rev#01 added if
ls_value = ProfileString('ibpuser.ini', "PAS",'u_base_dw_ext.ue_keydown', "None") 
If ls_value = 'None'	Then
	SetProfileString ( 'ibpuser.ini', 'PAS', 'u_base_dw_ext.ue_keydown', 'N' )
End If

end event

event ue_keydown;call super::ue_keydown;Long	ll_CurrentRow, &
		ll_CurrentColumn

String	ls_ColumnType, &
			ls_ColumnName, &
			ls_Text,&
			ls_value
			
// Get the current row and column that has focus
ll_CurrentRow = This.GetRow()
ll_CurrentColumn = This.GetColumn()
		
// Get the current column name
ls_ColumnName = GetColumnName()

// Get the column type of the clicked field	
ls_ColumnType = Lower(This.Describe("#" + String( &
									ll_CurrentColumn) + ".ColType"))

ls_Text = GetText()

Choose Case ls_ColumnType
	Case "date", "datetime"
		//rev#01 added if
		ls_value = ProfileString('ibpuser.ini', "PAS",'u_base_dw_ext.ue_keydown', "None") 
		If ls_value = 'Y'	Then
			Choose Case True
				Case KeyDown(KeyDownArrow!)
					SetText(String(RelativeDate(Date(ls_Text),-1)))				
				Case KeyDown(KeyUpArrow!)
					SetText(String(RelativeDate(Date(ls_Text),1)))			
			End Choose
		End If
End Choose
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1, 100)
end event

