HA$PBExportHeader$w_pas_file_open.srw
$PBExportComments$File Open Dialog Box.
forward
global type w_pas_file_open from w_base_response_ext
end type
type dw_window_list from datawindow within w_pas_file_open
end type
type st_1 from statictext within w_pas_file_open
end type
end forward

global type w_pas_file_open from w_base_response_ext
integer width = 1330
integer height = 1104
string title = "Open Window"
long backcolor = 12632256
dw_window_list dw_window_list
st_1 st_1
end type
global w_pas_file_open w_pas_file_open

on open;call w_base_response_ext::open;Long	ll_row_count, &
		ll_counter


ll_row_count = dw_window_list.RowCount()

For ll_counter = 1 to ll_row_count
	If Not iw_frame.iu_netwise_data.nf_IsWindowAccessable( &
					dw_window_list.GetItemString( ll_counter, "window_name")) Then
		dw_window_list.DeleteRow(ll_counter)
		ll_counter --
		ll_row_count --
	End if
Next

If ll_row_count > 0 Then
	dw_window_list.SetRow(1)
End if

dw_window_list.SetRowFocusIndicator(FocusRect!)
dw_window_list.SetFocus()
end on

on w_pas_file_open.create
int iCurrent
call super::create
this.dw_window_list=create dw_window_list
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_window_list
this.Control[iCurrent+2]=this.st_1
end on

on w_pas_file_open.destroy
call super::destroy
destroy(this.dw_window_list)
destroy(this.st_1)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Long	ll_row

Window	lw_open
String	ls_installed,ls_window,ls_RegistryString
Int		li_rtn

ll_row = dw_window_list.GetSelectedRow(0)
If ll_row = 0 then return
This.Enabled = False

ls_window = dw_window_list.GetItemString(ll_row, "window_name") 
CloseWithReturn(This, ls_window)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_file_open
integer x = 914
integer y = 280
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_file_open
integer x = 914
integer y = 156
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_file_open
integer x = 914
integer y = 32
integer taborder = 20
end type

type dw_window_list from datawindow within w_pas_file_open
integer x = 41
integer y = 92
integer width = 837
integer height = 860
integer taborder = 5
string dataobject = "d_pas_file_open"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;If row <> 0 Then
	This.SelectRow(0, False)
	This.SelectRow(row, True)
End if
end event

event doubleclicked;cb_base_ok.PostEvent(Clicked!)
end event

on rowfocuschanged;Long	ll_row

ll_row = This.GetRow()
If ll_row <> 0 Then
	This.SelectRow(0, False)
	This.SelectRow(ll_row, True)
End if
end on

type st_1 from statictext within w_pas_file_open
integer x = 41
integer y = 16
integer width = 274
integer height = 72
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean enabled = false
string text = "Windows:"
boolean focusrectangle = false
end type

