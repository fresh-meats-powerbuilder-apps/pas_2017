HA$PBExportHeader$w_division_inq.srw
forward
global type w_division_inq from w_base_response_ext
end type
type dw_division from u_division within w_division_inq
end type
end forward

global type w_division_inq from w_base_response_ext
int Width=1660
int Height=401
long BackColor=12632256
dw_division dw_division
end type
global w_division_inq w_division_inq

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
				ls_division
				

If dw_division.AcceptText() = -1 Then return

ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End If

iw_parentwindow.Event ue_set_data('division',ls_division)

ib_ok_to_close = True

Close(This)

end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('division')
dw_division.uf_set_division(Message.StringParm)


end event

on w_division_inq.create
int iCurrent
call w_base_response_ext::create
this.dw_division=create dw_division
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_division
end on

on w_division_inq.destroy
call w_base_response_ext::destroy
destroy(this.dw_division)
end on

event ue_base_cancel;call super::ue_base_cancel;Close (This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_division_inq
int X=929
int Y=161
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_division_inq
int X=650
int Y=161
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_division_inq
int X=371
int Y=161
int TabOrder=20
end type

type dw_division from u_division within w_division_inq
int X=33
int Y=25
int TabOrder=10
end type

