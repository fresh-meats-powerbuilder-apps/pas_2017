HA$PBExportHeader$w_plant_type_inq.srw
forward
global type w_plant_type_inq from w_base_response_ext
end type
type dw_inquire from u_base_dw_ext within w_plant_type_inq
end type
end forward

global type w_plant_type_inq from w_base_response_ext
integer width = 1829
integer height = 604
long backcolor = 67108864
dw_inquire dw_inquire
end type
global w_plant_type_inq w_plant_type_inq

type variables
Boolean		isvalidreturn

Window		iw_parent
end variables

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant_type')

dw_inquire.InsertRow(0)
dw_inquire.SetItem(1, 'plant_type', message.StringParm)

dw_inquire.SelectText(1, Len(dw_inquire.GetText()))
end event

event ue_base_ok;call super::ue_base_ok;String	ls_plant_type


If dw_inquire.AcceptText() = -1 then 
	dw_inquire.SetFocus()
	return
End if

ls_plant_type = dw_inquire.GetItemString(1, 'plant_type')

If iw_frame.iu_string.nf_IsEmpty(ls_plant_type) Then
	iw_frame.SetMicroHelp('Plant Type is a required field')
	dw_inquire.SetFocus()
	return
End if
	

iw_parentwindow.Event ue_set_data('plant_type',ls_plant_type)

ib_ok_to_close = True

Close(This)

end event

event close;call super::close;Close(This)
end event

event ue_base_cancel;call super::ue_base_cancel;IsValidReturn = True
CloseWithReturn(This, "")
end event

on w_plant_type_inq.create
int iCurrent
call super::create
this.dw_inquire=create dw_inquire
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_inquire
end on

on w_plant_type_inq.destroy
call super::destroy
destroy(this.dw_inquire)
end on

type cb_base_help from w_base_response_ext`cb_base_help within w_plant_type_inq
integer x = 1458
integer y = 312
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_plant_type_inq
integer x = 1458
integer y = 188
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_plant_type_inq
integer x = 1458
integer y = 64
end type

type dw_inquire from u_base_dw_ext within w_plant_type_inq
integer x = 32
integer y = 112
integer width = 1371
integer height = 164
integer taborder = 2
string dataobject = "d_plant_type_inq"
boolean border = false
end type

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild		ldwc_type


dw_inquire.GetChild("plant_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PLTGROUP")

end event

