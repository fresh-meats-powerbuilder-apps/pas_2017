HA$PBExportHeader$w_base_sheet_ext.srw
forward
global type w_base_sheet_ext from w_netwise_sheet
end type
end forward

global type w_base_sheet_ext from w_netwise_sheet
int Width=2889
int Height=1540
long BackColor=1090519039
event ue_revisions ( )
end type
global w_base_sheet_ext w_base_sheet_ext

type variables
Boolean			ib_inquire

w_base_response_ext	iw_inquirewindow

String			is_inquire_window_name, &
			is_original_title	
end variables

forward prototypes
public subroutine wf_change_color ()
public function boolean wf_insertrow ()
public function boolean wf_addrow ()
public function boolean wf_deleterow ()
public function boolean wf_retrieve ()
end prototypes

event ue_revisions;/*****************************************************************
**   REVISION NUMBER: rev#01
**   PROJECT NUMBER:  support
**   DATE:				 july 99            
**   PROGRAMMER:      Rich Muckey
**   PURPOSE:         Added ue_revisions
**                    
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

public subroutine wf_change_color ();
end subroutine

public function boolean wf_insertrow ();DataWindow		ldw_focus
GraphicObject	lg_control

lg_control = GetFocus( ) 
CHOOSE CASE TypeOf(lg_control)
CASE DataWindow!
	ldw_focus = lg_control
	ldw_focus.SetRow(ldw_focus.InsertRow(ldw_focus.GetRow()))
END CHOOSE
return true
end function

public function boolean wf_addrow ();DataWindow		ldw_focus
GraphicObject	lg_control
Long				ll_new_row


lg_control = GetFocus( ) 
CHOOSE CASE TypeOf(lg_control)
CASE DataWindow!
	ldw_focus = lg_control
	ll_new_row = ldw_focus.InsertRow(0)
	ldw_focus.SetRow(ll_new_row)
	ldw_focus.ScrollToRow(ll_new_row)	
	ldw_focus.SetFocus()
END CHOOSE
return true
end function

public function boolean wf_deleterow ();DataWindow		ldw_focus
GraphicObject	lg_control

lg_control = GetFocus( ) 
CHOOSE CASE TypeOf(lg_control)
CASE DataWindow!
	ldw_focus = lg_control
	If ldw_focus.GetItemStatus(ldw_focus.GetRow(), 0, Primary!) <> New! Then
		If MessageBox("Delete Row", "Are you sure you want to delete the selected row?", &
				Question!, YesNo!, 2) = 1 Then
			ldw_focus.DeleteRow(0)
		End if
	Else
		ldw_focus.DeleteRow(0)
	End if
END CHOOSE
return true
end function

public function boolean wf_retrieve ();

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

Return True
end function

on w_base_sheet_ext.create
call super::create
end on

on w_base_sheet_ext.destroy
call super::destroy
end on

event ue_fileprint;call super::ue_fileprint;This.wf_print()
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'title'
		Message.StringParm = is_original_title
End Choose

end event

event ue_set_data;call super::ue_set_data;u_conversion_functions		lu_conv


Choose Case as_data_item
	Case 'ib_inquire'
		ib_inquire = lu_conv.nf_Boolean(as_value)
End Choose
end event

event ue_postopen;call super::ue_postopen;is_original_title = This.Title

end event

