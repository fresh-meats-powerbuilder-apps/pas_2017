HA$PBExportHeader$w_about_ext.srw
forward
global type w_about_ext from w_about
end type
type mle_1 from multilineedit within w_about_ext
end type
end forward

type memory from structure
	unsignedlong		m_length
	unsignedlong		m_loaded
	unsignedlong		m_totalphys
	unsignedlong		m_availphys
	unsignedlong		m_totalpagefile
	unsignedlong		m_availpagefile
	unsignedlong		m_totalvirtual
	unsignedlong		m_availvirtual
end type

global type w_about_ext from w_about
int Height=1457
mle_1 mle_1
end type
global w_about_ext w_about_ext

type prototypes
Function boolean GetComputerNameA(ref string cname, ref long nbuf) Library "kernel32.dll" alias for "GetComputerNameA;Ansi"

SUBROUTINE GlobalMemoryStatus(ref memory mem2) LIBRARY "Kernel32.dll" alias for "GlobalMemoryStatus;Ansi"

Function int Get_Ip_add_string( Ref Char PCNAME[100]) Library "Ip.dll" alias for "Get_Ip_add_string;Ansi"

FUNCTION long MciSendStringA(string cmd, REF string rtn, long size, long wnd) LIBRARY "winmm.dll" alias for "MciSendStringA;Ansi"

end prototypes

on w_about_ext.create
int iCurrent
call w_about::create
this.mle_1=create mle_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=mle_1
end on

on w_about_ext.destroy
call w_about::destroy
destroy(this.mle_1)
end on

event open;environment	en_environ

u_sdkcalls	lu_sdkcalls

string		ls_filename, &
				ls_date, &
				ls_time

char			lc_PCIP_Address[100]

string ls_compname
long ll_buf = 500

String	ls_memory = ""
memory	sysmem
sysmem.m_length = 32

Constant Date ld_date = Today()
Constant Time lt_time = Now()

getenvironment(en_environ)
This.Title = "About " + iw_frame.Title
lu_sdkCalls = Create u_sdkCalls

ls_filename			=	lower(lu_sdkCalls.nf_GetModulefilename())
//Display File Creation Date/Time and IP address only for 32 bit operating systems
If en_environ.OSMajorRevision = 4 Or en_environ.OsType = WindowsNT! &
						Then
	mle_1.Text += "Release Date/Time : " + String(ld_date) + ' ' + String( lt_time) + "~r~n"
	mle_1.Text += "Path : " + ls_filename + "~r~n"
	lc_PCIP_Address = Space(100)
	Get_Ip_add_string( lc_PCIP_Address)
	mle_1.Text += "IP Address: " + String(lc_PCIP_Address) + "~r~n"
ELSE
	mle_1.Text += "Path : " + ls_filename + "~r~n"
END IF

If Not iw_frame.iu_string.nf_IsEmpty(ls_compname) Then
	mle_1.Text += "Computer: " + ls_compname + "~r~n"
End If

mle_1.Text += 'UserID: ' + SQLCA.UserID + "~r~n"

mle_1.Text += "Video: " + String(en_environ.screenwidth) + "x" + String(en_environ.screenheight) + "x" + &
		String(en_environ.Numberofcolors)
mle_1.Text += "~r~n"

mle_1.Text += "PowerBuilder"

Choose Case en_environ.OSType
	Case windows!
		If en_environ.Win16 Then
			mle_1.Text += "/16"
		Else
			mle_1.Text += "/32"
			ls_compname = space(ll_buf)
			GetComputerNameA(ls_compname, ll_buf)
			GlobalMemoryStatus(sysmem)
			ls_memory += "~tMemory Usage: ~t~t" + String(sysmem.m_loaded) + "%~r~n"
			ls_memory += "~tTotal Physical Memory: ~t" + string(sysmem.m_totalphys,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tTotal Available Memory: ~t" + string(sysmem.m_availphys,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tTotal Page Size: ~t~t" + string(sysmem.m_totalpagefile,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tAvailable Page Size: ~t" + string(sysmem.m_availpagefile,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tTotal Virtual Memory: ~t" +  string(sysmem.m_totalvirtual,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tAvailable Virtual Memory: ~t" + string(sysmem.m_availvirtual,"###,###,###,###,###") + "~r~n"
		End If

		If en_environ.OSMajorRevision = 4 Or &
			en_environ.OSMinorRevision = 95 Then
				mle_1.Text += " on Windows 95"
		Else
			mle_1.Text += " on Windows " + String(en_environ.OSMajorRevision) + &
								"." + String(en_environ.OSMinorRevision)
		End If
	Case windowsnt!
		If en_environ.Win16 Then
			mle_1.Text += "/16"
		Else
			mle_1.Text += "/32"
			ls_compname = space(ll_buf)
			GetComputerNameA(ls_compname, ll_buf)
			GlobalMemoryStatus(sysmem)
			ls_memory += "~tMemory Usage: ~t~t" + String(sysmem.m_loaded,"###,###,###,###,###") + "%~r~n"
			ls_memory += "~tTotal Physical Memory: ~t" + string(sysmem.m_totalphys,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tTotal Available Memory: ~t" + string(sysmem.m_availphys,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tTotal Page Size: ~t~t" + string(sysmem.m_totalpagefile,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tAvailable Page Size: ~t" + string(sysmem.m_availpagefile,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tTotal Virtual Memory: ~t" +  string(sysmem.m_totalvirtual,"###,###,###,###,###") + "~r~n"
			ls_memory += "~tAvailable Virtual Memory: ~t" + string(sysmem.m_availvirtual,"###,###,###,###,###") + "~r~n"
		End If

		mle_1.Text += " on Windows NT " + String(en_environ.OSMajorRevision) + &
							"." + String(en_environ.OSMinorRevision)							
	Case sol2!
		mle_1.Text += "/Unix"
		mle_1.Text += " on Solaris " +  + String(en_environ.OSMajorRevision) + &
							"." + String(en_environ.OSMinorRevision)
	Case macintosh!
		mle_1.Text += "/Mac"
		mle_1.Text += " on Macintosh " +  + String(en_environ.OSMajorRevision) + &
							"." + String(en_environ.OSMinorRevision)
End Choose

mle_1.Text += "~r~n"
If en_environ.PBType = desktop! Then
	mle_1.Text += "Desktop Version " + String(en_environ.PBMajorRevision) + &
						"." + String(en_environ.PBMinorRevision) + &
						"." + String(en_environ.PBFixesRevision)
Else
	mle_1.Text += "Enterprise Version " + String(en_environ.PBMajorRevision) + &
						"." + String(en_environ.PBMinorRevision) + &
						"." + String(en_environ.PBFixesRevision)
End If

mle_1.Text += "~r~n"

If Not iw_frame.iu_string.nf_IsEmpty(ls_memory) Then
	mle_1.Text += "Memory Information: ~r~n" + ls_memory
End If

Destroy lu_sdkcalls

end event

type cb_base_help from w_about`cb_base_help within w_about_ext
int TabOrder=20
boolean Visible=false
end type

type cb_base_cancel from w_about`cb_base_cancel within w_about_ext
int TabOrder=50
boolean Visible=false
end type

type cb_base_ok from w_about`cb_base_ok within w_about_ext
int TabOrder=30
boolean Visible=false
end type

type p_ibp from w_about`p_ibp within w_about_ext
int X=28
int Y=41
boolean BringToTop=true
end type

type cb_ok from w_about`cb_ok within w_about_ext
int X=526
int Y=1237
int TabOrder=10
boolean BringToTop=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
end type

type sle_exe_name from w_about`sle_exe_name within w_about_ext
int X=51
int Y=765
int Height=77
boolean Visible=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean DisplayOnly=false
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type sle_os from w_about`sle_os within w_about_ext
int Y=845
boolean Visible=false
boolean BringToTop=true
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type sle_pbtype from w_about`sle_pbtype within w_about_ext
int Y=905
boolean Visible=false
boolean BringToTop=true
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type sle_free_resources from w_about`sle_free_resources within w_about_ext
int Y=961
boolean Visible=false
boolean BringToTop=true
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type sle_colors from w_about`sle_colors within w_about_ext
int Y=1021
boolean Visible=false
boolean BringToTop=true
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type sle_height from w_about`sle_height within w_about_ext
int Y=1141
boolean Visible=false
boolean BringToTop=true
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type sle_width from w_about`sle_width within w_about_ext
int Y=1081
boolean Visible=false
boolean BringToTop=true
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type sle_datetime from w_about`sle_datetime within w_about_ext
int Height=77
boolean Visible=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean DisplayOnly=false
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type st_userid from w_about`st_userid within w_about_ext
int X=494
int Y=1209
boolean Visible=false
boolean BringToTop=true
int TextSize=-8
string FaceName="MS Sans Serif"
boolean Italic=false
end type

type mle_1 from multilineedit within w_about_ext
int X=33
int Y=689
int Width=1340
int Height=537
int TabOrder=40
boolean BringToTop=true
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
boolean AutoHScroll=true
boolean AutoVScroll=true
long BackColor=16777215
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

