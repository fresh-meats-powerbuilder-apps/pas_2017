$PBExportHeader$w_prd_not_sched.srw
forward
global type w_prd_not_sched from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_prd_not_sched
end type
type dw_sched_dtl from u_base_dw_ext within w_prd_not_sched
end type
end forward

global type w_prd_not_sched from w_base_sheet_ext
integer width = 1234
integer height = 1660
string title = "Product Not Scheduled"
long backcolor = 67108864
dw_header dw_header
dw_sched_dtl dw_sched_dtl
end type
global w_prd_not_sched w_prd_not_sched

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_updateable 

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

u_pas201		iu_pas201
u_pas203		iu_pas203

String		is_input, &
				is_debug, &
				is_name_string
	

u_sect_functions		iu_sect_functions

DataWindowChild		idwc_sub_desc
end variables

forward prototypes
public function boolean wf_get_sub_desc ()
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_get_sub_desc ();Integer	li_row_Count
				
String	ls_input, &
			ls_output_values
						
u_string_functions u_string

DataWindowChild	ldwc_temp


idwc_sub_desc.Reset()

ls_input = 'SUBSECT~r~n'

ls_output_values = iu_sect_functions.uf_get_names(ls_input)

li_row_count = idwc_sub_desc.ImportString(ls_output_values)

Return True

end function

public function boolean wf_retrieve ();Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_output_values, ls_temp, &
			ls_row_option
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_prd_not_sched_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

dw_header.AcceptText()
SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")


ls_input = dw_header.GetItemString(1, 'plant_code') + '~t' 

ls_input += dw_header.GetItemString(1, 'option') + '~t'
If dw_header.GetItemString(1,'option') = 'N' Then
	ls_temp = String(This.dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd')
	ls_temp = This.dw_header.GetItemString(1, 'shift')
	ls_input += String(This.dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + & 
			This.dw_header.GetItemString(1, 'shift') + '~r~n'
Else
	ls_input += '~t~r~n'		
End If

is_input = ls_input

li_ret = iu_pas201.nf_pasp66cr_inq_prod_not_sched(istr_error_info, &
									is_input, &
									ls_output_values) 
						

This.dw_sched_dtl.Reset()
If li_ret = 0 Then
	This.dw_sched_dtl.ImportString(ls_output_values)
End If

This.dw_sched_dtl.ResetUpdate()

ll_value = dw_sched_dtl.RowCount()
If ll_value < 0 Then ll_value = 0
IF ll_value > 0 THEN
	dw_sched_dtl.SetFocus()
	dw_sched_dtl.ScrollToRow(1)
	//dw_sched_dtl.TriggerEvent("RowFocusChanged")
END IF
SetMicroHelp(String(ll_value) + " rows retrieved")
Return True

end function
event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_pas203) Then Destroy(iu_pas203)


end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_sched_dtl.InsertRow(0)




end event

event ue_postopen;call super::ue_postopen;String						ls_char

DataWindowChild			ldwc_type, &
								ldwc_char, &
								ldwc_uom
								
Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Create Release Schedule"
istr_error_info.se_user_id = sqlca.userid

idwc_sub_desc.Reset()
dw_sched_dtl.GetChild("subsect_name_code", idwc_sub_desc)
//populate the description drop down
wf_get_sub_desc()

//populate the type drop down
dw_sched_dtl.GetChild('subsect_type', ldwc_type)
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PPSSTYPE")

// populate the characteristics drop down

iu_pas203 = Create u_pas203

istr_error_info.se_event_name = "ue_postconstructor"
istr_error_info.se_procedure_name = "u_pas203.nf_pasp61br"

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

If iu_pas203.nf_pasp61br_inq_characteristics(istr_error_info, ls_char) <> 0 Then
	MessageBox('Characteristics', 'There is a problem getting the characteristics. Please call Applications.')
end If

dw_sched_dtl.GetChild("characteristic", ldwc_char)
ldwc_char.ImportString(ls_char)

//populate the type drop down
dw_sched_dtl.GetChild('uom', ldwc_uom)
ldwc_uom.SetTransObject(SQLCA)
ldwc_uom.Retrieve("FABUOM")


This.PostEvent("ue_query")


end event

on w_prd_not_sched.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_sched_dtl=create dw_sched_dtl
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_sched_dtl
end on

on w_prd_not_sched.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_sched_dtl)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'sched_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
	Case 'shift'
		Message.StringParm = dw_header.GetItemString(1, 'shift')
	Case 'Row Option'
		Message.StringParm = dw_header.GetItemString(1, 'option')
End Choose


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant Desc'
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'sched_date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
	Case 'shift'
		dw_header.SetItem(1, 'shift', as_value)
	Case 'Row Option'
		dw_header.SetItem(1, 'option', as_value)
End Choose


end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_sched_dtl.x * 2) + 30 
li_y = dw_sched_dtl.y + 115

if width > li_x Then
	dw_sched_dtl.width	= width - li_x
end if

if height > li_y then
	dw_sched_dtl.height	= height - li_y
end if


end event

type dw_header from u_base_dw_ext within w_prd_not_sched
integer width = 1175
integer height = 408
integer taborder = 20
string dataobject = "d_prd_not_sched_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
end event
type dw_sched_dtl from u_base_dw_ext within w_prd_not_sched
integer x = 37
integer y = 420
integer width = 855
integer height = 1072
integer taborder = 10
string dataobject = "d_prd_not_sched_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event clicked;call super::clicked;Long		ll_rtn
String	ls_temp, &
			ls_ColumnName, &
			ls_sched_date, &
			ls_shift
			
Boolean 	lb_color

If dwo.Type <> "column" and dwo.Type <> "button" Then Return

ls_temp = dwo.Type
If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return






end event

event constructor;call super::constructor;ib_updateable = False

end event

event itemerror;call super::itemerror;return (1)
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

long			ll_source_row, &
				ll_RowCount

String		ls_ColName
				

nvuo_pa_business_rules	u_rule

ls_ColName = GetColumnName()
ll_source_row	= GetRow()
il_ChangedRow = 0

If ls_ColName = "quantity" Then
	This.SetItem(ll_source_row, "qty_chg_ind", "Q")
End If

If ls_ColName = "act_sched" or ls_ColName = "act_prod" Then
	This.SetItem(ll_source_row, "act_chg_ind", "C")
End If

// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

