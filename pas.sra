HA$PBExportHeader$pas.sra
forward
global u_netwise_transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global u_netwise_message message
end forward

global variables
// A pointer the MDI Frame window
w_base_frame_ext	iw_frame

w_base_frame	gw_base_frame
w_netwise_frame	gw_netwise_frame

string gs_Helpdesk_no //Added for HD0000000636867
end variables

global type pas from application
 end type
global pas pas

event idle;iw_frame.SetMicroHelp("Ready")
end event

event systemerror;open(w_system_error)
end event

event open;// Call a function on the Message Object that will open the frame
Message.nf_Set_App_ID("PAS")
OpenWithParm(iw_frame, "ibp002.ini", "w_base_frame_ext")
end event

on pas.create
appname = "pas"
message = create u_netwise_message
sqlca = create u_netwise_transaction
sqlda = create dynamicdescriptionarea
sqlsa = create dynamicstagingarea
error = create error
end on

on pas.destroy
destroy( sqlca )
destroy( sqlda )
destroy( sqlsa )
destroy( error )
destroy( message )
end on

