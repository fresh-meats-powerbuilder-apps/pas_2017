HA$PBExportHeader$w_pas_prod_variance_inq.srw
forward
global type w_pas_prod_variance_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_pas_prod_variance_inq
end type
type dw_shift_date from u_base_dw_ext within w_pas_prod_variance_inq
end type
end forward

global type w_pas_prod_variance_inq from w_base_response_ext
integer width = 1641
integer height = 388
long backcolor = 12632256
dw_plant dw_plant
dw_shift_date dw_shift_date
end type
global w_pas_prod_variance_inq w_pas_prod_variance_inq

type variables
boolean			ib_valid_to_close

w_pas_prod_variance	iw_parent
end variables

on close;call w_base_response_ext::close;If Not ib_valid_to_close Then Message.StringParm = ""
end on

event ue_postopen;call super::ue_postopen;String		ls_plant, &
				ls_shift_date, &
				ls_date, &
				ls_shift


SetRedraw(False)

ls_plant = iw_parent.wf_get_plant()
ls_date = iw_parent.wf_get_date()
ls_shift = iw_parent.wf_get_shift()

If Not iw_frame.iu_string.nf_IsEmpty(ls_plant) Then 
	dw_plant.Reset()
	dw_plant.ImportString(ls_plant)
End If

If iw_frame.iu_string.nF_IsEmpty(ls_date) then ls_date = String(today(), "mm/dd/yyyy")
If iw_frame.iu_string.nf_IsEmpty(ls_shift) Then ls_shift = ""

dw_shift_date.Reset()
dw_shift_date.ImportString(ls_date + "~t" + ls_shift)


SetRedraw(True)
end event

on open;call w_base_response_ext::open;iw_parent = Message.PowerObjectParm

This.Title = iw_parent.Title + " Inquire"

dw_plant.SetFocus()
end on

on w_pas_prod_variance_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_shift_date=create dw_shift_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_shift_date
end on

on w_pas_prod_variance_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_shift_date)
end on

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String	ls_plant, &
			ls_shift, &
			ls_date, &
			ls_string

			
If dw_plant.AcceptText() = -1 or dw_shift_date.AcceptText() = -1 &
	Then Return

ls_plant = dw_plant.nf_get_plant_code()
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End if

ls_date = String(dw_shift_date.GetItemDate(1, "start_date"),"yyyy-mm-dd")
If iw_frame.iu_string.nf_IsEmpty(ls_date) Then
	iw_frame.SetMicroHelp("Date is a required field")
	dw_shift_date.SetFocus()
	dw_shift_date.SetColumn("start_date")
	return
End if

ls_shift = dw_shift_date.GetItemString(1,"shift")
If iw_frame.iu_string.nf_IsEmpty(ls_shift) Then
	iw_frame.SetMicroHelp("Shift is a required field")
	dw_shift_date.SetFocus()
	dw_shift_date.SetColumn("shift")
	return
End if

ls_string = ls_plant + "~t" + dw_plant.uf_get_plant_descr() + &
				"~t" + ls_date + "~t" + ls_shift

ib_valid_to_close = True
CloseWithReturn(This, ls_string)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_prod_variance_inq
integer x = 1184
integer y = 144
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_prod_variance_inq
integer x = 873
integer y = 144
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_prod_variance_inq
integer x = 562
integer y = 144
integer taborder = 30
end type

type dw_plant from u_plant within w_pas_prod_variance_inq
integer taborder = 10
end type

type dw_shift_date from u_base_dw_ext within w_pas_prod_variance_inq
integer x = 23
integer y = 96
integer width = 489
integer height = 172
integer taborder = 20
string dataobject = "dw_shift_date"
boolean border = false
end type

event constructor;call super::constructor;//This.InsertRow(0)

DataWindowChild		ldwc_type


This.GetChild("shift", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("shift")
This.InsertRow(0)

end event

