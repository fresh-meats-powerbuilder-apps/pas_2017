HA$PBExportHeader$w_product_plant_parameters_inq.srw
forward
global type w_product_plant_parameters_inq from w_base_response_ext
end type
type dw_division from u_division within w_product_plant_parameters_inq
end type
type dw_plant from u_plant within w_product_plant_parameters_inq
end type
end forward

global type w_product_plant_parameters_inq from w_base_response_ext
int X=46
int Y=304
int Width=1659
int Height=492
long BackColor=12632256
dw_division dw_division
dw_plant dw_plant
end type
global w_product_plant_parameters_inq w_product_plant_parameters_inq

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
				ls_division
				

If dw_division.AcceptText() = -1 Then return

ls_division = dw_division.uf_get_division()
If iw_frame.iu_string.nf_IsEmpty(ls_division) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_division.SetFocus()
	return
End If

iw_parentwindow.Event ue_set_data('division',ls_division)

If dw_plant.AcceptText() = -1 Then return

ls_plant = dw_plant.uf_get_plant_code()
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End If

iw_parentwindow.Event ue_set_data('plant',ls_plant)

iw_parentwindow.Event ue_set_data('ok', "true")

Close(This)

end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('division')
dw_division.uf_set_division(Message.StringParm)
iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)
iw_parentwindow.Event ue_set_data('close', "true")


end event

on w_product_plant_parameters_inq.create
int iCurrent
call super::create
this.dw_division=create dw_division
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
this.Control[iCurrent+2]=this.dw_plant
end on

on w_product_plant_parameters_inq.destroy
call super::destroy
destroy(this.dw_division)
destroy(this.dw_plant)
end on

event ue_base_cancel;call super::ue_base_cancel;Close (This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_product_plant_parameters_inq
int X=923
int Y=252
int TabOrder=40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_product_plant_parameters_inq
int X=645
int Y=252
int TabOrder=30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_product_plant_parameters_inq
int X=366
int Y=252
int TabOrder=20
end type

type dw_division from u_division within w_product_plant_parameters_inq
int X=32
int Y=24
int TabOrder=10
end type

type dw_plant from u_plant within w_product_plant_parameters_inq
int X=73
int Y=132
int Height=92
int TabOrder=11
boolean BringToTop=true
end type

