HA$PBExportHeader$w_pas_order_detail_inq.srw
forward
global type w_pas_order_detail_inq from w_base_response_ext
end type
type dw_date from u_effective_date within w_pas_order_detail_inq
end type
type dw_option from u_plant_opt within w_pas_order_detail_inq
end type
type dw_fab from u_fab_product_code within w_pas_order_detail_inq
end type
type dw_plant from u_plant within w_pas_order_detail_inq
end type
end forward

global type w_pas_order_detail_inq from w_base_response_ext
integer x = 5
integer y = 4
integer width = 2089
integer height = 732
long backcolor = 67108864
dw_date dw_date
dw_option dw_option
dw_fab dw_fab
dw_plant dw_plant
end type
global w_pas_order_detail_inq w_pas_order_detail_inq

type variables
Boolean			ib_inquire,&
			ib_valid_return
end variables

on w_pas_order_detail_inq.create
int iCurrent
call super::create
this.dw_date=create dw_date
this.dw_option=create dw_option
this.dw_fab=create dw_fab
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_date
this.Control[iCurrent+2]=this.dw_option
this.Control[iCurrent+3]=this.dw_fab
this.Control[iCurrent+4]=this.dw_plant
end on

on w_pas_order_detail_inq.destroy
call super::destroy
destroy(this.dw_date)
destroy(this.dw_option)
destroy(this.dw_fab)
destroy(this.dw_plant)
end on

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
 				ls_manifest_num, &
				ls_date,ls_product,ls_option,ls_state, ls_status
				
u_string_functions		lu_strings
//integer i
//for i = 0 to 3000
//	Yield()
//Next
//
//dw_date.TriggerEvent("datechange")

//if dw_date.AcceptText() = -1 then 
//	dw_date.setFocus()
//	Return
//End If	
//
If dw_plant.AcceptText() = -1 or &
	dw_date.AcceptText() = -1  or &
	dw_option.AcceptText() = -1 Then return 

//dw_product.TriggerEvent ( itemchanged! )	
//call dw_product::itemchanged	
// Product state 10/03/02 ibdkdld
//If dw_product.AcceptText() = -1 Then 
//	//dw_product.selecttext(1,100)
//	dw_product.setfocus()
//	Return
//End If
// Product state 10/03/02 ibdkdld
If not dw_fab.uf_validate( ) Then 
	dw_fab.setfocus()
	Return
End If
	
ls_option = dw_option.uf_get_plant_option()
//If ls_option = "A" Then
	//iw_frame.SetMicroHelp("Plant is a required field")
	//dw_plant.SetFocus()
	//Return
//End If

ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End If

ls_date = String(dw_date.uf_get_effective_date(), 'YYYY-mm-dd')
If lu_strings.nf_IsEmpty(ls_date) Then
	iw_frame.SetMicroHelp("Date is a required field")
	dw_date.SetFocus()
	Return
End If

// Product state 10/03/02 ibdkdld
//ls_product = dw_product.uf_getproductcode()
//If lu_strings.nf_IsEmpty(ls_product) Then
//	iw_frame.SetMicroHelp("Product Code is a required field")
//	dw_product.SetFocus()
//	Return
//End If
ls_product = dw_fab.uf_get_product_code()
If lu_strings.nf_IsEmpty(ls_product) Then
	iw_frame.SetMicroHelp("Product Code is a required field")
	dw_fab.SetFocus()
	Return
End If
// Product state 10/03/02 ibdkdld
ls_state = dw_fab.uf_get_product_state()
If lu_strings.nf_IsEmpty(ls_state) Then
	iw_frame.SetMicroHelp("Product State is a required field")
	dw_fab.SetFocus()
	Return
End If

ls_status = dw_fab.uf_get_product_status()
If lu_strings.nf_IsEmpty(ls_status) Then
	iw_frame.SetMicroHelp("Product Status is a required field")
	dw_fab.SetFocus()
	Return
End If

 
iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('effective_date',ls_date)
iw_parentwindow.Event ue_set_data('product',ls_product)
iw_parentwindow.Event ue_set_data('plant_option',ls_option)
// Product state 10/03/02 ibdkdld
iw_parentwindow.Event ue_set_data('product_desc',dw_fab.uf_get_product_desc( ) )
iw_parentwindow.Event ue_set_data('state',dw_fab.uf_get_product_state( ) )
iw_parentwindow.Event ue_set_data('state_desc',dw_fab.uf_get_product_state_desc( ) )
iw_parentwindow.Event ue_set_data('status',dw_fab.uf_get_product_status( ) )
iw_parentwindow.Event ue_set_data('status_desc',dw_fab.uf_get_product_status_desc( ) )
//ib_inquire = True
ib_valid_return = True


Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen;call super::ue_postopen;String ls_temp

iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('effective_date')
dw_date.uf_set_effective_date(date(Message.StringParm))
// Product state 10/03/02 ibdkdld
iw_parentwindow.Event ue_get_data('product')
dw_fab.uf_set_product_code(Message.StringParm)
//dw_product.uf_setproductcode(Message.StringParm)
// Product state 10/03/02 ibdkdld
iw_parentwindow.Event ue_get_data('product_desc')
dw_fab.uf_set_product_desc(Message.StringParm)
// Product state 10/03/02 ibdkdld
iw_parentwindow.Event ue_get_data('state_desc')
dw_fab.uf_set_product_state_desc(Message.StringParm)
// Product state 10/03/02 ibdkdld
iw_parentwindow.Event ue_get_data('state')
dw_fab.uf_set_product_state(Message.StringParm)

iw_parentwindow.Event ue_get_data('status')
dw_fab.uf_set_product_status(Message.StringParm)
iw_parentwindow.Event ue_get_data('status_desc')
dw_fab.uf_set_product_status_desc(Message.StringParm)

iw_parentwindow.Event ue_get_data('plant_option')
ls_temp = Message.StringParm
dw_option.uf_set_plant_option(ls_temp) 
// BUG DEFECT 1672 SETFOCUS TO THE RIGHT ITEM IBDKDLD 11/25/02
If ls_temp = "A" then
	dw_plant.Hide()
	dw_fab.setfocus()
ELSE
	dw_plant.Show()
	dw_plant.setfocus()
END IF






end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")

// add ext pa date option
dw_date.uf_initilize('Ship Date:','EX')
dw_option.uf_enable(True)
//dw_plant.
end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_order_detail_inq
integer x = 1774
integer y = 504
integer taborder = 70
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_order_detail_inq
integer x = 1486
integer y = 504
integer taborder = 60
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_order_detail_inq
integer x = 1198
integer y = 504
integer taborder = 55
end type

type dw_date from u_effective_date within w_pas_order_detail_inq
integer x = 398
integer y = 380
integer width = 731
integer taborder = 44
boolean bringtotop = true
end type

type dw_option from u_plant_opt within w_pas_order_detail_inq
integer taborder = 11
boolean bringtotop = true
boolean border = false
end type

event constructor;call super::constructor;This.uf_enable(True)
end event

event itemchanged;call super::itemchanged;CHOOSE CASE data
	CASE 'A'
		dw_plant.Hide()
	CASE 'P'
		dw_plant.Show()
END CHOOSE

end event

type dw_fab from u_fab_product_code within w_pas_order_detail_inq
integer x = 439
integer y = 88
integer height = 276
integer taborder = 33
boolean bringtotop = true
end type

type dw_plant from u_plant within w_pas_order_detail_inq
boolean visible = false
integer x = 617
integer y = 12
integer taborder = 22
boolean bringtotop = true
end type

