HA$PBExportHeader$w_plant_constr_inq.srw
forward
global type w_plant_constr_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_plant_constr_inq
end type
type dw_product_code from u_fab_product_code within w_plant_constr_inq
end type
end forward

global type w_plant_constr_inq from w_base_response_ext
integer width = 1691
integer height = 676
long backcolor = 67108864
dw_plant dw_plant
dw_product_code dw_product_code
end type
global w_plant_constr_inq w_plant_constr_inq

type variables
boolean			ib_valid_to_close

w_plant_constr		iw_parent
end variables

on open;call w_base_response_ext::open;String				ls_plant, &
						ls_product

iw_parent = Message.PowerObjectParm

If Not IsValid(iw_parent) Then Close(This)

This.Title = iw_parent.Title + " Inquire"


end on

event ue_postopen;call super::ue_postopen;String		ls_plant, &
				ls_prod

SetRedraw(False)

ls_plant = iw_parent.wf_get_plant()
ls_prod = iw_parent.wf_get_prod()


If Not iw_frame.iu_string.nf_IsEmpty(ls_plant) Then 
	dw_plant.Reset()
	dw_plant.ImportString(ls_plant)
End If

If Not iw_frame.iu_string.nf_IsEmpty(ls_prod) Then 
	dw_product_code.Reset()
	dw_product_code.ImportString(ls_prod)
End If

SetRedraw(True)
end event

on w_plant_constr_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_product_code=create dw_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_product_code
end on

on w_plant_constr_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_product_code)
end on

event close;call super::close;If Not ib_valid_to_close Then Message.StringParm = ""
end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This,"")
end event

event ue_base_ok;call super::ue_base_ok;String	ls_plant, &
			ls_product, &
			ls_string, &
			ls_tab

ls_tab = "~t"

If dw_plant.AcceptText() = -1 Then
	dw_plant.SetFocus()
	return
End if

If dw_product_code.AcceptText() = -1 Then
	dw_product_code.SetFocus()
	Return
End if

ls_plant = dw_plant.nf_get_plant_code()
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End if

// IBDKEEM ** 08/08/2002 ** Product State
If not dw_product_code.uf_Validate() Then
	return
End if

ls_string = ls_plant + ls_tab + dw_plant.uf_get_plant_descr() + &
				ls_tab + dw_product_code.uf_exportstring( )

ib_valid_to_close = True
CloseWithReturn(This, ls_string)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_plant_constr_inq
integer x = 1221
integer y = 412
integer taborder = 50
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_plant_constr_inq
integer x = 910
integer y = 412
integer taborder = 40
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_plant_constr_inq
integer x = 599
integer y = 412
integer taborder = 30
end type

type dw_plant from u_plant within w_plant_constr_inq
integer x = 183
integer y = 12
integer width = 1431
integer taborder = 10
end type

type dw_product_code from u_fab_product_code within w_plant_constr_inq
integer x = 5
integer y = 104
integer width = 1499
integer taborder = 11
boolean bringtotop = true
end type

