HA$PBExportHeader$w_pas_to_ship_detail_inq.srw
forward
global type w_pas_to_ship_detail_inq from w_base_response_ext
end type
type sle_order from u_base_singlelineedit_ext within w_pas_to_ship_detail_inq
end type
type st_1 from statictext within w_pas_to_ship_detail_inq
end type
end forward

global type w_pas_to_ship_detail_inq from w_base_response_ext
integer y = 1000
integer width = 1733
integer height = 396
string title = "Transfer Order Ship Detail Inquire"
long backcolor = 67108864
sle_order sle_order
st_1 st_1
end type
global w_pas_to_ship_detail_inq w_pas_to_ship_detail_inq

on w_pas_to_ship_detail_inq.create
int iCurrent
call super::create
this.sle_order=create sle_order
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_order
this.Control[iCurrent+2]=this.st_1
end on

on w_pas_to_ship_detail_inq.destroy
call super::destroy
destroy(this.sle_order)
destroy(this.st_1)
end on

event ue_base_ok;call super::ue_base_ok;u_string_functions		lu_strings

If lu_strings.nf_IsEmpty(sle_order.text) Then
	iw_frame.SetMicroHelp("Order Number is a required field")
	sle_order.SetFocus()
	return
End If

iw_parentwindow.Event ue_set_data('order_number', sle_order.text)

iw_parentwindow.Event ue_set_data('ok', "true")

Close(This)
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('order_number')
sle_order.text = Message.StringParm

If sle_order.text > '' Then
	sle_order.SelectText(1, 7)
End If

sle_order.SetFocus()

iw_parentwindow.Event ue_set_data('close', "true")

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "False")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_to_ship_detail_inq
boolean visible = false
integer x = 1938
integer y = 448
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_to_ship_detail_inq
integer x = 1426
integer y = 160
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_to_ship_detail_inq
integer x = 1426
integer y = 32
integer taborder = 20
end type

type sle_order from u_base_singlelineedit_ext within w_pas_to_ship_detail_inq
integer x = 658
integer y = 64
integer width = 329
integer height = 96
integer taborder = 10
boolean bringtotop = true
textcase textcase = upper!
integer limit = 7
end type

type st_1 from statictext within w_pas_to_ship_detail_inq
integer x = 219
integer y = 64
integer width = 402
integer height = 64
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Order Number:"
boolean focusrectangle = false
end type

