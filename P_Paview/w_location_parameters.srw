HA$PBExportHeader$w_location_parameters.srw
$PBExportComments$add reduction days
forward
global type w_location_parameters from w_base_sheet_ext
end type
type tab_1 from tab within w_location_parameters
end type
type tabpage_2 from userobject within tab_1
end type
type dw_shortage_queue from u_base_dw_ext within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_shortage_queue dw_shortage_queue
end type
type tabpage_1 from userobject within tab_1
end type
type dw_cutoff_time from u_base_dw_ext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_cutoff_time dw_cutoff_time
end type
type tabpage_3 from userobject within tab_1
end type
type dw_plan_tran_parms from u_base_dw_ext within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_plan_tran_parms dw_plan_tran_parms
end type
type tab_1 from tab within w_location_parameters
tabpage_2 tabpage_2
tabpage_1 tabpage_1
tabpage_3 tabpage_3
end type
end forward

global type w_location_parameters from w_base_sheet_ext
integer x = 4
integer y = 3
integer width = 3408
integer height = 1523
string title = "Location Parameters"
long backcolor = 12632256
tab_1 tab_1
end type
global w_location_parameters w_location_parameters

type variables
u_pas203		iu_pas203
u_ws_pas4	iu_ws_pas4
DataStore	ids_locations

String		is_plant_type, &
		is_protection_status

s_error		istr_error_info

DataWindowChild  	idwc_plant
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();Date			ldt_input_date

Integer		li_count

String		ls_plants, &
				ls_output_string, &
				ls_input_plant, &
				ls_plant_type[], &
				ls_input_inv_type, &
				ls_input_shortage_queue, &
				ls_temp, &
				ls_description,ls_pt_cutoff_period,ls_chgord_cutoff_time,ls_pt_primary_prod_plant

Time			lt_input_cutoff_time

Long			ll_RowCount, &
				ll_row, &
				ll_input_cutoff_hours,ll_input_shell_days,ll_chgord_cutoff_days,ll_pt_reduction_days, ll_holdover_hours, ll_testhold_cutoff_hrs
date 			ld_pt_cutoff_date

IF Not Super::wf_retrieve() Then Return False

  SELECT tutltypes.type_desc  
    INTO :ls_description  
    FROM tutltypes  
   WHERE ( tutltypes.record_type = 'PLTGROUP' ) AND  
         ( tutltypes.type_short_desc = :is_plant_type )   ;

This.Title = 'Location Parameters ' + ls_description

ls_temp = is_plant_type
for li_count = 1 to len(Trim(is_plant_type))
	ls_plant_type[li_count] = left(ls_temp, 1)
	ls_temp = Right(ls_temp, len(ls_temp) - 1)
Next
	
ids_locations.SetTransObject(SQLCA)
ids_locations.Retrieve(ls_plant_type[])

ls_plants = ids_locations.object.datawindow.data

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp63fr_inq_location_parameters"
istr_error_info.se_message = Space(71)

tab_1.tabpage_1.dw_cutoff_time.SetRedraw(False)
tab_1.tabpage_1.dw_cutoff_time.Reset()

/*If iu_pas203.nf_pasp63br_inq_location_parm(istr_error_info, ls_output_string) < 0 Then
	tab_1.tabpage_1.dw_cutoff_time.SetRedraw(True)
	Return False
End If
*/
If iu_ws_pas4.NF_PASP63FR(istr_error_info, ls_output_string) < 0 Then
	tab_1.tabpage_1.dw_cutoff_time.SetRedraw(True)
	Return False
End If


tab_1.tabpage_1.dw_cutoff_time.ImportString(ls_plants)

ll_rowcount = tab_1.tabpage_1.dw_cutoff_time.RowCount()


Do While Not iw_frame.iu_string.nf_IsEmpty(ls_output_string)
	ls_input_plant = iw_frame.iu_string.nf_GetToken(ls_output_string, '~t')
	ll_input_cutoff_hours = Long(iw_frame.iu_string.nf_GetToken(ls_output_string, '~t'))
	lt_input_cutoff_time = Time(iw_frame.iu_string.nf_GetToken(ls_output_string, '~t'))
	ls_input_inv_type = iw_frame.iu_string.nf_GetToken(ls_output_string, '~t')
	ls_input_shortage_queue = iw_frame.iu_string.nf_GetToken(ls_output_string, '~t')
	ll_input_shell_days = Long(iw_frame.iu_string.nf_GetToken(ls_output_string, '~t'))
	ld_pt_cutoff_date = date(iw_frame.iu_string.nf_GetToken(ls_output_string, '~t'))
//	ls_temp = iw_frame.iu_string.nf_GetToken(ls_output_string, '~t')
//	IF ls_temp = '00/00/0000' Then
// 		messagebox('not a date', ls_temp)
//		ld_pt_cutoff_date = date('01/01/0001' )
//	Else	 
//		ld_pt_cutoff_date = date(ls_temp)
//	End If	
	ls_pt_cutoff_period = iw_frame.iu_string.nf_GetToken(ls_output_string, '~t')
// add another pt value ibdkdld
	ll_pt_reduction_days = Long(iw_frame.iu_string.nf_GetToken(ls_output_string, '~t'))

//removed this tab ibdkdld
//	ls_chgord_cutoff_time = STRING(Time(iw_frame.iu_string.nf_GetToken(ls_output_string, '~t')),'HH:MM')
//	ll_chgord_cutoff_days = Long(iw_frame.iu_string.nf_GetToken(ls_output_string, '~r~n'))

// ibdkdld add new column ibdkdld 12/18/02
	ls_pt_primary_prod_plant = iw_frame.iu_string.nf_GetToken(ls_output_string, '~t')
	ll_holdover_hours = Long(iw_frame.iu_string.nf_GetToken(ls_output_string, '~t'))
// ibdkslh add new column 12/2017 - SR25459	
	ll_testhold_cutoff_hrs = Long(iw_frame.iu_string.nf_GetToken(ls_output_string, '~r~n'))


	ll_row = tab_1.tabpage_1.dw_cutoff_time.Find("plant_code = '"+ ls_input_plant +"'", 1, ll_rowcount)
	
	If ll_row > 0 Then
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'cutoff_hours', ll_input_cutoff_hours)
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'cutoff_Time', lt_input_cutoff_time)
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'shortage_queue_ind', ls_input_shortage_queue)
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'type_of_invent', ls_input_inv_type)
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'tran_shell_days_out', ll_input_shell_days)
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'pt_cutoff_date', ld_pt_cutoff_date)
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'pt_cutoff_period', ls_pt_cutoff_period)
// add another pt value ibdkdld
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'pt_reduction_days', ll_pt_reduction_days)
//removed this tab ibdkdld		
//		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'chgord_cutoff_time', ls_chgord_cutoff_time)
//		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'chgord_cutoff_days', ll_chgord_cutoff_days)

// ibdkdld add new column ibdkdld 12/18/02
// ibdkslh add new column 12/2017 - SR25459
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'pt_primary_prod_plant', ls_pt_primary_prod_plant)
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'holdover_hours', ll_holdover_hours)
		tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'testhold_cutoff_hrs', ll_testhold_cutoff_hrs)
	End if
Loop
For ll_row = 1 to tab_1.tabpage_1.dw_cutoff_time.RowCount()
	tab_1.tabpage_1.dw_cutoff_time.SetItem(ll_row, 'protection_status', is_protection_status) 
Next

// jxr - nov 2018 -- filter out inactive locations by testing if type_of_invent is null
tab_1.tabpage_1.dw_cutoff_time.SetFilter("NOT IsNull(type_of_invent)" )
tab_1.tabpage_1.dw_cutoff_time.Filter()

tab_1.tabpage_1.dw_cutoff_time.SetRedraw(True)
tab_1.tabpage_1.dw_cutoff_time.ResetUpdate()
tab_1.tabpage_2.dw_shortage_queue.ResetUpdate()
tab_1.tabpage_3.dw_plan_tran_parms.ResetUpdate()
//tab_1.tabpage_4.dw_chgorder_parms.ResetUpdate()

Return True
end function

public function boolean wf_update ();Integer			li_changed_row[], &
					li_count

Time				lt_cutoff_time

String			ls_input_string, &
					ls_output_string, &
					ls_temp

Long				ll_row


If tab_1.tabpage_1.dw_cutoff_time.AcceptText() < 1 Then Return False
If tab_1.tabpage_2.dw_shortage_queue.AcceptText() < 1 Then Return False
If tab_1.tabpage_3.dw_plan_tran_parms.AcceptText() < 1 Then Return False
//If tab_1.tabpage_4.dw_chgorder_parms.AcceptText() < 1 Then Return False

ll_row = tab_1.tabpage_1.dw_cutoff_time.GetNextModified(0, primary!)
If ll_row = 0 Then
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End If

li_count = 0
ls_input_string = ''
Do 
	ls_input_string += tab_1.tabpage_1.dw_cutoff_time.GetItemString(ll_row, 'plant_code') + '~t'

	ls_temp = String(tab_1.tabpage_1.dw_cutoff_time.GetItemNumber(ll_row, 'cutoff_hours'))
	If IsNull(ls_temp) Then ls_temp = '0'
	ls_input_string += ls_temp + '~t'
	
	lt_cutoff_time = tab_1.tabpage_1.dw_cutoff_time.GetItemTime(ll_row, 'cutoff_time')
	If IsNull(lt_cutoff_time) Then
		lt_cutoff_time = Time('00:00')
	End If
	ls_input_string += String(lt_cutoff_time, 'hh:mm:ss') + '~t'
	
	ls_temp = tab_1.tabpage_1.dw_cutoff_time.GetItemString(ll_row, 'type_of_invent')
	If IsNull(ls_temp) Then ls_temp = ''
	ls_input_string += ls_temp + '~t'
	
	ls_temp = String(tab_1.tabpage_1.dw_cutoff_time.GetItemNumber(ll_row, 'tran_shell_days_out'))
	If IsNull(ls_temp) Then ls_temp = '0'
	ls_input_string += ls_temp + '~t'
	
	ls_temp = tab_1.tabpage_1.dw_cutoff_time.GetItemString(ll_row, 'shortage_queue_ind')
	If IsNull(ls_temp) Then ls_temp = ''
	ls_input_string += ls_temp + '~t'
	
	ls_temp = string(tab_1.tabpage_3.dw_plan_tran_parms.GetItemdate(ll_row, 'pt_cutoff_date'),'yyyy-mm-dd')
	If IsNull(ls_temp) Then ls_temp = ''
	ls_input_string += ls_temp + '~t'
	
	ls_temp = tab_1.tabpage_3.dw_plan_tran_parms.GetItemString(ll_row, 'pt_cutoff_period')
	If IsNull(ls_temp) Then ls_temp = ''
	ls_input_string += ls_temp + '~t'
	
// add another pt value ibdkdld
	ls_temp = string(tab_1.tabpage_3.dw_plan_tran_parms.GetItemNumber(ll_row, 'pt_reduction_days'))
	If IsNull(ls_temp) Then ls_temp = '0'
	ls_input_string += ls_temp + '~t'

// removed tab page ibdkdld
//	lt_cutoff_time = Time(tab_1.tabpage_4.dw_chgorder_parms.GetItemString(ll_row, 'chgord_cutoff_time'))
//	If IsNull(lt_cutoff_time) Then
//		lt_cutoff_time = Time('00:00')
//	End If
//	ls_input_string += String(lt_cutoff_time, 'hh:mm:ss') + '~t'
//	
//	ls_temp = String(tab_1.tabpage_4.dw_chgorder_parms.GetItemNumber(ll_row, 'chgord_cutoff_days'))
//	If IsNull(ls_temp) Then ls_temp = '0'
//	ls_input_string += ls_temp + '~r~n'
	
// add another pt value ibdkdld 12/17/02
	ls_temp = tab_1.tabpage_3.dw_plan_tran_parms.GetItemstring(ll_row, 'pt_primary_prod_plant')
	If IsNull(ls_temp) Then ls_temp = '000'
	ls_input_string += ls_temp + '~t'
	
	ls_temp = String(tab_1.tabpage_1.dw_cutoff_time.GetItemNumber(ll_row, 'holdover_hours'))
	If IsNull(ls_temp) Then ls_temp = '0'
	ls_input_string += ls_temp + '~t'

    // ibdkslh add new column 12/2017 - SR25459
	ls_temp = String(tab_1.tabpage_1.dw_cutoff_time.GetItemNumber(ll_row, 'testhold_cutoff_hrs'))
	If IsNull(ls_temp) Then ls_temp = '0'
	ls_input_string += ls_temp + '~r~n'

	
	li_count ++
	li_changed_row[li_count] = ll_row
	ll_row = tab_1.tabpage_1.dw_cutoff_time.GetNextModified(ll_row, primary!)
Loop While ll_row > 0

/*If iu_pas203.nf_pasp64br_upd_location_parm(istr_error_info, ls_input_string) <> 0 Then
	Return False
End If
*/
If iu_ws_pas4.NF_PASP64FR(istr_error_info, ls_input_string) <> 0 Then
	Return False
End If
iw_frame.SetMicroHelp('Modification Successful')
tab_1.tabpage_1.dw_cutoff_time.ResetUpdate()
Return True

end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

If tab_1.SelectedTab = 2 Then
	If is_protection_status = 'Y' Then
		iw_frame.im_menu.mf_Disable('m_save')
	Else
		iw_frame.im_menu.mf_Enable('m_save')
	End If
End If
end event

on w_location_parameters.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_location_parameters.destroy
call super::destroy
destroy(this.tab_1)
end on

event close;call super::close;Destroy ids_locations
Destroy iu_pas203

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

end event

event ue_fileprint;call super::ue_fileprint;tab_1.tabpage_1.dw_cutoff_time.Print()
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant_type'
		message.StringParm = is_plant_type
End choose

end event

event ue_postopen;call super::ue_postopen;String				ls_group_id, &
						ls_add_auth, &
						ls_save_auth

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Location Parameters"
istr_error_info.se_user_id 		= sqlca.userid

is_inquire_window_name = 'w_plant_type_inq'

iu_pas203 = Create u_pas203
iu_ws_pas4 = Create u_ws_pas4

ids_locations = Create DataStore
ids_locations.DataObject = 'd_location_code_name'

ls_group_id = iw_frame.iu_netwise_data.is_groupid

  SELECT group_profile.add_auth
    INTO :ls_add_auth 
    FROM group_profile  
   WHERE ( group_profile.appid = 'PRD' ) AND  
         ( group_profile.group_id = :ls_group_id ) AND  
         ( group_profile.window_name = 'TABPAGE_LOC_PARM' ) AND  
         ( group_profile.menuid = 'W_LOCATION_PARAMETERS' )   ;

  SELECT group_profile.modify_auth
    INTO :ls_save_auth 
    FROM group_profile  
   WHERE ( group_profile.appid = 'PRD' ) AND  
         ( group_profile.group_id = :ls_group_id ) AND  
         ( group_profile.window_name = 'W_LOCATION_PARAMETERS' ) AND  
         ( group_profile.menuid = 'M_LOCATIONPARAMETERS' )   ;

If ls_add_auth = 'N' Then
	is_protection_status = 'Y'
Else
	is_protection_status = 'N'
End If

If tab_1.SelectedTab = 2 Then
	IF is_protection_status = 'Y' Then
		iw_frame.im_menu.mf_disable('m_save')
	End If
End If

wf_retrieve()
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant_type'
		is_plant_type = as_value
End Choose
end event

event resize;call super::resize;constant integer li_tab_x		= 20
constant integer li_tab_y		= 20
  
constant integer li_dw_x		= 70
constant integer li_dw_y		= 150

  
  
tab_1.width	= newwidth - li_tab_x
tab_1.height = newheight - li_tab_y

tab_1.tabpage_1.dw_cutoff_time.width = newwidth - li_dw_x
tab_1.tabpage_1.dw_cutoff_time.height = newheight - li_dw_y

tab_1.tabpage_2.dw_shortage_queue.width = newwidth - li_dw_x
tab_1.tabpage_2.dw_shortage_queue.height = newheight - li_dw_y

tab_1.tabpage_3.dw_plan_tran_parms.width = newwidth - li_dw_x
tab_1.tabpage_3.dw_plan_tran_parms.height = newheight - li_dw_y


end event

type tab_1 from tab within w_location_parameters
event create ( )
event destroy ( )
integer x = 7
integer y = 13
integer width = 3343
integer height = 1405
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean raggedright = true
integer selectedtab = 1
tabpage_2 tabpage_2
tabpage_1 tabpage_1
tabpage_3 tabpage_3
end type

on tab_1.create
this.tabpage_2=create tabpage_2
this.tabpage_1=create tabpage_1
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_2,&
this.tabpage_1,&
this.tabpage_3}
end on

on tab_1.destroy
destroy(this.tabpage_2)
destroy(this.tabpage_1)
destroy(this.tabpage_3)
end on

event selectionchanged;//If newindex = 2 Then
//	If is_protection_status = 'Y' Then
//		iw_frame.im_menu.mf_disable('m_save')
//	End If
//Else
//	iw_frame.im_menu.mf_enable('m_save')
//End If

Parent.TriggerEvent(Activate!)
end event

type tabpage_2 from userobject within tab_1
integer x = 15
integer y = 90
integer width = 3313
integer height = 1302
long backcolor = 67108864
string text = "Shortage Queue"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_shortage_queue dw_shortage_queue
end type

on tabpage_2.create
this.dw_shortage_queue=create dw_shortage_queue
this.Control[]={this.dw_shortage_queue}
end on

on tabpage_2.destroy
destroy(this.dw_shortage_queue)
end on

type dw_shortage_queue from u_base_dw_ext within tabpage_2
event ue_post_itemchanged ( long row,  dwobject dwo )
integer width = 1499
integer height = 1245
integer taborder = 10
string dataobject = "d_shortage_queue"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_post_itemchanged;call super::ue_post_itemchanged;String				ls_original, &
						ls_new


ls_original = This.GetItemString(row, 'shortage_queue_ind', primary!, True)
ls_new = This.GetItemString(row, 'shortage_queue_ind')
	
If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
	This.SetItemStatus(row, 'shortage_queue_ind', primary!, notmodified!)
End If

end event

event itemchanged;call super::itemchanged;dwitemstatus ls_temp 

If dwo.name = 'shortage_queue_ind' Then
	This.EVENT POST ue_post_itemchanged(row, dwo)
End if

end event

event rbuttondown;call super::rbuttondown;m_shortage_queue	lm_popup

lm_popup = Create m_shortage_queue

lm_popup.m_shortagequeue.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup


end event

event ue_postconstructor;call super::ue_postconstructor;tab_1.tabpage_1.dw_cutoff_time.ShareData(tab_1.tabpage_2.dw_shortage_queue)

end event

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 15
integer y = 90
integer width = 3313
integer height = 1302
long backcolor = 67108864
string text = "Parameters"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
dw_cutoff_time dw_cutoff_time
end type

on tabpage_1.create
this.dw_cutoff_time=create dw_cutoff_time
this.Control[]={this.dw_cutoff_time}
end on

on tabpage_1.destroy
destroy(this.dw_cutoff_time)
end on

type dw_cutoff_time from u_base_dw_ext within tabpage_1
integer y = 13
integer width = 3288
integer height = 1245
integer taborder = 10
string dataobject = "d_location_parameters"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;nvuo_pa_business_rules pa_rules
long ll_data,ll_temp

Choose Case dwo.name
	Case 'cutoff_hours'
		If Long(data) < 0 Then
			iw_frame.SetMicrohelp('Cutoff Hours must be a positive number')
			Return 1
		End If
	Case 'holdover_hours'
		If Long(data) < 0 Then
			iw_frame.SetMicrohelp('Holdover Hours must be a positive number')
			Return 1
		End If	
	Case 'tran_shell_days_out'
		ll_data = Long(data)
		If ll_data < 0 Then
			iw_frame.SetMicrohelp('Transfer Shell Days Out must be a positive number')
			Return 1
		End If
		ll_temp = pa_rules.uf_check_pa_daily_range()
		If ll_temp < ll_data Then
			iw_frame.SetMicrohelp('Transfer Shell Days Out can not be greater than PA Daily Range which is currently set at ' + String(ll_temp))
			Return 1
		End If

End Choose



end event

event itemerror;call super::itemerror;Return 1
end event

event rbuttondown;call super::rbuttondown;m_tran_shell	lm_popup

If dwo.name = 'tran_shell_days_out' Then
	lm_popup = Create m_tran_shell
	dw_cutoff_time.SetRow ( row )
	lm_popup.m_transhell.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())
	Destroy lm_popup
End IF

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	ldwc_temp

This.GetChild('type_of_invent', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve('PAINVTYP')
end event

type tabpage_3 from userobject within tab_1
integer x = 15
integer y = 90
integer width = 3313
integer height = 1302
long backcolor = 67108864
string text = "Planned Transfer"
long tabtextcolor = 33554432
long picturemaskcolor = 553648127
dw_plan_tran_parms dw_plan_tran_parms
end type

on tabpage_3.create
this.dw_plan_tran_parms=create dw_plan_tran_parms
this.Control[]={this.dw_plan_tran_parms}
end on

on tabpage_3.destroy
destroy(this.dw_plan_tran_parms)
end on

type dw_plan_tran_parms from u_base_dw_ext within tabpage_3
event ue_post_itemchanged ( string name,  integer row,  dwobject dwo )
integer y = 3
integer width = 2326
integer height = 1280
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_plan_tran_parms"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_post_itemchanged(string name, integer row, dwobject dwo);String				ls_original, &
						ls_new

choose case name
	case 'date'
		ls_original = String(This.GetItemdate(row, 'pt_cutoff_date', primary!, True))
		ls_new = String(This.GetItemdate(row, 'pt_cutoff_date'))
		
		If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
			This.SetItemStatus(row, 'pt_cutoff_date', primary!, notmodified!)
		End If
	case 'period'
		ls_original = This.GetItemString(row, 'pt_cutoff_period', primary!, True)
		ls_new = This.GetItemString(row, 'pt_cutoff_period') 
		
		If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
			This.SetItemStatus(row, 'pt_cutoff_period', primary!, notmodified!)
		End If
//	case 'plant'
	
end choose



If name = 'date' Then
	ls_original = String(This.GetItemdate(row, 'pt_cutoff_date', primary!, True))
	ls_new = String(This.GetItemdate(row, 'pt_cutoff_date'))
	
	If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
		This.SetItemStatus(row, 'pt_cutoff_date', primary!, notmodified!)
	End If
Else
	ls_original = This.GetItemString(row, 'pt_cutoff_period', primary!, True)
	ls_new = This.GetItemString(row, 'pt_cutoff_period') 
	
	If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
		This.SetItemStatus(row, 'pt_cutoff_period', primary!, notmodified!)
	End If
End if
end event

event itemchanged;call super::itemchanged;dwitemstatus ls_temp 
DataWindowChild 		ldwc_child
Long 	ll_find


If Dwo.name = 'pt_reduction_days' Then
	If long(data) < 0 Then
			//If iw_frame.		
			iw_frame.SetMicrohelp('PT Reduction Days must be a positive number')
			This.SelectText(1,100)
			Return 1
	End If
	If Not isnumber(data) Then
			iw_frame.SetMicrohelp('PT Reduction Days must be a numeric')
			This.SelectText(1,100)
			Return 1
	End If
End If	

If dwo.name = 'pt_cutoff_date' Then
	If isnull(data)  then
		iw_frame.SetMicroHelp("PT Cut Date must be greater than or equal to 01/01/0001")
		This.SelectText(1, 100)
		Return 1
	End if		
	This.EVENT POST ue_post_itemchanged('date',row, dwo)
End if

If dwo.name = 'pt_cutoff_period' Then
	// Insure data is valid
	If Not IsNumber(data) or isnull(data) Then
		iw_frame.SetMicroHelp("PT Cut Off Period must be numeric")
		This.SelectText(1, 100)
		Return 1
	End If
	This.GetChild("pt_cutoff_period", ldwc_child)
	ll_find = ldwc_child.Find ( "type_code = '" + data + "'", 1, ldwc_child.RowCount() )
	If ll_find = 0 then
		iw_frame.SetMicroHelp("The PT Cut Off Period is not a valid period")
		This.SelectText(1, 100)
		Return 1
	End If
	This.EVENT POST ue_post_itemchanged('period',row, dwo)
End if
// ibdkdld 12/17/02 add prod
If dwo.name = "pt_primary_prod_plant" then
		ll_find = idwc_plant.Find( "location_code='"+ data +"'", 1, idwc_plant.RowCount()) 
		IF ll_find <= 0 THEN
			iw_frame.SetMicroHelp(data + " is not a valid Plant Code")
			This.selecttext(1,100)
			Return 1
		END IF
	This.EVENT POST ue_post_itemchanged('plant',row, dwo)
End if

end event

event itemerror;call super::itemerror;Return 2
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild	dwch_period

integer rtncode

tab_1.tabpage_1.dw_cutoff_time.ShareData(tab_1.tabpage_3.dw_plan_tran_parms)

rtncode = This.GetChild('pt_cutoff_period', dwch_period)
IF rtncode = -1 THEN 
	MessageBox( "Error", "pt_cutoff_period not a DataWindowChild") 
end if	
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
dwch_period.SetTransObject(SQLCA)
dwch_period.Retrieve()


// ibdkdld 12/17/02
rtncode = This.GetChild ( "pt_primary_prod_plant", idwc_Plant )
IF rtncode = -1 THEN 
	MessageBox( "Error", "Pt_primary_prod_plant is not a DataWindowChild!") 
end if	
idwc_Plant.SetTransObject(SQLCA)
rtncode = idwc_Plant.retrieve( )


end event

