HA$PBExportHeader$w_epa_view_new.srw
forward
global type w_epa_view_new from w_base_sheet_ext
end type
type st_1 from statictext within w_epa_view_new
end type
type st_total_qty from statictext within w_epa_view_new
end type
type dw_detail from u_base_dw_ext within w_epa_view_new
end type
type dw_header from u_base_dw_ext within w_epa_view_new
end type
end forward

global type w_epa_view_new from w_base_sheet_ext
integer width = 1765
integer height = 1592
string title = "EPA View New"
long backcolor = 67108864
st_1 st_1
st_total_qty st_total_qty
dw_detail dw_detail
dw_header dw_header
end type
global w_epa_view_new w_epa_view_new

type variables
u_ws_pas3	iu_ws_pas3

s_error		istr_error_info

DataStore			ids_print

Long		il_color,&
		il_SelectedColor,&
		il_SelectedTextColor

end variables

forward prototypes
public function boolean wf_retrieve ()
end prototypes

public function boolean wf_retrieve ();long			ll_rec_count, &
				ll_row,&
				ll_count,&
				ll_total_count,&
				ll_total_qty

String		ls_plant, &
			ls_product_code, &
			ls_state, &
			ls_period, &
			ls_date, &
			ls_oldest_date, &
			ls_available, &
			ls_header, &
			ls_detail

If dw_header.AcceptText() = -1 Then return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 
dw_detail.Reset ( )

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "uf_pasp13gr"
istr_error_info.se_message = Space(71)

ls_plant = Trim(dw_header.GetItemString( 1, "location_code"))
ls_product_code = Trim(dw_header.GetItemString( 1, "fab_product_code"))
ls_state = Trim(dw_header.GetItemString( 1, "product_state"))
ls_period = Trim(dw_header.GetItemString( 1, "period"))
ls_date = Trim(String(dw_header.GetItemDate( 1, "date"), "yyyy-mm-dd"))
ls_oldest_date = Trim(String(dw_header.GetItemDate( 1, "oldest_production_date"), "yyyy-mm-dd"))
ls_available = Trim(dw_header.GetItemString( 1, "available_to_ship"))

ls_header = ls_product_code + '~t' + &
				ls_state + '~t' + &
				ls_date + '~t' + &
				ls_period + '~t' + &
				ls_plant + '~t' + &
				ls_oldest_date + '~t' + &
				ls_available
				
If iu_ws_pas3.uf_pasp13gr(istr_error_info, ls_header, ls_detail) < 0 Then
	This.SetRedraw(True) 
	Return False
End If									

ll_total_count = dw_detail.ImportString(ls_detail)
	
If ll_total_count > 0 Then
	SetMicroHelp(String(ll_total_count) + " Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

//This.SetRedraw(True) 

dw_detail.ResetUpdate()
dw_detail.Sort()
This.SetRedraw(True) 
dw_detail.SetFocus()

ll_total_qty = 0
For ll_count = 1 to ll_total_count
	ll_total_qty += dw_detail.GetItemNumber(ll_count, 'qty_available')
Next

st_total_qty.text = string(ll_total_qty)

ids_print.Modify("plant_t.Text = 'Plant:  " + dw_header.GetItemString(1, "location_code") + " " + &
			dw_header.GetItemString(1, "location_name") + "' " + &
			"product_t.Text = 'Product:  " + dw_header.GetItemString(1, "fab_product_code") + " " + &
			dw_header.GetItemString(1, "fab_product_description") + "' " + &
			"latest_arrival_date_t.Text = 'Latest Arrival Date:  " + &
			String(dw_header.GetItemDate(1, "date")) + "' " + &
			"product_state_t.Text = 'Product State:  " + &
			dw_header.GetItemString(1, "product_state") + "' " + &
			"period_t.Text = 'Period:  " + &
			dw_header.GetItemString(1, "period") + "'")

Return True
end function

on w_epa_view_new.create
int iCurrent
call super::create
this.st_1=create st_1
this.st_total_qty=create st_total_qty
this.dw_detail=create dw_detail
this.dw_header=create dw_header
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.st_total_qty
this.Control[iCurrent+3]=this.dw_detail
this.Control[iCurrent+4]=this.dw_header
end on

on w_epa_view_new.destroy
call super::destroy
destroy(this.st_1)
destroy(this.st_total_qty)
destroy(this.dw_detail)
destroy(this.dw_header)
end on

event ue_postopen;call super::ue_postopen;Environment	le_env

//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_epa_view_new"
istr_error_info.se_user_id 		= sqlca.userid

GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
// 	il_SelectedColor = GetSysColor(13)
//	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

dw_header.ib_updateable = False
dw_detail.ib_updateable = False

// open inquire window
is_inquire_window_name = 'w_epa_view_new_inq'

iu_ws_pas3 = Create u_ws_pas3

ids_print = Create u_print_datastore

ids_print.DataObject = 'd_epa_view_print'

dw_detail.ShareData(ids_print)

wf_retrieve()
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant' 
		dw_header.SetItem(1, "location_code", as_value)
	Case 'plant_desc' 
		dw_header.SetItem(1, "location_name", as_value)
	Case 'product_code' 
		dw_header.SetItem(1, "fab_product_code", as_value)
	Case 'product_desc' 
		dw_header.SetItem(1, "fab_product_description", as_value)
	Case 'state' 
		dw_header.SetItem(1, "product_state", as_value)
	Case 'period' 
		dw_header.SetItem(1, "period", as_value)
	Case 'date' 
		dw_header.SetItem(1, "date", Date(as_value))	
	Case 'oldest_date' 
		dw_header.SetItem(1, "oldest_production_date", Date(as_value))	
	Case 'available' 
		dw_header.SetItem(1, "available_to_ship", as_value)
End Choose
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant' 
		Message.StringParm = dw_header.GetItemString(1, "location_code")
	Case 'plant_desc' 
		Message.StringParm = dw_header.GetItemString(1, "location_name")
	Case 'product_code' 
		Message.StringParm = dw_header.GetItemString(1, "fab_product_code")
	Case 'product_desc' 
		Message.StringParm = dw_header.GetItemString(1, "fab_product_description")
	Case 'state' 
		Message.StringParm = dw_header.GetItemString(1, "product_state")
	Case 'period' 
		Message.StringParm = dw_header.GetItemString(1, "period")	
	Case 'date' 
		Message.StringParm = String(dw_header.GetItemDate(1, "date"))
	Case 'oldest_date' 
		Message.StringParm = String(dw_header.GetItemDate(1, "oldest_production_date"))	
	Case 'available' 
		Message.StringParm = dw_header.GetItemString(1, "available_to_ship")	
End Choose
end event

event resize;call super::resize;integer li_y2	= 1200
integer li_y		= 368

dw_detail.height	= newheight - li_y

//st_1. y = newheight - li_y2
//st_total_qty. y = newheight - li_y2

dw_detail.Modify("l_4.y2 = " + string(newheight) + " - " + string(li_y2))
dw_detail.Modify("l_5.y2 = " + string(newheight))
end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return

ids_print.Print()
end event

type st_1 from statictext within w_epa_view_new
boolean visible = false
integer x = 46
integer y = 1424
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Total"
boolean focusrectangle = false
end type

type st_total_qty from statictext within w_epa_view_new
boolean visible = false
integer x = 818
integer y = 1424
integer width = 343
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "0"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_detail from u_base_dw_ext within w_epa_view_new
integer x = 5
integer y = 356
integer width = 1664
integer height = 1120
integer taborder = 20
string dataobject = "d_epa_view_detail"
boolean border = false
end type

type dw_header from u_base_dw_ext within w_epa_view_new
integer x = 32
integer y = 28
integer width = 1650
integer height = 312
integer taborder = 10
string dataobject = "d_epa_view_header"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

