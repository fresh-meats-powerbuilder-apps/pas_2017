HA$PBExportHeader$w_remove_pt.srw
$PBExportComments$add reduction days
forward
global type w_remove_pt from w_base_sheet_ext
end type
type dw_plants from u_base_dw_ext within w_remove_pt
end type
end forward

global type w_remove_pt from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 1344
integer height = 1128
string title = "Remove Unneeded Demand PT"
long backcolor = 12632256
dw_plants dw_plants
end type
global w_remove_pt w_remove_pt

type variables
u_pas201		iu_pas201

u_ws_pas3		iu_ws_pas3

s_error		istr_error_info


end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();Integer		li_count

String		ls_output_string

Long			ll_RowCount, &
				ll_row
				

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp49cr_inq_dest_plants"
istr_error_info.se_message = Space(71)

dw_plants.SetRedraw(False)
dw_plants.Reset()

//If iu_pas201.nf_pasp49cr_inq_dest_plants(istr_error_info, ls_output_string) < 0 Then
If iu_ws_pas3.uf_pasp49gr(istr_error_info, ls_output_string) < 0 Then
	Return False
End If

dw_plants.ImportString(ls_output_string)

ll_rowcount = dw_plants.RowCount()

dw_plants.SetRedraw(True)
dw_plants.ResetUpdate()

Return True
end function

public function boolean wf_update ();Integer			li_changed_row[], &
					li_count

Time				lt_cutoff_time

String			ls_input_string, &
					ls_output_string, &
					ls_temp, &
					ls_header_string

Long				ll_row


If dw_plants.AcceptText() < 1 Then Return False

ll_row = dw_plants.GetSelectedRow(0)
If ll_row = 0 Then
	iw_frame.SetMicroHelp('No Rows Selected')
	Return False
End If
ls_header_string = ''

li_count = 0
ls_input_string = ''

Do 
	//ls_input_string += dw_plants.GetItemString(ll_row, 'plant_code') + '~t'
	ls_input_string += dw_plants.GetItemString(ll_row, 'plant_code') 
	ls_input_string += '~r~n'
	dw_plants.SelectRow(ll_row, FALSE)
	ll_row = dw_plants.GetSelectedRow(0)
Loop While ll_row > 0

//If not iu_pas201.nf_pasp50cr_remove_demand_pts(istr_error_info, ls_input_string, ls_output_string, ls_header_string) Then
If not iu_ws_pas3.uf_pasp50gr(istr_error_info, ls_input_string, ls_output_string, ls_header_string) Then
	Return False
End If


iw_frame.SetMicroHelp("Update Successful")

Return True

end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_save')
	
end event

on w_remove_pt.create
int iCurrent
call super::create
this.dw_plants=create dw_plants
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plants
end on

on w_remove_pt.destroy
call super::destroy
destroy(this.dw_plants)
end on

event close;call super::close;Destroy iu_pas201
Destroy iu_ws_pas3

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

end event

event ue_postopen;call super::ue_postopen;String				ls_group_id, &
						ls_add_auth, &
						ls_save_auth

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Remove Unneeded Demand PT"
istr_error_info.se_user_id 		= sqlca.userid

iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3

wf_retrieve()
end event

event resize;call super::resize;constant integer li_tab_x		= 20
constant integer li_tab_y		= 20
  
constant integer li_dw_x		= 70
constant integer li_dw_y		= 150
  
  
dw_plants.width = newwidth - li_dw_x
dw_plants.height = newheight - li_dw_y


end event

type dw_plants from u_base_dw_ext within w_remove_pt
integer y = 4
integer width = 1307
integer height = 976
integer taborder = 20
string dataobject = "d_dest_plants"
boolean vscrollbar = true
end type

event clicked;call super::clicked;IF row > 0 Then
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If




end event

