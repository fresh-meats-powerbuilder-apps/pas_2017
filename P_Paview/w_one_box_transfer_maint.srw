HA$PBExportHeader$w_one_box_transfer_maint.srw
forward
global type w_one_box_transfer_maint from w_base_sheet_ext
end type
type dw_one_box_transfer_maint from u_base_dw_ext within w_one_box_transfer_maint
end type
end forward

global type w_one_box_transfer_maint from w_base_sheet_ext
integer x = 41
integer y = 268
integer width = 3602
integer height = 1472
string title = "One Box Transfer Maintenance"
boolean minbox = false
long backcolor = 67108864
boolean clientedge = true
event ue_postitemchanged pbm_custom01
dw_one_box_transfer_maint dw_one_box_transfer_maint
end type
global w_one_box_transfer_maint w_one_box_transfer_maint

type variables
Private:

Int		ii_async_commhandle
						
s_error     		istr_error_info
u_pas203    		iu_pas203
u_pas201		iu_pas201
u_ws_pas3   	iu_ws_pas3
w_production_pt_parameters		iw_parent


nvuo_fab_product_code invuo_fab_product_code

DataWindowChild  	idwc_ship_plant, &
						idwc_recv_plant, &
                  idwc_fab_product_code, &
						idwc_state

string      		is_ship_plant, &
						is_dest_plant, &
						is_product, &
						is_state, &
						is_start_date, &
						is_end_date, &
						is_ColName, &
						is_product_info

date        		id_date

Time					it_ChangedTime

Long					il_ChangedRow

String				is_ChangedColumnName, &
						is_inquire_flag

INTEGER				ii_Query_Retry_Count  



end variables

forward prototypes
public subroutine wf_delete ()
public subroutine wf_filenew ()
public function boolean wf_addrow ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_deleterow ()
public function boolean wf_validate (long al_row)
end prototypes

event ue_postitemchanged;If il_ChangedRow > 0 And il_ChangedRow <= dw_one_box_transfer_maint.RowCount() &
		And Not iw_frame.iu_string.nf_IsEmpty(is_ChangedColumnName) Then
	dw_one_box_transfer_maint.SetItem(il_ChangedRow, is_ChangedColumnName, it_ChangedTime)
End if
//
end event

public subroutine wf_delete ();wf_deleterow()
end subroutine

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_addrow ();Long 				ll_rownum, &
					ll_addrow
dwitemstatus	lis_temp
String			ls_column

This.setredraw(False)
//check to see if there is a row selected
ll_rownum = dw_one_box_transfer_maint.GetSelectedRow(0)

ls_column = 'ship_plant'	

IF ll_rownum = 0 Then // There is no row selected
	ll_rownum = dw_one_box_transfer_maint.RowCount()
	// If table is empty
	If ll_rownum = 0 Then
		Super:: wf_AddRow()
	Else
		If dw_one_box_transfer_maint.GetItemString(ll_rownum,'product_code') > '   ' Then
			Super:: wf_addrow()
		End If
	End If
	ll_addrow = dw_one_box_transfer_maint.GetSelectedRow(0)
// There is a selected row	
Else
	ll_addrow = dw_one_box_transfer_maint.insertrow(ll_rownum + 1)
	dw_one_box_transfer_maint.SetRow(ll_addrow)
End if

//dw_one_box_transfer_maint.setitem(ll_addrow, "end_date", date('12/31/2999'))
//dw_one_box_transfer_maint.setitem(ll_addrow, "start_date", Today())
//dw_one_box_transfer_maint.setitem(ll_addrow, "product_state", '1')
//
dw_one_box_transfer_maint.SetColumn (ls_column)
dw_one_box_transfer_maint.SetFocus()

This.SetRedraw(True)

Return True	


end function

public function boolean wf_retrieve ();Long				ll_value, &
					ll_count

Boolean			lb_return

String			ls_input, &
					ls_division_code, &
					ls_output_string

u_string_functions	lu_string


Call w_base_sheet::closequery
IF Message.ReturnValue = 1 THEN Return False

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")


istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_pasp67cr"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

IF Not IsValid( iu_ws_pas3 ) THEN
	iu_ws_pas3	=  CREATE u_ws_pas3
END IF

This.SetRedraw( False )

dw_one_box_transfer_maint.reset()

//lb_return	= iu_pas201.nf_pasp67cr_inq_one_box_trans( istr_Error_Info, ls_output_string)
lb_return	= iu_ws_pas3.uf_pasp67gr( istr_Error_Info, ls_output_string)
 
IF NOT lb_return THEN
	This.SetRedraw(True)
	Return( False )
END IF
dw_one_box_transfer_maint.importstring(ls_output_string)

ll_value = dw_one_box_transfer_maint.RowCount()
If ll_value < 0 Then ll_value = 0

dw_one_box_transfer_maint.ResetUpdate()

IF ll_value > 0 THEN
	dw_one_box_transfer_maint.SetFocus()
	dw_one_box_transfer_maint.ScrollToRow(1)
	dw_one_box_transfer_maint.SetColumn( "ship_plant" )
	dw_one_box_transfer_maint.TriggerEvent("RowFocusChanged")
END IF

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )
Return( True )

 
end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount, &
					ll_temp

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdatePRPT
boolean			lb_return

dwItemStatus	lis_status



IF dw_one_box_transfer_maint.AcceptText() = -1 THEN Return( False )

//ll_temp = dw_one_box_transfer_maint.DeletedCount()

//IF dw_one_box_transfer_maint.ModifiedCount() + dw_one_box_transfer_maint.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_one_box_transfer_maint.RowCount( )

dw_one_box_transfer_maint.SetReDraw(False)

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_one_box_transfer_maint.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then 
			dw_one_box_transfer_maint.SetReDraw(True)
			Return False
		ELSE
			ll_Row = ll_NbrRows + 1
		END IF
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF	
LOOP

ls_UpdatePRPT = iw_frame.iu_string.nf_BuildUpdateString(dw_one_box_transfer_maint)

//lb_return = iu_pas201.nf_pasp68cr_upd_one_box_trans(istr_error_info, ls_UpdatePRPT) 
lb_return = iu_ws_pas3.uf_pasp68gr(istr_error_info, ls_UpdatePRPT) 


IF NOT lb_return THEN
	This.SetRedraw(True)
	Return( False )
END IF

dw_one_box_transfer_maint.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_one_box_transfer_maint.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_one_box_transfer_maint.SetItem(li_Counter, 'update_flag', ' ')
Next
dw_one_box_transfer_maint.ResetUpdate()
//dw_one_box_transfer_maint.Sort()
//dw_one_box_transfer_maint.GroupCalc()
dw_one_box_transfer_maint.SetFocus()
dw_one_box_transfer_maint.SetReDraw(True)


Return( True )
end function

public function boolean wf_deleterow ();Boolean		lb_ret

long			ll_row
//ll_row = dw_one_box_transfer_maint.GetSelectedRow(0)
//IF ll_row = 0 Then
//	Beep(1)
//	iw_frame.SetMicroHelp('You must have a row selected to delete -- Please select a row and try again')
//	Return False
//Else
//	Super:: wf_deleterow()
//End If	
//
//Return True
//

//xxxxxxx
This.SetRedraw( False )
ll_row = dw_one_box_transfer_maint.GetRow()
//
If ll_row < 1 Then
	Return True
End If
//
//
dw_one_box_transfer_maint.SetItem(ll_row, "update_flag", 'D')
//
dw_one_box_transfer_maint.SetFocus()
lb_ret = super::wf_deleterow()
////
dw_one_box_transfer_maint.SelectRow(0,False)
//
This.SetRedraw( True )
////dw_one_box_transfer_maint.SelectRow(dw_one_box_transfer_maint.GetRow(),True)
return lb_ret
//
end function

public function boolean wf_validate (long al_row);Date					ldt_start_date, &
						ldt_end_date, &
						ldt_found_start_date, &
						ldt_found_end_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows

String				ls_fab_product, &
						ls_ship_plant, &
						ls_recv_plant, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag, &
						ls_product_code


ll_nbrrows = dw_one_box_transfer_maint.RowCount()

ls_temp = dw_one_box_transfer_maint.GetItemString(al_row, "ship_plant")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	MessageBox("Ship plant", "Please enter a ship plant.  Ship plant cannot be left blank.")
	This.SetRedraw(False)
	dw_one_box_transfer_maint.ScrollToRow(al_row)
	dw_one_box_transfer_maint.SetColumn("ship_plant")
	dw_one_box_transfer_maint.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_one_box_transfer_maint.GetItemString(al_row, "recv_plant")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	MessageBox("Recv plant", "Please enter a recv plant.  Recv plant cannot be left blank.")
	This.SetRedraw(False)
	dw_one_box_transfer_maint.ScrollToRow(al_row)
	dw_one_box_transfer_maint.SetColumn("recv_plant")
	dw_one_box_transfer_maint.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_one_box_transfer_maint.GetItemString(al_row, "age_code")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	MessageBox("Age Code", "Please enter an age code.  Age Code cannot be left blank.")
	This.SetRedraw(False)
	dw_one_box_transfer_maint.ScrollToRow(al_row)
	dw_one_box_transfer_maint.SetColumn("age_code")
	dw_one_box_transfer_maint.SetFocus()
	This.SetRedraw(True)
	Return False
End If


// Check that the start date has a value and is > current date and < end date.
ls_update_flag = dw_one_box_transfer_maint.GetItemString(al_row, "update_flag")

// Check that the end date has a value and is > current date and start date.

If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

ls_ship_plant = dw_one_box_transfer_maint.GetItemString(al_row, "ship_plant")
ls_recv_plant = dw_one_box_transfer_maint.GetItemString(al_row, "recv_plant")
ls_product_code = dw_one_box_transfer_maint.GetItemString(al_row, "product_code")

ls_SearchString	= "ship_plant = '" + ls_ship_plant + &
						"' and recv_plant = '" + ls_recv_plant + &
						"' and product_code = '" + ls_product_code + "'" 
// Find a matching row excluding the current row.

CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_one_box_transfer_maint.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_one_box_transfer_maint.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_one_box_transfer_maint.Find  &
			(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_one_box_transfer_maint.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE
//

If ll_rtn > 0 Then
//		MessageBox ("There are duplicate rows with the" + &
//  					" same Ship Plant and Receiving Plant.") 
		iw_Frame.SetMicroHelp("There are duplicate rows with the" + &
  					" same Ship Plant, Receiving Plant, and Product.") 
		dw_one_box_transfer_maint.SetRedraw(False)
		dw_one_box_transfer_maint.ScrollToRow(al_row)
		dw_one_box_transfer_maint.SetColumn("ship_plant")
		dw_one_box_transfer_maint.SetRow(al_row)
		dw_one_box_transfer_maint.SelectRow(ll_rtn, True)
		dw_one_box_transfer_maint.SelectRow(al_row, True)
		dw_one_box_transfer_maint.SetRedraw(True)
		Return False
End if
//
Return True
end function

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

IF IsValid( iu_pas201 ) THEN
	DESTROY iu_pas201
END IF

IF IsValid( iu_ws_pas3 ) THEN
	DESTROY iu_ws_pas3
END IF

dw_one_box_transfer_maint.ShareDataOff ( )


end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")




end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_Disable('m_graph')

end on

event ue_query;call super::ue_query;DataWindowChild	ldwc_ShipPlant, &
						ldwc_RecvPlant
						
integer		li_rc


dw_one_box_transfer_maint.GetChild ( "ship_plant", ldwc_ShipPlant )

idwc_ship_plant.ShareData( ldwc_ShipPlant )

dw_one_box_transfer_maint.GetChild ( "recv_plant", ldwc_RecvPlant )

idwc_recv_plant.ShareData( ldwc_RecvPlant )


istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "Production PT"
istr_error_info.se_user_id 		= sqlca.userid


wf_retrieve()
end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_one_box_transfer_maint.x * 2) + 30 
li_y = dw_one_box_transfer_maint.y + 115


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


if width > li_x Then
	dw_one_box_transfer_maint.width	= width - li_x
end if

if height > li_y then
	dw_one_box_transfer_maint.height	= height - li_y
end if
end event

on w_one_box_transfer_maint.create
int iCurrent
call super::create
this.dw_one_box_transfer_maint=create dw_one_box_transfer_maint
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_one_box_transfer_maint
end on

on w_one_box_transfer_maint.destroy
call super::destroy
destroy(this.dw_one_box_transfer_maint)
end on

event activate;call super::activate;//iw_frame.im_menu.mf_Enable('m_graph')

end event

event ue_get_data;call super::ue_get_data;//Choose Case as_value
//	Case 'plant'
////		message.StringParm = dw_production_pt_parameters.uf_get_plant_code()
//	Case 'product' , 'fab_product'
////		message.StringParm = dw_production_pt_parameters.uf_get_product_code()
//	Case 'product_desc'
////		message.StringParm = dw_production_pt_parameters.uf_get_product_desc()
//End Choose

	
end event

event ue_set_data;call super::ue_set_data;//Choose Case as_data_item
//	case 'ib_inquire'
//		is_inquire_flag = as_value
//End Choose

end event

type dw_one_box_transfer_maint from u_base_dw_ext within w_one_box_transfer_maint
event ue_dnwdropdown pbm_dwndropdown
event test pbm_dwntabout
integer y = 32
integer width = 3511
integer height = 1248
integer taborder = 30
string dataobject = "d_one_box_transfer_maint"
boolean controlmenu = true
boolean maxbox = true
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event ue_dnwdropdown;//Long			ll_row
//string		ls_ColName, &
//				ls_ship_plant, &
//				ls_dest_plant, &
//				ls_product, &
//				ls_state, &
//				ls_division
//
//date			ld_start_date, &
//				ld_end_date
//
//ls_ColName = GetColumnName()
//
//IF ls_ColName = "fab_product_code" THEN
//	GetChild ( "fab_product_code", idwc_fab_product_code )
//	ll_row = GetRow()
//	ls_ship_plant = GetItemString( ll_row, "ship_plant" )
//	ls_dest_plant = GetItemString( ll_row, "dest_plant" )
//	ls_product = GetItemString( ll_row, "fab_product_code" )
//	ls_state = GetItemString( ll_row, "product_state" )
//	
//	ld_start_date = GetItemDate( ll_row, "start_date" )
//	ld_end_date = GetItemDate( ll_row, "end_date" )
//	
//	IF iw_frame.iu_string.nf_IsEmpty(ls_ship_plant) or &
//		iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) or &
//			iw_frame.iu_string.nf_IsEmpty(ls_product) OR &
//				iw_frame.iu_string.nf_IsEmpty(ls_state) or &		
//					NOT IsDate( string(ld_start_Date)) Or IsNull(ld_start_Date) or &
//					NOT IsDate( string(ld_end_Date)) Or IsNull(ld_end_Date) THEN
//		MessageBox( "", "Enter a Valid Plant, Product and Date First.", stopsign!, ok! )
//		Return 1
//	END IF
//	
//	// check to see if you have all the 3 fields needed
//	
//	This.SetItem(This.GetRow(), "fab_product_code", "")
//END IF
end event

event itemchanged;call super::itemchanged;int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_prpt_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_GetText, &
				ls_temp, &
				ls_product, &
				ls_product_info 
Date			ldt_temp	

dwItemStatus	le_RowStatus
nvuo_pa_business_rules	u_rule
 
is_ColName    = GetColumnName()
ls_GetText    = data
ll_prpt_row   = GetRow()
il_ChangedRow = 0

CHOOSE CASE is_ColName
	CASE "ship_plant"
		ll_plant_row = idwc_ship_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_ship_plant.RowCount()) 
		IF ll_plant_row = 0 THEN
			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
			This.selecttext(1,100)
			Return 1
		END IF
		
		This.SetItem(ll_PRPT_row, 'ship_plant', ls_GetText)
	
	CASE "recv_plant"
		ll_plant_row = idwc_recv_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_recv_plant.RowCount()) 
		IF ll_plant_row = 0 THEN
			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
			This.selecttext(1,100)
			Return 1
		END IF
		
	CASE "product_code"
		If not invuo_fab_product_code.uf_check_product(data) then
			If	invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
				Return 1
			Else
				iw_frame.SetMicroHelp(data + " is an invalid Product Code")
				Return 1
			End If
		End If
//		End If	
			// We know we have a valid product -- is it SKU or fab?
		
//		ls_product_info =	invuo_fab_product_code.uf_get_product_data( )
//		ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // description
//		This.SetItem(ll_PRPT_row, 'product_descr', ls_temp)
//
//		ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')	  // flag
//		If ls_temp = 'F' Then
//			iw_frame.SetMicroHelp(data + " is not a SKU Product Code")
//			Return 1
//		End If		
			
	
//		End If
		
END CHOOSE
	
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_prpt_row > 0 AND ll_prpt_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_PRPT_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_PRPT_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_PRPT_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_PRPT_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;//String	ls_column_name, &
//			ls_GetText
//
//
//ls_GetText = This.GetText()
//
//Choose Case This.GetColumnName()
//	
//Case "product_code","ship_plant", "recv_plant", &
//		"start_date", "end_date"  
//	This.SelectText(1, Len(ls_gettext))
//	return 1
//End Choose
end event

event ue_graph;//w_graph		w_grp_child
//Window			lw_response

//Message.PowerObjectParm = This
//Message.StringParm = 'd_source_weekly_graph'
//OpenWithParm(lw_response ,  This, 'w_source_graph_select')
end event

event constructor;call super::constructor;DataWindowChild	ldwc_temp

ib_updateable = True
is_selection = '1'

This.GetChild('trans_mode', ldwc_temp)
CONNECT USING SQLCA;	
// Set the transaction object for the child
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

//ib_storedatawindowsyntax = TRUE


end event

event getfocus;call super::getfocus;If This.rowcount() > 0 then 
	This.SelectText(1, Len(GetText()))
end if
end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

event ue_postconstructor;call super::ue_postconstructor;
integer 	rtncode

rtncode = dw_one_box_transfer_maint.GetChild('ship_plant', idwc_ship_plant)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Ship Plant not a DataWindowChild") 
//	return false
end if	
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
idwc_ship_plant.SetTransObject(SQLCA)
idwc_ship_plant.Retrieve()

rtncode = dw_one_box_transfer_maint.GetChild('recv_plant', idwc_recv_plant)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Recv Plant not a DataWindowChild") 
//	return false
end if
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
idwc_recv_plant.SetTransObject(SQLCA)
idwc_recv_plant.Retrieve()

//return true
end event

