HA$PBExportHeader$w_production_pt_parameters.srw
forward
global type w_production_pt_parameters from w_base_sheet_ext
end type
type dw_division_code from u_division within w_production_pt_parameters
end type
type dw_production_pt_parameters from u_base_dw_ext within w_production_pt_parameters
end type
end forward

global type w_production_pt_parameters from w_base_sheet_ext
integer x = 40
integer y = 269
integer width = 2871
integer height = 1475
string title = "Production Planned Transfer Parameters"
long backcolor = 67108864
event ue_postitemchanged pbm_custom01
dw_division_code dw_division_code
dw_production_pt_parameters dw_production_pt_parameters
end type
global w_production_pt_parameters w_production_pt_parameters

type variables
Private:

Int		ii_async_commhandle
						
s_error     		istr_error_info
u_pas203          iu_pas203
u_pas201				iu_pas201
u_ws_pas4		iu_ws_pas4

w_production_pt_parameters		iw_parent


nvuo_fab_product_code invuo_fab_product_code

DataWindowChild  	idwc_ship_plant, &
						idwc_dest_plant, &
                  idwc_fab_product_code, &
						idwc_state

string      		is_ship_plant, &
						is_dest_plant, &
						is_product, &
						is_state, &
						is_start_date, &
						is_end_date, &
						is_ColName, &
						is_product_info

date        		id_date

Time					it_ChangedTime

Long					il_ChangedRow

String				is_ChangedColumnName, &
						is_inquire_flag

INTEGER				ii_Query_Retry_Count  



end variables

forward prototypes
public subroutine wf_delete ()
public subroutine wf_filenew ()
public function boolean wf_deleterow ()
public subroutine wf_print ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function boolean wf_retrieve ()
public function boolean wf_addrow ()
end prototypes

event ue_postitemchanged;If il_ChangedRow > 0 And il_ChangedRow <= dw_production_pt_parameters.RowCount() &
		And Not iw_frame.iu_string.nf_IsEmpty(is_ChangedColumnName) Then
	dw_production_pt_parameters.SetItem(il_ChangedRow, is_ChangedColumnName, it_ChangedTime)
End if

end event

public subroutine wf_delete ();wf_deleterow()
end subroutine

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_deleterow ();Boolean		lb_ret

dw_production_pt_parameters.SetFocus()
lb_ret = super::wf_deleterow()

dw_production_pt_parameters.SelectRow(0,False)
dw_production_pt_parameters.SelectRow(dw_production_pt_parameters.GetRow(),True)
return lb_ret

end function

public subroutine wf_print ();dw_production_pt_parameters.Print()
end subroutine

public function boolean wf_validate (long al_row);Date					ldt_start_date, &
						ldt_end_date, &
						ldt_found_start_date, &
						ldt_found_end_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows

String				ls_division_code, &
						ls_fab_product, &
						ls_product_state, &
						ls_product_descr, &
						ls_ship_plant, &
						ls_dest_plant, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag


ll_nbrrows = dw_production_pt_parameters.RowCount()

ls_temp = dw_production_pt_parameters.GetItemString(al_row, "ship_plant")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	MessageBox("Ship plant", "Please enter a ship plant.  Ship plant cannot be left blank.")
	This.SetRedraw(False)
	dw_production_pt_parameters.ScrollToRow(al_row)
	dw_production_pt_parameters.SetColumn("ship_plant")
	dw_production_pt_parameters.SetFocus()
	This.SetRedraw(True)
	Return False
End If

ls_temp = dw_production_pt_parameters.GetItemString(al_row, "dest_plant")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	MessageBox("Dest plant", "Please enter a dest plant.  Dest plant cannot be left blank.")
	This.SetRedraw(False)
	dw_production_pt_parameters.ScrollToRow(al_row)
	dw_production_pt_parameters.SetColumn("dest_plant")
	dw_production_pt_parameters.SetFocus()
	This.SetRedraw(True)
	Return False
End If

// Check that the start date has a value and is > current date and < end date.
ls_update_flag = dw_production_pt_parameters.GetItemString(al_row, "update_flag")
ldt_temp = dw_production_pt_parameters.GetItemDate(al_row, "start_date")
IF IsNull(ldt_temp) or ldt_temp <= Date('00/00/0000') Then 
	MessageBox("Start Date", "Please enter a Start Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_production_pt_parameters.ScrollToRow(al_row)
	dw_production_pt_parameters.SetColumn("start_date")
	dw_production_pt_parameters.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ldt_temp < Today() And ls_update_flag = 'A' Then
	MessageBox("Start Date", "Please enter a Start Date greater than Current Date")
	This.SetRedraw(False)
	dw_production_pt_parameters.ScrollToRow(al_row)
	dw_production_pt_parameters.SetColumn("start_date")
	dw_production_pt_parameters.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If ldt_temp > dw_production_pt_parameters.GetItemDate(al_row, "end_date") And ls_temp = 'A' Then
		MessageBox("Start Date", "Please enter a Start Date less than End Date")
		This.SetRedraw(False)
		dw_production_pt_parameters.ScrollToRow(al_row)
		dw_production_pt_parameters.SetColumn("start_date")
		dw_production_pt_parameters.SetFocus()
		This.SetRedraw(True)
		Return False
	End IF
End If

// Check that the end date has a value and is > current date and start date.
ldt_temp = dw_production_pt_parameters.GetItemDate(al_row, "end_date")
IF IsNull(ldt_temp) or ldt_temp <= Date('00/00/0000') Then 
	MessageBox("End Date", "Please enter an End Date Greater Than 00/00/0000")
	This.SetRedraw(False)
	dw_production_pt_parameters.ScrollToRow(al_row)
	dw_production_pt_parameters.SetColumn("end_date")
	dw_production_pt_parameters.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ldt_temp < Today() Then
	MessageBox("End Date", "Please enter an End Date Greater Than Current Date")
	This.SetRedraw(False)
	dw_production_pt_parameters.ScrollToRow(al_row)
	dw_production_pt_parameters.SetColumn("end_date")
	dw_production_pt_parameters.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If ldt_temp < dw_production_pt_parameters.GetItemDate(al_row, "start_date") Then
		MessageBox("End Date", "Please enter an End Date Greater Than Start Date")
		This.SetRedraw(False)
		dw_production_pt_parameters.ScrollToRow(al_row)
		dw_production_pt_parameters.SetColumn("end_date")
		dw_production_pt_parameters.SetFocus()
		This.SetRedraw(True)
		Return False
	End IF
End If


If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

ls_fab_product = dw_production_pt_parameters.GetItemString(al_row, "product_code")
ls_product_state = dw_production_pt_parameters.GetItemString(al_row, "product_state")
ls_product_descr = dw_production_pt_parameters.GetItemString(al_row, "product_descr")
ls_ship_plant = dw_production_pt_parameters.GetItemString(al_row, "ship_plant")
ls_dest_plant = dw_production_pt_parameters.GetItemString(al_row, "dest_plant")
ldt_start_date = dw_production_pt_parameters.GetItemDate &
	(al_row, "start_date")
ldt_end_date = dw_production_pt_parameters.GetItemDate &
	(al_row, "end_date")


ls_SearchString	= "product_code = '" + ls_fab_product + &
						"' and product_state = '" + ls_product_state + &
						"' and product_descr = '" + ls_product_descr + &
						"' and ship_plant = '" + ls_ship_plant + &
						"' and dest_plant = '" + ls_dest_plant + "'" 
// Find a matching row excluding the current row.

CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_production_pt_parameters.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_production_pt_parameters.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_production_pt_parameters.Find  &
			(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_production_pt_parameters.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE

If ll_rtn > 0 Then
	ldt_found_start_date = dw_production_pt_parameters.GetItemDate &
	(ll_rtn, "start_date")
	ldt_found_end_date = dw_production_pt_parameters.GetItemDate &
	(ll_rtn, "end_date")
	
	If (ldt_found_start_date > ldt_start_date And ldt_found_start_date < ldt_end_date) Then
		MessageBox ("Date", "There are duplicate products with the" + &
  					" same Ship Plant, Dest Plant and overlapping dates.") 
	ElseIf (ldt_found_end_date > ldt_start_date And ldt_found_end_date < ldt_end_date) Then
		MessageBox ("Date", "There are duplicate products with the" + &
  					" same Ship Plant, Dest Plant and overlapping dates.") 
	ElseIf (ldt_start_date > ldt_found_start_date and ldt_start_date < ldt_found_end_date) Then
		MessageBox ("Date", "There are duplicate products with the" + &
  					" same Ship Plant, Dest Plant and overlapping dates.") 
	ElseIf(ldt_end_date > ldt_found_start_date and ldt_end_date < ldt_found_end_date) Then
		MessageBox ("Date", "There are duplicate products with the" + &
  					" same Ship Plant, Dest Plant and overlapping dates.") 
	ElseIf (ldt_found_start_date = ldt_start_date) Then
		MessageBox ("Date", "There are duplicate products with the" + &
  					" same Ship Plant, Dest Plant and overlapping dates.") 	
	Else
		ll_rtn = 0
	End if
End if
If ll_rtn > 0 Then
//		MessageBox ("Date", "There are duplicate products with the" + &
//  					" same Ship Plant, Dest Plant and overlapping dates.") 
		dw_production_pt_parameters.SetRedraw(False)
		dw_production_pt_parameters.ScrollToRow(al_row)
		dw_production_pt_parameters.SetColumn("start_date")
		dw_production_pt_parameters.SetRow(al_row)
		dw_production_pt_parameters.SelectRow(ll_rtn, True)
		dw_production_pt_parameters.SelectRow(al_row, True)
		dw_production_pt_parameters.SetRedraw(True)
		Return False
End if

Return True
end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdatePRPT

dwItemStatus	lis_status

IF dw_production_pt_parameters.AcceptText() = -1 THEN Return( False )
IF dw_production_pt_parameters.ModifiedCount() + dw_production_pt_parameters.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF

IF Not IsValid(iu_ws_pas4 ) THEN
	iu_ws_pas4	=  CREATE u_ws_pas4
END IF


ll_NbrRows = dw_production_pt_parameters.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_production_pt_parameters.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		IF iw_frame.iu_string.nf_IsEmpty(dw_production_pt_parameters.GetItemString(ll_row, "product_code")) Then 
			MessageBox("Product Code", "Please choose a Product Code.")
			This.SetRedraw(False)
			dw_production_pt_parameters.ScrollToRow(ll_row)
			dw_production_pt_parameters.SetColumn("product_code")
			dw_production_pt_parameters.SetFocus()
			This.SetRedraw(True)
			Return False
		End If
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_UpdatePRPT = iw_frame.iu_string.nf_BuildUpdateString(dw_production_pt_parameters)

//If not iu_pas203.nf_pasp31cr_upd_prod_prod_transfer(istr_error_info, ls_UpdatePRPT) Then  Return False
If not iu_ws_pas4.NF_PASP31GR(istr_error_info, ls_UpdatePRPT) Then  Return False

dw_production_pt_parameters.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_production_pt_parameters.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_production_pt_parameters.SetItem(li_Counter, 'update_flag', ' ')
Next
dw_production_pt_parameters.ResetUpdate()
dw_production_pt_parameters.Sort()
dw_production_pt_parameters.GroupCalc()
dw_production_pt_parameters.SetFocus()
dw_production_pt_parameters.SetReDraw(True)
is_ship_plant   = ""
is_dest_plant   = ""
is_start_date   = ""
is_end_date   = ""
is_product = ""

Return( True )
end function

public function boolean wf_retrieve ();Long				ll_value, &
					ll_count

Boolean			lb_return

String			ls_input, &
					ls_division_code, &
					ls_output_string

u_string_functions	lu_string


Call w_base_sheet::closequery
IF Message.ReturnValue = 1 THEN Return False

OpenWithParm(w_production_pt_parameters_inq, This)
If is_inquire_flag = 'False' Then return False

dw_division_code.uf_ChangeRowStatus(1, NotModified!)

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")


ls_division_code = dw_division_code.uf_Get_Division()

ls_Input = ls_division_code + '~r~n'

istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_pasp30cr"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF

IF Not IsValid( iu_ws_pas4 ) THEN
	iu_ws_pas4	=  CREATE u_ws_pas4
END IF


This.SetRedraw( False )

dw_production_pt_parameters.reset()

//lb_return	= iu_pas203.nf_pasp30cr_inq_prod_plan_transfer( istr_Error_Info, ls_input, ls_output_string)
lb_return	= iu_ws_pas4.NF_PASP30GR( istr_Error_Info, ls_input, ls_output_string)
 
IF NOT lb_return THEN
	This.SetRedraw(True)
	Return( False )
END IF
dw_production_pt_parameters.importstring(ls_output_string)

ll_value = dw_production_pt_parameters.RowCount()
If ll_value < 0 Then ll_value = 0

dw_production_pt_parameters.ResetUpdate()

IF ll_value > 0 THEN
	dw_production_pt_parameters.SetFocus()
	dw_production_pt_parameters.ScrollToRow(1)
	dw_production_pt_parameters.SetColumn( "product_code" )
	dw_production_pt_parameters.TriggerEvent("RowFocusChanged")
END IF

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )
Return( True )

 
end function

public function boolean wf_addrow ();Long 				ll_rownum, &
					ll_addrow
dwitemstatus	lis_temp
String			ls_column

This.setredraw(False)
//check to see if there is a row selected
ll_rownum = dw_production_pt_parameters.GetSelectedRow(0)

ls_column = 'product_code'	

IF ll_rownum = 0 Then // There is no row selected
	ll_rownum = dw_production_pt_parameters.RowCount()
	// If table is empty
	If ll_rownum = 0 Then
		Super:: wf_AddRow()
	Else
		If dw_production_pt_parameters.GetItemString(ll_rownum,'product_code') > '   ' Then
			Super:: wf_addrow()
		End If
	End If
	ll_addrow = dw_production_pt_parameters.GetSelectedRow(0)
// There is a selected row	
Else
	ll_addrow = dw_production_pt_parameters.insertrow(ll_rownum + 1)
	dw_production_pt_parameters.SetRow(ll_addrow)
End if

dw_production_pt_parameters.setitem(ll_addrow, "end_date", date('12/31/2999'))
dw_production_pt_parameters.setitem(ll_addrow, "start_date", Today())
dw_production_pt_parameters.setitem(ll_addrow, "product_state", '1')

dw_production_pt_parameters.SetColumn (ls_column)
dw_production_pt_parameters.SetFocus()

This.SetRedraw(True)

Return True	


end function

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

IF IsValid( iu_pas201 ) THEN
	DESTROY iu_pas201
END IF

dw_production_pt_parameters.ShareDataOff ( )


end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")




end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_Disable('m_graph')

end on

event ue_query;call super::ue_query;DataWindowChild	ldwc_ShipPlant, &
						ldwc_DestPlant
						
integer		li_rc


dw_production_pt_parameters.GetChild ( "ship_plant", ldwc_ShipPlant )

idwc_ship_plant.ShareData( ldwc_ShipPlant )

dw_production_pt_parameters.GetChild ( "dest_plant", ldwc_DestPlant )

idwc_dest_plant.ShareData( ldwc_DestPlant )


istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "Production PT"
istr_error_info.se_user_id 		= sqlca.userid


wf_retrieve()
end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_production_pt_parameters.x * 2) + 30 
li_y = dw_production_pt_parameters.y + 135


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If



if width > li_x Then
	dw_production_pt_parameters.width	= width - li_x
end if

if height > li_y then
	dw_production_pt_parameters.height	= height - li_y
end if
end event

on w_production_pt_parameters.create
int iCurrent
call super::create
this.dw_division_code=create dw_division_code
this.dw_production_pt_parameters=create dw_production_pt_parameters
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division_code
this.Control[iCurrent+2]=this.dw_production_pt_parameters
end on

on w_production_pt_parameters.destroy
call super::destroy
destroy(this.dw_division_code)
destroy(this.dw_production_pt_parameters)
end on

event activate;call super::activate;//iw_frame.im_menu.mf_Enable('m_graph')

end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'division'
		Message.StringParm = dw_division_code.uf_get_division()
	Case 'title'
		Message.StringParm = this.title
	Case 'plant'
//		message.StringParm = dw_production_pt_parameters.uf_get_plant_code()
	Case 'product' , 'fab_product'
//		message.StringParm = dw_production_pt_parameters.uf_get_product_code()
	Case 'product_desc'
//		message.StringParm = dw_production_pt_parameters.uf_get_product_desc()
End Choose

	
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	case 'division'
		dw_division_code.uf_set_division(as_value)
	case 'ib_inquire'
		is_inquire_flag = as_value
End Choose

end event

type dw_division_code from u_division within w_production_pt_parameters
integer x = 59
integer y = 58
integer height = 90
integer taborder = 20
boolean enabled = false
end type

type dw_production_pt_parameters from u_base_dw_ext within w_production_pt_parameters
event ue_dnwdropdown pbm_dwndropdown
event test pbm_dwntabout
integer x = 22
integer y = 291
integer width = 2809
integer height = 1027
integer taborder = 30
string dataobject = "d_production_plan_transfer"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dnwdropdown;Long			ll_row
string		ls_ColName, &
				ls_ship_plant, &
				ls_dest_plant, &
				ls_product, &
				ls_state, &
				ls_division

date			ld_start_date, &
				ld_end_date

ls_ColName = GetColumnName()

IF ls_ColName = "fab_product_code" THEN
	GetChild ( "fab_product_code", idwc_fab_product_code )
	ll_row = GetRow()
	ls_ship_plant = GetItemString( ll_row, "ship_plant" )
	ls_dest_plant = GetItemString( ll_row, "dest_plant" )
	ls_product = GetItemString( ll_row, "fab_product_code" )
	ls_state = GetItemString( ll_row, "product_state" )
	
	ld_start_date = GetItemDate( ll_row, "start_date" )
	ld_end_date = GetItemDate( ll_row, "end_date" )
	
	IF iw_frame.iu_string.nf_IsEmpty(ls_ship_plant) or &
		iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) or &
			iw_frame.iu_string.nf_IsEmpty(ls_product) OR &
				iw_frame.iu_string.nf_IsEmpty(ls_state) or &		
					NOT IsDate( string(ld_start_Date)) Or IsNull(ld_start_Date) or &
					NOT IsDate( string(ld_end_Date)) Or IsNull(ld_end_Date) THEN
		MessageBox( "", "Enter a Valid Plant, Product and Date First.", stopsign!, ok! )
		Return 1
	END IF
	
	// check to see if you have all the 3 fields needed
	
	This.SetItem(This.GetRow(), "fab_product_code", "")
END IF
end event

event itemchanged;call super::itemchanged;int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_prpt_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_GetText, &
				ls_temp, &
				ls_product, &
				ls_product_info 
Date			ldt_temp	

dwItemStatus	le_RowStatus
nvuo_pa_business_rules	u_rule
 
is_ColName    = GetColumnName()
ls_GetText    = data
ll_prpt_row   = GetRow()
il_ChangedRow = 0

CHOOSE CASE is_ColName
	CASE "ship_plant"
		ll_plant_row = idwc_ship_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_ship_plant.RowCount()) 
		IF ll_plant_row = 0 THEN
			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
			This.selecttext(1,100)
			Return 1
		END IF
		
		This.SetItem(ll_PRPT_row, 'ship_plant', ls_GetText)
	
	CASE "dest_plant"
		ll_plant_row = idwc_dest_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_dest_plant.RowCount()) 
		IF ll_plant_row = 0 THEN
			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
			This.selecttext(1,100)
			Return 1
		END IF
		
	CASE "product_code"
		If not invuo_fab_product_code.uf_check_product(data) then
			If	invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
				Return 1
			Else
				iw_frame.SetMicroHelp(data + " is an invalid Product Code")
				Return 1
			End If
		End If
//		End If	
			// We know we have a valid product -- is it SKU or fab?
		
		ls_product_info =	invuo_fab_product_code.uf_get_product_data( )
		ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // description
		This.SetItem(ll_PRPT_row, 'product_descr', ls_temp)

		ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')	  // flag
		If ls_temp = 'F' Then
			iw_frame.SetMicroHelp(data + " is not a SKU Product Code")
			Return 1
		End If		
			
		This.SetItem(row, 'product_state', '1')
			
//		End If
		
	CASE "product_state"
		ls_product = this.getitemstring(row, "product_code")
		if trim(data) <> "1" then
			if not invuo_fab_product_code.uf_check_product_state(ls_product, Trim(data)) then	
				iw_Frame.SetMicroHelp(data + " is an invalid Product State for this Product")
				this.setfocus( )
				this.setitem( row, "product_state", "1")
				this.setcolumn("product_state") 
				This.SelectText(1, Len(data))	
				Return 1
			end if
		end if
		
	
	CASE "start_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
				
		This.SetItem(ll_PRPT_row, 'start_date', date(ls_GetText))
		ls_temp = ' '
		ldt_temp = This.GetItemDate( row, 'end_date')
		
		If date(ls_gettext) > ldt_temp Then
			iw_frame.SetMicroHelp("End Date cannot be less than the Start Date")
			This.selecttext(1,100)
			Return 1
		End If
		
	CASE "end_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
		
		ls_temp = ' '
		ldt_temp = This.GetItemDate( row, 'start_date')
		This.SetItem(ll_PRPT_row, 'end_date', date(ls_GetText))
		If date(ls_gettext) < ldt_temp Then
			iw_frame.SetMicroHelp("End Date cannot be less than the Start Date")
			This.selecttext(1,100)
			Return 1
		End If
END CHOOSE

	
// Update the Update_Flag column so the RPC will know how to update the row
IF ll_prpt_row > 0 AND ll_prpt_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_PRPT_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_PRPT_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_PRPT_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_PRPT_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;String	ls_column_name, &
			ls_GetText


ls_GetText = This.GetText()

Choose Case This.GetColumnName()
	
Case "product_code","product_state", "ship_plant", "dest_plant", &
		"start_date", "end_date"  
	This.SelectText(1, Len(ls_gettext))
	return 1
End Choose
end event

event ue_graph;//w_graph		w_grp_child
//Window			lw_response

//Message.PowerObjectParm = This
//Message.StringParm = 'd_source_weekly_graph'
//OpenWithParm(lw_response ,  This, 'w_source_graph_select')
end event

on constructor;call u_base_dw_ext::constructor;is_selection = '1'
//ib_storedatawindowsyntax = TRUE


end on

event getfocus;call super::getfocus;If This.rowcount() > 0 then 
	This.SelectText(1, Len(GetText()))
end if
end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

event ue_postconstructor;call super::ue_postconstructor;
integer 	rtncode

string	st_allrow

st_allrow = "ALL"

rtncode = dw_production_pt_parameters.GetChild('ship_plant', idwc_ship_plant)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Ship Plant not a DataWindowChild") 
//	return false
end if	
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
idwc_ship_plant.SetTransObject(SQLCA)
idwc_ship_plant.Retrieve()
idwc_ship_plant.InsertRow(1)
idwc_ship_plant.SetItem(1,1,st_allrow)
idwc_ship_plant.SetItem(1,2,st_allrow)


rtncode = dw_production_pt_parameters.GetChild('dest_plant', idwc_dest_plant)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Dest Plant not a DataWindowChild") 
//	return false
end if
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
idwc_dest_plant.SetTransObject(SQLCA)
idwc_dest_plant.Retrieve()

rtncode = dw_production_pt_parameters.GetChild('product_state', idwc_state)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Product state not a DataWindowChild") 
End if
DataStore	lds_state
lds_state = Create DataStore
lds_state.DataObject = "d_tutltype_dddw"
lds_state.SetTransObject(SQLCA)
lds_state.Retrieve("PRDSTATE")
idwc_state.ImportString(lds_state.Describe("DataWindow.Data"))
idwc_state.SetSort("type_code")
idwc_state.Sort()
Destroy lds_state
//return true
end event

