HA$PBExportHeader$w_plan_transfer_shells.srw
forward
global type w_plan_transfer_shells from w_netwise_sheet_ole
end type
type ole_transfer from olecustomcontrol within w_plan_transfer_shells
end type
end forward

global type w_plan_transfer_shells from w_netwise_sheet_ole
integer x = 5
integer y = 4
integer width = 1783
integer height = 1648
string title = "Generate Transfer Orders from Planned Transfers"
long backcolor = 79741120
ole_transfer ole_transfer
end type
global w_plan_transfer_shells w_plan_transfer_shells

type variables
s_error		istr_error_info
string		Is_inquire_parm
boolean		ib_OLE_Error
end variables

forward prototypes
public function boolean wf_update ()
end prototypes

public function boolean wf_update ();boolean lbln_Return

if ib_OLE_Error then return true

SetPointer(HourGlass!)
lbln_Return = ole_transfer.object.Save()
SetPointer(Arrow!)

return lbln_Return
end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	


end event

event ue_postopen;call super::ue_postopen;string	ls_server_suffix

if ib_OLE_Error then return

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_planned_transfer_shells"
istr_error_info.se_user_id 		= sqlca.userid

ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

//if ls_server_suffix = "" then
//	ls_server_suffix = " "
//end if 

ole_transfer.object.initializecontrol(sqlca.userid,sqlca.DBpass,ls_server_suffix,Message.nf_Get_App_ID(),"Generate Planned Transfer Shells/Orders") 
ole_transfer.Height = This.Height - 100

//If Len(Is_inquire_parm) > 0 Then
//	wf_retrieve()
//	Is_inquire_parm = ''                                                                        
//else	
//	ole_transfer.object.Clear()
//End if
//


end event

on w_plan_transfer_shells.create
int iCurrent
call super::create
this.ole_transfer=create ole_transfer
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_transfer
end on

on w_plan_transfer_shells.destroy
call super::destroy
destroy(this.ole_transfer)
end on

event resize;//if newwidth > 2694 then
//	ole_transfer.Width = newwidth - 25
//end if
//
//if (newheight - 100) > 500 then
//	ole_transfer.Height = newheight - 25
//end if

end event

event closequery;call super::closequery;//integer intMsgboxResult ,	intPromptOnSave
//
////gets the settings values
//gw_base_frame.iu_base_data.Trigger Event ue_get_sheetsettings(intPromptOnSave)
//IF gw_base_frame.ib_exit and intPromptOnSave = 0 THEN RETURN
////
//If ole_transfer.object.RowDataChanged(true) then
//	intMsgboxResult = ole_transfer.object.QuerySaveChanges() 
//	if intMsgboxResult = 2 then
//		Message.ReturnValue = 1 	// Cancel the closing of window
//		Return
//	end if
//end if
//
//Return
end event

event open;call super::open;//String ls_temp 
//
//ls_temp = Message.StringParm	
//
//If Len(ls_temp) > 0 and lower(ls_temp) <> 'w_plan_transfer_shells' Then
//	Is_inquire_parm = ls_temp
//End If
//
end event

type ole_transfer from olecustomcontrol within w_plan_transfer_shells
event postmessage ( string strmessage )
integer x = 9
integer y = 4
integer width = 1705
integer height = 1496
integer taborder = 10
boolean bringtotop = true
boolean border = false
long backcolor = 79741120
string binarykey = "w_plan_transfer_shells.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event postmessage;iw_frame.setmicrohelp(strmessage)
end event

event getfocus;if ib_OLE_Error then return

ole_transfer.object.setfocusback()
end event

event error;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

if errornumber <> 20535 then
	lstr_rpc_error_info.se_app_name = "PAS"
	lstr_rpc_error_info.se_window_name = ""
	lstr_rpc_error_info.se_function_name = ""
	lstr_rpc_error_info.se_event_name = errorwindowmenu
	lstr_rpc_error_info.se_procedure_name = errorobject
	lstr_rpc_error_info.se_user_id = ""
	lstr_rpc_error_info.se_return_code = ""
	lstr_rpc_error_info.se_message = "[" + string(errornumber) + "] "  + errortext + "~nThe window will be closed."
	
	lstr_rpc_error_info.se_rval = 0
	lstr_rpc_error_info.se_commerror = 0
	lstr_rpc_error_info.se_commerrmsg = ""
	lstr_rpc_error_info.se_neterror = 0
	lstr_rpc_error_info.se_primaryerror = 0
	lstr_rpc_error_info.se_secondaryerror = 0
	lstr_rpc_error_info.se_neterrmsg = space(100)
	
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
end if

action = ExceptionIgnore! 
close(parent)
end event

event externalexception;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

lstr_rpc_error_info.se_app_name = "PAS"
lstr_rpc_error_info.se_window_name = ""
lstr_rpc_error_info.se_function_name = ""
lstr_rpc_error_info.se_event_name = parent.title
lstr_rpc_error_info.se_procedure_name = source
lstr_rpc_error_info.se_user_id = ""
lstr_rpc_error_info.se_return_code = ""
lstr_rpc_error_info.se_message = "[" + string(resultcode) + "] "  + "[" + string(exceptioncode) + "] "  + description + "~nThe window will be closed."

lstr_rpc_error_info.se_rval = 0
lstr_rpc_error_info.se_commerror = 0
lstr_rpc_error_info.se_commerrmsg = ""
lstr_rpc_error_info.se_neterror = 0
lstr_rpc_error_info.se_primaryerror = 0
lstr_rpc_error_info.se_secondaryerror = 0
lstr_rpc_error_info.se_neterrmsg = space(100)

openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")

action = ExceptionIgnore! 
close(parent)
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
06w_plan_transfer_shells.bin 
2100000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000e28c55f001cce1e500000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a00000002000000010000000411f87c8e11d479d701002bb35a91280200000000e28c55f001cce1e5e28c55f001cce1e5000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
29ffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e0065007800740000268d000800034757f20affffffe00065005f00740078006e006500790074000026a7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
16w_plan_transfer_shells.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
