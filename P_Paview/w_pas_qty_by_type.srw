HA$PBExportHeader$w_pas_qty_by_type.srw
$PBExportComments$Planned Demand
forward
global type w_pas_qty_by_type from w_base_child_ext
end type
type dw_quantities from u_base_dw_ext within w_pas_qty_by_type
end type
type cb_cancel from u_base_commandbutton_ext within w_pas_qty_by_type
end type
type cb_1 from u_help_cb within w_pas_qty_by_type
end type
end forward

global type w_pas_qty_by_type from w_base_child_ext
int Width=1038
int Height=784
boolean TitleBar=true
string Title="Sold by Type"
long BackColor=12632256
boolean MinBox=false
boolean MaxBox=false
dw_quantities dw_quantities
cb_cancel cb_cancel
cb_1 cb_1
end type
global w_pas_qty_by_type w_pas_qty_by_type

on open;call w_base_child_ext::open;//The filter date will be the first field on the string then ~t then string of data to filter

Date		ldt_filter

Int		li_pos

String	ls_data


ls_data = Message.StringParm
li_pos = Pos(ls_data, '~t', 1)

ldt_filter = Date(Left(ls_data, li_pos -1))

ls_data = Right(ls_data, Len(ls_data) - li_pos)
dw_quantities.ImportString(ls_data)
dw_quantities.SetFilter("begin_date = Date('" + String(ldt_filter) + "')")
dw_quantities.Filter()
end on

on w_pas_qty_by_type.create
int iCurrent
call super::create
this.dw_quantities=create dw_quantities
this.cb_cancel=create cb_cancel
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_quantities
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.cb_1
end on

on w_pas_qty_by_type.destroy
call super::destroy
destroy(this.dw_quantities)
destroy(this.cb_cancel)
destroy(this.cb_1)
end on

type dw_quantities from u_base_dw_ext within w_pas_qty_by_type
int X=5
int Y=0
int Width=992
int Height=544
int TabOrder=0
string DataObject="d_pa_view_sold_by_type"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

type cb_cancel from u_base_commandbutton_ext within w_pas_qty_by_type
int X=370
int Y=556
int TabOrder=10
string Text="&Cancel"
boolean Default=true
boolean Cancel=true
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
end type

on clicked;call u_base_commandbutton_ext::clicked;Close(Parent)
end on

type cb_1 from u_help_cb within w_pas_qty_by_type
int X=667
int Y=556
int TabOrder=20
int TextSize=-8
int Weight=400
string FaceName="MS Sans Serif"
end type

