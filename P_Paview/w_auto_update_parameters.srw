HA$PBExportHeader$w_auto_update_parameters.srw
forward
global type w_auto_update_parameters from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_auto_update_parameters
end type
type dw_detail from u_base_dw_ext within w_auto_update_parameters
end type
end forward

global type w_auto_update_parameters from w_base_sheet_ext
integer width = 5793
integer height = 1408
string title = "Auto Update Parameter Maintenance"
long backcolor = 67108864
dw_header dw_header
dw_detail dw_detail
end type
global w_auto_update_parameters w_auto_update_parameters

type variables
String			is_inquire_flag

u_ws_pas3	iu_ws_pas3

s_error     		istr_error_info

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_addrow ()
public function boolean wf_update ()
public function boolean wf_validate_row (integer ai_row)
public function boolean wf_check_duplicate_rows ()
public subroutine wf_delete ()
public function boolean wf_deleterow ()
public subroutine wf_filenew ()
end prototypes

public function boolean wf_retrieve ();Long				ll_value

Boolean			lb_return

String				ls_header_string, ls_detail_string
				
u_string_functions	lu_string


Call w_base_sheet::closequery
IF Message.ReturnValue = 1 THEN Return False

OpenWithParm(w_auto_update_parameters_inq, This)
If is_inquire_flag = 'False' Then return False

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

ls_header_string = dw_header.Describe("Datawindow.data")  +  '~t' + 'I' + '~t'

istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_pasp05hr"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_ws_pas3 ) THEN
	iu_ws_pas3	=  CREATE u_ws_pas3
END IF

This.SetRedraw( False )

lb_return	= iu_ws_pas3.uf_pasp05hr( istr_error_info, ls_header_string, ls_detail_string)

dw_detail.Reset()
dw_detail.ImportString(ls_detail_string)
dw_detail.ResetUpdate()

IF NOT lb_return THEN
	This.SetRedraw(True)
	Return( False )
END IF

ll_value = dw_detail.RowCount()
If ll_value < 0 Then ll_value = 0

dw_header.ResetUpdate()
dw_detail.SelectRow(0,False)
dw_detail.ResetUpdate()
dw_detail.SetFocus()

SetMicroHelp(String(ll_value) + " rows retrieved")

This.SetRedraw( True )
Return( True )

 
end function

public function boolean wf_addrow ();Integer	li_new_row

li_new_row = dw_detail.InsertRow(0)
dw_detail.SetItem(li_new_row, "update_flag", 'A')
dw_detail.SetItem(li_new_row, "enable_auto_update", 'N')
dw_detail.SetItem(li_new_row, "last_init_status", "NEW")
dw_detail.SetItem(li_new_row, "total_proj_quantity", 0)
dw_detail.SetItem(li_new_row, "total_act_pa", 0)
dw_detail.SetItem(li_new_row, "total_act_yld", 0)
dw_detail.SetItem(li_new_row, "total_act_mh", 0)
dw_detail.SetItem(li_new_row, "production_date", date('0001-01-01'))
dw_detail.ScrolltoRow(li_new_row)
Return True


end function

public function boolean wf_update ();Long				ll_NbrRows, &
					ll_Row

String				ls_detail_string, &
					ls_header_string
					
Boolean			lb_return					

IF Not IsValid(iu_ws_pas3 ) THEN
	iu_ws_pas3	=  CREATE u_ws_pas3
END IF

dw_detail.AcceptText()

If Not wf_check_duplicate_rows() Then Return False

ll_NbrRows = dw_detail.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate_row(ll_Row) Then Return False
	Else
		ll_Row = ll_NbrRows + 1
	End If
LOOP

ls_detail_string = iw_frame.iu_string.nf_BuildUpdateString(dw_detail)

ls_header_string = dw_header.Describe("Datawindow.data")  +  '~t' + 'U' + '~t'

istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_pasp05hr"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_ws_pas3 ) THEN
	iu_ws_pas3	=  CREATE u_ws_pas3
END IF

This.SetRedraw( False )

lb_return	= iu_ws_pas3.uf_pasp05hr( istr_error_info, ls_header_string, ls_detail_string)

For ll_row = 1 to ll_NbrRows
	dw_detail.SetItem(ll_row, "update_flag", " ")
Next

IF lb_return then
	iw_frame.SetMicroHelp("Update Successful")
	dw_detail.ResetUpdate()
else
	iw_frame.SetMicroHelp(istr_error_info.se_message)
End If

This.SetRedraw( True )

Return True
end function

public function boolean wf_validate_row (integer ai_row);If (isnull(dw_detail.GetItemString(ai_row, "plant")) or (dw_detail.GetItemString(ai_row, "plant") <= '   ')) Then
	MessageBox("Ship plant", "Please enter a ship plant.  Ship plant cannot be left blank.")
	Return False
End If

If (isnull(dw_detail.GetItemString(ai_row, "day_of_week")) or (dw_detail.GetItemString(ai_row, "day_of_week") <=  ' ')) Then
	MessageBox("Day of Week", "Please enter the day of week.  Day of Week cannot be left blank.")
	Return False
End If

If (isnull(dw_detail.GetItemString(ai_row, "shift")) or (dw_detail.GetItemString(ai_row, "shift") <= ' ')) Then
	MessageBox("Shift", "Please enter the shift. Shift cannot be left blank.")
	Return False
End If

If (isnull(dw_detail.GetItemString(ai_row, "allow_reupdate")) or (dw_detail.GetItemString(ai_row, "allow_reupdate") <=  '        ')) Then
	MessageBox("Allow Reupdate", "Please enter the allow reupdate. Allow reupdate cannot be left blank.")
	Return False
End If

If (isnull(dw_detail.GetItemString(ai_row, "source_type")) or (dw_detail.GetItemString(ai_row, "source_type") <=  '        ')) Then
	MessageBox("Update Type", "Please enter the update type.  Update type cannot be left blank.")
	Return False
End If

If (isnull(dw_detail.GetItemString(ai_row, "earliest_init_day")) or (dw_detail.GetItemString(ai_row, "earliest_init_day") <= '        ')) Then
	MessageBox("Earliest Init Day", "Please enter the earliest init day.  Earliest init day cannot be left blank.")
	Return False
End If

If (isnull(dw_detail.GetItemTime(ai_row, "earliest_init_time")) or (dw_detail.GetItemTime(ai_row, "earliest_init_time") <= Time("00:00:00"))) Then
	MessageBox("Earliest Init Time", "Please enter the earliest init time.  Earliest init time must be greater than 00:00")
	Return False
End If

If (isnull(dw_detail.GetItemString(ai_row, "update_on_expire")) or (dw_detail.GetItemString(ai_row, "update_on_expire") <=  '        ')) Then
	MessageBox("Update on Expire Time", "Please enter the Update on expire time. Update on expire time cannot be left blank.")
	Return False
End If

If (isnull(dw_detail.GetItemNumber(ai_row, "proj_prod_variance")) or (dw_detail.GetItemNumber(ai_row, "proj_prod_variance") <= 0)) Then
	MessageBox("Projected Production Variance", "Please enter the projected production variance.  The projected production variance must be > 0")
	Return False
End If


Return True
end function

public function boolean wf_check_duplicate_rows ();Integer		li_sub1, li_sub2, li_RowCount

String			ls_day_of_week, ls_day_name, &
				ls_temp1, ls_temp2, ls_temp3, ls_temp4, ls_temp5, ls_temp6

li_RowCount = dw_detail.RowCount()

For li_sub1 = 1 to li_RowCount

	For li_sub2 = li_sub1 +1 to li_RowCount
		ls_day_of_week = dw_detail.GetItemString(li_sub1, "day_of_week")
		ls_temp1 = dw_detail.GetItemString(li_sub1, "plant")
		ls_temp2 = dw_detail.GetItemString(li_sub2, "plant")
		ls_temp3 = dw_detail.GetItemString(li_sub1, "day_of_week") 
		ls_temp4 =  dw_detail.GetItemString(li_sub2, "day_of_week")
		ls_temp5 = dw_detail.GetItemString(li_sub1, "shift")
		ls_temp6 = dw_detail.GetItemString(li_sub1, "shift")
		
		If (dw_detail.GetItemString(li_sub1, "plant") = dw_detail.GetItemString(li_sub2, "plant")) AND &
					(dw_detail.GetItemString(li_sub1, "day_of_week") = dw_detail.GetItemString(li_sub2, "day_of_week")) AND &
					(Trim(dw_detail.GetItemString(li_sub1, "shift")) = Trim(dw_detail.GetItemString(li_sub2, "shift"))) Then
				ls_day_of_week = dw_detail.GetItemString(li_sub1, "day_of_week")	
				Choose Case ls_day_of_week 
					Case '0'
						ls_day_name = 'Sunday'
					Case '1'
						ls_day_name = 'Monday'
					Case '2'
						ls_day_name = 'Tuesday'
					Case '3'
						ls_day_name = 'Wednesday'
					Case '4'
						ls_day_name = 'Thursday'
					Case '5'
						ls_day_name = 'Friday'
					Case '6'
						ls_day_name = 'Saturday'						
				End Choose
				MessageBox("Duplicate rows", "Duplicate rows found for Plant = "  + 	dw_detail.GetItemString(li_sub1, "plant") + &
					",  Day of Week = " + ls_day_name + ", Shift = " +  dw_detail.GetItemString(li_sub1, "shift"))
				Return False	
		End If
	Next
Next
				
Return True				
				
end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_deleterow ();Boolean		lb_ret

dw_detail.SetFocus()
lb_ret = super::wf_deleterow()

dw_detail.SelectRow(0,False)
dw_detail.SelectRow(dw_detail.GetRow(),True)
return lb_ret
end function

public subroutine wf_filenew ();wf_addrow()
end subroutine

on w_auto_update_parameters.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_detail
end on

on w_auto_update_parameters.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_detail)
end on

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'parameter_type'
		If IsNull(dw_header.GetItemString(1, "parameter_type")) Then
			Message.StringParm = ''
		Else
			Message.StringParm = dw_header.GetItemString(1, "parameter_type")
		End If
		
	Case 'location'
		If IsNull(dw_header.GetItemString(1, "location")) Then
			Message.StringParm = ''
		Else
			message.StringParm = dw_header.GetItemString(1, "location")
		End If
		
	Case 'day_of_week'
		If Isnull(dw_header.GetItemString(1, "day_of_week")) Then
			Message.StringParm = ''
		Else
			message.StringParm = dw_header.GetItemString(1, "day_of_week")
		End If
End Choose
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'parameter_type'
		dw_header.SetItem(1, "parameter_type", as_value)
	Case 'location'
		dw_header.SetItem(1, "location", as_value)
	Case 'day_of_week'	
		dw_header.SetItem(1, "day_of_week", as_value)
	case 'ib_inquire'
		is_inquire_flag = as_value	
End Choose



end event

event resize;call super::resize;constant integer li_x		= 20
constant integer li_y		= 290
  
dw_detail.width	= newwidth - li_x
dw_detail.height	= newheight - li_y

end event

type dw_header from u_base_dw_ext within w_auto_update_parameters
integer width = 2114
integer height = 243
integer taborder = 10
string dataobject = "d_auto_production_header"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild('parameter_type', ldwc_temp)

ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()
end event

type dw_detail from u_base_dw_ext within w_auto_update_parameters
integer x = 4
integer y = 259
integer width = 5720
integer height = 1024
integer taborder = 10
string dataobject = "d_auto_production_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

If This.RowCount() = 0 Then This.InsertRow(0)

is_selection = '1'

This.GetChild('plant', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

This.GetChild('allow_reupdate', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

This.GetChild('shift', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

This.GetChild('source_type', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()

This.GetChild('earliest_init_day', ldwc_temp)
ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve()


end event

event itemchanged;call super::itemchanged;If This.GetItemString(row, "update_flag") = 'A' Then
	// do nothing
Else
	This.SetItem(row, "update_flag", "U")
End if



end event

