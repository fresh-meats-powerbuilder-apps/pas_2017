HA$PBExportHeader$w_raw_material_specifications.srw
forward
global type w_raw_material_specifications from w_netwise_sheet_ole
end type
type ole_raw_material_specifications from olecustomcontrol within w_raw_material_specifications
end type
end forward

global type w_raw_material_specifications from w_netwise_sheet_ole
integer x = 5
integer y = 4
integer width = 2921
integer height = 1516
string title = "Raw Material Specifications"
long backcolor = 79741120
ole_raw_material_specifications ole_raw_material_specifications
end type
global w_raw_material_specifications w_raw_material_specifications

type variables
s_error					istr_error_info
string					is_inquire_parm,&
							is_plan_parm
boolean					ib_OLE_Error 
w_planned_transfer	iw_plan_transfer 
end variables

forward prototypes
public subroutine wf_delete ()
public function boolean wf_addrow ()
public function boolean wf_deleterow ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_query_save_changes ()
end prototypes

public subroutine wf_delete ();ole_raw_material_specifications.object.deleterow()

end subroutine

public function boolean wf_addrow ();if ib_OLE_Error then return true

ole_raw_material_specifications.object.addrow()
return true
end function

public function boolean wf_deleterow ();if ib_OLE_Error then return true

ole_raw_material_specifications.object.deleterow()
return true
end function

public function boolean wf_retrieve ();boolean lbln_Return

if ib_OLE_Error then return true

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "ole_raw_material_specifications.Inquire"

SetPointer(HourGlass!)

ole_raw_material_specifications.object.Inquire()

SetPointer(Arrow!)

return true
end function

public function boolean wf_update ();boolean blnReturn

if ib_OLE_Error then return true

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "ole_raw_material_specifications.save"

setPointer(HourGlass!)

blnReturn = ole_raw_material_specifications.object.Save()

return true // blnReturn
end function

public function boolean wf_query_save_changes ();IF ole_raw_material_specifications.object.CloseQuery then
		CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)

			CASE 1	// Save Changes
				If This.wf_update() = FALSE Then
					RETURN FALSE
				Else
					RETURN TRUE
				End If
			CASE 3	// Cancel the closing of window
				RETURN TRUE
		END CHOOSE
End if

RETURN TRUE

end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	


end event

event deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

end event

event ue_postopen;call super::ue_postopen;string	ls_server
integer 	li_rtn


istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_raw_material_specifications"
istr_error_info.se_user_id 		= sqlca.userid

ls_server = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

ole_raw_material_specifications.object.UserID = sqlca.userid
ole_raw_material_specifications.object.Password = sqlca.DBpass
ole_raw_material_specifications.object.SignOnSystem = ls_server
ole_raw_material_specifications.object.Window = this.title
ole_raw_material_specifications.object.Application = "PAS"

if this.width  > 10 then
	ole_raw_material_specifications.Width = this.width - 50
end if

if this.height > 10 then
	ole_raw_material_specifications.Height = this.height - 150
end if

wf_retrieve()
end event

on w_raw_material_specifications.create
int iCurrent
call super::create
this.ole_raw_material_specifications=create ole_raw_material_specifications
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_raw_material_specifications
end on

on w_raw_material_specifications.destroy
call super::destroy
destroy(this.ole_raw_material_specifications)
end on

event resize;integer li_x = 50

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

ole_raw_material_specifications.Width = This.Width - li_x
ole_raw_material_specifications.Height = This.Height - 100

end event

type ole_raw_material_specifications from olecustomcontrol within w_raw_material_specifications
event messageposted ( ref string strinfomessage )
event currentrecordchanged ( )
integer width = 2880
integer height = 1108
integer taborder = 10
boolean bringtotop = true
boolean border = false
long backcolor = 79741120
boolean focusrectangle = false
string binarykey = "w_raw_material_specifications.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event messageposted;iw_frame.SetMicroHelp(strinfomessage)
end event

event getfocus;if ib_OLE_Error then return

ole_raw_material_specifications.object.SetFocusBack()
end event

event externalexception;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

lstr_rpc_error_info.se_app_name = "PAS"
lstr_rpc_error_info.se_window_name = ""
lstr_rpc_error_info.se_function_name = ""
lstr_rpc_error_info.se_event_name = parent.title
lstr_rpc_error_info.se_procedure_name = source
lstr_rpc_error_info.se_user_id = ""
lstr_rpc_error_info.se_return_code = ""
lstr_rpc_error_info.se_message = "[" + string(resultcode) + "] "  + "[" + string(exceptioncode) + "] "  + description + "~nThe window will be closed."

lstr_rpc_error_info.se_rval = 0
lstr_rpc_error_info.se_commerror = 0
lstr_rpc_error_info.se_commerrmsg = ""
lstr_rpc_error_info.se_neterror = 0
lstr_rpc_error_info.se_primaryerror = 0
lstr_rpc_error_info.se_secondaryerror = 0
lstr_rpc_error_info.se_neterrmsg = space(100)

openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")

action = ExceptionIgnore! 
close(parent)
end event

event error;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

if errornumber <> 20535 then
	lstr_rpc_error_info.se_app_name = "PAS"
	lstr_rpc_error_info.se_window_name = ""
	lstr_rpc_error_info.se_function_name = ""
	lstr_rpc_error_info.se_event_name = errorwindowmenu
	lstr_rpc_error_info.se_procedure_name = errorobject
	lstr_rpc_error_info.se_user_id = ""
	lstr_rpc_error_info.se_return_code = ""
	lstr_rpc_error_info.se_message = "[" + string(errornumber) + "] "  + errortext + "~nThe window will be closed."
	
	lstr_rpc_error_info.se_rval = 0
	lstr_rpc_error_info.se_commerror = 0
	lstr_rpc_error_info.se_commerrmsg = ""
	lstr_rpc_error_info.se_neterror = 0
	lstr_rpc_error_info.se_primaryerror = 0
	lstr_rpc_error_info.se_secondaryerror = 0
	lstr_rpc_error_info.se_neterrmsg = space(100)
	
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
end if

action = ExceptionIgnore! 
close(parent)
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
08w_raw_material_specifications.bin 
2300000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000266c039001cce1f200000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004fe907c2211d575370100c79a4091280200000000266c039001cce1f2266c039001cce1f2000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
23ffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e0065007800740000411d000800034757f20affffffe00065005f00740078006e00650079007400001ca1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
18w_raw_material_specifications.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
