HA$PBExportHeader$w_division_parameters.srw
forward
global type w_division_parameters from w_base_sheet_ext
end type
type tab_1 from u_division_parameters_tab within w_division_parameters
end type
type tab_1 from u_division_parameters_tab within w_division_parameters
end type
end forward

global type w_division_parameters from w_base_sheet_ext
integer x = 389
integer y = 468
integer width = 1872
integer height = 1459
string title = "Division Parameters"
long backcolor = 12632256
tab_1 tab_1
end type
global w_division_parameters w_division_parameters

type variables
u_pas201		iu_pas201
u_ws_pas4	iu_ws_pas4

s_error		istr_error_info

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();long			ll_rec_count

string		ls_division, &  
            ls_input_data, &
				ls_output_data, &
				ls_temp

u_string_functions	lu_string

This.TriggerEvent('closequery')

SetPointer(HourGlass!)

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp96br_inq_upd_location_parameters"
istr_error_info.se_message = Space(71)

This.SetRedraw(False)

/*If iu_pas201.nf_pasp96br_inq_upd_div_parameters(istr_error_info, 'I', ls_input_data,ls_output_data) < 0 Then
	This.SetRedraw(True)
	Return False
End If
*/
If iu_ws_pas4.NF_PASP96FR(istr_error_info, 'I', ls_input_data,ls_output_data) < 0 Then
	This.SetRedraw(True)
	Return False
End If

If Not lu_string.nf_IsEmpty(ls_output_data) Then
	tab_1.tabpage_product_shortage.dw_div_product_shortage.Reset()
	ll_rec_count = tab_1.tabpage_product_shortage.dw_div_product_shortage.ImportString(ls_output_data)
	If ll_rec_count > 0 then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True)

tab_1.tabpage_product_shortage.dw_div_product_shortage.ResetUpdate()
tab_1.tabpage_product_shortage.dw_div_product_shortage.SetFocus()
 
Return True
end function

public function boolean wf_update ();String			ls_input_string, &
					ls_output_string, &
					ls_temp, &
					ls_header_string

Long				ll_row, &
					ll_error_row


If tab_1.tabpage_product_shortage.dw_div_product_shortage.AcceptText() < 1 Then Return False

ll_row = tab_1.tabpage_product_shortage.dw_div_product_shortage.GetNextModified(0, primary!)
If ll_row = 0 Then
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End If

ll_error_row = tab_1.tabpage_product_shortage.dw_div_product_shortage.Find("days_for_review < 0", 1, &
		tab_1.tabpage_product_shortage.dw_div_product_shortage.RowCount()) 
		
if ll_error_row > 0 then
	iw_frame.SetmicroHelp("Days for review must be greater than zero.")
	Return False
end if
	

ls_input_string = ''
Do 
	ls_input_string += tab_1.tabpage_product_shortage.dw_div_product_shortage.GetItemString( &
	   ll_row, 'division_code') + '~t'
	ls_input_string += tab_1.tabpage_product_shortage.dw_div_product_shortage.GetItemString( &
	   ll_row, 'division_name') + '~t'
	ls_input_string += tab_1.tabpage_product_shortage.dw_div_product_shortage.GetItemString( &
	   ll_row, 'send_to_sched_ind') + '~t'	
	ls_input_string += string(tab_1.tabpage_product_shortage.dw_div_product_shortage.GetItemNumber( &
	   ll_row, 'days_for_review')) + '~r~n'
	ll_row = tab_1.tabpage_product_shortage.dw_div_product_shortage.GetNextModified(ll_row, primary!)
	
Loop While ll_row > 0

/*If iu_pas201.nf_pasp96br_inq_upd_div_parameters(istr_error_info, 'U', ls_input_string, ls_output_string) < 0 then
	Return False
End If
*/
If iu_ws_pas4.NF_PASP96FR(istr_error_info, 'U', ls_input_string, ls_output_string) < 0 then
	Return False
End If

iw_frame.SetMicroHelp('Modification Successful')
tab_1.tabpage_product_shortage.dw_div_product_shortage.ResetUpdate()

Return True

end function

on w_division_parameters.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_division_parameters.destroy
call super::destroy
destroy(this.tab_1)
end on

event ue_postopen;call super::ue_postopen;//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "division_parameters"
istr_error_info.se_user_id 		= sqlca.userid

// create user object to use
iu_pas201 = Create u_pas201
iu_ws_pas4 = Create u_ws_pas4

wf_retrieve()
end event

event close;call super::close;// no destroy's needed in Version 6.0
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_previous')

end event

event deactivate;iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_previous')

end event

event resize;call super::resize;tab_1.Resize(This.width - tab_1.X - 50, This.height - tab_1.Y - 110)

tab_1.tabpage_product_shortage.Resize(tab_1.width - tab_1.tabpage_product_shortage.X - 10, &
		tab_1.height - tab_1.tabpage_product_shortage.Y - 25)
tab_1.tabpage_product_shortage.dw_div_product_shortage.Resize(tab_1.tabpage_product_shortage.width - tab_1.tabpage_product_shortage.dw_div_product_shortage.X - 15, &
		tab_1.tabpage_product_shortage.height - tab_1.tabpage_product_shortage.dw_div_product_shortage.Y)



end event

event ue_fileprint;call super::ue_fileprint;IF Not ib_print_ok Then Return

tab_1.tabpage_product_shortage.dw_div_product_shortage.Print()
end event

type tab_1 from u_division_parameters_tab within w_division_parameters
integer x = 15
integer y = 19
integer width = 1807
integer height = 1322
boolean bringtotop = true
end type

