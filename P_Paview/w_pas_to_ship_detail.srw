HA$PBExportHeader$w_pas_to_ship_detail.srw
forward
global type w_pas_to_ship_detail from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_pas_to_ship_detail
end type
type dw_detail from u_base_dw_ext within w_pas_to_ship_detail
end type
end forward

global type w_pas_to_ship_detail from w_base_sheet_ext
integer x = 41
integer y = 268
integer width = 3186
integer height = 1640
string title = "Transfer Order Shipped Detail "
long backcolor = 67108864
event ue_postitemchanged pbm_custom01
dw_header dw_header
dw_detail dw_detail
end type
global w_pas_to_ship_detail w_pas_to_ship_detail

type variables
String		is_order_number, &
				is_ColName, &
				is_delivery_state_updated, &
				is_pa_delivery_date_updated, &
				is_unload_date_updated 

w_netwise_response	iw_parentwindow

u_pas201		iu_pas201

s_error		istr_error_info

boolean		ib_inquire_flag

Long			il_ChangedRow = 0

DataStore	ids_retrieved_data

u_ws_pas5	iu_ws_pas5
end variables

forward prototypes
public subroutine wf_delete ()
public subroutine wf_filenew ()
public function boolean wf_deleterow ()
public subroutine wf_print ()
public function boolean wf_validate (long al_row)
public function boolean wf_addrow ()
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_check_totals ()
end prototypes

event ue_postitemchanged;//If il_ChangedRow > 0 And il_ChangedRow <= dw_production_pt_parameters.RowCount() &
//		And Not iw_frame.iu_string.nf_IsEmpty(is_ChangedColumnName) Then
//	dw_production_pt_parameters.SetItem(il_ChangedRow, is_ChangedColumnName, it_ChangedTime)
//End if

end event

public subroutine wf_delete ();wf_deleterow()
end subroutine

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_deleterow ();//Boolean		lb_ret
//
//dw_production_pt_parameters.SetFocus()
//lb_ret = super::wf_deleterow()
//
//dw_production_pt_parameters.SelectRow(0,False)
//dw_production_pt_parameters.SelectRow(dw_production_pt_parameters.GetRow(),True)
//return lb_ret
//
Return True
end function

public subroutine wf_print ();//dw_production_pt_parameters.Print()
end subroutine

public function boolean wf_validate (long al_row);//Date					ldt_start_date, &
//						ldt_end_date, &
//						ldt_found_start_date, &
//						ldt_found_end_date, &
//						ldt_temp
//
//Long					ll_rtn, &
//						ll_nbrrows
//
//String				ls_division_code, &
//						ls_fab_product, &
//						ls_product_state, &
//						ls_product_descr, &
//						ls_ship_plant, &
//						ls_dest_plant, &
//						ls_searchstring, &
//						ls_temp, &
//						ls_update_flag
//
//
//ll_nbrrows = dw_production_pt_parameters.RowCount()
//
//ls_temp = dw_production_pt_parameters.GetItemString(al_row, "ship_plant")
//IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
//	MessageBox("Ship plant", "Please enter a ship plant.  Ship plant cannot be left blank.")
//	This.SetRedraw(False)
//	dw_production_pt_parameters.ScrollToRow(al_row)
//	dw_production_pt_parameters.SetColumn("ship_plant")
//	dw_production_pt_parameters.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If
//
//ls_temp = dw_production_pt_parameters.GetItemString(al_row, "dest_plant")
//IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
//	MessageBox("Dest plant", "Please enter a dest plant.  Dest plant cannot be left blank.")
//	This.SetRedraw(False)
//	dw_production_pt_parameters.ScrollToRow(al_row)
//	dw_production_pt_parameters.SetColumn("dest_plant")
//	dw_production_pt_parameters.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If
//
//// Check that the start date has a value and is > current date and < end date.
//ls_update_flag = dw_production_pt_parameters.GetItemString(al_row, "update_flag")
//ldt_temp = dw_production_pt_parameters.GetItemDate(al_row, "start_date")
//IF IsNull(ldt_temp) or ldt_temp <= Date('00/00/0000') Then 
//	MessageBox("Start Date", "Please enter a Start Date greater than 00/00/0000")
//	This.SetRedraw(False)
//	dw_production_pt_parameters.ScrollToRow(al_row)
//	dw_production_pt_parameters.SetColumn("start_date")
//	dw_production_pt_parameters.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If
//If ldt_temp < Today() And ls_update_flag = 'A' Then
//	MessageBox("Start Date", "Please enter a Start Date greater than Current Date")
//	This.SetRedraw(False)
//	dw_production_pt_parameters.ScrollToRow(al_row)
//	dw_production_pt_parameters.SetColumn("start_date")
//	dw_production_pt_parameters.SetFocus()
//	This.SetRedraw(True)
//	Return False
//Else
//	If ldt_temp > dw_production_pt_parameters.GetItemDate(al_row, "end_date") And ls_temp = 'A' Then
//		MessageBox("Start Date", "Please enter a Start Date less than End Date")
//		This.SetRedraw(False)
//		dw_production_pt_parameters.ScrollToRow(al_row)
//		dw_production_pt_parameters.SetColumn("start_date")
//		dw_production_pt_parameters.SetFocus()
//		This.SetRedraw(True)
//		Return False
//	End IF
//End If
//
//// Check that the end date has a value and is > current date and start date.
//ldt_temp = dw_production_pt_parameters.GetItemDate(al_row, "end_date")
//IF IsNull(ldt_temp) or ldt_temp <= Date('00/00/0000') Then 
//	MessageBox("End Date", "Please enter an End Date Greater Than 00/00/0000")
//	This.SetRedraw(False)
//	dw_production_pt_parameters.ScrollToRow(al_row)
//	dw_production_pt_parameters.SetColumn("end_date")
//	dw_production_pt_parameters.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If
//If ldt_temp < Today() Then
//	MessageBox("End Date", "Please enter an End Date Greater Than Current Date")
//	This.SetRedraw(False)
//	dw_production_pt_parameters.ScrollToRow(al_row)
//	dw_production_pt_parameters.SetColumn("end_date")
//	dw_production_pt_parameters.SetFocus()
//	This.SetRedraw(True)
//	Return False
//Else
//	If ldt_temp < dw_production_pt_parameters.GetItemDate(al_row, "start_date") Then
//		MessageBox("End Date", "Please enter an End Date Greater Than Start Date")
//		This.SetRedraw(False)
//		dw_production_pt_parameters.ScrollToRow(al_row)
//		dw_production_pt_parameters.SetColumn("end_date")
//		dw_production_pt_parameters.SetFocus()
//		This.SetRedraw(True)
//		Return False
//	End IF
//End If
//
//
//If ll_nbrrows < 2 Then Return True
//
//IF al_row < 0 THEN Return True
//
//ls_fab_product = dw_production_pt_parameters.GetItemString(al_row, "product_code")
//ls_product_state = dw_production_pt_parameters.GetItemString(al_row, "product_state")
//ls_product_descr = dw_production_pt_parameters.GetItemString(al_row, "product_descr")
//ls_ship_plant = dw_production_pt_parameters.GetItemString(al_row, "ship_plant")
//ls_dest_plant = dw_production_pt_parameters.GetItemString(al_row, "dest_plant")
//ldt_start_date = dw_production_pt_parameters.GetItemDate &
//	(al_row, "start_date")
//ldt_end_date = dw_production_pt_parameters.GetItemDate &
//	(al_row, "end_date")
//
//
//ls_SearchString	= "product_code = '" + ls_fab_product + &
//						"' and product_state = '" + ls_product_state + &
//						"' and product_descr = '" + ls_product_descr + &
//						"' and ship_plant = '" + ls_ship_plant + &
//						"' and dest_plant = '" + ls_dest_plant + "'" 
//// Find a matching row excluding the current row.
//
//CHOOSE CASE al_row 
//	CASE 1
//		ll_rtn = dw_production_pt_parameters.Find  &
//				( ls_SearchString, al_row + 1, ll_nbrrows)
//	CASE 2 to (ll_nbrrows - 1)
//		ll_rtn = dw_production_pt_parameters.Find ( ls_SearchString, al_row - 1, 1)
//		If ll_rtn = 0 Then ll_rtn = dw_production_pt_parameters.Find  &
//			(ls_SearchString, al_row + 1, ll_nbrrows)
//	CASE ll_nbrrows 
//		ll_rtn = dw_production_pt_parameters.Find ( ls_SearchString, al_row - 1, 1)
//END CHOOSE
//
//If ll_rtn > 0 Then
//	ldt_found_start_date = dw_production_pt_parameters.GetItemDate &
//	(ll_rtn, "start_date")
//	ldt_found_end_date = dw_production_pt_parameters.GetItemDate &
//	(ll_rtn, "end_date")
//	
//	If (ldt_found_start_date > ldt_start_date And ldt_found_start_date < ldt_end_date) Then
//		MessageBox ("Date", "There are duplicate products with the" + &
//  					" same Ship Plant, Dest Plant and overlapping dates.") 
//	ElseIf (ldt_found_end_date > ldt_start_date And ldt_found_end_date < ldt_end_date) Then
//		MessageBox ("Date", "There are duplicate products with the" + &
//  					" same Ship Plant, Dest Plant and overlapping dates.") 
//	ElseIf (ldt_start_date > ldt_found_start_date and ldt_start_date < ldt_found_end_date) Then
//		MessageBox ("Date", "There are duplicate products with the" + &
//  					" same Ship Plant, Dest Plant and overlapping dates.") 
//	ElseIf(ldt_end_date > ldt_found_start_date and ldt_end_date < ldt_found_end_date) Then
//		MessageBox ("Date", "There are duplicate products with the" + &
//  					" same Ship Plant, Dest Plant and overlapping dates.") 
//	ElseIf (ldt_found_start_date = ldt_start_date) Then
//		MessageBox ("Date", "There are duplicate products with the" + &
//  					" same Ship Plant, Dest Plant and overlapping dates.") 	
//	Else
//		ll_rtn = 0
//	End if
//End if
//If ll_rtn > 0 Then
////		MessageBox ("Date", "There are duplicate products with the" + &
////  					" same Ship Plant, Dest Plant and overlapping dates.") 
//		dw_production_pt_parameters.SetRedraw(False)
//		dw_production_pt_parameters.ScrollToRow(al_row)
//		dw_production_pt_parameters.SetColumn("start_date")
//		dw_production_pt_parameters.SetRow(al_row)
//		dw_production_pt_parameters.SelectRow(ll_rtn, True)
//		dw_production_pt_parameters.SelectRow(al_row, True)
//		dw_production_pt_parameters.SetRedraw(True)
//		Return False
//End if
//
Return True
end function

public function boolean wf_addrow ();//Long 				ll_rownum, &
//					ll_addrow
//dwitemstatus	lis_temp
//String			ls_column
//
//This.setredraw(False)
////check to see if there is a row selected
//ll_rownum = dw_production_pt_parameters.GetSelectedRow(0)
//
//ls_column = 'product_code'	
//
//IF ll_rownum = 0 Then // There is no row selected
//	ll_rownum = dw_production_pt_parameters.RowCount()
//	// If table is empty
//	If ll_rownum = 0 Then
//		Super:: wf_AddRow()
//	Else
//		If dw_production_pt_parameters.GetItemString(ll_rownum,'product_code') > '   ' Then
//			Super:: wf_addrow()
//		End If
//	End If
//	ll_addrow = dw_production_pt_parameters.GetSelectedRow(0)
//// There is a selected row	
//Else
//	ll_addrow = dw_production_pt_parameters.insertrow(ll_rownum + 1)
//	dw_production_pt_parameters.SetRow(ll_addrow)
//End if
//
//dw_production_pt_parameters.setitem(ll_addrow, "end_date", date('12/31/2999'))
//dw_production_pt_parameters.setitem(ll_addrow, "start_date", Today())
//dw_production_pt_parameters.setitem(ll_addrow, "product_state", '1')
//
//dw_production_pt_parameters.SetColumn (ls_column)
//dw_production_pt_parameters.SetFocus()
//
//This.SetRedraw(True)

Return True	


end function

public function boolean wf_retrieve (); 
boolean	lb_rtn, lb_ret

string	ls_header_string_in, &
			ls_header_string_out, &
			ls_detail_string, &
			ls_inquire

long		ll_count

integer	li_ret

u_string_functions	lu_string

This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

if ib_inquire_flag = false then
	return false
end if

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_inquire"
istr_error_info.se_procedure_name = "nf_pasp78cr_inq_to_ship_detail"
istr_error_info.se_message = Space(71)

ls_header_string_in = trim(is_order_number) + '~t'

//li_ret = iu_pas201.nf_pasp78cr_inq_to_ship_detail(istr_error_info, & 
//										ls_header_string_in, ls_header_string_out, &
//										ls_detail_string)

li_ret = iu_ws_pas5.nf_pasp78gr(istr_error_info, ls_header_string_in, ls_header_string_out, ls_detail_string)
dw_header.Reset()
ll_count = dw_header.ImportString(ls_header_string_out)
If ll_count = -1 Then
	dw_header.InsertRow(0)
	dw_header.SetItem(1, "order_number", is_order_number)
//	iw_frame.SetMicroHelp("Order not found")
	dw_detail.ResetUpdate()
	dw_header.ResetUpdate()
	This.SetRedraw(True)
	Return True
End If

dw_header.ResetUpdate()

dw_detail.Reset()
dw_detail.ImportString(ls_detail_string)
dw_detail.ResetUpdate()
dw_detail.SetFocus()

If dw_header.GetItemString(1, "bol_status") > ' ' then
	dw_detail.Object.sched_ship_qty_t.Text = "Shipped"
	dw_detail.Object.sched_ship_wt_t.Text = "Shipped"
	dw_header.Object.unload_date.Protect = 0
	dw_header.Object.unload_date.Background.Color = 16777215 	 
Else
	dw_detail.Object.sched_ship_qty_t.Text = "Scheduled"
	dw_detail.Object.sched_ship_wt_t.Text = "Scheduled"
	dw_header.Object.unload_date.Protect = 1
	dw_header.Object.unload_date.Background.Color = 78682240	
End If

is_delivery_state_updated = "N"
is_pa_delivery_date_updated = "N"
is_unload_date_updated = "N"

This.SetRedraw(False)

ids_retrieved_data.Reset()
ids_retrieved_data.ImportString(ls_detail_string)

//ib_open_inq_window = True

This.SetRedraw(True)

If ll_count > 0 Then
	iw_frame.SetMicroHelp("Inquire Successful")
End If

Return True
end function

public function boolean wf_update ();
long				ll_NbrRows, &
					ll_Row
					
integer			li_ret					

string			ls_detail_string_in, &
					ls_detail_string_out, &
					ls_header_string


IF dw_header.AcceptText() = -1 THEN Return( False )
IF dw_detail.AcceptText() = -1 THEN Return( False )

IF dw_header.ModifiedCount() + dw_detail.ModifiedCount() + dw_detail.DeletedCount() <= 0 THEN 
	iw_frame.SetMicroHelp("No Updates Found.")
	Return( False )
End If

If Not wf_check_totals() Then Return False

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

ll_NbrRows = dw_detail.RowCount( )

ll_Row = 0

//send all lines, not just modified
ls_detail_string_in = dw_detail.Describe("DataWindow.Data")

If dw_header.ModifiedCount() > 0 Then
	ls_header_string = dw_header.Describe("DataWindow.Data") + '~t' + 'U'
Else
	ls_header_string = dw_header.Describe("DataWindow.Data") + '~t' + ' '
End If

ls_header_string += '~t' + is_delivery_state_updated
ls_header_string += '~t' + is_pa_delivery_date_updated 
ls_header_string += '~t' + is_unload_date_updated 

SetPointer(HourGlass!)

//li_ret = iu_pas201.nf_pasp79cr_upd_to_ship_detail(istr_error_info, ls_detail_string_in, ls_detail_string_out, ls_header_string) 
li_ret = iu_ws_pas5.nf_pasp79gr(istr_error_info, ls_detail_string_in, ls_detail_string_out, ls_header_string)

iw_frame.SetMicroHelp("Update Successful")
SetPointer(Arrow!)

dw_header.ResetUpdate()
dw_detail.ResetUpdate()

Return( True )
end function

public function boolean wf_check_totals ();Long		ll_count, &
			ll_old_rowcount, &
			ll_new_rowcount, &
			ll_old_ship_qty_total, &
			ll_new_ship_qty_total, &
			ll_old_find, &
			ll_new_find
			
Decimal{2}	ld_old_ship_wt_total, &
				ld_new_ship_wt_total, &
				ld_temp1, &
				ld_temp2

String	ls_FindString, &
			ls_line_number, &
			ls_product_code, &
			ls_production_date, &
			ls_production_plant
			
Date		ld_ship_date, &
			ld_ship_date_plus_30

	
ll_new_rowcount = dw_detail.RowCount()		
ll_old_rowcount = ids_retrieved_data.RowCount()

For ll_count = 1 to ll_new_rowcount
	ll_new_ship_qty_total = 0
	ld_new_ship_wt_total = 0
	ls_line_number = dw_detail.GetItemString(ll_count, "line_number")
	ls_product_code = dw_detail.GetItemString(ll_count, "product_code")
	ls_production_date = String(dw_detail.GetItemDate(ll_count, "production_date"),"mm/dd/yyyy")
	ls_production_plant = dw_detail.GetItemString(ll_count, "production_plant")	
	
	ls_FindString = "line_number = '" + ls_line_number + "' AND " 
	ls_FindString += "product_code = '" + ls_product_code + "' AND "
	ls_FindString += "production_date = date('" + ls_production_date  + "') AND "
	ls_FindString += "production_plant = '" + ls_production_plant + "'"
	ll_new_find = dw_detail.Find(ls_FindString, 1, ll_new_rowcount)
	
	Do While (ll_new_find > 0)
		ll_new_ship_qty_total += dw_detail.GetItemNumber(ll_new_find, "sched_ship_qty")
		ld_new_ship_wt_total += dw_detail.GetItemNumber(ll_new_find, "sched_ship_weight")
		ll_new_find ++
		If ll_new_find > ll_new_rowcount Then Exit
		ll_new_find = dw_detail.Find(ls_FindString, ll_new_find, ll_new_rowcount)
	Loop 
	
	ll_old_ship_qty_total = 0
	ld_old_ship_wt_total = 0
	
	ll_old_find = ids_retrieved_data.Find(ls_FindString, 1, ll_old_rowcount)
	
	Do While (ll_old_find > 0)
		ll_old_ship_qty_total += ids_retrieved_data.GetItemNumber(ll_old_find, "sched_ship_qty")
		ld_old_ship_wt_total += ids_retrieved_data.GetItemNumber(ll_old_find, "sched_ship_weight")
		ll_old_find ++
		If ll_old_find > ll_old_rowcount Then Exit
		ll_old_find = ids_retrieved_data.Find(ls_FindString, ll_old_find, ll_new_rowcount)
	Loop	
	
	If ll_new_ship_qty_total <> ll_old_ship_qty_total Then 
		MessageBox ("Quantity Error", "New quantity does not equal old quantity of " + String(ll_old_ship_qty_total) +  &
				" for order line " + ls_line_number + ", product " + trim(ls_product_code) + &
				", production date " + ls_production_date + ", production plant " + ls_production_plant) 
		Return False		
	End If
	
	If ld_new_ship_wt_total <> ld_old_ship_wt_total Then 
		MessageBox ("Weight Error", "New weight does not equal old weight of " + String(ld_old_ship_wt_total) +  &
				" for order line " + ls_line_number + ", product  " + trim(ls_product_code) + &
				", production date " + ls_production_date + ", production plant " + ls_production_plant) 
		Return False		
	End If	
	
Next	

For ll_count = 1 to ll_new_rowcount
	ld_temp1 = dw_detail.GetItemNumber(ll_count, "sched_ship_qty")
	If dw_detail.GetItemNumber(ll_count, "sched_ship_qty") > 0 Then
		ld_temp2 = dw_detail.GetItemNumber(ll_count, "sched_ship_weight")
		If dw_detail.GetItemNumber(ll_count, "sched_ship_weight") = 0 Then
			If dw_detail.Object.sched_ship_qty_t.Text = "Scheduled" Then
				MessageBox("Scheduled Weight Error", "Scheduled weight must be greater than zero if scheduled quantity greater than zero")
			Else	
				MessageBox("Shipped Weight Error", "Shipped weight must be greater than zero if shipped quantity greater than zero")
			End If
			dw_detail.SetRow(ll_count)
			dw_detail.SetColumn("sched_ship_weight")
			dw_detail.SelectText(1,10)
			Return False
		End If
	End If
Next

For ll_count = 1 to ll_new_rowcount
	ld_temp1 = dw_detail.GetItemNumber(ll_count, "sched_ship_qty")
	If dw_detail.GetItemNumber(ll_count, "sched_ship_qty") > 0 Then
		ld_temp2 = dw_detail.GetItemNumber(ll_count, "sched_ship_weight")
		If dw_detail.GetItemNumber(ll_count, "sched_ship_weight") = 0 Then
			If dw_detail.Object.sched_ship_qty_t.Text = "Scheduled" Then
				MessageBox("Scheduled Weight Error", "Scheduled weight must be greater than zero if scheduled quantity greater than zero")
			Else	
				MessageBox("Shipped Weight Error", "Shipped weight must be greater than zero if shipped quantity greater than zero")
			End If
			dw_detail.SetRow(ll_count)
			dw_detail.SetColumn("sched_ship_weight")
			dw_detail.SelectText(1,10)
			Return False
		End If
	End If
Next

ld_ship_date = dw_header.GetItemDate(1, "ship_date")
ld_ship_date_plus_30 = RelativeDate (dw_header.GetItemDate(1, "ship_date"), 30)

If dw_header.GetItemDate(1, "pa_delivery_date") < ld_ship_date or &
	dw_header.GetItemDate(1, "pa_delivery_date") >= ld_ship_date_plus_30 Then
	MessageBox ("PA Delivery Date Error", "PA Delivery Date must be equal to or greater than ship date and within 30 days of ship date")
	dw_header.SetRow(1)
	dw_header.SetColumn("pa_delivery_date")
	dw_header.SelectText(1,10)	
	dw_header.SetFocus()
	Return False
End If

If dw_header.GetItemDate(1, "unload_date") > Date("01/01/0001") Then
	If dw_header.GetItemDate(1, "unload_date") < ld_ship_date or &
		dw_header.GetItemDate(1, "unload_date") >= ld_ship_date_plus_30 Then
		MessageBox ("Unload Date Error", "Unload Date must be equal to or greater than ship date and within 30 days of ship date")
		dw_header.SetRow(1)
		dw_header.SetColumn("unload_date")
		dw_header.SelectText(1,10)	
		dw_header.SetFocus()
		Return False
	End If
End If

For ll_count = 1 to ll_new_rowcount
	If dw_detail.GetItemDate(ll_count, "pa_delivery_date") < ld_ship_date or &
		dw_detail.GetItemDate(ll_count, "pa_delivery_date") >= ld_ship_date_plus_30 Then
		MessageBox ("PA Delivery Date Error", "PA Delivery Date must be equal to or greater than ship date and within 30 days of ship date")
		dw_detail.SetRow(ll_count)
		dw_detail.SetColumn("pa_delivery_date")
		dw_detail.SelectText(1,10)	
		dw_detail.SetFocus()
		Return False
	End If	
	
	If dw_detail.GetItemDate(ll_count, "unload_date") > Date("01/01/0001") Then	
		If dw_detail.GetItemDate(ll_count, "unload_date") < ld_ship_date or &
			dw_detail.GetItemDate(ll_count, "unload_date") >= ld_ship_date_plus_30 Then
			MessageBox ("Unload Date Error", "Unload Date must be equal to or greater than ship date and within 30 days of ship date")
			dw_detail.SetRow(ll_count)
			dw_detail.SetColumn("unload_date")
			dw_detail.SelectText(1,10)	
			dw_detail.SetFocus()
			Return False
		End If
	End If	
Next

Return True
end function

event close;call super::close;
IF IsValid( iu_pas201 ) THEN
	DESTROY iu_pas201
END IF

Destroy iu_ws_pas5




end event

event ue_postopen;call super::ue_postopen;ids_retrieved_data = Create DataStore
ids_retrieved_data.DataObject = "d_to_ship_detail_dtl"
iu_ws_pas5 = Create u_ws_pas5

This.PostEvent("ue_query")




end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Disable('m_graph')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')

end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_detail.x * 2) + 30 
li_y = dw_detail.y + 135


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If



if width > li_x Then
	dw_detail.width	= width - li_x
end if

if height > li_y then
	dw_detail.height	= height - li_y
end if
end event

on w_pas_to_ship_detail.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_detail
end on

on w_pas_to_ship_detail.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_detail)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Enable('m_graph')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')


end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'order_number'
		If isnull(dw_header.GetItemString(1, "order_number")) Then
			Message.StringParm = ''
		Else
			Message.StringParm = dw_header.GetItemString(1, "order_number")
		End If
End Choose

	
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'order_number'
		dw_header.SetItem(1, "order_number", as_value)
		is_order_Number = as_value
	case 'close'
		ib_inquire_flag = false
	case 'ok'
		ib_inquire_flag = true		
		
End Choose
end event

event open;call super::open;is_inquire_window_name = 'w_pas_to_ship_detail_inq'

iu_pas201 = Create u_pas201
end event

event ue_addrow;call super::ue_addrow;Integer li_newrow

String	ls_line_num, &
			ls_product, &
			ls_production_plant, &
			ls_destination_plant
			
Date		ld_production_date, &
			ld_pa_delivery_date, &
			ld_unload_date
			 

ls_line_num = dw_detail.GetItemString(dw_detail.GetRow(), "line_number")
ls_product = dw_detail.GetItemString(dw_detail.GetRow(), "product_code")
ld_production_date = dw_detail.GetItemDate(dw_detail.GetRow(), "production_date")
ls_production_plant = dw_detail.GetItemString(dw_detail.GetRow(), "production_plant")
ls_destination_plant = dw_detail.GetItemString(dw_detail.GetRow(), "destination_plant")
ld_pa_delivery_date = dw_detail.GetItemDate(dw_detail.GetRow(), "pa_delivery_date")
ld_unload_date = dw_detail.GetItemDate(dw_detail.GetRow(), "unload_date")

li_newrow = dw_detail.InsertRow(dw_detail.GetRow() + 1)
dw_detail.SetItem(li_newrow, "line_number", ls_line_num)
dw_detail.SetItem(li_newrow, "product_code", ls_product)
dw_detail.SetItem(li_newrow, "production_date", ld_production_date)
dw_detail.SetItem(li_newrow, "production_plant", ls_production_plant)
dw_detail.SetItem(li_newrow, "destination_plant", ls_destination_plant)
dw_detail.SetItem(li_newrow, "pa_delivery_date", ld_pa_delivery_date)
dw_detail.SetItem(li_newrow, "unload_date", ld_unload_date)
dw_detail.SetItem(li_newrow, "pa_del_date_protect", "Y")
dw_detail.SetItem(li_newrow, "unload_date_protect", "Y")

dw_detail.SetItem(li_newrow, "bol_status", dw_header.GetItemString(1, "bol_status"))
dw_detail.SetITem(li_newrow, "sched_ship_qty", 0)
dw_detail.SetITem(li_newrow, "sched_ship_weight", 0)


dw_detail.SetItem(li_newrow, "update_flag", "A")


end event

type dw_header from u_base_dw_ext within w_pas_to_ship_detail
integer width = 3621
integer height = 192
integer taborder = 10
string dataobject = "d_to_ship_detail_hdr"
boolean border = false
end type

event constructor;call super::constructor;DataWindowChild	ldwc_temp

If This.RowCount() = 0 Then This.InsertRow(0)

This.GetChild("delivery_state", ldwc_temp)

ldwc_temp.SetTransObject(SQLCA)
ldwc_temp.Retrieve("PRDSTATE")




end event

event itemchanged;call super::itemchanged;String	ls_ColName

Long		ll_RowCount, & 
			ll_sub

ls_ColName    = GetColumnName()

Choose Case ls_ColName
	Case "pa_delivery_date"
		ll_RowCount = dw_detail.RowCount() 
		For ll_sub = 1 to ll_RowCount
			If dw_detail.GetItemString(ll_sub, "destination_plant") = This.GetItemString(1, "destination_code") Then
				dw_detail.SetItem(ll_sub, "pa_delivery_date", date(data))
			End If
		Next
		is_pa_delivery_date_updated = "Y"
	Case "unload_date"
		ll_RowCount = dw_detail.RowCount() 
		For ll_sub = 1 to ll_RowCount
			If dw_detail.GetItemString(ll_sub, "destination_plant") = This.GetItemString(1, "destination_code") Then
				dw_detail.SetItem(ll_sub, "unload_date", date(data))
			End If
		Next		
		is_unload_date_updated = "Y"
	Case "delivery_state"
		is_delivery_state_updated = "Y"		
End Choose
end event

type dw_detail from u_base_dw_ext within w_pas_to_ship_detail
event ue_dnwdropdown pbm_dwndropdown
event test pbm_dwntabout
integer y = 192
integer width = 3072
integer height = 1248
integer taborder = 30
string dataobject = "d_to_ship_detail_dtl"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dnwdropdown;//Long			ll_row
//string		ls_ColName, &
//				ls_ship_plant, &
//				ls_dest_plant, &
//				ls_product, &
//				ls_state, &
//				ls_division
//
//date			ld_start_date, &
//				ld_end_date
//
//ls_ColName = GetColumnName()
//
//IF ls_ColName = "fab_product_code" THEN
//	GetChild ( "fab_product_code", idwc_fab_product_code )
//	ll_row = GetRow()
//	ls_ship_plant = GetItemString( ll_row, "ship_plant" )
//	ls_dest_plant = GetItemString( ll_row, "dest_plant" )
//	ls_product = GetItemString( ll_row, "fab_product_code" )
//	ls_state = GetItemString( ll_row, "product_state" )
//	
//	ld_start_date = GetItemDate( ll_row, "start_date" )
//	ld_end_date = GetItemDate( ll_row, "end_date" )
//	
//	IF iw_frame.iu_string.nf_IsEmpty(ls_ship_plant) or &
//		iw_frame.iu_string.nf_IsEmpty(ls_dest_plant) or &
//			iw_frame.iu_string.nf_IsEmpty(ls_product) OR &
//				iw_frame.iu_string.nf_IsEmpty(ls_state) or &		
//					NOT IsDate( string(ld_start_Date)) Or IsNull(ld_start_Date) or &
//					NOT IsDate( string(ld_end_Date)) Or IsNull(ld_end_Date) THEN
//		MessageBox( "", "Enter a Valid Plant, Product and Date First.", stopsign!, ok! )
//		Return 1
//	END IF
//	
//	// check to see if you have all the 3 fields needed
//	
//	This.SetItem(This.GetRow(), "fab_product_code", "")
//END IF
end event

event itemchanged;call super::itemchanged;String ls_ColName

ls_ColName    = GetColumnName()

CHOOSE CASE ls_ColName
	CASE "destination_plant"
		If data = dw_header.GetItemString(1, "destination_code") then
			This.SetItem(row, "pa_del_date_protect", "Y")
			This.SetItem(row, "unload_date_protect", "Y")
			This.SetItem(row, "pa_delivery_date", dw_header.GetItemDate(1, "pa_delivery_date"))
			This.SetItem(row, "unload_date", dw_header.GetItemDate(1, "unload_date"))
		Else
			This.SetItem(row, "pa_del_date_protect", "N")
			This.SetItem(row, "unload_date_protect", "N")	
			This.SetItem(row, "pa_delivery_date", date("01/01/0001"))
			This.SetItem(row, "unload_date", date("01/01/0001"))
		End If
End Choose

If This.GetItemString(row, "update_flag") <> "A" Then
	This.SetItem(row, "update_flag", "U")
End If


end event

event itemerror;call super::itemerror;String	ls_column_name, &
			ls_GetText


ls_GetText = This.GetText()

Choose Case This.GetColumnName()
	
Case "product_code","product_state", "ship_plant", "dest_plant", &
		"start_date", "end_date"  
	This.SelectText(1, Len(ls_gettext))
	return 1
End Choose
end event

on ue_graph;//w_graph		w_grp_child
Window			lw_response

//Message.PowerObjectParm = This
//Message.StringParm = 'd_source_weekly_graph'
OpenWithParm(lw_response ,  This, 'w_source_graph_select')
end on

event constructor;call super::constructor;//is_selection = '1'

DataWindowChild	ldwc_child

This.GetChild("product_status", ldwc_child)

ldwc_child.SetTransObject(SQLCA)
ldwc_child.Retrieve('PRODSTAT')

This.GetChild("production_plant", ldwc_child)

ldwc_child.SetTransObject(SQLCA)
ldwc_child.Retrieve()

This.GetChild("destination_plant", ldwc_child)

ldwc_child.SetTransObject(SQLCA)
ldwc_child.Retrieve()
end event

event getfocus;call super::getfocus;If This.rowcount() > 0 then 
	This.SelectText(1, Len(GetText()))
end if
end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

event ue_postconstructor;call super::ue_postconstructor;
//integer 	rtncode
//
//string	st_allrow
//
//st_allrow = "ALL"
//
//rtncode = dw_production_pt_parameters.GetChild('ship_plant', idwc_ship_plant)
//IF rtncode = -1 THEN 
//	MessageBox( "Error", "Ship Plant not a DataWindowChild") 
////	return false
//end if	
//// Establish the connection if not already connected
//CONNECT USING SQLCA;	
//// Set the transaction object for the child
//idwc_ship_plant.SetTransObject(SQLCA)
//idwc_ship_plant.Retrieve()
//idwc_ship_plant.InsertRow(1)
//idwc_ship_plant.SetItem(1,1,st_allrow)
//idwc_ship_plant.SetItem(1,2,st_allrow)
//
//
//rtncode = dw_production_pt_parameters.GetChild('dest_plant', idwc_dest_plant)
//IF rtncode = -1 THEN 
//	MessageBox( "Error", "Dest Plant not a DataWindowChild") 
////	return false
//end if
//// Establish the connection if not already connected
//CONNECT USING SQLCA;	
//// Set the transaction object for the child
//idwc_dest_plant.SetTransObject(SQLCA)
//idwc_dest_plant.Retrieve()
//
//rtncode = dw_production_pt_parameters.GetChild('product_state', idwc_state)
//IF rtncode = -1 THEN 
//	MessageBox( "Error", "Product state not a DataWindowChild") 
//End if
//DataStore	lds_state
//lds_state = Create DataStore
//lds_state.DataObject = "d_tutltype_dddw"
//lds_state.SetTransObject(SQLCA)
//lds_state.Retrieve("PRDSTATE")
//idwc_state.ImportString(lds_state.Describe("DataWindow.Data"))
//idwc_state.SetSort("type_code")
//idwc_state.Sort()
//Destroy lds_state
////return true
end event

