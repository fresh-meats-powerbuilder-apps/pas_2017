HA$PBExportHeader$w_transfer_asap_order_gen.srw
forward
global type w_transfer_asap_order_gen from w_base_sheet_ext
end type
type dw_ship_date from u_asap_dates within w_transfer_asap_order_gen
end type
type dw_short_to_date from u_asap_dates within w_transfer_asap_order_gen
end type
type dw_short_from_date from u_asap_dates within w_transfer_asap_order_gen
end type
type dw_delv_date from u_asap_dates within w_transfer_asap_order_gen
end type
type dw_ship_plant from u_asap_ship_plant within w_transfer_asap_order_gen
end type
type st_max_order_weight from statictext within w_transfer_asap_order_gen
end type
type em_max_order_weight from editmask within w_transfer_asap_order_gen
end type
type st_ship_date from statictext within w_transfer_asap_order_gen
end type
type st_fill_shortages from statictext within w_transfer_asap_order_gen
end type
type em_delv_time from editmask within w_transfer_asap_order_gen
end type
type st_to from statictext within w_transfer_asap_order_gen
end type
type st_delv_date_time from statictext within w_transfer_asap_order_gen
end type
type st_ship_plant from statictext within w_transfer_asap_order_gen
end type
type st_dest_plant from statictext within w_transfer_asap_order_gen
end type
type dw_asap_type from u_asap_type within w_transfer_asap_order_gen
end type
type cb_generate_button from u_base_commandbutton_ext within w_transfer_asap_order_gen
end type
type dw_dest_plant from u_plant within w_transfer_asap_order_gen
end type
end forward

global type w_transfer_asap_order_gen from w_base_sheet_ext
integer x = 910
integer y = 420
integer width = 1874
integer height = 1300
string title = "ASAP Transfer Order Generation"
long backcolor = 67108864
dw_ship_date dw_ship_date
dw_short_to_date dw_short_to_date
dw_short_from_date dw_short_from_date
dw_delv_date dw_delv_date
dw_ship_plant dw_ship_plant
st_max_order_weight st_max_order_weight
em_max_order_weight em_max_order_weight
st_ship_date st_ship_date
st_fill_shortages st_fill_shortages
em_delv_time em_delv_time
st_to st_to
st_delv_date_time st_delv_date_time
st_ship_plant st_ship_plant
st_dest_plant st_dest_plant
dw_asap_type dw_asap_type
cb_generate_button cb_generate_button
dw_dest_plant dw_dest_plant
end type
global w_transfer_asap_order_gen w_transfer_asap_order_gen

type variables
Boolean		ib_async_running

Int		ii_async_commhandle

u_pas201		iu_pas201

u_ws_pas3		iu_ws_pas3

s_error		istr_error_info

long		il_last_clicked_row

String	is_option

end variables

forward prototypes
public subroutine wf_print ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_print ();
end subroutine

public function boolean wf_retrieve ();Return True
end function

event close;call super::close;Destroy iu_pas201
Destroy iu_ws_pas3
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_save')
end event

event resize;call super::resize;//integer li_x		= 1
//integer li_y		= 1
//
//dw_pas_test_upd.width	= width - (50 + li_x)
//dw_pas_test_upd.height	= height - (226 + li_y)
//
//
end event

event ue_postopen;call super::ue_postopen;iu_pas201 = create u_pas201
iu_ws_pas3 = create u_ws_pas3

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "ASAP Transfer Order Generation"
istr_error_info.se_user_id				= sqlca.userid


ib_inquire = False





end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_save')


end event

event ue_query;call super::ue_query;
wf_retrieve()

end event

on w_transfer_asap_order_gen.create
int iCurrent
call super::create
this.dw_ship_date=create dw_ship_date
this.dw_short_to_date=create dw_short_to_date
this.dw_short_from_date=create dw_short_from_date
this.dw_delv_date=create dw_delv_date
this.dw_ship_plant=create dw_ship_plant
this.st_max_order_weight=create st_max_order_weight
this.em_max_order_weight=create em_max_order_weight
this.st_ship_date=create st_ship_date
this.st_fill_shortages=create st_fill_shortages
this.em_delv_time=create em_delv_time
this.st_to=create st_to
this.st_delv_date_time=create st_delv_date_time
this.st_ship_plant=create st_ship_plant
this.st_dest_plant=create st_dest_plant
this.dw_asap_type=create dw_asap_type
this.cb_generate_button=create cb_generate_button
this.dw_dest_plant=create dw_dest_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ship_date
this.Control[iCurrent+2]=this.dw_short_to_date
this.Control[iCurrent+3]=this.dw_short_from_date
this.Control[iCurrent+4]=this.dw_delv_date
this.Control[iCurrent+5]=this.dw_ship_plant
this.Control[iCurrent+6]=this.st_max_order_weight
this.Control[iCurrent+7]=this.em_max_order_weight
this.Control[iCurrent+8]=this.st_ship_date
this.Control[iCurrent+9]=this.st_fill_shortages
this.Control[iCurrent+10]=this.em_delv_time
this.Control[iCurrent+11]=this.st_to
this.Control[iCurrent+12]=this.st_delv_date_time
this.Control[iCurrent+13]=this.st_ship_plant
this.Control[iCurrent+14]=this.st_dest_plant
this.Control[iCurrent+15]=this.dw_asap_type
this.Control[iCurrent+16]=this.cb_generate_button
this.Control[iCurrent+17]=this.dw_dest_plant
end on

on w_transfer_asap_order_gen.destroy
call super::destroy
destroy(this.dw_ship_date)
destroy(this.dw_short_to_date)
destroy(this.dw_short_from_date)
destroy(this.dw_delv_date)
destroy(this.dw_ship_plant)
destroy(this.st_max_order_weight)
destroy(this.em_max_order_weight)
destroy(this.st_ship_date)
destroy(this.st_fill_shortages)
destroy(this.em_delv_time)
destroy(this.st_to)
destroy(this.st_delv_date_time)
destroy(this.st_ship_plant)
destroy(this.st_dest_plant)
destroy(this.dw_asap_type)
destroy(this.cb_generate_button)
destroy(this.dw_dest_plant)
end on

event closequery;call super::closequery;//If KeyDown(KeyShift!) And KeyDown(KeyControl!) Then 
//	Message.ReturnValue = 0
//	return
//End if
//
//If ib_async_running Then
//	MessageBox("Start Make To Order", "This window cannot be closed until the Update task is completed")
//	Message.ReturnValue = 1
//End if
//
end event

event open;call super::open;if dw_delv_date.rowcount( ) = 0 Then
	dw_delv_date.insertrow( 0)
End If

if dw_ship_date.rowcount( ) = 0 Then
	dw_ship_date.insertrow( 0)
End If


if dw_short_from_date.rowcount( ) = 0 Then
	dw_short_from_date.insertrow( 0)
End If

if dw_short_to_date.rowcount( ) = 0 Then
	dw_short_to_date.insertrow( 0)
End If


dw_delv_Date.SetItem(1, "sched_date", Today())

dw_ship_Date.SetItem(1, "sched_date", Today())

dw_short_from_Date.SetItem(1, "sched_date", Today())

dw_short_to_Date.SetItem(1, "sched_date", Today())
end event

type dw_ship_date from u_asap_dates within w_transfer_asap_order_gen
integer x = 471
integer y = 560
integer width = 402
integer height = 96
integer taborder = 70
borderstyle borderstyle = stylelowered!
end type

type dw_short_to_date from u_asap_dates within w_transfer_asap_order_gen
integer x = 987
integer y = 288
integer width = 402
integer height = 96
integer taborder = 50
borderstyle borderstyle = stylelowered!
end type

type dw_short_from_date from u_asap_dates within w_transfer_asap_order_gen
integer x = 471
integer y = 288
integer width = 402
integer height = 96
integer taborder = 40
borderstyle borderstyle = stylelowered!
end type

type dw_delv_date from u_asap_dates within w_transfer_asap_order_gen
integer x = 471
integer y = 160
integer width = 402
integer height = 96
integer taborder = 20
borderstyle borderstyle = stylelowered!
end type

type dw_ship_plant from u_asap_ship_plant within w_transfer_asap_order_gen
integer x = 302
integer y = 432
integer taborder = 60
end type

type st_max_order_weight from statictext within w_transfer_asap_order_gen
integer y = 832
integer width = 443
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Max Order Weight:"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type em_max_order_weight from editmask within w_transfer_asap_order_gen
integer x = 475
integer y = 832
integer width = 329
integer height = 64
integer taborder = 90
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
boolean border = false
alignment alignment = right!
string mask = "#,###,##0"
end type

event constructor;em_max_order_weight.text = '9999999'
This.SelectText(1,9)
end event

type st_ship_date from statictext within w_transfer_asap_order_gen
integer x = 183
integer y = 576
integer width = 256
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ship Date:"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_fill_shortages from statictext within w_transfer_asap_order_gen
integer y = 292
integer width = 443
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Fill Shortages From:"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type em_delv_time from editmask within w_transfer_asap_order_gen
integer x = 914
integer y = 172
integer width = 146
integer height = 64
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
string text = "delv_time"
boolean border = false
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "hh:mm"
end type

type st_to from statictext within w_transfer_asap_order_gen
integer x = 818
integer y = 292
integer width = 146
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "To:"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_delv_date_time from statictext within w_transfer_asap_order_gen
integer y = 164
integer width = 443
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Delivery Date/Time:"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_ship_plant from statictext within w_transfer_asap_order_gen
integer x = 174
integer y = 444
integer width = 128
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ship"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_dest_plant from statictext within w_transfer_asap_order_gen
integer x = 18
integer y = 20
integer width = 293
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Destination"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type dw_asap_type from u_asap_type within w_transfer_asap_order_gen
integer x = 256
integer y = 704
integer width = 878
integer height = 96
integer taborder = 80
borderstyle borderstyle = stylelowered!
end type

type cb_generate_button from u_base_commandbutton_ext within w_transfer_asap_order_gen
integer x = 549
integer y = 992
integer width = 695
integer height = 96
integer taborder = 100
integer weight = 400
string text = "Generate Orders"
end type

event clicked;call super::clicked;String	ls_build_string, &
			ls_dest_plant, &
			ls_ship_plant, &
			ls_asap_type, &
			ls_product, &
			ls_group, &
			ls_selection, &
			ls_system, &
			ls_location_type, &
			ls_max_weight, &
			ls_delv_date
			
Date		ldt_shortage_from_date, &
			ldt_delv_date, &
			ldt_ship_date, &
			ldt_shortage_to_date
time		lt_delv_time
			
Long		ll_ret

dw_dest_plant.AcceptText()
dw_ship_plant.AcceptText()
dw_asap_type.AcceptText()
dw_ship_date.AcceptText()
dw_short_to_date.AcceptText()
dw_short_from_date.AcceptText()
dw_delv_date.AcceptText()

ls_dest_plant = dw_dest_plant.GetItemString(1, "location_code")
If isnull(ls_dest_plant) or ls_dest_plant <= '   ' Then
	MessageBox('Plant Code Error', "Plant Code is Missing")
	dw_dest_plant.SetFocus()
	Return
End If

ls_ship_plant = dw_ship_plant.GetItemString(1, "location_code")
If isnull(ls_ship_plant) or ls_ship_plant <= '   ' Then
	MessageBox('Plant Code Error', "Plant Code is Missing")
	dw_ship_plant.SetFocus()
	Return
End If

if ls_ship_plant = ls_dest_plant then
	MessageBox('Plant Code Error', "Ship plant and Dest Plant cannot be the same")
	dw_dest_plant.SetFocus()
	Return
End If

If Dec(em_max_order_weight.Text) > 0 Then
	ls_max_weight = string(dec(em_max_order_weight.Text))
end if


//ls_delv_date = String(dw_delv_date.GetItemDate(1, "sched_date"),'yyyy-mm-dd')
ldt_delv_date = dw_delv_date.GetItemDate(1, "sched_date")
ldt_ship_date = dw_ship_date.GetItemDate(1, "sched_date")
ldt_shortage_from_date = dw_short_from_date.GetItemDate(1, "sched_date")
ldt_shortage_to_date = dw_short_to_date.GetItemDate(1, "sched_date")
lt_delv_time = time(em_delv_time.text)

if ldt_delv_date  < ldt_ship_date then
	MessageBox('Delivery date error', "Delivery date cannot be before Ship date")
	Return
End If

if ldt_ship_date  < today() then
	MessageBox('Ship date error', "Ship date cannot be less than current date")
	Return
End If

if ldt_shortage_to_date  < ldt_delv_date then
	MessageBox('Shortage to date error', "Shortage to date cannot be before Delivery date")
	Return
End If


if ldt_shortage_to_date  < ldt_shortage_from_date then
	MessageBox('Shortage to date error', "Shortage to date cannot must be greater than Shortage From date")
	Return
End If

ls_asap_type = dw_asap_type.GetItemString(1, "asap_type")
If isnull(ls_asap_type) or ls_asap_type <= '   ' Then
	MessageBox('ASAP type error', "ASAP type is Missing")
	dw_asap_type.SetFocus()
	Return
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

ls_build_string  = ls_dest_plant + '~t'
ls_build_string += String(ldt_delv_date,'yyyy-mm-dd') +'~t' 
ls_build_string += String(lt_delv_time,'hh:mm') +'~t' 
ls_build_string += String(ldt_shortage_from_date,'yyyy-mm-dd') +'~t' 
ls_build_string += String(ldt_shortage_to_date,'yyyy-mm-dd') +'~t' 
ls_build_string += ls_ship_plant + '~t'
ls_build_string += String(ldt_ship_date,'yyyy-mm-dd') +'~t' 
ls_build_string += ls_asap_type + '~t'
ls_build_string += ls_max_weight + '~r~n'


//MessageBox('ls_build_string=', ls_build_string)

//if Not iu_pas201.nf_pasp74cr_transfer_asap_order_gen(istr_error_info, &
//									ls_build_string) THEN Return 
if Not iu_ws_pas3.uf_pasp74gr(istr_error_info, &
									ls_build_string) THEN Return 


//iw_frame.SetMicroHelp("Order Generation Successful")




end event

type dw_dest_plant from u_plant within w_transfer_asap_order_gen
integer x = 302
integer y = 8
integer width = 1445
integer taborder = 10
end type

event itemchanged;call super::itemchanged;//	This.SelectText(1, Len(data))
////	This.SetItem(1, 'location_name', ldw_plant.GetItemString(ll_row, 'location_name'))
//	//SetProfileString( iw_frame.is_UserINI, "Pas", "Lastplant",Trim(data))
//	SetProfileString( iw_frame.is_UserINI, "Pas", "LastPlant",Trim(data))
//	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
////End if
//
//return 
end event

