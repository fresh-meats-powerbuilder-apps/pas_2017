HA$PBExportHeader$w_planned_transfer.srw
$PBExportComments$Planned Transfer. For the editing of TPASPLTR
forward
global type w_planned_transfer from w_netwise_sheet_ole
end type
type ole_planned_transfer from olecustomcontrol within w_planned_transfer
end type
end forward

global type w_planned_transfer from w_netwise_sheet_ole
integer x = 5
integer y = 4
integer width = 3703
integer height = 1284
string title = "Planned Transfer"
long backcolor = 79741120
boolean ib_poolable = true
event ue_inquire ( )
event ue_inquire1 ( )
event ue_new1 ( )
event ue_inquire2 ( )
ole_planned_transfer ole_planned_transfer
end type
global w_planned_transfer w_planned_transfer

type variables
s_error							istr_error_info
string							is_inquire_parm
w_pas_order_detail			iw_parent2
Boolean							ib_no_inquire
w_planned_transfer_range	iw_parent1
w_netwise_sheet				iw_parent
w_pas_where_produced			iw_parent3
boolean							ib_OLE_Error
end variables

forward prototypes
public function boolean wf_update ()
public function boolean wf_addrow ()
public subroutine wf_delete ()
public function boolean wf_deleterow ()
public function boolean wf_retrieve ()
public function boolean wf_query_save_changes ()
public subroutine wf_filenew ()
end prototypes

event ue_inquire();
ib_no_inquire = True
is_inquire_parm = ''
iw_parent.event ue_get_data('SHIP PLANT')
is_inquire_parm  = message.StringParm + '~t'

iw_parent.event ue_get_data('effective_date')
is_inquire_parm += message.StringParm + '~t'

iw_parent.event ue_get_data('SHIP PERIOD')
is_inquire_parm += message.StringParm + '~t'

iw_parent.event ue_get_data('DELV PLANT')
is_inquire_parm += message.StringParm + '~t'

iw_parent.event ue_get_data('DELV DATE')
is_inquire_parm += message.StringParm + '~t'

iw_parent.event ue_get_data('DELV PERIOD')
is_inquire_parm += message.StringParm + '~t'

iw_parent.event ue_get_data('fab_product')
is_inquire_parm += message.StringParm + '~t'

iw_parent.event ue_get_data('fab_product_state')
is_inquire_parm += message.StringParm + '~t'

iw_parent.event ue_get_data('sched_qty')
is_inquire_parm += message.StringParm + '~t'

wf_retrieve()
end event

event ue_inquire1;is_inquire_parm = ''
iw_parent1.event ue_get_data('Inquire')
is_inquire_parm  = message.StringParm 
If is_inquire_parm = "" Then
	wf_filenew()
Else
	wf_retrieve()
End If
end event

event ue_new1;wf_filenew()

end event

event ue_inquire2;is_inquire_parm = ''
iw_parent3.event ue_get_data('Inquire')
is_inquire_parm  = message.StringParm 
If is_inquire_parm = "" Then
	wf_filenew()
Else
	wf_retrieve()
End If
end event

public function boolean wf_update ();boolean lbln_Return

if ib_OLE_Error then return true

SetPointer(HourGlass!)
lbln_Return = ole_planned_transfer.object.Save(true)
SetPointer(Arrow!)

return lbln_Return
end function

public function boolean wf_addrow ();if ib_OLE_Error then return true

return ole_planned_transfer.object.addrow()
end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_deleterow ();if ib_OLE_Error then return true
return ole_planned_transfer.object.deleterow()
end function

public function boolean wf_retrieve ();boolean lbln_Return

if ib_OLE_Error then return true

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "ole_planned_transfer.Inquire"
If Not wf_query_save_changes() Then Return False 
SetPointer(HourGlass!)

If Len(is_inquire_parm) > 0 Then
	lbln_Return = ole_planned_transfer.object.Inquire(Is_inquire_parm )
	// always make sure you clear the Instance Variable for future inquires
	Is_inquire_parm = ''                                                                        
else	
	lbln_Return = ole_planned_transfer.object.Inquire()
End if

return lbln_Return
end function

public function boolean wf_query_save_changes ();Int intMsgboxResult
if ib_OLE_Error then return true

// Do a security check for save and do nothing if the user does not have priviliges
IF NOT gw_base_frame.im_base_menu.m_file.m_save.enabled THEN RETURN True


if ole_planned_transfer.object.OptionMode() then 
	if ole_planned_transfer.object.OptionDataChanged() then
		intMsgboxResult = ole_planned_transfer.object.QuerySaveChanges() 
		if intMsgboxResult = 2 then
			Return False
		end if
	end if
elseif ole_planned_transfer.object.RowDataChanged(true) then
	intMsgboxResult = ole_planned_transfer.object.QuerySaveChanges() 
	if intMsgboxResult = 2 then
		Return False
	end if
end if

RETURN TRUE
end function

public subroutine wf_filenew ();if ib_OLE_Error then return 

If this.ib_pooled Then
	ole_planned_transfer.object.Clear()
ElseIf wf_query_save_changes() Then 
	ole_planned_transfer.object.Clear()
End If

return 
end subroutine

event activate;call super::activate;if ib_OLE_Error then 	
	return
end if

nvuo_pa_business_rules u_rules
If ole_planned_transfer.object.OptionMode() then
	iw_frame.im_menu.mf_Disable('m_delete')
	iw_frame.im_menu.mf_Disable('m_new')
	iw_frame.im_menu.mf_Disable('m_addrow')
	iw_frame.im_menu.mf_Disable('m_deleterow')
	iw_frame.im_menu.mf_Disable('m_inquire')
else	
	If Not gw_base_frame.im_base_menu.m_file.m_save.enabled Then
		iw_frame.im_menu.mf_Disable('m_addrow')
		iw_frame.im_menu.mf_Disable('m_deleterow')
	Else
		iw_frame.im_menu.mf_enable('m_addrow')
		iw_frame.im_menu.mf_enable('m_deleterow')
	End if
End if
	

//iw_frame.im_menu.mf_Disable('m_print')
//iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	

end event

event deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

//iw_frame.im_menu.mf_Enable('m_print')
//iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

end event

event ue_postopen;call super::ue_postopen;string	ls_server_suffix

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_planned_transfer"
istr_error_info.se_user_id 		= sqlca.userid

ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

if ls_server_suffix = "" then
	ls_server_suffix = " "
end if 

ole_planned_Transfer.object.initializecontrol(sqlca.userid,sqlca.DBpass,ls_server_suffix,Message.nf_Get_App_ID(),"Planned Transfer") 

ole_planned_Transfer.object.modifyrights =  gw_base_frame.im_base_menu.m_file.m_save.enabled
ole_planned_Transfer.object.optionrights =  gw_base_frame.im_base_menu.m_file.m_save.enabled 


if this.width  > 10 then
	ole_planned_Transfer.Width = this.width - 50
end if

if this.height > 10 then
	ole_planned_Transfer.Height = this.height - 150
end if


////dave
If isvalid(iw_parent1) Then
	If iw_parent1.title = "Planned Transfer Range" Then
	iw_parent1.Event ue_plantran_handle(This)
	ole_planned_transfer.object.Clear()
	This.event ue_inquire1()
	End If
ElseIf isvalid(iw_parent2) Then
	If iw_parent2.title = "Detail Information" Then
		iw_parent2.Event ue_plantran_handle(This)
		ole_planned_transfer.object.Clear()
		This.event ue_inquire()
	End If
ElseIf isvalid(iw_parent3) Then
	If iw_parent3.title = "Where Produced From" Then
		//iw_parent3.Event ue_plantran_handle(This)
		ole_planned_transfer.object.Clear()
		This.event ue_inquire2()
	End If
Else
	is_inquire_parm = ''
	ole_planned_transfer.object.Clear()
	wf_retrieve()
End If


//ib_no_inquire = False

end event

on w_planned_transfer.create
int iCurrent
call super::create
this.ole_planned_transfer=create ole_planned_transfer
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_planned_transfer
end on

on w_planned_transfer.destroy
call super::destroy
destroy(this.ole_planned_transfer)
end on

event resize;integer 	li_x = 25

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if newwidth > 2694 then
	ole_planned_transfer.Width = newwidth - li_x
end if

if (newheight - 100) > 500 then
	ole_planned_transfer.Height = newheight - 25
end if

end event

event open;call super::open;String ls_temp 


//Dave
iw_parent = Message.PowerObjectParm
If isvalid(iw_parent) Then
	CHOOSE CASE iw_parent.classname()
		CASE "w_planned_transfer_range"
			iw_parent1 = iw_parent
			setnull(iw_parent2)
			setnull(iw_parent3)
		CASE "w_pas_order_detail"
			iw_parent2 = iw_parent
			setnull(iw_parent1)
			setnull(iw_parent3)
		CASE "w_pas_where_produced"
			iw_parent3 = iw_parent
			setnull(iw_parent1)
			setnull(iw_parent2)
	END CHOOSE
End If

//If iw_parent.classname() = "w_pas_order_detail" Then
//	iw_parent2 = iw_parent
//Else
//	iw_parent1 = iw_parent
//End if
//	
//ls_temp = Message.StringParm	

//If Len(ls_temp) > 0 and lower(ls_temp) <> 'w_planned_transfer' Then
//	Is_inquire_parm = ls_temp
//End If

end event

event ue_fileprint;call super::ue_fileprint;ole_planned_transfer.object.PrintReport()
end event

event ue_pooled;call super::ue_pooled;wf_filenew()
end event

type ole_planned_transfer from olecustomcontrol within w_planned_transfer
event messageposted ( ref string strinfomessage )
event transferselected ( ref string strheader,  ref string strrowdata )
integer width = 3648
integer height = 1136
integer taborder = 20
boolean bringtotop = true
boolean border = false
long backcolor = 79741120
boolean focusrectangle = false
string binarykey = "w_planned_transfer.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event messageposted;iw_frame.SetMicroHelp(strinfomessage)
end event

event transferselected;if ole_planned_transfer.object.OptionMode() then
	iw_frame.im_menu.mf_Disable('m_inquire')
	iw_frame.im_menu.mf_Disable('m_delete')
	iw_frame.im_menu.mf_Disable('m_new')
	iw_frame.im_menu.mf_Disable('m_addrow')
	iw_frame.im_menu.mf_Disable('m_deleterow')
else	
	iw_frame.im_menu.mf_Enable('m_inquire')
	iw_frame.im_menu.mf_Enable('m_delete')
	iw_frame.im_menu.mf_Enable('m_new')
	iw_frame.im_menu.mf_Enable('m_addrow')
	iw_frame.im_menu.mf_Enable('m_deleterow')
end if
end event

event getfocus;if ib_OLE_Error then return

ole_planned_Transfer.object.SetFocusBack()
end event

event error;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

if ib_OLE_Error then 	
	action = ExceptionIgnore!
	return
end if

ib_OLE_Error = true

if errornumber <> 20535 then
	lstr_rpc_error_info.se_app_name = "PAS"
	lstr_rpc_error_info.se_window_name = ""
	lstr_rpc_error_info.se_function_name = ""
	lstr_rpc_error_info.se_event_name = errorwindowmenu
	lstr_rpc_error_info.se_procedure_name = errorobject
	lstr_rpc_error_info.se_user_id = ""
	lstr_rpc_error_info.se_return_code = ""
	lstr_rpc_error_info.se_message = "[" + string(errornumber) + "] "  + errortext + "~nThe window will be closed."
	
	lstr_rpc_error_info.se_rval = 0
	lstr_rpc_error_info.se_commerror = 0
	lstr_rpc_error_info.se_commerrmsg = ""
	lstr_rpc_error_info.se_neterror = 0
	lstr_rpc_error_info.se_primaryerror = 0
	lstr_rpc_error_info.se_secondaryerror = 0
	lstr_rpc_error_info.se_neterrmsg = space(100)
	
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
end if

action = ExceptionIgnore! 
close(parent)
end event

event externalexception;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

if ib_OLE_Error then 	
	action = ExceptionIgnore!
	return
end if

ib_OLE_Error = true

lstr_rpc_error_info.se_app_name = "PAS"
lstr_rpc_error_info.se_window_name = ""
lstr_rpc_error_info.se_function_name = ""
lstr_rpc_error_info.se_event_name = parent.title
lstr_rpc_error_info.se_procedure_name = source
lstr_rpc_error_info.se_user_id = ""
lstr_rpc_error_info.se_return_code = ""
lstr_rpc_error_info.se_message = "[" + string(resultcode) + "] "  + "[" + string(exceptioncode) + "] "  + description + "~nThe window will be closed."

lstr_rpc_error_info.se_rval = 0
lstr_rpc_error_info.se_commerror = 0
lstr_rpc_error_info.se_commerrmsg = ""
lstr_rpc_error_info.se_neterror = 0
lstr_rpc_error_info.se_primaryerror = 0
lstr_rpc_error_info.se_secondaryerror = 0
lstr_rpc_error_info.se_neterrmsg = space(100)

openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")

action = ExceptionIgnore! 
close(parent)
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
03w_planned_transfer.bin 
2F00000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000bdbdf94001d32cd000000003000001000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004b33f8f1c11d4512801007b9fdb90280200000000bdbdf94001d32cd0bdbdf94001d32cd000000000000000000000000000000001fffffffe00000003fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
20ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e0065007800740000527a000800034757f20affffffe00065005f00740078006e00650079007400001d5a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b29300000048000800034757f20b000000200065005f00740078006e0065007800740000527a000800034757f20affffffe00065005f00740078006e00650079007400001d5a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000200000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
13w_planned_transfer.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
