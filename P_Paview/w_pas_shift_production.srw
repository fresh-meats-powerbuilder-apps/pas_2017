HA$PBExportHeader$w_pas_shift_production.srw
forward
global type w_pas_shift_production from w_base_child_ext
end type
type dw_shift from u_base_dw_ext within w_pas_shift_production
end type
type cb_cancel from u_base_commandbutton_ext within w_pas_shift_production
end type
type cb_1 from u_help_cb within w_pas_shift_production
end type
end forward

global type w_pas_shift_production from w_base_child_ext
integer width = 1696
integer height = 892
string title = "Shift Production"
boolean minbox = false
boolean maxbox = false
long backcolor = 12632256
dw_shift dw_shift
cb_cancel cb_cancel
cb_1 cb_1
end type
global w_pas_shift_production w_pas_shift_production

on open;call w_base_child_ext::open;//The filter date will be the first field on the string then ~t then string of data to filter

Date		ldt_filter

Int		li_pos

String	ls_data


ls_data = Message.StringParm
li_pos = Pos(ls_data, '~t', 1)

ldt_filter = Date(Left(ls_data, li_pos -1))

ls_data = Right(ls_data, Len(ls_data) - li_pos)
dw_shift.ImportString(ls_data)
dw_shift.SetFilter("sold_date = Date('" + String(ldt_filter) + "')")
dw_shift.Filter()
end on

on w_pas_shift_production.create
int iCurrent
call super::create
this.dw_shift=create dw_shift
this.cb_cancel=create cb_cancel
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_shift
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.cb_1
end on

on w_pas_shift_production.destroy
call super::destroy
destroy(this.dw_shift)
destroy(this.cb_cancel)
destroy(this.cb_1)
end on

type dw_shift from u_base_dw_ext within w_pas_shift_production
integer width = 1632
integer height = 608
integer taborder = 0
string dataobject = "d_pa_view_shift_production"
boolean border = false
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

type cb_cancel from u_base_commandbutton_ext within w_pas_shift_production
integer x = 987
integer y = 616
integer width = 256
integer height = 108
integer taborder = 10
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = "&Cancel"
boolean cancel = true
boolean default = true
end type

on clicked;call u_base_commandbutton_ext::clicked;Close(Parent)
end on

type cb_1 from u_help_cb within w_pas_shift_production
integer x = 1280
integer y = 616
integer width = 256
integer height = 108
integer taborder = 20
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
end type

