HA$PBExportHeader$u_division_parameters_tab.sru
forward
global type u_division_parameters_tab from tab
end type
type tabpage_product_shortage from userobject within u_division_parameters_tab
end type
type dw_div_product_shortage from u_base_dw_ext within tabpage_product_shortage
end type
type tabpage_product_shortage from userobject within u_division_parameters_tab
dw_div_product_shortage dw_div_product_shortage
end type
end forward

global type u_division_parameters_tab from tab
int Width=1806
int Height=1368
int TabOrder=10
boolean RaggedRight=true
int SelectedTab=1
long BackColor=79741120
int TextSize=-8
int Weight=700
string FaceName="MS Sans Serif"
FontCharSet FontCharSet=Ansi!
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
tabpage_product_shortage tabpage_product_shortage
end type
global u_division_parameters_tab u_division_parameters_tab

on u_division_parameters_tab.create
this.tabpage_product_shortage=create tabpage_product_shortage
this.Control[]={this.tabpage_product_shortage}
end on

on u_division_parameters_tab.destroy
destroy(this.tabpage_product_shortage)
end on

type tabpage_product_shortage from userobject within u_division_parameters_tab
int X=18
int Y=100
int Width=1769
int Height=1252
long BackColor=79741120
string Text="Product Shortage"
long TabBackColor=79741120
long TabTextColor=33554432
long PictureMaskColor=536870912
dw_div_product_shortage dw_div_product_shortage
end type

on tabpage_product_shortage.create
this.dw_div_product_shortage=create dw_div_product_shortage
this.Control[]={this.dw_div_product_shortage}
end on

on tabpage_product_shortage.destroy
destroy(this.dw_div_product_shortage)
end on

type dw_div_product_shortage from u_base_dw_ext within tabpage_product_shortage
int X=0
int Y=16
int Width=1769
int Height=1236
int TabOrder=20
string DataObject="d_div_product_shortage"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event constructor;call super::constructor;datawindowchild		ldwc_send_to_sched
Long						ll_ret

This.GetChild('send_to_sched_ind', ldwc_send_to_sched)
ldwc_send_to_sched.SetTransObject(SQLCA)
ll_ret = ldwc_send_to_sched.Retrieve()
end event

event itemchanged;call super::itemchanged;integer	li_value

if dwo.name = 'days_for_review' then
	li_value = integer(data)
	if li_value < 0 then
		iw_Frame.SetMicrohelp("Days for review must be greater than zero.")
		this.SelectText(1,100)
		Return 1
	else
		if iw_frame.wf_getmicrohelp() = "Days for review must be greater than zero." then
			iw_Frame.SetMicrohelp("Ready.")
			Return 0
		end if
	end if
end if	
		

end event

event itemerror;call super::itemerror;Return 1
end event

