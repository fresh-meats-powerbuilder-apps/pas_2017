HA$PBExportHeader$w_actual_prod_not_updated_inq.srw
forward
global type w_actual_prod_not_updated_inq from w_base_response_ext
end type
type dw_date from u_base_dw_ext within w_actual_prod_not_updated_inq
end type
end forward

global type w_actual_prod_not_updated_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1568
integer height = 476
string title = "Actual Production Not Updated Inquire"
long backcolor = 67108864
dw_date dw_date
end type
global w_actual_prod_not_updated_inq w_actual_prod_not_updated_inq

forward prototypes
public function boolean wf_validate ()
end prototypes

public function boolean wf_validate ();
date ld_startDate, ld_endDate, ld_enteredDate
string ls_PARange

dw_date.AcceptText()

If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "PAST", ls_PARange) = -1 Then
	messagebox("PA Range", "PA Range not set in TUTLTYPE")
	return false
end if

ld_endDate = Today()
ld_startDate = RelativeDate(ld_endDate, (integer(ls_PARange) * -1))
ld_enteredDate = dw_date.GetItemDate(1, 'sched_date')

if not (ld_enteredDate >= ld_startDate and ld_enteredDate <= ld_endDate) then
	iw_frame.SetMicroHelp('Date must be between ' + STRING(ld_startDate, 'mm/dd/yyyy') + " and " + string(ld_endDate, 'mm/dd/yyyy'))
	return false
end if

return true
end function

on w_actual_prod_not_updated_inq.create
int iCurrent
call super::create
this.dw_date=create dw_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_date
end on

on w_actual_prod_not_updated_inq.destroy
call super::destroy
destroy(this.dw_date)
end on

event ue_base_ok();call super::ue_base_ok;string ls_date
if not (wf_validate()) then return
ls_date = string(dw_date.GetItemDate(1, 'sched_date'))
iw_parentwindow.Event ue_set_data('sched_date', ls_date)
//ib_valid_return = True
Close(This)
end event

event ue_postopen(unsignedlong wparam, long lparam);call super::ue_postopen;
string theDate

iw_parentwindow.Event ue_get_data('sched_date')
theDate = Message.StringParm

If iw_frame.iu_string.nf_IsEmpty(theDate) Then
	theDate = string(today(), 'mm/dd/yyyy')
end if

dw_date.SetText(theDate)
end event

event ue_base_cancel();call super::ue_base_cancel;Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_actual_prod_not_updated_inq
integer x = 1275
integer y = 256
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_actual_prod_not_updated_inq
integer x = 1275
integer y = 148
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_actual_prod_not_updated_inq
integer x = 1275
integer y = 40
end type

type dw_date from u_base_dw_ext within w_actual_prod_not_updated_inq
integer x = 315
integer y = 120
integer width = 649
integer height = 100
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_sched_date"
boolean border = false
end type

event constructor;call super::constructor;dw_date.InsertRow(0)
end event

