HA$PBExportHeader$w_actual_prod_not_updated.srw
forward
global type w_actual_prod_not_updated from w_base_sheet_ext
end type
type dw_date from u_base_dw_ext within w_actual_prod_not_updated
end type
type dw_prod_not_upd from u_base_dw_ext within w_actual_prod_not_updated
end type
end forward

global type w_actual_prod_not_updated from w_base_sheet_ext
integer width = 1381
integer height = 1296
string title = "Actual Production Not Updated"
long backcolor = 67108864
event ue_open_actual_prod ( )
dw_date dw_date
dw_prod_not_upd dw_prod_not_upd
end type
global w_actual_prod_not_updated w_actual_prod_not_updated

type variables
Boolean		ib_ReInquire
String		is_input
s_error		istr_error_info
u_pas201		iu_pas201
u_ws_pas3		iu_ws_pas3

w_base_sheet	iw_act_prod

Long			il_rbutton_row


end variables

forward prototypes
public function boolean wf_retrieve ()
public subroutine wf_open_actual_production ()
end prototypes

event ue_open_actual_prod();w_base_sheet_ext	lw_parent


/*lw_parent = This.ParentWindow
If IsValid(lw_parent)  Then
	lw_parent.Event ue_set_data('change order',' ')
End if*/
end event

public function boolean wf_retrieve ();Integer	li_ret, &
			li_row_Count, &
			li_counter
			
String	ls_input, &
			ls_output_values, ls_temp, &
			ls_row_option, &
			ls_option
			
Long		ll_value, &
			ll_rtn, &
			ll_row
	
u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_actual_prod_not_updated_inq, this)
	//ls_input = Message.StringParm
	//If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

dw_date.AcceptText()
SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")


ls_input = string(dw_date.GetItemDate(1, 'sched_date'))

If iw_frame.iu_string.nf_IsEmpty(ls_input) Then
	SetMicroHelp("Please inquire on a date.")
	return false
end if

ls_input += '~t' 

is_input = ls_input

//li_ret = iu_pas201.nf_pasp80cr_inq_act_prod_not_upd(istr_error_info, &
//									is_input, &
//									ls_output_values) 
									
li_ret = iu_ws_pas3.uf_pasp80gr(istr_error_info, &
									is_input, &
									ls_output_values) 

This.dw_prod_not_upd.Reset()
If li_ret = 0 Then
	This.dw_prod_not_upd.ImportString(ls_output_values)
End If

This.dw_prod_not_upd.ResetUpdate()

ll_value = dw_prod_not_upd.RowCount()
If ll_value < 0 Then ll_value = 0
IF ll_value > 0 THEN
	dw_prod_not_upd.SetFocus()
	dw_prod_not_upd.ScrollToRow(1)
	//dw_sched_dtl.TriggerEvent("RowFocusChanged")
END IF
SetMicroHelp(String(ll_value) + " rows retrieved")
Return True

end function

public subroutine wf_open_actual_production ();Window	lw_temp
/*
IF IsValid(iw_act_prod) THEN 
	iw_act_prod.TriggerEvent ('ue_inquire') 
	iw_act_prod.setFocus()
else
	OpenSheetWithParm (lw_temp,This,"w_pas_manual_production",iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)
end If*/

string ls_data


ls_data  = dw_prod_not_upd.GetItemString(il_rbutton_row, "plant")    + '~t' //Plant_Code
ls_data += ""                                            + '~t' //Plant_Descr
ls_data += string(dw_date.GetItemDate(1, "sched_date"))  + '~t' //begin_date
ls_data += dw_prod_not_upd.GetItemString(il_rbutton_row, "shift")    + '~t' //shift
ls_data += "Y"                                           + '~t' //production_ind
ls_data += "B"                                           + '~t' //uom_ind
ls_data += ""                                            + '~t' //end_of_shift
ls_data += ""                                            + '~t' //end_of_shift_date
ls_data += ""                                            + '~t' //end_of_shift_time
ls_data += ""                                            + '~t' //FROM_time
ls_data += ""                                            + '~t' //TO_time
ls_data += "A"                                           + '~t' //product indicator
ls_data += STRING(dw_prod_not_upd.GetItemNumber(il_rbutton_row, "areanamecode"), "000000000") + '~t' //area

IF IsValid(iw_act_prod) THEN 
	iw_act_prod.TriggerEvent ('ue_inquire') 
	iw_act_prod.setFocus()
else
	OpenSheetWithParm (lw_temp, ls_data,"w_pas_manual_production",iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)
end If
//OpenSheetWithParm(w_pas_manual_production, "")
//w_pas_manual_production.SetFocus()
end subroutine

on w_actual_prod_not_updated.create
int iCurrent
call super::create
this.dw_date=create dw_date
this.dw_prod_not_upd=create dw_prod_not_upd
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_date
this.Control[iCurrent+2]=this.dw_prod_not_upd
end on

on w_actual_prod_not_updated.destroy
call super::destroy
destroy(this.dw_date)
destroy(this.dw_prod_not_upd)
end on

event ue_postopen;call super::ue_postopen;iu_pas201 = Create u_pas201
iu_ws_pas3 = Create u_ws_pas3
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

dw_date.InsertRow(0)
dw_prod_not_upd.InsertRow(0)

ib_ReInquire = false
wf_retrieve()
end event

event ue_set_data(string as_data_item, string as_value);call super::ue_set_data;

Choose Case as_data_item
	Case 'sched_date'
		dw_date.SetItem(1,"sched_date",Date(as_value))		
End Choose
end event

event ue_get_data(string as_value);call super::ue_get_data;
Choose Case as_value
	Case 'sched_date'
		Message.StringParm = STRING(dw_date.GetItemDate(1, as_value), "MM/DD/YYYY")
end choose
end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_prod_not_upd.x * 2) + 30 
li_y = dw_prod_not_upd.y + 200


if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If



if width > li_x Then
	dw_prod_not_upd.width	= width - li_x
end if

if height > li_y then
	dw_prod_not_upd.height	= height - li_y
end if
end event

type dw_date from u_base_dw_ext within w_actual_prod_not_updated
integer x = 178
integer y = 96
integer width = 731
integer height = 96
integer taborder = 10
string dataobject = "d_sched_date"
end type

event constructor;call super::constructor;This.object.sched_date.Background.Color = 12632256
This.object.sched_date.Protect = 1
ib_updateable = False

end event

type dw_prod_not_upd from u_base_dw_ext within w_actual_prod_not_updated
integer x = 146
integer y = 192
integer width = 1097
integer height = 928
integer taborder = 20
string dataobject = "d_act_prd_not_upd"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;//This.object.plantcode.Background.Color = 12632256
This.object.plantcode.Protect = 1
This.object.planttype.Protect = 1
This.object.shift.Protect = 1
ib_updateable = False
//This.object.areanamecode.Protect = 1
end event

event rbuttondown;call super::rbuttondown;m_production	lm_popup
String plantCode, shift
integer areaNameCode 


IF row > 0 Then
	
	dw_prod_not_upd.SetRow ( row )
	il_rbutton_row = row
	
	//dw_prod_not_upd.SelectRow ( row, True )
	
	plantCode = This.GetItemString ( row, "plantcode")
	shift = This.GetItemString(row, "shift")
	areaNameCode = This.GetItemNumber(row, "areanamecode")
	
	lm_popup = Create m_production
	lm_popup.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())
	Destroy lm_popup
end if
end event

