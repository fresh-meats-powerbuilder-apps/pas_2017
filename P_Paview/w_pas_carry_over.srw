HA$PBExportHeader$w_pas_carry_over.srw
forward
global type w_pas_carry_over from w_base_child_ext
end type
type dw_carry_over from u_base_dw_ext within w_pas_carry_over
end type
type cb_cancel from u_base_commandbutton_ext within w_pas_carry_over
end type
type dw_header from u_base_dw_ext within w_pas_carry_over
end type
type cb_1 from u_help_cb within w_pas_carry_over
end type
end forward

global type w_pas_carry_over from w_base_child_ext
integer x = 5
integer y = 136
integer width = 2885
integer height = 1360
string title = ""
boolean minbox = false
boolean maxbox = false
long backcolor = 67108864
dw_carry_over dw_carry_over
cb_cancel cb_cancel
dw_header dw_header
cb_1 cb_1
end type
global w_pas_carry_over w_pas_carry_over

type variables
w_pas_pa_view	iw_parent
end variables

event ue_postopen;call super::ue_postopen;String	ls_data, &
			ls_window_type, &
			ls_selected_date, &
			ls_filter

If Not iw_parent.wf_carry_over_ready() Then
	// if the data is not back from the async rpc, then wait until it is
	MessageBox("Data Retrieval", "Data Retrieval is in Progress.  Please try again later.")
	Close(This)
	return
End if

dw_carry_over.SetRedraw(False)

ls_selected_date = String(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd")

ls_data = iw_parent.wf_get_co_fut_sales()
If Len(ls_data) = 0 Then Close(This)

ls_window_type = Left(ls_data, 1)

ls_data = Right(ls_data, Len(ls_data) - 2)

If ls_window_type = 'C' Then
	This.Title = 'Carry Over Production'
	ls_filter = "ship_date = Date('" + ls_selected_date + "')"
Else
	This.Title = 'Future Sales'
	ls_filter = "ready_to_ship_date = Date('" + ls_selected_date + "')" + &
			" and ship_date > Date('" + ls_selected_date + "')"
	//These next three lines will need to be commented out
//	If Date(ls_selected_date) = Today() Then
//		ls_filter += " or where_from_ind = 'I'"
//	End if
End if

dw_carry_over.ImportString(ls_data)
dw_carry_over.SetFilter(ls_filter)
dw_carry_over.Filter()

dw_carry_over.SetRedraw(True)

end event

event open;call super::open;Int	li_tab, &
		li_end_tab

String	ls_input


iw_parent = Message.PowerObjectParm

ls_input = iw_parent.wf_GetHeaderInfo()
li_tab = iw_frame.iu_string.nf_npos(ls_input, '~t', 1, 4)
li_end_tab = Pos(ls_input, '~t', li_tab + 1)
ls_input = Replace(ls_input, li_tab + 1, li_end_tab - li_tab - 1, &
				String(iw_parent.wf_get_selected_date(), "yyyy-mm-dd")) 
				

dw_header.ImportString(ls_input)


end event

on w_pas_carry_over.create
int iCurrent
call super::create
this.dw_carry_over=create dw_carry_over
this.cb_cancel=create cb_cancel
this.dw_header=create dw_header
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_carry_over
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.dw_header
this.Control[iCurrent+4]=this.cb_1
end on

on w_pas_carry_over.destroy
call super::destroy
destroy(this.dw_carry_over)
destroy(this.cb_cancel)
destroy(this.dw_header)
destroy(this.cb_1)
end on

type dw_carry_over from u_base_dw_ext within w_pas_carry_over
integer y = 192
integer width = 2825
integer taborder = 0
string dataobject = "d_carry_over"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

type cb_cancel from u_base_commandbutton_ext within w_pas_carry_over
integer x = 1760
integer y = 1100
integer width = 256
integer height = 108
integer taborder = 10
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = "&Cancel"
boolean cancel = true
boolean default = true
end type

on clicked;call u_base_commandbutton_ext::clicked;Close(Parent)
end on

type dw_header from u_base_dw_ext within w_pas_carry_over
integer x = 5
integer width = 2144
integer height = 180
integer taborder = 0
string dataobject = "d_where_produced_header"
boolean border = false
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

type cb_1 from u_help_cb within w_pas_carry_over
integer x = 2048
integer y = 1100
integer width = 256
integer height = 108
integer taborder = 20
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
end type

