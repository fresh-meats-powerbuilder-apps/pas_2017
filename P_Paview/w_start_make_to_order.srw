HA$PBExportHeader$w_start_make_to_order.srw
forward
global type w_start_make_to_order from w_base_sheet_ext
end type
type uo_groups from u_group_list within w_start_make_to_order
end type
type cb_mto_button from u_base_commandbutton_ext within w_start_make_to_order
end type
type dw_product from u_fab_product_code within w_start_make_to_order
end type
type dw_plant from u_plant within w_start_make_to_order
end type
type dw_selection from u_base_dw_ext within w_start_make_to_order
end type
type dw_shift from u_base_dw_ext within w_start_make_to_order
end type
type dw_to_date from u_base_dw_ext within w_start_make_to_order
end type
type dw_from_date from u_base_dw_ext within w_start_make_to_order
end type
end forward

global type w_start_make_to_order from w_base_sheet_ext
integer x = 910
integer y = 420
integer width = 1792
integer height = 1512
string title = "Start Make To Order"
boolean resizable = false
long backcolor = 67108864
uo_groups uo_groups
cb_mto_button cb_mto_button
dw_product dw_product
dw_plant dw_plant
dw_selection dw_selection
dw_shift dw_shift
dw_to_date dw_to_date
dw_from_date dw_from_date
end type
global w_start_make_to_order w_start_make_to_order

type variables
Boolean		ib_async_running

Int		ii_async_commhandle

u_pas203		iu_pas203

u_ws_pas3		iu_ws_pas3

s_error		istr_error_info

long		il_last_clicked_row

String	is_option

end variables

forward prototypes
public subroutine wf_print ()
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_print ();
end subroutine

public function boolean wf_retrieve ();Return True
end function

event close;call super::close;Destroy iu_pas203
Destroy iu_ws_pas3
end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_save')
end event

event resize;call super::resize;//integer li_x		= 1
//integer li_y		= 1
//
//dw_pas_test_upd.width	= width - (50 + li_x)
//dw_pas_test_upd.height	= height - (226 + li_y)
//
//
end event

event ue_postopen;call super::ue_postopen;iu_pas203 = create u_pas203
iu_ws_pas3 = create u_ws_pas3

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "Made to Order"
istr_error_info.se_user_id				= sqlca.userid

string ls_prod

ls_prod = dw_product.GetItemString(1, 'fab_product_code') + "~t" + &
					dw_product.GetItemString(1, 'fab_product_description') + "~t" + &
					dw_product.GetItemString(1, 'product_state') + "~t" + &
					dw_product.GetItemString(1, 'product_state_description')
								
If Not iw_frame.iu_string.nf_IsEmpty(ls_prod) Then 
	dw_product.Reset()
	dw_product.uf_importstring(ls_prod, TRUE)
End If

dw_product.uf_enable_state(false)
dw_product.uf_set_product_state('1')

//ole_group_display.object.GroupType(3)
//ole_group_display.object.LoadObject()
uo_groups.uf_load_groups('P')



ib_inquire = False





end event

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_save')


end event

event ue_query;call super::ue_query;
wf_retrieve()

end event

on w_start_make_to_order.create
int iCurrent
call super::create
this.uo_groups=create uo_groups
this.cb_mto_button=create cb_mto_button
this.dw_product=create dw_product
this.dw_plant=create dw_plant
this.dw_selection=create dw_selection
this.dw_shift=create dw_shift
this.dw_to_date=create dw_to_date
this.dw_from_date=create dw_from_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uo_groups
this.Control[iCurrent+2]=this.cb_mto_button
this.Control[iCurrent+3]=this.dw_product
this.Control[iCurrent+4]=this.dw_plant
this.Control[iCurrent+5]=this.dw_selection
this.Control[iCurrent+6]=this.dw_shift
this.Control[iCurrent+7]=this.dw_to_date
this.Control[iCurrent+8]=this.dw_from_date
end on

on w_start_make_to_order.destroy
call super::destroy
destroy(this.uo_groups)
destroy(this.cb_mto_button)
destroy(this.dw_product)
destroy(this.dw_plant)
destroy(this.dw_selection)
destroy(this.dw_shift)
destroy(this.dw_to_date)
destroy(this.dw_from_date)
end on

event ue_get_data;call super::ue_get_data;//Choose Case as_value
//	Case 'division'
//		Message.StringParm = dw_division.uf_get_division()
//End Choose


end event

event ue_set_data;call super::ue_set_data;//Choose Case as_data_item
//	case 'division'
//		dw_division.uf_set_division(as_value)
//End Choose

end event

event ue_fileprint;call super::ue_fileprint;//DataStore				lds_print
//DataWindowChild		ldwc_division, &
//							ldwc_detail
//
//
//lds_print = Create DataStore
//
//lds_print.DataObject = 'd_pas_rpt_data_coll_prt'
//
//lds_print.GetChild('dw_division', ldwc_division)
//lds_print.GetChild('dw_detail', ldwc_detail)
//
//dw_division.ShareData(ldwc_division)
//dw_pas_test_upd.ShareData(ldwc_detail)
//
//
//lds_print.print()
//
//destroy lds_print


end event

event timer;//Boolean	lb_ret
//
//String	ls_header, &
//			ls_input_string
//
//
//If iu_pas203.nf_async_complete(ii_async_commhandle) Then
//	ls_header = Space(20)
//	ls_input_string = Space(501)
//	lb_ret = iu_pas203.nf_pasp94br_upd_str_mk_to_ord(istr_error_info, &
//												ls_header, &
//												ls_input_string, &
//												ii_async_commhandle)
//
//	This.Pointer = ''
//	dw_division.Modify("DataWindow.Pointer = ''")
//	dw_pas_test_upd.Modify("DataWindow.Pointer = ''")
//	
//	If lb_ret Then MessageBox('Start Make To Order', "Update Successful.")
//
//	iu_pas203.nf_Release_CommHandle(ii_async_commhandle)
//	ii_async_commhandle = 0
//	ib_async_running = False
//	Timer(0, This)
//	dw_pas_test_upd.SelectRow(0, False)
//End if
end event

event closequery;call super::closequery;If KeyDown(KeyShift!) And KeyDown(KeyControl!) Then 
	Message.ReturnValue = 0
	return
End if

If ib_async_running Then
	MessageBox("Start Make To Order", "This window cannot be closed until the Update task is completed")
	Message.ReturnValue = 1
End if

end event

event open;call super::open;is_option = Message.StringParm
CHOOSE CASE is_option
	CASE "DS"
		This.Title = "Made to Order Decrease Shift"
		cb_mto_button.Text = "Decrease the Shift's Production"
		dw_to_date.Visible = False
		dw_shift.Visible = True
	CASE "D "
		This.Title = "Made to Decrease"
		cb_mto_button.Text = "Initiate Made to Decrease"
		dw_to_date.Visible = True
		dw_shift.Visible = False
	CASE "I "
		This.Title = "Made to Increase"	
		cb_mto_button.Text = "Initiate Made to Increase"
		dw_to_date.Visible = True
		dw_shift.Visible = False
END CHOOSE

dw_product.Visible = False
//ole_group_display.Visible = False
uo_groups.Visible = False


end event

type uo_groups from u_group_list within w_start_make_to_order
integer x = 370
integer y = 640
integer width = 1216
integer height = 548
integer taborder = 60
end type

on uo_groups.destroy
call u_group_list::destroy
end on

type cb_mto_button from u_base_commandbutton_ext within w_start_make_to_order
integer x = 50
integer y = 1216
integer width = 1029
integer height = 120
integer taborder = 70
string text = ""
end type

event clicked;call super::clicked;String	ls_build_string, &
			ls_plant, &
			ls_shift, &
			ls_product, &
			ls_group, &
			ls_selection, &
			ls_system, &
			ls_location_type
			
Date		ldt_from_date, &
			ldt_to_date
			
Long		ll_ret

dw_plant.AcceptText()
dw_from_date.AcceptText()
dw_to_date.AcceptText()
dw_shift.AcceptText()
dw_selection.AcceptText()
dw_product.AcceptText()

ls_plant = dw_plant.GetItemString(1, "location_code")
If isnull(ls_plant) or ls_plant <= '   ' Then
	MessageBox('Plant Code Error', "Plant Code is Missing")
	dw_plant.SetFocus()
	Return
Else
	If is_option = 'D ' Then
		// Establish the connection if not already connected
		CONNECT USING SQLCA;	
		SELECT locations.location_type  
		  INTO :ls_location_type  
		FROM locations  
		  WHERE ( locations.location_code = :ls_plant ); 
		If (ls_location_type = 'F') OR  (ls_location_type = 'Z') Then
			MessageBox('Plant Code Error', "Plant cannot be FWH for Made to Decrease")
			dw_plant.SetFocus()
			Return
		End If
	End If
End If
	

ldt_from_date = dw_from_date.GetItemDate(1, "from_date")
If isnull(ldt_from_date) Then
	MessageBox('From Date Error', "From Date is Missing")
	Return
End If

If (is_option = 'D ') or (is_option = 'I ') Then
	ldt_to_date = dw_to_date.GetItemDate(1, "to_date")
	If isnull(ldt_to_date) Then
		MessageBox('To Date Error', "To Date is Missing")
		Return
	End If
End If

If (is_option = 'DS') Then
	ls_shift = dw_shift.GetItemString(1, "shift")
	If isnull(ls_shift) or ls_shift <= ' ' Then
		MessageBox('Shift Error', "Shift is Missing")
		Return
	End If
Else
	ls_shift = ' '
End If 

ls_selection = dw_selection.GetItemString(1, "selection")
Choose Case ls_selection
	Case 'M' 
		ls_product = ''
		ls_system = ''
		ls_group = '0'
	Case 'P'
		ls_product = dw_product.GetItemString(1, "fab_product_code")
		if isnull(ls_product) or ls_product <= '          ' Then
			MessageBox('Product Error', "Product is Missing")
			Return
		End If
		ls_system = ''
		ls_group = '0'
	Case 'G'
		ls_product = ''
//		ls_system = string(ole_group_display.object.systemname())
//		ls_group = string(ole_group_display.object.groupID())
		ls_system = uo_groups.uf_get_owner()
		ls_group = string(uo_groups.uf_get_sel_id(ls_group))
End Choose

ls_build_string  = is_option + '~t' 
ls_build_string += ls_plant + '~t'
ls_build_string += String(ldt_from_date,'yyyy/mm/dd') +'~t' 
ls_build_string += String(ldt_to_date,'yyyy/mm/dd') +'~t' 
ls_build_string += ls_shift + '~t'
ls_build_string += ls_selection + '~t'
ls_build_string += ls_product + '~t'
ls_build_string += ls_system + '~t'
ls_build_string += ls_group + '~r~n'


//MessageBox('ls_build_string=', ls_build_string)

//ll_ret = iu_pas203.nf_pasp28cr_init_made_to_order(istr_error_info, &
//									ls_build_string) 
ll_ret = iu_ws_pas3.uf_pasp28gr(istr_error_info, &
									ls_build_string) 




end event

type dw_product from u_fab_product_code within w_start_make_to_order
integer y = 512
integer height = 104
integer taborder = 50
end type

event itemchanged;call super::itemchanged;dw_product.object.product_state.Protect = 1
dw_product.object.product_status.Protect = 1



end event

type dw_plant from u_plant within w_start_make_to_order
integer x = 9
integer y = 8
integer width = 1445
integer taborder = 80
end type

type dw_selection from u_base_dw_ext within w_start_make_to_order
integer x = 5
integer y = 284
integer width = 823
integer height = 228
integer taborder = 40
string dataobject = "d_mto_selection"
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then
	This.InsertRow(0)
End If

This.SetItem(1, 'selection', 'M') 
end event

event itemchanged;call super::itemchanged;Choose Case data
	Case 'M'
		dw_product.Visible = False
//		ole_group_display.Visible = False
		uo_groups.Visible = False
	Case 'P'
		dw_product.Visible = True
//		ole_group_display.Visible = False
		uo_groups.Visible = False
	Case 'G'
		dw_product.Visible = False
//		ole_group_display.Visible = True
		uo_groups.Visible = True
End Choose
end event

type dw_shift from u_base_dw_ext within w_start_make_to_order
integer x = 9
integer y = 192
integer width = 352
integer height = 80
integer taborder = 30
string dataobject = "d_mto_shift"
boolean border = false
end type

event constructor;call super::constructor;//If This.Rowcount() = 0 Then
//	This.InsertRow(0)
//End If


DataWindowChild		ldwc_type


This.GetChild("shift", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("shift")
This.InsertRow(0)

end event

type dw_to_date from u_base_dw_ext within w_start_make_to_order
integer x = 494
integer y = 96
integer width = 453
integer height = 100
integer taborder = 20
string dataobject = "d_mto_to_date"
boolean border = false
end type

event constructor;call super::constructor;If This.Rowcount() = 0 Then
	This.InsertRow(0)
End If

This.SetItem(1, "to_date", RelativeDate(Today(), 7))
end event

type dw_from_date from u_base_dw_ext within w_start_make_to_order
integer x = 9
integer y = 96
integer width = 485
integer height = 92
integer taborder = 10
string dataobject = "d_mto_from_date"
boolean border = false
end type

event constructor;call super::constructor;If This.Rowcount() = 0 Then
	This.InsertRow(0)
End If

This.SetItem(1, "from_date", Today())
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
05w_start_make_to_order.bin 
2300000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000004fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000cbbcc1b001d2d57900000003000001000000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004800000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004d88d0dd94b17bf242ce0499e65d215f300000000cbbcc1b001d2d579cbbcc1b001d2d57900000000000000000000000000000001fffffffe00000003fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
26ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000b29300000048000800034757f20b000000200065005f00740078006e00650078007400001727000800034757f20affffffe00065005f00740078006e00650079007400000d70000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000200000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
15w_start_make_to_order.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
