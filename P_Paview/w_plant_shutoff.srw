HA$PBExportHeader$w_plant_shutoff.srw
forward
global type w_plant_shutoff from w_base_sheet_ext
end type
type dw_plant_shutoff from u_base_dw_ext within w_plant_shutoff
end type
end forward

global type w_plant_shutoff from w_base_sheet_ext
integer width = 2162
integer height = 1592
string title = "Lock Plant PA"
long backcolor = 12632256
event ue_getdata ( )
dw_plant_shutoff dw_plant_shutoff
end type
global w_plant_shutoff w_plant_shutoff

type variables
DataStore	ids_locations

u_pas203		iu_pas203

u_ws_pas3		iu_ws_pas3

s_error		istr_error_info

String		is_plant_type
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public function boolean wf_retrieve ();Date			ldt_input_date

Integer		li_count

String		ls_plants, &
				ls_input_string, &
				ls_input_plant, &
				ls_input_status, &
				ls_plant_type[], &
				ls_temp, &
				ls_description

Time			lt_input_time

Long			ll_RowCount, &
				ll_row


IF Not Super::wf_retrieve() Then Return False

  SELECT tutltypes.type_desc  
    INTO :ls_description  
    FROM tutltypes  
   WHERE ( tutltypes.record_type = 'PLTGROUP' ) AND  
         ( tutltypes.type_short_desc = :is_plant_type )   ;

This.Title = 'Lock Plant PA ' + ls_description

ls_temp = is_plant_type
for li_count = 1 to len(Trim(is_plant_type))
	ls_plant_type[li_count] = left(ls_temp, 1)
	ls_temp = Right(ls_temp, len(ls_temp) - 1)
Next
	
ids_locations.SetTransObject(SQLCA)
ids_locations.Retrieve(ls_plant_type[])

ls_plants = ids_locations.object.datawindow.data

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp59br_inq_plant_shutoff"
istr_error_info.se_message = Space(71)

dw_plant_shutoff.SetRedraw(False)
dw_plant_shutoff.Reset()

//If iu_pas203.nf_pasp59br_inq_plant_shutoff(istr_error_info, ls_input_string) < 0 Then
If iu_ws_pas3.uf_pasp59fr(istr_error_info, ls_input_string) < 0 Then
	dw_plant_shutoff.SetRedraw(True)
	Return False
End If

dw_plant_shutoff.ImportString(ls_plants)

ll_rowcount = dw_plant_shutoff.RowCount()

Do While Not iw_frame.iu_string.nf_IsEmpty(ls_input_string)
	ls_input_plant = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
	ls_input_status = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
	ldt_input_date = Date(iw_frame.iu_string.nf_GetToken(ls_input_string, '~t'))
	lt_input_time = Time(iw_frame.iu_string.nf_GetToken(ls_input_string, '~r~n'))
	
	ll_row = dw_plant_shutoff.Find("plant = '"+ ls_input_plant +"'", 1, ll_rowcount)
	
	If ll_row > 0 Then
		dw_plant_shutoff.SetItem(ll_row, 'shutoff_status', ls_input_status)
		dw_plant_shutoff.SetItem(ll_row, 'shutoff_Date', ldt_input_date)
		dw_plant_shutoff.SetItem(ll_row, 'shutoff_Time', lt_input_time)
	End if

Loop

dw_plant_shutoff.SetRedraw(True)
dw_plant_shutoff.ResetUpdate()

Return True
end function

public function boolean wf_update ();Integer			li_changed_row[], &
					li_count

String			ls_input_string, &
					ls_output_string

Long				ll_row


ll_row = dw_plant_shutoff.GetNextModified(0, primary!)
If ll_row = 0 Then
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End If

li_count = 0
ls_input_string = ''
Do 
	ls_input_string += dw_plant_shutoff.GetItemString(ll_row, 'plant') + '~t'
	ls_input_string += dw_plant_shutoff.GetItemString(ll_row, 'shutoff_status') + '~r~n'
	li_count ++
	li_changed_row[li_count] = ll_row
	ll_row = dw_plant_shutoff.GetNextModified(ll_row, primary!)
	
Loop While ll_row > 0

//If iu_pas203.nf_pasp60br_update_plant_shutoff(istr_error_info, ls_input_string) <> 0 Then
If iu_ws_pas3.uf_pasp60fr(istr_error_info, ls_input_string) <> 0 Then
	Return False
End If

for li_count = 1 to upperbound(li_changed_row)
	dw_plant_shutoff.SetItem(li_changed_row[li_count], 'shutoff_date', Today())
	dw_plant_shutoff.SetItem(li_changed_row[li_count], 'shutoff_time', Now())
Next

iw_frame.SetMicroHelp('Modification Successful')
dw_plant_shutoff.ResetUpdate()
Return True

end function

event close;call super::close;Destroy ids_locations
Destroy iu_pas203
Destroy iu_ws_pas3
end event

on w_plant_shutoff.create
int iCurrent
call super::create
this.dw_plant_shutoff=create dw_plant_shutoff
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant_shutoff
end on

on w_plant_shutoff.destroy
call super::destroy
destroy(this.dw_plant_shutoff)
end on

event ue_postopen;call super::ue_postopen;istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "Lock Plant PA"
istr_error_info.se_user_id 		= sqlca.userid

is_inquire_window_name = 'w_plant_type_inq'

iu_pas203 = Create u_pas203
iu_ws_pas3 = Create u_ws_pas3

ids_locations = Create DataStore
ids_locations.DataObject = 'd_location_code_name'

wf_retrieve()
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event deactivate;call super::deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

end event

event ue_fileprint;call super::ue_fileprint;dw_plant_shutoff.Print()
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant_type'
		is_plant_type = as_value
End Choose
end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant_type'
		message.StringParm = is_plant_type
End Choose
end event

event resize;call super::resize; integer li_x		= 20
 integer li_y		= 30

  
dw_plant_shutoff.width	= newwidth - li_x
dw_plant_shutoff.height	= newheight - li_y


end event

type dw_plant_shutoff from u_base_dw_ext within w_plant_shutoff
event ue_post_itemchanged ( long row,  dwobject dwo )
integer x = 5
integer y = 16
integer width = 2053
integer height = 1424
integer taborder = 10
string dataobject = "d_plant_shutoff"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_post_itemchanged;call super::ue_post_itemchanged;String				ls_original, &
						ls_new

ls_original = This.GetItemString(row, 'shutoff_status', primary!, True)
ls_new = This.GetItemString(row, 'shutoff_status')
	
	
	
	
	If (ls_new = ls_original) Or (iw_frame.iu_string.nf_IsEmpty(ls_original) and ls_new = 'STA') Then
		This.SetItemStatus(row, 'shutoff_status', primary!, notmodified!)
	End If

end event

event rbuttondown;call super::rbuttondown;m_plant_lock_pa	lm_popup

lm_popup = Create m_plant_lock_pa

lm_popup.m_plantlockpa.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup


end event

event itemchanged;call super::itemchanged;dwitemstatus ls_temp 

If dwo.name = 'shutoff_status' Then
	This.EVENT POST ue_post_itemchanged(row, dwo)
End if
	
end event

