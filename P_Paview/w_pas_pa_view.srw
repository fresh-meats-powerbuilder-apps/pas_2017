HA$PBExportHeader$w_pas_pa_view.srw
$PBExportComments$Add pallet
forward
global type w_pas_pa_view from w_base_sheet_ext
end type
type dw_epa_pt from u_base_dw_ext within w_pas_pa_view
end type
type st_1 from statictext within w_pas_pa_view
end type
type dw_totals from u_base_dw_ext within w_pas_pa_view
end type
type cb_status_5 from u_base_commandbutton_ext within w_pas_pa_view
end type
type cb_status_4 from u_base_commandbutton_ext within w_pas_pa_view
end type
type cb_status_3 from u_base_commandbutton_ext within w_pas_pa_view
end type
type cb_status_2 from u_base_commandbutton_ext within w_pas_pa_view
end type
type cb_status_1 from u_base_commandbutton_ext within w_pas_pa_view
end type
type dw_header from u_base_dw_ext within w_pas_pa_view
end type
type dw_pa_view from u_base_dw_ext within w_pas_pa_view
end type
type uo_1 from u_spin within w_pas_pa_view
end type
type st_previous from statictext within w_pas_pa_view
end type
type st_next from statictext within w_pas_pa_view
end type
end forward

global type w_pas_pa_view from w_base_sheet_ext
integer width = 5157
integer height = 2636
string title = "PA View"
long backcolor = 67108864
event ue_set_detail_handle ( window aw_order_detail )
dw_epa_pt dw_epa_pt
st_1 st_1
dw_totals dw_totals
cb_status_5 cb_status_5
cb_status_4 cb_status_4
cb_status_3 cb_status_3
cb_status_2 cb_status_2
cb_status_1 cb_status_1
dw_header dw_header
dw_pa_view dw_pa_view
uo_1 uo_1
st_previous st_previous
st_next st_next
end type
global w_pas_pa_view w_pas_pa_view

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
		ib_toolbarVisible, &
		ib_ReInquire, &
		ib_Dont_Increment_timeout

Double		id_p24b_task_number, &
		id_p24b_last_record, &
		id_p24b_max_record

DWObject	idwo_last_clicked

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long		il_SelectedColor, &
		il_SelectedTextColor,&
		il_lastclicked_background

s_error		istr_error_info

u_pas202		iu_pas202
u_ws_pas1		iu_ws_pas1

// This is for the async call
u_pas203		iu_pas203

String		is_qty_by_age_code, &
		is_shift_production, &
		is_sold_qty_by_type, &
		is_sold_qty_by_age, &
		is_carry_over,&
		is_input
		
// pjm 09/10/2014 added for extended tooltip
w_tooltip_expanded iw_tooltip
application 	ia_apptool

String is_description, &
       is_colname, &
		 is_col, &
		 is_save_col, &
		 is_lastcolumn

       
Long	 il_xpos, &
       il_ypos, &
		 il_row
		 
String		is_debug, &
		is_status1, &
		is_status2, &
		is_status3, &
		is_status4, &
		is_status5, &
		is_product_state_desc, &
		is_product_status_desc

w_base_sheet	iw_order_detail
end variables

forward prototypes
public subroutine wf_set_dates ()
public subroutine wf_open_inventory ()
public subroutine wf_open_available ()
public subroutine wf_open_shiftwise_prod ()
public subroutine wf_open_qty_by_type ()
public subroutine wf_open_sold_by_age ()
public subroutine wf_open_where_produced (string as_direction)
public subroutine wf_open_order_details ()
public function string wf_get_co_fut_sales ()
public subroutine wf_open_carry_over ()
public function boolean wf_carry_over_ready ()
public function boolean wf_next ()
public function boolean wf_previous ()
public function date wf_get_selected_date ()
public subroutine wf_open_prorate ()
public subroutine wf_show_statuses (string as_status_info)
public function string wf_getheaderinfo ()
public subroutine wf_open_manual_inventory ()
public subroutine wf_tooltip (string as_column, readonly long row, character ac_indicator)
public subroutine wf_get_available (string as_column_name, integer ai_row, integer ai_col, character ac_indicator)
public function boolean wf_retrieve ()
public subroutine wf_show_hide_second_week ()
end prototypes

event ue_set_detail_handle;iw_order_detail = aw_order_detail

end event

public subroutine wf_set_dates ();If dw_pa_view.RowCount() > 0 And dw_header.RowCount() > 0 Then
	dw_pa_view.SetItem(1, "begin_date", dw_header.GetItemDate(1, "begin_date"))
End if
end subroutine

public subroutine wf_open_inventory ();String	ls_inventory
Window	lw_temp
date		ld_filter_date

//dld added
ld_filter_date = wf_get_selected_date()
ls_inventory = string(ld_filter_date) + '~t'

ls_inventory += dw_header.GetItemString(1, 'plant_code') + '~tI~t' + is_qty_by_age_code
OpenWithParm(lw_temp, ls_inventory , "w_pas_age_code")

return
end subroutine

public subroutine wf_open_available ();String	ls_inventory
Window	lw_temp
date		ld_filter_date
//dld added
ld_filter_date = wf_get_selected_date()
ls_inventory = string(ld_filter_date) + '~t'

ls_inventory += dw_header.GetItemString(1, 'plant_code') + '~tA~t' + is_qty_by_age_code
OpenWithParm(lw_temp, ls_inventory , "w_pas_age_code")

return
end subroutine

public subroutine wf_open_shiftwise_prod ();String	ls_prod
Window	lw_temp

ls_prod = String(wf_get_Selected_Date(), "yyyy-mm-dd")

ls_prod = String(wf_get_Selected_Date(), "yyyy-mm-dd") + '~t' + &
				is_shift_production
OpenWithParm(lw_temp, ls_prod , "w_pas_shift_production")

return
end subroutine

public subroutine wf_open_qty_by_type ();String	ls_prod
Window	lw_temp


ls_prod = String(wf_get_Selected_Date(), "yyyy-mm-dd") + '~t' + &
				is_sold_qty_by_type
OpenWithParm(lw_temp, ls_prod , "w_pas_qty_by_type")

return
end subroutine

public subroutine wf_open_sold_by_age ();String	ls_prod
Window	lw_temp


ls_prod = dw_header.GetItemString(1, "plant_code") + '~t' + &
				String(wf_get_Selected_Date(), "yyyy-mm-dd") + '~t' + &
				is_sold_qty_by_age
OpenWithParm(lw_temp, ls_prod , "w_pas_sold_by_age")

return
end subroutine

public subroutine wf_open_where_produced (string as_direction);Int		li_tab, &
			li_end_tab
String	ls_input
Window	lw_temp


ls_input = wf_getheaderinfo()
li_tab = iw_frame.iu_string.nf_npos(ls_input, '~t', 1, 4)
li_end_tab = Pos(ls_input, '~t', li_tab + 1)
ls_input = Replace(ls_input, li_tab + 1, li_end_tab - li_tab - 1, &
				String(wf_get_selected_date(), "yyyy-mm-dd"))
ls_input = as_direction + '~t' +  ls_input

//OpenWithParm(lw_temp, ls_input , "w_pas_where_produced")
OpenSheetWithParm (lw_temp,ls_input,"w_pas_where_produced",iw_frame,0)
return
end subroutine

public subroutine wf_open_order_details ();Int		li_tab, &
			li_end_tab
String	ls_input
Window	lw_temp


//ls_input = wf_getheaderinfo()
//li_tab = iw_frame.iu_string.nf_npos(ls_input, '~t', 1, 4)
//li_end_tab = Pos(ls_input, '~t', li_tab + 1)
//ls_input = Replace(ls_input, li_tab + 1, li_end_tab - li_tab - 1, &
//				String(wf_get_selected_date(), "yyyy-mm-dd"))
//
//


IF IsValid(iw_order_detail) THEN 
	iw_order_detail.TriggerEvent ('ue_inquire') 
	iw_order_detail.setFocus()
else
	OpenSheetWithParm (lw_temp,This,"w_pas_order_detail",iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)
end If
//return
end subroutine

public function string wf_get_co_fut_sales ();Char	lc_indicator
String	ls_data


Choose Case Left(idwo_Last_Clicked.Name, 5)
Case "carry"
	lc_indicator = 'C'
Case "futur"
	lc_indicator = 'F'
Case Else
	return ""
End Choose

ls_data = lc_indicator + '~t' + is_carry_over

return ls_data
end function

public subroutine wf_open_carry_over ();Window	lw_temp


OpenWithParm(lw_temp, This , "w_pas_carry_over")

return
end subroutine

public function boolean wf_carry_over_ready ();return Not ib_async_running
end function

public function boolean wf_next ();If iw_frame.iu_string.nf_IsEmpty(dw_header.GetItemString(1, 'fab_product_code')) Then
	iw_frame.SetMicroHelp("Please inquire before hitting Next or Previous")
	Return False
End if
If dw_header.GetItemString(1, "one_week") = "Y" Then 
	iw_frame.SetMicroHelp("Inquiring on Next Week")
	dw_header.SetItem(1, "begin_date", RelativeDate(dw_header.GetItemDate(1, "begin_date"), 7))
Else
	iw_frame.SetMicroHelp("Inquiring on Next 2 Weeks")
	dw_header.SetItem(1, "begin_date", RelativeDate(dw_header.GetItemDate(1, "begin_date"), 14))	
End If

ib_ReInquire = True
This.PostEvent('ue_query')

return True
end function

public function boolean wf_previous ();Date	ldt_begin


If iw_frame.iu_string.nf_IsEmpty(dw_header.GetItemString(1, 'fab_product_code')) Then
	iw_frame.SetMicroHelp("Please inquire before choosing Next or Previous")
	return False
End if

ldt_begin = dw_header.GetItemDate(1, "begin_date")

If ldt_begin <= Today() Then
	iw_frame.SetMicroHelp("Begin Date cannot be earlier than Today")
	return False
End if

If dw_header.GetItemString(1, "one_week") = "Y" Then
	If DaysAfter(Today(), ldt_begin) < 7 Then
		If	MessageBox("Begin Date", "Begin Date cannot be earlier than Today.  " + &
						"Would you like to continue using today's date as begin date?", Question!, &
						YesNo!) = 2 Then 
			return False
		End if
		ldt_begin = Today()
	Else
		ldt_begin = RelativeDate(ldt_begin, -7)
	End if
Else
	If DaysAfter(Today(), ldt_begin) < 14 Then
		If	MessageBox("Begin Date", "Begin Date cannot be earlier than Today.  " + &
						"Would you like to continue using today's date as begin date?", Question!, &
						YesNo!) = 2 Then 
			return False
		End if
		ldt_begin = Today()
	Else
		ldt_begin = RelativeDate(ldt_begin, -14)
	End if	
End If

If dw_header.GetItemString(1, "one_week") = "Y" Then
	iw_frame.SetMicroHelp("Inquiring on previous week")
Else
	iw_frame.SetMicroHelp("Inquiring on previous 2 weeks")
End If

dw_header.SetItem(1, "begin_date", ldt_begin)

ib_ReInquire = True
This.PostEvent('ue_query')

Return True
end function

public function date wf_get_selected_date ();//ibdkdld Ext PA changes recoded the whole Function 09/05/2002
string	ls_last_2_chars, ls_col_name
date	ls_date
ls_last_2_chars = Right(idwo_Last_Clicked.Name,2)


If Left(ls_last_2_chars,1) = '_' Then
	ls_col_name = "dates_" + string(Integer(Right(idwo_Last_Clicked.Name, 1)))
	return relativedate(dw_pa_view.getitemdate(1, & 
		"dates_" + string(Integer(Right(idwo_Last_Clicked.Name, 1)))), 0)
Else
	ls_col_name = "dates_" + string(Integer(Right(idwo_Last_Clicked.Name, 2)))
	return relativedate(dw_pa_view.getitemdate(1, & 
		"dates_" + string(Integer(Right(idwo_Last_Clicked.Name, 2)))), 0)
End If



end function

public subroutine wf_open_prorate ();String	ls_temp
Window	lw_temp

ls_temp =  dw_header.GetItemString(1,"plant_code") + "~t"
ls_temp += dw_header.GetItemString(1,"fab_product_code") + "~t"
ls_temp += dw_header.GetItemString(1,"fab_product_descr") + "~t"
ls_temp += String(wf_get_selected_date(), "yyyy-mm-dd") + "~t"
// product state ibdkdld 10/02/02 
ls_temp += string(dw_header.getitemnumber(1, "product_state")) + "~t"
ls_temp += dw_header.GetItemString(1, "product_status")


OpenSheetWithParm(lw_temp, ls_temp, "w_prorate",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)

return
end subroutine

public subroutine wf_show_statuses (string as_status_info);
u_string_functions u_string
long	ll_num 	
string	ls_status_code, &
			ls_status_name

FOR ll_num = 1 TO 5
	ls_status_code = u_string.nf_gettoken(as_status_info,'~t')
	ls_status_name = u_string.nf_gettoken(as_status_info,'~t')

	CHOOSE CASE ll_num
		CASE 1  
			If ls_status_code > ' ' Then
				cb_status_1.Visible = True
				cb_status_1.Text = ls_status_name
				is_status1 = ls_status_code
			Else
				cb_status_1.Visible = False
				cb_status_1.Text = ''
				is_status1 = ''
			End If
		CASE 2  
			If ls_status_code > ' ' Then
				cb_status_2.Visible = True
				cb_status_2.Text = ls_status_name
				is_status2 = ls_status_code
			Else
				cb_status_2.Visible = False
				cb_status_2.Text = ''
				is_status2 = ''
			End If
		CASE 3  
			If ls_status_code > ' ' Then
				cb_status_3.Visible = True
				cb_status_3.Text = ls_status_name
				is_status3 = ls_status_code
			Else
				cb_status_3.Visible = False
				cb_status_3.Text = ''
				is_status3 = ''
			End If
		CASE 4  
			If ls_status_code > ' ' Then
				cb_status_4.Visible = True
				cb_status_4.Text = ls_status_name
				is_status4 = ls_status_code
			Else
				cb_status_4.Visible = False
				cb_status_4.Text = ''
				is_status4 = ''
			End If	
		CASE 5
			If ls_status_code > ' ' Then
				cb_status_5.Visible = True
				cb_status_5.Text = ls_status_name
				is_status5 = ls_status_code
			Else
				cb_status_5.Visible = False
				cb_status_5.Text = ''
				is_status5 = ''
			End If			
			
	END CHOOSE
NEXT
end subroutine

public function string wf_getheaderinfo ();//return dw_header.Describe("DataWindow.Data")

String		ls_string


ls_string = dw_header.GetItemString(1, 'plant_code') + '~t' + &
				dw_header.GetItemString(1, 'plant_description') + '~t' + &
				dw_header.GetItemString(1, 'fab_product_code') + '~t' + &
				dw_header.GetItemString(1, 'fab_product_descr') + '~t' + &
				String(dw_header.GetItemDate(1, 'begin_date')) + '~t' + &
				String(dw_header.GetItemnumber(1, 'product_state')) + '~t' + &
				dw_header.GetItemString(1, 'product_status') + '~t'

return ls_string
end function

public subroutine wf_open_manual_inventory ();String	ls_temp
Window	lw_temp

ls_temp =  dw_header.GetItemString(1,"plant_code") + "~t"
ls_temp += dw_header.GetItemString(1,"plant_description") + "~t"
ls_temp += dw_header.GetItemString(1,"fab_product_code") + "~t"
ls_temp += dw_header.GetItemString(1,"fab_product_descr") + "~t"
ls_temp += String(dw_header.GetItemDate(1,"begin_date")) + "~t"
ls_temp += "Z" + "~t" + "N" + "~t" + "N" + "~t"  
ls_temp += string(dw_header.getitemnumber(1, "product_state")) + "~t"

OpenSheetWithParm(lw_temp, ls_temp, "w_manual_inventory",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)

return
end subroutine

public subroutine wf_tooltip (string as_column, readonly long row, character ac_indicator);// pjm 08/08/2014 added for available qtys
String	 ls_temp="",ls_column, ls_last_two_chars, ls_col
int   li_ret
Long	ll_ids_rowcount, &
			ll_count, &
			ll_row=0
			
	If IsValid(iw_tooltip) and il_Row = Row and as_column = is_save_col Then Return  
	
	If dw_pa_view.GetItemNumber(row,as_column) = 0 Then Return 
	
	IF IsValid(iw_tooltip) Then
		close(iw_tooltip)
	END IF 

	ls_col = right(as_column,1)
	ls_last_two_chars = right(as_column,2)
	if left(ls_last_two_chars,1) = '_' Then
		ls_column = "dates_" + ls_col
		wf_get_available(ls_column,row,integer(ls_col),ac_indicator) // sets is_description with available qtys
	else
		ls_column = "dates_" + ls_last_two_chars
		wf_get_available(ls_column,row,integer(ls_last_two_chars),ac_indicator) // sets is_description with available qtys		
	end if
	
	if len(is_description) > 0 then 
		openwithparm(iw_tooltip, this, iw_frame)		
	end if
		
	//il_row = Row

	is_col = as_column
end subroutine

public subroutine wf_get_available (string as_column_name, integer ai_row, integer ai_col, character ac_indicator);datastore lds_avail
integer li_ret, li_row
date ld_filter_date
string ls_tooltip_text


lds_avail = create datastore
lds_avail.dataobject="d_pa_view_age_code"
li_ret = lds_avail.ImportString(is_qty_by_age_code)
ld_filter_date = dw_pa_view.GetItemDate(1, as_column_name)
li_ret = lds_avail.SetSort("sort_date D")
li_ret = lds_avail.Sort()
If ac_indicator = 'A' Then
	li_ret = lds_avail.SetFilter("filter_date = " + string(ld_filter_date,"yyyy-mm-dd") )
Else
	li_ret = lds_avail.SetFilter("filter_date = " + string(ld_filter_date,"yyyy-mm-dd") ) 
End If	
li_ret = lds_avail.Filter()

For li_row = 1 to lds_avail.RowCount()
	 if ac_indicator = 'A' And lds_avail.GetItemNumber(li_row,"available_qty") = 0 Then Continue
	 if ac_indicator = 'I' And lds_avail.GetItemNumber(li_row,"inventory_qty") = 0 Then Continue
		
	 ls_tooltip_text = ls_tooltip_text + string(ai_row) + "~t" + string(ai_col) + "~t"
	 ls_tooltip_text = ls_tooltip_text + lds_avail.object.age_code[li_row] + space(5)
	 If ac_indicator = 'A' Then
	 	ls_tooltip_text = ls_tooltip_text + String(lds_avail.object.available_qty[li_row]) + "~r~n"
	 Else
		
		ls_tooltip_text = ls_tooltip_text + String(lds_avail.object.inventory_qty[li_row]) + "~r~n"
	 End If
Next
If Len(Trim(ls_tooltip_text)) > 0 Then 
	ls_tooltip_text = string(ai_row) + "~t" + string(ai_col) + "~t" &
	 	+ "Production Date" + Space(7) + "Quantity~r~n" + ls_tooltip_text
End If

Destroy lds_avail

is_description = ls_tooltip_text
end subroutine

public function boolean wf_retrieve ();Boolean	lb_ret

String	ls_input, ls_modify, &
			ls_output_values,ls_inventory,ls_name,ls_color,ls_text_color,ls_problem,ls_temp, &
			ls_status_info, ls_weekly_totals, ls_epa_pt_info,ll_epapt_ind,ls_qname, ls_number_of_weeks
			
Long		ll_pos,ll_invt,ll_temp,ll_num, epapt_total_amount
Date 		ldt_date

Integer	li_rtn

u_string_functions u_string

If IsValid(iw_tooltip) Then
	close(iw_tooltip)
End If

If Not ib_ReInquire Then
	OpenWithParm(w_pas_pa_view_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
//	This.dw_header.Reset()
//	This.dw_header.ImportString(ls_input)
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")
is_debug = This.dw_header.GetItemString(1, 'fab_product_code')

// display dates before rpc call
// remove for weekend date changes ibdkdld 07/02
//wf_set_dates()

ls_input = This.dw_header.GetItemString(1, "plant_code")
ls_input = This.dw_header.GetItemString(1, "fab_product_code")

ls_input = String(This.dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd")
ls_input = This.dw_header.GetItemString(1, "ranges") 
ls_input = string(This.dw_header.GetItemnumber(1, "product_state"))
ls_input = This.dw_header.GetItemString(1, "product_status")
ls_input = This.dw_header.GetItemString(1, "pt_view2")
ls_input = This.dw_header.GetItemString(1, "epa_view")

If dw_header.GetItemString(1, "one_week") = 'Y' Then
	ls_number_of_weeks = '1'
Else
	ls_number_of_weeks = '2'
End If

// product state changes ibdkdld 10/02/02
ls_input = This.dw_header.GetItemString(1, "plant_code") + '~t' + &
				This.dw_header.GetItemString(1, "fab_product_code") + '~t' + &
				String(This.dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd") + '~t' + &
				This.dw_header.GetItemString(1, "ranges") + '~t' + &
				string(This.dw_header.GetItemnumber(1, "product_state")) + '~t' + &
				This.dw_header.GetItemSTring(1, "product_status") + '~t' + &
				This.dw_header.GetItemString(1, "pt_view2") + '~t' + &
				This.dw_header.GetItemString(1, "epa_view") + '~t' + &
				ls_number_of_weeks + '~r~n' 

//Do While ib_async_running
//	// Re-inquired before last one back, wait until it is back
//	ib_Dont_Increment_timeout = True
//	This.TriggerEvent("Timer")
//	Yield()
//Loop

//ib_Dont_Increment_timeout = False
//dld
//dw_pa_view.DataObject = ''
//dw_pa_view.DataObject = 'd_pa_view'
ls_color = '67108864'
ls_text_color = '0'
FOR ll_pos = 1 TO 14
	ls_name = 'avail_amount_' + string(ll_pos)
	This.dw_pa_view.modify( ls_name + ".Background.Color = " + ls_color)
	This.dw_pa_view.modify( ls_name + ".Color = " + ls_text_color)
	This.dw_pa_view.modify( ls_name + ".Tag = ' '")
NEXT

is_input = ls_input

//If Not iu_pas202.nf_inq_pa_view(istr_error_info, &
//									is_input, &
//									ls_output_values, &
//									is_qty_by_age_code, &
//									is_shift_production, &
//									is_sold_qty_by_age, &
//									is_sold_qty_by_type, &
//									id_p24b_task_number, &
//									id_p24b_last_record, &
//									id_p24b_max_record, &
//									ls_status_info, &
//									ls_weekly_totals, &
//									ls_epa_pt_info) Then return false

If Not iu_ws_pas1.nf_pasp17fr(istr_error_info, &
									is_input, &
									ls_output_values, &
									is_qty_by_age_code, &
									is_shift_production, &
									is_sold_qty_by_age, &
									is_sold_qty_by_type, &
									ls_status_info, &
									ls_weekly_totals, &
									ls_epa_pt_info,ls_qname) Then return false

This.dw_pa_view.Reset()
This.dw_pa_view.SetRedraw(False)

li_rtn = This.dw_pa_view.ImportString(ls_output_values)

ls_input = is_input 

FOR ll_num = 1 TO 15
	ls_temp = u_string.nf_gettoken(ls_input,'~t')
	CHOOSE CASE ll_num
		CASE 1  
			dw_header.SetItem(1, "boxes_per_pallet", long(ls_temp))
		CASE 2  
			dw_pa_view.setitem(1, "dates_1",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_1", ls_temp)
		CASE 3  
			dw_pa_view.setitem(1, "dates_2",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_2", ls_temp)
		CASE 4  
			dw_pa_view.setitem(1, "dates_3",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_3", ls_temp)
		CASE 5  
			dw_pa_view.setitem(1, "dates_4",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_4", ls_temp)
		CASE 6  
			dw_pa_view.setitem(1, "dates_5",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_5", ls_temp)
		CASE 7  
			dw_pa_view.setitem(1, "dates_6",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_6", ls_temp)
		CASE 8  
			dw_pa_view.setitem(1, "dates_7",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_7", ls_temp)
		CASE 9  
			dw_pa_view.setitem(1, "dates_8",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_8", ls_temp)
		CASE 10  
			dw_pa_view.setitem(1, "dates_9",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_9", ls_temp)
		CASE 11  
			dw_pa_view.setitem(1, "dates_10",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_10", ls_temp)
		CASE 12  
			dw_pa_view.setitem(1, "dates_11",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_11", ls_temp)
		CASE 13  
			dw_pa_view.setitem(1, "dates_12",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_12", ls_temp)
		CASE 14  
			dw_pa_view.setitem(1, "dates_13",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_13", ls_temp)
		CASE 15  
			dw_pa_view.setitem(1, "dates_14",date(ls_temp))	
			ls_temp = u_string.nf_gettoken(ls_input,'~t')
			dw_pa_view.setitem(1, "protect_date_14", ls_temp)			
	END CHOOSE
NEXT

// pjm 09/11/2014 added for new column - average weight
dw_header.setitem(1, "avg_weight",dec(Trim(ls_input)) )
/* added new columns to be sent down for weekend date functionality 
	ibdkdld 07/02 end

*/
//dld added code to highlight the avail-qty if it contains a negative
ls_inventory = is_qty_by_age_code
ls_color = string(il_SelectedColor)
ls_text_color = string(il_SelectedTextColor)
Do While Len(ls_inventory) > 0
	ll_pos = u_string.nf_npos(ls_inventory,'~t',1,2)
	ll_invt = long(mid(ls_inventory,ll_pos + 1,pos(ls_inventory,'~t',ll_pos + 1) - ll_pos))
	If ll_invt < 0 Then
		ll_pos = u_string.nf_npos(ls_inventory,'~t',1,4)
		ldt_date = Date(mid(ls_inventory,ll_pos + 1,pos(ls_inventory,'~r~n',ll_pos + 1) - ll_pos))
		// bug ibdkdld 10/28/02 begin
//		ll_pos = DaysAfter(This.dw_header.GetItemDate(1, "begin_date"),ldt_date) + 1		
		If ldt_date = This.dw_pa_view.getitemdate(1,"dates_1") Then
			ll_pos = 1
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_2") Then
			ll_pos = 2
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_3") Then
			ll_pos = 3
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_4") Then
			ll_pos = 4
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_5") Then
			ll_pos = 5
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_6") Then
			ll_pos = 6
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_7") Then
			ll_pos = 7
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_8") Then
			ll_pos = 8
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_9") Then
			ll_pos = 9
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_10") Then
			ll_pos = 10
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_11") Then
			ll_pos = 11
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_12") Then
			ll_pos = 12
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_13") Then
			ll_pos = 13
		Elseif ldt_date = This.dw_pa_view.getitemdate(1,"dates_14") Then
			ll_pos = 14
		end If
		// bug ibdkdld 10/28/02 end
		ls_name = 'avail_amount_' + string(ll_pos)
		
		This.dw_pa_view.modify( ls_name + ".Background.Color = " + ls_color)
		This.dw_pa_view.modify(ls_name + ".Background.Mode = '0'")
		This.dw_pa_view.modify( ls_name + ".Color = " + ls_text_color)
		This.dw_pa_view.modify( ls_name + ".Tag = 'Selected'")
	End If
	ls_name = u_string.nf_gettoken(ls_inventory,'~r~n')
LOOP 

dw_pa_view.SetItem(1, 'display_cut', dw_header.GetItemString(1, 'ship_off_the_cut'))
wf_show_hide_second_week();
This.dw_pa_view.SetRedraw(True)

wf_show_statuses (ls_status_info)
dw_totals.Reset()
dw_totals.ImportString(ls_weekly_totals)

// ibdkkad 4/28/14 added epa pt views

// pjm 09/09/2014 changes for epa pt
//if epa checked
If this.dw_epa_pt.Visible = TRUE Then
	If This.dw_header.GetItemString(1,"epa_view") = "E" Then
				
	  dw_epa_pt.object.t_option.text = 'Weekly EPA'
	  ls_input = String(dw_totals.GetItemDate(1, "weekly_total_date_1"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date1.text = String(dw_totals.GetItemDate(1, "weekly_total_date_1"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date2.text = String(dw_totals.GetItemDate(1, "weekly_total_date_2"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date3.text = String(dw_totals.GetItemDate(1, "weekly_total_date_3"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date4.text = String(dw_totals.GetItemDate(1, "weekly_total_date_4"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date5.text = String(dw_totals.GetItemDate(1, "weekly_total_date_5"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date6.text = String(dw_totals.GetItemDate(1, "weekly_total_date_6"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date7.text = String(dw_totals.GetItemDate(1, "weekly_total_date_7"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date8.text = String(dw_totals.GetItemDate(1, "weekly_total_date_8"), "mm/dd/yyyy")	  
	  dw_epa_pt.object.t_date9.text = String(dw_totals.GetItemDate(1, "weekly_total_date_9"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date10.text = String(dw_totals.GetItemDate(1, "weekly_total_date_10"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date11.text = String(dw_totals.GetItemDate(1, "weekly_total_date_11"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date12.text = String(dw_totals.GetItemDate(1, "weekly_total_date_12"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date13.text = String(dw_totals.GetItemDate(1, "weekly_total_date_13"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date14.text = String(dw_totals.GetItemDate(1, "weekly_total_date_14"), "mm/dd/yyyy")	  
	// if pt checked
	elseif This.dw_header.GetItemString(1,"pt_view2") = "P" Then
	  dw_epa_pt.object.t_option.text = 'Daily PT'
	  dw_epa_pt.object.t_date1.text = String(dw_pa_view.GetItemDate(1, "dates_1"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date2.text = String(dw_pa_view.GetItemDate(1, "dates_2"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date3.text = String(dw_pa_view.GetItemDate(1, "dates_3"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date4.text = String(dw_pa_view.GetItemDate(1, "dates_4"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date5.text = String(dw_pa_view.GetItemDate(1, "dates_5"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date6.text = String(dw_pa_view.GetItemDate(1, "dates_6"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date7.text = String(dw_pa_view.GetItemDate(1, "dates_7"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date8.text = String(dw_pa_view.GetItemDate(1, "dates_8"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date9.text = String(dw_pa_view.GetItemDate(1, "dates_9"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date10.text = String(dw_pa_view.GetItemDate(1, "dates_10"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date11.text = String(dw_pa_view.GetItemDate(1, "dates_11"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date12.text = String(dw_pa_view.GetItemDate(1, "dates_12"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date13.text = String(dw_pa_view.GetItemDate(1, "dates_13"), "mm/dd/yyyy")
	  dw_epa_pt.object.t_date14.text = String(dw_pa_view.GetItemDate(1, "dates_14"), "mm/dd/yyyy")	  
	End If

	dw_epa_pt.Reset()
	dw_epa_pt.ImportString(ls_epa_pt_info)
End If


// ibdkdld 11/27/02 add error logic
//RevGLL//	Begin - Added check for call to PASP24BR.



If len(ls_output_values) > 0 Then
//	If Not iu_pas203.nf_inq_carry_over(istr_error_info, &
//									ls_input, &
//									id_p24b_task_number, &
//									id_p24b_last_record, &
//									id_p24b_max_record, &
//									is_carry_over, &
//									ii_async_commhandle) Then 
		If Not iu_ws_pas1.nf_pasp24fr(istr_error_info, &
									is_input,&
									is_carry_over,ls_qname) Then 
		SetMicroHelp("Ready")
		Return False
	End If	
End If



//is_carry_over = ""
//This.Pointer = 'async.cur'
//This.dw_header.Modify("DataWindow.Pointer = 'async.cur'")
//This.dw_pa_view.Modify("DataWindow.Pointer = 'async.cur'")
//SetPointer(Arrow!)

// If this timer value changes, change ii_async_timeout_count max in timer event
//Timer(1, This)
//ib_async_running = True
//This.TriggerEvent("Timer")

SetMicroHelp("Inquire Successful")

return true

end function

public subroutine wf_show_hide_second_week ();Boolean		lb_visible 

If dw_header.GetItemString(1, "one_week") = "Y" Then
	lb_visible = False
Else
	lb_visible = True
End If		
	
dw_pa_view.Object.dates_8.Visible = lb_visible
dw_pa_view.Object.dates_9.Visible = lb_visible
dw_pa_view.Object.dates_10.Visible = lb_visible
dw_pa_view.Object.dates_11.Visible = lb_visible
dw_pa_view.Object.dates_12.Visible = lb_visible
dw_pa_view.Object.dates_13.Visible = lb_visible
dw_pa_view.Object.dates_14.Visible = lb_visible	

dw_pa_view.Object.compute_15.Visible = lb_visible	
dw_pa_view.Object.compute_17.Visible = lb_visible
dw_pa_view.Object.compute_19.Visible = lb_visible
dw_pa_view.Object.compute_21.Visible = lb_visible
dw_pa_view.Object.compute_23.Visible = lb_visible
dw_pa_view.Object.compute_25.Visible = lb_visible
dw_pa_view.Object.compute_27.Visible = lb_visible

dw_pa_view.Object.carry_over_8.Visible = lb_visible
dw_pa_view.Object.carry_over_9.Visible = lb_visible
dw_pa_view.Object.carry_over_10.Visible = lb_visible
dw_pa_view.Object.carry_over_11.Visible = lb_visible
dw_pa_view.Object.carry_over_12.Visible = lb_visible
dw_pa_view.Object.carry_over_13.Visible = lb_visible
dw_pa_view.Object.carry_over_14.Visible = lb_visible	

dw_pa_view.Object.production_8.Visible = lb_visible
dw_pa_view.Object.production_9.Visible = lb_visible
dw_pa_view.Object.production_10.Visible = lb_visible
dw_pa_view.Object.production_11.Visible = lb_visible
dw_pa_view.Object.production_12.Visible = lb_visible
dw_pa_view.Object.production_13.Visible = lb_visible
dw_pa_view.Object.production_14.Visible = lb_visible

dw_pa_view.Object.further_pr_8.Visible = lb_visible
dw_pa_view.Object.further_pr_9.Visible = lb_visible
dw_pa_view.Object.further_pr_10.Visible = lb_visible
dw_pa_view.Object.further_pr_11.Visible = lb_visible
dw_pa_view.Object.further_pr_12.Visible = lb_visible
dw_pa_view.Object.further_pr_13.Visible = lb_visible
dw_pa_view.Object.further_pr_14.Visible = lb_visible

dw_pa_view.Object.prod_delay_8.Visible = lb_visible
dw_pa_view.Object.prod_delay_9.Visible = lb_visible
dw_pa_view.Object.prod_delay_10.Visible = lb_visible
dw_pa_view.Object.prod_delay_11.Visible = lb_visible
dw_pa_view.Object.prod_delay_12.Visible = lb_visible
dw_pa_view.Object.prod_delay_13.Visible = lb_visible
dw_pa_view.Object.prod_delay_14.Visible = lb_visible

dw_pa_view.Object.curr_sales_8.Visible = lb_visible
dw_pa_view.Object.curr_sales_9.Visible = lb_visible
dw_pa_view.Object.curr_sales_10.Visible = lb_visible
dw_pa_view.Object.curr_sales_11.Visible = lb_visible
dw_pa_view.Object.curr_sales_12.Visible = lb_visible
dw_pa_view.Object.curr_sales_13.Visible = lb_visible
dw_pa_view.Object.curr_sales_14.Visible = lb_visible

dw_pa_view.Object.future_sales_8.Visible = lb_visible
dw_pa_view.Object.future_sales_9.Visible = lb_visible
dw_pa_view.Object.future_sales_10.Visible = lb_visible
dw_pa_view.Object.future_sales_11.Visible = lb_visible
dw_pa_view.Object.future_sales_12.Visible = lb_visible
dw_pa_view.Object.future_sales_13.Visible = lb_visible
dw_pa_view.Object.future_sales_14.Visible = lb_visible

dw_pa_view.Object.prod_buffer_8.Visible = lb_visible
dw_pa_view.Object.prod_buffer_9.Visible = lb_visible
dw_pa_view.Object.prod_buffer_10.Visible = lb_visible
dw_pa_view.Object.prod_buffer_11.Visible = lb_visible
dw_pa_view.Object.prod_buffer_12.Visible = lb_visible
dw_pa_view.Object.prod_buffer_13.Visible = lb_visible
dw_pa_view.Object.prod_buffer_14.Visible = lb_visible

dw_pa_view.Object.avail_amount_8.Visible = lb_visible
dw_pa_view.Object.avail_amount_9.Visible = lb_visible
dw_pa_view.Object.avail_amount_10.Visible = lb_visible
dw_pa_view.Object.avail_amount_11.Visible = lb_visible
dw_pa_view.Object.avail_amount_12.Visible = lb_visible
dw_pa_view.Object.avail_amount_13.Visible = lb_visible
dw_pa_view.Object.avail_amount_14.Visible = lb_visible

dw_totals.Object.weekly_total_date_8.Visible = lb_visible
dw_totals.Object.weekly_total_date_9.Visible = lb_visible
dw_totals.Object.weekly_total_date_10.Visible = lb_visible
dw_totals.Object.weekly_total_date_11.Visible = lb_visible
dw_totals.Object.weekly_total_date_12.Visible = lb_visible
dw_totals.Object.weekly_total_date_13.Visible = lb_visible
dw_totals.Object.weekly_total_date_14.Visible = lb_visible

dw_totals.Object.weekly_total_amt_8.Visible = lb_visible
dw_totals.Object.weekly_total_amt_9.Visible = lb_visible
dw_totals.Object.weekly_total_amt_10.Visible = lb_visible
dw_totals.Object.weekly_total_amt_11.Visible = lb_visible
dw_totals.Object.weekly_total_amt_12.Visible = lb_visible
dw_totals.Object.weekly_total_amt_13.Visible = lb_visible
dw_totals.Object.weekly_total_amt_14.Visible = lb_visible

dw_epa_pt.Object.t_date8.Visible = lb_visible
dw_epa_pt.Object.t_date9.Visible = lb_visible
dw_epa_pt.Object.t_date10.Visible = lb_visible
dw_epa_pt.Object.t_date11.Visible = lb_visible
dw_epa_pt.Object.t_date12.Visible = lb_visible
dw_epa_pt.Object.t_date13.Visible = lb_visible
dw_epa_pt.Object.t_date14.Visible = lb_visible

dw_epa_pt.Object.week_total_8.Visible = lb_visible
dw_epa_pt.Object.week_total_9.Visible = lb_visible
dw_epa_pt.Object.week_total_10.Visible = lb_visible
dw_epa_pt.Object.week_total_11.Visible = lb_visible
dw_epa_pt.Object.week_total_12.Visible = lb_visible
dw_epa_pt.Object.week_total_13.Visible = lb_visible
dw_epa_pt.Object.week_total_14.Visible = lb_visible

If (dw_header.GetItemString(1, "two_week") = "Y") and (dw_header.GetItemString(1, "ship_off_the_cut") = "Y") Then
	lb_visible =True
Else
	lb_visible =False
End If	

dw_pa_view.Object.ship_off_cut_8.Visible = lb_visible
dw_pa_view.Object.ship_off_cut_9.Visible = lb_visible
dw_pa_view.Object.ship_off_cut_10.Visible = lb_visible
dw_pa_view.Object.ship_off_cut_11.Visible = lb_visible
dw_pa_view.Object.ship_off_cut_12.Visible = lb_visible
dw_pa_view.Object.ship_off_cut_13.Visible = lb_visible
dw_pa_view.Object.ship_off_cut_14.Visible = lb_visible







end subroutine

event timer;//Boolean	lb_ret
//
//Int	li_rc
//
//String	ls_carry_over, &
//			ls_input_string
//
//
//If Not IsValid(iu_pas203) Then 
//	Timer(0, This)
//	return
//End if
//
//ls_carry_over = ""
//
//li_rc = 0
//
//If iu_pas203.nf_async_complete(ii_async_commhandle) Then
//	li_rc = sqlca.nf_checkrpcError( li_rc, istr_error_info, ii_async_commhandle )
//	IF li_rc = 0 THEN
//		ii_async_timeout_count = 0
//		lb_ret = iu_pas203.nf _ inq _ carry _ over(istr_error_info, &
//											ls_input_string, &
//											id_p24b_task_number, &
//											id_p24b_last_record, &
//											id_p24b_max_record, &
//											ls_carry_over, &
//											ii_async_commhandle)	
//		If Not lb_ret Then id_p24b_last_record = id_p24b_max_record
//		ib_async_running = False
//		is_carry_over += ls_carry_over
//
//		If id_p24b_last_record <> id_p24b_max_record Then
//			ls_input_string = dw_header.GetItemString(1, "plant_code") + '~t' + &
//								dw_header.GetItemString(1, "fab_product_code") + '~t' + &
//								String(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd") + '~r~n'
//			// ibdkdld 11/27/02 add error logic
//			if not iu_pas203.nf _ inq _ carry _ over(istr_error_info, &
//												ls_input_string, &
//												id_p24b_task_number, &
//												id_p24b_last_record, &
//												id_p24b_max_record, &
//												ls_carry_over, &
//												ii_async_commhandle)	Then
//												
//				// Error occurred
//				is_carry_over = ""
//				ib_async_running = False
//				Timer(0, This)
//				This.Pointer = ''
//				This.dw_header.Modify("DataWindow.Pointer = ''")
//				This.dw_pa_view.Modify("DataWindow.Pointer = ''")
//				ib_async_running = False
//			Else
//				ib_async_running = True
//			End if
//		Else
//			ib_async_running = False
//			Timer(0, This)
//			This.Pointer = ''
//			This.dw_header.Modify("DataWindow.Pointer = ''")
//			This.dw_pa_view.Modify("DataWindow.Pointer = ''")
//
//			ib_async_running = False
//		End if
//	Else
//		// Error occurred
//		ib_async_running = False
//		Timer(0, This)
//		This.Pointer = ''
//		This.dw_header.Modify("DataWindow.Pointer = ''")
//		This.dw_pa_view.Modify("DataWindow.Pointer = ''")
//		ib_async_running = False
//	End if
//Elseif Not ib_Dont_Increment_timeout Then
//	ii_async_timeout_count ++
//
//	// If the rpc is not done in 2 minutes, then abandon it
//	If ii_async_timeout_count = 120 Then
//		ii_async_timeout_count = 0
//		ib_async_running = False
//		Timer(0, This)
//		This.Pointer = ''
//		This.dw_header.Modify("DataWindow.Pointer = ''")
//		This.dw_pa_view.Modify("DataWindow.Pointer = ''")
//		ib_async_running = False
//		MessageBox("RPC Failed", "An problem has occurred retrieving data for Carry Over or" + &
//						" Future Sales.  The detail information will not be available without" + &
//						" reinquiring.")
//	End if
//End if
//
end event

event close;call super::close;iu_pas203.nf_release_commhandle(ii_async_commhandle)

If IsValid(iu_pas202) Then Destroy(iu_pas202)
If IsValid(iu_pas203) Then Destroy(iu_pas203)

SetProfileString( iw_frame.is_UserINI, "Pas", "DisplayCut", &
		dw_header.GetItemString(1, 'ship_off_the_cut'))

SetProfileString( iw_frame.is_UserINI, "Pas", "PAViewDisplayRanges", &
		dw_header.GetItemString(1, 'ranges'))

end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_Enable('m_save')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

iw_frame.im_menu.mf_Disable('m_next')
iw_frame.im_menu.mf_Disable('m_previous')

//If IsValid(iw_frame) Then This.ChangeMenu( m_base_menu_ext )

If IsValid(iw_tooltip) Then
	close(iw_tooltip)
End If
	

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_Disable('m_save')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

iw_frame.im_menu.mf_Enable('m_next')
iw_frame.im_menu.mf_Enable('m_previous')

end event

event open;call super::open;String				ls_text, ls_number_of_weeks


dw_header.InsertRow(0)
dw_pa_view.InsertRow(0)

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "DisplayCut","")
dw_header.SetItem(1, 'ship_off_the_cut', ls_text)
dw_pa_view.SetItem(1, 'display_cut', ls_text)

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "PAViewDisplayRanges","")
//dw_pa_view.SetItem(1, 'Ranges', ls_text)



end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_ws_pas1 = Create u_ws_pas1
iu_pas202 = Create u_pas202
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if
iu_pas203 = Create u_pas203
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

ii_async_commhandle = iu_pas203.nf_Get_Async_Commhandle()
If ii_async_commhandle < 0 Then
	MessageBox("PA View", "Problem Getting Commhandle for Carry Over/Future Sales")
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "PA View"
istr_error_info.se_user_id = sqlca.userid

GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

//To use transparency as the highlight:
//li_colcount = dw_pa_view.Object.DataWindowColumn.Count
//ls_modstring = ""
//For li_Counter = 1 to li_colcount
//	ls_modstring += "#" + String(li_Counter) + &
//			".Background.Color = '" + String(il_SelectedColor) + "' "
//Next
//dw_pa_view.Modify(ls_modstring)


dw_header.SetItem(1, "begin_date", Today())
// remove for weekend date changes ibdkdld 07/02
//wf_set_dates()
// pjm 09/09/2014 hide the epa/pt dates if none selected
If dw_header.GetItemString(1,'epa_view') = 'N' and dw_header.GetItemString(1,'pt_view2') = 'N' Then
	dw_epa_pt.visible = False
Else
	dw_epa_pt.visible = True
End If

This.PostEvent("ue_query")

end event

on key;call w_base_sheet_ext::key;Int	li_curr_col

If KeyDown(KeyTab!) Then
	li_curr_col = dw_pa_view.GetColumn()
	If li_curr_col < Integer(dw_pa_view.Describe("DataWindow.Column.Count")) Then
		dw_pa_view.SetColumn(li_curr_col + 1)
	Else
		dw_pa_view.SetColumn(1)
	End if
	dw_pa_view.SetFocus()
End if
end on

on w_pas_pa_view.create
int iCurrent
call super::create
this.dw_epa_pt=create dw_epa_pt
this.st_1=create st_1
this.dw_totals=create dw_totals
this.cb_status_5=create cb_status_5
this.cb_status_4=create cb_status_4
this.cb_status_3=create cb_status_3
this.cb_status_2=create cb_status_2
this.cb_status_1=create cb_status_1
this.dw_header=create dw_header
this.dw_pa_view=create dw_pa_view
this.uo_1=create uo_1
this.st_previous=create st_previous
this.st_next=create st_next
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_epa_pt
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_totals
this.Control[iCurrent+4]=this.cb_status_5
this.Control[iCurrent+5]=this.cb_status_4
this.Control[iCurrent+6]=this.cb_status_3
this.Control[iCurrent+7]=this.cb_status_2
this.Control[iCurrent+8]=this.cb_status_1
this.Control[iCurrent+9]=this.dw_header
this.Control[iCurrent+10]=this.dw_pa_view
this.Control[iCurrent+11]=this.uo_1
this.Control[iCurrent+12]=this.st_previous
this.Control[iCurrent+13]=this.st_next
end on

on w_pas_pa_view.destroy
call super::destroy
destroy(this.dw_epa_pt)
destroy(this.st_1)
destroy(this.dw_totals)
destroy(this.cb_status_5)
destroy(this.cb_status_4)
destroy(this.cb_status_3)
destroy(this.cb_status_2)
destroy(this.cb_status_1)
destroy(this.dw_header)
destroy(this.dw_pa_view)
destroy(this.uo_1)
destroy(this.st_previous)
destroy(this.st_next)
end on

event ue_get_data;call super::ue_get_data;//jxr 8/15/2018 added local utility var for string functions
		
u_string_functions		lu_string

Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Product'
		Message.StringParm = dw_header.GetItemString(1, 'fab_product_code')
	Case 'Product_Descr'
		Message.StringParm = dw_header.GetItemString(1, 'fab_product_descr')
	// state changes ibdkdld 10/01/02
	Case 'State'
		Message.StringParm = String(dw_header.GetItemNumber(1, 'product_state'))
	Case 'Status'
		Message.StringParm = dw_header.GetItemString(1, 'product_status')			
	Case 'State_Desc'
		Message.StringParm = is_product_state_desc
	Case 'Status_Desc'
		Message.StringParm = is_product_status_desc	
	Case 'Begin_Date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'begin_date'))
	Case 'Date'
		Message.StringParm = String(wf_get_selected_date(), 'yyyy-mm-dd')
	Case 'Ship_Off_The_Cut'
		Message.StringParm = dw_header.GetItemString(1, 'ship_off_the_cut')
	Case 'Ranges'
		Message.StringParm = dw_header.GetItemString(1, 'ranges')
	// pjm 09/09/2014 - epa and pt option correction
	Case 'epa_view'
	// jxr 08/15/2018 - get saved epa from ini file
		if lu_string.nf_IsEmpty(dw_header.GetItemString(1, 'epa_view')) then
			Message.StringParm = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), 'pa_view_inq.epa_checked', 'N')
		else
			Message.StringParm = dw_header.GetItemString(1, 'epa_view')
		end if
	Case 'pt_view2'
	// jxr 08/15/2018 - get saved epa from ini file
		if lu_string.nf_IsEmpty(dw_header.GetItemString(1, 'pt_view2')) then
			Message.StringParm = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), 'pa_view_inq.pt_checked', 'N')
		else
			Message.StringParm = dw_header.GetItemString(1, 'pt_view2')
		end if
	// pjm 09/10/2014 added for w_tooltip_expanded
	Case 'number_of_weeks'
		Message.StringParm = ProfileString(gw_netwise_frame.is_UserIni, Message.nf_Get_App_ID(), 'pa_view_inq.number_of_weeks', '1')
	Case 'ToolTip'
		Message.StringParm = is_description
	CASE 'CurrRow'
		Message.StringParm = String(il_row)
		//iw_frame.SetMicroHelp("Row: " + message.stringparm)
	CASE 'CurrCol'
		Message.StringParm = is_col
	case "ColName"
		message.Stringparm = is_colname 

End Choose

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant_Desc' 
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Product'
		dw_header.SetItem(1, 'fab_product_code', as_value)
	Case 'Product_Desc'
		dw_header.SetItem(1, 'fab_product_descr', as_value)
		// state changes ibdkdld 10/01/02
	Case 'State'
		dw_header.SetItem(1, 'product_state', integer(as_value))
	Case 'Status'
		dw_header.SetItem(1, 'product_status', as_value)	
	Case 'State_Desc'
		is_product_state_desc = as_value
	Case 'Status_Desc'
		is_product_status_desc = as_value			
	Case 'Begin_Date'
		dw_header.SetItem(1, 'begin_date', Date(as_value))
	Case 'Ship_Off_The_Cut'
		dw_header.SetItem(1, 'ship_off_the_cut', as_value)
	Case 'Ranges'
		dw_header.SetItem(1, 'ranges', as_value)
	Case 'epa_view'
		dw_header.SetItem(1, 'epa_view', as_value)
	Case 'pt_view2'
		dw_header.SetItem(1, 'pt_view2', as_value)
	Case 'number_of_weeks'
		If as_value = '1' then
			dw_header.SetItem(1, 'one_week', 'Y')
			dw_header.SetItem(1, 'two_week', 'N')
		Else
			dw_header.SetItem(1, 'one_week', 'N')
			dw_header.SetItem(1, 'two_week', 'Y')			
		End if
End Choose

end event

type dw_epa_pt from u_base_dw_ext within w_pas_pa_view
integer x = 110
integer y = 1888
integer width = 4983
integer height = 604
integer taborder = 100
string dataobject = "d_pa_view_weekly_epa"
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;call super::doubleclicked;// pjm 09/11/2014 Use plant and date of cell to reinquire on PA View.
string ls_ColName, ls_ColNo, ls_text_name , ls_plant, ls_date, ls_describe, ls_last_2_chars
Date ldt_begin_date

IF String(dwo.Type) = "column" THEN
	ls_ColName = String(dwo.Name)
	ls_last_2_chars = Right(dwo.Name,2)	
	If Left(ls_last_2_chars,1) = '_' Then
		ls_ColNo = RIght(ls_ColName,1)
	Else
		ls_ColNo = RIght(ls_ColName,2)
	End If

	If dw_header.GetItemString(1,"epa_view") = "E" Then
		ls_ColName = "weekly_total_date_" + ls_ColNo
		ldt_begin_date = dw_totals.GetItemDate(1, ls_ColName)
		ldt_begin_date = RelativeDate(ldt_begin_date,  -6)  // prior week
		If ldt_begin_date < Today() Then
			ldt_begin_date = Today()
		End If
	elseif dw_header.GetItemString(1,"pt_view2") = "P" Then		
		ls_ColName = "dates_" + ls_ColNo
    	ldt_begin_date = dw_pa_view.GetItemDate(1, ls_ColName)
	else
		Return
	End If

	dw_header.SetItem(1, "begin_date", ldt_begin_date )
	dw_header.SetItem(1, "plant_code", This.GetItemString(row,"plant") )
	ib_reinquire = True // don't open the inquire window
	Post Function wf_retrieve()
End If
end event

type st_1 from statictext within w_pas_pa_view
integer y = 1700
integer width = 357
integer height = 160
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Other Statuses Exist"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_totals from u_base_dw_ext within w_pas_pa_view
integer y = 1444
integer width = 5120
integer height = 256
integer taborder = 50
string dataobject = "d_pa_view_weekly_totals"
boolean border = false
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

type cb_status_5 from u_base_commandbutton_ext within w_pas_pa_view
integer x = 2121
integer y = 1700
integer width = 393
integer height = 108
integer taborder = 90
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = ""
end type

event clicked;call super::clicked;ib_ReInquire = True
dw_header.SetItem(1, "product_status", is_status5)
wf_retrieve()
end event

type cb_status_4 from u_base_commandbutton_ext within w_pas_pa_view
integer x = 1682
integer y = 1700
integer width = 393
integer height = 108
integer taborder = 80
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = ""
end type

event clicked;call super::clicked;ib_ReInquire = True
dw_header.SetItem(1, "product_status", is_status4)
wf_retrieve()
end event

type cb_status_3 from u_base_commandbutton_ext within w_pas_pa_view
integer x = 1243
integer y = 1700
integer width = 393
integer height = 108
integer taborder = 70
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = ""
end type

event clicked;call super::clicked;ib_ReInquire = True
dw_header.SetItem(1, "product_status", is_status3)
wf_retrieve()
end event

type cb_status_2 from u_base_commandbutton_ext within w_pas_pa_view
integer x = 805
integer y = 1700
integer width = 393
integer height = 108
integer taborder = 60
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = ""
end type

event clicked;call super::clicked;ib_ReInquire = True
dw_header.SetItem(1, "product_status", is_status2)
wf_retrieve()
end event

type cb_status_1 from u_base_commandbutton_ext within w_pas_pa_view
integer x = 366
integer y = 1700
integer width = 421
integer height = 108
integer taborder = 30
integer textsize = -8
integer weight = 400
string facename = "MS Sans Serif"
string text = ""
end type

event clicked;call super::clicked;ib_ReInquire = True
dw_header.SetItem(1, "product_status", is_status1)
wf_retrieve()
end event

type dw_header from u_base_dw_ext within w_pas_pa_view
integer width = 2994
integer height = 236
integer taborder = 10
string dataobject = "d_pa_view_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
This.SetItem(1, 'begin_date', Today())
end event

event itemchanged;call super::itemchanged;Choose Case dwo.name
	Case 'ship_off_the_cut'
		dw_pa_view.SetItem(1, 'display_cut', data)
End Choose
end event

type dw_pa_view from u_base_dw_ext within w_pas_pa_view
integer x = 5
integer y = 252
integer width = 5102
integer height = 1160
integer taborder = 40
string dataobject = "d_pa_view"
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return

If dwo.Name = 'pa_resolve_ind' Then return


dwo.Background.Color = il_SelectedColor
dwo.Color = il_SelectedTextColor
dwo.Background.Mode = '0'

If IsValid(idwo_Last_Clicked) Then
	If idwo_Last_Clicked.Name = dwo.Name Then return
	IF	Not idwo_Last_Clicked.tag = 'Selected' Then
		idwo_Last_Clicked.Background.Color = 67108864
		idwo_Last_Clicked.Color = 0
		idwo_Last_Clicked.Background.Mode='1'
	End If
End if

idwo_Last_Clicked = dwo

end event

event rbuttondown;call super::rbuttondown;Boolean	lb_zero

Long		ll_value

nvuo_pa_business_rules	lu_pa_rules

m_paview_popup	lm_popmenu

String	ls_temp, &
			ls_plant_code,ls_col,ls_protect, ls_last_2_chars

IF IsValid(iw_tooltip) Then
	close(iw_tooltip)
END IF 


ls_plant_code = dw_header.GetItemString(1, 'plant_code')

If dwo.Type <> "column" Then Return

Event Trigger Clicked(xpos, ypos, row, dwo)

If iw_frame.iu_string.nf_IsEmpty( dwo.Name ) Then return
If Right(dwo.Name, 2) = '_t' Then return

ls_temp = dwo.Name

If dwo.Band <> 'detail' Then return

If dwo.name = 'pa_resolve_ind' Then Return

ll_value = This.GetItemNumber ( row, ls_temp)
If IsNull(ll_value) or ll_value = 0 Then
	lb_zero = True
End if

lm_popmenu = Create m_paview_popup

Choose Case Left(dwo.Name, 5)
Case "inven"
	If lb_zero Then
		lm_popmenu.m_inven.m_inventory.Enabled = False
	End if
	lm_popmenu.m_inven.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Case "carry"
	If lb_zero Then
		lm_popmenu.m_carry.m_carryoverprod.Enabled = False		
	End if
	lm_popmenu.m_carry.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Case "produ"
	If lb_zero Then
		lm_popmenu.m_produ.m_shiftwiseproduction.Enabled = False
		lm_popmenu.m_produ.m_whereproducedfrom.Enabled = False
	// weekend date logic ibdkdld 07/02/
	// Duane wants it changed back ibdkdld 09/09
	//else
	//	ls_col = right(dwo.name,1)
	//	ls_protect = "protect_date_" + ls_col 
	//	If dw_pa_view.getitemstring(1, ls_protect) = "Y" Then
	//  		lm_popmenu.m_produ.m_whereproducedfrom.Enabled = False
	//	End If	
	End if
	lm_popmenu.m_produ.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Case "furth"
	If lb_zero Then
		lm_popmenu.m_furth.m_whereproducedto.Enabled = False
		// weekend date logic ibdkdld 07/02
	// Duane wants it changed back ibdkdld 09/09
	//else
	//	ls_col = right(dwo.name,1)
	//	ls_protect = "protect_date_" + ls_col 
	//	If dw_pa_view.getitemstring(1, ls_protect) = "Y" Then
	 // 		lm_popmenu.m_furth.m_whereproducedto.Enabled = False
	//	End If	
End if
	lm_popmenu.m_furth.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Case "curr_"
	If lu_pa_rules.uf_check_pasldtyp(ls_plant_code) Then
		lm_popmenu.m_curr_.m_soldquantitybyage.text = 'Sold By Production Date'
	End If
	If lb_zero Then
		lm_popmenu.m_curr_.m_soldquantitybyage.Enabled = False
		lm_popmenu.m_curr_.m_orderquantities.Enabled = False
		lm_popmenu.m_curr_.m_orderinformation.Enabled = False
		lm_popmenu.m_curr_.m_prorate.Enabled = False
	// weekend date logic ibdkdld 07/02
	else
		ls_last_2_chars = right(dwo.name,2)
		If left(ls_last_2_chars,1) = '_' then
			ls_col = right(dwo.name,1)
		Else
			ls_col = right(dwo.name,2)
		End If
		ls_protect = "protect_date_" + ls_col 
		If dw_pa_view.getitemstring(1, ls_protect) = "Y" Then
	  		lm_popmenu.m_curr_.m_prorate.Enabled = False
		End If	
	End if
	lm_popmenu.m_curr_.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Case "futur"
	If lb_zero Then
		lm_popmenu.m_futur.m_futuresales.Enabled = False
	End if
	lm_popmenu.m_futur.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Case "avail"
	If lu_pa_rules.uf_check_pasldtyp(ls_plant_code) Then
		lm_popmenu.m_avail.m_availablebyage.text = 'Available By Production Date'
	End If
	// can only look at age code info if day 1 is today and column is day1
//	If lb_zero or wf_get_selected_date() <> Today() Then
//	If wf_get_selected_date() <> Today() Then
//		lm_popmenu.m_avail.m_availablebyage.Enabled = False
//	End if
	lm_popmenu.m_avail.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

End Choose

Destroy lm_popmenu


end event

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
end on

event ue_mousemove;// pjm 09/10/2014 added for extended tooltip
int li_last_pos

is_colname = String(dwo.name)

ia_apptool = GetApplication()
il_xpos = gw_netwise_frame.PointerX() + 50
il_ypos = gw_netwise_frame.PointerY() + 50

IF ia_apptool.ToolBarTips = TRUE THEN
 	If row > 0 Then
 	
		Choose case dwo.name
			case 'avail_amount_1'
					wf_tooltip('avail_amount_1',row,'A')
			case 'avail_amount_2'
					wf_tooltip('avail_amount_2',row,'A')
			case 'avail_amount_3'
					wf_tooltip('avail_amount_3',row,'A')
			case 'avail_amount_4'
					wf_tooltip('avail_amount_4',row,'A')
			case 'avail_amount_5'
					wf_tooltip('avail_amount_5',row,'A')
			case 'avail_amount_6'
					wf_tooltip('avail_amount_6',row,'A')		
			case 'avail_amount_7'
					wf_tooltip('avail_amount_7',row,'A')
			case 'avail_amount_8'
					wf_tooltip('avail_amount_8',row,'A')
			case 'avail_amount_9'
					wf_tooltip('avail_amount_9',row,'A')
			case 'avail_amount_10'
					wf_tooltip('avail_amount_10',row,'A')
			case 'avail_amount_11'
					wf_tooltip('avail_amount_11',row,'A')
			case 'avail_amount_12'
					wf_tooltip('avail_amount_12',row,'A')
			case 'avail_amount_13'
					wf_tooltip('avail_amount_13',row,'A')		
			case 'avail_amount_14'
					wf_tooltip('avail_amount_14',row,'A')					
			case 'inventory_1'
					wf_tooltip('inventory_1',row,'I')
		End Choose
	End If
End If

il_Row = Row
is_save_col = is_col
is_lastcolumn = String(dwo.name)

If IsValid(iw_tooltip) Then
	If Lower(Trim(string(dwo.name))) = 'datawindow' Then
		close(iw_tooltip)
	End If
End If
end event

type uo_1 from u_spin within w_pas_pa_view
integer x = 3017
integer y = 84
integer height = 128
integer taborder = 20
end type

on uo_1.destroy
call u_spin::destroy
end on

type st_previous from statictext within w_pas_pa_view
integer x = 3118
integer y = 148
integer width = 430
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
boolean enabled = false
string text = "Previous "
boolean focusrectangle = false
end type

type st_next from statictext within w_pas_pa_view
integer x = 3118
integer y = 88
integer width = 425
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
boolean enabled = false
string text = "Next "
boolean focusrectangle = false
end type

