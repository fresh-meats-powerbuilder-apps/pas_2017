HA$PBExportHeader$w_age_ranges_new.srw
$PBExportComments$Age Schedule Ranges
forward
global type w_age_ranges_new from w_base_sheet_ext
end type
type dw_age_ranges_new from u_base_dw_ext within w_age_ranges_new
end type
end forward

global type w_age_ranges_new from w_base_sheet_ext
integer width = 3410
integer height = 1604
string title = "Age Ranges"
dw_age_ranges_new dw_age_ranges_new
end type
global w_age_ranges_new w_age_ranges_new

type variables
datawindowchild 			idddw_child_age



Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product, &
				lb_updating, ib_updating , lb_repeat

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow, il_last_clicked_row

s_error		istr_error_info

u_ws_pas4       iu_ws_pas4

String		     is_colname, &
				is_input, &
				is_ChangedColumnName, &
				is_debug, &
				age_code, & 
				date_from, &
				date_to, &
				update_date, &
				update_userid
				
w_base_sheet	iw_order_det
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean uf_set_age_range_desc (integer as_selected_row, string as_age_range_code)
public function boolean wf_validate (integer as_row)
public function boolean wf_update ()
public function integer wf_checkstatus ()
public subroutine wf_delete ()
end prototypes

public function boolean wf_retrieve ();String 	ls_paspdav, ls_outputstring,ls_detail, ls_indicator

Long		ll_rec_count

Integer   li_rtn_code

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false 

SetPointer(HourGlass!)

istr_error_info.se_event_name = "wf_retrieve"

dw_age_ranges_new.Reset()
ls_indicator = 'I'

If (iu_ws_pas4.nf_pasp01gr(istr_error_info,ls_indicator,ls_detail,ls_outputstring)) < 0 Then Return False

If Not iw_frame.iu_string.nf_IsEmpty(ls_outputstring) Then
	ll_rec_count = dw_age_ranges_new.ImportString(ls_outputstring)
	If ll_rec_count > 0 then SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
		
	//dw_age_ranges_new.SelectRow(1, TRUE)
End if

age_code = dw_age_ranges_new.GetItemString(1,"age_schedule_code")
date_from = dw_age_ranges_new.GetItemString(1,"date_from")
date_to = dw_age_ranges_new.GetItemString(1,"date_to")
update_date = dw_age_ranges_new.GetItemString(1,"update_date")
update_userid = dw_age_ranges_new.GetItemString(1,"update_user_id")

dw_age_ranges_new.ResetUpdate()
dw_age_ranges_new.SetSort("age_schedule_code asc, date_from asc")	
dw_age_ranges_new.Sort()
return True
end function

public function boolean uf_set_age_range_desc (integer as_selected_row, string as_age_range_code);Return (dw_age_ranges_new.SetItem( as_selected_row, "age_schedule_desc", as_age_range_code) = 1)
end function

public function boolean wf_validate (integer as_row);integer ls_total_row, li_count
long date_to1,date_to2,date_from1, date_from2
String code1, code2


If iw_frame.iu_string.nf_IsEmpty(dw_age_ranges_new.GetItemString &
		(as_row, "age_schedule_code")) Then 
		dw_age_ranges_new.ScrollToRow(as_row)
		dw_age_ranges_new.SetColumn("age_schedule_code")
		dw_age_ranges_new.TriggerEvent (Itemerror!)
		Return False
End If

If dw_age_ranges_new.GetItemNumber(as_row,"date_from") > 9999 Then
		dw_age_ranges_new.ScrollToRow(as_row)
		dw_age_ranges_new.SetColumn("date_from")
		dw_age_ranges_new.TriggerEvent (Itemerror!)
		Return False
End If

If dw_age_ranges_new.GetItemNumber(as_row,"date_to") > 9999 Then
		dw_age_ranges_new.ScrollToRow(as_row)
		dw_age_ranges_new.SetColumn("date_to")
		dw_age_ranges_new.TriggerEvent (Itemerror!)
		Return False
End If


//check if there's no overlapped days_to for the same schedule_code
ls_total_row = dw_age_ranges_new.RowCount()
For li_count = 1 To ls_total_row
	If li_count <> as_row Then
		code1 = dw_age_ranges_new.GetItemString(as_row,"age_schedule_code")
		code2 = dw_age_ranges_new.GetItemString(li_count,"age_schedule_code")
		date_from1 = dw_age_ranges_new.GetItemNumber(as_row,"date_from")
     	date_to1 = dw_age_ranges_new.GetItemNumber(as_row,"date_to")
		date_from2 = dw_age_ranges_new.GetItemNumber(li_count,"date_from")
		date_to2 = dw_age_ranges_new.GetItemNumber(li_count,"date_to")
		If  date_from1 > date_to1 Then
			dw_age_ranges_new.ScrollToRow(as_row)
			dw_age_ranges_new.SetColumn("date_to")
			lb_repeat = True
			dw_age_ranges_new.TriggerEvent (Itemerror!)
			iw_frame.SetMicroHelp("Date From cannot be lower to Date To")
			Return False
		else
			lb_repeat = False
		End If
		If (code1 = code2) then
			if(date_from1 >= date_from2 AND date_from1 <= date_to2 ) OR (date_to1 <= date_to2 AND date_to1 >= date_from2)Then
				dw_age_ranges_new.ScrollToRow(as_row)
				dw_age_ranges_new.SetColumn("date_to")
				lb_repeat = True
				dw_age_ranges_new.TriggerEvent (Itemerror!)
				iw_frame.SetMicroHelp("Cannot overlapp From or To dates with the same Shedule Code")
				Return False
			end if	
		else
			lb_repeat = False
			
		End If
	End If
Next


end function

public function boolean wf_update ();int				li_count, ii_rec_count, li_rval
long				ll_Row, &
					ll_modrows, &
					ll_delrows, &
					total_rows, date_from1, date_from2, date_to1, date_to2
Char				lc_status_ind					
string			ls_update_string,ls_outputstring, ls_indicator

IF dw_age_ranges_new.AcceptText() = -1 Then Return False

total_rows = dw_age_ranges_new.RowCount()
ll_modrows = dw_age_ranges_new.ModifiedCount()
ll_delrows = dw_age_ranges_new.DeletedCount()



IF ll_modrows <= 0 and ll_delrows <= 0 Then Return False

wf_checkstatus()
// INSERT AND UPDATE ----
ll_row = 0
For li_count = 1 To total_rows
	//build update string
	If dw_age_ranges_new.GetItemString(li_count,"update_flag") = 'I' Then
		If Not wf_validate(li_count) Then 
			ib_updating = False
			Return False
		else
//			date_ =	string(dw_age_ranges_new.GetItemDate(li_count,"update_date"))
			ls_update_string +=  dw_age_ranges_new.GetItemString(li_count,"age_schedule_code") + '~t' +&
			string(dw_age_ranges_new.GetItemNumber(li_count,"date_from")) + '~t' +&
			string(dw_age_ranges_new.GetItemNumber(li_count,"date_to")) + '~t' + 'I' + '~r~n'
		End if
	Else 
		If dw_age_ranges_new.GetItemString(li_count,"update_flag") = 'U' Then
			If Not wf_validate(li_count) Then 
				ib_updating = False
				iw_frame.SetMicroHelp("Data error, Check 'Date From' and 'Date To' values...")
				Return False
			else
				date_from1 = dw_age_ranges_new.GetItemNumber(li_count,"date_from")
				date_from2 = dw_age_ranges_new.GetItemNumber(li_count,"date_from_old")
				date_to1 = dw_age_ranges_new.GetItemNumber(li_count,"date_to")
				date_to2 = dw_age_ranges_new.GetItemNumber(li_count,"date_to_old")
			     IF date_from1 = date_from2 AND date_to1 <> date_to2 THEN
					  ls_update_string += dw_age_ranges_new.GetItemString(li_count,"age_schedule_code") + '~t' +&
				       string(dw_age_ranges_new.GetItemNumber(li_count,"date_from_old")) + '~t' +&
		                string(dw_age_ranges_new.GetItemNumber(li_count,"date_to")) + '~t' + 'N' +'~r~n'
				else		  
				     IF date_from2 <> date_from1 OR date_to1 <> date_to2 THEN 
						ls_update_string += dw_age_ranges_new.GetItemString(li_count,"age_schedule_code") + '~t' +&
				         string(dw_age_ranges_new.GetItemNumber(li_count,"date_from_old")) + '~t' +&
		                  string(dw_age_ranges_new.GetItemNumber(li_count,"date_to_old")) + '~t' + 'D' + '~r~n' +&
						dw_age_ranges_new.GetItemString(li_count,"age_schedule_code") + '~t' +&
				       string(dw_age_ranges_new.GetItemNumber(li_count,"date_from")) + '~t' +&
		                string(dw_age_ranges_new.GetItemNumber(li_count,"date_to")) + '~t' + 'I' + '~r~n'
				    END IF		 
				END IF
			End If
		End If
	End If
Next

If ls_update_string <> '' Then
	ls_indicator = 'U'
	li_rval = iu_ws_pas4.NF_PASP01GR(istr_error_info,ls_indicator,ls_update_string,ls_outputstring)

	ib_updating = False
	iw_frame.SetMicroHelp("Modification Successful")
	//wf_retrieve()
	dw_age_ranges_new.ResetUpdate()
	dw_age_ranges_new.SetColumn("age_schedule_code")
	dw_age_ranges_new.SetRedraw(True)
End If

Return( True )
end function

public function integer wf_checkstatus ();//----------------Get modified rows---------------------------
long total_rows, cur_row, ll_Row
integer li_count 
string val

total_rows = dw_age_ranges_new.RowCount()

For li_count = 1 To total_rows 
	ll_row = li_count
	Choose Case dw_age_ranges_new.GetItemStatus(ll_row,0,Primary!)
		Case DataModified!
			dw_age_ranges_new.SetItem(ll_row,"update_flag","U")
		Case NewModified!
			dw_age_ranges_new.SetItem(ll_row,"update_flag","I")
	End Choose
Next


return 1

end function

public subroutine wf_delete ();DataWindowChild			ldwc_week_end

int  						li_rval

long  					    ll_source_row

String						ls_flag, ls_update_string, ls_outputstring, code,ls_indicator




If age_code <> '' Then
	ls_update_string = age_code + '~t' +&
				date_from + '~t' +&
				date_to + '~t' +&
				'D' + '~r~n'
			End If

If ls_update_string <> '' Then
	ls_indicator = 'U'
	li_rval = iu_ws_pas4.NF_PASP01GR(istr_error_info,ls_indicator,ls_update_string,ls_outputstring)
End If
dw_age_ranges_new.SetRedraw(False)
dw_age_ranges_new.SetRedraw(True)

end subroutine

on w_age_ranges_new.create
int iCurrent
call super::create
this.dw_age_ranges_new=create dw_age_ranges_new
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_age_ranges_new
end on

on w_age_ranges_new.destroy
call super::destroy
destroy(this.dw_age_ranges_new)
end on

event ue_postopen;call super::ue_postopen;string ls_age_schedule_code
		

dw_age_ranges_new.GetChild("age_schedule_code", idddw_child_age)

idddw_child_age.SetTransObject(SQLCA)
idddw_child_age.Retrieve("AGE SCHD")
idddw_child_age.SetSort("type_code")
idddw_child_age.Sort()
//idddw_child_age.InsertRow(0)

iu_ws_pas4 = create u_ws_pas4

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_age_ranges_new"
istr_error_info.se_user_id 		= sqlca.userid

This.PostEvent('ue_query')
end event

event ue_query;call super::ue_query;wf_retrieve()
end event

event ue_addrow;call super::ue_addrow;DataWindowChild			ldwc_week_end

Integer						li_count, &
								li_sequence

Long							ll_row, ll_sel_row, ll_source_row

String						ls_string



ll_row = dw_age_ranges_new.RowCount() + 1
ll_sel_row = dw_age_ranges_new.GetSelectedRow(0)
ll_source_row	= dw_age_ranges_new.GetRow()
dw_age_ranges_new.SetRedraw(False)

/// SET DEFAULT VALUES-----------
dw_age_ranges_new.SetItem(ll_source_row,"age_schedule_code",0)
dw_age_ranges_new.SetItem(ll_source_row,"date_from",0)
dw_age_ranges_new.SetItem(ll_source_row,"date_to",0)
dw_age_ranges_new.SetItem(ll_source_row,"date_from_old",0)
dw_age_ranges_new.SetItem(ll_source_row,"date_to_old",0)
dw_age_ranges_new.SetItem(ll_source_row,"update_date",now())
dw_age_ranges_new.SetItem(ll_source_row,"update_user_id",sqlca.userid)
dw_age_ranges_new.SetItem(ll_source_row,"update_flag", 'I')

dw_age_ranges_new.object.age_schedule_code.protect = False
dw_age_ranges_new.SelectRow(ll_sel_row, FALSE)
dw_age_ranges_new.SelectRow(ll_source_row,TRUE)
dw_age_ranges_new.object.update_date.protect = True

dw_age_ranges_new.GroupCalc()
dw_age_ranges_new.ScrollToRow(ll_row)
dw_age_ranges_new.SetColumn( "age_schedule_code" )
dw_age_ranges_new.SetFocus()
dw_age_ranges_new.SetRedraw(True)

return 0
end event

event ue_deleterow;call super::ue_deleterow;wf_delete()
end event

type dw_age_ranges_new from u_base_dw_ext within w_age_ranges_new
integer x = 50
integer y = 28
integer width = 3301
integer height = 1428
integer taborder = 10
string dataobject = "d_age_ranges_new"
end type

event constructor;call super::constructor;If This.RowCount() = 0 Then This.InsertRow(0)
end event

event itemchanged;call super::itemchanged;String	ls_age_range_code, &
			ls_age_range_desc
			
						
Long		ll_row, selected_row

If ib_Updateable Then
	choose case dwo.name

			case "age_schedule_code" 
			selected_row = This.GetRow()
			ls_age_range_code = data
						
			If Len(Trim(ls_age_range_code)) = 0 Then 
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				This.ScrollToRow(This.RowCount())
				This.SetItem(1,"ls_age_range_code","")
				uf_set_age_range_desc(selected_row,"")
				return 
			End if
			
			// Find the entered plant code in the table -- don't count the last row, it's blank
			
			ll_row = idddw_child_age.Find('type_code = "' + Trim(ls_age_range_code) + '"', 1, idddw_child_age.RowCount())
			If ll_row <= 0 Then
				iw_Frame.SetMicroHelp(ls_age_range_code + " is an Invalid Range Code")
	
				This.SetFocus()
				This.SelectText(1, Len(ls_age_range_code))
				
				return 1
			Else
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				ls_age_range_desc = idddw_child_age.GetItemString(ll_row, 'type_short_desc')
				uf_set_age_range_desc(selected_row,ls_age_range_desc)
				This.SetFocus()
				This.SelectText(1, Len(ls_age_range_code))
			End if
			
	end choose	
End If

Return 0
			
end event

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color


IF row > 0 Then
	If il_last_clicked_row = 0 and row <> 1 Then
		il_last_clicked_row = This.GetSelectedRow(0)
		If il_last_clicked_row = 0 Then
			il_last_clicked_row = 1
		End If
	End If
	
	If row <> il_last_clicked_row Then 
		SelectRow(row, TRUE)
		SelectRow(il_last_clicked_row,FALSE)
	End If
	
	
	
	 age_code = dw_age_ranges_new.GetItemString(row,"age_schedule_code") 
      date_from = string(dw_age_ranges_new.GetItemNumber(row,"date_from"))
      date_to =  string(dw_age_ranges_new.GetItemNumber(row,"date_to"))
	
	
	This.SetRow(Row)
End If
il_ChangedRow = row
il_last_clicked_row = row

end event

event ue_deleterow;call super::ue_deleterow;wf_delete()
end event

