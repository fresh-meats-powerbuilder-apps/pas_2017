HA$PBExportHeader$w_plant_constr.srw
forward
global type w_plant_constr from w_base_sheet_ext
end type
type dw_plant_prod_code from u_base_dw within w_plant_constr
end type
type dw_plant_constr from u_base_dw_ext within w_plant_constr
end type
end forward

global type w_plant_constr from w_base_sheet_ext
integer width = 2939
integer height = 1516
string title = "Plant Constraint"
long backcolor = 67108864
dw_plant_prod_code dw_plant_prod_code
dw_plant_constr dw_plant_constr
end type
global w_plant_constr w_plant_constr

type variables
u_pas203		iu_pas203

u_pas201		iu_pas201
u_ws_pas1		iu_ws_pas1

s_error		istr_error_info

Integer		ii_rec_count

String		is_update_string

Boolean		ib_updating
end variables

forward prototypes
public function boolean wf_check_required ()
public function boolean wf_update_modify (long al_row, character ac_status_ind, string as_header)
public subroutine wf_delete ()
public subroutine wf_filenew ()
public subroutine wf_print ()
public function string wf_get_plant ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function boolean wf_retrieve ()
public function string wf_get_prod ()
public function boolean wf_addrow ()
end prototypes

public function boolean wf_check_required ();Long			ll_row

Integer		li_colnbr

String		ls_colname, &
				ls_textname

//dw_plant_constr.SetRedraw(False)

dw_plant_constr.uf_required(true)

li_colnbr = 0

ll_Row = 1
DO WHILE ll_Row <> 0
	// If there's an error, exit
	IF dw_plant_constr.FindRequired(Primary!,	&
			ll_Row, li_ColNbr, ls_ColName, FALSE ) < 0 THEN 
		uf_required ( dw_plant_constr, false )
		li_colnbr = dw_plant_constr.GetColumn ( )
		dw_plant_constr.Setcolumn("shift_code")
		dw_plant_constr.Setcolumn(li_colnbr)
//		dw_plant_constr.SetRedraw(True)
		Return( False )
	END IF

	// If a row was found, save the row and column
	IF ll_Row <> 0 THEN
		// Get the text of that column's label.
		ls_TextName = ls_colname + "_t.Text"
		ls_colname = dw_plant_constr.Describe(ls_TextName)
//		dw_plant_constr.SetRedraw(True)
		// Tell the user which column to fill in.
		MessageBox(ls_colname,  "Please enter a value for '"  &
			+ ls_colname + "', row " + String(ll_Row) + ".")
		// Make the problem column current.
		uf_required ( dw_plant_constr, false )
		dw_plant_constr.SetColumn(li_ColNbr)
		dw_plant_constr.ScrollToRow(ll_Row)
		dw_plant_constr.SetFocus()
		Return( False )
	END IF
	// If no row was found, drop out of the loop
LOOP

// There is a bug with the edit mask in PB 4.  Resets Focus.  rem

uf_required ( dw_plant_constr, false )

li_colnbr = dw_plant_constr.GetColumn ( )
dw_plant_constr.Setcolumn("shift_code")
dw_plant_constr.Setcolumn(li_colnbr)
//dw_plant_constr.SetRedraw(True)

Return (True)
end function

public function boolean wf_update_modify (long al_row, character ac_status_ind, string as_header);String						ls_group_code

Char							lc_status_ind

DWBuffer						ldwb_buffer


IF ac_status_ind = "D" THEN
	ldwb_buffer = delete!
ELSE
	ldwb_buffer = primary!
End If

ls_group_code = dw_plant_constr.GetItemString(al_row, "group_code",ldwb_buffer, False)
If IsNull(ls_group_code) then ls_group_code = ' ' 

is_update_string += dw_plant_constr.GetItemString(al_row, &
	"shift_code",ldwb_buffer, False) + "~t" + &
	String(dw_plant_constr.GetItemDate &
	(al_row,"begin_date",ldwb_buffer, False),"yyyy-mm-dd") + "~t" + &
	String(dw_plant_constr.GetItemDate &
	(al_row,"end_date",ldwb_buffer, False),"yyyy-mm-dd") + "~t" + &
	String(dw_plant_constr.GetItemNumber(al_row, &
	"mfg_step_sequence",ldwb_buffer, False)) + "~t" + &
	ls_group_code + "~t" + &
	String(dw_plant_constr.GetItemDecimal &
	(al_row,"min_conv",ldwb_buffer, False)) + "~t" + &
	String(dw_plant_constr.GetItemDecimal &
	(al_row,"max_conv",ldwb_buffer, False)) + "~t" + &
	String(dw_plant_constr.GetItemDecimal &
	(al_row,"cost_to_prod",ldwb_buffer, False)) + "~t" + &
	ac_status_ind + "~r~n"
	
ii_rec_count ++

If ii_rec_count = 100 Then
//	IF Not iu_pas203.nf_upd_plt_const(istr_error_info, as_header, &
//			is_update_string) THEN Return False
	ii_rec_count = 0
	is_update_string = ""
END IF

Return True
end function

public subroutine wf_delete ();wf_deleterow()

end subroutine

public subroutine wf_filenew ();This.wf_addrow()


end subroutine

public subroutine wf_print ();dw_plant_constr.print()
end subroutine

public function string wf_get_plant ();String			ls_plant

ls_plant = dw_plant_prod_code.GetItemString &
						(1, "plant_code") + "~t" + &
					dw_plant_prod_code.GetItemString &
					(1, "plant_descr") 

Return(ls_plant)
end function

public function boolean wf_validate (long al_row);Long				ll_rtn, &
					ll_nbrrows, &
					ll_select

String			ls_searchstring, &
					ls_shift, &
					ls_date, &
					ls_temp

Integer			li_mfg_sequence

Date				ldt_begin_date

u_string_functions		lu_string


ls_temp = String(dw_plant_constr.GetItemNumber(al_row, "mfg_step_sequence"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant_constr.ScrollToRow(al_row)
	dw_plant_constr.SetColumn("mfg_step_sequence")
	MessageBox('Required Field', 'Step is a required field.  Please enter a value for Step.')
	Return False
End If

ls_temp = String(dw_plant_constr.GetItemDecimal(al_row, "cost_to_prod"))
If lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant_constr.ScrollToRow(al_row)
	dw_plant_constr.SetColumn("cost_to_prod")
	MessageBox('Required Field', 'Cost to Produce is a required field.  Please enter a value for Cost to Produce.')
	Return False
End If


ll_nbrrows = dw_plant_constr.RowCount()

If ll_nbrrows > 1 Then
	ls_date = string(dw_plant_constr.GetItemDate &
								(al_row, "begin_date"), "mm/dd/yyyy")
	If al_row < 1 Then Return False
	ls_SearchString	= "shift_code ='" + dw_plant_constr.GetItemString &
								(al_row, "shift_code") + &
								"' and mfg_step_sequence = " + &
								String(dw_plant_constr.GetItemNumber &
								(al_row, "mfg_step_sequence")) + &
								" and end_date >= Date('" + &
								string(dw_plant_constr.GetItemDate &
								(al_row, "begin_date"), "mm/dd/yyyy") + "')" + &						
								" and begin_date <= Date('" + &
								string(dw_plant_constr.GetItemDate &
								(al_row, "end_date"), "mm/dd/yyyy") + "')"
	
	
	CHOOSE CASE al_row 
		CASE 1
			ll_rtn = dw_plant_constr.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
		CASE 2 to (ll_nbrrows - 1)
			ll_rtn = dw_plant_constr.Find ( ls_SearchString, al_row - 1, 1)
			If ll_rtn = 0 Then ll_rtn = dw_plant_constr.Find  &
				(ls_SearchString, al_row + 1, ll_nbrrows)
		CASE ll_nbrrows 
			ll_rtn = dw_plant_constr.Find ( ls_SearchString, al_row - 1, 1)
	END CHOOSE
	
	
	If ll_rtn > 0 Then
		MessageBox ("Begin and End Date", "Plant Constraint" + &
						" With the same Manufacturing Step and the same Shift" + &
						" can not have overlapping dates.") 
		dw_plant_constr.SetRedraw(False)
		dw_plant_constr.ScrollToRow(al_row)
		dw_plant_constr.SetColumn("begin_date")
		dw_plant_constr.SetRow(al_row)
		dw_plant_constr.SelectRow(ll_rtn, True)
		dw_plant_constr.SelectRow(al_row, True)
		dw_plant_constr.SetRedraw(True)
		Return False
	End If
End if
If dw_plant_constr.GetItemDate(al_row,"end_date") < &
		dw_plant_constr.GetItemDate(al_row,"begin_date") Then
	dw_plant_constr.ScrollToRow(al_row)
	dw_plant_constr.SetColumn("end_date")
	dw_plant_constr.TriggerEvent (Itemerror!)
//	dw_plant_constr.SetActionCode(1)
	Return False
End If
If dw_plant_constr.GetItemDecimal(al_row,"min_conv") > &
		dw_plant_constr.GetItemDecimal(al_row,"max_conv") Then
	dw_plant_constr.ScrollToRow(al_row)
	dw_plant_constr.SetColumn("min_conv")
	dw_plant_constr.TriggerEvent (Itemerror!)
//	dw_plant_constr.SetActionCode(1)
	Return False
End If
If dw_plant_constr.GetItemDecimal(al_row,"max_conv") < &
		dw_plant_constr.GetItemDecimal(al_row,"min_conv") Then
	dw_plant_constr.ScrollToRow(al_row)
	dw_plant_constr.SetColumn("max_conv")
	dw_plant_constr.TriggerEvent (Itemerror!)
//	dw_plant_constr.SetActionCode(1)
	Return False
End If

Return True
end function

public function boolean wf_update ();// ** IBDKEEM ** 08/12/2002 ** Added Product State

int				li_count

long				ll_Row, &
					ll_modrows, &
					ll_delrows

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_prod_state, &
					ls_prod_status, &
					ls_header


SetPointer(HourGlass!)

ib_updating = True

IF dw_plant_constr.AcceptText() = -1 Then 
	ib_updating = False
	Return False
End if

ls_plant = dw_plant_prod_code.GetItemString(1, "plant_code")
ls_prod  = dw_plant_prod_code.GetItemString(1, "prod_code")
ls_prod_state = dw_plant_prod_code.GetItemString(1, "prod_state")
ls_prod_status = dw_plant_prod_code.GetItemString(1, "prod_status")

If iw_frame.iu_string.nf_IsEmpty(ls_Plant) or &
	iw_frame.iu_string.nf_IsEmpty(ls_prod) or &
	iw_frame.iu_string.nf_IsEmpty(ls_prod_state) or &
	iw_frame.iu_string.nf_IsEmpty(ls_prod_status) Then 
	iw_frame.SetMicroHelp("Please enter a Plant, Product, State and Status before updating.")
	ib_updating = False
	Return False
End If

ls_header = ls_plant	+ "~t" + ls_prod + "~t" + ls_prod_state + "~t" + ls_prod_status

ll_modrows = dw_plant_constr.ModifiedCount()
ll_delrows = dw_plant_constr.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0 Then 
	ib_updating = False
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

This.SetRedraw(False)

//If Not This.wf_check_required() Then 
//	ib_updating = False
//	This.SetRedraw(True)
//	Return False
//End if

is_update_string = ""
ii_rec_count = 0
ll_row = 0
dw_plant_constr.SelectRow(0,False)

For li_count = 1 To ll_modrows

	ll_Row = dw_plant_constr.GetNextModified(ll_Row, Primary!) 

	If Not wf_validate(ll_row) Then 
		This.SetRedraw(True)
		Return False
	End If
	
	Choose Case dw_plant_constr.GetItemStatus(ll_row, 0, Primary!)
		CASE NewModified!
			lc_status_ind = 'A'
		CASE DataModified!
			lc_status_ind = 'M'
	END CHOOSE	

	If Not This.wf_update_modify(ll_row, lc_status_ind, ls_header) Then 
		This.SetRedraw(True)
		Return False
	End If
Next

For li_count = 1 To ll_delrows 

	lc_status_ind = 'D'

	If Not This.wf_update_modify(li_count, lc_status_ind, ls_header) Then 
		ib_updating = False
		This.SetRedraw(True)
		Return False
	End if

Next

//dw_plant_constr.SetRedraw(False)
If ii_rec_count > 0 Then
	SetMicroHelp("Wait, Updating ...")
	SetPointer(HourGlass!)

//	IF Not iu_pas203.nf_upd_plt_const(istr_error_info, ls_header, is_update_string) THEN 
	IF Not iu_ws_pas1.nf_pasp34fr(istr_error_info, ls_header, is_update_string) THEN 
		ib_updating = False
		This.SetRedraw(True)
		Return False
	End if
End If

iw_frame.SetMicroHelp("Modification Successful")
dw_plant_constr.ResetUpdate()
dw_plant_constr.SetColumn("end_date")
//dw_plant_constr.SetRedraw(True)
This.SetRedraw(True)

ib_updating = False
Return( True )
end function

public function boolean wf_retrieve ();//** IBDKEEM ** 08/12/2002 ** Added Product State
Int			li_ret

Char			lc_fab_product_descr[30], &
				lc_fab_group[2]

String		ls_string, &
				ls_prod_data, &
				ls_prod_code, &
				ls_prod_state, &
				ls_prod_status, &
				ls_plant, &
				ls_input, &
				ls_mfg_sequence, &
				ls_header_string,&
				ls_fab_product_descr,&
	  			ls_fab_group

Long			ll_rec_count


DataWindowChild	ldwc_mfg_sequence

Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return false

OpenWithParm(w_plant_constr_inq, This)
ls_string = Message.StringParm
If iw_frame.iu_string.nF_IsEmpty(ls_string) Then Return False

SetPointer(HourGlass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

dw_plant_constr.GetChild("mfg_step_sequence", ldwc_mfg_sequence)
ldwc_mfg_sequence.Reset()
dw_plant_prod_code.Reset()
dw_plant_constr.Reset()

dw_plant_prod_code.ImportString(ls_string) 
ls_plant = dw_plant_prod_code.GetItemString(1,"plant_code")
ls_prod_code = dw_plant_prod_code.GetItemString(1,"prod_code")
ls_prod_state = dw_plant_prod_code.GetItemString(1,"prod_state")
ls_prod_status = dw_plant_prod_code.GetItemString(1,"prod_status")

If iw_frame.iu_string.nf_IsEmpty(ls_Plant) or &
	iw_frame.iu_string.nf_IsEmpty(ls_prod_code) or &
	iw_frame.iu_string.nf_IsEmpty(ls_prod_state) or &
	iw_frame.iu_string.nf_IsEmpty(ls_prod_status) Then 
	iw_frame.SetMicroHelp("Please enter a Plant, Product, State and Status before inquiring.")
	ib_updating = False
	Return False
End If

ls_header_string = ls_prod_code + '~t' + ls_prod_state + '~t' + ls_prod_status + '~t' 

ls_fab_product_descr = lc_fab_product_descr
ls_fab_group = lc_fab_group

//If Not iu_pas201.nf_pasp04br(istr_error_info, &
//										ls_header_string, &
//										lc_fab_product_descr, &
//										lc_fab_group, &
//										ls_mfg_sequence) Then Return False

If Not iu_ws_pas1.nf_pasp04fr(istr_error_info, &
										ls_header_string, &
										ls_fab_product_descr, &
										ls_fab_group, &
										ls_mfg_sequence) Then Return False

lc_fab_product_descr = ls_fab_product_descr
lc_fab_group = ls_fab_group



ldwc_mfg_sequence.ImportString(ls_mfg_sequence)
	
istr_error_info.se_event_name = "wf_retrieve"
//If Not iu_pas203.nf_inq_plt_const(istr_error_info, &
//											ls_plant + "~t" + ls_prod_code + "~t" + &
//											ls_prod_state + "~t" +  ls_prod_status, &
//											ls_prod_Data) Then Return False


If Not iu_ws_pas1.nf_pasp33fr(istr_error_info, &
											ls_plant + "~t" + ls_prod_code + "~t" + &
											ls_prod_state + "~t" +  ls_prod_status, &
											ls_prod_Data) Then Return False

SetRedraw (False)

ll_rec_count = dw_plant_constr.ImportString(ls_prod_Data)

If ll_rec_count > 0 Then 
	iw_frame.SetMicroHelp(String(ll_rec_count) + " Rows Retrieved")
Else
	iw_frame.SetMicroHelp("0 Records Retrieved")
End If

dw_plant_constr.ResetUpdate()

// The first two fields are protected. Move focus to 1rst unprotected.
dw_plant_constr.SetColumn("end_date")
SetRedraw(True)

Return True
end function

public function string wf_get_prod ();String				ls_prod

ls_prod = dw_plant_prod_code.GetItemString (1, "prod_code")  + "~t" + &
			 dw_plant_prod_code.GetItemString (1, "prod_descr") + "~t" + & 
			 dw_plant_prod_code.GetItemString (1, "prod_state")  + "~t" + &
			 dw_plant_prod_code.GetItemString (1, "prod_state_descr") + "~t" + & 
			 dw_plant_prod_code.GetItemString (1, "prod_status")  + "~t" + &
			 dw_plant_prod_code.GetItemString (1, "prod_status_descr")

Return(ls_prod)
end function

public function boolean wf_addrow ();Long				ll_row

DataWindowChild	ldwc_type

If iw_frame.iu_string.nf_IsEmpty(dw_plant_prod_code.GetItemString(1, "plant_code")) or &
		iw_frame.iu_string.nf_IsEmpty(dw_plant_prod_code.GetItemString(1, "prod_code")) Then
	iw_frame.SetMicroHelp("Please reinquire and enter a Plant" + &
			" and a Product before adding a row.")
	Return False
End If

dw_plant_constr.SetRedraw(False)

ll_row = dw_plant_constr.InsertRow(0)

dw_plant_constr.SetItem ( ll_row, "begin_date", today())
dw_plant_constr.SetItem ( ll_row, "end_date", date("2999-12-31"))
dw_plant_constr.SetItem ( ll_row, "shift_code", "A")
dw_plant_constr.SetItem ( ll_row, "group_code", " ")
dw_plant_constr.SetItemStatus(ll_row, 0, Primary!, NotModified!)

dw_plant_constr.ScrollToRow(ll_row)
dw_plant_constr.SetColumn( "shift_code" )
//jac 12-13
dw_plant_constr.GetChild ("shift_code", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("shift")
dw_plant_constr.SetColumn( "shift_code" )
dw_plant_constr.InsertRow(0)
//
dw_plant_constr.SetFocus()
dw_plant_constr.SetRedraw(True)

Return( true )
end function

event close;call super::close;IF IsValid( iu_pas203 ) THEN
	DESTROY iu_pas203
END IF

IF IsValid( iu_pas201 ) THEN
	DESTROY iu_pas201
END IF

IF IsValid( iu_ws_pas1 ) THEN
	DESTROY iu_ws_pas1
END IF

end event

event ue_postopen;call super::ue_postopen;iu_pas203 = create u_pas203
iu_ws_pas1 = Create u_ws_pas1
If Message.ReturnValue = -1 Then
	Close(This)
	Return
End If

If Not IsValid(iu_pas201) then iu_pas201 = create u_pas201
If Message.ReturnValue = -1 Then
	Close(This)
	Return
End If

istr_error_info.se_app_name			= "Pas"
istr_error_info.se_window_name		= "pltconst"
istr_error_info.se_user_id				= sqlca.userid

// This.PostEvent(iw_frame.im_menu.m_file.m_inquire.PostEvent("Clicked"))

This.PostEvent('ue_query')
end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

on w_plant_constr.create
int iCurrent
call super::create
this.dw_plant_prod_code=create dw_plant_prod_code
this.dw_plant_constr=create dw_plant_constr
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant_prod_code
this.Control[iCurrent+2]=this.dw_plant_constr
end on

on w_plant_constr.destroy
call super::destroy
destroy(this.dw_plant_prod_code)
destroy(this.dw_plant_constr)
end on

event resize;call super::resize;integer li_y		= 120


if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


if this.width > dw_plant_constr.x + 40 then
	dw_plant_constr.width = this.width - (dw_plant_constr.x + 40)
end if


//if this.height > dw_plant_constr.y + 120 then
//	dw_plant_constr.Height = this.height - (dw_plant_constr.y + 120)
//end if

if this.height > dw_plant_constr.y + li_y then
	dw_plant_constr.Height = this.height - (dw_plant_constr.y + li_y)
end if


end event

type dw_plant_prod_code from u_base_dw within w_plant_constr
integer x = 5
integer y = 12
integer width = 2830
integer height = 244
integer taborder = 0
string dataobject = "d_plant_fabprod_code"
boolean border = false
end type

on constructor;call u_base_dw::constructor;This.insertrow(0)

ib_updateable = False
end on

type dw_plant_constr from u_base_dw_ext within w_plant_constr
integer y = 288
integer width = 2853
integer height = 1056
integer taborder = 10
string dataobject = "d_plant_constr"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;Decimal	ldc_value

Date		ldt_value, ldt_begin_date

String	ls_colnam

dwItemStatus	lis_Status


Choose Case GetColumnName()
	Case "begin_date" 
		ldt_value = Date(Data)
		If ldt_value < today() Then
			Return 1
		End If
		If GetItemStatus(row, "end_date", primary!) = &
				DataModified! Then
			If ldt_value > Getitemdate(row,"end_date") Then
				Return 1
			End If
		End If
	Case "end_date" 
		ldt_value = Date(Data)
		If GetItemStatus(row, "begin_date", primary!) = &
				DataModified! Then
			If ldt_value < Getitemdate(row,"begin_date") Then
				Return 1
			End If
		End If
	Case "min_conv" 
		ldc_value = Dec(Data)
		If ldc_value < 0 or ldc_value > 100 Then
			Return 1
		End If
		
		If GetItemStatus(row, 0, primary!) = DataModified! Then
			If ldc_value > GetItemDecimal(row,"max_conv") Then
				Return 1
			End If
		End If
	Case "max_conv" 
		ldc_value = Dec(Data)
		If ldc_value < 0 or ldc_value > 100 Then
			Return 1
		End If
		If GetItemStatus(row, 0, primary!) = &
				DataModified! Then
			If ldc_value < GetItemDecimal(row,"min_conv") Then
				Return 1
			End If
		End If
	Case "cost_to_prod" 
		If Dec(Data) < 0 Then
			Return 1
		End If
End Choose

SetItem(row,"upd_user_id", sqlca.userid)
SetItem(row,"upd_date",Today())

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
end event

event itemerror;call super::itemerror;Date			ldt_value

Decimal		ldc_value

// Default to return 1
Int			li_ReturnCode = 1

String		ls_error_msg, &
				ls_error_ttl


// These will be null if this event was triggered
If IsNull(Row) Then row = This.GetRow()
If IsNull(data) Then data = This.GetText()

CHOOSE CASE GetColumnName()
	CASE "begin_date"
		ldt_value = Date(data)
		If ldt_value < Today() Then
			ls_error_msg = "Begin Date must be" + &
				" greater than or equal to today" 
		Else
			If ldt_value > This.GetItemDate(row, "end_date") Then
				ls_error_msg = "Begin Date must be less than" + &
					" the End Date"
			End IF
		End If
		ls_error_ttl = "Begin Date"

	CASE "end_date"
		ls_error_msg = "End Date must be greater than" + &
			" the Begin Date"
		ls_error_ttl = "End Date"

	CASE "min_conv"
		ldc_value = Dec(data)
		If ldc_value < 0 or ldc_value > 100 Then
			ls_error_msg = "Minimum Conversion must be" + &
					" between 0 and 100"
		Else
			If ldc_value > This.GetItemDecimal(row, "max_conv") Then
				ls_error_msg = "Minimum Conversion must be less than" + &
					" or equal to the Maximum Conversion"
			End If
		End If
		ls_error_ttl = "Minimum Conversion"

	CASE "max_conv"
		ldc_value = Dec(data)
		If ldc_value < 0 or ldc_value > 100 Then
			ls_error_msg = "Maximum Conversion must be" + &
				" between 0 and 100"
		Else
			If ldc_value < This.GetItemDecimal(row, "min_conv") Then
				ls_error_msg = "Maximum Conversion must be greater than" + &
				" or equal to the Minimum Conversion"
			End If
		End If
		ls_error_ttl = "Maximum Conversion"

	CASE "cost_to_prod"
		ls_error_msg = "Cost to Produce must be a positive Number"
		ls_error_ttl = "Cost to Produce"

END CHOOSE

If ib_updating Then
	MessageBox(ls_error_ttl, ls_error_msg + ".")
	ib_updating = False
	return 1
Else
	iw_frame.SetMicroHelp(ls_error_msg) 
	Return li_ReturnCode
End if


end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

on constructor;call u_base_dw_ext::constructor;is_selection = '1'
ib_updateable = True
ib_updating = False
end on

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;SelectText(1, Len(GetText()))
end on

