HA$PBExportHeader$w_pas_order_detail.srw
forward
global type w_pas_order_detail from w_base_sheet_ext
end type
type cbx_detail_view from checkbox within w_pas_order_detail
end type
type dw_fab from u_fab_product_code within w_pas_order_detail
end type
type st_wkend_message from statictext within w_pas_order_detail
end type
type dw_order_details from u_base_dw_ext within w_pas_order_detail
end type
type dw_effective_date from u_effective_date within w_pas_order_detail
end type
type dw_plant from u_plant within w_pas_order_detail
end type
type dw_option from u_plant_opt within w_pas_order_detail
end type
end forward

global type w_pas_order_detail from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 4119
integer height = 1696
string title = "order_details"
long backcolor = 67108864
event ue_plantran_handle ( window aw_plan_transfer )
event ue_inquire ( )
cbx_detail_view cbx_detail_view
dw_fab dw_fab
st_wkend_message st_wkend_message
dw_order_details dw_order_details
dw_effective_date dw_effective_date
dw_plant dw_plant
dw_option dw_option
end type
global w_pas_order_detail w_pas_order_detail

type variables
s_error		istr_error_info

Boolean		ib_no_inquire

String		is_data,&
				is_input, &
				is_inc_exc_string, &
				is_mcool_string, &
				is_detail_string
				
Long			il_lastrow

time			it_PT_Instance_date

u_pas202		iu_pas202
w_pas_pa_view	iw_parent


u_ws_pas5 iu_ws_pas5

w_planned_transfer_new	iw_planned_transfer_new

end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_import_data ()
end prototypes

event ue_plantran_handle(window aw_plan_transfer);//iw_plan_transfer = aw_plan_transfer

//
//
//
//
//
//
//
//
//
//String 					ls_temp,ls_temp1,ls_plant,ls_product,ls_date,ls_inquire
//u_string_functions 	lu_string
//
//ls_temp = as_data	
//ib_no_inquire = True
//ls_plant = lu_string.nf_gettoken(ls_temp,"~t") 
//dw_plant.uf_set_plant_code(ls_plant)
//ls_temp1 =lu_string.nf_gettoken(ls_temp,"~t")
//ls_product = lu_string.nf_gettoken(ls_temp,"~t")
//dw_product_code.uf_setproductcode(ls_product)
//dw_product_code.uf_setproductdesc(lu_string.nf_gettoken(ls_temp,"~t"))
//ls_date = lu_string.nf_gettoken(ls_temp,"~t")
//dw_effective_date.uf_set_effective_date(date(ls_date))
//is_input = ls_product + '~t' + ls_plant + '~t' + ls_date 
//wf_retrieve()
//
//
//
////String 					ls_temp,ls_temp1,ls_plant,ls_product,ls_date,ls_inquire
////u_string_functions 	lu_string
////
////ls_temp = message.stringparm
////ls_inquire = lu_string.nf_gettoken(ls_temp,"~t") 
////If ls_inquire = "INQDI" Then
////	ib_no_inquire = True
////	ls_plant = lu_string.nf_gettoken(ls_temp,"~t") 
////	dw_plant.uf_set_plant_code(ls_plant)
////	ls_temp1 =lu_string.nf_gettoken(ls_temp,"~t")
////	ls_product = lu_string.nf_gettoken(ls_temp,"~t")
////	dw_product_code.uf_setproductcode(ls_product)
////	dw_product_code.uf_setproductdesc(lu_string.nf_gettoken(ls_temp,"~t"))
////	ls_date = lu_string.nf_gettoken(ls_temp,"~t")
////	dw_effective_date.uf_set_effective_date(date(ls_date))
////	is_input = ls_product + '~t' + ls_plant + '~t' + ls_date 
////	wf_retrieve()
////End If
end event

event ue_inquire();String 					ls_temp,ls_temp1,ls_plant,ls_product,ls_date,ls_inquire,ls_desc, &
							ls_state,ls_state_desc,ls_status,ls_status_desc
							
u_string_functions 	lu_string

ib_no_inquire = True
iw_parent.event ue_get_data('Plant') 
ls_plant = message.StringParm
dw_plant.uf_set_plant_code(ls_plant)

dw_fab.event ue_retrieve(0,0)

iw_parent.event ue_get_data('Product')
ls_product = message.StringParm
// Product state 10/03/02 ibdkdld
//dw_product_code.uf_setproductcode(ls_product)
dw_fab.uf_set_product_code(ls_product)

iw_parent.event ue_get_data('Product_Descr')
ls_desc =  message.StringParm
// Product state 10/03/02 ibdkdld
//dw_product_code.uf_setproductdesc(ls_desc)
dw_fab.uf_set_product_desc(ls_desc)

// Product state 10/03/02 ibdkdld
iw_parent.event ue_get_data('State')
ls_state =  message.StringParm
dw_fab.uf_set_product_state(ls_state)

iw_parent.event ue_get_data('Status')
ls_status =  message.StringParm
dw_fab.uf_set_product_status(ls_status)


// Product state 10/03/02 ibdkdld
//message.StringParm = ""
//iw_parent.event ue_get_data('State_Desc')
//ls_state_desc =  message.StringParm
//If len(ls_state_desc) > 0 Then // IBDKEEM PA View has no State Description
//	dw_fab.uf_set_product_state_desc(ls_state_desc)
//End If

//message.StringParm = ""
//iw_parent.event ue_get_data('Status_Desc')
//ls_status_desc =  message.StringParm
//If len(ls_status_desc) > 0 Then // PA View has no Status Description
//	dw_fab.uf_set_product_status(ls_status, ls_status_desc)
//End If

iw_parent.event ue_get_data('Date')
ls_date = message.StringParm
dw_effective_date.uf_set_effective_date(date(ls_date))
is_input = ls_product + '~t' + ls_plant + '~t' + ls_date + '~t' + ls_state  + '~t' + ls_status 

wf_retrieve()
end event

public function boolean wf_retrieve ();DataWindowChild	dwc_typesale

Long			ll_row_count, &
				ll_row


String		ls_input, &
				ls_order_detail
iu_pas202 = Create u_pas202
iu_ws_pas5 = Create u_ws_pas5

nvuo_pa_business_rules lu_rules
date ld_parangeDate,ld_inquiredDate,ld_inqDatebackaweek

If Message.ReturnValue <> 0 Then Return False

If Not ib_no_inquire Then
	OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)
// Product state 10/03/02 ibdkdld
//	ls_input  = dw_product_code.GetItemString(1, "fab_product_code") + '~t' 
	ls_input  = dw_fab.uf_get_product_code( ) + '~t' 
	ls_input +=	dw_plant.GetItemString(1, "location_code") + '~t' 
	ls_input +=	String(dw_effective_date.GetItemDate(1, "effective_date"), "yyyy-mm-dd") + '~t' 
// Product state 10/03/02 ibdkdld
	ls_input += dw_fab.uf_get_product_state( ) + '~t'
	ls_input += dw_fab.uf_get_product_status( ) + '~t'	
	ls_input +=	dw_option.GetItemString(1,'plant_option')  
Else
	ib_no_inquire = False
	ib_inquire = True
	ls_input = is_input + '~t' + 'P'
	dw_option.uf_set_plant_option('P')
End If
// begin ibdkdld pa ext date changes 09/20/02
ld_PaRangeDate = RelativeDate ( today(), lu_rules.uf_check_pa_daily_range())
ld_InquiredDate = dw_effective_date.GetItemDate(1, "effective_date")
ld_InqDateBackaWeek = relativedate(dw_effective_date.GetItemDate(1, "effective_date"), -6)

If ld_InquiredDate >  ld_PaRangeDate Then  
	If ld_PaRangeDate < ld_InqDateBackaWeek Then
		st_wkend_message.text = "*** The Detail Information shown is from " + &
			string(ld_InqDateBackaWeek,"mm/dd/yyyy") &
			+ " to " + String(ld_InquiredDate,"mm/dd/yyyy") + " ***" 	
	Else
		st_wkend_message.text = "*** The Detail Information shown is from " + &
			string(ld_PaRangeDate,"mm/dd/yyyy") &
			+ " to " + String(ld_InquiredDate,"mm/dd/yyyy") + " ***"  	
	End IF
Else
	st_wkend_message.text = ""
End if	
// end ibdkdld pa ext date changes 09/20/02




If ib_inquire = False Then Return False
SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_inq_order_detail"
				
//iu_pas202.nf_inq_order_detail(istr_error_info, &
//									 ls_input, &
//									 ls_order_detail)
iu_ws_pas5.nf_pasp19fr(istr_error_info, ls_input, ls_order_detail)
This.dw_order_details.Reset()
//ls_order_detail = "026~tS~tC73660100~tN~t00000400~tB~t00000000~t ~t05/25~t00000400~tB~tN~tWA~tALBERTSON'S, INC.~t168~t05/16~t05/13~r~n"  + &
//						"026~tP~tC73660100~tN~t00000400~tB~t00000000~t ~t05/25~t00000400~tB~tN~tWA~tALBERTSON'S, INC.~t168~t05/16~t05/13~r~n"  + &
//                  "004~tG~tC73660100~tN~t00000400~tB~t00000000~t ~t05/25~t00000400~tB~tN~tWA~tALBERTSON'S, INC.~t168~t05/16~t05/13~r~n"  + &
//                  "004~tR~tC73660100~tN~t00000400~tB~t00000000~t ~t05/25~t00000400~tB~tN~tWA~tALBERTSON'S, INC.~t168~t05/16~t05/13~r~n"


If Not iw_frame.iu_string.nf_IsEmpty(ls_order_detail) Then
		is_detail_string = ls_order_detail
		wf_import_data()  
//	ll_row_count = dw_order_details.ImportString(ls_order_detail)
		dw_order_details.AcceptText()
		ll_row_count = dw_order_details.RowCount()
		iw_frame.SetMicroHelp(string(ll_row_count ) + " Rows Retrieved")
		do
			ll_row = dw_order_details.Find("type_of_order = 'Y'", ll_row + 1, ll_row_count + 1)
			If ll_row <> 0 Then
				dw_order_details.SetItem(ll_row, "type_of_order", 'W')
			End if
		Loop while ll_row <> 0 and ll_row <> ll_row_count
	
Else
	iw_frame.SetMicroHelp("0 Rows Retrieved")
End if
// bug Defect 1291 ibdkdld 11/24/02
if ll_row_count > 0 Then
	// unselect current row because sort and groupcalc will not unselect the currentrow or reset row index
	dw_order_details.SELECTROW(dw_order_details.GETROW(),false)
//dmk set new sort
	If cbx_detail_view.Checked = TRUE then
		dw_order_details.setsort("plant A, sort_fld A, deadline_dt_tm A, line_detail A")
	Else
		dw_order_details.setsort("plant A, sort_fld A, line_detail A")
	End if
	dw_order_details.Sort()
	dw_order_details.groupcalc()
	dw_order_details.ResetUpdate()
	//dw_order_details.SELECTROW(1,TRUE)
End if
This.SetRedraw(True)
dw_order_details.SetFocus()

RETURN True
end function

public function boolean wf_import_data ();Integer			li_rtn, &
					li_temp

Long				ll_row, &
					ll_rtn

String			ls_detail_string, &
					ls_inc_exc_ind, &
					ls_row_descr, &
					ls_mcool_string, &
					ls_temp_string, &
					ls_record_type
					

DataWindowChild	ldwc_temp
ls_temp_string = ''
ls_temp_string = ' ' +  '~r~n' + ' ' + '~r~n' + ' ' +  '~r~n' + ' ' + '~r~n' + ' ' + '~r~n'

//load the inc_exc_plants and mcool_code with blanks to set the drop down size
li_rtn = dw_order_details.GetChild('inc_exc_plt', ldwc_temp)
ldwc_temp.Reset()
ll_rtn = ldwc_temp.ImportString(ls_temp_string)

li_rtn = dw_order_details.GetChild('mcool_code', ldwc_temp)
ldwc_temp.Reset()
ll_rtn = ldwc_temp.ImportString(ls_temp_string)


is_inc_exc_string = ''
is_mcool_string = ''
ls_detail_string = is_detail_string
ll_row = 1


Do While Not iw_frame.iu_string.nf_IsEmpty(ls_detail_string)
	dw_order_details.InsertRow(ll_row)
	dw_order_details.SetItem(ll_row, 'plant', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'plant_des', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'record_type', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'line_detail', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'gpo_ind', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'sched_qty', Long(iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t')))
	dw_order_details.SetItem(ll_row, 'sched_age', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'ship_qty', Long(iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t')))
	dw_order_details.SetItem(ll_row, 'ship_age', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'ship_date', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'order_qty', Long(iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t')))
	dw_order_details.SetItem(ll_row, 'order_age', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'type_of_order', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'customer_state', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'customer_name', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'sales_person_code', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'sched_date', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'order_date', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'inc_exc_ind', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'star_ranch', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	dw_order_details.SetItem(ll_row, 'age_calc_type', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	If cbx_detail_view.Checked = TRUE Then
		dw_order_details.SetItem(ll_row, 'load_status', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
		dw_order_details.SetItem(ll_row, 'load_platform', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
		dw_order_details.SetItem(ll_row, 'credit_status', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
		dw_order_details.SetItem(ll_row, 'micro_test_ind', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
		dw_order_details.SetItem(ll_row, 'deadline_date', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
		dw_order_details.SetItem(ll_row, 'deadline_time', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
		dw_order_details.SetItem(ll_row, 'deadline_dt_tm', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
		dw_order_details.SetItem(ll_row, 'micro_test_ship_type', iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t'))
	Else
		li_temp = 1
		Do While li_temp < 9
			ls_temp_string = iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t')
			li_temp = li_temp + 1
		Loop
	End If
	
	ls_inc_exc_ind = dw_order_details.GetItemString(ll_row, 'inc_exc_ind')
	ls_row_descr = 'Line Detail is ' + &
			dw_order_details.GetItemString(ll_row, 'record_type') + &
			dw_order_details.GetItemString(ll_row, 'line_detail')
			
	is_inc_exc_string = is_inc_exc_string + ls_row_descr + '~r~n' + iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t') + '~t'
	
	ls_mcool_string = iw_frame.iu_string.nf_GetToken(ls_detail_string, '~t') + '~t'
	is_mcool_string = is_mcool_string + ls_row_descr + '~r~n' + ls_mcool_string + '~t'
	If cbx_detail_view.Checked = TRUE Then
		dw_order_details.SetItem(ll_row, 'mcool_code', iw_frame.iu_string.nf_GetToken(ls_mcool_string, '~r~n'))
	End If
		ls_record_type = dw_order_details.GetItemString(ll_row, 'record_type')
		Choose Case ls_record_type
			Case 'T'
				dw_order_details.SetItem(ll_row, 'sort_fld', 1)
			Case 'S'
				dw_order_details.SetItem(ll_row, 'sort_fld', 2)
			Case 'G'
				dw_order_details.SetItem(ll_row, 'sort_fld', 3)
			Case 'R'
				dw_order_details.SetItem(ll_row, 'sort_fld', 4)
			Case 'P'
				dw_order_details.SetItem(ll_row, 'sort_fld', 5)
			Case 'D'
				dw_order_details.SetItem(ll_row, 'sort_fld', 6)
			Case 'M'
				dw_order_details.SetItem(ll_row, 'sort_fld', 7)	
		End Choose

	ll_row = ll_row + 1
	
Loop

return True
end function

on w_pas_order_detail.create
int iCurrent
call super::create
this.cbx_detail_view=create cbx_detail_view
this.dw_fab=create dw_fab
this.st_wkend_message=create st_wkend_message
this.dw_order_details=create dw_order_details
this.dw_effective_date=create dw_effective_date
this.dw_plant=create dw_plant
this.dw_option=create dw_option
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_detail_view
this.Control[iCurrent+2]=this.dw_fab
this.Control[iCurrent+3]=this.st_wkend_message
this.Control[iCurrent+4]=this.dw_order_details
this.Control[iCurrent+5]=this.dw_effective_date
this.Control[iCurrent+6]=this.dw_plant
this.Control[iCurrent+7]=this.dw_option
end on

on w_pas_order_detail.destroy
call super::destroy
destroy(this.cbx_detail_view)
destroy(this.dw_fab)
destroy(this.st_wkend_message)
destroy(this.dw_order_details)
destroy(this.dw_effective_date)
destroy(this.dw_plant)
destroy(this.dw_option)
end on

event ue_postopen;call super::ue_postopen;u_pas202		lu_pas202
s_error		lstr_error_info

DataWindowChild		ldwc_state

iu_ws_pas5 = Create u_ws_pas5
lstr_error_info.se_User_ID = sqlca.userid
lstr_error_info.se_window_name = "OrderDet"

is_inquire_window_name = 'w_pas_order_detail_inq'

lu_pas202 = Create u_pas202

If isvalid(iw_parent) Then
	If iw_parent.title = "PA View" Then
		iw_parent.Event ue_set_detail_handle(This)
		This.event ue_inquire()
	Else
		wf_retrieve()
	End If
Else
	wf_retrieve()
End If

dw_order_details.GetChild('load_platform', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("LOADINST")
//
dw_order_details.object.datawindow.hidegrayline = true
ib_no_inquire = False

end event

event activate;call super::activate;
iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')


//If isvalid(iw_parent) Then
//	If iw_parent.title = "Detail Information" Then
//		This.event ue_inquire()
//	End If
//End If
//
end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;String 					ls_temp,ls_temp1,ls_plant,ls_product,ls_date
u_string_functions 	lu_string

iw_parent = Message.PowerObjectParm

This.Title = 'Detail Information'
dw_effective_date.uf_enable(false)
// Product state 10/03/02 ibdkdld
//dw_product_code.uf_disable()
dw_fab.uf_enable( false)
dw_effective_date.uf_initilize('Ship Date:','PA')
dw_effective_date.uf_set_effective_date(Today())
dw_option.uf_enable(false)

ls_temp = ProfileString( iw_frame.is_UserINI, "Pas", "PAViewDetailView", "FALSE")

If ls_temp = "TRUE" Then
	cbx_detail_view.Checked = TRUE
	dw_order_details.dataobject = "d_order_detail_det_view"
Else
	cbx_detail_view.Checked = FALSE
	dw_order_details.dataobject = "d_order_detail"
End If


end event

event ue_get_data;call super::ue_get_data;Long			ll_window_x, &
				ll_window_y, &
				ll_datawindow_x, &
				ll_datawindow_y
				
string		ls_temp				

Choose Case as_value
	Case 'data'
		message.stringparm = is_data
	Case 'xy_values'
		ll_window_x = This.PointerX()
		ll_window_y = This.PointerY()
		ll_datawindow_x = dw_order_details.PointerX() + &
				Long(dw_order_details.object.sales_person_code.width)
		ll_datawindow_y = dw_order_details.PointerY()
		message.StringParm = String(ll_datawindow_x + (ll_window_x - ll_window_x)) + '~t' + &
				String(ll_datawindow_y + (ll_window_y - ll_datawindow_y)) + '~t'
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'effective_date'
		message.StringParm = String(dw_effective_date.uf_get_effective_date(), 'YYYY-MM-dd')
	Case 'plant_option'
		message.StringParm = dw_option.uf_get_plant_option()
	Case 'product' , 'fab_product'
// Product state 10/03/02 ibdkdld
//		message.StringParm = dw_product_code.uf_getproductcode()
		message.StringParm = dw_fab.uf_get_product_code()
// Product state 10/03/02 ibdkdld
	Case 'product_desc'
		message.StringParm = dw_fab.uf_get_product_desc()
// Product state 10/03/02 ibdkdld
	case 'state' , 'fab_product_state' 
		message.StringParm = dw_fab.uf_get_product_State()
	case 'status' , 'fab_product_status' 
		message.StringParm = dw_fab.uf_get_product_Status()		
// Product state 10/03/02 ibdkdld
	case 'state_desc'
		message.StringParm = dw_fab.uf_get_product_State_desc()
	case 'status_desc'
		message.StringParm = dw_fab.uf_get_product_Status_desc()		
	//** IBDKEEM ** 08/26/2002 ** Added Product State ** MRFREEZE Code until next release
// Product state 10/03/02 ibdkdld
//	Case 'fab_product'
//		//message.StringParm = dw_fab_product_code.uf_get_product_code()
//		ls_temp = trim(dw_product_code.uf_getproductcode())
//		
//		If right(ls_temp ,1) = "*" then
//			message.StringParm = left(ls_temp, pos(ls_temp, "*") - 1)
//		else		
//			message.StringParm = ls_temp
//		end if
//		
//	Case 'fab_product_state'
//		//message.StringParm = dw_fab_product_code.uf_get_product_state()
//		If right(trim(dw_product_code.uf_getproductcode()),1) = "*" then
//			message.StringParm = "2"
//		else
//			message.StringParm = "1"
//		end if

	Case 'sched_qty'
		message.StringParm = string(dw_order_details.GetItemNumber(dw_order_details.GetRow ( ), "sched_qty"))
				
	//** END IBDKEEM ** 08/26/2002
	///dave
	case 'SHIP PLANT'		
		message.StringParm = dw_order_details.GetItemString(dw_order_details.GetRow ( ), "plant")
	Case 'SHIP PERIOD'
		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
		message.StringParm = Mid (ls_temp, 18 ,1)
	Case 'DELV PLANT'
		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
		message.StringParm = Mid (ls_temp,13 ,3)
	Case 'DELV DATE'
		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
		message.StringParm = String(DATE(left(ls_temp,10)), "yyyy-mm-dd")
	Case 'DELV PERIOD'
		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
		message.StringParm = Mid (ls_temp, 21 ,1)
End choose

end event

event ue_set_data;call super::ue_set_data;String	ls_temp,ls_res,ls_gpo,ls_month,ls_day,ls_year,ls_del_date,ls_del_plant,ls_del_period,ls_ship_period, &
			ls_order_num,ls_line_num, &
			ls_plant, &
			ls_sched_date, &
			ls_state, &
			ls_status, &
			ls_product, &
			ls_params

Date		ldt_temp
			
Window	lw_temp
Int		li_rtn
Long		ll_row



Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date(Date(as_value ))
	Case 'product'
// Product state 10/03/02 ibdkdld
	//dw_product_code.uf_setproductcode(as_value)
	dw_fab.uf_set_product_code( as_value)
// Product state 10/03/02 ibdkdld
	Case 'state'
	dw_fab.uf_set_product_state( as_value)
// Product state 10/03/02 ibdkdld
	Case 'state_desc'
	dw_fab.uf_set_product_state_desc( as_value)
	
	Case 'status'
	dw_fab.uf_set_product_status( as_value)
	Case 'status_desc'
	dw_fab.uf_set_product_status_desc( as_value)	
	
// Product state 10/03/02 ibdkdld
	Case 'product_desc'
	dw_fab.uf_set_product_desc( as_value)
	Case 'plant_option'
		dw_option.uf_set_plant_option(as_value)
	Case 'change order'
		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
		ls_order_num = left(ls_temp,5)
		ls_line_num = Mid ( ls_temp, 8 , 4)
		ls_temp = ls_order_num + '~t' + ls_line_num
		OpenSheetWithParm(lw_temp, ls_temp, "w_change_order_production_dates_new",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
	Case 'plan tran'
//		IF IsValid(iw_plan_transfer) THEN 
//			if NOT iw_plan_transfer.ib_pooled &
//	 		 and iw_plan_transfer.it_Instance_date = it_PT_Instance_date then
//				iw_plan_transfer.TriggerEvent ('ue_inquire') 
//				iw_plan_transfer.setFocus()
//			else
//				iw_plan_transfer = gw_netwise_frame.iu_window_pool.uf_get_pooled_window( "w_planned_transfer", This)
//			end If
//		else
//			//OpenSheetWithParm(lw_temp, This, "w_planned_transfer",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
//			iw_plan_transfer = gw_netwise_frame.iu_window_pool.uf_get_pooled_window( "w_planned_transfer", This)
//		end If
//		
//		it_PT_Instance_date = iw_plan_transfer.it_Instance_date

		ls_params = dw_order_details.GetItemString(dw_order_details.GetRow(), "plant") + '~t'
		ls_params += String(dw_effective_date.GetItemDate(1, "effective_date"), "mm/dd/yyyy")  + '~t'  
		ls_params += mid(dw_order_details.GetItemString(dw_order_details.GetRow(),"line_detail"),18,1) + '~t'  
		ls_params += mid(dw_order_details.GetItemString(dw_order_details.GetRow(),"line_detail"),13,3) + '~t'  
		ls_params += String(date(mid(dw_order_details.GetItemString(dw_order_details.GetRow(),"line_detail"),1,10)), "mm/dd/yyyy") + '~t' 
		ls_params += mid(dw_order_details.GetItemString(dw_order_details.GetRow(),"line_detail"),21,1) + '~t' 
		ls_params += dw_fab.GetItemString(1, "fab_product_code") + '~t' 
		
		If isValid(iw_planned_transfer_new) then
			iw_planned_transfer_new.SetFocus()
			iw_planned_transfer_new.wf_pass_params(ls_params)
		Else	
			OpenSheetWithParm ( iw_planned_transfer_new, ls_params, "w_planned_transfer_new", iw_frame	, 0, iw_frame.im_menu.iao_arrangeopen )
		End If

	
	Case 'res move'
		ll_row = dw_order_details.GetRow ( )
		ls_temp = dw_order_details.GetItemString (ll_row, 'line_detail')
		ls_res = left(ls_temp,7) + '~t'
		ls_gpo = Mid ( ls_temp, 10 , 6)
		ls_month = right(ls_gpo,2) + '/'
		ls_day = '01/' 
		ls_year = left(ls_gpo,4)
		ls_temp = ls_res + ls_month + ls_day + ls_year + '~t' + 'S'
		OpenSheetWithParm(lw_temp, ls_temp, "w_reservation_review_scheduling",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
	Case 'sched det'
		ls_plant = dw_plant.GetItemString(1, "location_code")
		ls_state = dw_fab.uf_get_product_State()
	//1-13 jac
	//	ls_status = dw_fab.uf_get_product_State()	
		ls_status = dw_fab.uf_get_product_Status()
	//
		ls_product = dw_fab.uf_get_product_code()
		ls_temp 	= ""
	//	ls_temp 	=	dw_plant.GetItemString(1, "location_code") + '~t' 
		ls_temp +=	ls_plant + '~t'
		ls_temp +=	ls_product + '~t' 
		ls_temp +=	ls_state + '~t' 
		ls_temp +=	ls_status + '~t' 
		ls_temp +=	String(dw_effective_date.uf_get_effective_date(), 'YYYY-MM-dd')+ '~t'
	//	ll_row = dw_order_details.GetRow ( )
	//	ls_temp = dw_order_details.GetItemString (ll_row, 'line_detail')
	//	ls_plant = dw_plant.GetItemString(1, "location_code")
	//	ls_sched_date = String(dw_effective_date.uf_get_effective_date(), 'YYYY-MM-dd')
	
	//	ls_product = dw_fab.uf_get_product_code()
		OpenSheetWithParm(lw_temp, ls_temp, "w_raw_mat_det_sched",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)	
End Choose


end event

event resize;call super::resize;If newheight > dw_order_details.y then
	dw_order_details.height = newheight - dw_order_details.y
end if

If newwidth > dw_order_details.x then
	dw_order_details.width = newwidth - dw_order_details.x
end if
end event

event close;call super::close;Destroy u_ws_pas5
end event

type cbx_detail_view from checkbox within w_pas_order_detail
integer x = 2944
integer y = 148
integer width = 407
integer height = 84
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Detailed View"
end type

event clicked;DataWindowChild		ldwc_state

If cbx_detail_view.Checked = TRUE Then
	dw_order_details.dataobject = "d_order_detail_det_view"
	SetProfileString( iw_frame.is_UserINI, "Pas", "PAViewDetailView", "TRUE")
Else
	dw_order_details.dataobject = "d_order_detail"
	SetProfileString( iw_frame.is_UserINI, "Pas", "PAViewDetailView", "FALSE")
End If

wf_import_data()

If cbx_detail_view.Checked = TRUE then
	dw_order_details.setsort("plant A, sort_fld A, deadline_dt_tm A, line_detail A")
	dw_order_details.GetChild('load_platform', ldwc_state)
	ldwc_state.SetTransObject(SQLCA)
	ldwc_state.Retrieve("LOADINST")
Else
	dw_order_details.setsort("plant A, sort_fld A, line_detail A")
End if
dw_order_details.SELECTROW(dw_order_details.GETROW(),false)
dw_order_details.Sort()
dw_order_details.groupcalc()
dw_order_details.ResetUpdate()
end event

type dw_fab from u_fab_product_code within w_pas_order_detail
integer width = 1536
integer height = 288
integer taborder = 0
end type

type st_wkend_message from statictext within w_pas_order_detail
integer x = 5
integer y = 300
integer width = 2048
integer height = 76
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type dw_order_details from u_base_dw_ext within w_pas_order_detail
integer x = 9
integer y = 404
integer width = 4059
integer height = 1108
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_order_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean resizable = true
boolean border = false
boolean livescroll = true
string is_selection = "1"
end type

event constructor;call super::constructor;DataWindowChild		ldwc_state

ib_updateable = False

//
//This.GetChild('load_platform', ldwc_state)
//ldwc_state.SetTransObject(SQLCA)
//ldwc_state.Retrieve("LOADINST")

end event

event ue_mousemove;call super::ue_mousemove;String			ls_name, &
					ls_code, &
					ls_type

Choose Case dwo.name
	Case 'sales_person_code'
			If il_LastRow = row Then return
		  ls_code = This.GetItemString(row, 'sales_person_code')
		  
		  is_data = ""
  			SELECT salesman.smanname  
		    INTO :is_data  
		    FROM salesman  
		   WHERE ( salesman.smancode = :ls_code ) AND  
				         ( salesman.smantype = 'S' )   ;

		If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
		This.SetFocus()
		il_LastRow = row
		If iw_frame.iu_string.nf_IsEmpty(is_data) Then Return
		OpenWithParm(w_order_detail_tooltip, Parent, Parent)
	//dmk SR5032
	Case 'micro_test_ind'
			is_data = ""
			is_data = 'Micro Test Indicator'
			OpenWithParm(w_order_detail_tooltip, Parent, Parent)
	//SLH SR25459
	Case 'micro_test_ship_type'
		    if il_lastrow = row Then return
			ls_type = This.GetItemString(row, 'micro_test_ship_type')
			is_data = ""
			
			SELECT type_desc
			INTO :is_data
			FROM tutltypes
			WHERE record_type = 'MICROTYP'
			    AND type_code = :ls_type;			
			
			If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
		    This.SetFocus()
     		il_LastRow = row
		    If iw_frame.iu_string.nf_IsEmpty(is_data) Then Return
     		OpenWithParm(w_order_detail_tooltip, Parent, Parent)			
	Case Else
		il_LastRow = 0
		If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
End Choose

end event

event rbuttondown;call super::rbuttondown;m_detail_information	lm_popup
String ls_temp
integer li_rtn

IF row > 0 Then
	dw_order_details.SetRow ( row )
	dw_order_details.SelectRow ( row, True )
	lm_popup = Create m_detail_information
	ls_temp = This.GetItemString ( row, "record_type")
	CHOOSE CASE ls_temp
	CASE 'S'
		If iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_change_order_production_dates_new") Then
			lm_popup.m_manifestdetail.m_changeorderproductiondates.Enable()
		Else
			lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
		End If
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		lm_popup.m_manifestdetail.m_scheduledetail.Disable()
	CASE 'T'
		If iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_change_order_production_dates_new") Then
			lm_popup.m_manifestdetail.m_changeorderproductiondates.Enable()
		Else
			lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
		End If
		
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		lm_popup.m_manifestdetail.m_scheduledetail.Disable()
	CASE 'R'
		If iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_reservation_review_scheduling") Then
		   lm_popup.m_manifestdetail.m_reservationmovement.Enable()
		Else
	   	lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		End If
		
		lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
		lm_popup.m_manifestdetail.m_scheduledetail.Disable()
	CASE 'P'
		If iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_planned_transfer") Then
			lm_popup.m_manifestdetail.m_plannedtransfer.Enable()
		Else
			lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		End If
		lm_popup.m_manifestdetail.m_scheduledetail.Disable()
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
	CASE 'M'
		If iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_raw_mat_sched_det") Then
			lm_popup.m_manifestdetail.m_scheduledetail.Enable()
		Else
			lm_popup.m_manifestdetail.m_scheduledetail.Disable()
		End If
		lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()	
	CASE Else
		lm_popup.m_manifestdetail.m_reservationmovement.Disable()
		lm_popup.m_manifestdetail.m_plannedtransfer.Disable()
		lm_popup.m_manifestdetail.m_changeorderproductiondates.Disable()
		lm_popup.m_manifestdetail.m_scheduledetail.Disable()
	END CHOOSE
	lm_popup.m_manifestdetail.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())
	Destroy lm_popup
End If


end event

event clicked;call super::clicked;Integer	li_rtn, &
			li_start_pos

Long		ll_row, & 
			ll_find, &
			ll_rtn

String	ls_ColumnName, &
			ls_row_descr, &
			ls_temp_string, &
			ls_inc_exc_string, &
			ls_mcool_string
			
DataWindowChild	ldwc_temp


dw_order_details.SetRedraw(False)
ll_row = This.GetRow()

IF dwo.Type = "column" THEN
	ls_ColumnName = dwo.Name
END IF

If ls_ColumnName = "inc_exc_plt" or &
	ls_Columnname = "mcool_code" Then
//set the description of the selected line to find in the data string	
	ls_row_descr = 'Line Detail is ' + &
			dw_order_details.GetItemString(ll_row, 'record_type') + &
			dw_order_details.GetItemString(ll_row, 'line_detail')
//Set the temp string to start where the selected row is located
	If ls_ColumnName = "inc_exc_plt" Then
		li_start_pos = Pos(is_inc_exc_string, ls_row_descr, 1)
		ls_temp_string = Mid(is_inc_exc_string,li_start_pos)
		
		ls_inc_exc_string = iw_frame.iu_string.nf_GetToken(ls_temp_string, '~r~n')
		li_rtn = dw_order_details.GetChild('inc_exc_plt', ldwc_temp)
		ldwc_temp.Reset()
//String in the list of plants for the selected row. 
		ls_inc_exc_string = iw_frame.iu_string.nf_GetToken(ls_temp_string, '~t')
		ll_rtn = ldwc_temp.ImportString(ls_inc_exc_string)
	Else
//first
		li_start_pos = Pos(is_mcool_string, ls_row_descr, 1)
		ls_temp_string = Mid(is_mcool_string,li_start_pos)
		
		ls_mcool_string = iw_frame.iu_string.nf_GetToken(ls_temp_string, '~r~n')
		li_rtn = dw_order_details.GetChild('mcool_code', ldwc_temp)
		ldwc_temp.Reset()
//String in the list of plants for the selected row. 
		ls_mcool_string = iw_frame.iu_string.nf_GetToken(ls_temp_string, '~t')
		ll_rtn = ldwc_temp.ImportString(ls_mcool_string)
	End If
End If


dw_order_details.SetRedraw(True)
dw_order_details.AcceptText()
end event

type dw_effective_date from u_effective_date within w_pas_order_detail
integer x = 1550
integer y = 12
integer width = 832
integer taborder = 10
boolean bringtotop = true
end type

type dw_plant from u_plant within w_pas_order_detail
boolean visible = false
integer x = 2208
integer y = 12
integer height = 92
integer taborder = 30
boolean bringtotop = true
end type

type dw_option from u_plant_opt within w_pas_order_detail
integer x = 2423
integer taborder = 20
boolean bringtotop = true
boolean border = false
end type

