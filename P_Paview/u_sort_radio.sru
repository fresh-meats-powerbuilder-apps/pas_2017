HA$PBExportHeader$u_sort_radio.sru
$PBExportComments$ibdkdld
forward
global type u_sort_radio from u_base_dw_ext
end type
end forward

global type u_sort_radio from u_base_dw_ext
int Width=594
int Height=216
string DataObject="d_sort_radio"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type
global u_sort_radio u_sort_radio

forward prototypes
public subroutine uf_enable (boolean ab_enable)
public function string uf_get_choice ()
public subroutine uf_set_choice (string as_choice)
public subroutine uf_set_box_text (string as_title)
end prototypes

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("choice.Protect = 0 " + &
				"choice.Pointer = 'Arrow!'")
	Case False
		This.Modify("choice.Protect = 1 " + &
				"choice.Pointer = 'Beam!'")
		This.Ib_updateable = False		
END CHOOSE

end subroutine

public function string uf_get_choice ();return This.GetItemString(1, "choice")
end function

public subroutine uf_set_choice (string as_choice);This.SetItem(1,"choice",as_choice)
end subroutine

public subroutine uf_set_box_text (string as_title);this.object.display.text = as_title
end subroutine

event constructor;call super::constructor;This.InsertRow(0)
end event

