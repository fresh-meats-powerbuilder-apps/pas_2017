HA$PBExportHeader$w_planned_transfer_range.srw
$PBExportComments$Planned Transfer Range. For Inquiring from TPASPLTR for a given range of values.
forward
global type w_planned_transfer_range from w_netwise_sheet_ole
end type
type ole_planned_transfer_range from olecustomcontrol within w_planned_transfer_range
end type
end forward

global type w_planned_transfer_range from w_netwise_sheet_ole
integer x = 5
integer y = 4
integer width = 2949
integer height = 1536
string title = "Planned Transfer Range"
long backcolor = 79741120
event ue_plantran_handle ( window aw_plan_tran )
ole_planned_transfer_range ole_planned_transfer_range
end type
global w_planned_transfer_range w_planned_transfer_range

type variables
s_error					istr_error_info
string					is_inquire_parm,&
							is_plan_parm
boolean					ib_OLE_Error	

w_planned_transfer	iw_plan_transfer 
time						it_PT_Instance_date
end variables

forward prototypes
public subroutine wf_filenew ()
public function boolean wf_retrieve ()
private subroutine wf_planned_transfer (string ls_inquire_parm, boolean ls_new)
end prototypes

event ue_plantran_handle;iw_plan_transfer = aw_plan_tran

end event

public subroutine wf_filenew ();wf_planned_transfer("",True)
end subroutine

public function boolean wf_retrieve ();boolean lbln_Return

if ib_OLE_Error then return true

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "ole_planned_transfer_range.Inquire"

SetPointer(HourGlass!)
If Len(is_inquire_parm) > 0 Then
/* Is_****_**** can then be broken down into the appropriate Variables to Call the ocx controls(in this example all I needed was a Order Number) */
	lbln_Return = ole_planned_transfer_range.object.Inquire(is_inquire_parm)
/* always make sure you clear the Instance Variable for future inquires */
	is_inquire_parm = ''                                                                        
else	
	lbln_Return = ole_planned_transfer_range.object.Inquire(" ")
End if

SetPointer(Arrow!)

return lbln_Return
end function

private subroutine wf_planned_transfer (string ls_inquire_parm, boolean ls_new);Window	lw_temp

is_plan_parm = ls_inquire_parm
	
IF IsValid(iw_plan_transfer) THEN 
	if NOT iw_plan_transfer.ib_pooled &
	 and iw_plan_transfer.it_Instance_date = it_PT_Instance_date then
		If ls_new = True Then 
			iw_plan_transfer.setFocus()
			iw_plan_transfer.TriggerEvent ('ue_new1') 
		Else
			iw_plan_transfer.setFocus()
			iw_plan_transfer.TriggerEvent ('ue_inquire1') 
		End If
	else
		iw_plan_transfer = gw_netwise_frame.iu_window_pool.uf_get_pooled_window( "w_planned_transfer", This)				
	end if
else
	//OpenSheetWithParm (lw_temp,This,"w_planned_transfer",iw_frame, 0, iw_frame.im_menu.iao_ArrangeOpen)
	iw_plan_transfer = gw_netwise_frame.iu_window_pool.uf_get_pooled_window( "w_planned_transfer", This)
end If

it_PT_Instance_date = iw_plan_transfer.it_Instance_date
end subroutine

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_save')

//iw_frame.im_menu.mf_Disable('m_print')
//iw_frame.im_menu.mf_Disable('m_nonvisprint')

iw_frame.im_menu.mf_Disable('m_clearwindow')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	


end event

event deactivate;iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_save')

//iw_frame.im_menu.mf_Enable('m_print')
//iw_frame.im_menu.mf_Enable('m_nonvisprint')

iw_frame.im_menu.mf_Enable('m_sort')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	

end event

event ue_postopen;call super::ue_postopen;string	ls_server_suffix
integer 	li_rtn

if ib_OLE_Error then return 

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_planned_transfer_range"
istr_error_info.se_user_id 		= sqlca.userid

ls_server_suffix = ProfileString ("Ibp002.ini", "Netwise Server Info", "ServerSuffix", "p" ) 

if ls_server_suffix = "" then
	ls_server_suffix = " "
end if 

ole_planned_transfer_range.object.initializecontrol(sqlca.userid,sqlca.DBpass,ls_server_suffix,Message.nf_Get_App_ID(),"Planned Transfer Range") 
ole_planned_Transfer_range.object.PlannedTransferRights =  iw_frame.iu_netwise_data.nf_IsWindowAccessable("w_planned_transfer") 

if this.width  > 10 then
	ole_planned_Transfer_range.Width = this.width - 50
end if

if this.height > 10 then
	ole_planned_Transfer_range.Height = this.height - 150
end if

If Len(is_inquire_parm) > 0 Then
	wf_retrieve()
End if

end event

on w_planned_transfer_range.create
int iCurrent
call super::create
this.ole_planned_transfer_range=create ole_planned_transfer_range
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole_planned_transfer_range
end on

on w_planned_transfer_range.destroy
call super::destroy
destroy(this.ole_planned_transfer_range)
end on

event resize;ole_planned_transfer_range.Width = This.Width - 50
ole_planned_transfer_range.Height = This.Height - 100

end event

event open;call super::open;String ls_temp 

ls_temp = Message.StringParm	

If Len(ls_temp) > 0 and lower(ls_temp) <> 'w_planned_transfer_range' Then
	is_inquire_parm = ls_temp
End If

end event

event ue_get_data;Choose Case as_value
	case 'Inquire'		
		message.StringParm = is_plan_parm
//	Case 'SHIP PERIOD'
//		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
//		message.StringParm = Mid (ls_temp, 18 ,1)
//	Case 'DELV PLANT'
//		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
//		message.StringParm = Mid (ls_temp,13 ,3)
//	Case 'DELV DATE'
//		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
//		message.StringParm = String(DATE(left(ls_temp,10)), "yyyy-mm-dd")
//	Case 'DELV PERIOD'
//		ls_temp = dw_order_details.GetItemString (dw_order_details.GetRow ( ), 'line_detail')
//		message.StringParm = Mid (ls_temp, 21 ,1)
End choose
end event

event ue_fileprint;call super::ue_fileprint;ole_planned_transfer_range.object.PrintReport()
end event

type ole_planned_transfer_range from olecustomcontrol within w_planned_transfer_range
event messageposted ( ref string strinfomessage )
event transferselected ( ref string strselection )
integer width = 2880
integer height = 1408
integer taborder = 30
boolean bringtotop = true
boolean border = false
long backcolor = 79741120
boolean focusrectangle = false
string binarykey = "w_planned_transfer_range.win"
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
end type

event messageposted;iw_frame.SetMicroHelp(strinfomessage)

end event

event transferselected;wf_planned_transfer(strselection,False)
	
end event

event getfocus;if ib_OLE_Error then return
ole_planned_transfer_range.object.SetFocusBack()
end event

event externalexception;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

lstr_rpc_error_info.se_app_name = "PAS"
lstr_rpc_error_info.se_window_name = ""
lstr_rpc_error_info.se_function_name = ""
lstr_rpc_error_info.se_event_name = parent.title
lstr_rpc_error_info.se_procedure_name = source
lstr_rpc_error_info.se_user_id = ""
lstr_rpc_error_info.se_return_code = ""
lstr_rpc_error_info.se_message = "[" + string(resultcode) + "] "  + "[" + string(exceptioncode) + "] "  + description + "~nThe window will be closed."

lstr_rpc_error_info.se_rval = 0
lstr_rpc_error_info.se_commerror = 0
lstr_rpc_error_info.se_commerrmsg = ""
lstr_rpc_error_info.se_neterror = 0
lstr_rpc_error_info.se_primaryerror = 0
lstr_rpc_error_info.se_secondaryerror = 0
lstr_rpc_error_info.se_neterrmsg = space(100)

openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")

action = ExceptionIgnore! 
close(parent)
end event

event error;window					lw_RPCError
s_rpc_error 			lstr_rpc_error_info

ib_OLE_Error = true

if errornumber <> 20535 then
	lstr_rpc_error_info.se_app_name = "PAS"
	lstr_rpc_error_info.se_window_name = ""
	lstr_rpc_error_info.se_function_name = ""
	lstr_rpc_error_info.se_event_name = errorwindowmenu
	lstr_rpc_error_info.se_procedure_name = errorobject
	lstr_rpc_error_info.se_user_id = ""
	lstr_rpc_error_info.se_return_code = ""
	lstr_rpc_error_info.se_message = "[" + string(errornumber) + "] "  + errortext + "~nThe window will be closed."
	
	lstr_rpc_error_info.se_rval = 0
	lstr_rpc_error_info.se_commerror = 0
	lstr_rpc_error_info.se_commerrmsg = ""
	lstr_rpc_error_info.se_neterror = 0
	lstr_rpc_error_info.se_primaryerror = 0
	lstr_rpc_error_info.se_secondaryerror = 0
	lstr_rpc_error_info.se_neterrmsg = space(100)
	
	openwithparm(lw_RPCError, lstr_rpc_error_info, "w_rpc_error")
end if

action = ExceptionIgnore! 
close(parent)
end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
0Fw_planned_transfer_range.bin 
2500000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000300000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffd00000002fffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000300000000000000000000000000000000000000000000000000000000f9ff040001c3b06900000004000000800000000000500003004c004200430049004e0045004500530045004b000000590000000000000000000000000000000000000000000000000000000000000000000000000002001cffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe000000000000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000002001affffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000fffffffe0000000000000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000101001a000000020000000100000004b33f8f1911d4512801007b9fdb90280200000000f9ff040001c3b069f9ff040001c3b069000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000048000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffff
25ffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000b29300000048000800034757f20b000000200065005f00740078006e0065007800740000411d000800034757f20affffffe00065005f00740078006e0065007900740000246100620070005f006d007700640072006e00730065007a00690000006500620070005f006d007700640072006e007400650069007200760065006500650064006e00700000006d00620064005f006e007700650072007200740065006900650076006f00720000007700620070005f006d007700640072006e00740065006900720076006500730065006100740074007200700000006d00620064005f006e0077006f007200630077006100680067006e0000006500620070005f006d007700640072006e0077006f00680063006e0061006900670067006e00700000006d00620064005f006e007700650073006600740063006f0073007500700000006d00620064005f006e0077007100730000006c00620070005f006d007700640074006e00620061006f0064006e00770075006f0000007400620070005f006d007700640074006e006200610075006f0000007400620070005f006d007700640074006e00620061007000750075006f0000007400620070005f006d007700640075006e0064007000740061006500650064006e00700000006d00620064005f006e007700700075006100640065007400740073007200610000007400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
1Fw_planned_transfer_range.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
