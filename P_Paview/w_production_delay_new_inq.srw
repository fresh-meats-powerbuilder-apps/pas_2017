HA$PBExportHeader$w_production_delay_new_inq.srw
forward
global type w_production_delay_new_inq from w_base_response_ext
end type
type dw_1 from u_division within w_production_delay_new_inq
end type
end forward

global type w_production_delay_new_inq from w_base_response_ext
integer x = 1075
integer y = 485
integer width = 1952
integer height = 520
long backcolor = 67108864
dw_1 dw_1
end type
global w_production_delay_new_inq w_production_delay_new_inq

type variables
String is_division, &
		 is_division_code, &
		 is_division_desc
end variables

on w_production_delay_new_inq.create
int iCurrent
call super::create
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on w_production_delay_new_inq.destroy
call super::destroy
destroy(this.dw_1)
end on

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

event ue_postopen;call super::ue_postopen;//String ls_division
////If dw_1.RowCount() = 0 Then dw_1.InsertRow(0)
//
//If dw_1.AcceptText() = -1 Then return
//ls_division = dw_1.uf_Get_Division()

//iw_parentwindow.Event ue_get_data('division')
//dw_1.uf_set_division(Message.StringParm)

//is_division_code = dw_1.getitemString(1, "division_code")
//is_division_desc = dw_1.getitemstring(1, "division_description")
//is_division = is_division_code + '>' + is_division_desc
//iw_parentwindow.Event ue_set_data('close', "true")


//dw_1.SetFocus()

end event

event ue_base_cancel;call super::ue_base_cancel;

Close(This)
end event

event ue_base_ok;call super::ue_base_ok;String		ls_division, &
			ls_division_desc
				

If dw_1.AcceptText() = -1 Then return

is_division_code = dw_1.uf_get_division()

is_division_desc = dw_1.uf_get_division_descr()

If iw_frame.iu_string.nf_IsEmpty(is_division_code) Then
	iw_frame.SetMicroHelp("Division is a required field")
	dw_1.SetFocus()
	return
End If

iw_parentwindow.Event ue_set_data('division',is_division_code)
is_division_code = dw_1.getitemString(1, "division_code")
is_division_desc = dw_1.getitemstring(1, "division_description")
is_division = is_division_code + '>' + is_division_desc
iw_parentwindow.Event ue_set_data('close', "true")

ib_ok_to_close = True

Close(This)

end event

event close;call super::close;closewithreturn(this, is_division)
//closewithreturn(this, is_division_desc)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_production_delay_new_inq
integer x = 1957
integer y = 236
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_production_delay_new_inq
integer x = 1349
integer y = 288
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_production_delay_new_inq
integer x = 910
integer y = 288
end type

type dw_1 from u_division within w_production_delay_new_inq
integer x = 105
integer y = 88
integer taborder = 10
boolean bringtotop = true
end type

