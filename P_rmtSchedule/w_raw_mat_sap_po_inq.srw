HA$PBExportHeader$w_raw_mat_sap_po_inq.srw
forward
global type w_raw_mat_sap_po_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_raw_mat_sap_po_inq
end type
type dw_from_date from u_sched_date within w_raw_mat_sap_po_inq
end type
type delivery_t from statictext within w_raw_mat_sap_po_inq
end type
type from_date_t from statictext within w_raw_mat_sap_po_inq
end type
type dw_to_date from u_sched_date within w_raw_mat_sap_po_inq
end type
type to_date_t from statictext within w_raw_mat_sap_po_inq
end type
type rb_changed_po from radiobutton within w_raw_mat_sap_po_inq
end type
type rb_all_po from radiobutton within w_raw_mat_sap_po_inq
end type
type gb_po_option from groupbox within w_raw_mat_sap_po_inq
end type
end forward

global type w_raw_mat_sap_po_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 1691
integer height = 1080
string title = "Inquire SAP Purchase Orders "
long backcolor = 67108864
event begindatechanged ( )
dw_plant dw_plant
dw_from_date dw_from_date
delivery_t delivery_t
from_date_t from_date_t
dw_to_date dw_to_date
to_date_t to_date_t
rb_changed_po rb_changed_po
rb_all_po rb_all_po
gb_po_option gb_po_option
end type
global w_raw_mat_sap_po_inq w_raw_mat_sap_po_inq

type variables
Boolean		ib_valid_return
w_base_sheet	iw_parent

w_raw_mat_det_sched 	iw_parent_window
end variables

event ue_postopen;call super::ue_postopen;Date							ldt_temp

Int							li_pos, &
								li_ret

String						ls_header, &
								ls_temp, &
								ls_option, &
								ls_date, &
								ls_future_days

u_string_functions		lu_string

Window   lw_parent

lw_parent = this.parentwindow()
This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)

iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If


iw_parent.Event ue_get_data('from_date')
ls_date = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_date) Then
	dw_from_Date.uf_set_sched_date(date(ls_date))
Else
	dw_from_Date.SetItem(1, "sched_date", Today())
End If

iw_parent.Event ue_get_data('to_date')
ls_date = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_date) Then
	dw_to_Date.uf_set_sched_date(date(ls_date))
Else
	CONNECT USING SQLCA;	
			SELECT CONVERT(VARCHAR, DATEADD (DAY, CONVERT(INT,type_short_desc), GETDATE() ), 101)
			INTO :ls_date
			FROM dbo.tutltypes
			WHERE record_type = 'PA RANGE'
			AND TYPE_CODE = 'FUTURE';
		
	If SQLCA.SQLCode = 00 THEN
		dw_to_Date.SetItem(1, "sched_date", date(ls_date))
	END IF
End If

iw_parent.Event ue_get_data('po_option')
ls_option = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_option) Then	
	If ls_option = 'A' Then
		rb_all_po.checked = True
	Else
		rb_changed_po.Checked = True
	End If
Else
	rb_changed_po.Checked = True
End If
	

If dw_plant.RowCount() < 1 Then 
	dw_plant.InsertRow(0)
//ElseIf Not lu_string.nf_IsEmpty(dw_plant.GetItemString(1,1)) Then
//	dw_product.setfocus( )
End if

Message.StringParm = ''
end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

//if dw_product.rowcount( ) = 0 Then
//	dw_product.insertrow( 0)
//End If

if dw_from_Date.rowcount( ) = 0 Then
	dw_from_Date.insertrow( 0)
End If

end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

on w_raw_mat_sap_po_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_from_date=create dw_from_date
this.delivery_t=create delivery_t
this.from_date_t=create from_date_t
this.dw_to_date=create dw_to_date
this.to_date_t=create to_date_t
this.rb_changed_po=create rb_changed_po
this.rb_all_po=create rb_all_po
this.gb_po_option=create gb_po_option
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_from_date
this.Control[iCurrent+3]=this.delivery_t
this.Control[iCurrent+4]=this.from_date_t
this.Control[iCurrent+5]=this.dw_to_date
this.Control[iCurrent+6]=this.to_date_t
this.Control[iCurrent+7]=this.rb_changed_po
this.Control[iCurrent+8]=this.rb_all_po
this.Control[iCurrent+9]=this.gb_po_option
end on

on w_raw_mat_sap_po_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_from_date)
destroy(this.delivery_t)
destroy(this.from_date_t)
destroy(this.dw_to_date)
destroy(this.to_date_t)
destroy(this.rb_changed_po)
destroy(this.rb_all_po)
destroy(this.gb_po_option)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date	ldt_temp

String	ls_temp,ls_temp1, ls_date
Long 		ll_rtn

u_string_functions		lu_strings

If dw_plant.AcceptText() = -1 or &
	dw_from_Date.AcceptText() = -1 or &
	dw_to_Date.AcceptText() = -1 Then 
	return
End if


If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant_Desc', ls_temp)

ls_temp = String(dw_from_Date.GetItemDate(1, "sched_date"), 'yyyy-mm-dd')

iw_parent.Event ue_Set_Data('from_date', ls_temp)

ls_temp = String(dw_to_Date.GetItemDate(1, "sched_date"),'yyyy-mm-dd')

iw_parent.Event ue_Set_Data('to_date', ls_temp)

If rb_changed_po.checked = True Then
	ls_temp = 'C'
Else
	ls_temp = 'A'
End If

iw_parent.Event ue_Set_Data('po_option', ls_temp)
ib_valid_return = True
Message.StringParm = ''
Close(This)

end event

type cb_base_help from w_base_response_ext`cb_base_help within w_raw_mat_sap_po_inq
boolean visible = false
integer x = 2254
integer y = 388
integer taborder = 0
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_raw_mat_sap_po_inq
integer x = 521
integer y = 828
integer taborder = 60
fontcharset fontcharset = ansi!
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_raw_mat_sap_po_inq
integer x = 128
integer y = 828
integer taborder = 50
end type

type dw_plant from u_plant within w_raw_mat_sap_po_inq
integer x = 210
integer y = 12
integer taborder = 10
end type

type dw_from_date from u_sched_date within w_raw_mat_sap_po_inq
integer x = 151
integer y = 556
integer taborder = 20
boolean bringtotop = true
end type

type delivery_t from statictext within w_raw_mat_sap_po_inq
integer x = 64
integer y = 460
integer width = 338
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Delivery Dates:"
alignment alignment = right!
boolean focusrectangle = false
end type

type from_date_t from statictext within w_raw_mat_sap_po_inq
integer x = 192
integer y = 568
integer width = 187
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "From:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_to_date from u_sched_date within w_raw_mat_sap_po_inq
integer x = 155
integer y = 672
integer taborder = 20
boolean bringtotop = true
end type

type to_date_t from statictext within w_raw_mat_sap_po_inq
integer x = 192
integer y = 680
integer width = 187
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "To:"
alignment alignment = right!
boolean focusrectangle = false
end type

type rb_changed_po from radiobutton within w_raw_mat_sap_po_inq
integer x = 402
integer y = 184
integer width = 393
integer height = 108
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Changed PO~'s"
end type

type rb_all_po from radiobutton within w_raw_mat_sap_po_inq
integer x = 398
integer y = 300
integer width = 343
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "All PO~'s"
end type

type gb_po_option from groupbox within w_raw_mat_sap_po_inq
integer x = 389
integer y = 112
integer width = 430
integer height = 324
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

