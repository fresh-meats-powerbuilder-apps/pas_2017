HA$PBExportHeader$w_raw_mat_sched_inq.srw
forward
global type w_raw_mat_sched_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_raw_mat_sched_inq
end type
type dw_input_product from u_fab_product_code within w_raw_mat_sched_inq
end type
type input_prod_st from statictext within w_raw_mat_sched_inq
end type
type input_state_st from statictext within w_raw_mat_sched_inq
end type
type dw_sched_uom from u_uom_code within w_raw_mat_sched_inq
end type
type dw_output_product from u_fab_product_code within w_raw_mat_sched_inq
end type
type dw_choice from datawindow within w_raw_mat_sched_inq
end type
type dw_exist_prod from datawindow within w_raw_mat_sched_inq
end type
type gb_1 from groupbox within w_raw_mat_sched_inq
end type
end forward

global type w_raw_mat_sched_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 2441
integer height = 1344
string title = "Raw Material Default Schedule Inquire"
long backcolor = 67108864
event begindatechanged ( )
event ue_query pbm_custom01
dw_plant dw_plant
dw_input_product dw_input_product
input_prod_st input_prod_st
input_state_st input_state_st
dw_sched_uom dw_sched_uom
dw_output_product dw_output_product
dw_choice dw_choice
dw_exist_prod dw_exist_prod
gb_1 gb_1
end type
global w_raw_mat_sched_inq w_raw_mat_sched_inq

type variables
Boolean		ib_valid_return

String		is_input_string
w_base_sheet	iw_parent

w_raw_mat_sched		iw_parent_window

s_error		istr_error_info

u_pas201		iu_pas201

u_ws_pas2       iu_ws_pas2

DataWindowChild		idwc_exist_prod, &
							idddw_child
							

end variables

forward prototypes
public function boolean wf_get_output_prods ()
end prototypes

event ue_query;wf_get_output_prods()
end event

public function boolean wf_get_output_prods ();Integer	li_ret

Long		ll_row_count

String	ls_input_string, &
			ls_fab_product_descr, &
			ls_plant, &
			ls_product, & 
			ls_state, &
			ls_product_status, &
			ls_uom, &
			ls_output_values

ls_output_values = ' '

dw_plant.AcceptText()
dw_input_product.AcceptText()
dw_sched_uom.AcceptText()

ls_plant = dw_plant.GetItemString(1, "location_code")
If iw_frame.iu_string.nf_IsEmpty(ls_plant) Then return false

ls_input_string = ls_plant

ls_product = dw_input_product.GetItemString(1, "fab_product_code")
If iw_frame.iu_string.nf_IsEmpty(ls_product) Then return False

ls_state = dw_input_product.GetItemString(1, "product_state")
If iw_frame.iu_string.nf_IsEmpty(ls_state) Then return False
//1-13 jac
ls_product_status = dw_input_product.GetItemString(1, "product_status")
If iw_frame.iu_string.nf_IsEmpty(ls_product_status) Then return False
//ls_input_string += '~t' + ls_product + '~t' + ls_state 
ls_input_string += '~t' + ls_product + '~t' + ls_state + '~t' + ls_product_status
//
ls_uom = dw_sched_uom.GetItemString(1, "uom_code")
If iw_frame.iu_string.nf_IsEmpty(ls_uom) Then return False

ls_input_string += '~t' + ls_uom +'~r~n'

is_input_string = ls_input_string
SetPointer(HourGlass!)

istr_error_info.se_event_name = "Output Products Inquire"
istr_error_info.se_procedure_name = "nf_pasp42gr"
istr_error_info.se_message = Space(70)

//li_ret =iu_pas201.nf_pasp42cr_inq_rmt_sched_output( &
//									istr_error_info, &
//									is_input_string, &
//									ls_output_values)

li_ret =iu_ws_pas2.nf_pasp42gr(is_input_string, &
									ls_output_values, istr_error_info)
									
If li_ret = 0 Then 
	dw_exist_prod.SetFocus()
Else
	// If The product had focus, set focus back there
	// This is safe since either plant or product change called this event
	If ClassName(GetFocus()) = "dw_plant" Then
		dw_plant.SetColumn('fab_product_code')
		dw_plant.SetFocus()
	End if
	This.SetRedraw(True)
	Return( False )
End if

//li_ret = dw_exist_prod.GetChild("prd_prdstate", idwc_exist_prod)	

//idwc_exist_prod.ImportString(dw_exist_prod.Describe("DataWindow.Data"))
ll_row_count = idwc_exist_prod.ImportString(ls_output_values)
If ll_row_count < 1 Then
	iw_frame.SetMicroHelp("No Scheduled Product Codes are defined for Plant " + ls_plant + &
								", Product " + ls_product + ", Product State " + ls_state + &
								", Product Status " + ls_product_status + ", UOM " + ls_uom)
	dw_choice.SetItem(1, "choice", "N")
	dw_exist_prod.Enabled = False
	dw_exist_prod.visible = False
	dw_output_product.Enabled = True
	dw_output_product.visible = True
Else
	iw_frame.SetMicroHelp(String(ll_row_count) + " rows retrieved")
	dw_choice.SetItem(1, "choice", "E")
	dw_output_product.Enabled = False
	dw_output_product.visible = False
	dw_exist_prod.Enabled = True
	dw_exist_prod.visible = True
End if

return true
end function

event ue_postopen;call super::ue_postopen;DataWindowChild			ldwc_header

Int							li_pos, &
								li_ret, &
								li_null

String						ls_header, &
								ls_temp, &
								ls_product_code, &
								ls_product_descr, &
								ls_state, &
								ls_product_status, &
								ls_uom, &
								ls_prdstate

u_string_functions		lu_string

Window   lw_parent

lw_parent = this.parentwindow()
This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)

DataWindowChild			ldwc_state

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

iu_ws_pas2 = Create u_ws_pas2
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

dw_sched_uom.GetChild('uom_code', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("RAWUOM")


iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If

iw_parent.Event ue_Get_Data('Input_Product')
ls_product_code = Message.StringParm
iw_parent.Event ue_Get_Data('Input_Product_Descr')
ls_product_descr = Message.StringParm
iw_parent.Event ue_Get_Data('Input_State')
ls_state = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_state) Then dw_input_product.uf_set_product_state(ls_state)
dw_input_product.uf_set_product_code( ls_product_code, ls_product_descr)
//1-13 jac
iw_parent.Event ue_Get_Data('Input_Status')
ls_product_status = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_product_status) Then dw_input_product.uf_set_product_status(ls_product_status)
//
iw_parent.Event ue_Get_Data('UOM')
ls_uom = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_uom) Then
	dw_sched_uom.uf_set_uom_code(ls_uom)
End If
//if Not lu_string.nf_IsEmpty(ls_state) Then dw_output_product.uf_set_product_state(ls_state)
//dw_output_product.uf_set_product_code( ls_product_code, ls_product_descr)

If dw_plant.RowCount() < 1 Then 
	dw_plant.InsertRow(0)
ElseIf Not lu_string.nf_IsEmpty(dw_plant.GetItemString(1,1)) Then
	dw_input_product.setfocus( )
End if

dw_choice.SetItem(1, "choice", "N")
dw_exist_prod.Enabled = False
dw_exist_prod.visible = False
dw_output_product.Enabled = True
dw_output_product.visible = True

SetNull(li_null)
idwc_exist_prod.Reset()
dw_exist_prod.SetItem(1, "prd_prdstate", li_null)
PostEvent("ue_query")

dw_exist_prod.GetChild("prd_prdstate", idwc_exist_prod)

iw_parent.Event ue_Get_Data('Prd_Prdstate')
ls_prdstate = Message.StringParm
If Not lu_string.nf_IsEmpty(ls_prdstate) Then 
	dw_exist_prod.SetItem(1, "prd_prdstate", ls_prdstate)
End If

end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

if dw_input_product.rowcount( ) = 0 Then
	dw_input_product.insertrow( 0)
End If

if dw_sched_uom.rowcount( ) = 0 Then
	dw_sched_uom.insertrow( 0)
End If

if dw_output_product.rowcount( ) = 0 Then
	dw_output_product.insertrow( 0)
End If

if dw_exist_prod.rowcount( ) = 0 Then
	dw_exist_prod.insertrow( 0)
End If

if dw_choice.rowcount( ) = 0 Then
	dw_choice.insertrow( 0)
End If
end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

on w_raw_mat_sched_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_input_product=create dw_input_product
this.input_prod_st=create input_prod_st
this.input_state_st=create input_state_st
this.dw_sched_uom=create dw_sched_uom
this.dw_output_product=create dw_output_product
this.dw_choice=create dw_choice
this.dw_exist_prod=create dw_exist_prod
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_input_product
this.Control[iCurrent+3]=this.input_prod_st
this.Control[iCurrent+4]=this.input_state_st
this.Control[iCurrent+5]=this.dw_sched_uom
this.Control[iCurrent+6]=this.dw_output_product
this.Control[iCurrent+7]=this.dw_choice
this.Control[iCurrent+8]=this.dw_exist_prod
this.Control[iCurrent+9]=this.gb_1
end on

on w_raw_mat_sched_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_input_product)
destroy(this.input_prod_st)
destroy(this.input_state_st)
destroy(this.dw_sched_uom)
destroy(this.dw_output_product)
destroy(this.dw_choice)
destroy(this.dw_exist_prod)
destroy(this.gb_1)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date	ldt_temp

String	ls_temp, &
			ls_temp1, &
			ls_choice, &
			ls_data, &
			ls_SearchString, &
			ls_PrdPrdstate
			
Long 		ll_rtn, &
			ll_len

If dw_plant.AcceptText() = -1 Then 
	dw_plant.SetFocus()
	return
End if

ls_choice = dw_choice.GetItemString(1,'choice')

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End if

If not dw_input_product.uf_validate_rmt( ) Then
	dw_input_product.setfocus()
	return
End If

If ls_choice = 'N' Then
	If not dw_output_product.uf_validate_rmt( ) Then
		dw_output_product.setfocus()
		return
	End If
End If

If iw_frame.iu_string.nf_IsEmpty(dw_input_product.GetItemString(1, "fab_product_code")) Then
	iw_frame.SetMicroHelp("Input Product Code is a required field")
	dw_input_product.SetFocus()
	return
End if


If iw_frame.iu_string.nf_IsEmpty(dw_input_product.GetItemString(1, "product_state")) Then
	iw_frame.SetMicroHelp("Input Product State is a required field")
	dw_input_product.SetFocus()
	return
End if
//1-13 jac
If iw_frame.iu_string.nf_IsEmpty(dw_input_product.GetItemString(1, "product_status")) Then
	iw_frame.SetMicroHelp("Input Product Status is a required field")
	dw_input_product.SetFocus()
	return
End if
//
If iw_frame.iu_string.nf_IsEmpty(dw_sched_uom.GetItemString(1, "uom_code")) Then
	iw_frame.SetMicroHelp("UOM code is a required field")
	dw_sched_uom.SetFocus()
	return
End if

If ls_choice = 'N' Then
	If iw_frame.iu_string.nf_IsEmpty(dw_output_product.GetItemString(1, "fab_product_code")) Then
		iw_frame.SetMicroHelp("Output Product Code is a required field")
		dw_output_product.SetFocus()
		return
	End if

	If iw_frame.iu_string.nf_IsEmpty(dw_output_product.GetItemString(1, "product_state")) Then
		iw_frame.SetMicroHelp("Output Product State is a required field")
		dw_output_product.SetFocus()
		return
	End if
	//1-13 jac 
	If iw_frame.iu_string.nf_IsEmpty(dw_output_product.GetItemString(1, "product_status")) Then
		iw_frame.SetMicroHelp("Output Product Status is a required field")
		dw_output_product.SetFocus()
		return
	End if
	//
Else
	If iw_frame.iu_string.nf_IsEmpty(dw_exist_prod.GetItemString(1, "prd_prdstate")) Then
		iw_frame.SetMicroHelp("Must choose an existing product when Existing is selected.")
		dw_exist_prod.SetFocus()
		return
	End if
End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant_Desc', ls_temp)

ls_temp = dw_input_product.uf_get_product_code( )
iw_parent.Event ue_Set_Data('Input_Product', ls_temp)

ls_temp = dw_input_product.uf_get_product_desc( )
iw_parent.Event ue_Set_Data('Input_Product_Desc', ls_temp)

ls_temp = dw_input_product.uf_get_product_state( )
iw_parent.Event ue_Set_Data('Input_State', ls_temp)
//1-13 jac
ls_temp = dw_input_product.uf_get_product_status( )
iw_parent.Event ue_Set_Data('Input_Status', ls_temp)
//
ls_temp = dw_sched_uom.GetItemString(1, 'uom_code')
iw_parent.Event ue_Set_Data('UOM', ls_temp)

If ls_choice = 'N' Then
	ls_temp = dw_output_product.uf_get_product_code( )
	iw_parent.Event ue_Set_Data('Output_Product', ls_temp)

	ls_temp = dw_output_product.uf_get_product_desc( )
	iw_parent.Event ue_Set_Data('Output_Product_Desc', ls_temp)

	ls_temp = dw_output_product.uf_get_product_state( )
	iw_parent.Event ue_Set_Data('Output_State', ls_temp)
//1-13 jac	
	ls_temp = dw_output_product.uf_get_product_status( )
	iw_parent.Event ue_Set_Data('Output_Status', ls_temp)
//	
	ls_PrdPrdstate = space(18)
	ls_PrdPrdstate = dw_output_product.GetItemString(1, 'fab_product_code') 
	ll_len = Len(ls_PrdPrdstate)
	ls_PrdPrdstate = ls_PrdPrdstate + space(10 - ll_len) + '  ' 
	ls_PrdPrdstate = ls_PrdPrdstate + dw_output_product.GetItemString(1, 'product_state') + '  ' 
	//1-13 jac
	ls_PrdPrdstate = ls_PrdPrdstate + dw_output_product.GetItemString(1, 'product_status')
	//
	ls_PrdPrdstate = ls_PrdPrdstate + '  ' 
	
	iw_parent.Event ue_Set_Data('Prd_Prdstate', ls_PrdPrdstate)
Else
	//search the list of existing products for the one chosen to
	//pass back to the parent window
	ls_SearchString = 	"prd_prdstate = '"+ dw_exist_prod.GetItemString(1, 'prd_prdstate') + "'"
								
	ll_rtn = idwc_exist_prod.Find  &
				( ls_SearchString, 1, idwc_exist_prod.RowCount())
	
	If ll_rtn < 1 Then
		//If iw_frame.iu_string.nf_IsEmpty(dw_exist_prod.GetItemString(1, "prd_prdstate")) Then
			iw_frame.SetMicroHelp("Existing product not found in list.")
			dw_exist_prod.SetFocus()
			return
		//End If
	End if
	
	ls_temp = idwc_exist_prod.GetItemString(ll_rtn, 'product_code')
	iw_parent.Event ue_Set_Data('Output_Product', ls_temp)
	
	ls_temp = idwc_exist_prod.GetItemString(ll_rtn, 'product_descr')
	iw_parent.Event ue_Set_Data('Output_Product_Desc', ls_temp)
	
	ls_temp = idwc_exist_prod.GetItemString(ll_rtn, 'product_state')
	iw_parent.Event ue_Set_Data('Output_State', ls_temp)
	//1-13 jac
	ls_temp = idwc_exist_prod.GetItemString(ll_rtn, 'product_status')
	iw_parent.Event ue_Set_Data('Output_Status', ls_temp)
	//
	ls_temp = dw_exist_prod.GetItemString(1, 'prd_prdstate')
	iw_parent.Event ue_Set_Data('Prd_Prdstate', ls_temp)
End If


ib_valid_return = True
Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_raw_mat_sched_inq
boolean visible = false
integer x = 1696
integer y = 824
integer taborder = 0
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_raw_mat_sched_inq
integer x = 814
integer y = 1096
integer taborder = 90
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_raw_mat_sched_inq
integer x = 315
integer y = 1088
integer taborder = 80
end type

type dw_plant from u_plant within w_raw_mat_sched_inq
integer x = 302
integer y = 12
integer taborder = 10
end type

event itemchanged;call super::itemchanged;Int	li_null
Long	ll_result

ll_result = Super::Event itemchanged(row, dwo, data)

SetNull(li_null)
idwc_exist_prod.Reset()

dw_exist_prod.Reset()
dw_exist_prod.InsertRow(0)
dw_exist_prod.SetItem(1, "prd_prdstate", li_null)
Parent.PostEvent("ue_query")

return ll_result
end event

type dw_input_product from u_fab_product_code within w_raw_mat_sched_inq
integer x = 256
integer y = 88
integer width = 1627
integer taborder = 20
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state, ldwc_status

dw_input_product.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
ldwc_state.SetSort("type_code")
ldwc_state.Sort()

//1-13 jac
dw_input_product.GetChild('product_status', ldwc_status)
ldwc_status.SetTransObject(SQLCA)
ldwc_status.Retrieve("PRODSTAT")
ldwc_status.SetSort("type_code")
ldwc_status.Sort()
//
end event

event itemchanged;call super::itemchanged;Int	li_null
Long	ll_result

string 	ls_product_state, & 
         ls_product_status

Choose Case This.GetColumnName()
	Case "fab_product_code"
		
		dw_exist_prod.Reset()
		dw_exist_prod.InsertRow(0)
		
		idwc_exist_prod.Reset()
		SetNull(li_null)
		dw_exist_prod.SetItem(1, "prd_prdstate", li_null)
		Parent.PostEvent("ue_query")
		
	case "product_state" 
			ls_product_state = data
			
			dw_exist_prod.Reset()
			dw_exist_prod.InsertRow(0)
			
			idwc_exist_prod.Reset()
			SetNull(li_null)
			dw_exist_prod.SetItem(1, "prd_prdstate", li_null)
			Parent.PostEvent("ue_query")
						
			If Len(Trim(ls_product_state)) = 0 Then 
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				This.ScrollToRow(This.RowCount())
				This.SetItem(1,"product_state","")
				this.setitem(1,"product_state_description",(""))
				return 
			End if
	//1-13 jac		
	case "product_status" 
			ls_product_status = data
			
			dw_exist_prod.Reset()
			dw_exist_prod.InsertRow(0)
			
			idwc_exist_prod.Reset()
			SetNull(li_null)
			dw_exist_prod.SetItem(1, "prd_prdstate", li_null)
			Parent.PostEvent("ue_query")
						
			If Len(Trim(ls_product_status)) = 0 Then 
				iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
				This.ScrollToRow(This.RowCount())
				This.SetItem(1,"product_status","")
				this.setitem(1,"product_status_description",(""))
				return 
			End if
    //
End Choose

If not dw_input_product.uf_validate_rmt( ) Then
	dw_input_product.setfocus()
	return
End If

return ll_result
end event

type input_prod_st from statictext within w_raw_mat_sched_inq
integer x = 201
integer y = 112
integer width = 201
integer height = 68
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Input"
alignment alignment = right!
boolean focusrectangle = false
end type

type input_state_st from statictext within w_raw_mat_sched_inq
integer x = 178
integer y = 204
integer width = 270
integer height = 56
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Product"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_sched_uom from u_uom_code within w_raw_mat_sched_inq
integer x = 187
integer y = 416
integer height = 96
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_rmt_uom_code"
end type

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

dw_sched_uom.GetChild('uom_code', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("RAWUOM")

end event

event itemchanged;call super::itemchanged;Int	li_null
Long	ll_result

ll_result = Super::Event itemchanged(row, dwo, data)

SetNull(li_null)
idwc_exist_prod.Reset()

dw_exist_prod.Reset()
dw_exist_prod.InsertRow(0)

dw_exist_prod.SetItem(1, "prd_prdstate", li_null)
Parent.PostEvent("ue_query")

return ll_result
end event

type dw_output_product from u_fab_product_code within w_raw_mat_sched_inq
integer x = 251
integer y = 620
integer taborder = 40
boolean bringtotop = true
end type

event clicked;call super::clicked;//Integer		li_null
//
//idwc_exist_prod.Reset()
//SetNull(li_null)
//dw_exist_prod.SetItem(1, "prd_prdstate", li_null)
//Parent.PostEvent("ue_query")
end event

type dw_choice from datawindow within w_raw_mat_sched_inq
integer x = 1934
integer y = 576
integer width = 411
integer height = 300
integer taborder = 60
boolean bringtotop = true
string title = "none"
string dataobject = "d_output_choice"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;String			ls_choice

ls_choice = data

If ls_choice = 'N' Then
	dw_exist_prod.Enabled = False
	dw_exist_prod.visible = False
	dw_output_product.Enabled = True
	dw_output_product.visible = True
Else
	dw_output_product.Enabled = False
	dw_output_product.visible = False
	dw_exist_prod.Enabled = True
	dw_exist_prod.visible = True
End If
end event

type dw_exist_prod from datawindow within w_raw_mat_sched_inq
integer x = 133
integer y = 896
integer width = 1824
integer height = 96
integer taborder = 50
boolean bringtotop = true
string title = "none"
string dataobject = "d_prd_prdstate"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;DataWindowChild		ldwc_type


This.GetChild("prd_prdstate", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PRDSTATE")
ldwc_type.SetSort("type_code")
ldwc_type.Sort()
end event

type gb_1 from groupbox within w_raw_mat_sched_inq
integer x = 91
integer y = 540
integer width = 2286
integer height = 504
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Output"
end type

