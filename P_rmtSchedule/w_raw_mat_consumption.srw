HA$PBExportHeader$w_raw_mat_consumption.srw
forward
global type w_raw_mat_consumption from w_base_sheet_ext
end type
type rb_deselect from radiobutton within w_raw_mat_consumption
end type
type rb_select from radiobutton within w_raw_mat_consumption
end type
type sched_products_only from checkbox within w_raw_mat_consumption
end type
type dw_consumption_scroll_product from u_base_dw_ext within w_raw_mat_consumption
end type
type dw_header from u_base_dw_ext within w_raw_mat_consumption
end type
type dw_rmt_detail from u_base_dw_ext within w_raw_mat_consumption
end type
type gb_selection from groupbox within w_raw_mat_consumption
end type
end forward

global type w_raw_mat_consumption from w_base_sheet_ext
integer width = 2681
integer height = 1994
string title = "Raw Material Consumption"
long backcolor = 67108864
rb_deselect rb_deselect
rb_select rb_select
sched_products_only sched_products_only
dw_consumption_scroll_product dw_consumption_scroll_product
dw_header dw_header
dw_rmt_detail dw_rmt_detail
gb_selection gb_selection
end type
global w_raw_mat_consumption w_raw_mat_consumption

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product	

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow

s_error		istr_error_info

u_pas201		iu_pas201

u_ws_pas2       iu_ws_pas2

String		is_colname, &
				is_input, &
				is_ChangedColumnName, &
				is_debug
				
w_base_sheet	iw_order_detail
end variables

forward prototypes
public subroutine wf_delete ()
public function boolean wf_deleterow ()
public function string wf_getheaderinfo ()
public function boolean wf_addrow ()
public subroutine wf_filter ()
public function boolean wf_validate (long al_row)
public function boolean wf_retrieve ()
public function boolean wf_update ()
end prototypes

public subroutine wf_delete ();wf_deleterow()
end subroutine

public function boolean wf_deleterow ();Boolean		lb_ret

dw_rmt_detail.SetFocus()
lb_ret = super::wf_deleterow()

dw_rmt_detail.SelectRow(0,False)
dw_rmt_detail.SelectRow(dw_rmt_detail.GetRow(),True)
return lb_ret
end function

public function string wf_getheaderinfo ();String		ls_string

//1-13 jac added product status to string
ls_string = dw_header.GetItemString(1, 'plant_code') + '~t' + &
				dw_header.GetItemString(1, 'plant_description') + '~t' + &
				string(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy') + '~t' + &	
				dw_header.GetItemString(1, 'Shift') + '~t' + &
				String(dw_header.GetItemnumber(1, 'Product_State')) + '~t' + &
				dw_header.GetItemString(1, 'Product_Status') + '~t' + &
				dw_header.GetItemString(1, 'mts_pa_inq') + '~t' 

return ls_string


end function

public function boolean wf_addrow ();Long			ll_row, &
				ll_current_row

If dw_rmt_detail.accepttext( ) <> 1 then return false

This.SetRedraw(False)

ll_current_row = dw_rmt_detail.GetRow()

If ll_current_row > 0 Then
	ll_row = dw_rmt_detail.InsertRow(ll_current_row + 1)
	dw_rmt_detail.SetItem(ll_row, "product_code", dw_rmt_detail.GetItemString(ll_current_row, "product_code"))
	dw_rmt_detail.SetItem(ll_row, "product_description", dw_rmt_detail.GetItemString(ll_current_row, "product_description"))
Else
	ll_row = dw_rmt_detail.InsertRow(0)
	dw_rmt_detail.SetItem(ll_row, "production_date", Today())
End If
//sap
//ll_row_count = dw_manage_inventory_detail_fresh.RowCount()
//ll_total_qty = 0
//ll_total_wgt = 0
//FOR li_row = 1 TO ll_Row_count - 1
//	ll_qty = dw_manage_Inventory_detail_fresh.GetItemNumber(li_row, "quantity")
//	ll_wgt = dw_manage_inventory_detail_fresh.GetItemNumber(li_row, "inventory_wgt")
//	ll_total_qty = ll_total_qty + ll_qty
//	ll_total_wgt = ll_total_wgt + ll_wgt
//Next
//if ll_total_qty > 0 Then
//	dw_manage_inventory_detail_fresh.SetItem(ll_row_count, "prev_avg_wgt", ll_total_wgt/ll_total_qty )
//Else
//	dw_manage_inventory_detail_fresh.SetItem(ll_row_count, "prev_avg_wgt", 0 )	
//end if
//
//sap
dw_rmt_detail.ScrollToRow(ll_row)
dw_rmt_detail.SetItem(ll_row, "update_flag", 'A')
dw_rmt_detail.SetColumn("production_date")
dw_rmt_detail.SetFocus()
This.SetRedraw(True)

return true



end function

public subroutine wf_filter ();String	ls_filter

dw_rmt_detail.SetRedraw(False)

ls_filter = ""
if sched_products_only.checked then 
		ls_filter = "sched_product = 'Y'"
		
//	else
//		ls_filter = "sched_product = 'N' or sched_product = 'Y'"
End if
dw_rmt_detail.SetFilter(ls_filter)
dw_rmt_detail.Filter()
dw_rmt_detail.Sort()
dw_rmt_detail.SetRedraw(True)
end subroutine

public function boolean wf_validate (long al_row);Date					ldt_temp

Integer				li_boxes, &
						li_weight
						
Long					ll_rtn, &
						ll_nbrrows, &
						ll_boxes, &
						ll_weight, &
						ll_ModifiedRow, &
						ll_avg_wgt, &
						ll_prod_avg_wgt, & 
		 				ll_sales_avg_wgt

String				ls_product_description, &
						ls_fab_product, &
						ls_product_state, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag, &
						ls_production_date, &
						ls_shift, &
						ls_hold_code, &
						ls_hold_plant, &
						ls_product_status
						
IF dw_rmt_detail.AcceptText() = -1 THEN Return( False )

ll_nbrrows = dw_rmt_detail.RowCount()

ls_temp = dw_rmt_detail.GetItemString(al_row, "product_code")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	MessageBox("Product Code", "Please enter a product code.  Product Code cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("product_code")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If not invuo_fab_product_code.uf_check_product(ls_temp) then
		If	invuo_fab_product_code.ib_error_occurred then
			iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
			Return False
		Else
			iw_frame.SetMicroHelp(ls_temp + " is an invalid Product Code")
			Return False
		End If
	End If
End If

//ls_temp = dw_rmt_detail.GetItemString(al_row, "Product_State")
//IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
//	//MessageBox("Product State", "Please enter a product state.  Product state cannot be left blank.")
//	iw_Frame.SetMicroHelp("Please enter a product state.  Product state cannot be left blank.")
//	This.SetRedraw(False)
//	dw_rmt_detail.ScrollToRow(al_row)
//	dw_rmt_detail.SetColumn("Product_State")
//	dw_rmt_detail.SetFocus()
//	This.SetRedraw(True)
//	Return False
//Else
//	ls_fab_product = dw_rmt_detail.getitemstring(al_row, "product_code")
//	if trim(ls_temp) <> "1" then
//		if not invuo_fab_product_code.uf_check_product_state_rmt(ls_fab_product, Trim(ls_temp)) then	
//			iw_Frame.SetMicroHelp(ls_temp + " is an invalid Product State for this Product")
//			dw_rmt_detail.setfocus( )
//			dw_rmt_detail.setcolumn("Product_State") 
//			dw_rmt_detail.SelectText(1, Len(ls_temp))	
//			Return False
//		end if
//	end if
//End If
//
//ll_boxes = dw_rmt_detail.GetItemNumber(al_row, "boxes")
//ll_weight = dw_rmt_detail.GetItemNumber(al_row, "weight")
//if IsNull(ll_boxes) Then
//	SetMicroHelp("Please enter Boxes")
//	This.SetRedraw(False)
//	dw_rmt_detail.ScrollToRow(al_row)
//	dw_rmt_detail.SetColumn("boxes")
//	dw_rmt_detail.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If
////
//if IsNull(ll_weight) Then
//	SetMicroHelp("Please enter Weight")
//	This.SetRedraw(False)
//	dw_rmt_detail.ScrollToRow(al_row)
//	dw_rmt_detail.SetColumn("weight")
//	dw_rmt_detail.SetFocus()
//	This.SetRedraw(True)
//	Return False
//End If
////if ll_boxes = 0 and ll_weight = 0 Then
////	SetMicroHelp("Please enter either Boxes or Weight")
////	This.SetRedraw(False)
////	dw_rmt_detail.ScrollToRow(al_row)
////	dw_rmt_detail.SetColumn("boxes")
////	dw_rmt_detail.SetFocus()
////	This.SetRedraw(True)
////	Return False
////End If
////
//
////sap

//sap

ls_update_flag = dw_rmt_detail.GetItemString(al_row, "update_flag")
ldt_temp = dw_rmt_detail.GetItemDate(al_row, "Production_Date")
IF IsNull(ldt_temp) Then 
	//MessageBox("From Date", "Please enter a From Date greater than 00/00/0000")
	iw_Frame.SetMicroHelp("Please enter a Production Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("Production_Date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If
IF ldt_temp > Date('12/31/2999') Then 
	//MessageBox("From Date", "Please enter a From Date less than 12/31/2999")
	iw_Frame.SetMicroHelp("Please enter a Production Date less than 12/31/2999")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("Production_Date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If


If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True


ls_fab_product = dw_rmt_detail.GetItemString(al_row, "product_code")
ls_product_description = dw_rmt_detail.GetItemString(al_row, "product_description")
ls_production_date = String(ldt_temp)
ls_product_state = dw_rmt_detail.GetItemString(al_row, "product_state")
ls_product_status = dw_rmt_detail.GetItemString(al_row, "product_status")
//
//ls_SearchString	= "product_code = '" + ls_fab_product + &
//							"and product_description = '" + ls_product_description + &
//						" and production_date = date('" + ls_production_date+ "')" 
						
ls_SearchString	=	"product_code = '"+ ls_fab_product +&
							"' and production_date = date('" + ls_production_date+ "')" +& 
							" and product_state = '"+ ls_product_state +&
							"' and product_status = '"+ ls_product_status + "'"
//						
//"' and product_description = '" + ls_product_description + "'" +&
//// Find a matching row excluding the current row.
//
CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_rmt_detail.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_rmt_detail.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_rmt_detail.Find  &
			(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_rmt_detail.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE

If ll_rtn > 0 Then
	iw_Frame.SetMicroHelp( "There are duplicate products with the" + &
  					" same production dates.")
		dw_rmt_detail.SetRedraw(False)
		dw_rmt_detail.ScrollToRow(al_row)
		dw_rmt_detail.SetColumn("boxes")
		dw_rmt_detail.SetRow(al_row)
		dw_rmt_detail.SelectRow(ll_rtn, True)
		dw_rmt_detail.SelectRow(al_row, True)
		dw_rmt_detail.SetRedraw(True)
		Return False
End if

//al_row = 0
//Do
//	ll_ModifiedRow = al_row
//	ll_ModifiedRow = dw_rmt_detail.GetNextModified(al_row, Primary!)
//	al_row = ll_ModifiedRow
////
//	If al_row > 0 Then
//		ll_boxes = dw_rmt_detail.GetItemNumber(al_row, "boxes")
//		ll_weight = dw_rmt_detail.GetItemNumber(al_row, "weight") 
//		If ll_boxes = 0  and ll_weight = 0 Then
//			ll_avg_wgt = 0
//		Else
//			if ll_weight = 0  Then
//			ls_hold_plant = dw_header.GetItemString(1,'plant_code')
//			ls_hold_code = dw_rmt_detail.GetItemString(al_row, 'product_code')
//			CONNECT USING SQLCA;	
//			SELECT plt.production_avg_wt
//					,plt.sales_avg_weight
//				INTO :ll_prod_avg_wgt 
// 				 	 ,:ll_sales_avg_wgt
//				FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
//				WHERE ( plt.sku_product_code = :ls_hold_code ) AND  
//     					( plt.plant_code  = :ls_hold_plant)   ;
//		
//				If SQLCA.SQLCode = 00 Then
//					If ll_prod_avg_wgt > 0 Then
//						ll_avg_wgt = ll_prod_avg_wgt
//					Else
//						ll_avg_wgt = ll_sales_avg_wgt 
//					End if	
//				Else
//					ll_avg_wgt = 0
//				End If
//			End If
////		End If
//		if ll_weight = 0 Then
//			dw_rmt_detail.SetItem(al_row, "weight", ll_boxes * ll_avg_wgt)
//		End if
//		if ll_boxes = 0 Then
//			dw_rmt_detail.SetItem(al_row, "boxes", ll_weight / ll_avg_wgt)
//		End if
//			End if	
//		End if
//		
//Loop while al_row > 0 
//
//
Return True
end function

public function boolean wf_retrieve ();Boolean	lb_ret
Integer	li_ret, &
			li_row_Count,li_loop
String	ls_input, &
			ls_output_values, &
			ls_temp, &
			ls_shift, &
			ls_pa_shift, &
			ls_from_time, &
			ls_to_time
		
			
Long		ll_value, &
			ll_rtn, &
			ll_row_count, ll_row, &
			ll_weight, ll_boxes
			
Date		ll_prod_date

u_string_functions u_string

If Not ib_ReInquire Then
	OpenWithParm(w_raw_mat_consumption_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")
is_debug = This.dw_header.GetItemString(1, 'plant_code')
is_debug = String(dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') 
is_debug = dw_header.GetItemString(1, 'mts_pa_inq') 
is_debug = dw_header.GetItemString(1, 'consumption_choice')

//move spaces to these fields for the header input string 
ls_shift = This.dw_header.GetItemString(1, "Shift")
If iw_frame.iu_string.nf_IsEmpty(ls_shift) Then
	ls_shift = space(1)
end if

ls_from_time = dw_header.GetItemString(1, 'from_time')
If iw_frame.iu_string.nf_IsEmpty(ls_from_time) Then
	ls_from_time = space(4)
end if
ls_to_time = dw_header.GetItemString(1, 'to_time')
If iw_frame.iu_string.nf_IsEmpty(ls_to_time) Then
	ls_to_time = space(4)
end if

ls_pa_shift = dw_header.GetItemString(1, 'pa_shift')
If iw_frame.iu_string.nf_IsEmpty(ls_pa_shift) Then
	ls_pa_shift = space(1)
end if

//1-13 jac  added product status to string
ls_input = This.dw_header.GetItemString(1, "plant_code") + '~t' + &
				String(dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' + &
				ls_shift + '~t' + &
				dw_header.GetItemString(1, 'mts_pa_inq') + '~t' + &
				dw_header.GetItemString(1, 'consumption_choice') + '~t' + &
				ls_from_time + '~t' + &
				ls_to_time + '~t' + &
				ls_pa_shift + '~r~n'
		
								
is_input = ls_input

//li_ret = iu_pas201.nf_pasp40cr_inq_rmt_consumption(istr_error_info, &
//									is_input, &
//									ls_output_values) 

li_ret = iu_ws_pas2.nf_pasp40gr(is_input, &
									ls_output_values,istr_error_info) 
						

This.dw_rmt_detail.Reset()
//
If li_ret = 0 Then
This.dw_rmt_detail.ImportString(ls_output_values)

	ll_value = dw_rmt_detail.RowCount()
	If ll_value < 0 Then ll_value = 0

	dw_rmt_detail.ResetUpdate()

	IF ll_value > 0 THEN
 		dw_rmt_detail.SetFocus()
		dw_rmt_detail.ScrollToRow(1)
		dw_rmt_detail.SetColumn( "production_date" )
		dw_rmt_detail.TriggerEvent("RowFocusChanged")
	END IF

	SetMicroHelp(String(ll_value) + " rows retrieved")
	This.SetRedraw( True )
	dw_rmt_detail.SetSort("product_code A, production_date A, product_state A, product status A")
	dw_rmt_detail.Sort()
	li_Row_count = dw_rmt_detail.RowCount()

	ll_row = 1
	If li_Row_count > 0 Then
		Do
			ll_boxes = dw_rmt_detail.GetItemNumber(ll_row, "boxes")
			ll_weight = dw_rmt_detail.GetItemNumber(ll_row, "weight")
			dw_rmt_detail.SetItem(ll_row, "prev_boxes", ll_boxes)
			dw_rmt_detail.SetItem(ll_row, "prev_weight", ll_weight)

			ll_row = ll_row + 1
		Loop While ll_row <= li_row_count
	Else
		dw_rmt_detail.SetItem(ll_row, "prev_boxes", 0 )
		dw_rmt_detail.SetItem(ll_row, "prev_weight", 0 )
	End If
Else
	ll_rtn = iw_frame.im_menu.mf_disable('m_save')
	ll_rtn = iw_frame.im_menu.mf_disable('m_new')
	ll_rtn = iw_frame.im_menu.mf_disable('m_addrow')
	This.SetRedraw(True)
	Return( False )
End If
//dw_rmt_detail.SetRedraw(False)
wf_filter()
//dw_rmt_detail.SetRedraw(True)
dw_rmt_detail.ResetUpdate()

if dw_header.GetItemString(1, "mts_pa_inq") = 'M' Then
	gb_selection.Visible = True
	rb_deselect.Visible = True
	rb_select.Visible = True
	ll_rtn = iw_frame.im_menu.mf_disable('m_new')
	ll_rtn = iw_frame.im_menu.mf_disable('m_addrow')
	ll_rtn = iw_frame.im_menu.mf_disable('m_delete')
	ll_rtn = iw_frame.im_menu.mf_disable('m_deleterow')
else
	gb_selection.Visible = False
	rb_deselect.Visible = False
	rb_select.Visible = False
end if

Return True

end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter
					
long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount, &
					ll_ModifiedRow, &
					ll_boxes, ll_weight, &
					ll_prod_avg_wgt, & 
				 	ll_sales_avg_wgt, &
					ll_avg_wgt, ll_prev_weight, &
					ll_prev_boxes, &
					ll_selected_count

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdateRMDS, &
					ls_header_string, &
					ls_hold_code, &	
					ls_hold_plant
					
dwItemStatus	lis_status

//set the update flag on the lines that have been selected when WMS
If dw_header.GetItemString(1,"mts_pa_inq") = 'M' Then
		ll_selected_count = long(dw_rmt_detail.describe("evaluate('sum(if(isselected(), 1, 0) for all)',1)"))
		ll_NbrRows = dw_rmt_detail.RowCount( )
		For ll_row = 1 to ll_NbrRows
	 	If dw_rmt_detail.IsSelected(ll_row) or ll_selected_count = 0 Then
			dw_rmt_detail.SetItem(ll_row, 'update_flag', 'U')
		else
			dw_rmt_detail.SetItem(ll_row, 'update_flag', ' ')
			dw_rmt_detail.SetItemStatus(ll_row, 0, Primary!, NotModified!)
		End if
	Next
End If

IF dw_rmt_detail.AcceptText() = -1 THEN Return( False )
IF dw_rmt_detail.ModifiedCount() + dw_rmt_detail.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_rmt_detail.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_rmt_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP
////  sap
//ll_row = 0
//Do
//	ll_ModifiedRow = ll_row
//
//	ll_ModifiedRow = dw_rmt_detail.GetNextModified(ll_row, Primary!)
//	ll_row = ll_ModifiedRow
//
//	If ll_row > 0 Then
//		ll_prev_weight = dw_rmt_detail.GetItemNumber(ll_row, "prev_weight")
//		ll_prev_boxes = dw_rmt_detail.GetItemNumber(ll_row, "prev_boxes")
//		ll_boxes = dw_rmt_detail.GetItemNumber(ll_row, "boxes")
//		ll_weight = dw_rmt_detail.GetItemNumber(ll_row, "weight") 
//		if ll_weight = 0 or ll_boxes = 0 then
//			if ll_weight = 0 and (ll_prev_weight = 0 or IsNull(ll_prev_weight)) Then
//					ls_hold_plant = dw_header.GetItemString(1,'plant_code')
//					ls_hold_code = dw_rmt_detail.GetItemString(ll_row, 'product_code')
//					CONNECT USING SQLCA;	
//					SELECT plt.production_avg_wt
//							,plt.sales_avg_weight
//  					INTO :ll_prod_avg_wgt 
//						 ,:ll_sales_avg_wgt
//  					FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
//  					WHERE ( plt.sku_product_code = :ls_hold_code ) AND  
//        					( plt.plant_code  = :ls_hold_plant)   ;
//		
//					If SQLCA.SQLCode = 00 Then
//						If ll_prod_avg_wgt > 0 Then
//							ll_avg_wgt = ll_prod_avg_wgt
//						Else
//							ll_avg_wgt = ll_sales_avg_wgt
//						End if
//					Else
//						ll_avg_wgt = 0
//					end if
//			Else
//				ll_avg_wgt = ll_prev_weight
//			End if
//		end if
//		//		if (ll_boxes > 0 and ll_weight = 0) then
//		dw_rmt_detail.SetItem(ll_row, "weight", ll_boxes * ll_avg_wgt)
//		ll_weight = dw_rmt_detail.GetItemNumber(ll_row, "weight") 
////		End if	
//		if ll_boxes = 0 and (ll_prev_boxes = 0 or IsNull(ll_prev_boxes)) Then
//				ls_hold_plant = dw_header.GetItemString(1,'plant_code')
//				ls_hold_code = dw_rmt_detail.GetItemString(ll_row, 'product_code')
//				CONNECT USING SQLCA;	
//				SELECT plt.production_avg_wt
//						,plt.sales_avg_weight
//  				INTO :ll_prod_avg_wgt 
//					 	 ,:ll_sales_avg_wgt
//  				FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
//  				WHERE ( plt.sku_product_code = :ls_hold_code ) AND  
//        				( plt.plant_code  = :ls_hold_plant)   ;
//		
//				If SQLCA.SQLCode = 00 Then
//					If ll_prod_avg_wgt > 0 Then
//						ll_avg_wgt = ll_prod_avg_wgt
//					Else
//						ll_avg_wgt = ll_sales_avg_wgt
//					End if
//				Else
//					ll_avg_wgt = 0
//				end if
////		Else
////				ll_avg_wgt = ll_prev_weight
//		End if
//			dw_rmt_detail.SetItem(ll_row, "boxes", ll_weight / ll_avg_wgt)
//	End if	 
////// sap
//		ll_weight = dw_rmt_detail.GetItemNumber(ll_row, "weight") 
//		ll_boxes = dw_rmt_detail.GetItemNumber(ll_row, "boxes")
//		End If
////		ll_weight = dw_rmt_detail.GetItemNumber(ll_row, "weight") 

//		If (ll_qty = 0) And (ll_wgt <> 0) Then
//			SetMicroHelp("Pounds must be zero if quantity is zero.")
//			dw_manage_inventory_detail_fresh.SetRow(ll_row)
//			dw_manage_inventory_detail_fresh.ScrollToRow(ll_row)
//			dw_manage_inventory_detail_fresh.SetColumn("quantity")
//			dw_manage_inventory_detail_fresh.SetFocus()
//			return false
//		End If
//	End If

//Loop while ll_row > 0 


ls_header_string = is_input
ls_UpdateRMDS = iw_frame.iu_string.nf_BuildUpdateString(dw_rmt_detail)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp41cr_upd_rmt_consumption"
istr_error_info.se_message = Space(71)

//If not iu_pas201.nf_pasp41cr_upt_rmt_consumption(istr_error_info, ls_UpdateRMDS, ls_header_string) Then  Return False

If not iu_ws_pas2.nf_pasp41gr(ls_UpdateRMDS, ls_header_string,istr_error_info) Then  Return False

dw_rmt_detail.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")
ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_rmt_detail.SetItem(li_Counter, 'update_flag', ' ')
Next
dw_rmt_detail.ResetUpdate()
dw_rmt_detail.SetSort("product_code A, production_date A, product_state A, product_status A")
dw_rmt_detail.Sort()
dw_rmt_detail.SetFocus()
//dw_rmt_detail.SelectRow(0, FALSE)
dw_rmt_detail.SetReDraw(True)
dw_rmt_detail.GroupCalc()
ib_reinquire = True

If dw_header.GetItemString(1,"mts_pa_inq") = 'P' Then 
	wf_retrieve()
else
	rb_deselect.Checked = FALSE
	rb_select.Checked = FALSE
End if

Return( True )
end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_ws_pas2) Then Destroy(iu_ws_pas2)




end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
//iw_frame.im_menu.mf_disable('m_save')
//iw_frame.im_menu.mf_disable('m_delete')
//iw_frame.im_menu.mf_disable('m_new')
//iw_frame.im_menu.mf_disable('m_addrow')
//iw_frame.im_menu.mf_disable('m_deleterow')
//
//iw_frame.im_menu.mf_Disable('m_next')
//iw_frame.im_menu.mf_Disable('m_previous')
//
//iw_frame.im_menu.mf_enable('m_print')
//iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

	

end event

event ue_query;call super::ue_query;
wf_retrieve()
end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')

iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')
iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')


end event

event open;call super::open;String				ls_text

dw_header.InsertRow(0)
dw_rmt_detail.InsertRow(0)


end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

iu_ws_pas2 = Create u_ws_pas2
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Raw Material Consumption"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")

end event

on w_raw_mat_consumption.create
int iCurrent
call super::create
this.rb_deselect=create rb_deselect
this.rb_select=create rb_select
this.sched_products_only=create sched_products_only
this.dw_consumption_scroll_product=create dw_consumption_scroll_product
this.dw_header=create dw_header
this.dw_rmt_detail=create dw_rmt_detail
this.gb_selection=create gb_selection
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_deselect
this.Control[iCurrent+2]=this.rb_select
this.Control[iCurrent+3]=this.sched_products_only
this.Control[iCurrent+4]=this.dw_consumption_scroll_product
this.Control[iCurrent+5]=this.dw_header
this.Control[iCurrent+6]=this.dw_rmt_detail
this.Control[iCurrent+7]=this.gb_selection
end on

on w_raw_mat_consumption.destroy
call super::destroy
destroy(this.rb_deselect)
destroy(this.rb_select)
destroy(this.sched_products_only)
destroy(this.dw_consumption_scroll_product)
destroy(this.dw_header)
destroy(this.dw_rmt_detail)
destroy(this.gb_selection)
end on

event ue_get_data(string as_value);call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Plant_description' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_description')	
	Case 'sched_date'
		Message.StringParm = string(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
//	Case 'Product_State'
//		Message.StringParm = dw_header.GetItemString(1, 'Product_State')
//	//1-13 jac
//	Case 'Product_Status'
//		Message.StringParm = dw_header.GetItemString(1, 'Product_Status')
//	//
   Case 'Shift'
		Message.StringParm = dw_header.GetItemString(1, 'Shift')	
	Case 'mts_pa_inq'
		Message.StringParm = dw_header.GetItemString(1, 'mts_pa_inq')	
	Case 'pa_shift'
		Message.StringParm = dw_header.GetItemString(1, 'pa_shift')	
	Case 'from_time'
		Message.StringParm = dw_header.GetItemString(1, 'from_time')	
	Case 'to_time'
		Message.StringParm = dw_header.GetItemString(1, 'to_time')	
	Case 'consumption_choice'
		Message.StringParm = dw_header.GetItemString(1, 'consumption_choice')	
End Choose

// comment by sap

end event

event ue_set_data(string as_data_item, string as_value);call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant_Desc' 
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'sched_date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
	Case 'Shift'
		dw_header.SetItem(1, 'Shift', as_value)	
//	Case 'Product_State'
//	   dw_header.SetItem(1, 'Product_State', as_value)
	//1-13 jac
//   Case 'Product_Status'
//		dw_header.SetItem(1, 'Product_Status', as_value)
	//
	Case 'mts_pa_inq'
		dw_header.SetItem(1, 'mts_pa_inq', as_value)	
	Case 'pa_shift'
		dw_header.SetItem(1, 'pa_shift', as_value)	
	Case 'from_time'
		dw_header.SetItem(1, 'from_time', as_value)	
	Case 'to_time'
		dw_header.SetItem(1, 'to_time', as_value)	
	Case 'consumption_choice'
		dw_header.SetItem(1, 'consumption_choice', as_value)	
		
End Choose

end event

event resize;call super::resize;integer li_x		
integer li_y
integer ly_height = 115

li_x = (dw_rmt_detail.x * 2) + 30 
//li_y = dw_rmt_detail.y + 115

if il_BorderPaddingHeight > ly_height Then
                ly_height = il_BorderPaddingHeight
End If

li_y = dw_rmt_detail.y + ly_height


if width > li_x Then
	dw_rmt_detail.width	= width - li_x
end if

if height > li_y then
	dw_rmt_detail.height	= height - li_y
end if
end event

type rb_deselect from radiobutton within w_raw_mat_consumption
boolean visible = false
integer x = 1799
integer y = 186
integer width = 344
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Deselect All"
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	 dw_rmt_detail.SelectRow(li_counter,False)
	 
Next	

end event

type rb_select from radiobutton within w_raw_mat_consumption
boolean visible = false
integer x = 1799
integer y = 96
integer width = 344
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Select All"
end type

event clicked;Integer		li_counter

Long			ll_RowCount

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	 dw_rmt_detail.SelectRow(li_counter,True)
	
Next
end event

type sched_products_only from checkbox within w_raw_mat_consumption
integer x = 1627
integer y = 483
integer width = 622
integer height = 83
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Sched Products Only"
end type

event clicked;String	ls_filter

ls_filter = ""
if sched_products_only.checked then 
		ls_filter = "sched_product = 'Y'"
//	else
//		ls_filter = "sched_product = 'N'"
End if
dw_rmt_detail.SetFilter(ls_filter)
dw_rmt_detail.Filter()
dw_rmt_detail.Sort()
end event

type dw_consumption_scroll_product from u_base_dw_ext within w_raw_mat_consumption
integer x = 22
integer y = 474
integer width = 1595
integer height = 93
integer taborder = 10
string dataobject = "d_consumption_scroll_product"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
ib_updateable = False
end event

event editchanged;call super::editchanged;Long	ll_row, &
		ll_first_row, &
		ll_last_row

ll_row = dw_rmt_detail.Find("product_code >= '" + data + "'",1,dw_rmt_detail.RowCount()+1)

If ll_row > 0 Then 
	dw_rmt_detail.ScrollToRow(ll_row)
	dw_rmt_detail.SetRow(ll_row + 1)
End If

ll_first_row = Long(dw_rmt_detail.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_rmt_detail.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_rmt_detail.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_rmt_detail.ScrollToRow(ll_row)
	dw_rmt_detail.SetRow(ll_row + 1)
	dw_rmt_detail.SetRedraw(True)
End If
end event

type dw_header from u_base_dw_ext within w_raw_mat_consumption
integer x = 260
integer y = 3
integer width = 1360
integer height = 429
integer taborder = 0
string dataobject = "d_rmt_consumption_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False
DataWindowChild		ldwc_type, ldwc_status


This.GetChild("product_state", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PRDSTATE")


//1-13 JAC  tried this to get full word description instead of code
//into status field on header but this code is not hit but on the very 
//time in
This.GetChild("product_status", ldwc_status)

ldwc_status.SetTransObject(SQLCA)
ldwc_status.Retrieve("PRODSTAT")
//



end event

event ue_postconstructor;call super::ue_postconstructor;
//This.SetItem(1, 'effective_pending_ind', 'P')
end event

type dw_rmt_detail from u_base_dw_ext within w_raw_mat_consumption
integer x = 26
integer y = 685
integer width = 2564
integer height = 1165
integer taborder = 30
string dataobject = "d_rmt_consumption_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return

If dw_header.GetItemString(1,"mts_pa_inq") = 'P' Then return

IF row > 0 Then
	If IsSelected(row) Then
		SelectRow(row, FALSE)
	Else
//		SelectRow(0, False)
		SelectRow(row, TRUE)
	End If
	This.SetRow(Row)
End If





end event

event constructor;call super::constructor;ib_updateable = True
//This.SetItem(1, 'from_eff_date', Today())
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

//dw_rmt_detail.GetChild('uom', ldwc_state)
//ldwc_state.SetTransObject(SQLCA)
//ldwc_state.Retrieve("RAWUOM")
//
//dw_rmt_detail.GetChild('product_state', ldwc_state)
//ldwc_state.SetTransObject(SQLCA)
//ldwc_state.Retrieve("PRDSTATE")
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus
int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row, &
				ll_boxes, ll_wgt, &
				ll_prod_avg_wgt, & 
			 	ll_sales_avg_wgt, &
				ll_avg_wgt, ll_prev_avg_wgt, &
				ll_prev_weight, ll_prev_boxes, &
				ll_weight

string		ls_GetText, &
				ls_tem, ls_temp, &
				ls_prod_descr, &
				ls_hold_code, &
				ls_hold_plant
Boolean		lb_changed
				
Date			ldt_temp	

nvuo_pa_business_rules	u_rule


is_ColName = GetColumnName()
ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0

CHOOSE CASE is_ColName
	CASE "product_code"
		If not invuo_fab_product_code.uf_check_product(data) then
			If	invuo_fab_product_code.ib_error_occurred then
				iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
				Return 1
			Else				
				iw_frame.SetMicroHelp(data + " is an invalid Product Code")
				Return 1
				End If
		else		
			ls_prod_descr = invuo_fab_product_code.uf_get_product_description()
			this.setitem(row, "product_description", ls_prod_descr)
		End If
	CASE "production_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			this.setfocus( )
			this.setcolumn("production_date") 
			This.SelectText(1, Len(data))	
		//	This.selecttext(1,100)
			return 1
		End If
		//ldt_temp = dw_rmt_detail.GetItemDate(ll_source_row, "production_date")
		real(date(ls_getText))
		ldt_temp = date(ls_getText)
		if ldt_temp > today() Then
			iw_frame.SetMicroHelp("Production date must be equal to or less than current date")
			this.setfocus( )
			this.setcolumn("production_date") 
			This.SelectText(1, Len(data))	
		//	This.selecttext(1,100)
			return 1
		End If
	Case "boxes"
		If Not IsNumber(ls_GetText) Then
			iw_frame.SetMicroHelp("Boxes must be a number")
			this.setfocus( )
			this.setcolumn("boxes") 
			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
			return 1
		End if
		If Real(ls_GetText) < 0 Then
			iw_frame.SetMicroHelp("Boxes cannot be negative")
			this.setfocus( )
			this.setcolumn("boxes") 
			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
			return 1
			End if
		ll_boxes = long(ls_getText)
		lb_changed = false
		le_rowstatus = This.GetItemStatus(row, 0, Primary!) 
		CHOOSE CASE le_RowStatus
			CASE New!, NewModified!
				lb_changed = true
		end choose		
	//	If le_rowstatus = NotModified! or New! then
	//	if lb_changed = true  or
 		if This.GetItemStatus(row, "weight", Primary!) = NotModified!	 Then 
				lb_changed = true
			end if	
	//		This.GetItemStatus(row, "weight", Primary!) = New! Then
	//	Then
	if lb_changed = true then
			ll_prev_weight = dw_rmt_detail.GetItemNumber(row, "prev_weight")
			ll_prev_boxes = dw_rmt_detail.GetItemNumber(row, "prev_boxes")
			ll_weight = dw_rmt_detail.GetItemNumber(row, "weight") 
			if ll_weight = 0 and (ll_prev_weight = 0 or IsNull(ll_prev_weight)) Then
				ls_hold_plant = dw_header.GetItemString(1,'plant_code')
				ls_hold_code = dw_rmt_detail.GetItemString(row, 'product_code')
				CONNECT USING SQLCA;	
				SELECT plt.production_avg_wt
						,plt.sales_avg_weight
  				INTO :ll_prod_avg_wgt 
					 ,:ll_sales_avg_wgt
  	//			FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
	  			FROM sku_plant plt
  				WHERE ( plt.sku_product_code = :ls_hold_code ) AND  
     					( plt.plant_code  = :ls_hold_plant)   ;
		
				If SQLCA.SQLCode = 00 Then
					If ll_prod_avg_wgt > 0 Then
						ll_avg_wgt = ll_prod_avg_wgt
					Else
						ll_avg_wgt = ll_sales_avg_wgt
					End if
				Else
					ll_avg_wgt = 0
				end if
			Else
				if ll_prev_boxes = 0 then
					ll_avg_wgt = 0
				else
					ll_avg_wgt = ll_prev_weight / ll_prev_boxes
				end if
			End if
		dw_rmt_detail.SetItem(row, "weight", ll_boxes * ll_avg_wgt)	
		This.SetItemStatus(row, "weight", Primary!, NotModified!)
	end if

			
	Case "weight"
		If Not IsNumber(ls_GetText) Then
			iw_frame.SetMicroHelp("Weight must be a number")
			this.setfocus( )
			this.setcolumn("weight") 
			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
			return 1
		End if
		If Real(ls_GetText) < 0 Then
			iw_frame.SetMicroHelp("Weight cannot be negative")
			this.setfocus( )
			this.setcolumn("weight") 
			This.SelectText(1, Len(data))	
//			This.selecttext(1,100)
			return 1
		End if	
		ll_weight = long(ls_getText)
		lb_changed = false
		le_rowstatus = This.GetItemStatus(row, 0, Primary!) 
		CHOOSE CASE le_RowStatus
			CASE New!, NewModified!
				lb_changed = true
		end choose		
		If This.GetItemStatus(row, "boxes", Primary!) = NotModified!	 Then 
				lb_changed = true
		end if		
		if lb_changed = true then
			ll_prev_weight = dw_rmt_detail.GetItemNumber(row, "prev_weight")
			ll_prev_boxes = dw_rmt_detail.GetItemNumber(row, "prev_boxes")
			ll_boxes = dw_rmt_detail.GetItemNumber(row, "boxes") 
			if ll_boxes = 0 and (ll_prev_boxes = 0 or IsNull(ll_prev_boxes)) Then
				ls_hold_plant = dw_header.GetItemString(1,'plant_code')
				ls_hold_code = dw_rmt_detail.GetItemString(row, 'product_code')
				CONNECT USING SQLCA;	
				SELECT plt.production_avg_wt
						,plt.sales_avg_weight
  				INTO :ll_prod_avg_wgt 
					 ,:ll_sales_avg_wgt
 // 				FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
				FROM sku_plant plt
  				WHERE ( plt.sku_product_code = :ls_hold_code ) AND  
     					( plt.plant_code  = :ls_hold_plant)   ;
		
				If SQLCA.SQLCode = 00 Then
					If ll_prod_avg_wgt > 0 Then
						ll_avg_wgt = ll_prod_avg_wgt
					Else
						ll_avg_wgt = ll_sales_avg_wgt
					End if
				Else
					ll_avg_wgt = 0
				end if
			Else
				ll_avg_wgt = ll_prev_weight / ll_prev_boxes
			End if
			
		if ll_avg_wgt = 0 then
			dw_rmt_detail.SetITem(row, "boxes", 0)
		else
			dw_rmt_detail.SetItem(row, "boxes", ll_weight / ll_avg_wgt)	
		end if
		
		This.SetItemStatus(row, "boxes", Primary!, NotModified!)
		end if
			
END CHOOSE

// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

//parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;return (1)
end event

type gb_selection from groupbox within w_raw_mat_consumption
boolean visible = false
integer x = 1788
integer y = 48
integer width = 406
integer height = 234
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
end type

