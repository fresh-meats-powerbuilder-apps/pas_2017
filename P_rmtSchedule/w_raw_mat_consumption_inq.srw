HA$PBExportHeader$w_raw_mat_consumption_inq.srw
forward
global type w_raw_mat_consumption_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_raw_mat_consumption_inq
end type
type dw_mts_pa_radio from u_mts_pa_radio within w_raw_mat_consumption_inq
end type
type dw_sched_date from u_sched_date within w_raw_mat_consumption_inq
end type
type dw_raw_mat_consumption_choice from datawindow within w_raw_mat_consumption_inq
end type
type dw_shift from u_shift within w_raw_mat_consumption_inq
end type
type dw_from_to_time from u_from_to_time within w_raw_mat_consumption_inq
end type
type dw_pa_shift from u_shift within w_raw_mat_consumption_inq
end type
type st_pa_shift from statictext within w_raw_mat_consumption_inq
end type
end forward

global type w_raw_mat_consumption_inq from w_base_response_ext
integer x = 87
integer y = 176
integer width = 1851
integer height = 1160
string title = "Raw Material Consumption Inquire"
long backcolor = 67108864
event begindatechanged ( )
event ue_get_params ( string as_value )
event ue_get_default_times ( )
dw_plant dw_plant
dw_mts_pa_radio dw_mts_pa_radio
dw_sched_date dw_sched_date
dw_raw_mat_consumption_choice dw_raw_mat_consumption_choice
dw_shift dw_shift
dw_from_to_time dw_from_to_time
dw_pa_shift dw_pa_shift
st_pa_shift st_pa_shift
end type
global w_raw_mat_consumption_inq w_raw_mat_consumption_inq

type variables
Boolean		ib_valid_return, &
				ib_retrieved
w_base_sheet	iw_parent

string	is_begin_date

u_pas201		iu_pas201

u_ws_pas2      iu_ws_pas2

s_error		istr_error_info
end variables

forward prototypes
public function boolean wf_get_default_times ()
public function boolean wf_validate_time ()
end prototypes

event ue_get_params(string as_value);Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_plant.GetItemString(1, 'location_code')
	Case 'Plant_description' 
		Message.StringParm = dw_plant.GetItemString(1, 'location_name')	
	Case 'sched_date'
		Message.StringParm = string(dw_sched_date.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd')
End Choose

end event

event ue_get_default_times();wf_get_default_times()
end event

public function boolean wf_get_default_times ();Int		li_ret
	

String	ls_header, &
			ls_detail, &
			ls_temp, &
			ls_plant, &
			ls_date
			
Long		ll_rowcount
			
w_raw_mat_consumption 	lw_parent


If ib_retrieved then return True

If dw_plant.AcceptText() = -1 or &
	dw_sched_date.AcceptText() = -1 Then
	return True
End if

dw_from_to_time.SetRedraw(False)

if dw_pa_shift.rowcount( ) = 0 Then
	dw_pa_shift.insertrow( 0)
End If
if dw_from_to_time.rowcount( ) = 0 Then
	dw_from_to_time.insertrow( 0)
End If

lw_parent = This.GetParent()
ib_retrieved = TRUE

SetPointer(Hourglass!)

w_raw_mat_consumption_inq.Event ue_get_params('Plant')
ls_plant = Message.StringParm

w_raw_mat_consumption_inq.Event ue_get_params('sched_date')
ls_date = Message.StringParm

ls_header = ls_plant + '~t' + ls_date + '~t'


//IF Not IsValid( iu_pas201 ) THEN
//	iu_pas201	=  CREATE u_pas201
//END IF

IF Not IsValid(iu_ws_pas2) THEN
	iu_ws_pas2	=  CREATE u_ws_pas2
END IF

//li_ret = iu_pas201.nf_pasp83cr_inq_def_times_rmt(istr_error_info, &
//										ls_header, &
//										ls_detail)										

li_ret = iu_ws_pas2.nf_pasp83gr(ls_header, &
										ls_detail,istr_error_info)	

If li_ret = 0 Then
	dw_from_to_time.Reset()
	dw_from_to_time.ImportString(ls_detail)
	dw_from_to_time.AcceptText()
//	dw_from_to_time.ResetUpdate()
end if

dw_from_to_time.SetRedraw(True)
return true
end function

public function boolean wf_validate_time ();Int		li_ret, li_ret_2
	

String	ls_header, &
			ls_detail, &
			ls_temp, &
			ls_plant, &
			ls_date
			
Long		ll_rowcount, &
			ll_temp
			

If iw_frame.iu_string.nf_IsEmpty(dw_from_to_time.GetItemString(1, "from_time")) Then
	iw_frame.SetMicroHelp("From Time is a required field")
	dw_from_to_time.SetFocus()
	return False
End if

If iw_frame.iu_string.nf_IsEmpty(dw_from_to_time.GetItemString(1, "to_time")) Then
	iw_frame.SetMicroHelp("To Time is a required field")
	dw_from_to_time.SetFocus()
	return False
End if

If iw_frame.iu_string.nf_IsEmpty(dw_pa_shift.GetItemString(1, "shift")) Then
	iw_frame.SetMicroHelp("PA Shift is a required field")
	dw_pa_shift.SetFocus()
	return False
End if

li_ret = Long(dw_from_to_time.GetItemString(1, "from_time"))

If Long(Mid(dw_from_to_time.GetItemString(1, "from_time"), 1, 2)) > 23 or &
	Long(Mid(dw_from_to_time.GetItemString(1, "from_time"), 3, 2)) > 59  or &
	li_ret = 0 Then
		iw_frame.SetMicroHelp("Invalid From Time. Format is HHMI")
		dw_from_to_time.SetFocus()
		dw_from_to_time.SetColumn("from_time")
		return False
End If

li_ret = Long(dw_from_to_time.GetItemString(1, "to_time"))

If Long(Mid(dw_from_to_time.GetItemString(1, "to_time"), 1, 2)) > 23 or &
	Long(Mid(dw_from_to_time.GetItemString(1, "to_time"), 3, 2)) > 59 or &
	li_ret = 0 Then
	iw_frame.SetMicroHelp("Invalid To Time. Format is HHMI")
	dw_from_to_time.SetFocus()
	dw_from_to_time.SetColumn("to_time")
	return False
End If

Return True
end function

event ue_postopen;call super::ue_postopen;Int							li_pos, &
								li_ret
String						ls_header, &
								ls_temp, &
								ls_production_date, &
								ls_product_descr,ls_state, &
								ls_mts_pa_inq, &
								ls_shift, ls_date, &
								ls_product_status, ls_consumption_choice

u_string_functions		lu_string

Window   lw_parent

lw_parent = this.parentwindow()
This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2) 

iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If

iw_parent.Event ue_Get_Data('Shift')
ls_shift = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_shift) Then dw_shift.uf_set_shift(ls_shift)

iw_parent.Event ue_get_data('sched_date')
ls_date = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_date) Then dw_sched_date.uf_set_sched_date(date(ls_date))

//iw_parent.Event ue_Get_Data('Product_State')
//ls_state = Message.StringParm
//
iw_parent.Event ue_get_data('mts_pa_inq')
ls_mts_pa_inq = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_mts_pa_inq) Then dw_mts_pa_radio.uf_set_value(ls_mts_pa_inq)

If dw_plant.RowCount() < 1 Then 
	dw_plant.InsertRow(0)
ElseIf Not lu_string.nf_IsEmpty(dw_plant.GetItemString(1,1)) Then
	dw_plant.setfocus( )
End if

iw_parent.Event ue_get_data('consumption_choice')
ls_consumption_choice = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_consumption_choice) Then 
   dw_raw_mat_consumption_choice.SetItem(1, "consumption_choice", ls_consumption_choice)
else
	dw_raw_mat_consumption_choice.SetItem(1, "consumption_choice", "D")
end if

ls_temp = dw_raw_mat_consumption_choice.GetItemString(1, "consumption_choice")

CHOOSE CASE ls_temp
	Case 'D'
		dw_shift.Visible = False
		dw_from_to_time.Visible = False
		dw_pa_shift.Visible = False
		st_pa_shift.Visible = False
	Case 'S'
		dw_shift.Visible = True
		dw_from_to_time.Visible = False
		dw_pa_shift.Visible = False
		st_pa_shift.Visible = False
	Case 'T'
		dw_shift.Visible = False
		dw_from_to_time.Visible = True
		dw_pa_shift.Visible = True
		st_pa_shift.Visible = True
	Case Else
		dw_shift.Visible = False
		dw_from_to_time.Visible = False
		dw_pa_shift.Visible = False
		st_pa_shift.Visible = False
END CHOOSE

iw_parent.Event ue_Get_Data('from_time')
ls_temp = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_temp) Then dw_from_to_time.uf_set_from_time(ls_temp)

iw_parent.Event ue_Get_Data('to_time')
ls_temp = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_temp) Then dw_from_to_time.uf_set_to_time(ls_temp)

iw_parent.Event ue_Get_Data('pa_shift')
ls_shift = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_shift) Then dw_pa_shift.uf_set_shift(ls_shift)

//dw_shift.Visible = False
//dw_from_to_time.Visible = False
//dw_pa_shift.Visible = False
//st_pa_shift.Visible = False
//

end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

if dw_sched_date.rowcount( ) = 0 Then
	dw_sched_date.insertrow( 0)
End If

//if dw_state.rowcount( ) = 0 Then
//	dw_state.insertrow( 0)
//End If

if dw_shift.rowcount( ) = 0 Then
	dw_shift.insertrow( 0)
End If
//1-13 jac
//if dw_product_status.rowcount( ) = 0 Then
//	dw_product_status.insertrow( 0)
//End If
//
if dw_raw_mat_consumption_choice.rowcount( ) = 0 Then
	dw_raw_mat_consumption_choice.insertrow( 0)
End If


end event

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

on w_raw_mat_consumption_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_mts_pa_radio=create dw_mts_pa_radio
this.dw_sched_date=create dw_sched_date
this.dw_raw_mat_consumption_choice=create dw_raw_mat_consumption_choice
this.dw_shift=create dw_shift
this.dw_from_to_time=create dw_from_to_time
this.dw_pa_shift=create dw_pa_shift
this.st_pa_shift=create st_pa_shift
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_mts_pa_radio
this.Control[iCurrent+3]=this.dw_sched_date
this.Control[iCurrent+4]=this.dw_raw_mat_consumption_choice
this.Control[iCurrent+5]=this.dw_shift
this.Control[iCurrent+6]=this.dw_from_to_time
this.Control[iCurrent+7]=this.dw_pa_shift
this.Control[iCurrent+8]=this.st_pa_shift
end on

on w_raw_mat_consumption_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_mts_pa_radio)
destroy(this.dw_sched_date)
destroy(this.dw_raw_mat_consumption_choice)
destroy(this.dw_shift)
destroy(this.dw_from_to_time)
destroy(this.dw_pa_shift)
destroy(this.st_pa_shift)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok();call super::ue_base_ok;Date	ldt_temp, &
		date1, &
		date2

String	ls_temp,ls_temp1
Long 		ll_rtn
Integer	li_rtn

//If dw_plant.AcceptText() = -1 or &
//	dw_sched_date.AcceptText() = -1 or &
//	dw_state.AcceptText() = -1 
//	Then 
//	dw_plant.SetFocus()
//	return
//End if
//


If dw_plant.AcceptText() = -1 or &
	dw_raw_mat_consumption_choice.AcceptText() = -1 or &
	dw_mts_pa_radio.AcceptText() = -1 or &
	dw_sched_date.AcceptText() = -1 	then
	return
End if


If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant Code is a required field")
	dw_plant.SetFocus()
	return
End if


//If iw_frame.iu_string.nf_IsEmpty(dw_state.GetItemString(1, "product_state")) Then
//	iw_frame.SetMicroHelp("Product State is a required field")
//	dw_state.SetFocus()
//	return
//End if

//If iw_frame.iu_string.nf_IsEmpty(dw_production_date.GetItemString(1, "production_date")) Then
//	iw_frame.SetMicroHelp("Production date is a required field")
//	dw_production_date.SetFocus()
//	return
//End if

//1-13 jac
//If iw_frame.iu_string.nf_IsEmpty(dw_product_status.GetItemString(1, "product_status")) Then
//	iw_frame.SetMicroHelp("Product Status is a required field")
//	dw_product_status.SetFocus()
//	return
//End if

ls_temp = dw_raw_mat_consumption_choice.GetItemString(1, "consumption_choice" ) 
CHOOSE CASE ls_temp
	Case 'D'
		
	Case 'S'
		If dw_shift.AcceptText() = -1 Then
			return
		End if
		If iw_frame.iu_string.nf_IsEmpty(dw_shift.GetItemString(1, "Shift")) Then
			iw_frame.SetMicroHelp("Shift is a required field")
			dw_shift.SetFocus()
			return
		End if
		ls_temp = dw_shift.uf_get_shift( )
		iw_parent.Event ue_Set_Data('Shift', ls_temp)
	Case 'T'
		If dw_pa_shift.AcceptText() = -1 or &
			dw_from_to_time.AcceptText() = -1 then
			return
		End if
		
		if not wf_validate_time() Then
			return
		end if
		ls_temp = dw_from_to_time.uf_get_from_time( )
		iw_parent.Event ue_Set_Data('from_time', ls_temp)
				
		ls_temp = dw_from_to_time.uf_get_to_time( )
		iw_parent.Event ue_Set_Data('to_time', ls_temp)

		
		ls_temp = dw_pa_shift.uf_get_shift( )
		iw_parent.Event ue_Set_Data('pa_shift', ls_temp)
END CHOOSE

//If iw_frame.iu_string.nf_IsEmpty(dw_shift.GetItemString(1, "Shift")) Then
//	iw_frame.SetMicroHelp("Shift is a required field")
//	dw_shift.SetFocus()
//	return
//End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant_Desc', ls_temp)


ldt_temp = dw_sched_date.GetItemDate(1, "sched_date")
//ls_temp = String(ldt_temp, 'yyyy-mm-dd')
iw_parent.Event ue_Set_Data('sched_date', ls_temp)
iw_parentwindow.Event ue_set_data('sched_date', &
		String(ldt_temp, 'yyyy-mm-dd'))
date1 = Today()
date2 = ldt_temp

li_rtn = Daysafter(date1, date2)
if li_rtn < -7 or li_rtn > 0 Then
	iw_frame.setmicrohelp("Date must be between current date minus 7 days and current date")
	dw_sched_date.setfocus()
	return 
end if
//ls_temp = dw_shift.uf_get_shift( )
//iw_parent.Event ue_Set_Data('Shift', ls_temp)

//ls_temp = dw_state.uf_get_product_state( )
//iw_parent.Event ue_Set_Data('Product_State', ls_temp)
////1-13 jac
//ls_temp = dw_product_status.uf_get_product_status( )
//iw_parent.Event ue_Set_Data('Product_Status', ls_temp)
////
ls_temp = dw_mts_pa_radio.GetItemString(1, "mts_pa_inq" )
iw_parent.Event ue_Set_Data('mts_pa_inq', ls_temp)

ls_temp = dw_raw_mat_consumption_choice.GetItemString(1, "consumption_choice")
iw_parent.Event ue_Set_Data('consumption_choice', ls_temp)
//ls_temp = iw_parent.Event ue_set_data('mts_pa_inq', &
//		dw_header.GetItemString(1, 'mts_pa_inq'))
//
ib_valid_return = True
Close(This)
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_raw_mat_consumption_inq
boolean visible = false
integer x = 1696
integer y = 392
integer taborder = 120
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_raw_mat_consumption_inq
integer x = 731
integer y = 900
integer taborder = 110
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_raw_mat_consumption_inq
integer x = 274
integer y = 904
integer height = 104
integer taborder = 100
end type

type dw_plant from u_plant within w_raw_mat_consumption_inq
integer x = 224
integer y = 12
integer taborder = 10
end type

event itemchanged;call super::itemchanged;string 	ls_temp

ib_retrieved = False
ls_temp = dw_raw_mat_consumption_choice.GetItemString(1, "consumption_choice")
if ls_temp = 'T' Then
	Parent.TriggerEvent("ue_get_default_times")
end if
end event

type dw_mts_pa_radio from u_mts_pa_radio within w_raw_mat_consumption_inq
integer x = 219
integer y = 600
integer taborder = 70
boolean bringtotop = true
end type

event constructor;call super::constructor;uf_enable(true)
end event

event itemchanged;call super::itemchanged;dw_from_to_time.Visible = False
dw_pa_shift.Visible = False

if dw_raw_mat_consumption_choice.GetItemString(1,"consumption_choice") = 'T' Then
	dw_raw_mat_consumption_choice.SetItem(1,"consumption_choice", "D")
end if
end event

type dw_sched_date from u_sched_date within w_raw_mat_consumption_inq
integer x = 174
integer y = 112
integer taborder = 20
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;string 	ls_temp

ib_retrieved = False
ls_temp = dw_raw_mat_consumption_choice.GetItemString(1, "consumption_choice")
if ls_temp = 'T' Then
	Parent.TriggerEvent("ue_get_default_times")
end if
end event

type dw_raw_mat_consumption_choice from datawindow within w_raw_mat_consumption_inq
integer x = 64
integer y = 220
integer width = 398
integer height = 388
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_raw_mat_consumption_choice"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;CHOOSE CASE data
	Case 'D'
		dw_shift.Visible = False
		dw_from_to_time.Visible = False
		dw_pa_shift.Visible = False
		st_pa_shift.Visible = False
	Case 'S'
		dw_shift.Visible = True
		dw_from_to_time.Visible = False
		dw_pa_shift.Visible = False
		st_pa_shift.Visible = False
	Case 'T'
		if dw_mts_pa_radio.GetItemString(1, "mts_pa_inq" ) = 'P' Then
			iw_frame.SetMicroHelp("Time is not a valid option for PA consumption.")
			Return 2
		else	
			dw_shift.Visible = False
			dw_from_to_time.Visible = True
			dw_pa_shift.Visible = True
			st_pa_shift.Visible = True
			if ib_retrieved = False Then
//				wf_get_default_times()
				Parent.TriggerEvent("ue_get_default_times")
	 		end if
		end if
	Case Else
		dw_shift.Visible = False
		dw_from_to_time.Visible = False
		dw_pa_shift.Visible = False
		st_pa_shift.Visible = False
END CHOOSE

end event

event constructor;//uf_enable(true)
end event

type dw_shift from u_shift within w_raw_mat_consumption_inq
integer x = 334
integer y = 380
integer width = 361
integer height = 76
integer taborder = 40
boolean bringtotop = true
end type

event constructor;call super::constructor;this.object.shift_t.Text = ' '
uf_enable(true)
end event

type dw_from_to_time from u_from_to_time within w_raw_mat_consumption_inq
event type long ue_get_times ( )
integer x = 462
integer y = 496
integer taborder = 50
boolean bringtotop = true
end type

event type long ue_get_times();wf_get_default_times()
return 0
end event

event getfocus;call super::getfocus;This.SelectText(1, Len(This.GetText()))
end event

event itemchanged;call super::itemchanged;dw_shift.Visible = False
end event

type dw_pa_shift from u_shift within w_raw_mat_consumption_inq
integer x = 1413
integer y = 504
integer width = 361
integer height = 76
integer taborder = 60
boolean bringtotop = true
end type

event constructor;call super::constructor;this.object.shift_t.Text = ' '
uf_enable(true)
end event

type st_pa_shift from statictext within w_raw_mat_consumption_inq
integer x = 1358
integer y = 512
integer width = 78
integer height = 72
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "PA"
boolean focusrectangle = false
end type

