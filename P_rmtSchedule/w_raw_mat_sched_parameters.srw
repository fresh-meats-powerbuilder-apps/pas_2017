HA$PBExportHeader$w_raw_mat_sched_parameters.srw
$PBExportComments$Defauld Sched Parameters
forward
global type w_raw_mat_sched_parameters from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_raw_mat_sched_parameters
end type
type dw_rmt_detail from u_base_dw_ext within w_raw_mat_sched_parameters
end type
end forward

global type w_raw_mat_sched_parameters from w_base_sheet_ext
integer width = 1842
integer height = 1516
string title = "Raw Material Schedule Parameters"
long backcolor = 67108864
event ue_set_detail_handle ( window aw_order_detail )
dw_header dw_header
dw_rmt_detail dw_rmt_detail
end type
global w_raw_mat_sched_parameters w_raw_mat_sched_parameters

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
		ib_toolbarVisible, &
		ib_ReInquire, &
		ib_Dont_Increment_timeout, &
		ib_good_product

DWObject	idwo_last_clicked

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long		il_SelectedColor, &
		il_SelectedTextColor,&
		il_lastclicked_background, &
		il_ChangedRow

s_error		istr_error_info

// This is for the async call
u_pas203		iu_pas203

u_ws_pas2       iu_ws_pas2

String		is_colname, &
				is_product, &
				is_input, &
				is_ChangedColumnName, &
				is_debug, &
				is_start_date, &
				is_end_Date

end variables

forward prototypes
public function string wf_getheaderinfo ()
public subroutine wf_open_carry_over ()
public subroutine wf_set_dates ()
public function boolean wf_addrow ()
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function boolean wf_deleterow ()
public function boolean wf_retrieve ()
public subroutine wf_delete ()
end prototypes

event ue_set_detail_handle(window aw_order_detail);//iw_order_detail = aw_order_detail
 
end event

public function string wf_getheaderinfo ();//return dw_header.Describe("DataWindow.Data")

String		ls_string

//1-13 JAC added product_status
ls_string = dw_header.GetItemString(1, 'plant_code') + '~t' + &
				dw_header.GetItemString(1, 'plant_description') + '~t' + &
				dw_header.GetItemString(1, 'fab_product_code') + '~t' + &
				dw_header.GetItemString(1, 'fab_product_descr') + '~t' + &
				String(dw_header.GetItemnumber(1, 'product_state')) + '~t' + &
				dw_header.GetItemString(1, 'product_status') + '~t' + &
				dw_header.GetItemString(1, 'shift') + '~t' 
return ls_string
end function

public subroutine wf_open_carry_over ();
end subroutine

public subroutine wf_set_dates ();
end subroutine

public function boolean wf_addrow ();//dw_rmt_detail.SetFocus()

Long			ll_row

If dw_rmt_detail.accepttext( ) <> 1 then return false

This.SetRedraw(False)

// Super::wf_addrow()
ll_row = dw_rmt_detail.InsertRow(0)
dw_rmt_detail.SetItem(ll_row, "start_date", Today())
dw_rmt_detail.ScrollToRow(ll_row)

//dw_step_detail.setitem(ll_row,"product_state", wf_default_state())

dw_rmt_detail.SetRow(dw_rmt_detail.RowCount())

dw_rmt_detail.SetColumn("uom")
dw_rmt_detail.SetFocus()
This.SetRedraw(True)

return true


end function

public function boolean wf_validate (long al_row);Date					ldt_start_date, &
						ldt_end_date
//						ldt_found_start_date, &
//						ldt_found_end_date, &
//						ldt_temp

Long					ll_rtn, &
						ll_nbrrows, &
						ll_min_qty, &
						ll_max_qty

//String				ls_start_date, &
//						ls_end_date, &
String				ls_searchstring, &
						ls_uom, &
						ls_update_flag


IF dw_rmt_detail.AcceptText() = -1 THEN Return( False )

ll_nbrrows = dw_rmt_detail.RowCount()

ls_uom = dw_rmt_detail.GetItemString(al_row, "uom")
IF IsNull(ls_uom) or Len(trim(ls_uom)) = 0 Then 
	SetMicroHelp("Please enter a UOM.  UOM cannot be left blank.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("uom")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

// Check that the start date has a value and is > current date and < end date.
ls_update_flag = dw_rmt_detail.GetItemString(al_row, "update_flag")
ldt_start_date = dw_rmt_detail.GetItemDate(al_row, "start_date")
ldt_end_date = dw_rmt_detail.GetItemDate(al_row, "end_date")
ll_min_qty = dw_rmt_detail.GetItemNumber(al_row, "min_quantity_hour")
ll_max_qty = dw_rmt_detail.GetItemNumber(al_row, "max_quantity_hour")
IF IsNull(ldt_start_date) or ldt_start_date <= Date('00/00/0000') Then 
   SetMicroHelp("Please enter a From Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("start_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ldt_start_date < Today() And ls_update_flag = 'A' Then
	SetMicroHelp("Please enter a From Date greater than Current Date")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("start_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If ldt_start_date > ldt_end_date And ls_update_flag = 'A' Then
		SetMicroHelp("Please enter a From Date less than the To Date")
		This.SetRedraw(False)
		dw_rmt_detail.ScrollToRow(al_row)
		dw_rmt_detail.SetColumn("start_date")
		dw_rmt_detail.SetFocus()
		This.SetRedraw(True)
		Return False
	End IF
End If

// Check that the end date has a value and is > current date and start date.

IF IsNull(ldt_end_date) or ldt_end_date <= Date('00/00/0000') Then 
	SetMicroHelp("Please enter a To Date Greater Than 00/00/0000")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("end_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If
If ldt_end_date < Today() Then
	SetMicroHelp("Please enter a To Date Greater Than Current Date")	
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("end_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
Else
	If ldt_end_date < ldt_start_date Then
		SetMicroHelp("Please enter a To Date greater than the From Date")
		This.SetRedraw(False)
		dw_rmt_detail.ScrollToRow(al_row)
		dw_rmt_detail.SetColumn("end_date")
		dw_rmt_detail.SetFocus()
		This.SetRedraw(True)
		Return False
	End IF
End If

If ll_min_qty > ll_max_qty  Then
	SetMicroHelp("Max Quantity must be greater than or equal Min Quantity")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("max_quantity_hour")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

if IsNull(ll_min_qty) Then
	SetMicroHelp("Please enter Min Quantity")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("min_quantity_hour")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

if IsNull(ll_max_qty) Then
	SetMicroHelp("Please enter Max Quantity")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("max_quantity_hour")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

ls_SearchString	= "uom = '" + ls_uom + "'"

dw_rmt_detail.SelectRow(0, False)

ll_rtn = iw_frame.iu_string.nf_compare_dates(al_row, ls_SearchString, dw_rmt_detail) 
if ll_rtn > 0 then
	iw_Frame.SetMicroHelp( "There are duplicate products with the" + &
  					" same UOM and overlapping dates.")
	dw_rmt_detail.SetRedraw(False) 
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("start_date")
	dw_rmt_detail.SetRow(al_row)
	dw_rmt_detail.SelectRow(ll_rtn, True)
	dw_rmt_detail.SelectRow(al_row, True)
	dw_rmt_detail.SetRedraw(True)
	Return False
End If

Return True
end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdateRMDS, &
					ls_header_string

dwItemStatus	lis_status

IF dw_rmt_detail.AcceptText() = -1 THEN Return( False )
IF dw_rmt_detail.ModifiedCount() + dw_rmt_detail.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_ws_pas2 ) THEN
	iu_ws_pas2	=  CREATE u_ws_pas2
END IF

ll_NbrRows = dw_rmt_detail.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_rmt_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		IF iw_frame.iu_string.nf_IsEmpty(dw_rmt_detail.GetItemString(ll_row, "uom")) Then 
			SetMicroHelp("Please choose a UOM")
			This.SetRedraw(False)
			dw_rmt_detail.ScrollToRow(ll_row)
			dw_rmt_detail.SetColumn("uom")
			dw_rmt_detail.SetFocus()
			This.SetRedraw(True)
			Return False
		End If
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_header_string = is_input
ls_UpdateRMDS = iw_frame.iu_string.nf_BuildUpdateString(dw_rmt_detail)

//If not iu_pas203.nf_pasp35cr_upd_rmt_sched_params(istr_error_info, ls_UpdateRMDS, ls_header_string) Then  Return False

If not iu_ws_pas2.nf_pasp35gr(ls_UpdateRMDS, ls_header_string, istr_error_info) Then  Return False

dw_rmt_detail.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_rmt_detail.SetItem(li_Counter, 'update_flag', ' ')
Next
dw_rmt_detail.ResetUpdate()
dw_rmt_detail.SetSort("uom A, start_date A, end_date A, min_quantity_hour A, max_quantity_hour A")
dw_rmt_detail.Sort()
dw_rmt_detail.GroupCalc()
dw_rmt_detail.SetFocus()
dw_rmt_detail.SelectRow(0, FALSE)
dw_rmt_detail.SetReDraw(True)

Return( True )
end function

public function boolean wf_deleterow ();Boolean		lb_ret

dw_rmt_detail.SetFocus()
lb_ret = super::wf_deleterow()

dw_rmt_detail.SelectRow(0,False)
dw_rmt_detail.SelectRow(dw_rmt_detail.GetRow(),True)
return lb_ret
end function

public function boolean wf_retrieve ();Boolean	lb_ret

String	ls_input, ls_modify, &
			ls_output_string
						
Long		ll_value, &
			ll_rtn, & 
			ls_message
			
u_string_functions u_string

Call w_base_sheet::CloseQuery
//ls_message = Message.ReturnValue
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_raw_mat_sched_parameters_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")
is_debug = This.dw_header.GetItemString(1, 'fab_product_code')
//1-13 jac  added product_status
ls_input = This.dw_header.GetItemString(1, "plant_code") + '~t' + &
				This.dw_header.GetItemString(1, "fab_product_code") + '~t' + &
				string(This.dw_header.GetItemnumber(1, "product_state")) + '~t' + &
				This.dw_header.GetItemString(1, "product_status") + '~t' + &
				This.dw_header.GetItemString(1, "shift") + '~r~n'
is_input = ls_input


//lb_ret = iu_pas203.nf_pasp34cr_inq_rmt_sched_params(istr_error_info, &
//									ls_input, &
//									ls_output_string)

lb_ret = iu_ws_pas2.nf_pasp34gr(ls_input, &
									ls_output_string,istr_error_info)

This.dw_rmt_detail.Reset()

If lb_ret Then
	This.dw_rmt_detail.ImportString(ls_output_string)

	ll_value = dw_rmt_detail.RowCount()
	If ll_value < 0 Then ll_value = 0

	dw_rmt_detail.ResetUpdate()

	IF ll_value > 0 THEN
		dw_rmt_detail.SetFocus()
		dw_rmt_detail.ScrollToRow(1)
		dw_rmt_detail.SetColumn( "uom" )
		dw_rmt_detail.TriggerEvent("RowFocusChanged")
	END IF

	SetMicroHelp(String(ll_value) + " rows retrieved")
//	This.SetRedraw( True )
	dw_rmt_detail.SetSort("uom A, start_date A, end_date A, min_quantity_hour A, max_quantity_hour A")
	dw_rmt_detail.Sort()
//	Return( True )
	ib_good_product = true
	iw_frame.im_menu.mf_enable('m_save')
//	iw_frame.im_menu.mf_Enable('m_delete')
	iw_frame.im_menu.mf_Enable('m_new')
	iw_frame.im_menu.mf_Enable('m_addrow')
	iw_frame.im_menu.mf_Enable('m_deleterow')
	This.SetRedraw( True )
	Return( True )
	//SetMicroHelp("Inquire Successful")
	
Else
	ll_rtn = iw_frame.im_menu.mf_disable('m_save')
	ll_rtn = iw_frame.im_menu.mf_disable('m_new')
	ll_rtn = iw_frame.im_menu.mf_disable('m_addrow')
	ib_good_product = False
	This.SetRedraw(True)
	Return( False )
End If

return true
//
end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

event close;call super::close;iu_pas203.nf_release_commhandle(ii_async_commhandle)

If IsValid(iu_pas203) Then Destroy(iu_pas203)

If IsValid(u_ws_pas2) Then Destroy(iu_ws_pas2)
end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


	

end event

event ue_query;call super::ue_query;wf_retrieve()

end event

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
//iw_frame.im_menu.mf_Enable('m_save')
//iw_frame.im_menu.mf_Enable('m_delete')
//iw_frame.im_menu.mf_Enable('m_new')
//iw_frame.im_menu.mf_Enable('m_addrow')
//iw_frame.im_menu.mf_Enable('m_deleterow')
//
//
//if ib_good_product = false then
//	iw_frame.im_menu.mf_disable('m_save')
//	iw_frame.im_menu.mf_disable('m_delete')
//	iw_frame.im_menu.mf_disable('m_new')
//	iw_frame.im_menu.mf_disable('m_addrow')
//	iw_frame.im_menu.mf_disable('m_deleterow')
//end if
//

// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

If not ib_good_product Then
	iw_frame.im_menu.mf_disable('m_new')
	iw_frame.im_menu.mf_disable('m_addrow')
	iw_frame.im_menu.mf_disable('m_save')
	iw_frame.im_menu.mf_disable('m_deleterow')
End If


end event

event open;call super::open;//String				ls_text


dw_header.InsertRow(0)
dw_rmt_detail.InsertRow(0)

ib_good_product = false

dw_header.ResetUpdate()
dw_rmt_detail.ResetUpdate()

end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_pas203 = Create u_pas203
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

iu_ws_pas2 = Create u_ws_pas2
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

ii_async_commhandle = iu_pas203.nf_Get_Async_Commhandle()
If ii_async_commhandle < 0 Then
	MessageBox("RMT Schedule", "Problem Getting Commhandle for RMT Sched Parameters")
	Close(This)
	return
End if


istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "RMT Schedule"
istr_error_info.se_user_id = sqlca.userid

GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if


This.PostEvent("ue_query")

end event

event key;call super::key;//Int	li_curr_col
//
//If KeyDown(KeyTab!) Then
//	li_curr_col = dw_rmt_detail.GetColumn()
//	If li_curr_col < Integer(dw_rmt_detail.Describe("DataWindow.Column.Count")) Then
//		dw_rmt_detail.SetColumn(li_curr_col + 1)
//	Else
//		dw_rmt_detail.SetColumn(1)
//	End if
//	dw_rmt_detail.SetFocus()
//End if
end event

on w_raw_mat_sched_parameters.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_rmt_detail=create dw_rmt_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_rmt_detail
end on

on w_raw_mat_sched_parameters.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_rmt_detail)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Product'
		Message.StringParm = dw_header.GetItemString(1, 'fab_product_code')
	Case 'Product_Descr'
		Message.StringParm = dw_header.GetItemString(1, 'fab_product_descr')
	Case 'State'
		Message.StringParm = String(dw_header.GetItemNumber(1, 'product_state'))
		//1-13 jac
	Case 'product_status'
		Message.StringParm = dw_header.GetItemString(1, 'product_status')
		//	
   Case 'Shift'
		Message.StringParm = dw_header.GetItemString(1, 'shift')
//	Case 'Date'
//		Message.StringParm = String(wf_get_selected_date(), 'yyyy-mm-dd')
//	Case 'Ship_Off_The_Cut'
//		Message.StringParm = dw_header.GetItemString(1, 'ship_off_the_cut')
//	Case 'Ranges'
//		Message.StringParm = dw_header.GetItemString(1, 'ranges')
//		
End Choose
//
end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant_Desc' 
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Product'
		dw_header.SetItem(1, 'fab_product_code', as_value)
	Case 'Product_Desc'
		dw_header.SetItem(1, 'fab_product_descr', as_value)
	Case 'State'
		dw_header.SetItem(1, 'product_state', integer(as_value))
		//1-13 jac
	Case 'Product_status'
		dw_header.SetItem(1, 'product_status', as_value)
		//
	Case 'Shift'
		dw_header.SetItem(1, 'shift', as_value)
End Choose

end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_rmt_detail.x * 2) + 30 
li_y = dw_rmt_detail.y + 115

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


if width > li_x Then
	dw_rmt_detail.width	= width - li_x
end if

if height > li_y then
	dw_rmt_detail.height	= height - li_y
end if
end event

type dw_header from u_base_dw_ext within w_raw_mat_sched_parameters
integer y = 4
integer width = 2231
integer height = 304
integer taborder = 10
string dataobject = "d_rmt_sched_header"
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False

end event

type dw_rmt_detail from u_base_dw_ext within w_raw_mat_sched_parameters
integer y = 328
integer width = 1797
integer height = 1196
integer taborder = 20
string dataobject = "d_rmt_parameter_detail"
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return


end event

event constructor;call super::constructor;ib_updateable = True

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

dw_rmt_detail.GetChild('uom', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("RAWUOM")

end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus


//int			li_rc, &
//				li_seconds,li_temp
//
long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

//string		ls_GetText, &
string		ls_temp
				
Date			ldt_temp	

nvuo_pa_business_rules	u_rule


//is_ColName = GetColumnName()
//ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0

CHOOSE CASE dwo.name
	CASE "uom"
		If  isnull(data) Then
			iw_frame.SetMicroHelp("Invalid UOM")
			This.selecttext(1,100)
			return 1
		End if
	CASE "start_date", "end_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
	
	CASE "min_quantity_hour", "max_quantity_hour"
		If Not IsNumber(data) Then
			iw_frame.SetMicroHelp("Quantity must be a number")
			This.selecttext(1,100)
			return 1
		End if
		If Real(data) < 0 Then
			iw_frame.SetMicroHelp("Quantity cannot be negative")
			This.selecttext(1,100)
			return 1
		End if
END CHOOSE

// Update the Update_Flag column so the RPC will know how to update the row
//IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
//	IF Long(This.Describe("update_flag.id")) > 0 THEN
//		ls_temp_update_flag = this.describe("update_flag.id")
//		ls_temp_update_flag = This.GetItemString(ll_source_row, "update_flag")
//		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
//			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
//			CHOOSE CASE le_RowStatus
//			CASE NewModified!, New!
//				This.SetItem(ll_source_row, "update_flag", "A")
//			CASE DataModified!, NotModified!
//				This.SetItem(ll_source_row, "update_flag", "U")
//			END CHOOSE		
//		End if
//	END IF
//END IF
//
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

//parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0


end event

event itemerror;call super::itemerror;return 1

end event

