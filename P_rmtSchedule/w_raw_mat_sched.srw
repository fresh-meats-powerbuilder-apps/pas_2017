HA$PBExportHeader$w_raw_mat_sched.srw
$PBExportComments$Add pallet
forward
global type w_raw_mat_sched from w_base_sheet_ext
end type
type cbx_scroll from checkbox within w_raw_mat_sched
end type
type dw_set_qty from u_base_dw_ext within w_raw_mat_sched
end type
type cb_set_qty from commandbutton within w_raw_mat_sched
end type
type cb_recalculate from commandbutton within w_raw_mat_sched
end type
type st_2 from statictext within w_raw_mat_sched
end type
type st_1 from statictext within w_raw_mat_sched
end type
type dw_header from u_base_dw_ext within w_raw_mat_sched
end type
type dw_rmt_detail from u_base_dw_ext within w_raw_mat_sched
end type
end forward

global type w_raw_mat_sched from w_base_sheet_ext
integer width = 3639
integer height = 2152
string title = "Raw Material Schedule"
long backcolor = 67108864
event ue_recalculate pbm_custom70
cbx_scroll cbx_scroll
dw_set_qty dw_set_qty
cb_set_qty cb_set_qty
cb_recalculate cb_recalculate
st_2 st_2
st_1 st_1
dw_header dw_header
dw_rmt_detail dw_rmt_detail
end type
global w_raw_mat_sched w_raw_mat_sched

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product, &
				ib_set_qty, &
				ib_drag, &
				ib_auto_scroll
	
Date			idt_sched_date, &
				idt_drag_date
nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow, &
				il_drag_qty, &
				il_drag_row

s_error		istr_error_info

u_pas201		iu_pas201

u_ws_pas2      iu_ws_pas2

String		is_colname, &
				is_input, &
				is_ChangedColumnName, &
				is_debug, &
				is_total_string, &
				is_shift, &
				is_scroll_pos, &
				is_date_string
				
w_base_sheet	iw_order_detail

datastore		ids_rmsh_detail

u_base_dw	idw_focus
end variables

forward prototypes
public function integer wf_find_pa_date (date adt_sched_date)
public function boolean wf_validate (long al_row)
public function boolean wf_update ()
public function string wf_build_total_string (string as_status_ind)
public function boolean wf_validate_set_qty ()
public function boolean wf_unstring_output (string as_output_string)
public function string wf_find_holiday_ind (string as_sched_date)
public function boolean wf_recalculate_qty ()
public function boolean wf_reset_qty ()
public function boolean wf_retrieve ()
public function boolean wf_importdates (string as_headerstring)
end prototypes

event ue_recalculate;wf_reset_qty() 

wf_recalculate_qty()


end event

public function integer wf_find_pa_date (date adt_sched_date);Date	ldt_pa_date

Int	li_date_position

Long 	ll_value

String	ls_date_column

ll_value = 1
Do 
	
	ls_date_column = "pa_date_" + String(ll_value)

	ldt_pa_date = Date(dw_rmt_detail.Describe(ls_date_column + ".Expression"))
	If adt_sched_date = ldt_pa_date Then
		li_date_position = ll_value
		ll_value = 44
	Else
		ll_value = ll_value + 1
	End If
Loop while ll_value < 43


return li_date_position
end function

public function boolean wf_validate (long al_row);Long					ll_rtn, &
						ll_nbrrows, &
						ll_value

String				ls_qty_column

For ll_value = 1 to 40
	ls_qty_column = "quantity_" + String(ll_value) + "a"
	If dw_rmt_detail.GetItemNumber(al_row, ls_qty_column) < 0 Then
		iw_Frame.SetMicroHelp("Please enter a Quantity greater than 0")
		This.SetRedraw(False)
		dw_rmt_detail.ScrollToRow(al_row)
		dw_rmt_detail.SetColumn(ls_qty_column)
		dw_rmt_detail.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
	
	ls_qty_column = "quantity_" + String(ll_value) + "b"
	If dw_rmt_detail.GetItemNumber(al_row, ls_qty_column) < 0 Then
		iw_Frame.SetMicroHelp("Please enter a Quantity greater than 0")
		This.SetRedraw(False)
		dw_rmt_detail.ScrollToRow(al_row)
		dw_rmt_detail.SetColumn(ls_qty_column)
		dw_rmt_detail.SetFocus()
		This.SetRedraw(True)
		Return False
	End If
Next

IF al_row < 0 THEN Return True

Return True
end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount

string			ls_total_string , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdateRMSH, &
					ls_header_string, &
					ls_output_string

dwItemStatus	lis_status

IF dw_rmt_detail.AcceptText() = -1 THEN Return( False )
IF dw_rmt_detail.ModifiedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

IF Not IsValid(iu_ws_pas2) THEN
	iu_ws_pas2	=  CREATE u_ws_pas2
END IF


ll_NbrRows = dw_rmt_detail.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_rmt_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_header_string = is_input
ls_total_string = wf_build_total_string('A')
ls_total_string = is_total_string + ls_total_string

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp39cr_upd_rmt_sched"
istr_error_info.se_message = Space(71)

//If not iu_pas201.nf_pasp39cr_upd_rmt_sched(istr_error_info, ls_total_string, ls_output_string, ls_header_string) Then
//	if ls_output_string > '' Then
//		wf_unstring_output(ls_output_string)
//	End If
//	Return False
//end If

If not iu_ws_pas2.nf_pasp39gr(ls_total_string, ls_output_string, ls_header_string,istr_error_info) Then
	if ls_output_string > '' Then
		wf_unstring_output(ls_output_string)
	End If
	Return False
end If

ib_ReInquire = True
This.post wf_retrieve()
dw_rmt_detail.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

dw_rmt_detail.ResetUpdate()
dw_set_qty.ResetUpdate()

dw_rmt_detail.SetFocus()
dw_rmt_detail.SetReDraw(True)

Return( True )

end function

public function string wf_build_total_string (string as_status_ind);Boolean		lb_uom_found

Long			ll_value, &
				ll_row_count, &
				ll_rmsh_count, &
				ll_row, &
				ll_qty, &
				ll_rtn

String		ls_total_string, &
				ls_date_column, &
				ls_qty_column, &
				ls_date, &
				ls_qty, &
				ls_fab_product, &
				ls_product_state, &
				ls_product_status, &
				ls_searchstring, &
				ls_shift

//This logic will build a string consisting of each pa date
// followed by the total for the shift and then the uom found
// for that shift
ls_fab_product = dw_header.GetItemString(1, "out_product_code")
ls_product_state = String(dw_header.GetItemNumber(1, "out_product_state"))
//1-13 jac
ls_product_status = dw_header.GetItemString(1, "out_product_status")
//

ll_row_count = dw_rmt_detail.RowCount()
If ll_row_count < 0 Then ll_row_count = 0
ll_rmsh_count = ids_rmsh_detail.RowCount()

ll_value = 1

Do while ll_value < 43
	
	ls_date_column = "pa_date_" + String(ll_value)
	ls_qty_column = "sum_quantity_"+ String(ll_value) + "a"

	ls_date = dw_rmt_detail.Describe(ls_date_column + ".Expression")
	
	ls_qty = 'Evaluate ("'+ls_qty_column +'",1)'
	ll_qty = Long(dw_rmt_detail.Describe(ls_qty))
	
	ls_total_string = ls_total_string + as_status_ind + '~t' + &
					ls_date + '~t' +  String(ll_qty) + '~t' 
					

 //1-13 jac  added product status fields
//Search for the from and to dates	
	ls_shift = 'A'
	ls_SearchString = 	"product_code = '"+ ls_fab_product +&
							"' and product_state = '" + ls_product_state + "'" +&
							" and product_status = '" + ls_product_status + "'" +&
							" and sched_date = date('" + ls_date+ "')" +& 
							" and shift = '" + ls_shift + "'"
	
	ll_rtn = ids_rmsh_detail.Find  &
					( ls_SearchString, 1, ll_rmsh_count)
			
	ls_total_string = ls_total_string + &
		ids_rmsh_detail.GetItemString(ll_rtn, 'uom') + '~t' + &
		String(ids_rmsh_detail.GetItemDate(ll_rtn, 'start_date'), 'yyyy-mm-dd') + '~t' + &
		String(ids_rmsh_detail.GetItemDate(ll_rtn, 'end_date'), 'yyyy-mm-dd') + '~t' 
	
	// do same logic again for the b shift
	
	ls_qty_column = "sum_quantity_"+ String(ll_value) + "b"


	ls_qty = 'Evaluate ("'+ ls_qty_column +'",1)'
	ll_qty = Long(dw_rmt_detail.Describe(ls_qty))
	ls_total_string = ls_total_string + String(ll_qty) + '~t' 
	//search for the from and to dates
	ls_shift = 'B'
	ls_SearchString = 	"product_code = '"+ ls_fab_product +&
							"' and product_state = '" + ls_product_state + "'" +&
							" and product_status = '" + ls_product_status + "'" +&
							" and sched_date = date('" + ls_date+ "')" +& 
							" and shift = '" + ls_shift + "'"
	
	ll_rtn = ids_rmsh_detail.Find  &
					( ls_SearchString, 1, ll_rmsh_count)
					
	ls_total_string = ls_total_string + &
		ids_rmsh_detail.GetItemString(ll_rtn, 'uom') + '~t' + &
		String(ids_rmsh_detail.GetItemDate(ll_rtn, 'start_date'), 'yyyy-mm-dd') + '~t' + &
		String(ids_rmsh_detail.GetItemDate(ll_rtn, 'end_date'), 'yyyy-mm-dd') 					
	
	ll_value = ll_value + 1		
	ls_total_string = ls_total_string + '~r~n'
Loop


return ls_total_string
end function

public function boolean wf_validate_set_qty ();
dw_set_qty.accepttext()

If dw_set_qty.GetItemNumber(1, "set_qty") < 0 Then
	iw_Frame.SetMicroHelp("Please enter a Quantity greater than 0")
	This.SetRedraw(False)
	dw_set_qty.SetColumn("set_qty")
	dw_set_qty.SetFocus()
	This.SetRedraw(True)
	Return False
End If
Return True
end function

public function boolean wf_unstring_output (string as_output_string);Boolean				lb_date_found

Integer				li_counter

Long					ll_rtn, &
						ll_nbrrows, &
						ll_value

String				ls_sched_date, &
						ls_shift, &
						ls_date, &
						ls_date_column, &
						ls_qty_column, &
						ls_protect_column
						

u_string_functions	lu_string_functions
					
ll_nbrrows = dw_rmt_detail.RowCount()

dw_rmt_detail.SelectRow(0,False)

ll_nbrrows = dw_rmt_detail.RowCount()
ls_sched_date = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_shift = lu_string_functions.nf_gettoken(as_output_string, '~t')

// find the column that matches the date in error.	
ll_value = 1
lb_date_found = False

Do While lb_date_found = False
	ls_date_column = "pa_date_" + String(ll_value)
	ls_date = dw_rmt_detail.Describe(ls_date_column + ".Expression")
	If ls_date = ls_sched_date Then
		lb_date_found = True
	End If
	ll_value = ll_value + 1		
	if ll_value = 43 Then
	   Return False
	End If
Loop

as_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function

public function string wf_find_holiday_ind (string as_sched_date);Date	ldt_pa_date

Long 	ll_value

String	ls_holiday_ind


ll_value = Pos(is_date_string, '~t', Pos(is_date_string, as_sched_date, 1))

ll_value = ll_value + 1

ls_holiday_ind = Mid(is_date_string, ll_value, 1) 

return ls_holiday_ind
end function

public function boolean wf_recalculate_qty ();Date			ldt_sched_date, &
				ldt_avail_date, &
				ldt_to_date, &
				ldt_from_date, &
				ldt_prod_date
				
Integer		li_position, &
				li_curr_col

Long			ll_row_cnt_rmsh, &
				ll_row_cnt_source, &
				ll_row_rmsh, &
				ll_row_source, &
				ll_qty, &
				ll_remain_qty, &
				ll_avail_qty, &
				ll_sched_qty, &
				ll_orig_qty

String		ls_qty_column, &
				ls_protect_column, &
				ls_product_cde, &
				ls_product_ste, &
				ls_hdr_product_cde, &
				ls_hdr_product_ste, &
				ls_shift, &
				ls_detail_errors, &
				ls_hdr_product_stat, &
				ls_product_stat


//This logic will recalculate the qty by comparing the dates
//and available qty to the RMSH information.

ls_detail_errors = space(84)
ls_detail_errors = Fill(char(0),84)
ll_row_cnt_source = dw_rmt_detail.RowCount()
ll_row_cnt_rmsh = ids_rmsh_detail.RowCount()
If ll_row_cnt_rmsh < 0 Then ll_row_cnt_rmsh = 0

If ll_row_cnt_rmsh = 0 Then
	iw_Frame.SetMicroHelp("Error setting up Raw Material Rows to recalculate ")
	dw_rmt_detail.SetRedraw(False)
	dw_rmt_detail.SetFocus()
	dw_rmt_detail.SetRedraw(True)
	return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Recalculating quantity distribution")

ls_hdr_product_cde = RightTrim(dw_header.GetItemString(1, "out_product_code"))
ls_hdr_product_ste = String(dw_header.GetItemNumber(1, "out_product_state"))
//1-13 jac
ls_hdr_product_stat = RightTrim(dw_header.GetItemString(1, "out_product_status"))

//process all the rows in the ids_rmsh_detail
ll_row_rmsh = 1

dw_rmt_detail.SetRedraw(False)
Do 
	ldt_sched_date = ids_rmsh_detail.GetItemDate(ll_row_rmsh, "sched_date")
	ldt_from_date = ids_rmsh_detail.GetItemDate(ll_row_rmsh, "start_date")
	ldt_to_date = ids_rmsh_detail.GetItemDate(ll_row_rmsh, "end_date")
	ls_product_cde = RightTrim(ids_rmsh_detail.GetItemString(ll_row_rmsh, "product_code"))
	ls_product_ste	= ids_rmsh_detail.GetItemString(ll_row_rmsh, "product_state")
	ll_sched_qty = ids_rmsh_detail.GetItemNumber(ll_row_rmsh, "orig_qty")
	ls_shift	= ids_rmsh_detail.GetItemString(ll_row_rmsh, "shift")
	ls_product_stat = ids_rmsh_detail.GetItemString(ll_row_rmsh, "product_status")
	
	
	//start at the beginning of the detail source lines, there
	//should be a matching available date to apply the qty
	ll_row_source = 1
	
	Do 
		ldt_avail_date = dw_rmt_detail.GetItemDate(ll_row_source, "avail_date")
		ldt_prod_date = dw_rmt_detail.GetItemDate(ll_row_source, "prod_date")
		If ldt_avail_date <= ldt_sched_date Then
			If ldt_prod_date >= ldt_from_date and &
						ldt_prod_date <= ldt_to_date Then
				ll_avail_qty = dw_rmt_detail.GetItemNumber(ll_row_source, "total_avail_qty")
				ll_remain_qty = dw_rmt_detail.GetItemNumber(ll_row_source, "total_remain_qty")
		//if the rmsh product <> the out product on the header, then
		// adjust the available qty field.
		//1-13 jac  added product status
				if (ls_product_cde <> ls_hdr_product_cde or &
					ls_product_ste <> ls_hdr_product_ste or &
					ls_product_stat <> ls_hdr_product_stat) and &
							ll_remain_qty > 0 Then
				//	If ll_avail_qty >= ll_sched_qty Then
				//	ll_avail_qty = ll_avail_qty - ll_sched_qty
					If ll_remain_qty >= ll_sched_qty Then
						ll_avail_qty = ll_avail_qty - ll_sched_qty
					Else
						//ll_avail_qty = 0
						ll_avail_qty = ll_avail_qty - ll_remain_qty
					End If
					dw_rmt_detail.SetItem(ll_row_source, "total_avail_qty", ll_avail_qty)
				End If
		//adjust the remaining qty for all rmsh products
				If ll_remain_qty >= ll_sched_qty Then
					ll_remain_qty = ll_remain_qty - ll_sched_qty
					ll_sched_qty = 0
				Else
					ll_sched_qty = ll_sched_qty - ll_remain_qty
					ll_remain_qty = 0
				End If
				dw_rmt_detail.SetItem(ll_row_source, "total_remain_qty", ll_remain_qty)
		//1-13 jac		added product status
				If (ls_product_cde = ls_hdr_product_cde and &
							ls_product_ste = ls_hdr_product_ste and &
							ls_product_stat = ls_hdr_product_stat) Then				
					li_position = wf_find_pa_date(ldt_sched_date)
					ls_qty_column = "quantity_"+ String(li_position) + String(ls_shift)
					dw_rmt_detail.SetItem(ll_row_source, ls_qty_column, (ids_rmsh_detail.GetItemNumber(ll_row_rmsh, "remain_qty") - ll_sched_qty))
				End If
				ids_rmsh_detail.SetItem(ll_row_rmsh, "remain_qty", ll_sched_qty)
			End If
			if ll_sched_qty = 0 Then
				ll_row_source = ll_row_cnt_source + 1
			Else
				ll_row_source = ll_row_source + 1	
			End If
		Else		
			If ll_sched_qty = 0 Then
				ll_row_source = ll_row_cnt_source + 1
			Else
	//if the schedule qty > zero and the sched date > avail date
	//for the product inquired upon then add the quantity 
	//to that previous cell (date should be equal) and stop looping
	//1-13 jac added product status
				If (ls_product_cde = ls_hdr_product_cde and &
						ls_product_ste = ls_hdr_product_ste and &
						ls_product_stat = ls_hdr_product_stat)Then &
						
					If ib_set_qty Then
						ll_orig_qty = ids_rmsh_detail.GetItemNumber(ll_row_rmsh, "orig_qty") - ll_sched_qty
						ids_rmsh_detail.SetItem(ll_row_rmsh, "orig_qty", ll_orig_qty)
						ids_rmsh_detail.SetItem(ll_row_rmsh, "remain_qty", 0)
					Else
						li_position = wf_find_pa_date(ldt_sched_date)
						ls_qty_column = "quantity_"+ String(li_position) + String(ls_shift)
						dw_rmt_detail.SetItem((ll_row_source - 1), ls_qty_column, (dw_rmt_detail.GetItemNumber((ll_row_source - 1), ls_qty_column) + ll_sched_qty))
						ids_rmsh_detail.SetItem(ll_row_rmsh, "remain_qty", 0)	
						If ls_shift = 'A' Then
							li_curr_col = (li_position * 2) - 1
						Else
							li_curr_col = (li_position * 2)
						End If
						
						ls_detail_errors = dw_rmt_detail.GetItemString(ll_row_source - 1, 'detail_errors')
						ls_detail_errors = Replace(ls_detail_errors, li_curr_col, 1, 'Y')
						dw_rmt_detail.SetItem(ll_row_source - 1, 'detail_errors', ls_detail_errors)
					End If
					ll_sched_qty = 0
									
				End If	
				ll_row_source = ll_row_cnt_source + 1
			End If
	
		End If
		
	Loop while ll_row_source <= ll_row_cnt_source
	
	If ll_sched_qty = 0 Then
		// do nothing 
	Else
		//1-13 jac added product status
		ll_row_source = ll_row_cnt_source
		If (ls_product_cde = ls_hdr_product_cde and &
			ls_product_ste = ls_hdr_product_ste and &
			ls_product_stat = ls_hdr_product_stat) Then 
			If ib_set_qty Then
				ll_orig_qty = ids_rmsh_detail.GetItemNumber(ll_row_rmsh, "orig_qty") - ll_sched_qty
				ids_rmsh_detail.SetItem(ll_row_rmsh, "orig_qty", ll_orig_qty)
				ids_rmsh_detail.SetItem(ll_row_rmsh, "remain_qty", 0)
			Else
				li_position = wf_find_pa_date(ldt_sched_date)
				ls_qty_column = "quantity_"+ String(li_position) + String(ls_shift)
				dw_rmt_detail.SetItem(ll_row_source, ls_qty_column, (dw_rmt_detail.GetItemNumber(ll_row_source, ls_qty_column) + ll_sched_qty))
				ids_rmsh_detail.SetItem(ll_row_rmsh, "remain_qty", 0)	
				If ls_shift = 'A' Then
					li_curr_col = (li_position * 2) - 1
				Else
					li_curr_col = (li_position * 2)
				End If
				ls_detail_errors = dw_rmt_detail.GetItemString(ll_row_source, 'detail_errors')
				ls_detail_errors = Replace(ls_detail_errors, li_curr_col, 1, 'Y')
				dw_rmt_detail.SetItem(ll_row_source, 'detail_errors', ls_detail_errors)
			End If
		End If
		ll_sched_qty = 0
	End If						
	ll_row_rmsh = ll_row_rmsh + 1
Loop while ll_row_rmsh <=  ll_row_cnt_rmsh

dw_rmt_detail.SetRedraw(True)
dw_rmt_detail.accepttext()
ib_set_qty = False
iw_frame.SetMicroHelp("Ready")
Return True

end function

public function boolean wf_reset_qty ();Boolean		lb_uom_found

Date			ldt_pa_date

Long			ll_value, &
				ll_row_count, &
				ll_rmsh_count, &
				ll_row, &
				ll_qty, &
				ll_rtn, &
				ll_qty_cell, &
				ll_set_qty, &
				ll_date_position

String		ls_total_string, &
				ls_date_column, &
				ls_qty_column, &
				ls_uom_column, &
				ls_date, &
				ls_qty, &
				ls_fab_product, &
				ls_product_state, &
				ls_product_status, &
				ls_shift, &
				ls_sched_date, &
				ls_SearchString, &
				ls_rmsh_product, &
				ls_rmsh_product_state, &
				ls_rmsh_product_status, &
				ls_holiday_ind		


ll_row_count = dw_rmt_detail.RowCount()
ll_rmsh_count = ids_rmsh_detail.RowCount()

If ll_row_count < 0 Then ll_row_count = 0
If ll_row_count = 0 Then
	iw_Frame.SetMicroHelp("Error setting up Raw Material Rows to schedule ")
	dw_rmt_detail.SetRedraw(False)
	dw_rmt_detail.SetFocus()
	dw_rmt_detail.SetRedraw(True)
	return False
End If
dw_rmt_detail.accepttext()
//if the set qty button was pressed loop through and set 
//all rmsh quantities to set qty
dw_rmt_detail.SetRedraw(False)
ls_fab_product = RightTrim(dw_header.GetItemString(1, "out_product_code"))
ls_product_state = String(dw_header.GetItemNumber(1, "out_product_state"))
//1-13 jac
ls_product_status = RightTrim(dw_header.GetItemString(1, "out_product_status"))
//
If ib_set_qty Then
	If Not wf_validate_set_qty() Then Return False
	ll_set_qty = dw_set_qty.GetItemNumber(1, "set_qty")
	ll_row = 1
	Do
		ls_rmsh_product = RightTrim(ids_rmsh_detail.GetItemString(ll_row, "product_code"))
		ls_rmsh_product_state = ids_rmsh_detail.GetItemString(ll_row, "product_state")
		//1-13 jac
		ls_rmsh_product_status = ids_rmsh_detail.GetItemString(ll_row, "product_status")
		//
		ls_sched_date = String(ids_rmsh_detail.GetItemDate(ll_row, "sched_date"), 'yyyy-mm-dd')
		ls_holiday_ind = wf_find_holiday_ind(ls_sched_date)
		If ls_holiday_ind = 'X' or ls_holiday_ind = 'H' Then
			// do nothing
		Else
			//1-13 jac added product status
			If (ls_rmsh_product = ls_fab_product and &
							ls_rmsh_product_state = ls_product_state and &
							ls_rmsh_product_status = ls_product_status) Then
	
				ids_rmsh_detail.SetItem(ll_row, "orig_qty", ll_set_qty)
			End If
		End If
		ll_row = ll_row + 1
	Loop While ll_row <= ll_rmsh_count
	
Else

//This logic will update the datastore with the total values
// from the detail screen
	ll_value = 1

	Do 
		
		ls_date_column = "pa_date_" + String(ll_value)
		ls_sched_date = dw_rmt_detail.Describe(ls_date_column + ".Expression")
	
		ls_qty_column = "sum_quantity_"+ String(ll_value) + "a"
		ls_shift = 'A'
	
		ls_qty = 'Evaluate ("'+ls_qty_column +'",1)'
		ll_qty = Long(dw_rmt_detail.Describe(ls_qty))
	//1-13 jac added product status
		ls_SearchString = 	"product_code = '"+ ls_fab_product +&
							"' and product_state = '" + ls_product_state + "'" +&
							" and product_status = '" + ls_product_status + "'" +&
							" and sched_date = date('" + ls_sched_date+ "')" +& 
							" and shift = '" + ls_shift + "'"
	
	
		ll_rtn = ids_rmsh_detail.Find  &
					( ls_SearchString, 1, ll_rmsh_count)
					
		If ll_rtn = 0 Then Return False
	
		ids_rmsh_detail.SetItem(ll_rtn, "orig_qty", ll_qty)
//do same logic again for B shift	
		ls_qty_column = "sum_quantity_"+ String(ll_value) + "b"
		ls_shift = 'B'
	
		ls_qty = 'Evaluate ("'+ls_qty_column +'",1)'
		ll_qty = Long(dw_rmt_detail.Describe(ls_qty))
//1-13 jac added product status
		ls_SearchString = 	"product_code = '"+ ls_fab_product +&
							"' and product_state = '" + ls_product_state + "'" +&
							" and product_status = '" + ls_product_status + "'" +&
							" and sched_date = date('" + ls_sched_date+ "')" +& 
							" and shift = '" + ls_shift + "'"
	
	
		ll_rtn = ids_rmsh_detail.Find &
					( ls_SearchString, 1, ll_rmsh_count)
					
		If ll_rtn = 0 Then Return False
	
		ids_rmsh_detail.SetItem(ll_rtn, "orig_qty", ll_qty)	
		ll_value = ll_value + 1
	Loop while ll_value < 43
End If

// reset all the remaining qty so that it can be reapplied in
// the wf_recalculate_qty function
ll_row = 1

Do
	ids_rmsh_detail.SetItem(ll_row, "remain_qty", ids_rmsh_detail.GetItemNumber(ll_row, "orig_qty"))	
	ll_row = ll_row + 1
Loop While ll_row <= ll_rmsh_count 

ll_row = 1

Do
	dw_rmt_detail.SetItem(ll_row, "total_avail_qty", dw_rmt_detail.GetItemNumber(ll_row, "total_orig_qty"))	
	dw_rmt_detail.SetItem(ll_row, "total_remain_qty", dw_rmt_detail.GetItemNumber(ll_row, "total_orig_qty"))	
	ll_qty_cell = 1
	Do 
		ls_qty_column = "quantity_"+ String(ll_qty_cell) + "a"
		dw_rmt_detail.SetItem(ll_row, ls_qty_column, 0)	
		ls_qty_column = "quantity_"+ String(ll_qty_cell) + "b"
		dw_rmt_detail.SetItem(ll_row, ls_qty_column, 0)
		dw_rmt_detail.SetItem(ll_row, 'detail_errors', Space(84))
		ll_qty_cell = ll_qty_cell + 1
	Loop while ll_qty_cell < 43
	ll_row = ll_row + 1
Loop While ll_row <= ll_row_count 

//ids_rmsh_detail.SetSort("sort_date A, sort_ind A, end_date A, shift A")
//1-13 jac
//ids_rmsh_detail.SetSort("product_code A, product_state A, sort_date A, start_date A, shift A")
ids_rmsh_detail.SetSort("product_code A, product_state A, product_status A, sort_date A, start_date A, shift A")
ids_rmsh_detail.Sort()
//dw_rmt_detail.SetRedraw(True)
return True
end function

public function boolean wf_retrieve ();Integer	li_ret

String	ls_input, &
			ls_HeaderString, &
			ls_DetailString, &
			ls_RmshString, &
			ls_product
			
Date		ldt_today
			
Long		ll_value, &
			ll_rtn
			

u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false

If Not ib_ReInquire Then
	OpenWithParm(w_raw_mat_sched_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
This.SetRedraw( False )
SetMicroHelp("Retrieving ...")
is_debug = This.dw_header.GetItemString(1, 'in_product_code')

ldt_today = today()
//1-13 jac added product status fields
ls_input = This.dw_header.GetItemString(1, "plant_code") + '~t' + &
				This.dw_header.GetItemString(1, "in_product_code") + '~t' + &
				string(This.dw_header.GetItemnumber(1, "in_product_state")) + '~t' + &
				This.dw_header.GetItemString(1, "in_product_status") + '~t' + &
				This.dw_header.GetItemString(1, "uom") + '~t' + &
				This.dw_header.GetItemString(1, "out_product_code") + '~t' + & 
				string(This.dw_header.GetItemnumber(1, "out_product_state")) + '~t' + & 
				This.dw_header.GetItemString(1, "out_product_status") + '~r~n'
				

is_input = ls_input

//li_ret = iu_pas201.nf_pasp38cr_inq_rmt_sched(istr_error_info, &
//									is_input, &
//									ls_HeaderString, &
//									ls_DetailString, &
//									ls_RmshString)

li_ret = iu_ws_pas2.nf_pasp38gr(is_input, &
									ls_HeaderString, &
									ls_DetailString, &
									ls_RmshString,istr_error_info)

dw_rmt_detail.Reset()

If li_ret = 0 Then
	wf_ImportDates(ls_HeaderString)
	is_date_string = ls_HeaderString
	
	This.dw_rmt_detail.ImportString(ls_DetailString)
	
	ids_rmsh_detail.Reset()
	ids_rmsh_detail.ImportString(ls_RmshString)
	//ids_rmsh_detail.SetSort("sort_date A, sort_ind A, end_date A, shift A")
	ids_rmsh_detail.SetSort("product_code A, product_state A, sort_date A, start_date A, shift A")
	ids_rmsh_detail.Sort()
	wf_recalculate_qty()	

	ll_value = dw_rmt_detail.RowCount()
	If ll_value < 0 Then ll_value = 0
	
	This.dw_rmt_detail.Event ue_set_hspscroll()
	
	dw_rmt_detail.ResetUpdate()
		
	IF ll_value > 0 THEN
 		dw_rmt_detail.SetFocus()
		dw_rmt_detail.ScrollToRow(1)
		//dw_rmt_detail.SetColumn( "start_date" )
		dw_rmt_detail.TriggerEvent("RowFocusChanged")
	END IF

	SetMicroHelp(String(ll_value) + " rows retrieved")
	ib_good_product = True
	This.SetRedraw( True )
	iw_frame.im_menu.mf_enable('m_save')
	is_total_string = wf_build_total_string('B')
	Return( True )
Else
	ll_rtn = iw_frame.im_menu.mf_disable('m_save')
	ib_good_product = False
	This.SetRedraw(True)
	Return( False )
End If

return true

end function

public function boolean wf_importdates (string as_headerstring);Long			ll_value

String		ls_date_string, &
				ls_holiday_ind, &
				ls_column_name
				
				
u_string_functions	lu_string

ls_date_string = ''
ls_holiday_ind = ''
ll_value = 1

Do While Len(Trim(as_HeaderString)) > 0
		ls_date_string = lu_string.nf_GetToken(as_HeaderString, '~t')
		ls_holiday_ind = lu_string.nf_GetToken(as_HeaderString, '~t')
		
		ls_column_name = "pa_date_" + String(ll_value)
		dw_rmt_detail.Modify(ls_column_name + '.Expression="' + ls_date_string + '"')
		If ls_holiday_ind = 'H' Then
			dw_rmt_detail.Modify(ls_column_name + ".Background.Color='65280'")
		Else
			dw_rmt_detail.Modify(ls_column_name + ".Background.Color='16777215'")
		End If
		
		ll_value = ll_value + 1		
Loop

dw_rmt_detail.accepttext()
return true


end function

event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)

If IsValid(iu_ws_pas2) Then Destroy(iu_ws_pas2)



end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')


end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

If not ib_good_product Then
	iw_frame.im_menu.mf_disable('m_new')
	iw_frame.im_menu.mf_disable('m_save')
End If
end event

event open;call super::open;dw_header.InsertRow(0)
dw_rmt_detail.InsertRow(0)
dw_set_qty.InsertRow(0)


end event

event ue_postopen;call super::ue_postopen;String			ls_value

Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

iu_ws_pas2 = Create u_ws_pas2
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

ids_rmsh_detail = Create DataStore
ids_rmsh_detail.DataObject = "d_rmt_sched_rmsh_det"

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Raw Material Schedule"
istr_error_info.se_user_id = sqlca.userid

ls_value = ProfileString('ibpuser.ini', 'PAS', 'w_raw_mat_sched.AutoScroll', "None") 
If ls_value = 'Y' Then
	cbx_scroll.Checked = True
	ib_auto_scroll = True
Else
	cbx_scroll.Checked = False
	ib_auto_scroll = False
End If


This.PostEvent("ue_query")

end event

on w_raw_mat_sched.create
int iCurrent
call super::create
this.cbx_scroll=create cbx_scroll
this.dw_set_qty=create dw_set_qty
this.cb_set_qty=create cb_set_qty
this.cb_recalculate=create cb_recalculate
this.st_2=create st_2
this.st_1=create st_1
this.dw_header=create dw_header
this.dw_rmt_detail=create dw_rmt_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_scroll
this.Control[iCurrent+2]=this.dw_set_qty
this.Control[iCurrent+3]=this.cb_set_qty
this.Control[iCurrent+4]=this.cb_recalculate
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.dw_header
this.Control[iCurrent+8]=this.dw_rmt_detail
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Input_Product'
		Message.StringParm = dw_header.GetItemString(1, 'in_product_code')
	Case 'Input_Product_Descr'
		Message.StringParm = dw_header.GetItemString(1, 'in_product_descr')
	Case 'Input_State'
		Message.StringParm = String(dw_header.GetItemNumber(1, 'in_product_state'))
	Case 'UOM'
		Message.StringParm = dw_header.GetItemString(1, 'uom')
	Case 'Output_Product'
		Message.StringParm = dw_header.GetItemString(1, 'out_product_code')
	Case 'Output_Product_Descr'
		Message.StringParm = dw_header.GetItemString(1, 'out_product_descr')
	Case 'Prd_Prdstate'
		Message.StringParm = dw_header.GetItemString(1, 'prd_prdstate')
	Case 'Output_State'
		Message.StringParm = String(dw_header.GetItemNumber(1, 'out_product_state'))
//1-13 jac	
   Case 'Input_Status'
		Message.StringParm = dw_header.GetItemString(1, 'in_product_status')
	Case 'Output_Status'
		Message.StringParm = dw_header.GetItemString(1, 'out_product_status')
//
End Choose

end event

event ue_set_data;call super::ue_set_data;String 		ls_plant, &
				ls_state, &
				ls_product, &
				ls_temp, &
				ls_product_status

Window		lw_temp

Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant_Desc' 
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Input_Product'
		dw_header.SetItem(1, 'in_product_code', as_value)
	Case 'Input_Product_Desc'
		dw_header.SetItem(1, 'in_product_descr', as_value)
	Case 'Input_State'
		dw_header.SetItem(1, 'in_product_state', integer(as_value))
//1-13 jac
	Case 'Input_Status'
		dw_header.SetItem(1, 'in_product_status', as_value)
//
	Case 'UOM'
		dw_header.SetItem(1, 'uom', as_value)
//		ls_temp = dw_header.uf_get_description( )
//		iw_parent.Event ue_Set_Data('uom_desc', ls_temp)
	Case 'Output_Product'
		dw_header.SetItem(1, 'out_product_code', as_value)
	Case 'Output_Product_Desc'
		dw_header.SetItem(1, 'out_product_descr', as_value)
	Case 'Output_State'
		dw_header.SetItem(1, 'out_product_state', integer(as_value))
//1-13 jac
	Case 'Output_Status'
		dw_header.SetItem(1, 'out_product_status', as_value)
//
	Case 'Prd_Prdstate'
		dw_header.SetItem(1, 'prd_prdstate', as_value)
	Case 'sched det'
		ls_plant = dw_header.GetItemString(1, "plant_code")
		ls_state = String(dw_header.GetItemNumber(1, "in_product_state"))
		ls_product = dw_header.GetItemString(1, "in_product_code")
		//1-13 jac
		ls_product_status = dw_header.GetItemString(1, "in_product_status")
		//
		ls_temp 	= ""
		ls_temp +=	ls_plant + '~t'
		ls_temp +=	ls_product + '~t' 
		ls_temp +=	ls_state + '~t' 
		ls_temp +=	ls_product_status + '~t'
		ls_temp +=	String(idt_sched_date)+ '~t'
//		//1-13 jac
//	    ls_temp +=	ls_product_status + '~t'
	   //
		OpenSheetWithParm(lw_temp, ls_temp, "w_raw_mat_det_sched",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
	Case 'def sched'
		ls_plant = dw_header.GetItemString(1, "plant_code")
		ls_state = String(dw_header.GetItemNumber(1, "in_product_state"))
		ls_product = dw_header.GetItemString(1, "in_product_code")
		//1-13 jac
		ls_product_status = dw_header.GetItemString(1, "in_product_status")
		//
		ls_temp 	= ""
		ls_temp +=	ls_plant + '~t'
		ls_temp +=	ls_product + '~t' 
		ls_temp +=	ls_state + '~t' 
		ls_temp +=	is_shift + '~t'
			//1-13 jac
	   ls_temp +=	ls_product_status + '~t'
		//
		OpenSheetWithParm(lw_temp, ls_temp, "w_raw_mat_def_sched",iw_frame,0,iw_frame.im_menu.iao_arrangeopen)
End Choose

end event

event resize;call super::resize;integer li_x		
integer li_y	
integer li_height = 115

li_x = (dw_rmt_detail.x * 2) + 30 
//li_y = dw_rmt_detail.y + 115

if il_BorderPaddingHeight > li_height Then
                li_height = il_BorderPaddingHeight
End If

if il_BorderPaddingWidth > li_x Then
                li_x = il_BorderPaddingWidth
End If



li_y = dw_rmt_detail.y + li_height

if width > li_x Then
	dw_rmt_detail.width	= width - li_x
end if

if height > li_y then
	dw_rmt_detail.height	= height - li_y
end if
end event

on w_raw_mat_sched.destroy
call super::destroy
destroy(this.cbx_scroll)
destroy(this.dw_set_qty)
destroy(this.cb_set_qty)
destroy(this.cb_recalculate)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_header)
destroy(this.dw_rmt_detail)
end on

type cbx_scroll from checkbox within w_raw_mat_sched
integer x = 2043
integer y = 392
integer width = 645
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Inactivate Auto Scroll"
end type

event clicked;If cbx_scroll.checked = True Then
	SetProfileString ( 'ibpuser.ini', 'PAS', 'w_raw_mat_sched.AutoScroll', 'Y' )
	ib_auto_scroll = True
Else
	SetProfileString ( 'ibpuser.ini', 'PAS', 'w_raw_mat_sched.AutoScroll', 'N' )
	ib_auto_scroll = False
End if
end event

type dw_set_qty from u_base_dw_ext within w_raw_mat_sched
integer x = 2889
integer y = 164
integer width = 352
integer height = 96
integer taborder = 30
string dataobject = "d_rmt_set_qty"
boolean border = false
end type

event constructor;call super::constructor;This.ib_updateable = False

end event

type cb_set_qty from commandbutton within w_raw_mat_sched
integer x = 2496
integer y = 164
integer width = 384
integer height = 92
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Set Qty Per Shift"
end type

event clicked;ib_set_qty = True

Parent.PostEvent("ue_recalculate")
end event

type cb_recalculate from commandbutton within w_raw_mat_sched
integer x = 1998
integer y = 164
integer width = 343
integer height = 92
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Recalculate"
end type

event clicked;
Parent.PostEvent("ue_recalculate")
end event

type st_2 from statictext within w_raw_mat_sched
integer x = 2473
integer y = 660
integer width = 539
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Scheduled Raw Material"
boolean focusrectangle = false
end type

type st_1 from statictext within w_raw_mat_sched
integer x = 411
integer y = 668
integer width = 416
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Input Raw Material"
boolean focusrectangle = false
end type

type dw_header from u_base_dw_ext within w_raw_mat_sched
integer y = 4
integer width = 1751
integer height = 628
integer taborder = 0
string dataobject = "d_rmt_schedule_header"
boolean border = false
end type

event constructor;call super::constructor;This.ib_updateable = False

end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state


dw_header.GetChild('uom', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("RAWUOM")

end event

type dw_rmt_detail from u_base_dw_ext within w_raw_mat_sched
event ue_set_hspscroll ( )
event ue_set_hscroll ( )
event lbuttonup pbm_lbuttonup
event lbuttondown pbm_lbuttondown
integer y = 752
integer width = 3515
integer height = 1220
integer taborder = 40
string dragicon = "row.ico"
string dataobject = "d_rmt_sched_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_set_hspscroll();Int	li_position, &
		li_position2


if this.hsplitscroll then
	If Integer(This.Object.DataWindow.HorizontalScrollMaximum) <= 1 Then return
	
	li_position = Integer(This.Object.sched_total.X) + &
						Integer(This.Object.sched_total.Width) + 15
	li_position2 = Integer(This.Object.sched_total.X) + &
						Integer(This.Object.sched_total.Width) + 15
	
	This.Object.DataWindow.HorizontalScrollSplit = String(li_position)
	This.Object.DataWindow.HorizontalScrollPosition2 =  String(li_position2)
end if
end event

event ue_set_hscroll();Date 		ldt_avail_date, &
			ldt_current_pa_date

Decimal	ld_pos2, &
			ld_cur_hpos2

Integer	li_curr_col, &
			li_date_position

Long		ll_pos2, &
			ll_row_cnt, &
			ll_row, &
			ll_date_position, &
			ll_current_row

String 	ls_pos1, &
		 	ls_smax, &
			ls_hz_smax, &
			ls_sscroll, &
			ls_modstring, &
			ls_qty_column, &
			ls_hpos, &
			ls_cur_hpos, &
			ls_curr_col_label, &
			ls_date_column

If ib_auto_scroll Then Return

ll_row_cnt = dw_rmt_detail.RowCount()

//find the position of the vertical scroll bar
ls_pos1 = &
		dw_rmt_detail.Describe("DataWindow.VerticalScrollPosition")

ls_smax = &
		dw_rmt_detail.Describe("DataWindow.VerticalScrollMaximum")

//find the position of the horizontal scroll bar  
ls_cur_hpos = &
		dw_rmt_detail.Describe("DataWindow.HorizontalScrollPosition2")

ls_hz_smax = &
		dw_rmt_detail.Describe("DataWindow.HorizontalScrollMaximum2")

If Long(is_scroll_pos) < long(ls_pos1) Then
//find the position of the row based upon the number of rows
//and where the vertical scroll bar is positioned
	ld_pos2 = Long(ls_pos1)/Long(ls_smax)
	ll_row = Round(ll_row_cnt * ld_pos2, 0)
		
	If ll_row <= 0 Then
		ll_row = 1
	End If

//find the pa date that coincides with the row above
	ldt_avail_date = dw_rmt_detail.GetItemDate(ll_row, "avail_date")

//find what dates are currently showing on the screen based upon
//where the horizontal scroll bar is positioned
	ld_cur_hpos2 = Long(ls_cur_hpos)/Long(ls_hz_smax)
	li_date_position = Round(42 * ld_cur_hpos2, 0)
	
	ls_date_column = "pa_date_" + String(li_date_position)
	ldt_current_pa_date = Date(dw_rmt_detail.Describe(ls_date_column + ".Expression"))
	
		
	if ldt_avail_date < ldt_current_pa_date then
		return
	end if
	ll_date_position = wf_find_pa_date(ldt_avail_date)
	ls_qty_column = "quantity_"+ String(ll_date_position) + "a"

	ls_hpos = dw_rmt_detail.Describe(ls_qty_column + ".X")

	ls_modstring = &
		"DataWindow.HorizontalScrollPosition2=" + ls_hpos

	dw_rmt_detail.Modify(ls_modstring)
End If

is_scroll_pos = dw_rmt_detail.Describe("DataWindow.VerticalScrollPosition")
end event

event lbuttonup;Long			ll_row

String		ls_ColumnName


ls_ColumnName = dw_rmt_detail.GetColumnName()
ll_row = dw_rmt_detail.GetRow()

This.Drag(End!)


end event

event lbuttondown;Long			ll_row, ll_rtn

String		ls_ColumnName, &
				ls_objectname, &
				ls_row

u_string_functions	lu_string

ls_objectname = dw_rmt_detail.GetObjectAtPointer()

ls_ColumnName = lu_string.nf_GetToken(ls_objectname, '~t')
ls_row = lu_string.nf_GetToken(ls_objectname, '~t')

Choose Case ls_ColumnName
	Case 'total_avail_qty'
		  il_drag_qty = This.GetItemNumber(long(ls_row), 'total_avail_qty')
		  il_drag_row = long(ls_row)
		  idt_drag_date = This.GetItemDate(long(ls_row), 'avail_date')
		  ib_drag = True
		  dw_rmt_detail.SelectRow(long(ls_row),True)
		  This.Drag(begin!)	
	Case Else
			ib_drag = False
End Choose

end event

event clicked;call super::clicked;If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return


end event

event constructor;call super::constructor;This.ib_updateable = True
ib_draggable = True
is_scroll_pos = '0'


end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state, ldwc_status

dw_rmt_detail.GetChild('uom', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("RAWUOM")

dw_rmt_detail.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")
//1-13 jac
dw_rmt_detail.GetChild('product_status', ldwc_status)
ldwc_status.SetTransObject(SQLCA)
ldwc_status.Retrieve("PRODSTAT")
//
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_GetText, &
				ls_temp
Date			ldt_temp	
nvuo_pa_business_rules	u_rule


is_ColName = GetColumnName()
ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0

If Match(is_ColName, "quantity_") Then
	If Not IsNumber(ls_GetText) Then
		iw_frame.SetMicroHelp("Quantity must be a number")
		This.selecttext(1,100)
		return 1
	End if
	If Real(ls_GetText) < 0 Then
		iw_frame.SetMicroHelp("Quantity cannot be negative")
		This.selecttext(1,100)
		return 1
	End if
End If

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;return (1)
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1, 100)


end event

event rbuttondown;call super::rbuttondown;m_rmt_sched	lm_popup
alignment la_align

String 	ls_temp, &
			ls_date_column, &
			ls_curr_col_label

integer 	li_rtn, &
			li_curr_col, &
			li_width, &
			li_date_position
			
ls_curr_col_label = dw_rmt_detail.GetColumnName()

Match(ls_curr_col_label, "quantity_")

IF Match(ls_curr_col_label, "quantity_") Then
	li_curr_col = dw_rmt_detail.GetColumn()
//subtract 8 from the column number to position it on the
//quantity fields.
	li_curr_col = li_curr_col - 8
	If li_curr_col = 1 Then
		li_date_position = 1
	Else
		li_date_position = Truncate((li_curr_col / 2),0)
	End If
	ls_date_column = "pa_date_" + String(li_date_position)

	idt_sched_date = Date(dw_rmt_detail.Describe(ls_date_column + ".Expression"))
	is_shift = Upper(Right(ls_curr_col_label, 1))
	lm_popup = Create m_rmt_sched
	
	lm_popup.m_rmtdetail.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

End If

Destroy lm_popup
end event

event scrollvertical;call super::scrollvertical;
This.PostEvent("ue_set_hscroll")
end event

event dragdrop;call super::dragdrop;Date			ldt_pa_date
String		ls_ColumnName, &
				ls_date_column, &
				ls_string


ls_ColumnName = dwo.Name
IF source.TypeOf() = DataWindow! and &
	ClassName(source) = "dw_rmt_detail" Then
		IF row > 0 THEN
			IF Match(ls_ColumnName, "quantity_") and ib_drag Then
				If IsNumber(Mid(ls_ColumnName,10,2)) Then
					ls_string = Mid(ls_ColumnName,10,2) 
				Else
					ls_string = Mid(ls_ColumnName,10,1)
				End If
				ls_date_column = "pa_date_" + ls_string
				ldt_pa_date = Date(dw_rmt_detail.Describe(ls_date_column + ".Expression"))
				If idt_drag_date <= ldt_pa_date Then
					dw_rmt_detail.SetItem(il_drag_row, ls_ColumnName, il_drag_qty)
					iw_frame.SetMicroHelp("Ready")
				Else
					iw_frame.SetMicroHelp("Quantity cannot be dropped in this area")
				End If
			End If

		END IF
END IF

 dw_rmt_detail.SelectRow(il_drag_row,False)
end event

event dragwithin;call super::dragwithin;//this code will scroll the data window while the user is 
//in drag drop mode, doesn't work kindly so it is commented
//out for now


//Date				ldt_pa_date
//Long				ll_hpos
//String 			ls_ColumnName, ls_date_column, &
//					ls_hpos, &
//					ls_min_hpos, &
//					ls_cur_hpos, &
//					ls_modstring
//
//
////This will move the horizontal scroll bar based on 
////where the cursor moves.
//ls_ColumnName = dwo.Name
//ls_min_hpos = dw_rmt_detail.Describe("quantity_1a.X")
//
//IF source.TypeOf() = DataWindow! and &
//	ClassName(source) = "dw_rmt_detail" Then
//		IF row > 0 THEN
//			ls_hpos = dw_rmt_detail.Describe(ls_ColumnName + ".X")
//			ll_hpos = Long(ls_hpos) - Long(dw_rmt_detail.Describe(ls_ColumnName + ".Width"))
//			ls_hpos = String(ll_hpos)
//			If ls_hpos < ls_min_hpos Then
//				ls_hpos = ls_min_hpos
//			End If
//			ls_modstring = &
//					"DataWindow.HorizontalScrollPosition2=" + ls_hpos
//			dw_rmt_detail.Modify(ls_modstring)
//			
//		END IF
//END IF
end event

event ue_mousemove;call super::ue_mousemove;Long			ll_row

String	ls_temp, &
			ls_col_label, &
			ls_ColumnName
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return


end event

