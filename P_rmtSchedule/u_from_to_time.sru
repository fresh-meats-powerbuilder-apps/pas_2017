HA$PBExportHeader$u_from_to_time.sru
forward
global type u_from_to_time from datawindow
end type
end forward

global type u_from_to_time from datawindow
integer width = 928
integer height = 84
integer taborder = 10
string dataobject = "d_from_to_time"
boolean border = false
boolean livescroll = true
end type
global u_from_to_time u_from_to_time

forward prototypes
public function string uf_get_from_time ()
public function string uf_get_to_time ()
public subroutine uf_disable ()
public subroutine uf_enable ()
public subroutine uf_set_from_time (string as_from_time)
public subroutine uf_set_to_time (string as_to_time)
end prototypes

public function string uf_get_from_time ();return This.GetItemString(1, "from_time")
end function

public function string uf_get_to_time ();return This.GetItemString(1, "to_time")
end function

public subroutine uf_disable ();This.object.from_time.Background.Color = 12632256
This.object.from_time.Protect = 1
This.object.to_time.Background.Color = 12632256
This.object.to_time.Protect = 1
end subroutine

public subroutine uf_enable ();This.object.from_time.Background.Color = 16777215
This.object.from_time.Protect = 0
This.object.to_time.Background.Color = 16777215
This.object.to_time.Protect = 0

end subroutine

public subroutine uf_set_from_time (string as_from_time);This.SetItem(1, "from_time", as_from_time)
This.AcceptText()
end subroutine

public subroutine uf_set_to_time (string as_to_time);This.SetItem(1, "to_time", as_to_time)
This.AcceptText()
end subroutine

event constructor;String	ls_text

This.InsertRow(0)


end event

event getfocus;This.SelectText(1, Len(This.GetText()))
end event

on u_from_to_time.create
end on

on u_from_to_time.destroy
end on

