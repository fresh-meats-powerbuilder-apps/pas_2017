HA$PBExportHeader$w_pas_source_inq.srw
forward
global type w_pas_source_inq from w_base_response_ext
end type
type dw_begin_end_date from u_base_dw_ext within w_pas_source_inq
end type
type dw_plant from u_plant within w_pas_source_inq
end type
type dw_fab_product_code from u_fab_product_code within w_pas_source_inq
end type
end forward

global type w_pas_source_inq from w_base_response_ext
integer x = 635
integer y = 724
integer width = 1719
integer height = 688
string title = "Sourcing Inquiry"
long backcolor = 67108864
dw_begin_end_date dw_begin_end_date
dw_plant dw_plant
dw_fab_product_code dw_fab_product_code
end type
global w_pas_source_inq w_pas_source_inq

type variables
w_pas_source	iw_Parent_Window

datawindowchild    idwc_master, &
                           idwc_slave

Int		ii_PaFuture, &
		ii_PAPast
		
String	is_begin_date, &
			is_end_date
end variables

event open;call super::open;iw_Parent_Window = Message.PowerObjectParm
If Not IsValid(iw_Parent_Window) Then 
	Close(This)
	return
End if

This.Title = iw_Parent_Window.Title + " Inquire"


end event

event ue_postopen;call super::ue_postopen;Date	ldt_temp

Int	li_rc

String	ls_PARange, &
			ls_temp


// get the number of valid days for PA
If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "SRCRANGE", ls_PARange) = -1 Then
//If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "FUTURE", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "Future PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
ii_PAFuture = Integer(ls_PARange) -1

If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "PAST", ls_PARange) = -1 Then
	// Row not found
	MessageBox("PA Range", "Past PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
ii_PAPast = Integer(ls_PARange)


ls_temp = iw_parent_window.dw_plant.uf_Get_Plant_Code()
If Not iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.Reset()
	dw_plant.ImportString(ls_temp + '~t' + iw_parent_window.dw_plant.uf_Get_Plant_Descr() )
End if

//ls_temp = iw_parent_window.dw_fab_product_code.uf_Get_Product_Code()
//If iw_frame.iu_string.nf_IsEmpty(ls_temp) Then
//	dw_fab_product_code.uf_set_product_state('','')
//	dw_fab_product_code.uf_set_product_status('','')
//else
//	ls_temp = iw_parent_window.dw_fab_product_code.uf_exportstring()
//	dw_fab_product_code.uf_ImportString(ls_temp,true)
//// dw_fab_product_code.uf_set_product_state('1')
//End if
//
//ls_temp = dw_fab_product_code.uf_Get_Product_State()
//dw_fab_product_code.uf_set_product_state(ls_temp)
//
//ls_temp = dw_fab_product_code.uf_Get_Product_Status()
//dw_fab_product_code.uf_set_product_status(ls_temp)

ls_temp = iw_parent_window.dw_fab_product_code.uf_exportstring()
dw_fab_product_code.uf_ImportString(ls_temp,true)

ldt_temp = Date(iw_parent_window.dw_begin_end_date.uf_GetBeginDate())
// take out code 07/30/02 ibdkdld
//If DaysAfter(Today(), ldt_temp) < 0 - ii_PAPast Then 
//	ldt_temp = RelativeDate(Today(), 0 - ii_PAPast)
//End if
dw_begin_end_date.SetItem( 1, "begin_date", ldt_temp)


ldt_temp = Date(iw_parent_window.dw_begin_end_date.uf_GetEndDate())
// take out code 07/30/02 ibdkdld
//If ldt_temp < Today() Or DaysAfter(Today(), ldt_temp) > ii_paFuture Then 
//	ldt_temp = RelativeDate(Today(), ii_PAFuture)
//End if
dw_begin_end_date.SetItem( 1, "end_date", ldt_temp)
						

end event

on w_pas_source_inq.create
int iCurrent
call super::create
this.dw_begin_end_date=create dw_begin_end_date
this.dw_plant=create dw_plant
this.dw_fab_product_code=create dw_fab_product_code
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_begin_end_date
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_fab_product_code
end on

on w_pas_source_inq.destroy
call super::destroy
destroy(this.dw_begin_end_date)
destroy(this.dw_plant)
destroy(this.dw_fab_product_code)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "N")
end event

event ue_base_ok;call super::ue_base_ok;date		ld_begin_date, &
			ld_end_date
string 	ls_temp

IF (dw_plant.AcceptText() = -1) OR (dw_fab_product_code.AcceptText() = -1) OR &
	(dw_begin_end_date.AcceptText() = -1) THEN return

IF len(dw_fab_product_code.uf_get_product_code( )) > 0 then
	if dw_fab_product_code.uf_validate( ) = false then return
End If

IF iw_parent_window.dw_plant.uf_Set_Plant_Code( &
		dw_plant.GetItemString( 1, "location_code" )) <> 0 THEN Return

ls_temp = dw_fab_product_code.uf_exportstring( )
iw_parent_window.dw_fab_product_code.uf_importstring(ls_temp,true)

ld_begin_Date	= dw_begin_end_date.GetItemDate( 1, "begin_date" )
ld_end_date		= dw_begin_end_date.GetItemDate( 1, "end_date" )
If ld_end_date < ld_begin_date Then
	iw_frame.SetMicroHelp("End Date must be greater than Begin Date")
	dw_begin_end_date.SetColumn("end_date")
	dw_begin_end_date.SetFocus()
	dw_begin_end_date.TriggerEvent(ItemError!)
	Return
End If
	
IF NOT iw_parent_window.dw_begin_end_date.uf_SetBeginDate(ld_begin_Date) THEN Return
IF NOT iw_parent_window.dw_begin_end_date.uf_SetEndDate(ld_end_date) THEN Return

CloseWithReturn(This, "Y")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_pas_source_inq
integer x = 1390
integer y = 432
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_pas_source_inq
integer x = 1088
integer y = 432
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_pas_source_inq
integer x = 786
integer y = 432
integer taborder = 40
end type

type dw_begin_end_date from u_base_dw_ext within w_pas_source_inq
event begindatechange ( )
event enddatechange ( )
integer x = 87
integer y = 372
integer width = 672
integer height = 188
integer taborder = 30
string dataobject = "d_pas_begin_end_date"
boolean border = false
end type

event begindateChange();If is_begin_date > "" Then This.setitem(1,"begin_date",date(is_begin_date))
end event

event enddatechange();If is_end_date > "" Then This.setitem(1,"end_date",date(is_end_date))
end event

event itemerror;call super::itemerror;Return 1
end event

event itemchanged;call super::itemchanged;string		ls_gettext,ls_temp1,ls_temp
Long			ls_DaysAfter,ll_rtn
Date			ldt_date
ls_gettext = GetText()
//dld 07/26
nvuo_pa_business_rules 	nvuo_pa

CHOOSE CASE GetColumnName() 
	CASE "begin_date"
//dld 07/26/2002 old
//dld add back in prod problem 10/11
		ldt_date = Date(ls_gettext)
		ls_DaysAfter = DaysAfter(Today(), ldt_date) 
		If ls_DaysAfter < (0 - ii_PAPast) THEN
			iw_frame.SetMicroHelp("Begin Date must be within " + String(ii_PaPast) + &
											" days of today" )
			Return 1
		else	
		//		END IF	

//new
//dld add if and upper else for prod problem 10/11
			IF ldt_date > today() then
				ls_temp1 = ''
				ll_rtn = nvuo_pa.uf_check_pa_date_ext(Date(data),ls_temp1)
				
				Choose case ll_rtn
					Case 0 
						// Valid PA Range
						Return 0
					Case 1	
						// Valid Ext PA WeekEnd date
						Return 0
					Case 2
						// Set valid Ext PA WeekEnd date 
						ls_temp = 'The date of ' + String(date(data),'mm/dd/yyyy') + ' is beyond the daily PA range -- ' & 
						  + 'the Begin Date has been changed to the next Sunday date'
						iw_frame.SetMicroHelp(ls_temp)
						is_begin_date = ls_temp1
						This.PostEvent("begindatechange")
						Return 1
					Case 3
						// Invalid Ext PA WeekEnd date/PA Range date
						ls_temp = 'You must select a date between '+ String(today(),'mm/dd/yyyy') + ' and ' &
								+ ls_temp1 + ' or a Sunday date beyond ' + ls_temp1  
						iw_frame.SetMicroHelp(ls_temp)
						This.SetFocus()
						This.SelectText ( 1, 15 )
						Return 1
					Case 4
						ls_temp = 'The date of ' + String(date(data),'mm/dd/yyyy') + ' is beyond the extended PA weekend date -- ' & 
						  + 'the Begin Date has been changed to the max Sunday date'
						iw_frame.SetMicroHelp(ls_temp)
						is_begin_date = ls_temp1
						This.PostEvent("begindatechange")
						return 1
					Case 5 
						ls_temp = 'The date of ' + String(date(data),'mm/dd/yyyy') + ' is beyond the Daily PA date range -- ' & 
						  + 'the date has been changed to the max Daily PA Date'
						iw_frame.SetMicroHelp(ls_temp)		
						is_begin_date = ls_temp1
						This.PostEvent("begindatechange")
						return 1
				end choose		
			end If	
		End if
	CASE "end_date"
//dld 07/26/2002 old
//dld add back in prod problem 10/14
		IF Date(ls_GetText) < GetItemDate(1, "begin_date") THEN
			iw_frame.SetMicroHelp("End Date must be greater then Begin Date" )
			Return 1
		//END IF	
//new
		else
//dld add if and upper else for prod problem 10/14
			ldt_date = Date(data)
			IF ldt_date > today() then
				ls_temp1 = ''
				ll_rtn = nvuo_pa.uf_check_pa_date_ext(Date(data),ls_temp1)
				
				Choose case ll_rtn
					Case 0 
						// Valid PA Range
						Return 0
					Case 1	
						// Valid Ext PA WeekEnd date
						Return 0
					Case 2
						// Set valid Ext PA WeekEnd date 
						ls_temp = 'The date of ' + String(date(data),'mm/dd/yyyy') + ' is beyond the daily PA range -- ' & 
						  + 'the End Date has been changed to the next Sunday date'
						iw_frame.SetMicroHelp(ls_temp)
						is_end_date = ls_temp1
						This.PostEvent("enddatechange")
						Return 1
					Case 3
						// Invalid Ext PA WeekEnd date/PA Range date
						ls_temp = 'You must select a date between '+ String(today(),'mm/dd/yyyy') + ' and ' &
								+ ls_temp1 + ' or a Sunday date beyond ' + ls_temp1  
						iw_frame.SetMicroHelp(ls_temp)
						This.SetFocus()
						This.SelectText ( 1, 15 )
						Return 1
					Case 4
						ls_temp = 'The date of ' + String(date(data),'mm/dd/yyyy') + ' is beyond the extended PA weekend date -- ' & 
						  + 'the End Date has been changed to the max Sunday date'
						iw_frame.SetMicroHelp(ls_temp)
						is_end_date = ls_temp1
						This.PostEvent("enddatechange")
						return 1
					Case 5 
						ls_temp = 'The date of ' + String(date(data),'mm/dd/yyyy') + ' is beyond the Daily PA date range -- ' & 
						  + 'the date has been changed to the max Daily PA Date'
						iw_frame.SetMicroHelp(ls_temp)		
						is_end_date = ls_temp1
						This.PostEvent("enddatechange")
						return 1
				end choose		
			end If	
		End if
END CHOOSE

//Removed ibdkdld 09/15/2002
//If DaysAfter(Today(), Date(ls_gettext)) > ii_paFuture Then
//	iw_frame.SetMicroHelp("Date must be within " + String(ii_paFuture) + " days of today")
//	Return 1
//End if
end event

on getfocus;call u_base_dw_ext::getfocus;SelectText(1, Len(GetText()))
end on

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;SelectText(1, Len(GetText()))
end on

on constructor;call u_base_dw_ext::constructor;This.InsertRow(0)
end on

type dw_plant from u_plant within w_pas_source_inq
integer x = 192
integer y = 8
integer taborder = 10
end type

type dw_fab_product_code from u_fab_product_code within w_pas_source_inq
integer x = 9
integer y = 84
integer height = 276
integer taborder = 11
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;string ls_product

If dwo.name = "fab_product_code" Then
		ls_product = data
		IF IsNull(ls_product) THEN ls_product = ""
		If Len(Trim(ls_product)) = 0 Then
			dw_fab_product_code.uf_set_product_state('','')
			dw_fab_product_code.uf_set_product_status('','')
			return 0
		Else
			If len(dw_fab_product_code.uf_get_product_state( )) = 0 then
				dw_fab_product_code.uf_set_product_state('1')
			End If
			If len(dw_fab_product_code.uf_get_product_status( )) = 0 then
				dw_fab_product_code.uf_set_product_status('G')
			End If
		End If
End If


return AncestorReturnValue
end event

