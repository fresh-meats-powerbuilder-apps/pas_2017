HA$PBExportHeader$w_pas_source.srw
forward
global type w_pas_source from w_base_sheet_ext
end type
type dw_fab_product_code from u_fab_product_code within w_pas_source
end type
type dw_begin_end_date from u_source_begin_end_date within w_pas_source
end type
type dw_source from u_base_dw_ext within w_pas_source
end type
type dw_plant from u_plant within w_pas_source
end type
end forward

global type w_pas_source from w_base_sheet_ext
integer x = 41
integer y = 268
integer width = 3442
integer height = 1596
string title = "Source"
long backcolor = 67108864
event ue_postitemchanged pbm_custom01
dw_fab_product_code dw_fab_product_code
dw_begin_end_date dw_begin_end_date
dw_source dw_source
dw_plant dw_plant
end type
global w_pas_source w_pas_source

type variables
Private:

Int					ii_PARange, &
						ii_PAPast
						
s_error     		istr_error_info
u_pas202          iu_pas202
u_pas201          iu_pas201
u_ws_pas1	iu_ws_pas1

nvuo_fab_product_code invuo_fab_product_code

DataWindowChild  	idwc_plant, &
                  idwc_wgt_fab_product

string      		is_plant, &
						is_product, &
						is_state, &
						is_status, &
						is_date, &
						is_ColName, &
						is_product_info

date        		id_date

Time					it_ChangedTime

Long					il_ChangedRow

String				is_ChangedColumnName

INTEGER				ii_Query_Retry_Count  

end variables

forward prototypes
public function boolean wf_product (character ac_product[10])
public subroutine wf_delete ()
public subroutine wf_filenew ()
public function boolean wf_validate (long al_row)
public subroutine wf_print ()
public function boolean wf_set_wgt (long al_row, integer ai_qty, string as_product_code, string as_plant)
public function boolean wf_update ()
public function boolean wf_deleterow ()
protected function boolean wf_inq_srce_char ()
public function boolean wf_retrieve ()
public function boolean wf_addrow ()
end prototypes

event ue_postitemchanged;call super::ue_postitemchanged;If il_ChangedRow > 0 And il_ChangedRow <= dw_Source.RowCount() &
		And Not iw_frame.iu_string.nf_IsEmpty(is_ChangedColumnName) Then
	dw_source.SetItem(il_ChangedRow, is_ChangedColumnName, it_ChangedTime)
End if

end event

public function boolean wf_product (character ac_product[10]);boolean			lb_return

String			ls_product_info, &
					ls_product_desc, &
					ls_product_flag,as_product
					
iw_frame.SetMicroHelp("Wait.. Inquiring Database")

istr_error_info.se_event_name			= "wf_product"
istr_error_info.se_procedure_name	= "nf_pasp03br"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF
as_product = ac_product
//lb_return	= iu_pas201.nf_pasp03br( istr_Error_Info, ac_product, is_product_info)
lb_return	= iu_ws_pas1.nf_pasp03fr( istr_Error_Info, as_product, is_product_info)

ls_product_info = is_product_info
ls_product_desc = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')
ls_product_flag = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')

IF ls_product_flag = ' ' THEN
	iw_frame.SetMicroHelp(ac_product + " is Not a Valid Product Code.")
END IF

Return( ls_product_flag <> ' ' )
end function

public subroutine wf_delete ();wf_deleterow()
end subroutine

public subroutine wf_filenew ();wf_addrow()
end subroutine

public function boolean wf_validate (long al_row);Date					ldt_production_date,ldt_process_date
Time					lt_begin_time, &
						lt_temp

Long					ll_rtn, &
						ll_nbrrows

String				ls_plant, &
						ls_fab_product, &
						ls_production_date, &
						ls_searchstring, &
						ls_begin_time, &
						ls_production_plant, &
						ls_temp, &
						ls_product_state, &
						ls_product_status


ll_nbrrows = dw_source.RowCount()

ls_temp = dw_source.GetItemString(al_row, "shift")
IF IsNull(ls_temp) or Len(trim(ls_temp)) = 0 Then 
	MessageBox("Shift", "Please enter a shift.  Shift cannot be left blank.")
	This.SetRedraw(False)
	dw_source.ScrollToRow(al_row)
	dw_source.SetColumn("shift")
	dw_source.SetFocus()
	This.SetRedraw(True)
	Return False
End If

lt_temp = dw_source.GetItemTime(al_row, "begin_time")
IF IsNull(lt_temp) or lt_temp <= Time('0:00') Then 
	MessageBox("Begin Time", "Please enter a Begin Time Greater Than 0:00.")
	This.SetRedraw(False)
	dw_source.ScrollToRow(al_row)
	dw_source.SetColumn("begin_time")
	dw_source.SetFocus()
	This.SetRedraw(True)
	Return False
End If

lt_temp = dw_source.GetItemTime(al_row, "end_time")
IF IsNull(lt_temp) or lt_temp <= Time('0:00') Then 
	MessageBox("Begin Time", "Please enter a End Time Greater Than 0:00.")
	This.SetRedraw(False)
	dw_source.ScrollToRow(al_row)
	dw_source.SetColumn("end_time")
	dw_source.SetFocus()
	This.SetRedraw(True)
	Return False
End If


If ll_nbrrows < 2 Then Return True

IF al_row < 0 THEN Return True

ls_plant = dw_source.GetItemString(al_row, "plant")
ls_fab_product = dw_source.GetItemString(al_row, "fab_product_code")
ls_product_state = dw_source.GetItemString(al_row, "product_state")
ls_product_status = dw_source.GetItemString(al_row, "product_status")
ldt_production_date = dw_source.GetItemDate &
	(al_row, "production_date")
lt_begin_time = dw_source.GetItemTime(al_row, "begin_time")
//dld
//CREATE TYPE 2 UNIQUE INDEX IBP.IPASSRC1 ON IBP.TPASSRCE                  
//  (PLANT_CODE ASC, PROCESS_DATE ASC, BEGIN_TIME ASC,                     
//   FAB_PRODUCT_CODE ASC, PRODUCTION_PLANT ASC, PRODUCTION_DATE ASC )     
ldt_process_date = dw_source.GetItemDate &
	(al_row, "process_date")
ls_production_plant = dw_source.GetItemString(al_row, "production_plant")

	ls_SearchString	= "plant = '" + ls_plant + &
						"' and fab_product_code = '" + ls_fab_product + &
						"' and production_plant = '" + ls_production_plant + &
						"' and product_state = '" + ls_product_state + &
						"' and product_status = '" + ls_product_status + &
						"' and production_date = Date('" + String(ldt_production_date) + &
						"') and process_date = Date('" + String(ldt_process_date) + &
						"') and begin_time = Time('" + String(lt_begin_time) + "')"
// Find a matching row excluding the current row.

CHOOSE CASE al_row 
	CASE 1
		ll_rtn = dw_source.Find  &
				( ls_SearchString, al_row + 1, ll_nbrrows)
	CASE 2 to (ll_nbrrows - 1)
		ll_rtn = dw_source.Find ( ls_SearchString, al_row - 1, 1)
		If ll_rtn = 0 Then ll_rtn = dw_source.Find  &
			(ls_SearchString, al_row + 1, ll_nbrrows)
	CASE ll_nbrrows 
		ll_rtn = dw_source.Find ( ls_SearchString, al_row - 1, 1)
END CHOOSE

If ll_rtn > 0 Then
	MessageBox ("Begin Time", "There are duplicate Sources with the" + &
  					" same Begin Time.") 
	dw_source.SetRedraw(False)
	dw_source.ScrollToRow(al_row)
	dw_source.SetColumn("begin_time")
	dw_source.SetRow(al_row)
	dw_source.SelectRow(ll_rtn, True)
	dw_source.SelectRow(al_row, True)
	dw_source.SetRedraw(True)
	Return False
End If

Return True
end function

public subroutine wf_print ();//dmk
dw_source.object.DataWindow.Print.Orientation = 1

dw_source.Print()
end subroutine

public function boolean wf_set_wgt (long al_row, integer ai_qty, string as_product_code, string as_plant);boolean 	lb_return

string 	ls_product_code, ls_plant_code, &
			ls_product_info, ls_temp
			 
integer 	li_wgt, li_qty, &
			li_prod_avg_wgt, &
			li_sales_avg_wgt
									 
ls_temp = dw_source.GetItemString(al_row,"source_flag")
If ls_temp > ' ' Then
	//do nothing
Else
	invuo_fab_product_code.uf_check_product(as_product_code)
	ls_product_info =	invuo_fab_product_code.uf_get_product_data( )
	ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // description
	ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')	  // flag
End If
			
If ls_temp = 'S' Then
	ls_product_code = as_product_code
  	ls_plant_code = as_plant
 			
	li_wgt = dw_source.GetItemNumber(al_row, "source_wgt") 
	li_qty = ai_qty
	If li_wgt = 0 Or IsNull(li_wgt) Then
		CONNECT USING SQLCA;	
		SELECT plt.production_avg_wt
				,plt.sales_avg_weight
  		INTO :li_prod_avg_wgt 
		  	 ,:li_sales_avg_wgt
  //		FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
		FROM sku_plant plt
  		WHERE ( plt.sku_product_code = :ls_product_code ) AND  
         	( plt.plant_code  = :ls_plant_code)   ;

		If SQLCA.SQLCode = 00 Then
			If li_prod_avg_wgt > 0 Then
				dw_source.SetItem(al_row, "source_wgt", li_prod_avg_wgt * li_qty)
			Else
				dw_source.SetItem(al_row, "source_wgt", li_sales_avg_wgt * li_qty)
			End if
			
			IF dw_source.GetItemString(al_row, "qty_wgt_ind") = 'B' Then
			//do nothing
			ELSE
				IF dw_source.GetItemString(al_row, "qty_wgt_ind") = 'Q' THEN
					dw_source.SetItem(al_row, "qty_wgt_ind", 'B')
				ELSE
					dw_source.SetItem(al_row, "qty_wgt_ind", 'P')
				END IF
			END IF
			
		Else
			dw_source.SetItem(al_row, "source_wgt", 0)
		End If
  	End If
Else
	dw_source.SetItem(al_row, "source_wgt", 0)
End If

Return TRUE
end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter, &
					li_check
		
					
long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount, &
					ll_ModifiedRow, &
					ll_wgt, &
					ll_qty, & 
					ll_prev_avg_wgt, ll_avg_wgt, &
					ll_prod_avg_wgt, &
		 			ll_sales_avg_wgt

string			ls_tpassrce_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_UpdateSource, &
					ls_hold_code, ls_hold_plant

dwItemStatus	lis_status

IF dw_source.AcceptText() = -1 THEN Return( False )
IF dw_source.ModifiedCount() + dw_source.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

//f_required ( dw_source, true )

//ll_Row = 1
//DO WHILE ll_Row <> 0
//	// If there's an error, exit
//	IF dw_source.FindRequired(Primary!,	ll_Row, li_ColNbr, ls_ColName, FALSE ) < 0 THEN 
//		f_required ( dw_source, false )
//		Return( False )
//	END IF
//
//	// If a row was found, save the row and column
//	IF ll_Row <> 0 THEN
//		// Get the text of that column's label.
//		ls_TextName = ls_colname + "_t.Text"
//		ls_colname = dw_source.Describe(ls_TextName)
//		// Tell the user which column to fill in.
//		MessageBox("Required Value Missing",  "Please enter a value for '"  &
//			+ ls_colname + "', row " + String(ll_Row) + ".", StopSign! )
//		// Make the problem column current.
//		f_required ( dw_source, false )
//		dw_source.SetColumn(li_ColNbr)
//		dw_source.ScrollToRow(ll_Row)
//		dw_source.SetFocus()
//		Return( False )
//	END IF
//	// If no row was found, drop out of the loop
//LOOP

//f_required ( dw_source, false )

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas202 ) THEN
	iu_pas202	=  CREATE u_pas202
END IF

ll_NbrRows = dw_source.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_source.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		IF iw_frame.iu_string.nf_IsEmpty(dw_source.GetItemString(ll_row, "wgt_fab_product")) Then 
			MessageBox("Wgt Fab Product", "Please choose a Wgt Fab Product.")
			This.SetRedraw(False)
			dw_source.ScrollToRow(ll_row)
			dw_source.SetColumn("wgt_fab_product")
			dw_source.SetFocus()
			This.SetRedraw(True)
			Return False
		End If
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

//dmk- March 2009 - check that the qty_wgt_ind = 'B', signifying
// both fields have changed.  Check that both are negative or positive
ll_row = 0
Do
	ll_ModifiedRow = ll_row

	ll_ModifiedRow = dw_source.GetNextModified(ll_row, Primary!)
	ll_row = ll_ModifiedRow

	If ll_row > 0 Then
		//If dw_source.GetItemString(ll_row,'qty_wgt_ind') <> 'B' Then
		If dw_source.GetItemString(ll_row,'qty_wgt_ind') = 'Q' Then
			ll_prev_avg_wgt = dw_source.GetItemNumber(ll_row, "prev_avg_wgt")
			ll_qty = dw_source.GetItemNumber(ll_row, "source_qty")
			ll_wgt = dw_source.GetItemNumber(ll_row, "source_wgt") 
			If ll_qty = 0 Then
				ll_avg_wgt = 0
			Else
				if ll_wgt = 0 or IsNull(ll_prev_avg_wgt) Then
						
					ls_hold_code = dw_source.GetItemString(ll_row, 'fab_product_code')
					ls_hold_plant = dw_plant.GetItemString(1, 'location_code')
					CONNECT USING SQLCA;	
					SELECT plt.production_avg_wt
							,plt.sales_avg_weight
  					INTO :ll_prod_avg_wgt 
		 			 	 ,:ll_sales_avg_wgt
  			//		FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
					FROM sku_plant plt
  					WHERE ( plt.sku_product_code = :ls_hold_code ) AND  
         				( plt.plant_code  = :ls_hold_plant)   ;
					
					If SQLCA.SQLCode = 00 Then
						If ll_prod_avg_wgt > 0 Then
							ll_avg_wgt = ll_prod_avg_wgt
						Else
							ll_avg_wgt = ll_sales_avg_wgt 
						End if	
					Else
						ll_avg_wgt = 0
					End If
				Else
					ll_avg_wgt = ll_prev_avg_wgt
				End If
			End If
			dw_source.SetItem(ll_row, "source_wgt", ll_qty * ll_avg_wgt)
		End if
		
		ll_qty = dw_source.GetItemNumber(ll_row, "source_qty")
		ll_wgt = dw_source.GetItemNumber(ll_row, "source_wgt") 
	
		If dw_source.GetItemString(ll_row,'source_flag') <> 'S' Then
			//do nothing
		ElseIf ((ll_qty < 0) And (ll_wgt > 0)) or &
		   	 ((ll_qty > 0) And (ll_wgt < 0)) Then
			SetMicroHelp("Quantity and Weight must be both negative or both positive.")
			dw_source.SetRow(ll_row)
			dw_source.ScrollToRow(ll_row)
			dw_source.SetColumn("source_qty")
			dw_source.SetFocus()
			return false
		ElseIf (ll_qty = 0) And (ll_wgt <> 0) Then
			SetMicroHelp("Weight must be zero if quantity is zero.")
			dw_source.SetRow(ll_row)
			dw_source.ScrollToRow(ll_row)
			dw_source.SetColumn("source_wgt")
			dw_source.SetFocus()
			return false
		End If
	End If
Loop while ll_row > 0 


ls_UpdateSource = iw_frame.iu_string.nf_BuildUpdateString(dw_Source) 


//If not iu_pas202.nf_upd_source(istr_error_info, ls_updatesource) Then  Return False

If not iu_ws_pas1.nf_pasp11fr(istr_error_info, ls_UpdateSource) Then  Return False

dw_source.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_Source.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_source.SetItem(li_Counter, 'update_flag', ' ')
//dmk March 2009
	dw_source.SetItem(li_Counter, 'qty_wgt_ind', ' ')
Next
dw_source.ResetUpdate()
dw_source.Sort()
dw_source.GroupCalc()
dw_source.SetColumn("begin_time")
dw_source.SetFocus()
dw_source.SetReDraw(True)
is_plant   = ""
is_date    = ""
is_product = ""
idwc_wgt_fab_product.Reset()

//revgll//
if istr_error_info.se_message > Space(1) then 
	messagebox("PA Message", "A source has been modified after the production schedule was generated for " + istr_error_info.se_message + ", shift A.")
end if	

Return( True )
end function

public function boolean wf_deleterow ();Boolean		lb_ret
long			ll_row
date	     ldt_process_date

dw_source.SetFocus()
ll_row = dw_source.GetRow()
ldt_process_date = dw_source.GetItemDate(ll_row, "process_date")
If ldt_process_date < Today() then
	SetMicroHelp("Cannot delete a source with a process date less than current date")
	This.SetRedraw(False)
	dw_source.ScrollToRow(ll_row)
	dw_source.SetFocus()
	This.SetRedraw(True)
	Return False
end if
lb_ret = super::wf_deleterow()
dw_source.SelectRow(0,False)
dw_source.SelectRow(dw_source.GetRow(),True)
return lb_ret

end function

protected function boolean wf_inq_srce_char ();boolean			lb_return

Long				ll_row

string			ls_input, &
					ls_char, &
					ls_plant, &
					ls_product, & 
					ls_state, &
					ls_status

date				ld_date


ll_row = dw_source.GetRow()

ls_plant	= dw_source.GetItemString( ll_row, "plant" )
ls_product = dw_source.GetItemString( ll_row, "fab_product_code" )
ls_state = dw_source.GetItemString( ll_row, "product_state" )
ls_status = dw_source.GetItemString( ll_row, "product_status" )
//dld changed from production_date to process-date
ld_date				= dw_source.GetItemDate( ll_row, "process_date" )

IF (ls_plant = is_plant) AND (ls_product = is_product) AND &
	(is_state	= ls_state) AND (ld_date = id_date) AND (is_status = ls_status) THEN
	Return( true )
END IF

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

IF Not IsValid( iu_pas202 ) THEN
	iu_pas202	=  CREATE u_pas202
END IF

ls_input			= ls_plant + '~t' + &
					  ls_product + '~t' + &
					  ls_state + '~t' + &
					  ls_status + '~t' + &
					  string(ld_date, "yyyy-mm-dd") + '~r~n'
					  
//lb_return		= iu_pas202.nf_inq_srce_char( istr_error_info, ls_input, ls_char )
lb_return		= iu_ws_pas1.nf_pasp09fr( istr_error_info, ls_input, ls_char )
 
idwc_wgt_fab_product.Reset()

is_plant		= ls_plant
is_product	= ls_product
is_state		= ls_state
is_status	= ls_status 
id_date		= ld_date

// Check the return code from the above function call
IF lb_return THEN
	If Len(Trim(ls_char)) > 0 Then
		iw_frame.SetMicroHelp(String( idwc_wgt_fab_product.ImportString( ls_char ) ) + &
			" Characteristic(s) retrieved")
	Else
		iw_frame.SetMicroHelp("0 Characteristics retrieved")
	End if

	Return( True )
ELSE
	Return( False )
END IF


end function

public function boolean wf_retrieve ();Long				ll_value, ll_row, &
					ll_qty, ll_wgt

Boolean			lb_return

String			ls_input, &
					ls_plant_code, &
					ls_product_code, & 
					ls_product_state, &
					ls_product_status

u_string_functions	lu_string


Call w_base_sheet::closequery
IF Message.ReturnValue = 1 THEN Return False

OpenWithParm(w_pas_source_inq, This)
If Message.StringParm <> 'Y' Then return False

dw_source.uf_ChangeRowStatus(1, NotModified!)

SetPointer(Hourglass!)
iw_frame.SetMicroHelp("Wait... Inquiring Database")

ls_plant_code = dw_plant.uf_Get_Plant_Code()
If lu_string.nf_IsEmpty(ls_plant_code) Then
	ls_plant_code = ''
End If

ls_product_code = dw_fab_product_code.uf_Get_Product_Code()

If lu_string.nf_IsEmpty(ls_product_code) Then
	ls_product_code = ''
End If

ls_product_state = dw_fab_product_code.uf_Get_Product_State()
If lu_string.nf_IsEmpty(ls_product_state) Then
	ls_product_state = ''
End If

ls_product_status = dw_fab_product_code.uf_Get_Product_Status()
If lu_string.nf_IsEmpty(ls_product_status) Then
	ls_product_status = ''
End If

ls_Input = ls_plant_code + '~t' + &
				ls_product_code + '~t' + &
				ls_product_state + '~t' + &
				ls_product_status + '~t' + &
				dw_begin_end_date.uf_GetBeginDate() + '~t' + &
				dw_begin_end_date.uf_GetEndDate() + '~r~n'

istr_error_info.se_event_name			= "wf_retrieve"
istr_error_info.se_procedure_name	= "nf_pasp10ar"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_pas202 ) THEN
	iu_pas202	=  CREATE u_pas202
END IF

This.SetRedraw( False )

//lb_return	= iu_pas202.nf_inq_source( istr_Error_Info, ls_input, dw_source )
lb_return	= iu_ws_pas1.nf_pasp10fr( istr_Error_Info, ls_input, dw_source )
 
IF NOT lb_return THEN
	This.SetRedraw(True)
	Return( False )
END IF

//sr8171   dmk set the prev_quantity field to be used later
ll_value = dw_source.RowCount()
ll_row = 1
if ll_value > 0 Then
	Do
		ll_qty = dw_source.GetItemNumber(ll_row, "source_qty")
		ll_wgt = dw_source.GetItemNumber(ll_row, "source_wgt")
		if ll_qty > 0 Then
			dw_source.SetItem(ll_row, "prev_avg_wgt", ll_wgt/ll_qty )
		end if
		ll_row = ll_row + 1
	Loop While ll_row <= ll_value
End if

ll_value = dw_source.RowCount()
If ll_value < 0 Then ll_value = 0

dw_source.ResetUpdate()

IF ll_value > 0 THEN
	dw_source.Sort()
	dw_source.GroupCalc()
	dw_source.SetFocus()
	dw_source.ScrollToRow(1)
	dw_source.SetColumn( "end_time" )
	dw_source.TriggerEvent("RowFocusChanged")
	dw_source.SelectRow(0,False)
	dw_source.SelectRow(dw_source.GetRow(),True)
END IF
SetMicroHelp(String(ll_value) + " rows retrieved")
This.SetRedraw( True )
Return( True )

 
end function

public function boolean wf_addrow ();Long	ll_row, &
		ll_CurrentRow, &
		ll_GroupChange

DataWindowChild	ldwc_type
//jac 12-13
dw_source.GetChild ("shift", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("shift")
dw_source.SetColumn( "shift" )
//dw_source.InsertRow(0)
//
		
dw_source.SetRedraw(False)		
ll_CurrentRow = dw_source.GetRow()
ll_GroupChange = dw_Source.FindGroupChange(ll_CurrentRow , 1)
If ll_GroupChange = ll_CurrentRow Then
	ll_GroupChange = dw_Source.FindGroupChange(ll_CurrentRow  + 1, 1)
End if
ll_row = dw_source.InsertRow(ll_GroupChange)

If ll_CurrentRow > 0 Then
	dw_source.SetItem(ll_Row, 'plant', dw_source.GetItemString(ll_CurrentRow, 'plant'))
	//dld
	dw_source.SetItem(ll_Row, 'production_plant', dw_source.GetItemString(ll_CurrentRow, 'plant'))

	dw_source.SetItem(ll_Row, 'fab_product_code', dw_source.GetItemString(ll_CurrentRow, &
						'fab_product_code'))
	dw_source.SetItem(ll_Row, 'source_qty_uom', dw_source.GetItemString(ll_CurrentRow, &
						'source_qty_uom'))
						
	dw_source.SetItem(ll_Row, 'production_date', dw_source.GetItemDate(ll_CurrentRow, &
						'production_date'))
	//dld
	dw_source.SetItem(ll_Row, 'process_date', dw_source.GetItemDate(ll_CurrentRow, &
						'process_date'))
	dw_source.SetItem(ll_Row, 'shift', dw_source.GetItemString(ll_CurrentRow, 'shift'))
	dw_source.SetItem(ll_Row, 'pa_date', dw_source.GetItemDate(ll_CurrentRow, 'pa_date'))

	dw_source.SetColumn("begin_time")
	//sap
	dw_source.SetItem(ll_row, 'begin_type_seconds', '00')
Else
	dw_source.SetColumn("plant")
End if

dw_source.SetRow(ll_row)
dw_source.ScrollToRow(ll_row)
dw_source.SetFocus()
dw_source.SetRedraw(True)		

Return( true )
end function

event close;call super::close;IF IsValid( iu_pas202 ) THEN
	DESTROY iu_pas202
END IF

IF IsValid( iu_pas201 ) THEN
	DESTROY iu_pas201
END IF

IF IsValid( iu_ws_pas1 ) THEN
	DESTROY iu_ws_pas1
END IF



dw_source.ShareDataOff ( )
dw_plant.ShareDataOff ( )


end event

event ue_postopen;call super::ue_postopen;This.PostEvent("ue_query")
iu_ws_pas1 = Create u_ws_pas1

end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_Disable('m_graph')

end on

event ue_query;call super::ue_query;DataWindowChild	ldwc_SourcePlant, &
						ldwc_Source_Qty_Uom,ldwc_productionPlant, &
						ldwc_begin_type_seconds

integer		li_rc

String		ls_PARange

dw_plant.GetChild ( "location_code", idwc_plant )
dw_source.GetChild ( "plant", ldwc_SourcePlant )
//dld
dw_source.GetChild ( "production_plant", ldwc_productionPlant )
dw_source.GetChild ( "source_qty_uom", ldwc_source_qty_uom )

idwc_plant.ShareData( ldwc_SourcePlant )
//dld
idwc_plant.ShareData( ldwc_productionPlant )
 
ldwc_source_qty_uom.SetTransObject(SQLCA)
ldwc_source_qty_uom.Retrieve("FABUOM")
ldwc_source_qty_uom.Retrieve("UOM")


istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "Sourcing"
istr_error_info.se_user_id 		= sqlca.userid

// get the number of valid days for PA
//If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "FUTURE", ls_PARange) = -1 Then
If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "SRCRANGE", ls_PARange) = -1 Then
	if (sqlca.sqldbcode = 10005 or sqlca.sqldbcode = 10025) &
		and ii_Query_Retry_Count < 2 then
		
		ii_Query_Retry_Count += 1
		SQLCA.postevent("ue_reconnect")
		this.postevent("ue_query")
		return
	end if

	// Row not found
	MessageBox("PA Range", "Future PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
ii_PARange = Integer(ls_PARange) -1

// comment until it gets approved
If iw_frame.iu_netwise_data.nf_GetShortDesc("PA RANGE", "PAST", ls_PARange) = -1 Then
	if (sqlca.sqldbcode = 10005 or sqlca.sqldbcode = 10025) &
		and ii_Query_Retry_Count < 2 then
		
		ii_Query_Retry_Count += 1
		SQLCA.postevent("ue_reconnect")
		this.postevent("ue_query")
		return
	end if
	
	// Row not found
	MessageBox("PA Range", "Past PA Range not defined in TUTLTYPE, Contact Applications.", StopSign!)
	Close(This)
	return
End if
ii_PAPast = Integer(ls_PARange)

dw_begin_end_date.uf_SetBeginDate(Today())
dw_begin_end_date.uf_SetEndDate(RelativeDate(Today(), ii_PARange))

wf_retrieve()
end event

event resize;call super::resize;integer li_x		
integer li_y
integer ly_height = 115

li_x = (dw_source.x * 2) + 30 
//li_y = dw_source.y + 115


if il_BorderPaddingHeight > ly_height Then
                ly_height = il_BorderPaddingHeight
End If


li_y = dw_source.y + ly_height


if width > li_x Then
	dw_source.width	= width - li_x
end if

if height > li_y then
	dw_source.height	= height - li_y
end if
end event

on w_pas_source.create
int iCurrent
call super::create
this.dw_fab_product_code=create dw_fab_product_code
this.dw_begin_end_date=create dw_begin_end_date
this.dw_source=create dw_source
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_fab_product_code
this.Control[iCurrent+2]=this.dw_begin_end_date
this.Control[iCurrent+3]=this.dw_source
this.Control[iCurrent+4]=this.dw_plant
end on

on w_pas_source.destroy
call super::destroy
destroy(this.dw_fab_product_code)
destroy(this.dw_begin_end_date)
destroy(this.dw_source)
destroy(this.dw_plant)
end on

event activate;call super::activate;//iw_frame.im_menu.mf_Enable('m_graph')

end event

type dw_fab_product_code from u_fab_product_code within w_pas_source
integer y = 96
integer width = 1646
integer height = 288
integer taborder = 20
end type

event constructor;call super::constructor;this.resetupdate( )
this.uf_enable(false)
end event

type dw_begin_end_date from u_source_begin_end_date within w_pas_source
integer x = 1691
integer y = 60
integer width = 754
integer taborder = 0
end type

on rowfocuschanged;call u_source_begin_end_date::rowfocuschanged;This.ResetUpdate()

end on

on constructor;call u_source_begin_end_date::constructor;ib_Updateable = False
TriggerEvent( "ue_insertrow" )
This.ResetUpdate()

end on

type dw_source from u_base_dw_ext within w_pas_source
event ue_dnwdropdown pbm_dwndropdown
event test pbm_dwntabout
integer y = 384
integer width = 3365
integer height = 1056
integer taborder = 30
string dataobject = "d_source"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_dnwdropdown;Long			ll_row
string		ls_ColName, &
				ls_plant, &
				ls_product, &
				ls_state, &
				ls_status

date			ld_date

ls_ColName = GetColumnName()

IF ls_ColName = "wgt_fab_product" THEN
	GetChild ( "wgt_fab_product", idwc_wgt_fab_product )
	ll_row = GetRow()
	ls_plant = GetItemString( ll_row, "plant" )
	ls_product = GetItemString( ll_row, "fab_product_code" )
	ls_state = GetItemString( ll_row, "product_state" )
	ls_status = GetItemString( ll_row, "product_status" )
	//dld process_date instead of production_date
	ld_date = GetItemDate( ll_row, "process_date" )
	
	IF iw_frame.iu_string.nf_IsEmpty(ls_plant) or &
			iw_frame.iu_string.nf_IsEmpty(ls_product) OR &
			iw_frame.iu_string.nf_IsEmpty(ls_state) or &	
			iw_frame.iu_string.nf_IsEmpty(ls_status) or &
					NOT IsDate( string(ld_Date)) Or IsNull(ld_Date) THEN
		MessageBox( "", "Enter a Valid Plant, Product, State, Status, and Date First.", stopsign!, ok! )
		Return 1
	END IF
	
	// check to see if you have all the 3 fields needed
	wf_inq_srce_char()
	This.SetItem(This.GetRow(), "wgt_fab_product", "")
END IF
end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

int			li_rc, &
				li_seconds,li_temp, li_qty

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_GetText, &
				ls_temp, &
				ls_plant, &
				ls_product_code, &
				ls_product
				
				
Date			ldt_temp, ldt_temp2	
nvuo_pa_business_rules	u_rule


is_ColName = GetColumnName()
ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0

CHOOSE CASE is_ColName
	CASE "plant"
		ll_plant_row = idwc_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_plant.RowCount()) 
		IF ll_plant_row = 0 THEN
			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
			This.selecttext(1,100)
			Return 1
		END IF
//dld		
		This.SetItem(ll_source_row, 'production_plant', ls_GetText)
//dld		
//dmk March 2009
		le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
		CHOOSE CASE le_RowStatus
		CASE NewModified!, New!
			li_qty = dw_source.GetItemNumber(ll_source_row,"source_qty")
			ls_product_code = dw_source.GetItemString(ll_source_row,"fab_product_code")
			ls_plant = ls_GetText
			wf_set_wgt(ll_source_row, li_qty, ls_product_code, ls_plant)
		END CHOOSE
//dmk
	CASE "production_plant"
		ll_plant_row = idwc_plant.Find( "location_code='"+ls_GetText+"'", 1, idwc_plant.RowCount()) 
		IF ll_plant_row = 0 THEN
			iw_frame.SetMicroHelp(ls_GetText + " is Not a Valid Plant Code")
			This.selecttext(1,100)
			Return 1
		END IF
		
	CASE "fab_product_code"
		ll_find_row = Find("fab_product_code = '" + data + "'",1,RowCount()+1)
		If ll_find_row > 0 Then
			This.SetItem(ll_source_row, 'source_qty_uom',This.GetItemString(ll_find_row,'source_qty_uom'))
		Else
			If not invuo_fab_product_code.uf_check_product(data) then
				If	invuo_fab_product_code.ib_error_occurred then
					iw_frame.SetMicroHelp(invuo_fab_product_code.uf_get_return_message())
					Return 1
				Else
					iw_frame.SetMicroHelp(data + " is an invalid Product Code")
					Return 1
				End If
			End If
			
			// We know we have a valid product -- is it SKU or fab?
			string ls_product_info 
			ls_product_info =	invuo_fab_product_code.uf_get_product_data( )
			ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // description
			ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')	  // flag
		//dmk March 2009
			This.SetItem( ll_source_row, "source_flag", ls_temp)
			
			If ls_temp = 'S' Then
				ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // fab uom
				ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // sku uom
			Else
				ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // fab uom
			End If		
			This.SetItem(ll_source_row, 'source_qty_uom', ls_temp)

			This.SetItem(row, 'product_state', '1')
			This.SetItem(row, 'product_status', 'G')
		End If
	//dmk March 2009
		le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
		CHOOSE CASE le_RowStatus
		CASE NewModified!, New!
			li_qty = dw_source.GetItemNumber(ll_source_row,"source_qty")
			ls_product_code = ls_GetText 
			ls_plant = dw_source.GetItemString(ll_source_row,"plant")
			wf_set_wgt(ll_source_row, li_qty, ls_product_code, ls_plant)
		END CHOOSE
//dmk
		
	CASE "product_state"
		ls_product = this.getitemstring(row, "fab_product_code")
		if trim(data) <> "1" then
			if not invuo_fab_product_code.uf_check_product_state(ls_product, Trim(data)) then	
				iw_Frame.SetMicroHelp(data + " is an invalid Product State for this Product")
				this.setfocus( )
				this.setitem( row, "product_state", "1")
				this.setcolumn("product_state") 
				This.SelectText(1, Len(data))	
				Return 1
			end if
		end if
		
	CASE "product_status"
		ls_product = this.getitemstring(row, "fab_product_code")
		if trim(data) <> "1" then
			if not invuo_fab_product_code.uf_check_product_status(ls_product, Trim(data)) then	
				iw_Frame.SetMicroHelp(data + " is an invalid Product Status for this Product")
				this.setfocus( )
				this.setitem( row, "product_status", "G")
				this.setcolumn("product_status") 
				This.SelectText(1, Len(data))	
				Return 1
			end if
		end if
		
		
	CASE "wgt_fab_product"
		This.GetChild('wgt_fab_product', idwc_wgt_fab_product)
		ll_char_row		= idwc_wgt_fab_product.GetRow()
		If ll_char_row < 1 Then
//			If Not wf_inq_srce_char() Then 
//				This.SetActionCode(1)
				idwc_wgt_fab_product.Reset()
				return
//			Else
//				idwc_wgt_fab_product.SetRow(1)
//				ll_char_row = 1
//			End if
		End if
		li_rc = dw_source.SetItem( ll_source_row, "wgt_product_state", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "wgt_product_state" ))
		li_rc = dw_source.SetItem( ll_source_row, "wgt_product_status", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "wgt_product_status" ))
		li_rc = dw_source.SetItem( ll_source_row, "species", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "species" ))
		li_rc = dw_source.SetItem( ll_source_row, "grade", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "grade" ))
		li_rc = dw_source.SetItem( ll_source_row, "wt_range", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "wgt_range" ))
		li_rc = dw_source.SetItem( ll_source_row, "yld_grade", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "yld_grade" ))
		li_rc = dw_source.SetItem( ll_source_row, "hormone_free_ind", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "hormone_ind" ))
		li_rc = dw_source.SetItem( ll_source_row, "sex", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "sex" ))
		li_rc = dw_source.SetItem( ll_source_row, "char_key", &
					idwc_wgt_fab_product.GetItemString( ll_char_row, "char_key" ))
//dld
	CASE "process_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
					
		If DaysAfter(Today(), Date(ls_GetText)) > ii_paRange Then
			iw_frame.SetMicroHelp("Process Date cannot be greater than " + &
							String(ii_paRange) + " days " + &
							"from today -- Maximum allowable date is " + String(RelativeDate( &
							Today(), ii_paRange), "mm/dd/yyyy"))
			This.selecttext(1,100)
			return 1
		End if

		If DaysAfter(Today(), Date(ls_GetText)) < 0 - ii_paPast Then
			iw_frame.SetMicroHelp("Process Date cannot be less than " + &
							String(ii_paPast) + " days " + &
							"before today -- Minimum allowable date is " + String(RelativeDate( &
							Today(), ii_paPast), "mm/dd/yyyy"))
							This.selecttext(1,100)
							return 1
		End if
		
		if RelativeDate(Date(ls_GetText),1) <= This.GetItemDate(row, "pa_date") Then
			This.SetItem(row, "source_qty", 0)
		End If
		
		This.SetItem(ll_source_row, 'production_date', date(ls_GetText))
// dld can't be > then process_date and < parange dayofinvt of process_date
	CASE "production_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
		
		ls_temp = ' '
		ldt_temp = This.GetItemDate( row, 'process_date')
		If date(ls_gettext) > ldt_temp Then
			iw_frame.SetMicroHelp("Production Date cannot be greater than the Process Date")
			This.selecttext(1,100)
			Return 1
		End If

		li_temp = u_rule.uf_check_parange_dayofinv()
		u_rule.uf_check_parange_dayofinv_date(date(ls_gettext),ldt_temp,ls_temp)
		If ls_temp > ' ' Then
			iw_frame.SetMicroHelp("Production Date cannot be less than " + &
							String(li_temp) + " days " + &
							"from the Process date -- Minimum allowable date is " + ls_temp )
			This.selecttext(1,100)
			return 1
		End If	
		
	Case "begin_time"
		If GetItemStatus(ll_source_row, "end_time", primary!) = DataModified! Then
			If SecondsAfter(Time(ls_gettext), This.GetItemTime(ll_source_row, "end_time")) &
				< -0 Then
				iw_frame.SetMicroHelp("Begin Time must be less than" + &
					" the End Time")
				This.selecttext(1,100)	
				Return 1
			End IF
		End If

		This.SetRedraw(False)
		// This row needs to be deleted and added with the new time
		If This.GetItemStatus(ll_Source_Row, 0, Primary!) <> NewModified! Then
			// I need to set the begin time back to the original value
			// but once it is in the delete buffer, I can't change it
			This.RowsCopy(ll_Source_Row, ll_Source_Row, Primary!, This, 10000, Primary!)

			ll_RowCount = This.RowCount()
			// It is now duplicated at the end of this DW
			This.SetItem(ll_RowCount, 'begin_time', This.GetItemTime(ll_Source_Row, &
							'begin_time', Primary!, True))
			// original value is now set, Move it to the delete buffer
			This.RowsMove(ll_RowCount, ll_RowCount, Primary!, This, 10000, Delete!)
			This.uf_ChangeRowStatus(ll_source_row, NewModified!)
			This.SetItem(ll_source_row, "update_flag", "A")
		End if
		This.SetRedraw(True)

		li_seconds = Second(This.GetItemTime(ll_source_row, 'begin_time', Primary!, True))
		If li_seconds > 0 Then
			it_changedtime = Time(String(Time(ls_getText), "hh:mm") + ":" + &
										 String(li_seconds, "00"))
			il_ChangedRow = ll_Source_Row
			is_ChangedColumnName = 'begin_time'
		End if


	Case "end_time"
		If GetItemStatus(ll_source_row, "begin_time", primary!) = &
				DataModified! Then
			If SecondsAfter(This.GetItemTime(ll_source_row, "begin_time"), Time(ls_GetText)) &
				< -60 Then
				iw_frame.SetMicroHelp("End Time must be greater than" + &
					" the Begin Time")
					This.selecttext(1,100)
				Return 1
			End IF
		End If

		li_seconds = Second(This.GetItemTime(ll_source_row, 'end_time', Primary!, True))
		If li_seconds > 0 Then
			it_ChangedTime = Time(String(Time(ls_getText), "hh:mm") + ":" + &
										 String(li_seconds, "00"))
			il_ChangedRow = ll_Source_Row
			is_ChangedColumnName = 'end_time'
		End if

	Case "source_qty", "actual_quantity"
		If Not IsNumber(ls_GetText) Then
			iw_frame.SetMicroHelp("Quantity must be a number")
			This.selecttext(1,100)
			return 1
		End if
		If Real(ls_GetText) < 0 Then
			iw_frame.SetMicroHelp("Quantity cannot be negative")
			This.selecttext(1,100)
			return 1
		End if
		If is_ColName = "source_qty" Then
			If Integer(data) > 0 Then
				If RelativeDate(This.GetItemDate(ll_source_row, "process_date"),1) <= This.GetItemDate(ll_source_row, "pa_date") Then
					iw_frame.SetMicroHelp("Source quantity must be zero if process date less than PA date")
					This.selecttext(1,100)
					return 1
				End If
			End If
		End If
		
		If is_ColName = "source_qty" Then
			If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, 'source_qty_uom')) Then
				This.SetItem(ll_source_row, 'source_qty_uom', 'PCS')
			End if
	//dmk March 2009
			ls_product_code = dw_source.GetItemString(ll_source_row,"fab_product_code")
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				li_qty = Real(ls_GetText)
				ls_plant = dw_source.GetItemString(ll_source_row,"plant")
				wf_set_wgt(ll_source_row, li_qty, ls_product_code, ls_plant)
			END CHOOSE
			
			ls_temp = dw_source.GetItemString(ll_source_row,"source_flag")
			If ls_temp > ' ' Then
				//do nothing
			Else
				invuo_fab_product_code.uf_check_product(ls_product_code)
				ls_product_info =	invuo_fab_product_code.uf_get_product_data( )
				ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // description
				ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')	  // flag
				This.SetItem( ll_source_row, "source_flag", ls_temp)
			End If
			
			If ls_temp = 'S' Then
			  	IF This.GetItemString(ll_source_row, "qty_wgt_ind") = 'B' Then
					//do nothing
				ELSE
					IF This.GetItemString(ll_source_row, "qty_wgt_ind") = 'P' THEN
						This.SetItem( ll_source_row, "qty_wgt_ind", 'B')
					ELSE
						This.SetItem( ll_source_row, "qty_wgt_ind", 'Q')
					END IF
				END IF
			ELSE
				This.SetItem( ll_source_row, "qty_wgt_ind", 'B')
			END IF
		End If
		
		Case "source_wgt"
		If Not IsNumber(ls_GetText) Then
			iw_frame.SetMicroHelp("Weight must be a number")
			This.selecttext(1,100)
			return 1
		End if
		If Real(ls_GetText) < 0 Then
			iw_frame.SetMicroHelp("Weight cannot be negative")
			This.selecttext(1,100)
			return 1
		End if
		ls_temp = dw_source.GetItemString(ll_source_row,"source_flag")
		If ls_temp > ' ' Then
			//do nothing
		Else
			ls_product_code = dw_source.GetItemString(ll_source_row,"fab_product_code")
			invuo_fab_product_code.uf_check_product(ls_product_code)
			
			ls_product_info =	invuo_fab_product_code.uf_get_product_data( )
			ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')   // description
			ls_temp = iw_frame.iu_string.nf_gettoken(ls_product_info,'~t')	  // flag
		End If
		
		If ls_temp = 'S' Then
			IF This.GetItemString(ll_source_row, "qty_wgt_ind") = 'B' Then
				//do nothing
			ELSE
				IF This.GetItemString(ll_source_row, "qty_wgt_ind") = 'Q' THEN
					This.SetItem( ll_source_row, "qty_wgt_ind", 'B')
				ELSE
					This.SetItem( ll_source_row, "qty_wgt_ind", 'P')
				END IF
			END IF
		ELSE
			This.SetItem( ll_source_row, "qty_wgt_ind", 'B')
		END IF
END CHOOSE

// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

parent.PostEvent("ue_postitemchanged")
iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemfocuschanged;call super::itemfocuschanged;Long	ll_row

ll_row = This.GetRow()
If ll_row < 1 then return

If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_row, "wgt_fab_product")) And &
		Not iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_row, "char_key")) And &
		idwc_wgt_fab_product.RowCount() > 0 Then
	This.SetItem(ll_row, "wgt_fab_product", idwc_wgt_fab_product.GetItemString( &
		idwc_wgt_fab_product.GetRow(), "wgt_fab_product"))
End if

If This.GetColumnName() = "source_qty" Or &
				This.GetColumnName() = "actual_quantity" Then
	This.SelectText(1, 50)
End If


end event

event itemerror;call super::itemerror;String	ls_column_name, &
			ls_GetText


ls_GetText = This.GetText()

Choose Case This.GetColumnName()
//dld		
Case "plant", "fab_product_code","product_state", "product_status", "production_date", "begin_time", &
		"end_time", "wgt_fab_product" , "process_date", "production_plant" 
	This.SelectText(1, Len(ls_gettext))
	return 1
Case "source_qty", "actual_quantity"
	If Not IsNumber(ls_GetText) and Not iw_frame.iu_string.nf_IsEmpty(ls_GetText)Then
		iw_frame.SetMicroHelp("Quantity must be a number")
		This.SelectText(1, Len(ls_gettext))
		return 1
	End if
	If Real(ls_GetText) < 0 Then
		iw_frame.SetMicroHelp("Quantity cannot be negative")
		This.SelectText(1, Len(ls_GetText))
		return 1
	End If
End Choose
end event

event ue_graph;//w_graph		w_grp_child
//Window			lw_response

//Message.PowerObjectParm = This
//Message.StringParm = 'd_source_weekly_graph'
//OpenWithParm(lw_response ,  This, 'w_source_graph_select')
end event

on constructor;call u_base_dw_ext::constructor;is_selection = '1'
//ib_storedatawindowsyntax = TRUE


end on

event getfocus;call super::getfocus;If This.rowcount() > 0 then 
	This.SelectText(1, Len(GetText()))
end if
end event

on ue_keydown;call u_base_dw_ext::ue_keydown;IF KeyDown(KeyEscape!) Then iw_frame.SetMicroHelp &
			(iw_frame.ia_application.MicroHelpDefault)
end on

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_begin_type_seconds

dw_source.GetChild('begin_type_seconds', ldwc_begin_type_seconds)
ldwc_begin_type_seconds.SetTransObject(SQLCA)
ldwc_begin_type_seconds.Retrieve("PASRCETM")

end event

type dw_plant from u_plant within w_pas_source
integer x = 183
integer y = 20
integer width = 1490
integer height = 92
integer taborder = 10
end type

event constructor;call super::constructor;ib_Updateable = False
ib_protected = True
TriggerEvent( "ue_insertrow" )
This.SetRow(1)
end event

