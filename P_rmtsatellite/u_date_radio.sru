HA$PBExportHeader$u_date_radio.sru
$PBExportComments$IBDKDLD
forward
global type u_date_radio from datawindow
end type
end forward

global type u_date_radio from datawindow
int Width=475
int Height=236
int TabOrder=10
string DataObject="d_date_radio"
boolean Border=false
boolean LiveScroll=true
end type
global u_date_radio u_date_radio

forward prototypes
public subroutine uf_set_groupbox_text (string as_text)
public function string uf_get_date_radio ()
public subroutine uf_set_date_radio (string as_choice)
public subroutine uf_enable (boolean ab_enable)
end prototypes

public subroutine uf_set_groupbox_text (string as_text);This.object.display.text = as_text
end subroutine

public function string uf_get_date_radio ();Return This.GetItemString(1,'date_rb')
end function

public subroutine uf_set_date_radio (string as_choice);This.SetItem(1,"date_rb",as_choice)
end subroutine

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("date_rb.Protect = 0 " + &
				"date_rb.Pointer = 'Arrow!'")
	Case False
		This.Modify("date_rb.Protect = 1 " + &
				"date_rb.Pointer = 'Beam!'")
END CHOOSE

end subroutine

event constructor;This.InsertRow(0)
end event

