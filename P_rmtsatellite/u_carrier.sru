HA$PBExportHeader$u_carrier.sru
$PBExportComments$IBDKDLD
forward
global type u_carrier from u_base_dw_ext
end type
end forward

global type u_carrier from u_base_dw_ext
integer width = 384
integer height = 72
string dataobject = "d_carrier"
boolean border = false
end type
global u_carrier u_carrier

forward prototypes
public function string uf_get_carrier ()
public subroutine uf_set_carrier (string as_carrier)
public subroutine uf_enable (boolean ab_enable)
end prototypes

public function string uf_get_carrier ();Return This.GetItemString(1,'carrier')
end function

public subroutine uf_set_carrier (string as_carrier);This.SetItem(1,"carrier",as_carrier)
end subroutine

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("carrier.Protect = 0 " + &
				"carrier.Pointer = 'Arrow!'")	
		This.object.carrier.Background.Color = 16777215

	Case False
		This.Modify("carrier.Protect = 1 " + &
				"carrier.Pointer = 'Beam!'")
		This.object.carrier.Background.Color = 67108864
		ib_updateable = False
END CHOOSE

end subroutine

event constructor;call super::constructor;This.InsertRow(0)
end event

on u_carrier.create
end on

on u_carrier.destroy
end on

