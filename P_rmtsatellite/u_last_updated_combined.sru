HA$PBExportHeader$u_last_updated_combined.sru
forward
global type u_last_updated_combined from datawindow
end type
end forward

global type u_last_updated_combined from datawindow
int Width=1184
int Height=88
int TabOrder=10
string DataObject="d_last_updated_combined"
boolean Border=false
boolean LiveScroll=true
end type
global u_last_updated_combined u_last_updated_combined

forward prototypes
public function integer uf_modified ()
public subroutine uf_set_last_updated (string as_combination)
end prototypes

public function integer uf_modified (); Return 0
end function

public subroutine uf_set_last_updated (string as_combination);This.SetItem(1,"last_update",as_combination)
end subroutine

event constructor;This.InsertRow(0)
end event

