HA$PBExportHeader$u_ship_plant.sru
$PBExportComments$IBDKDLD
forward
global type u_ship_plant from u_base_dw_ext
end type
end forward

global type u_ship_plant from u_base_dw_ext
integer width = 1362
integer height = 72
string dataobject = "d_ship_plant"
boolean border = false
end type
global u_ship_plant u_ship_plant

forward prototypes
public function string uf_get_ship_plant ()
public subroutine uf_set_ship_plant (string as_ship_plant)
public subroutine uf_set_text (string as_text)
public subroutine uf_set_ship_plant_desc (string as_ship_plant_desc)
public function string uf_get_ship_plant_desc ()
public subroutine uf_enable (boolean ab_enable)
end prototypes

public function string uf_get_ship_plant ();Return This.GetItemString(1,'ship_plant')
end function

public subroutine uf_set_ship_plant (string as_ship_plant);This.SetItem(1,"ship_plant",as_ship_plant)
end subroutine

public subroutine uf_set_text (string as_text);This.object.ship_plant_t.Text = as_text
end subroutine

public subroutine uf_set_ship_plant_desc (string as_ship_plant_desc);This.SetItem(1,"ship_plant_desc",as_ship_plant_desc)
end subroutine

public function string uf_get_ship_plant_desc ();Return This.GetItemString(1,'ship_plant_desc')
end function

public subroutine uf_enable (boolean ab_enable);CHOOSE CASE ab_enable
	CASE True
		This.Modify("ship_plant.Protect = 0 " + &
				"ship_plant.Pointer = 'Arrow!'")
		This.object.ship_plant.Background.Color = 16777215
		This.object.ship_plant_desc.Background.Color = 16777215
	Case False
		This.Modify("ship_plant.Protect = 1 " + &
				"ship_plant.Pointer = 'Beam!'")
		This.object.ship_plant.Background.Color = 67108864
		This.object.ship_plant_desc.Background.Color = 67108864
		ib_updateable = False
END CHOOSE

end subroutine

event constructor;call super::constructor;This.InsertRow(0)
end event

on u_ship_plant.create
end on

on u_ship_plant.destroy
end on

