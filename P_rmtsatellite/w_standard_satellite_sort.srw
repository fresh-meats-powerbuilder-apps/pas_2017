HA$PBExportHeader$w_standard_satellite_sort.srw
$PBExportComments$ibdkdld
forward
global type w_standard_satellite_sort from w_base_sheet_ext
end type
type dw_satellite_cattle from u_base_dw_ext within w_standard_satellite_sort
end type
type dw_plant from u_plant within w_standard_satellite_sort
end type
type dw_effective_date from u_effective_date within w_standard_satellite_sort
end type
type dw_last_updated from u_last_updated within w_standard_satellite_sort
end type
type dw_thru_dates from u_begin_end_dates within w_standard_satellite_sort
end type
type dw_user from u_last_userid within w_standard_satellite_sort
end type
type gb_1 from groupbox within w_standard_satellite_sort
end type
end forward

global type w_standard_satellite_sort from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 2875
integer height = 1516
long backcolor = 67108864
dw_satellite_cattle dw_satellite_cattle
dw_plant dw_plant
dw_effective_date dw_effective_date
dw_last_updated dw_last_updated
dw_thru_dates dw_thru_dates
dw_user dw_user
gb_1 gb_1
end type
global w_standard_satellite_sort w_standard_satellite_sort

type variables
u_rmt001		iu_rmt001

s_error		istr_error_info

String		is_update_string, &
		is_update_string1

Long		il_rec_count

Window		iw_parentwindow
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_filenew ()
public subroutine wf_delete ()
public function boolean wf_update_modify (long al_row, string as_header, string as_column_name, string as_buffer, boolean ab_original_value)
public function boolean wf_update_dump (long al_row)
public function boolean wf_validate ()
end prototypes

public function boolean wf_retrieve ();Long						ll_rec_count, &
							ll_row, &
							ll_message, &
							ll_count, &
							ll_column = 1
							
String					ls_plant, &
							ls_column, & 
							ls_header, &
							ls_column_headings, &
							ls_detail, &
							ls_date, &
							ls_inquire_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_time, &
							ls_describe
							
u_string_functions 	lu_string	
Date						ld_date
DateTime 				ldt_datetime
	
If dw_plant.AcceptText() = -1 Then Return False
If dw_effective_date.AcceptText() = -1 Then Return False
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If


SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 
// clear out prev values reset the datawindow
ls_describe = "col_" + String(ll_column) + ".Visible"
Do While dw_satellite_cattle.Describe(ls_describe) > '0'
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 0 " + &
		" col_" + String(ll_column) + ".Visible = 0 " + &
		" co_col_" + String(ll_column) + ".Visible = 0 "
	ll_column ++
	ls_describe = "col_" + String(ll_column) + ".Visible"
Loop
dw_satellite_cattle.Modify ( ls_TitleModify )
ll_column = 1	
ls_TitleModify = ''

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr06mr_inq_manifest_detail"
iu_rmt001 = Create u_rmt001

ls_inquire_date = String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd') 

ls_plant	  = dw_plant.uf_get_plant_code()
ls_header  = ls_plant + '~t'
ls_header += ls_inquire_date + '~r~n' 

//MessageBox("header",ls_header)

dw_satellite_cattle.reset()

//If Not iu_rmt001.uf_rmtr10mr_inq_stand_sat_sort(istr_error_info, & 
//										ls_detail, &
//										ls_column_headings, &
//										ls_header) Then 
//										This.SetRedraw(True) 
//										Return False
//End If			

//MessageBox("Return header",ls_header)
//MessageBox("Column headings",ls_column_headings)
//MessageBox("Detail",ls_detail)

ls_date = lu_string.nf_gettoken(ls_header,"~t")
ls_Time = lu_string.nf_gettoken(ls_header,"~t")
dw_user.uf_set_user(lu_string.nf_gettoken(ls_header,"~t"))

ldt_datetime = DateTime(date(ls_date),Time(ls_time))
dw_last_updated.uf_set_last_updated(ldt_datetime)
//
//ls_column_headings = 'NOROLL~tNO ROLL~tDAVE 3~tDAVE 4~tDAVE 5~tDAVE 6~tDAVE 7~tDAVE 8~tDAVE 9~tDAVE 10~tDAVE 11~tDAVE 12~t'
// If nothing sent down they need to set up plant characteristics
If lu_string.nf_amiempty(ls_column_headings) Then
	ll_message = 1
End If

DO WHILE Not lu_string.nf_amiempty(ls_column_headings) or ll_column > 50
	ls_temp = lu_string.nf_gettoken(ls_column_headings,"~t")
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 1 " + &
		" col_" + String(ll_column) + ".Visible = 1 " + &
		" co_col_" + String(ll_column) + ".Visible = 1 "
	ll_column ++
LOOP
dw_satellite_cattle.Modify ( ls_TitleModify )


//ls_detail = &
//'23~tN~t90~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'24~tY~t.88~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'25~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'26~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'27~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'283~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'293~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'231~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'232~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'233~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'234~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'235~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'236~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'237~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'238~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'239~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'123~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'223~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'323~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'423~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'523~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'623~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'723~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'823~tN~t.01~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' 
//MessageBox("ls_detail",ls_detail)
//
ll_rec_count = dw_satellite_cattle.ImportString(ls_detail)
//dw_satellite_cattle.SetItem(dw_satellite_cattle.rowcount(),'protect','N')

If ll_rec_count > 0 Then
	ld_date = Date(ls_inquire_date)
	// Set update dates to date inquired on 
	dw_thru_dates.uf_set_begin_date(ld_date)
	dw_thru_dates.uf_set_end_date(ld_Date)
	//reset the status
	dw_thru_dates.ResetUpdate()
	
//	FOR ll_row = 1 TO ll_rec_count
//		If dw_satellite_cattle.GetItemString(ll_row,'dump_ind') = 'Y' Then
//			dw_satellite_cattle.SetItem(ll_row,'protect','Y')
//			Exit
//		End If
//	NEXT
	
	IF ll_message = 0 Then
		ll_message = 4
	Else
		ll_message = 1
	End If
Else
	IF ll_message = 0 Then
		ll_message = 3
	Else		
		ll_message = 2 
	End If
End if
// Must do because of the reinquire
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '500'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '500'

This.SetRedraw(True) 
//If lu_string.nf_isempty(String(dw_satellite_cattle.GetitemDecimal(1,'col_1'))) Then
//	If dw_satellite_cattle.GetitemDecimal(1,'col_1') = 0 Then
//		messagebox('worked',' ')
//	End If
CHOOSE CASE ll_message
	CASE 1
		Beep(1)
		SetMicroHelp('There are no Characteristics '+ &
		'associated with Ship Plant ' + ls_plant)
	CASE 2
		SetMicroHelp("0 Rows Retrieved")
	CASE 3
		Beep(1)
		SetMicroHelp('There are no Destination Plants ' + &
		'associated with Ship Plant ' + ls_plant)
	CASE 4
		SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
END CHOOSE

dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.SetFocus()
 
Return True

end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_moddate, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name
dwItemStatus	ldwis_status			
u_string_functions u_string
Date				ldt_end,ldt_begin

Integer			li_test

IF dw_satellite_cattle.AcceptText() = -1 Then
	Return False
End If

//If dw_thru_dates.TriggerEvent(Itemchanged!) = -1 Then
//	Return False
//End If	
//If dw_thru_dates.AcceptText() = -1 Then 
 	//Return False
//End If
ll_moddate = dw_thru_dates.ModifiedCount()
ll_modrows = dw_satellite_cattle.ModifiedCount()
ll_delrows = dw_satellite_cattle.DeletedCount()

IF ll_modrows <= 0 and ll_delrows <= 0  and ll_moddate <= 0 Then 
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

ls_header = ''
ls_header += dw_plant.uf_get_plant_code() + '~t'
ldt_begin = dw_thru_dates.uf_get_begin_date()
li_test  = Year(ldt_begin)  
If li_test = 1900 or IsNull(li_test) Then
	messagebox('Begin Date','The Begin Date is not valid.',StopSign!)
	dw_thru_dates.SetColumn('begin_date')
	dw_thru_dates.Setfocus()
	dw_thru_dates.SelectText(1,100)
	Return false
Else
	ls_header += String(ldt_begin, 'yyyy-mm-dd') + '~t'
End If

ldt_end = dw_thru_dates.uf_get_end_date()
li_test  = Year(ldt_end)  
If li_test = 1900 or IsNull(li_test) Then
	messagebox('End Date','The End Date is not valid.',StopSign!)
	dw_thru_dates.SetColumn('end_date')
	dw_thru_dates.Setfocus()
	dw_thru_dates.SelectText(1,100)
	Return false
Else
	ls_header += String(ldt_end, 'yyyy-mm-dd') + '~t'
End If

If Not wf_validate() Then
	Return False
End If

SetMicroHelp("Wait, Updating ...")
SetPointer(HourGlass!)

This.SetRedraw(False)

is_update_string = ""
is_update_string1 = ""
// If date range changed must send all rows back for modification
IF ll_moddate > 0 Then
	For ll_count = 1 To dw_satellite_cattle.RowCount()
		ldwis_status = dw_satellite_cattle.GetItemStatus(ll_count,"dump_ind", Primary!)
		// If dump ind changed then send another update string
		If ldwis_status = DataModified! Then
			wf_update_dump(ll_count)
		End If
		ll_column_num = 1
		ls_describe = "col_" + String(ll_column_num) + ".Visible"
		// loop through all visable columns to return everthing back to be modified 
		Do While dw_satellite_cattle.Describe(ls_describe) > '0'
			ls_column_name = "col_" + String(ll_column_num)
			wf_update_modify(ll_count, ls_header,ls_column_name,lc_status_ind,False)  
			ll_column_num ++
			ls_describe = "col_" + String(ll_column_num) + ".Visible"
		Loop
	Next
// else just return modified rows	
Else
	ll_row = 0
	dw_satellite_cattle.SelectRow(0,False)
	
	For ll_count = 1 To ll_modrows
		ll_Row = dw_satellite_cattle.GetNextModified(ll_Row, Primary!) 
		ldwis_status = dw_satellite_cattle.GetItemStatus(ll_row,"dump_ind", Primary!)
		// If dump ind changed then send another update string
		If ldwis_status = DataModified! Then
			wf_update_dump(ll_row)
		End If
		ll_column_num = 1
		ls_describe = "col_" + String(ll_column_num) + ".Visible"
		// loop through all visable columns checking status 
		Do While dw_satellite_cattle.Describe(ls_describe) > '0'
			ls_column_name = "col_" + String(ll_column_num)
			If dw_satellite_cattle.GetItemStatus(ll_row,ls_column_name, Primary!) = DataModified! Then
				wf_update_modify(ll_row, ls_header,ls_column_name,lc_status_ind,False)  
			End If
			ll_column_num ++
			ls_describe = "col_" + String(ll_column_num) + ".Visible"
		Loop
	Next
End If

For ll_count = 1 To ll_delrows 
	lc_status_ind = 'D'
	If Not This.wf_update_modify(ll_row, ls_header,ls_column_name,lc_status_ind,False)  Then 
		This.SetRedraw(True)
		Return False
	End if
Next

//MessageBox('header',ls_header)
//MessageBox('Update String',is_update_string)
//MessageBox('Update dump',is_update_string1)

//IF Not iu_rmt001.uf_rmtr11mr_upd_stand_sat_sort(istr_error_info, ls_header, &
//												is_update_string,is_update_string1) THEN 
//	This.SetRedraw(True)
//	Return False
//End if

SetMicroHelp("Modification Successful")
dw_satellite_cattle.ResetUpdate()
dw_thru_dates.ResetUpdate()
This.SetRedraw(True)

Return( True )
end function

public subroutine wf_filenew ();wf_addrow()
end subroutine

public subroutine wf_delete ();wf_deleteRow()
end subroutine

public function boolean wf_update_modify (long al_row, string as_header, string as_column_name, string as_buffer, boolean ab_original_value);String						ls_describe

DWBuffer						ldwb_buffer


CHOOSE CASE as_buffer
	CASE 'D'
		ldwb_buffer = Delete!
	CASE ELSE
		ldwb_buffer = Primary!
END CHOOSE


is_update_string += &
	dw_satellite_cattle.GetItemString(al_row,"dest",ldwb_buffer, ab_original_value) + "~t" 

ls_describe = as_column_name + "_t" + ".text"	

is_update_string += dw_satellite_cattle.Describe(ls_describe) + "~t"

is_update_string += &
	String(dw_satellite_cattle.GetItemDecimal(al_row,as_column_name,ldwb_buffer, ab_original_value)) + "~r~n"	 


Return True
end function

public function boolean wf_update_dump (long al_row);is_update_string1 += &
	dw_satellite_cattle.GetItemString(al_row,'dest') + "~t"	 

is_update_string1 += &
	dw_satellite_cattle.GetItemString(al_row,"dump_ind") + "~r~n"

Return True
end function

public function boolean wf_validate ();Long		ll_row,ll_column_num
String 	ls_describe,ls_column_name
Decimal	ld_100_percent = 100.00,ld_column
Boolean	lb_error

ll_column_num = 1
ls_describe = "co_col_" + String(ll_column_num) + ".Visible"
// loop through all visable columns  
Do While dw_satellite_cattle.Describe(ls_describe) > '0'
	ls_column_name = "co_col_" + String(ll_column_num)
	// If the computed column doesn't = 100 error out
	ld_column = Dec(dw_satellite_cattle.GetItemNumber(1,ls_column_name))
	If Not ld_column = ld_100_percent Then
		ls_column_name = "col_" + String(ll_column_num) + "_t"
		ls_column_name = dw_satellite_cattle.Describe(ls_column_name + '.Text')
		// If there is only one row and it is a dump don't set column to protected field
		If dw_satellite_cattle.RowCount() = 1 and &
				dw_satellite_cattle.GetItemString(1, 'dump_ind') = "Y" Then
			dw_satellite_cattle.SetColumn('dump_ind')
		Else
			dw_satellite_cattle.SetColumn('col_' + String(ll_column_num))
			//dw_satellite_cattle.GetColumnName().SelectText(1,100)
		End If
		iw_frame.SetMicroHelp('The total for ' + ls_column_name + ' must equal 100 percent') 
		Beep(1)
		lb_error = True
		Exit
	End If
	ll_column_num ++
	ls_describe = "co_col_" + String(ll_column_num) + ".Visible"
Loop

If lb_error Then
	Return False
Else
	Return True
End If
end function

on w_standard_satellite_sort.create
int iCurrent
call super::create
this.dw_satellite_cattle=create dw_satellite_cattle
this.dw_plant=create dw_plant
this.dw_effective_date=create dw_effective_date
this.dw_last_updated=create dw_last_updated
this.dw_thru_dates=create dw_thru_dates
this.dw_user=create dw_user
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_satellite_cattle
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_effective_date
this.Control[iCurrent+4]=this.dw_last_updated
this.Control[iCurrent+5]=this.dw_thru_dates
this.Control[iCurrent+6]=this.dw_user
this.Control[iCurrent+7]=this.gb_1
end on

on w_standard_satellite_sort.destroy
call super::destroy
destroy(this.dw_satellite_cattle)
destroy(this.dw_plant)
destroy(this.dw_effective_date)
destroy(this.dw_last_updated)
destroy(this.dw_thru_dates)
destroy(this.dw_user)
destroy(this.gb_1)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;String 					ls_temp
u_string_functions 	lu_string


ls_temp = Message.StringParm	

This.Title = 'Standard Satellite Sort'
//dw_truck.uf_enable(False)
//dw_ship_plant.uf_enable(False)
dw_thru_dates.uf_enable(True)
dw_thru_dates.uf_set_date_type('parange')

dw_effective_date.uf_enable(False)

//dw_manifest_num.uf_enable(False)
dw_plant.disable()

dw_effective_date.uf_set_text('Ship Date:')

//If lu_string.nf_IsEmpty(ls_temp)  Then 
//	ib_no_inquire = False
//Else
//	ib_no_inquire = True
//	dw_plant.uf_set_plant_code(lu_string.nf_gettoken(ls_temp,"~t"))
//	dw_effective_date.uf_set_effective_date(date(lu_string.nf_gettoken(ls_temp,"~t")))
////	dw_manifest_num.uf_set_manifest_num(lu_string.nf_gettoken(ls_temp,"~t"))
//End If


end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'effective_date'
		message.StringParm = String(dw_effective_date.uf_get_effective_date(), 'YYYY-MM-dd')
	Case 'date_object_text'
		message.StringParm = 'Ship Date:' + '~t' + 'PA' + '~t'
End choose

end event

event ue_postopen;call super::ue_postopen;This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "satcat"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_carcass_tracking_inq'

iu_rmt001 = Create u_rmt001

wf_retrieve()


end event

event ue_set_data;call super::ue_set_data;
Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date( Date(as_value ))
End Choose


end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288

integer li_x		= 57
integer li_y		= 318

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

  
//dw_satellite_cattle.width	= newwidth - (30 +li_x)
//dw_satellite_cattle.height	= newheight - (30 + li_y)

dw_satellite_cattle.width	= newwidth - (li_x)
dw_satellite_cattle.height	= newheight - (li_y)

// Must do because of the split horizonal bar 
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '500'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '500'


end event

type dw_satellite_cattle from u_base_dw_ext within w_standard_satellite_sort
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
integer y = 260
integer width = 2738
integer height = 1132
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_standard_satellite_sort"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event ue_post_itemchanged;Long 		ll_column,ll_row_count,ll_dump
String	ls_column_name,ls_describe
Decimal	ld_compute,ld_column_total,ld_100_percent = 100.00
/////////////////////////////////////
// If there is a dump plant set the calculate the values
// Need to add up all occurances of all the columns
ll_column = 1
ls_describe = "col_" + String(ll_column) + ".Visible"
Do While dw_satellite_cattle.Describe(ls_describe) > '0'
	ls_column_name = "col_" + String(ll_column)
	ld_column_total = 0
	FOR ll_row_count = 1 TO dw_satellite_cattle.RowCount()
		If dw_satellite_cattle.GetItemString(ll_row_count, 'dump_ind') = 'N' Then
			ld_column_total += dw_satellite_cattle.GetItemDecimal(ll_row_count,ls_column_name)
		Else
		//If dump plant just get the row to use later
			ll_dump = ll_row_count
		End If
	NEXT
	ld_compute = ld_100_percent - ld_column_total
// IF there is any percent left set the dump plants percent
	If ld_compute >= 0 Then
		dw_satellite_cattle.SetItem( ll_dump, ls_column_name, ld_compute)
	Else
		iw_frame.SetMicroHelp("You may not exceed 100 percent on any column")
		This.SelectText(1, 100)
//		Return 1
	End IF
	ll_column ++
	ls_describe = "col_" + String(ll_column) + ".Visible"
Loop
/////////////////////////////////////////		

end event

event itemchanged;call super::itemchanged;Long 		ll_row_count,ll_dump,ll_row,ll_column
String	ls_column,ls_column_name,ls_temp,ls_describe
Decimal	ld_column_total,ld_100_percent= 100.00,ld_compute,ld_temp
Boolean	lb_post

// I did this to make the code more generic
ls_column_name = dwo.name
ls_column = left(dwo.name,4)

CHOOSE CASE ls_column
	CASE 'dump'
		ll_row_count = dw_satellite_cattle.RowCount()
		CHOOSE CASE row
			CASE 1
				ll_dump = dw_satellite_cattle.Find  &
					( "dump_ind = 'Y'", row + 1, ll_row_count)
			CASE 2 to (ll_row_count - 1)
				ll_dump = dw_satellite_cattle.Find ( "dump_ind = 'Y'", &
					row - 1, 1)
				If ll_dump = 0 Then ll_dump = dw_satellite_cattle.Find  &
					("dump_ind = 'Y'", row + 1, ll_row_count)
			CASE ll_row_count 
				ll_dump = dw_satellite_cattle.Find ( "dump_ind = 'Y'", &
					row - 1, 1)
		END CHOOSE	
		If ll_dump <> 0 Then This.SetItem(ll_dump, "dump_ind", "N")
		lb_post = True
///////////////////////////////////////
//// If there is a dump plant set the calculate the values
//// Need to add up all occurances of all the columns
//		ll_column = 1
//		ls_describe = "col_" + String(ll_column) + ".Visible"
//		Do While dw_satellite_cattle.Describe(ls_describe) > '0'
//			ls_column_name = "col_" + String(ll_column)
//			ld_column_total = 0
//			FOR ll_row_count = 1 TO dw_satellite_cattle.RowCount()
//				If dw_satellite_cattle.GetItemString(ll_row_count, 'dump_ind') = 'N' Then
//					ld_column_total += dw_satellite_cattle.GetItemDecimal(ll_row_count,ls_column_name)
//				Else
//				//If dump plant just get the row to use later
//					ll_dump = ll_row_count
//				End If
//			NEXT
//			ld_compute = ld_100_percent - ld_column_total
//		// IF there is any percent left set the dump plants percent
//			If ld_compute >= 0 Then
//				dw_satellite_cattle.SetItem( ll_dump, ls_column_name, ld_compute)
//			Else
//				iw_frame.SetMicroHelp("You may not exceed 100 percent on any column")
//				This.SelectText(1, 100)
//				Return 1
//			End IF
//			ll_column ++
//			ls_describe = "col_" + String(ll_column) + ".Visible"
//		Loop
///////////////////////////////////////////		
	CASE 'col_'
		// Insure data is valid
		If Not IsNumber(data) Then
			iw_frame.SetMicroHelp("All Columns must be numeric")
			This.SelectText(1, 100)
			Return 1
		End If
		IF Sign(Integer(data)) = -1 Then
			iw_frame.SetMicroHelp("The Number must be Positive")
			This.SelectText(1, 100)
			Return 1
		End If
		// Need to add up all occurances of the column
		FOR ll_row_count = 1 TO dw_satellite_cattle.RowCount()
//			ls_temp = dw_satellite_cattle.GetItemString(ll_row_count, 'dump_ind')
//			messagebox('Dump Ind', 'Row = ' + string(ll_Row_count) + '~r~n' &
//						+ 'Getitemstring result = ' + ls_temp)
			If dw_satellite_cattle.GetItemString(ll_row_count, 'dump_ind') = 'N' Then
				If ll_row_count = row Then
					ld_column_total += Dec(Data)
				Else
					ld_column_total += dw_satellite_cattle.GetItemDecimal(ll_row_count,ls_column_name)
				End If
			Else
				//If dump plant just get the row to use later
				ll_dump = ll_row_count
			End If
		NEXT
		// Sub accumulated total 
		ld_compute = ld_100_percent - ld_column_total
		// IF there is any percent left set the dump plants percent
		If ld_compute >= 0 Then
			dw_satellite_cattle.SetItem( ll_dump, ls_column_name, ld_compute)
		Else
			iw_frame.SetMicroHelp("You may not exceed 100 percent on any column")
			This.SelectText(1, 100)
			Return 1
		End IF
END CHOOSE

If lb_post Then This.PostEvent('ue_post_itemchanged')

Return 0


end event

event constructor;call super::constructor; iw_parentwindow = Parent
end event

event itemerror;call super::itemerror;Return 2
end event

event doubleclicked;/* I had to override this it was causing a datawindow error when
you double clicked on the check box any ideas let me know. ibdkdld */ 
end event

event getfocus;//dw_thru_dates.AcceptText()

end event

type dw_plant from u_plant within w_standard_satellite_sort
integer width = 1449
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()

end event

type dw_effective_date from u_effective_date within w_standard_satellite_sort
integer x = 1422
integer y = 4
integer width = 722
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;uf_set_effective_date(Today())
uf_enable(False)
end event

type dw_last_updated from u_last_updated within w_standard_satellite_sort
integer y = 88
integer taborder = 0
boolean bringtotop = true
end type

type dw_thru_dates from u_begin_end_dates within w_standard_satellite_sort
event ue_post_validate ( )
integer x = 2153
integer y = 64
integer width = 672
integer height = 168
integer taborder = 20
boolean bringtotop = true
boolean livescroll = false
end type

event ue_post_validate;String				ls_columnname

Date					ldt_value


ls_columnname = This.GetColumnName()

Choose Case ls_columnname 
	Case "begin_date" 
		ldt_value = This.GetItemDate(1,'begin_date')
		If ldt_value < today() Then
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("Begin Date must be greater than or equal to today")   
//			Return 1
		End If
		If ldt_value > This.Getitemdate(1,"end_date") Then
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("Begin Date must be less than End Date")   
//			Return 1
		End If
	Case "end_date" 
		ldt_value = This.GetitemDate(1,'end_date')
		IF Upper(is_date_type) = 'PARANGE' Then
			If ldt_value > idt_parange_end_date then 
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("End date cannot be later than " + &
					String(idt_parange_end_date))
//				return 1
			End If
		End IF
		If ldt_value < This.Getitemdate(1,"begin_date") Then
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("End Date must be greater than the Begin Date")				
//			Return 1
		End If
End Choose

//Return 0
end event

event constructor;call super::constructor;This.InsertRow(0)
ib_Updateable = True
end event

event losefocus;If This.TriggerEvent(ItemChanged!) = 1 Then Return 1 
//If This.AcceptText() = -1 Then Return 1
//This.PostEvent('ue_lose_focus')
end event

event itemchanged;String					ls_columnname
Boolean					lb_data
Date						ldt_value
u_string_functions	u_string

ls_columnname = This.GetColumnName()

Choose Case ls_columnname 
	Case "begin_date" 
		// I'm doing this because I trigger this from losefocus
		// If you have a better idea let me know
		If u_string.nf_amiempty(Data) Then
			ldt_value = This.GetItemDate(1,'begin_date')
		Else
			lb_data = True
			ldt_value = Date(data)
		End If
		If ldt_value < today() or IsNull(ldt_value) Then
			// I know I shouldn't do a setcolumn in itemchanged but
			// it seems to be working without a stack fault
			If Not lb_data Then
				This.SetColumn('begin_date')
			End If
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("Begin Date must be greater than or equal to today")   
			Return 1
		End If
		If ldt_value > This.Getitemdate(1,"end_date") Then
			// I know I shouldn't do a setcolumn in itemchanged but
			// it seems to be working without a stack fault
			IF Not lb_data Then
				This.SetColumn('begin_date')
			End If
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("Begin Date must be less than End Date")   
			Return 1
		End If
	Case "end_date" 
		// I'm doing this because i trigger this from losefocus
		// If you have a better idea let me know
		If u_string.nf_amiempty(Data) Then
			ldt_value = This.GetItemDate(1,'end_date')
		Else
			lb_data = True
			ldt_value = Date(data)
		End If			
		IF Upper(is_date_type) = 'PARANGE' Then
			If ldt_value > idt_parange_end_date or IsNull(ldt_value) Then 
				// I know I shouldn't do a setcolumn in itemchanged but
				// it seems to be working without a stack fault
				IF Not lb_data Then
					This.SetColumn('end_date')
				End If
				This.Setfocus()
				This.SelectText(1,100)
				iw_frame.SetMicroHelp("End date cannot be later than " + &
					String(idt_parange_end_date))
				return 1
			End If
		End IF
		If ldt_value < This.Getitemdate(1,"begin_date") Then
			// I know I shouldn't do a setcolumn in itemchanged but
			// it seems to be working without a stack fault
			If Not lb_data Then
				This.SetColumn('end_date')
			End If
			This.Setfocus()
			This.SelectText(1,100)
			iw_frame.SetMicroHelp("End Date must be greater than the Begin Date")				
			Return 1
		End If
End Choose

Return 0
end event

type dw_user from u_last_userid within w_standard_satellite_sort
integer x = 928
integer y = 84
integer taborder = 0
boolean bringtotop = true
end type

type gb_1 from groupbox within w_standard_satellite_sort
integer x = 2144
integer width = 690
integer height = 248
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "Date Range For Updates "
end type

