HA$PBExportHeader$w_sched_est_satellite_sort.srw
$PBExportComments$ibdkdld
forward
global type w_sched_est_satellite_sort from w_base_sheet_ext
end type
type dw_satellite_cattle from u_base_dw_ext within w_sched_est_satellite_sort
end type
type dw_plant from u_plant within w_sched_est_satellite_sort
end type
type dw_effective_date from u_effective_date within w_sched_est_satellite_sort
end type
type dw_last_updated from u_last_updated within w_sched_est_satellite_sort
end type
type dw_user from u_last_userid within w_sched_est_satellite_sort
end type
end forward

global type w_sched_est_satellite_sort from w_base_sheet_ext
integer x = 5
integer y = 4
integer width = 2871
integer height = 1516
long backcolor = 67108864
dw_satellite_cattle dw_satellite_cattle
dw_plant dw_plant
dw_effective_date dw_effective_date
dw_last_updated dw_last_updated
dw_user dw_user
end type
global w_sched_est_satellite_sort w_sched_est_satellite_sort

type variables
u_rmt001		iu_rmt001

s_error		istr_error_info

String		is_update_string, &
		is_update_string1

Long		il_rec_count

Window		iw_parentwindow
end variables

forward prototypes
public function boolean wf_retrieve ()
public function boolean wf_update ()
public subroutine wf_filenew ()
public subroutine wf_delete ()
public function boolean wf_update_modify (long al_row, string as_header, string as_column_name, string as_buffer, boolean ab_original_value)
public function integer wf_modified ()
end prototypes

public function boolean wf_retrieve ();Long						ll_rec_count, &
							ll_row, &
							ll_count, &
							ll_column = 1
							
String					ls_plant, &
							ls_column, & 
							ls_sort_totals, &
							ls_header, &
							ls_column_headings, &
							ls_detail, &
							ls_date, &
							ls_inquire_date, &
							ls_TitleModify, &
							ls_temp, &
							ls_temp1, &
							ls_time, &
							ls_describe, &
							ls_est_production
u_string_functions 	lu_string	
Date						ld_date
DateTime 				ldt_datetime
	
If dw_plant.AcceptText() = -1 Then Return False
If dw_effective_date.AcceptText() = -1 Then Return False
				
This.TriggerEvent('closequery') 

If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If


SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 
// clear out prev values reset the datawindow
ls_describe = "col_" + String(ll_column) + ".Visible"
Do While dw_satellite_cattle.Describe(ls_describe) > '0'
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 0 " + &
		" col_" + String(ll_column) + ".Visible = 0 " + &
		" co_col_" + String(ll_column) + ".Visible = 0 " + &
		" co_head_total_" + String(ll_column) + ".Visible = 0 " 
	ll_column ++
	ls_describe = "col_" + String(ll_column) + ".Visible"
Loop
dw_satellite_cattle.Modify ( ls_TitleModify )

ll_column = 1	
ls_TitleModify = ''

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_message = Space(71)
istr_error_info.se_procedure_name = "nf_rmtr06mr_inq_manifest_detail"

ls_inquire_date = String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd') 

ls_header  = dw_plant.uf_get_plant_code() + '~t'
ls_header += ls_inquire_date + '~r~n' 

//MessageBox("header",ls_header)

dw_satellite_cattle.reset()

//If Not iu_rmt001.uf_rmtr12mr_inq_sch_est_sat_sort(istr_error_info, & 
//										ls_detail, &
//										ls_column_headings, &
//										ls_header, &
//										ls_est_production) Then 
//										This.SetRedraw(True) 
//										Return False
//End If			
 
//MessageBox("Return header",ls_header)
//MessageBox("Column headings",ls_column_headings)
//MessageBox("Detail",ls_detail)
//MessageBox('Estimated Production',ls_est_production)

ls_date = lu_string.nf_gettoken(ls_header,"~t")
ls_Time = lu_string.nf_gettoken(ls_header,"~t")
dw_user.uf_set_user(lu_string.nf_gettoken(ls_header,"~t"))

ldt_datetime = DateTime(date(ls_date),Time(ls_time))
dw_last_updated.uf_set_last_updated(ldt_datetime)
 
//ls_column_headings = 'NOROLL~tNO ROLL~tDAVE 3~tDAVE 4~tDAVE 5~tDAVE 6~tDAVE 7~tDAVE 8~tDAVE 9~tDAVE 10~tDAVE 11~tDAVE 12~t'
//ls_est_production = '0~t550.50~t80.00~t98.00~t50.99~t60.01~t88.50~t600.00~t1.009~t0.00~t2.00~t3.00~t'
DO WHILE Not lu_string.nf_amiempty(ls_column_headings) or ll_column > 50
	ls_temp = lu_string.nf_gettoken(ls_column_headings,"~t")
	ls_temp1 = lu_string.nf_gettoken(ls_est_production,"~t")
	ls_TitleModify += 'col_' + String(ll_column) + "_t.text = '" + /* right trim to remove spaces*/RightTrim(ls_temp) + &
		"' col_" + String(ll_column) + "_t.Visible = 1 " + &
		" col_" + String(ll_column) + ".Visible = 1 " + &
		" co_col_" + String(ll_column) + ".Visible = 1 " + &
		" co_head_total_" + String(ll_column) + ".Visible = 1 " + &
		" co_head_total_" + String(ll_column) + ".Expression = '" + ls_temp1 + "' "
		ll_column ++
LOOP
//messageBox('my modify', ls_TitleModify)
dw_satellite_cattle.Modify ( ls_TitleModify )


//ls_detail = &
//'23~tN~t90~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'24~tY~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'25~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'26~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'27~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'283~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'293~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'231~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'232~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'233~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'234~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'235~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'236~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'237~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'238~tN~t0~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'239~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'123~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'223~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'323~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'423~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'523~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'623~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'723~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' + &
//'823~tN~t1~t2~t3~t4~t5~t6~t1~t2~t3~t4~t5~t6~r~n' 
////MessageBox("ls_detail",ls_detail)
//
ll_rec_count = dw_satellite_cattle.ImportString(ls_detail)

If ll_rec_count > 0 Then
	SetMicroHelp(String(ll_rec_count) + &
														" Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if
// Must do because of the reinquire
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '710'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '710'

This.SetRedraw(True) 

dw_satellite_cattle.ResetUpdate()
dw_satellite_cattle.SetFocus()
 
Return True

end function

public function boolean wf_update ();long				ll_Row, &
					ll_modrows, &
					ll_moddate, &
					ll_delrows, 	ll_count,ll_count1,ll_count2,ll_column_num

Char				lc_status_ind					

string			ls_plant, &
					ls_prod, &
					ls_header, &
					ls_describe,ls_column_name
dwItemStatus	ldwis_status			
u_string_functions u_string



IF dw_satellite_cattle.AcceptText() = -1 Then
	Return False
End If

ll_modrows = dw_satellite_cattle.ModifiedCount()
//ll_delrows = dw_satellite_cattle.DeletedCount()

IF ll_modrows <= 0 /*and ll_delrows <= 0 */ Then 
	iw_frame.SetMicroHelp('No Update Necessary')
	Return False
End if

ls_header = ''
ls_header += dw_plant.uf_get_plant_code() + '~t'
ls_header += String(dw_effective_date.uf_get_effective_date(), 'yyyy-mm-dd')	+ '~t'

SetMicroHelp("Wait, Updating ...")
SetPointer(HourGlass!)

This.SetRedraw(False)

is_update_string = ""
ll_row = 0
dw_satellite_cattle.SelectRow(0,False)

//For ll_count = 1 To ll_delrows 
//	lc_status_ind = 'D'
//	If Not This.wf_update_modify(ll_row, ls_header,ls_column_name,lc_status_ind,False)  Then 
//		This.SetRedraw(True)
//		Return False
//	End if
//Next

For ll_count = 1 To ll_modrows
	ll_Row = dw_satellite_cattle.GetNextModified(ll_Row, Primary!) 
	ll_column_num = 1
	ls_describe = "col_" + String(ll_column_num) + ".Visible"
	// loop through all visable columns checking status 
	lc_status_ind = ' '
	Do While dw_satellite_cattle.Describe(ls_describe) > '0'
		ls_column_name = "col_" + String(ll_column_num)
		If dw_satellite_cattle.GetItemStatus(ll_row,ls_column_name, Primary!) = DataModified! Then
			wf_update_modify(ll_row, ls_header,ls_column_name,lc_status_ind,False)  
		End If
		ll_column_num ++
		ls_describe = "col_" + String(ll_column_num) + ".Visible"
	Loop
Next

//MessageBox('header',ls_header)
//MessageBox('Update String',is_update_string)

//IF Not iu_rmt001.uf_rmtr13mr_upd_sch_est_sat_sort(istr_error_info, ls_header, &
//												is_update_string) THEN 
//	This.SetRedraw(True)
//	Return False
//End if

SetMicroHelp("Modification Successful")
dw_satellite_cattle.ResetUpdate()
This.SetRedraw(True)

Return( True )
end function

public subroutine wf_filenew ();//wf_addrow()
end subroutine

public subroutine wf_delete ();//wf_deleteRow()
end subroutine

public function boolean wf_update_modify (long al_row, string as_header, string as_column_name, string as_buffer, boolean ab_original_value);String						ls_describe

DWBuffer						ldwb_buffer


CHOOSE CASE as_buffer
	CASE 'D'
		ldwb_buffer = Delete!
	CASE ELSE
		ldwb_buffer = Primary!
END CHOOSE

is_update_string += &
	dw_satellite_cattle.GetItemString(al_row,"dest",ldwb_buffer, ab_original_value) + "~t" 

ls_describe = as_column_name + "_t" + ".text"	

is_update_string += dw_satellite_cattle.Describe(ls_describe) + "~t"

is_update_string += &
	String(dw_satellite_cattle.GetItemNumber(al_row,as_column_name,ldwb_buffer, ab_original_value)) + "~r~n"	 


Return True
end function

public function integer wf_modified ();Return 0
end function

on w_sched_est_satellite_sort.create
int iCurrent
call super::create
this.dw_satellite_cattle=create dw_satellite_cattle
this.dw_plant=create dw_plant
this.dw_effective_date=create dw_effective_date
this.dw_last_updated=create dw_last_updated
this.dw_user=create dw_user
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_satellite_cattle
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_effective_date
this.Control[iCurrent+4]=this.dw_last_updated
this.Control[iCurrent+5]=this.dw_user
end on

on w_sched_est_satellite_sort.destroy
call super::destroy
destroy(this.dw_satellite_cattle)
destroy(this.dw_plant)
destroy(this.dw_effective_date)
destroy(this.dw_last_updated)
destroy(this.dw_user)
end on

event activate;call super::activate;iw_frame.im_menu.mf_disable('m_sort')
iw_frame.im_menu.mf_disable('m_filter')
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event deactivate;iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_delete')
iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_deleterow')

end event

event open;call super::open;String 					ls_temp
u_string_functions 	lu_string


ls_temp = Message.StringParm	

This.Title = 'Scheduling Estimated Satellite Sort'

dw_effective_date.uf_enable(False)

dw_plant.disable()

dw_effective_date.uf_set_text('Ship Date:')


end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant'
		message.StringParm = dw_plant.uf_get_plant_code()
	Case 'effective_date'
		message.StringParm = String(dw_effective_date.uf_get_effective_date(), 'YYYY-MM-dd')
	Case 'date_object_text'
		message.StringParm = 'Ship Date:' + '~t' + 'PA' + '~t'
End choose

end event

event ue_postopen;call super::ue_postopen;This.PostEvent('ue_query')
//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "satcat"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_carcass_tracking_inq'

iu_rmt001 = Create u_rmt001

wf_retrieve()


end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant'
		dw_plant.uf_set_plant_code(as_value)
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date( Date(as_value ))
End Choose


end event

event resize;call super::resize;//constant integer li_x		= 27
//constant integer li_y		= 288

integer li_x		= 7
integer li_y		= 190


if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If

  
//dw_satellite_cattle.width	= newwidth - (30 + li_x)
//dw_satellite_cattle.height	= newheight - (30 + li_y)

dw_satellite_cattle.width	= newwidth - (li_x)
dw_satellite_cattle.height	= newheight - (li_y)

// Must do because of the split horizonal bar 
dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '0'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '0'

dw_satellite_cattle.Object.DataWindow.HorizontalScrollSplit = '710'
dw_satellite_cattle.Object.DataWindow.HorizontalScrollPosition2 =  '710'


end event

type dw_satellite_cattle from u_base_dw_ext within w_sched_est_satellite_sort
event ue_post_itemchanged ( long al_row,  dwobject adwo_dwo )
integer y = 196
integer width = 2793
integer height = 1132
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_sched_est_satellite_sort"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event itemchanged;call super::itemchanged;Long 		ll_row_count,ll_dump
String	ls_column,ls_column_name,ls_temp
Decimal	ld_column_total,ld_100_percent= 100.00,ld_compute


// I did this to make the code more generic
ls_column_name = dwo.name
ls_column = left(dwo.name,4)

CHOOSE CASE ls_column
	CASE 'col_'
		If Not IsNumber(data) Then
			iw_frame.SetMicroHelp("All Columns must be numeric")
			This.SelectText(1, 100)
			Return 1
		End If
		IF Sign(Integer(data)) = -1 Then
			iw_frame.SetMicroHelp("The Number must be Positive")
			This.SelectText(1, 100)
			Return 1
		End If		
END CHOOSE




end event

event constructor;call super::constructor; iw_parentwindow = Parent
end event

event itemerror;call super::itemerror;Return 2
end event

type dw_plant from u_plant within w_sched_est_satellite_sort
integer width = 1449
integer taborder = 0
boolean bringtotop = true
end type

event ue_postconstructor;call super::ue_postconstructor;This.Disable()

end event

type dw_effective_date from u_effective_date within w_sched_est_satellite_sort
integer x = 1422
integer y = 4
integer width = 722
integer taborder = 0
boolean bringtotop = true
end type

event constructor;call super::constructor;uf_set_effective_date(Today())
uf_enable(False)
end event

type dw_last_updated from u_last_updated within w_sched_est_satellite_sort
integer y = 88
integer taborder = 0
boolean bringtotop = true
end type

type dw_user from u_last_userid within w_sched_est_satellite_sort
integer x = 928
integer y = 84
integer taborder = 0
boolean bringtotop = true
end type

