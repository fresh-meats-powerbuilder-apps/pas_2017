HA$PBExportHeader$w_selection_percent_inq.srw
$PBExportComments$ibdkdld
forward
global type w_selection_percent_inq from w_base_response_ext
end type
type dw_effective_date from u_effective_date within w_selection_percent_inq
end type
type dw_plant from u_plant within w_selection_percent_inq
end type
type dw_shift from u_shift within w_selection_percent_inq
end type
type dw_select_time from u_selection within w_selection_percent_inq
end type
end forward

global type w_selection_percent_inq from w_base_response_ext
integer width = 1719
integer height = 524
long backcolor = 67108864
dw_effective_date dw_effective_date
dw_plant dw_plant
dw_shift dw_shift
dw_select_time dw_select_time
end type
global w_selection_percent_inq w_selection_percent_inq

type variables
String		is_select_time
end variables

forward prototypes
public subroutine wf_determine_selected_time (string as_selected_time)
end prototypes

public subroutine wf_determine_selected_time (string as_selected_time);CHOOSE CASE as_selected_time
	CASE 'd'
		dw_effective_date.Hide()
	CASE 's'
		dw_effective_date.Show()
END CHOOSE

end subroutine

on w_selection_percent_inq.create
int iCurrent
call super::create
this.dw_effective_date=create dw_effective_date
this.dw_plant=create dw_plant
this.dw_shift=create dw_shift
this.dw_select_time=create dw_select_time
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_effective_date
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_shift
this.Control[iCurrent+4]=this.dw_select_time
end on

on w_selection_percent_inq.destroy
call super::destroy
destroy(this.dw_effective_date)
destroy(this.dw_plant)
destroy(this.dw_shift)
destroy(this.dw_select_time)
end on

event ue_base_ok;String		ls_plant, &
 				ls_plant_desc, &
				ls_shift,&
				ls_date
u_string_functions		lu_strings
				
				
//	I have disabled the ancestor		

If dw_plant.AcceptText() = -1 &
	or dw_effective_date.AcceptText() = -1 &
	or dw_select_time.AcceptText() = -1 &
	or dw_shift.AcceptText() = -1 Then return
	
ls_plant = dw_plant.uf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End if

ls_shift = dw_shift.uf_get_shift()
If lu_strings.nf_IsEmpty(ls_shift) Then
	iw_frame.SetMicroHelp("Shift is a required field")
	dw_shift.SetFocus()
	Return
End if

If is_select_time = 'S' Then
	ls_date = string(dw_effective_date.uf_get_effective_date())
	If lu_strings.nf_IsEmpty(ls_date) Then
		iw_frame.SetMicroHelp("Production Date is a required field")
		dw_effective_date.SetFocus()
		Return
	End If
	If dw_effective_date.uf_get_effective_date() < Today() Then
		iw_frame.SetMicroHelp("Production Date must greater then or equal to today")
		dw_effective_date.SetFocus()
		Return
	End If
	iw_parentwindow.Event ue_set_data('effective_date',ls_date)
End If

iw_parentwindow.Event ue_set_data('plant',ls_plant)
iw_parentwindow.Event ue_set_data('shift',ls_shift)
iw_parentwindow.Event ue_set_data('time',is_select_time)


ib_ok_to_close = True

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)

iw_parentwindow.Event ue_get_data('shift')
dw_shift.uf_set_shift(Message.StringParm)

iw_parentwindow.Event ue_get_data('time')
dw_select_time.uf_set_select_time(Message.StringParm)
is_select_time = Message.StringParm 

iw_parentwindow.Event ue_get_data('effective_date')
dw_effective_date.uf_set_effective_date(date(Message.StringParm))

If dw_select_time.uf_get_select_time() = 'S' Then
	dw_effective_date.Show()
Else
	dw_effective_date.Hide()
End IF


dw_shift.SetFocus()




end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_selection_percent_inq
integer x = 1390
integer y = 300
integer taborder = 60
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_selection_percent_inq
integer x = 1102
integer y = 300
integer taborder = 50
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_selection_percent_inq
integer x = 818
integer y = 300
integer taborder = 40
end type

type dw_effective_date from u_effective_date within w_selection_percent_inq
integer x = 722
integer y = 164
integer height = 84
integer taborder = 30
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_enable(True)
This.uf_set_text('Production Date:')
This.Hide()

end event

type dw_plant from u_plant within w_selection_percent_inq
integer x = 279
integer y = 12
integer taborder = 70
boolean bringtotop = true
end type

type dw_shift from u_shift within w_selection_percent_inq
integer x = 293
integer y = 100
integer width = 343
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_enable(True)
end event

type dw_select_time from u_selection within w_selection_percent_inq
integer y = 164
integer taborder = 20
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;CHOOSE CASE data
	CASE 'S'
	   is_select_time = 'S'
		dw_effective_date.Show()
		dw_effective_date.SetFocus()
	Case 'D'
		is_select_time = 'D'
		dw_effective_date.Hide()
END CHOOSE

end event

