HA$PBExportHeader$w_cattle_characteristics_response.srw
$PBExportComments$IBDKDLD
forward
global type w_cattle_characteristics_response from w_base_response_ext
end type
type dw_response from u_base_dw_ext within w_cattle_characteristics_response
end type
end forward

global type w_cattle_characteristics_response from w_base_response_ext
integer x = 1504
integer y = 412
integer width = 1102
integer height = 624
boolean titlebar = false
boolean controlmenu = false
long backcolor = 67108864
event ue_revisions ( )
dw_response dw_response
end type
global w_cattle_characteristics_response w_cattle_characteristics_response

type variables
w_base_sheet	iw_parent
end variables

event ue_revisions;/*****************************************************************
**   REVISION NUMBER: Rev#01
**   PROJECT NUMBER:  Support
**   DATE:				 January 2000            
**   PROGRAMMER:      David Deal
**   PURPOSE:         Added Program, Removed Sex & Destination
**                    
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

on w_cattle_characteristics_response.create
int iCurrent
call super::create
this.dw_response=create dw_response
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_response
end on

on w_cattle_characteristics_response.destroy
call super::destroy
destroy(this.dw_response)
end on

event open;call super::open;iw_parent = Message.PowerObjectParm	
//This.Title = "Description Data"
//This.Title = iw_Parent.Title + " Ellipsis	"
iw_frame.SetMicroHelp("Ready")
end event

event ue_postopen;call super::ue_postopen;Long ll_temp

Window   lw_parent

lw_parent = this.parentwindow()
This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)

iw_parent.Event ue_get_data('ellipsis')
dw_response.ImportString(Message.StringParm)
ll_temp = dw_response.SetFocus()
//messagebox('setfocus', string(ll_temp))
//dw_response.ScrollToRow ( 1 )
dw_response.SelectRow ( 1, True )

end event

event ue_base_ok;call super::ue_base_ok;Long 			ll_row
String		ls_temp


If IsValid ( iw_parent ) Then
	ll_row = dw_response.GetRow()
		ls_temp = 	dw_response.GetItemString  ( ll_row, 'desc_child') + '~t' 
		ls_temp += 	dw_response.GetItemString  ( ll_row, 'desc') + '~t' 
		ls_temp +=	dw_response.GetItemString  ( ll_row, 'grade') + '~t' 
		ls_temp +=	String(dw_response.GetItemDecimal ( ll_row, 'min_wght')) + '~t' 
		ls_temp +=	String(dw_response.GetItemDecimal ( ll_row, 'max_wght')) + '~t' 
		ls_temp +=	String(dw_response.GetItemDecimal ( ll_row, 'min_grade')) + '~t'
		ls_temp +=	String(dw_response.GetItemDecimal ( ll_row, 'max_grade')) + '~t'
//Rev#01		
//		ls_temp +=	dw_response.GetItemString  ( ll_row, 'destination') + '~t' 
//		ls_temp +=	dw_response.GetItemString  ( ll_row, 'sex') + '~t'  
		ls_temp +=	dw_response.GetItemString  ( ll_row, 'program') + '~t'
		ls_temp +=	dw_response.GetItemString  ( ll_row, 'cool_descr') + '~t'
		If ll_row > 0 Then
			iw_parent.Event ue_set_data('selectrow',ls_temp)
		Else
			iw_frame.SetMicroHelp("You must select a row or hit Cancel")
		End If
End If

Close(This)
	


end event

event ue_base_cancel;call super::ue_base_cancel;If IsValid(iw_parent) Then
	iw_parent.Event ue_set_data('closechild','')
End If
Close(This)
end event

event doubleclicked;//w_cattle_characteristics_response.TriggerEvent ( 'ue_base_ok')
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_cattle_characteristics_response
boolean visible = false
integer x = 809
integer y = 496
integer taborder = 40
boolean enabled = false
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_cattle_characteristics_response
integer x = 805
integer y = 492
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_cattle_characteristics_response
integer x = 521
integer y = 492
integer taborder = 20
end type

type dw_response from u_base_dw_ext within w_cattle_characteristics_response
integer width = 1065
integer height = 484
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_cattle_characteristics_child"
boolean vscrollbar = true
boolean border = false
end type

event constructor;call super::constructor;is_selection = '1'
end event

