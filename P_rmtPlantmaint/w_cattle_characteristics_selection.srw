HA$PBExportHeader$w_cattle_characteristics_selection.srw
forward
global type w_cattle_characteristics_selection from w_base_child_ext
end type
type dw_dddw from u_base_dw_ext within w_cattle_characteristics_selection
end type
type cb_cancel from commandbutton within w_cattle_characteristics_selection
end type
type cb_ok from commandbutton within w_cattle_characteristics_selection
end type
end forward

global type w_cattle_characteristics_selection from w_base_child_ext
integer width = 1371
integer height = 1468
string title = ""
boolean controlmenu = false
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
event ue_base_cancel ( )
event ue_base_ok ( )
dw_dddw dw_dddw
cb_cancel cb_cancel
cb_ok cb_ok
end type
global w_cattle_characteristics_selection w_cattle_characteristics_selection

type variables
w_base_sheet		iw_parentwindow

Boolean				ib_ok_to_close, &
						ib_modified
						
Long					il_char_clicked_row

DataStore			ids_program_type, &
						ids_chr
String				is_char_type
end variables

forward prototypes
public function integer setmicrohelp (string t)
end prototypes

event ue_base_cancel();Close(This)
end event

event ue_base_ok();String		ls_selected, &
 				ls_char_desc, &
				ls_date
				
Int			li_pa_range				

Long			ll_row


Choose Case is_char_type
	Case 'PGM'
		ls_char_desc = 'program'
	Case 'GRD'
		ls_char_desc = 'grade'
	Case 'COOL'
		ls_char_desc = 'code'
End Choose

ls_selected = is_char_type +'~t'

This.SetRedraw(False)

dw_dddw.SetFilter("")
dw_dddw.Filter()
dw_dddw.SetFilter("IsSelected()")
dw_dddw.Filter()

if dw_dddw.RowCount() = 0 then
	MessageBox("Selection", "Please select at least one item.")
	dw_dddw.SetFilter("")
	dw_dddw.Filter()
	
	This.SetRedraw(True)
	return 
End if
This.SetRedraw(True)

For ll_Row = 1 to dw_dddw.RowCount()
	ls_selected += dw_dddw.GetItemString(ll_row, ls_char_desc) + '~t'
Next

dw_dddw.SetFilter("")
dw_dddw.Filter()

iw_parentwindow.Event ue_set_data('Selected',ls_selected)

ib_ok_to_close = True

Close(This)

end event

public function integer setmicrohelp (string t);return gw_netwise_frame.SetMicroHelp(t)

end function

on w_cattle_characteristics_selection.create
int iCurrent
call super::create
this.dw_dddw=create dw_dddw
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_dddw
this.Control[iCurrent+2]=this.cb_cancel
this.Control[iCurrent+3]=this.cb_ok
end on

on w_cattle_characteristics_selection.destroy
call super::destroy
destroy(this.dw_dddw)
destroy(this.cb_cancel)
destroy(this.cb_ok)
end on

event open;call super::open;String			ls_title, &
					ls_char


iw_ParentWindow = Message.PowerObjectParm

iw_parentwindow.Event ue_get_data('Type')
is_char_type = Message.StringParm

Choose Case is_char_type
	Case 'PGM'
		dw_dddw.dataobject = 'd_rmt_program_dddw'
		ls_title = "Program Type"
	Case 'GRD'
		dw_dddw.dataobject = 'd_rmt_grade_dddw'
		ls_title = "Grade "
	Case 'COOL'
		dw_dddw.dataobject = 'd_rmt_cool_code_dddw'
		ls_title = "Cool Code"
End Choose

This.Move(iw_parentwindow.X + (iw_parentwindow.Width / 2) - (This.Width / 2), &
			 iw_parentwindow.Y + WorkSpaceY() + (iw_parentwindow.Height / 4 ) - (This.Height / 4))

iw_parentwindow.Event ue_get_data('Characteristic')
ls_char = Message.StringParm

This.Title = ls_title + " for " + ls_char + " Selection"
iw_frame.SetMicroHelp("Ready")

end event

event ue_postopen;call super::ue_postopen;u_string_functions	lu_string_functions

String 					ls_find_str, &
							ls_char_desc
Long						ll_find

ib_ok_to_close = False

Window   lw_parent

lw_parent = this.parentwindow()
This.X = lw_parent.X + (lw_parent.width / 2) - (this.width / 2)
This.Y = lw_parent.Y + (lw_parent.Height / 2) - (This.Height / 2)


Choose Case is_char_type
	Case 'PGM'
		ls_char_desc = "program"
	Case 'GRD'
		ls_char_desc = "grade"
	Case 'COOL'
		ls_char_desc = "code"
End Choose		
iw_parentwindow.Event ue_get_data(is_char_type)
dw_dddw.ImportString(Message.StringParm)
dw_dddw.SetSort("#1 A")
dw_dddw.Sort()

iw_parentwindow.Event ue_get_data('Char')

Do While Len(Trim(Message.StringParm)) > 0

	ls_find_str = ls_char_desc + " = '" + lu_string_functions.nf_gettoken(Message.StringParm, '~t') + "'"
			
	ll_find = dw_dddw.Find ( ls_find_str, 1, dw_dddw.RowCount() )
	
	if ll_find > 0 then
		dw_dddw.SelectRow(ll_find, true)
	end if
Loop

dw_dddw.ResetUpdate()

end event

event resize;call super::resize;////constant integer li_x		= 1376
//constant integer li_y		= 340
//
//dw_dddw.width = 1330
//
//dw_dddw.height	= newheight - li_y - 100
//
//
end event

type dw_dddw from u_base_dw_ext within w_cattle_characteristics_selection
integer x = 32
integer y = 28
integer width = 1317
integer height = 1092
integer taborder = 10
string dataobject = "d_rmt_program_dddw"
boolean vscrollbar = true
boolean livescroll = true
end type

event clicked;call super::clicked;Int	li_idx

if row = 0 then return

ib_modified = True

If KeyDown(KeyShift!) Then
	SetRedraw(FALSE)
	If il_char_clicked_row > row Then
		For li_idx = il_char_clicked_row To row STEP -1
			SelectRow(li_idx, TRUE)	
		End For
	Else
		For li_idx = il_char_clicked_row To row
			SelectRow(li_idx, TRUE)	
		End For
	End If
	SetRedraw(TRUE)
	Return
Else
	If IsSelected(row) then
		This.SelectRow(row, False)
	Else
		This.SelectRow(row, True)
	End if
End If

il_char_clicked_row = row	
end event

type cb_cancel from commandbutton within w_cattle_characteristics_selection
integer x = 965
integer y = 1184
integer width = 343
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Cancel"
boolean cancel = true
end type

event clicked;Parent.TriggerEvent ("ue_base_cancel")
end event

type cb_ok from commandbutton within w_cattle_characteristics_selection
integer x = 594
integer y = 1184
integer width = 343
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "OK"
boolean default = true
end type

event clicked;Parent.TriggerEvent ("ue_base_ok")
end event

