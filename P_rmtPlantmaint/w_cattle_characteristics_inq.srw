HA$PBExportHeader$w_cattle_characteristics_inq.srw
$PBExportComments$IBDKDLD
forward
global type w_cattle_characteristics_inq from w_base_response_ext
end type
type dw_plant from u_plant within w_cattle_characteristics_inq
end type
end forward

global type w_cattle_characteristics_inq from w_base_response_ext
integer width = 1518
integer height = 348
long backcolor = 67108864
dw_plant dw_plant
end type
global w_cattle_characteristics_inq w_cattle_characteristics_inq

on w_cattle_characteristics_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
end on

on w_cattle_characteristics_inq.destroy
call super::destroy
destroy(this.dw_plant)
end on

event ue_base_ok;call super::ue_base_ok;String		ls_plant, &
 				ls_plant_desc, &
				ls_date
				
Int			li_pa_range				

u_string_functions		lu_strings


If dw_plant.AcceptText() = -1 Then return 
	
ls_plant = dw_plant.nf_get_plant_code()
If lu_strings.nf_IsEmpty(ls_plant) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	Return
End if

iw_parentwindow.Event ue_set_data('plant',ls_plant)

ib_ok_to_close = True

Close(This)
            

end event

event ue_base_cancel;call super::ue_base_cancel;Close(This)
end event

event ue_postopen;call super::ue_postopen;iw_parentwindow.Event ue_get_data('plant')
dw_plant.uf_set_plant_code(Message.StringParm)






end event

event open;call super::open;iw_ParentWindow = Message.PowerObjectParm

This.Title = iw_ParentWindow.Title + " Inquire"
iw_frame.SetMicroHelp("Ready")
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_cattle_characteristics_inq
integer x = 1147
integer y = 132
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_cattle_characteristics_inq
integer x = 859
integer y = 132
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_cattle_characteristics_inq
integer x = 571
integer y = 132
integer taborder = 20
end type

type dw_plant from u_plant within w_cattle_characteristics_inq
integer x = 5
integer y = 12
integer taborder = 10
boolean bringtotop = true
end type

