HA$PBExportHeader$u_selection.sru
$PBExportComments$ibdkdld
forward
global type u_selection from datawindow
end type
end forward

global type u_selection from datawindow
int Width=695
int Height=260
int TabOrder=10
string DataObject="d_selection"
boolean Border=false
boolean LiveScroll=true
end type
global u_selection u_selection

type variables
Boolean			ib_updatable
end variables

forward prototypes
public subroutine uf_set_select_time (string as_select_time)
public function string uf_get_select_time ()
public function integer uf_modified ()
public function integer uf_enable (boolean ab_enable)
end prototypes

public subroutine uf_set_select_time (string as_select_time);	This.SetItem(1,"selection",as_select_time)
  
end subroutine

public function string uf_get_select_time ();Return This.GetItemString(1, "selection")
end function

public function integer uf_modified ();Return 0
end function

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.selection.Protect = 0
Else
	This.object.selection.Protect = 1
	ib_updatable = False
End If

Return 1
end function

event constructor;This.InsertRow(0)
 

end event

