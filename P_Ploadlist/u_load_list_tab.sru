HA$PBExportHeader$u_load_list_tab.sru
forward
global type u_load_list_tab from tab
end type
type tabpage_1 from userobject within u_load_list_tab
end type
type dw_maintenance from u_base_dw_ext within tabpage_1
end type
type tabpage_1 from userobject within u_load_list_tab
dw_maintenance dw_maintenance
end type
type tabpage_2 from userobject within u_load_list_tab
end type
type dw_exceptions from u_base_dw_ext within tabpage_2
end type
type tabpage_2 from userobject within u_load_list_tab
dw_exceptions dw_exceptions
end type
type tabpage_3 from userobject within u_load_list_tab
end type
type dw_loads_not_transmitted from u_base_dw_ext within tabpage_3
end type
type tabpage_3 from userobject within u_load_list_tab
dw_loads_not_transmitted dw_loads_not_transmitted
end type
end forward

global type u_load_list_tab from tab
string tag = "Maintenance"
integer width = 3063
integer height = 1288
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 12632256
boolean raggedright = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type
global u_load_list_tab u_load_list_tab

forward prototypes
public subroutine wf_filldddw (datawindowchild ldw_childdw)
end prototypes

public subroutine wf_filldddw (datawindowchild ldw_childdw);IF ldw_childdw.RowCount()	> 0 THEN RETURN
ldw_childdw.SetTransObject(SQLCA)
ldw_childdw.Retrieve()
end subroutine

on u_load_list_tab.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3}
end on

on u_load_list_tab.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
end on

event rightclicked;m_load_list_maint_popup	lm_popup

lm_popup = Create m_load_list_maint_popup

lm_popup.m_loadlistmaintenance.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup

end event

type tabpage_1 from userobject within u_load_list_tab
integer x = 18
integer y = 100
integer width = 3026
integer height = 1172
long backcolor = 79741120
string text = "Maintenance"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 553648127
dw_maintenance dw_maintenance
end type

on tabpage_1.create
this.dw_maintenance=create dw_maintenance
this.Control[]={this.dw_maintenance}
end on

on tabpage_1.destroy
destroy(this.dw_maintenance)
end on

type dw_maintenance from u_base_dw_ext within tabpage_1
integer x = 9
integer y = 12
integer width = 2999
integer height = 1160
integer taborder = 20
string dataobject = "d_auto_load_list_maint_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event doubleclicked;call super::doubleclicked;String		ls_primary, &
				ls_ColumnName

// You must get clicked column because primary order is protected it will 
// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name
 
if row > 0 then
	if ls_ColumnName = "primary_order" then
		ls_primary = This.GetItemString(row, "primary_order")
		if ls_primary > '     ' then
				OpenWithParm(w_load_combination_inq, ls_primary)
		end if
	end if
end if

end event

event rbuttondown;call super::rbuttondown;m_load_list_maint_popup	lm_popup

lm_popup = Create m_load_list_maint_popup

lm_popup.m_loadlistmaintenance.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup


end event

event itemchanged;call super::itemchanged;String 	ls_time, &
			ls_temp

If This.GetItemString(row, 'update_flag') = ' ' Then 
	This.SetItem( row, 'update_flag', 'U')
end if

If dwo.Name = "load_time" then
	If row = This.RowCount() then
		This.InsertRow(0)
	end if
end if
	
end event

event ue_insertrow;call super::ue_insertrow;This.SetItem(This.RowCount(), "business_rules", 'UUVUUVVVVVVV')
end event

event constructor;call super::constructor;//datawindowchild 	ldwc_Temp
//integer 				rtncode
//
//rtncode = dw_maintenance.getchild('load_instr', ldwc_temp)
//If ldwc_temp.RowCount() <= 1 Then
//	wf_filldddw(ldwc_temp)
//end if
end event

type tabpage_2 from userobject within u_load_list_tab
integer x = 18
integer y = 100
integer width = 3026
integer height = 1172
long backcolor = 12632256
string text = "Exceptions"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_exceptions dw_exceptions
end type

on tabpage_2.create
this.dw_exceptions=create dw_exceptions
this.Control[]={this.dw_exceptions}
end on

on tabpage_2.destroy
destroy(this.dw_exceptions)
end on

type dw_exceptions from u_base_dw_ext within tabpage_2
integer x = 5
integer width = 3013
integer height = 1164
integer taborder = 11
string dataobject = "d_auto_load_list_except_detail"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event rbuttondown;call super::rbuttondown;m_load_list_maint_popup	lm_popup

lm_popup = Create m_load_list_maint_popup

lm_popup.m_loadlistmaintenance.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

Destroy lm_popup

end event

event itemchanged;call super::itemchanged;String 	ls_time, &
			ls_temp

If This.GetItemString(row, 'update_flag') = ' ' Then 
	This.SetItem( row, 'update_flag', 'U')
end if


end event

event ue_insertrow;call super::ue_insertrow;This.SetItem(This.RowCount(), "business_rules", 'UVVVVVVVVVVV')
end event

event doubleclicked;call super::doubleclicked;String		ls_primary, &
				ls_ColumnName

// You must get clicked column because primary order is protected it will 
// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name
 
if row > 0 then
	if ls_ColumnName = "primary_order" then
		ls_primary = This.GetItemString(row, "primary_order")
		if ls_primary > '     ' then
				OpenWithParm(w_load_combination_inq, ls_primary)
		end if
	end if
end if
end event

event constructor;call super::constructor;//datawindowchild 	ldwc_Temp
//integer 				rtncode
//
//rtncode = dw_exceptions.getchild('load_instr', ldwc_temp)
//If ldwc_temp.RowCount() <= 1 Then
//	wf_filldddw(ldwc_temp)
//end if
end event

type tabpage_3 from userobject within u_load_list_tab
integer x = 18
integer y = 100
integer width = 3026
integer height = 1172
long backcolor = 79741120
string text = "Loads Not Transmitted"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_loads_not_transmitted dw_loads_not_transmitted
end type

on tabpage_3.create
this.dw_loads_not_transmitted=create dw_loads_not_transmitted
this.Control[]={this.dw_loads_not_transmitted}
end on

on tabpage_3.destroy
destroy(this.dw_loads_not_transmitted)
end on

type dw_loads_not_transmitted from u_base_dw_ext within tabpage_3
integer x = 5
integer width = 3013
integer height = 1164
integer taborder = 11
string dataobject = "d_loads_not_transmitted"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;String		ls_load_number, &
				ls_ColumnName

// You must get clicked column because primary order is protected it will 
// never be the column returned by "GetColumnName()"
ls_ColumnName = dwo.name
 
if row > 0 then
	if ls_ColumnName = "load_number" then
		ls_load_number = This.GetItemString(row, "load_number")
		if ls_load_number > '     ' then
				OpenWithParm(w_load_combination_inq, ls_load_number)
		end if
	end if
end if

end event

