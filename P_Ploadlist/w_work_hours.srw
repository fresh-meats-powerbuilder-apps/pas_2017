HA$PBExportHeader$w_work_hours.srw
$PBExportComments$IBDKDLD
forward
global type w_work_hours from w_base_sheet_ext
end type
type dw_plant from u_plant within w_work_hours
end type
type dw_shift from u_shift within w_work_hours
end type
type dw_effective_date from u_effective_date within w_work_hours
end type
type dw_select_time from u_select_time within w_work_hours
end type
type tab_1 from tab within w_work_hours
end type
type tabpage_1 from userobject within tab_1
end type
type dw_work_hours_prod from u_base_dw_ext within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_work_hours_prod dw_work_hours_prod
end type
type tabpage_2 from userobject within tab_1
end type
type dw_work_hours_tran from u_base_dw_ext within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_work_hours_tran dw_work_hours_tran
end type
type tabpage_3 from userobject within tab_1
end type
type dw_work_hours_ship from u_base_dw_ext within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_work_hours_ship dw_work_hours_ship
end type
type tabpage_4 from userobject within tab_1
end type
type dw_work_hours_rec from u_base_dw_ext within tabpage_4
end type
type tabpage_4 from userobject within tab_1
dw_work_hours_rec dw_work_hours_rec
end type
type tabpage_5 from userobject within tab_1
end type
type dw_work_hours_pt from u_base_dw_ext within tabpage_5
end type
type tabpage_5 from userobject within tab_1
dw_work_hours_pt dw_work_hours_pt
end type
type tab_1 from tab within w_work_hours
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type
end forward

global type w_work_hours from w_base_sheet_ext
integer width = 2738
integer height = 1604
string title = "Work Hours"
long backcolor = 12632256
dw_plant dw_plant
dw_shift dw_shift
dw_effective_date dw_effective_date
dw_select_time dw_select_time
tab_1 tab_1
end type
global w_work_hours w_work_hours

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Datawindow	idw_active_dw
u_pas201		iu_pas201

s_error		istr_error_info

Long		il_color,&
		il_SelectedColor,&
		il_SelectedTextColor

Boolean		ib_reinquire, &
		ib_close_this

Integer		ii_max_page_number

Datastore		ids_report

w_netwise_response	iw_parentwindow

String		is_select_time,&
		is_input_string
u_ws_pas5		iu_ws_pas5

end variables

forward prototypes
public subroutine wf_error_handling (string as_column_name, string as_error_type, string as_error_message, string as_error_column)
public subroutine wf_process_time_fields (string as_right, boolean ab_error)
public subroutine wf_mod_time_fields (string as_right)
public subroutine wf_mod_dura_fields (string as_right)
public function integer wf_work_duration_check ()
public subroutine wf_mod_others (string as_right)
public function boolean wf_post_update (datawindow adw_active_dw)
public function boolean wf_update ()
public subroutine wf_build_string (long al_column, datawindow adw_active_dw)
public function boolean wf_retrieve ()
end prototypes

public subroutine wf_error_handling (string as_column_name, string as_error_type, string as_error_message, string as_error_column);String 	ls_modify, &
			ls_temp, &
			ls_left, &
			ls_time, &
			ls_shift
Long 		ll_num

CHOOSE CASE as_error_type
	CASE 	'noerror'
			ls_modify = as_column_name + ".Background.Color='0'" + &
							as_column_name + ".Background.Color='" + String(RGB(255,255,255)) + "'" 
			idw_active_dw.Modify(ls_modify)
	CASE 	'severe'
			idw_active_dw.SelectText ( 1, 10 )
			idw_active_dw.SetFocus()
	CASE 	'caution'
			ls_modify = as_column_name + ".Background.Color='0'" + &
							as_column_name + ".Background.Color='" + String(RGB(255,0,0)) + "'" 
			idw_active_dw.Modify(ls_modify)
END CHOOSE

ls_left = left(as_error_message, Len(as_error_message) - 9) 

If ls_left = 'shiftmin' Then
	ls_temp = Right(as_error_message,9)
	ls_time = Right(ls_temp,8)
	ls_shift = Left(ls_temp, Len(ls_temp) -8)   
	as_error_message = ls_left
End IF

If	left(as_column_name,5) = 'chain' Then
	ll_num = Long(Mid( as_column_name, 14 ,Len(as_column_name)))
End IF

CHOOSE CASE as_error_message
	CASE 'mintime'
		iw_frame.SetMicroHelp('Time should be less than ' + as_error_column) 
	Case 'shiftmin'
		iw_frame.SetMicroHelp('Start Time should be greater than ' + ls_time + ' for Shift ' + ls_shift)		
	Case 'invalidtime'
		iw_frame.SetMicroHelp('The time you entered is not a valid time ')
	Case 'ready'
		iw_frame.SetMicroHelp('Ready')
	Case 'bothtime'
		iw_frame.SetMicroHelp('Both of the time fields may be in error')
//// I am using this next case as a catch all for diferrent messages		
	Case 'duration'
		iw_frame.SetMicroHelp(as_error_column)
	Case 'durationgreat'
		iw_frame.SetMicroHelp('Work Duration must be greater than all other durations combined')
	Case 'durationless'
		iw_frame.SetMicroHelp("All duration's combined should be less than work duration")
	Case 'lagless'
		iw_frame.SetMicroHelp("Shipping lag should be less than work duration")
	Case 'chain'
		iw_frame.SetMicroHelp("Chain speed can't be greater than " + String(ll_num))
	Case 'chainneg'
		iw_frame.SetMicroHelp("Chain speed can't be a negitive number")
END CHOOSE


                 
end subroutine

public subroutine wf_process_time_fields (string as_right, boolean ab_error);u_string_functions		u_string
String	ls_temp
Long		ll_comp,ll_comp1,ll_comp2	
Time		lt_time,lt_temp,lt_temp1,lt_temp2,lt_cal_overflow_time,lt_break1,lt_meal,lt_max_meal,&
			lt_max_break1,lt_break2
Boolean  lb_next_day,lb_b1_current_day,lb_meal_current_day,lb_b2_current_day 


lt_time 		= Time(idw_active_dw.GetItemString(1, 'Start_time' + as_right))
ll_comp		= SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString(1, 'work_duration' + as_right))) 
ll_comp	  += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString(1, 'meal_duration' + as_right))) 
lt_cal_overflow_time = RelativeTime(lt_time,ll_comp)
If lt_cal_overflow_time = Time('23:59:59') Then
	ll_comp1 = SecondsAfter (lt_time,Time('23:59:59')) + 1
	ll_comp2 = ll_comp - ll_comp1
	lt_cal_overflow_time = RelativeTime ( Time('00:00:00'), ll_comp2 )
	lb_next_day = True
End If




////////////////break_1_start_time//////////		
If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString(1, 'break_1_start_time' + as_right)) Then
	ls_temp = idw_active_dw.GetItemString ( 1, 'break_1_start_time' + as_right)
	lt_break1 = Time(ls_temp)
	If Not ab_error Then
		If lb_next_day Then
			If lt_break1 > lt_time and lt_break1 < time('23:59:59') Then
				lb_b1_current_day = True
			End If
			If lt_break1 > lt_cal_overflow_time and not lb_b1_current_day Then
				wf_error_handling('break_1_start_time' + as_right,'caution','duration','The 1st Break Start Time has exceeded the shift end time of ' + String(lt_cal_overflow_time))//'The 1st Break Start Time has exceed the Start Time')
				ab_error = True
			End If
		Else
			If lt_break1 <= lt_time Then
				wf_error_handling('break_1_start_time' + as_right,'caution','duration',"The 1st Break Start Time can't be less then the start time of " + String(lt_time))//'The 1st Break Start Time has exceed the Start Time')
				ab_error = True
			End If
		End If
	End If
End If
				
////////////////meal_start_time//////////////////////////				
If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString(1, 'meal_start_time' + as_right)) Then
		ls_temp = idw_active_dw.GetItemString ( 1, 'meal_start_time' + as_right)
		lt_meal = Time(ls_temp)
		ll_comp = SecondsAfter (Time('00:00:00'),Time(idw_active_dw.GetItemString ( 1, 'break_1_duration' + as_right)))
		lt_max_break1 = RelativeTime ( lt_break1, ll_comp )
		If lt_max_break1 = Time('23:59:59') Then
			ll_comp1 = SecondsAfter (lt_break1,Time('23:59:59')) + 1
			ll_comp2 = ll_comp - ll_comp1
			lt_max_break1 = RelativeTime ( Time('00:00:00'), ll_comp2 )
		End If
		If lt_meal > lt_break1 and lt_meal < time('23:59:59') Then
			lb_meal_current_day = True
		End If
		If not ab_error Then
			If lb_next_day Then
				If lb_b1_current_day Then
					If lt_meal <= lt_max_break1 and lb_meal_current_day Then
						wf_error_handling('meal_start_time' + as_right,'caution','duration',"The Meal Start Time must be greater then " + String(lt_max_break1))
						ab_error = True
					Else
						If lt_meal > lt_cal_overflow_time and not lb_meal_current_day Then
							wf_error_handling('meal_start_time' + as_right,'caution','duration','The Meal Start Time has exceeded the shift end time of ' + String(lt_cal_overflow_time))
							ab_error = True
						End If
					End If
				Else
					If lt_meal <= lt_max_break1 and lb_meal_current_day Then
						wf_error_handling('meal_start_time' + as_right,'caution','duration',"The Meal Start Time must be greater then " + String(lt_max_break1))
						ab_error = True
					Else
						If lt_meal > lt_cal_overflow_time Then
							wf_error_handling('meal_start_time' + as_right,'caution','duration','The Meal Start Time has exceeded the shift end time of ' + String(lt_cal_overflow_time))
							ab_error = True
						End If
					End If
				End If
			Else
				If lt_meal <= lt_max_break1 Then
					wf_error_handling('meal_start_time' + as_right,'caution','duration',"The Meal Start Time must be greater then " + String(lt_max_break1))
					ab_error = True
				End If
				If lt_meal > lt_cal_overflow_time /*and not lb_next_day*/ Then
					wf_error_handling('meal_start_time' + as_right,'caution','duration',"The Meal Start Time has exceeded the shift end time of " + String(lt_cal_overflow_time))
					ab_error = True
				End If
 			End If 
		End If
End if		

//////////////////////break_2_start_time///////////////////////		
If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString(1, 'break_2_start_time' + as_right)) Then
		ls_temp = idw_active_dw.GetItemString ( 1, 'break_2_start_time' + as_right)
		lt_break2 = Time(ls_temp)
		ll_comp = SecondsAfter (Time('00:00:00'),Time(idw_active_dw.GetItemString ( 1, 'meal_duration' + as_right)))
		lt_max_meal = RelativeTime ( lt_meal, ll_comp )
		If lt_max_meal = Time('23:59:59') Then
			ll_comp1 = SecondsAfter (lt_meal,Time('23:59:59')) + 1
			ll_comp2 = ll_comp - ll_comp1
			lt_max_meal = RelativeTime ( Time('00:00:00'), ll_comp2 )
		End If
		If lt_break2 > lt_meal and lt_break2 < time('23:59:59') Then
			lb_b2_current_day = True
		End If
		If not ab_error Then
			If lb_next_day Then
				If lb_meal_current_day Then
					If lt_break2 <= lt_max_meal and lb_b2_current_day Then
						wf_error_handling('break_2_start_time' + as_right,'caution','duration',"The 2nd Break Start Time must be greater then " + String(lt_max_meal))
						ab_error = True
					Else
						If lt_break2 > lt_cal_overflow_time and lb_b2_current_day Then
							wf_error_handling('break_2_start_time' + as_right,'caution','duration','The 2nd Break Start Time has exceeded the shift end time of ' + String(lt_cal_overflow_time))
							ab_error = True
						End If
					End If
				Else
					If lt_break2 <= lt_max_meal and lb_b2_current_day Then
						wf_error_handling('break_2_start_time' + as_right,'caution','duration',"The 2nd Break Start Time must be greater then " + String(lt_max_meal))
						ab_error = True
					Else
						If lt_break2 > lt_cal_overflow_time Then
							wf_error_handling('break_2_start_time' + as_right,'caution','duration','The 2nd Break Start Time has exceeded the shift end time of ' + String(lt_cal_overflow_time))
							ab_error = True
						End If
					End If
				End If
			Else
				If lt_break2 <= lt_max_meal Then
					wf_error_handling('break_2_start_time' + as_right,'caution','duration',"The 2nd Break Start Time must be greater then " + String(lt_max_meal))
					ab_error = True
				End If
				If lt_break2 > lt_cal_overflow_time /*and not lb_next_day*/ Then
					wf_error_handling('break_2_start_time' + as_right,'caution','duration',"The 2nd Break Start Time has exceeded the shift end time of " + String(lt_cal_overflow_time))
					ab_error = True
				End If
 			End If 
		End If
End if

end subroutine

public subroutine wf_mod_time_fields (string as_right);String	ls_modify,ls_white

ls_white = String(RGB(255,255,255))

ls_modify = 'start_time' + as_right + ".Background.Color='0'" + &
				'start_time' + as_right + ".Background.Color='" + ls_white + "'" + &
				'break_1_start_time' + as_right + ".Background.Color='0'" + &
				'break_1_start_time' + as_right + ".Background.Color='" + ls_white + "'" + &
				'meal_start_time' + as_right + ".Background.Color='0'" + &
				'meal_start_time' + as_right + ".Background.Color='" + ls_white + "'" + &
				'break_2_start_time' + as_right + ".Background.Color='0'" + &
				'break_2_start_time' + as_right + ".Background.Color='" + ls_white + "'" 

idw_active_dw.Modify(ls_modify)

end subroutine

public subroutine wf_mod_dura_fields (string as_right);String	ls_modify,ls_white

ls_white = String(RGB(255,255,255))

ls_modify = 'break_1_duration' + as_right + ".Background.Color='0'" + &
				'break_1_duration' + as_right + ".Background.Color='" + ls_white + "'" + &
				'meal_duration' + as_right + ".Background.Color='0'" + &
				'meal_duration' + as_right + ".Background.Color='" + ls_white + "'" + &
				'break_2_duration' + as_right + ".Background.Color='0'" + &
				'break_2_duration' + as_right + ".Background.Color='" + ls_white + "'" + &
				'work_duration' + as_right + ".Background.Color='0'" + &
				'work_duration' + as_right + ".Background.Color='" + ls_white + "'"
idw_active_dw.Modify(ls_modify)

end subroutine

public function integer wf_work_duration_check ();Long 		ll_count = 1,&
			ll_max = 7,&
			ll_row = 1,&
			ll_comp,&
			ll_comp1,&
			ll_comp2
Time		lt_max,&
			lt_temp,&
			lt_check,&
			lt_start
Boolean	lb_next_day

DO UNTIL ll_count > ll_max
	lt_start 	= Time(idw_active_dw.GetItemString(ll_row, 'Start_time_' + String(ll_count)))
	ll_comp		= SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString(ll_row, 'work_duration_' + String(ll_count)))) 
	ll_comp	  += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString(ll_row, 'meal_duration_' + String(ll_count)))) 
	lt_max      = RelativeTime(lt_start,ll_comp)
	If lt_max = Time('23:59:59') Then
		ll_comp1 = SecondsAfter (lt_start,Time('23:59:59')) + 1
		ll_comp2 = ll_comp - ll_comp1
		lt_max = RelativeTime ( Time('00:00:00'), ll_comp2 )
		lb_next_day = True
	End If
/////////////////break_1//////////	
	lt_temp		= Time(idw_active_dw.GetItemString(ll_row, 'break_1_start_time_' + String(ll_count))) 
	If lt_temp <= lt_max & 
		or (lb_next_day and lt_temp <= Time('23:59:59') and lt_temp > lt_start) Then
//    no not
	Else
		idw_active_dw.SetColumn('break_1_start_time_' + String(ll_count))
		wf_error_handling('break_1_start_time_' + String(ll_count),'caution','duration',"The 1st Break Start Time has exceeded the shift end time of " + String(lt_max))//'has exceeded the shift end time of ' + String(lt_max))
		idw_active_dw.SelectText ( 1, 5 )
		Return 1
	End If
	ll_comp 		= SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString(ll_row, 'break_1_duration_' + String(ll_count)))) 
	lt_check  	= RelativeTime(lt_temp,ll_comp)
	If lt_check = Time('23:59:59') Then
		ll_comp1 = SecondsAfter (lt_temp,Time('23:59:59')) + 1
		ll_comp2 = ll_comp - ll_comp1
		lt_check = RelativeTime ( Time('00:00:00'), ll_comp2 )
		If lb_next_day Then
			If lt_check >= lt_max Then
				idw_active_dw.SetColumn('break_1_duration_' + String(ll_count))
				wf_error_handling('break_1_duration_' + String(ll_count),'caution','duration','The 1st Break Duration has exceeded the shift end time of ' + String(lt_max))//combined Start Time, Work Duration and Meal Duration')
				idw_active_dw.SelectText ( 1, 5 )
				Return 1
			End If
		Else
			If lt_check <= lt_max Then
				idw_active_dw.SetColumn('break_1_duration_' + String(ll_count))
				wf_error_handling('break_1_duration_' + String(ll_count),'caution','duration','The 1st Break Duration has exceeded the shift end time of ' + String(lt_max))
				idw_active_dw.SelectText ( 1, 5 )
				Return 1
			End If
		End If
	Else
		If lt_check <= lt_max  &
			or (lb_next_day and lt_check <= Time('23:59:59') and lt_check > lt_start) Then
//			no not
		Else
			idw_active_dw.SetColumn('break_1_duration_' + String(ll_count))
			wf_error_handling('break_1_duration_' + String(ll_count),'caution','duration','The 1st Break Duration has exceeded the shift end time of ' + String(lt_max))
			idw_active_dw.SelectText ( 1, 5 )
			Return 1
		End if
	End If
	
//////////////meal////////	
	lt_temp 		= Time(idw_active_dw.GetItemString(ll_row, 'meal_start_time_' + String(ll_count))) 
	If lt_temp <= lt_max & 
		or (lb_next_day and lt_temp <= Time('23:59:59') and lt_temp > lt_start) Then
//    no not
	Else
		idw_active_dw.SetColumn('meal_start_time_' + String(ll_count))
		wf_error_handling('meal_start_time_' + String(ll_count),'caution','duration',"The Meal	Start Time has exceeded the shift end time of " + String(lt_max))//'has exceeded the shift end time of ' + String(lt_max))
		idw_active_dw.SelectText ( 1, 5 )
		Return 1
	End If
	ll_comp 		= SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString(ll_row, 'meal_duration_' + String(ll_count))))
	lt_check  	= RelativeTime(lt_temp,ll_comp)
	If lt_check = Time('23:59:59') Then
		ll_comp1 = SecondsAfter (lt_temp,Time('23:59:59')) + 1
		ll_comp2 = ll_comp - ll_comp1
		lt_check = RelativeTime ( Time('00:00:00'), ll_comp2 )
		If lb_next_day Then
			If lt_check >= lt_max Then
				idw_active_dw.SetColumn('meal_duration_' + String(ll_count))
				wf_error_handling('meal_duration_' + String(ll_count),'caution','duration','The Meal Duration has exceeded the shift end time of ' + String(lt_max))//combined Start Time, Work Duration and Meal Duration')
				idw_active_dw.SelectText ( 1, 5 )
				Return 1
			End If
		Else
			If lt_check <= lt_max Then
				idw_active_dw.SetColumn('meal_duration_' + String(ll_count))
				wf_error_handling('meal_duration_' + String(ll_count),'caution','duration','The Meal Duration has exceeded the shift end time of ' + String(lt_max))
				idw_active_dw.SelectText ( 1, 5 )
				Return 1
			End If
		End If
	Else
		If lt_check <= lt_max &
			or (lb_next_day and lt_check <= Time('23:59:59') and lt_check > lt_start) Then
//			no not
		Else
			idw_active_dw.SetColumn('meal_duration_' + String(ll_count))
			wf_error_handling('meal_duration_' + String(ll_count),'caution','duration','The Meal Duration has exceeded the shift end time of ' + String(lt_max))
			idw_active_dw.SelectText ( 1, 5 )
			Return 1
		End if
	End If	
/////////break_2////////	
	lt_temp 		= Time(idw_active_dw.GetItemString(ll_row, 'break_2_start_time_' + String(ll_count)))
	If lt_temp <= lt_max & 
		or (lb_next_day and lt_temp <= Time('23:59:59') and lt_temp > lt_start) Then
//    no not
	Else
		idw_active_dw.SetColumn('break_2_start_time_' + String(ll_count))
		wf_error_handling('break_2_start_time_' + String(ll_count),'caution','duration',"The 2nd Break Start Time has exceeded the shift end time of " + String(lt_max))//'has exceeded the shift end time of ' + String(lt_max))
		idw_active_dw.SelectText ( 1, 5 )
		Return 1
	End If
	ll_comp     = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString(ll_row, 'break_2_duration_' + String(ll_count))))
	lt_check  	= RelativeTime(lt_temp,ll_comp)
	If lt_check = Time('23:59:59') Then
		ll_comp1 = SecondsAfter (lt_temp,Time('23:59:59')) + 1
		ll_comp2 = ll_comp - ll_comp1
		lt_check = RelativeTime ( Time('00:00:00'), ll_comp2 )
		If lb_next_day Then
			If lt_check >= lt_max Then
				idw_active_dw.SetColumn('break_2_duration_' + String(ll_count))
				wf_error_handling('break_2_duration_' + String(ll_count),'caution','duration','The 2nd Break Duration has exceeded the shift end time of ' + String(lt_max))
				idw_active_dw.SelectText ( 1, 5 )
				Return 1
			End If
		Else
			If lt_check <= lt_max Then
				idw_active_dw.SetColumn('break_2_duration_' + String(ll_count))
				wf_error_handling('break_2_duration_' + String(ll_count),'caution','duration','The 2nd Break Duration has exceeded the shift end time of ' + String(lt_max))
				idw_active_dw.SelectText ( 1, 5 )
				Return 1
			End If
		End If
	Else
		If lt_check <= lt_max &
			or (lb_next_day and lt_check <= Time('23:59:59')and lt_check > lt_start) Then
//			no not
		Else
			idw_active_dw.SetColumn('break_2_duration_' + String(ll_count))
			wf_error_handling('break_2_duration_' + String(ll_count),'caution','duration','The 2nd Break Duration has exceeded the shift end time of ' + String(lt_max))
			idw_active_dw.SelectText ( 1, 5 )
			Return 1
		End if
	End If
	lb_next_day		= False
	ll_count ++
LOOP
Return 0
end function

public subroutine wf_mod_others (string as_right);String	ls_modify,ls_white

ls_white = String(RGB(255,255,255))

ls_modify = 'chain_speed' + as_right + ".Background.Color='0'" + &
				'chain_speed' + as_right + ".Background.Color='" + ls_white + "'" + &
				'shipping_lag' + as_right + ".Background.Color='0'" + &
				'shipping_lag' + as_right + ".Background.Color='" + ls_white + "'" + &
				'load_crew' + as_right + ".Background.Color='0'" + &
				'load_crew' + as_right + ".Background.Color='" + ls_white + "'" + &
				'box_hour' + as_right + ".Background.Color='0'" + &
				'box_hour' + as_right + ".Background.Color='" + ls_white + "'" + &
				'combos_hour' + as_right + ".Background.Color='0'" + &
				'combos_hour' + as_right + ".Background.Color='" + ls_white + "'" 
				
idw_active_dw.Modify(ls_modify)

end subroutine

public function boolean wf_post_update (datawindow adw_active_dw);String			ls_input_string,&
					ls_header_string
					
Long				ll_row = 1,&
					ll_count = 1,&
					ll_max = 7,&
					ll_check,&
					ll_color,&
					ll_red
Boolean			lb_error,&
					lb_build_string

dwItemStatus	l_status



SetPointer(HourGlass!)

ll_red = RGB(255,0,0)

DO UNTIL ll_count > ll_max
	ll_color = Long(adw_active_dw.Describe("Start_time_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "Start_time_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("work_duration_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "work_duration_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("break_1_start_time_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "break_1_start_time_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("break_1_duration_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "break_1_duration_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("meal_start_time_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "meal_start_time_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("meal_duration_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "meal_duration_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("break_2_start_time_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "break_2_start_time_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("break_2_duration_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "break_2_duration_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("chain_speed_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "chain_speed_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("shipping_lag_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "shipping_lag_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("load_crew_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "load_crew_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("box_hour_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "box_hour_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_color = Long(adw_active_dw.Describe("combo_hour_"+ String(ll_count)+".Background.Color"))
	If ll_color = ll_red Then
		adw_active_dw.SetColumn ( "combo_hour_"+ String(ll_count) )
		lb_error =True
		EXIT
	End If
	ll_count ++
LOOP

If lb_error Then
		wf_error_handling('','','duration','You must fix all highlighted errors')
		adw_active_dw.SelectText ( 1, 5 )
		adw_active_dw.SetFocus()
		Return False
End If

ll_check  = wf_work_duration_check()
If ll_check > 0 Then
	Return False
End If
			
ls_header_string = ''
ls_header_string  = 	String(dw_plant.uf_get_plant_code()) + '~t' 
//ls_header_string +=  String(dw_type.uf_get_type()) + '~t'
ls_header_string +=  is_select_time + '~t'
ls_header_string +=  String(dw_effective_date.uf_get_effective_date(),'yyyy-mm-dd') + '~t'
ls_header_string +=	String(dw_shift.uf_get_shift()) + '~r~n'

//ls_input_string = adw_active_dw.Object.DataWindow.Data
ll_count = 1	
DO UNTIL ll_count > ll_max
	l_status = adw_active_dw.GetItemStatus ( 1, 'Start_time_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'work_duration_' + String(ll_count), Primary! )
		If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'break_1_start_time_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'break_1_duration_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'meal_start_time_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'meal_duration_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'break_2_start_time_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'break_2_duration_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'chain_speed_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'shipping_lag_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'load_crew_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'box_hour_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	l_status = adw_active_dw.GetItemStatus ( 1, 'combo_hour_' + String(ll_count), Primary! )
	If l_status = DataModified! Then 
		lb_build_string = True
	End If
	
	If lb_build_string Then
		wf_build_string(ll_count,adw_active_dw)
		lb_build_string = False
	End If
	ll_count ++
LOOP


	
//MessageBox('Header Data', ls_header_string)
//messageBox('DataWindow Data', ls_input_string)

//If Not iu_pas201.nf_pasp89br_upd_work_hours(istr_error_info, &
//																ls_header_string, &
//																is_input_string) Then	
//																Return False
//End If 

iu_ws_pas5.nf_pasp89fr(istr_error_info, ls_header_string, is_input_string)

is_input_string = ''
iw_frame.SetMicroHelp('Modification Successful')
adw_active_dw.ResetUpdate()
// I had to do this because of the posting look at closequery

If ib_reinquire Then
	wf_retrieve()
End If

If ib_close_this Then
	Close(This)
End If

Return True
end function

public function boolean wf_update ();Long 				ll_count, &
					ll_counter
		
//dmk
//dmk sr6722
IF tab_1.tabpage_1.dw_work_hours_prod.AcceptText() = -1 Or &
	tab_1.tabpage_2.dw_work_hours_tran.AcceptText() = -1 Or &
	tab_1.tabpage_3.dw_work_hours_ship.AcceptText() = -1 Or & 
	tab_1.tabpage_4.dw_work_hours_rec.AcceptText() = -1 Or &
	tab_1.tabpage_5.dw_work_hours_pt.AcceptText() = -1 THEN Return False
//dmk
For ll_count = 1 to 5
	CHOOSE CASE ll_count
		CASE 1
			tab_1.tabpage_1.dw_work_hours_prod.SetFocus()
		CASE 2
			tab_1.tabpage_2.dw_work_hours_tran.SetFocus()
		CASE 3
			tab_1.tabpage_3.dw_work_hours_ship.SetFocus()
		CASE 4
			tab_1.tabpage_4.dw_work_hours_rec.SetFocus()
		CASE 5
			tab_1.tabpage_5.dw_work_hours_pt.SetFocus()
	END CHOOSE

	IF idw_active_dw.ModifiedCount ( ) <> 0 Then
		Post wf_post_update(idw_active_dw)
	Else
		ll_counter ++
	End If
Next

If ll_counter = 3 then
	iw_frame.SetMicroHelp('No update necessary')
	Return False
End If
Return True
end function

public subroutine wf_build_string (long al_column, datawindow adw_active_dw);String 	ls_column_name

//dmk
//dmk sr6722 add planned transfers
CHOOSE CASE adw_active_dw.ClassName()
		CASE 'dw_work_hours_prod'
			is_input_string += 'PR' + '~t'
		CASE 'dw_work_hours_tran'
			is_input_string +=  'TR' + '~t'
		CASE 'dw_work_hours_ship'
			is_input_string +=  'SP' + '~t'
		CASE 'dw_work_hours_rec'
			is_input_string +=  'RC' + '~t'
		CASE 'dw_work_hours_pt'
			is_input_string +=  'SH' + '~t'
END CHOOSE
is_input_string += adw_active_dw.GetItemString(1, 'Start_time_' + String(al_column)) + '~t'
is_input_string += adw_active_dw.GetItemString(1, 'work_duration_' + String(al_column)) + '~t'
is_input_string += adw_active_dw.GetItemString(1, 'break_1_start_time_' + String(al_column)) + '~t'
is_input_string += adw_active_dw.GetItemString(1, 'break_1_duration_' + String(al_column)) + '~t'
is_input_string += adw_active_dw.GetItemString(1, 'meal_start_time_' + String(al_column)) + '~t'
is_input_string += adw_active_dw.GetItemString(1, 'meal_duration_' + String(al_column)) + '~t'
is_input_string += adw_active_dw.GetItemString(1, 'break_2_start_time_' + String(al_column)) + '~t'
is_input_string += adw_active_dw.GetItemString(1, 'break_2_duration_' + String(al_column)) + '~t'
is_input_string += String(adw_active_dw.GetItemNumber(1, 'chain_speed_' + String(al_column))) + '~t'
is_input_string += adw_active_dw.GetItemString(1, 'shipping_lag_' + String(al_column)) + '~t'
is_input_string += String(adw_active_dw.GetItemNumber(1, 'load_crew_' + String(al_column))) + '~t'
is_input_string += String(adw_active_dw.GetItemNumber(1, 'box_hour_' + String(al_column))) + '~t'
is_input_string += String(adw_active_dw.GetItemNumber(1, 'combo_hour_' + String(al_column))) + '~t'
	
If is_select_time = 'S' Then
	ls_column_name = "date"+ String(al_column)+"_c"
	is_input_string += string(tab_1.tabpage_1.dw_work_hours_prod.GetItemDatetime(1,string(ls_column_name)),'yyyy-mm-dd') + '~r~n'
//	is_input_string += string(tab_1.tabpage_1.dw_work_hours_prod./*adw_active_dw*/.GetItemDatetime(1,string(ls_column_name)),'yyyy-mm-dd') + '~r~n'
Else	
	is_input_String += '0001-01-0' + string(al_column) + '~r~n'
End If
	
end subroutine

public function boolean wf_retrieve ();long			ll_rec_count, &
				ll_row,&
				ll_count,&
				ll_max = 7,ll_total_count

//dmk
string		ls_plant, &
				ls_plant_desc, & 
				ls_shift,&
				ls_production_date, &
				ls_weekly_data, &
				ls_temp, &
				ls_header,&
				ls_prod, &
				ls_ship, &
				ls_tran, &
				ls_rec, &
				ls_plan_tfr


date			ldt_week_end_date

u_string_functions	lu_string


// If inquiring set a boolean for closequery 
ib_reinquire = True
If This.Event closequery() <> 0 Then Return False
ib_reinquire = False

//If Message.ReturnValue <> 0 Then Return False

OpenWithParm(iw_inquirewindow, This, is_inquire_window_name)

If Not ib_inquire Then
	Return False
End If

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait.. Inquiring Database")

This.SetRedraw(False) 

istr_error_info.se_event_name = "wf_retrieve"
istr_error_info.se_procedure_name = "nf_pasp88br_inq_work_hours"
istr_error_info.se_message = Space(71)

ls_plant = dw_plant.uf_get_plant_code()
ls_plant_desc = dw_plant.uf_get_plant_descr()  

//ls_type = dw_type.uf_get_type()

ls_shift = dw_shift.uf_get_shift()

If ls_shift = 'D' Then
	tab_1.tabpage_1.Enabled = True
	tab_1.tabpage_2.Enabled = True
	tab_1.tabpage_3.Enabled = False
	tab_1.tabpage_4.Enabled = False
	tab_1.tabpage_5.Enabled = False	
Else	
	tab_1.tabpage_1.Enabled = True
	tab_1.tabpage_2.Enabled = True
	tab_1.tabpage_3.Enabled = True
	tab_1.tabpage_4.Enabled = True
	tab_1.tabpage_5.Enabled = True	
End If

tab_1.tabpage_1.dw_work_hours_prod.reset()
tab_1.tabpage_2.dw_work_hours_tran.reset()
tab_1.tabpage_3.dw_work_hours_ship.reset()
//dmk
tab_1.tabpage_4.dw_work_hours_rec.reset()
//sr6722
tab_1.tabpage_5.dw_work_hours_pt.reset()

ls_production_date = string(dw_effective_date.uf_get_effective_date(), "yyyy-mm-dd" )
If is_select_time = 'S' Then
	tab_1.tabpage_1.dw_work_hours_prod.object.date_dt.expression = ls_production_date
	tab_1.tabpage_2.dw_work_hours_tran.object.date_dt.expression = ls_production_date
	tab_1.tabpage_3.dw_work_hours_ship.object.date_dt.expression = ls_production_date
	//dmk
	tab_1.tabpage_4.dw_work_hours_rec.object.date_dt.expression = ls_production_date
	//sr6722
	tab_1.tabpage_5.dw_work_hours_pt.object.date_dt.expression = ls_production_date
	dw_effective_date.Show()
Else
	tab_1.tabpage_1.dw_work_hours_prod.object.date_dt.expression = '0001-01-01'
	tab_1.tabpage_2.dw_work_hours_tran.object.date_dt.expression = '0001-01-01'
	tab_1.tabpage_3.dw_work_hours_ship.object.date_dt.expression = '0001-01-01'
	//dmk
	tab_1.tabpage_4.dw_work_hours_rec.object.date_dt.expression = '0001-01-01'
	//sr6722
	tab_1.tabpage_5.dw_work_hours_pt.object.date_dt.expression = '0001-01-01'
	dw_effective_date.Hide()
End If

ls_header = ls_plant + '~t' + &
				is_select_time + '~t' + &
				ls_production_date + '~t' + &
				ls_shift + '~r~n' 

//MessageBox('Header Data',ls_header)				
//dmk
//sr6722
//If Not iu_pas201.nf_pasp88br_inq_work_hours(istr_error_info, & 
//										ls_header, &
//										ls_prod, &
//										ls_ship, &
//										ls_tran, &
//										ls_rec, &
//										ls_plan_tfr) Then //< 0 Then
//										This.SetRedraw(True) 
//										Return False
//End If		

iu_ws_pas5.nf_pasp88fr(istr_error_info, & 
										ls_header, &
										ls_prod, &
										ls_ship, &
										ls_tran, &
										ls_rec, &
										ls_plan_tfr)
This.SetRedraw(True)





//MessageBox('prod',ls_prod)				

//MessageBox('tran',ls_tran)				

//MessageBox('ship',ls_ship)				

//ls_prod = '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '" "~t" "~t" "~t" "~t" "~t" "~t" "~t" "~t452~t" "~t1000~t2000~t3000~r~n' 
//ls_tran = '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '" "~t" "~t" "~t" "~t" "~t" "~t" "~t" "~t452~t" "~t1000~t2000~t3000~r~n' 
//ls_ship = '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '06:30~t00:00~t06:31~t00:00~t06:32~t00:00~t06:33~t00:00~t452~t00:00~t1000~t2000~t3000~t' + &
//                 '" "~t" "~t" "~t" "~t" "~t" "~t" "~t" "~t452~t" "~t1000~t2000~t3000~r~n' 
					  
//MessageBox('Return Data',string(ls_weekly_data))

If Not lu_string.nf_IsEmpty(ls_prod) Then
	ll_rec_count = tab_1.tabpage_1.dw_work_hours_prod.ImportString(ls_prod)
	If ll_rec_count > 0 Then ll_total_count += ll_rec_count
End If
If Not lu_string.nf_IsEmpty(ls_tran) Then
	ll_rec_count = tab_1.tabpage_2.dw_work_hours_tran.ImportString(ls_tran)
	If ll_rec_count > 0 Then ll_total_count += ll_rec_count
End If
If Not lu_string.nf_IsEmpty(ls_ship) Then
	ll_rec_count = tab_1.tabpage_3.dw_work_hours_ship.ImportString(ls_ship)
	If ll_rec_count > 0 Then ll_total_count += ll_rec_count
End If
//dmk
If Not lu_string.nf_IsEmpty(ls_rec) Then
	ll_rec_count = tab_1.tabpage_4.dw_work_hours_rec.ImportString(ls_rec)
	If ll_rec_count > 0 Then ll_total_count += ll_rec_count
End If
//SR6722
If Not lu_string.nf_IsEmpty(ls_rec) Then
	ll_rec_count = tab_1.tabpage_5.dw_work_hours_pt.ImportString(ls_plan_tfr)
	If ll_rec_count > 0 Then ll_total_count += ll_rec_count
End If
	
If ll_total_count > 0 Then
	SetMicroHelp(String(ll_total_count) + " Rows Retrieved")
Else
	SetMicroHelp("0 Rows Retrieved")
End if

This.SetRedraw(True) 

tab_1.tabpage_1.dw_work_hours_prod.ResetUpdate()
tab_1.tabpage_2.dw_work_hours_tran.ResetUpdate()
tab_1.tabpage_3.dw_work_hours_ship.ResetUpdate()
//dmk
tab_1.tabpage_4.dw_work_hours_rec.ResetUpdate()
//sr6722
tab_1.tabpage_5.dw_work_hours_pt.ResetUpdate()

tab_1.tabpage_1.dw_work_hours_prod.SetFocus()
 
Return True

end function

event deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')
iw_frame.im_menu.mf_enable('m_sort')
iw_frame.im_menu.mf_enable('m_filter')
iw_frame.im_menu.mf_enable('m_new')


iw_frame.im_menu.mf_Disable('m_save')

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_nonvisprint')
iw_frame.im_menu.mf_Disable('m_sort')
iw_frame.im_menu.mf_Disable('m_filter')

iw_frame.im_menu.mf_Enable('m_save')


end event

event ue_postopen;call super::ue_postopen;Environment	le_env


GetEnvironment(le_env)
If le_Env.OsMajorRevision = 4 Or le_Env.OSType = WindowsNT! Then
 	il_SelectedColor = GetSysColor(13)
	il_SelectedTextColor = GetSysColor(14)
Else
 	il_SelectedColor = 255//GetSysColor(13)
	il_SelectedTextColor = 0//GetSysColor(14)
End if

//dw_effective_date.uf_initilize('Production Date:',' ')

//ib_reinquire = FALSE

//This.PostEvent('ue_query')//  structure for calls to the rpc's
istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "workhour"
istr_error_info.se_user_id 		= sqlca.userid

// open inquire window
is_inquire_window_name = 'w_work_hours_inq'

iu_pas201 = Create u_pas201
iu_ws_pas5 = Create u_ws_pas5

This.post wf_retrieve()

end event

on w_work_hours.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_shift=create dw_shift
this.dw_effective_date=create dw_effective_date
this.dw_select_time=create dw_select_time
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_shift
this.Control[iCurrent+3]=this.dw_effective_date
this.Control[iCurrent+4]=this.dw_select_time
this.Control[iCurrent+5]=this.tab_1
end on

on w_work_hours.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_shift)
destroy(this.dw_effective_date)
destroy(this.dw_select_time)
destroy(this.tab_1)
end on

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'plant' 
		Message.StringParm = dw_plant.uf_get_plant_code()
	Case 'shift'
		Message.StringParm = dw_shift.uf_get_shift()
	//Case 'worktype'
		//Message.StringParm = dw_type.uf_get_type()
	Case 'time'
		Message.StringParm = dw_select_time.uf_get_select_time()
		Message.StringParm = is_select_time
	Case 'effective_date'
		Message.StringParm = String(dw_effective_date.uf_get_effective_date())
End Choose






end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'plant' 
		dw_plant.uf_set_plant_code(as_value)
	Case 'shift' 
		dw_shift.uf_set_shift(as_value)
	//Case 'worktype'
		//dw_type.uf_set_type(as_value)
	Case 'time'
		dw_select_time.uf_set_select_time(as_value)
		is_select_time = as_value
	Case 'effective_date'
		dw_effective_date.uf_set_effective_date(Date(as_value))
		
End Choose




end event

event closequery;
integer	li_prompt_onsave

//gets the settings values
gw_base_frame.iu_base_data.Trigger Event ue_get_sheetsettings(li_prompt_onsave)
IF gw_base_frame.ib_exit and li_prompt_onsave = 0 THEN RETURN

// Do a security check for save and do nothing if the user does not have priviliges
IF NOT gw_base_frame.im_base_menu.m_file.m_save.enabled THEN RETURN

CHOOSE CASE This.wf_modified( )

	CASE 1
		// Changes were made to one of the datawindows
			CHOOSE CASE MessageBox(This.Title, "Do you want to save changes?", Question!, YesNoCancel!)
// I had to do this do to the posting of the update case 1 changed ibdkdld					
// Set boolean to close window after wf_post_update finished if its not a inquire from wf_retrieve	
				CASE 1	// Save Changes
					If Not ib_reinquire Then
						ib_close_this = True
					End If
						This.wf_update() 
					Return 1	 //  do not close window

				CASE 2	// Do not save changes

				CASE 3	// Cancel the closing of window
					Message.ReturnValue = 1
					Return
			END CHOOSE

	CASE 0
		// No modifications were made to any datawindows on this sheet...closing of sheet allowed

	CASE -1
		// Problem with the AcceptText() function against a datawindow.
		// The item error will handle setting focus to the correct column. 
		Message.ReturnValue = 1
		Return

END CHOOSE

end event

event close;call super::close;Destroy iu_ws_pas5
end event

type dw_plant from u_plant within w_work_hours
integer y = 4
integer taborder = 10
boolean bringtotop = true
end type

event constructor;call super::constructor;Disable()
end event

type dw_shift from u_shift within w_work_hours
integer x = 14
integer y = 92
integer taborder = 40
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_enable(False)
end event

type dw_effective_date from u_effective_date within w_work_hours
boolean visible = false
integer x = 1143
integer y = 160
integer width = 727
integer taborder = 20
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_initilize('Production Date:','')
This.uf_set_effective_date(Today())
This.uf_enable(False)
end event

type dw_select_time from u_select_time within w_work_hours
integer x = 1902
integer taborder = 30
boolean bringtotop = true
end type

event constructor;call super::constructor;This.uf_enable(False)
end event

type tab_1 from tab within w_work_hours
event ue_postitemchanged ( string as_column_name,  string as_data )
string tag = "Production"
integer x = 9
integer y = 240
integer width = 2674
integer height = 1248
integer taborder = 50
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 79741120
boolean raggedright = true
boolean showpicture = false
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2638
integer height = 1132
long backcolor = 79741120
string text = "Production"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_work_hours_prod dw_work_hours_prod
end type

on tabpage_1.create
this.dw_work_hours_prod=create dw_work_hours_prod
this.Control[]={this.dw_work_hours_prod}
end on

on tabpage_1.destroy
destroy(this.dw_work_hours_prod)
end on

type dw_work_hours_prod from u_base_dw_ext within tabpage_1
event ue_postitemchanged ( string as_column_name,  string as_data )
integer width = 2638
integer height = 1160
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_work_hours"
boolean border = false
end type

event ue_postitemchanged;This.SetItem(1, as_column_name, as_data)
end event

event constructor;call super::constructor;ib_updateable = true
is_select_time = 'D'
//
This.Object.load_crew_t.Visible = 0
This.Object.boxes_t.Visible = 0
This.Object.combos_t.Visible = 0
This.Object.load_crew_1.Visible = 0
This.Object.load_crew_2.Visible = 0
This.Object.load_crew_3.Visible = 0
This.Object.load_crew_4.Visible = 0
This.Object.load_crew_5.Visible = 0
This.Object.load_crew_6.Visible = 0
This.Object.load_crew_7.Visible = 0
This.Object.box_hour_1.Visible = 0
This.Object.box_hour_2.Visible = 0
This.Object.box_hour_3.Visible = 0
This.Object.box_hour_4.Visible = 0
This.Object.box_hour_5.Visible = 0
This.Object.box_hour_6.Visible = 0
This.Object.box_hour_7.Visible = 0
This.Object.combo_hour_1.Visible = 0
This.Object.combo_hour_2.Visible = 0
This.Object.combo_hour_3.Visible = 0
This.Object.combo_hour_4.Visible = 0
This.Object.combo_hour_5.Visible = 0
This.Object.combo_hour_6.Visible = 0
This.Object.combo_hour_7.Visible = 0

end event

event editchanged;String 					ls_temp	
u_string_functions	lu_string
Boolean					lb_numeric


ls_temp = Right(data,1)
If Not lu_string.nf_IsEmpty(ls_temp) Then
	CHOOSE CASE dwo.name
		CASE 'chain_speed_1'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_2'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_3'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_4'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_5'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_6'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_7'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE ELSE
			IF Not IsNumber(ls_temp) THEN
				If Len(Data) > 1 Then
					If lu_string.nf_IsEmpty(ls_temp) or ls_temp = ':' Then
						//do no not
					Else
						wf_error_handling('','severe','duration','Must be numeric, spaces, or a Colon')
						beep(1)
						idw_active_dw.SelectText ( Len(data) , 1)
						Return  1
					End if
				Else
					If not lu_string.nf_IsEmpty(ls_temp) Then
						If ls_temp = ':' Then 
							wf_error_handling('','severe','duration','Must have a hour(number) before the colon')
						Else	
							wf_error_handling('','severe','duration','Must be numeric, spaces, or a colon')
						End If
						beep(1)
						idw_active_dw.SelectText ( 1, 1)
						Return  1
				End if
			End If
		END IF
	END CHOOSE
End If

IF lb_numeric then
   wf_error_handling('','severe','duration','Must be numeric')
	beep(1)
	idw_active_dw.SelectText ( Len(data) , 1)
	Return  1
End IF

Return 0
end event

event itemchanged;call super::itemchanged;Time			lt_time,&
				lt_min,&
				lt_comp, &
				lt_temp_above, &
				lt_temp_time, &
				lt_temp_below, &
				lt_temp
				
Boolean		lb_time, &
				lb_dura, &
				lb_data_empty, &
				lb_column_above_empty, &
				lb_column_below_empty, &
				lb_temp_above, &
				lb_temp_below, &
				lb_error,&
				lb_min_error,&
				lb_more_mess, &
				lb_Invalid
				
long			ll_error, &
				ll_len,&
				ll_chain,&
				ll_count,&
				ll_time,&
				ll_temp_time,&
				ll_comp

string		ls_name,&
				ls_name1,&
				ls_modify,&
				ls_right,&
				ls_shift,&
				ls_time, &
				ls_temp,&
				ls_temp_above,&
				ls_temp_below,&
				ls_compare_byte
				
nvuo_pa_business_rules 	nvuo_pa
u_string_functions		u_string


// I did this to make the code more generic
ls_name = dwo.name
ls_right = Right(ls_name,2)
ls_name1 = Left(ls_name, Len(ls_name) - 2)
ll_len = Len(data)


wf_error_handling('','','ready','')

CHOOSE CASE ls_name1
	CASE "start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			ls_shift = dw_shift.uf_get_shift() 
			lt_min	= nvuo_pa.uf_check_min_shift_start_time(ls_shift)
 			If lt_time < lt_min Then
				wf_error_handling(ls_name,'severe','shiftmin' + ls_shift + string(lt_min),'')
				beep(1)
				Return 1
				lb_error = True
			End If
			lb_time = True
		Else
			lb_data_empty = True
		End If

	CASE "break_1_start_time"
		wf_mod_time_fields(ls_right)
  		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "break_2_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "work_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_time = SecondsAfter (Time('00:00:00'), lt_time )
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationgreat','')
				lb_error = True
			End  If
		Else
			lb_data_empty = True
		End If

	CASE "break_1_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End IF
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The 1st Break Duration change has exceeded the time set for Meal Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End If
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The Meal Duration change has exceeded the time set for the 2nd Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If

	CASE "break_2_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
		Else
			lb_data_empty = True
		End If
		
	CASE "shipping_lag"
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
		Else
			lb_data_empty = True
		End If
		
	CASE "chain_speed"
		ll_chain = Sign(Long(data))
		If ll_chain < 0 Then
			wf_error_handling(ls_name,'severe','chainneg','')
			beep(1)
			Return 1
		End If	
		ll_chain = nvuo_pa.uf_check_max_chain_speed()
		If Long(Data) > ll_chain Then
			wf_error_handling(ls_name + String(ll_chain),'severe','chain','')
			Beep (1)
			Return 1
		Else
			wf_error_handling(ls_name,'noerror','ready','')
		End If
End Choose							

If Not lb_data_empty /*and ls_name1 <> 'load_crew'*/ Then
	ls_time = String(lt_time, 'hh:mm')
	This.Event Post ue_postitemchanged(ls_name,ls_time)
End If			
	
If	lb_time Then
	POST wf_process_time_fields(ls_right,lb_error)
End If	

return 0

end event

event itemerror;call super::itemerror;return 2
end event

event getfocus;idw_active_dw = This
end event

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2638
integer height = 1132
long backcolor = 79741120
string text = "Transfer"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_work_hours_tran dw_work_hours_tran
end type

on tabpage_2.create
this.dw_work_hours_tran=create dw_work_hours_tran
this.Control[]={this.dw_work_hours_tran}
end on

on tabpage_2.destroy
destroy(this.dw_work_hours_tran)
end on

type dw_work_hours_tran from u_base_dw_ext within tabpage_2
event ue_postitemchanged ( string as_column_name,  string as_data )
integer width = 2638
integer height = 1160
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_work_hours"
boolean border = false
end type

event ue_postitemchanged;This.SetItem(1, as_column_name, as_data)
end event

event constructor;call super::constructor;ib_updateable = true
is_select_time = 'D'
//
This.Object.load_crew_t.Visible = 0
This.Object.boxes_t.Visible = 0
This.Object.combos_t.Visible = 0
This.Object.load_crew_1.Visible = 0
This.Object.load_crew_2.Visible = 0
This.Object.load_crew_3.Visible = 0
This.Object.load_crew_4.Visible = 0
This.Object.load_crew_5.Visible = 0
This.Object.load_crew_6.Visible = 0
This.Object.load_crew_7.Visible = 0
This.Object.box_hour_1.Visible = 0
This.Object.box_hour_2.Visible = 0
This.Object.box_hour_3.Visible = 0
This.Object.box_hour_4.Visible = 0
This.Object.box_hour_5.Visible = 0
This.Object.box_hour_6.Visible = 0
This.Object.box_hour_7.Visible = 0
This.Object.combo_hour_1.Visible = 0
This.Object.combo_hour_2.Visible = 0
This.Object.combo_hour_3.Visible = 0
This.Object.combo_hour_4.Visible = 0
This.Object.combo_hour_5.Visible = 0
This.Object.combo_hour_6.Visible = 0
This.Object.combo_hour_7.Visible = 0


end event

event editchanged;String 					ls_temp	
u_string_functions	lu_string
Boolean					lb_numeric


ls_temp = Right(data,1)
If Not lu_string.nf_IsEmpty(ls_temp) Then
	CHOOSE CASE dwo.name
		CASE 'chain_speed_1'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_2'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_3'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_4'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_5'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_6'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_7'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE ELSE
			IF Not IsNumber(ls_temp) THEN
				If Len(Data) > 1 Then
					If lu_string.nf_IsEmpty(ls_temp) or ls_temp = ':' Then
						//do no not
					Else
						wf_error_handling('','severe','duration','Must be numeric, spaces, or a Colon')
						beep(1)
						idw_active_dw.SelectText ( Len(data) , 1)
						Return  1
					End if
				Else
					If not lu_string.nf_IsEmpty(ls_temp) Then
						If ls_temp = ':' Then 
							wf_error_handling('','severe','duration','Must have a hour(number) before the colon')
						Else	
							wf_error_handling('','severe','duration','Must be numeric, spaces, or a colon')
						End If
						beep(1)
						idw_active_dw.SelectText ( 1, 1)
						Return  1
				End if
			End If
		END IF
	END CHOOSE
End If

IF lb_numeric then
   wf_error_handling('','severe','duration','Must be numeric')
	beep(1)
	idw_active_dw.SelectText ( Len(data) , 1)
	Return  1
End IF

Return 0
end event

event itemchanged;call super::itemchanged;Time			lt_time,&
				lt_min,&
				lt_comp, &
				lt_temp_above, &
				lt_temp_time, &
				lt_temp_below, &
				lt_temp
				
Boolean		lb_time, &
				lb_dura, &
				lb_data_empty, &
				lb_column_above_empty, &
				lb_column_below_empty, &
				lb_temp_above, &
				lb_temp_below, &
				lb_error,&
				lb_min_error,&
				lb_more_mess, &
				lb_Invalid
				
long			ll_error, &
				ll_len,&
				ll_chain,&
				ll_count,&
				ll_time,&
				ll_temp_time,&
				ll_comp

string		ls_name,&
				ls_name1,&
				ls_modify,&
				ls_right,&
				ls_shift,&
				ls_time, &
				ls_temp,&
				ls_temp_above,&
				ls_temp_below,&
				ls_compare_byte
				
nvuo_pa_business_rules 	nvuo_pa
u_string_functions		u_string


// I did this to make the code more generic
ls_name = dwo.name
ls_right = Right(ls_name,2)
ls_name1 = Left(ls_name, Len(ls_name) - 2)
ll_len = Len(data)


wf_error_handling('','','ready','')

CHOOSE CASE ls_name1
	CASE "start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			ls_shift = dw_shift.uf_get_shift() 
			lt_min	= nvuo_pa.uf_check_min_shift_start_time(ls_shift)
 			If lt_time < lt_min Then
				wf_error_handling(ls_name,'severe','shiftmin' + ls_shift + string(lt_min),'')
				beep(1)
				Return 1
				lb_error = True
			End If
			lb_time = True
		Else
			lb_data_empty = True
		End If

	CASE "break_1_start_time"
		wf_mod_time_fields(ls_right)
  		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "break_2_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "work_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_time = SecondsAfter (Time('00:00:00'), lt_time )
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationgreat','')
				lb_error = True
			End  If
		Else
			lb_data_empty = True
		End If

	CASE "break_1_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End IF
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The 1st Break Duration change has exceeded the time set for Meal Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End If
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The Meal Duration change has exceeded the time set for the 2nd Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If

	CASE "break_2_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
		Else
			lb_data_empty = True
		End If
		
	CASE "shipping_lag"
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
		Else
			lb_data_empty = True
		End If
		
	CASE "chain_speed"
		ll_chain = Sign(Long(data))
		If ll_chain < 0 Then
			wf_error_handling(ls_name,'severe','chainneg','')
			beep(1)
			Return 1
		End If	
		ll_chain = nvuo_pa.uf_check_max_chain_speed()
		If Long(Data) > ll_chain Then
			wf_error_handling(ls_name + String(ll_chain),'severe','chain','')
			Beep (1)
			Return 1
		Else
			wf_error_handling(ls_name,'noerror','ready','')
		End If
End Choose							

If Not lb_data_empty Then
	ls_time = String(lt_time, 'hh:mm')
	This.Event Post ue_postitemchanged(ls_name,ls_time)
End If			
	
If	lb_time Then
	POST wf_process_time_fields(ls_right,lb_error)
End If	

return 0

end event

event itemerror;call super::itemerror;Return 2
end event

event getfocus;idw_active_dw = This
end event

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2638
integer height = 1132
long backcolor = 79741120
string text = "Shipping"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_work_hours_ship dw_work_hours_ship
end type

on tabpage_3.create
this.dw_work_hours_ship=create dw_work_hours_ship
this.Control[]={this.dw_work_hours_ship}
end on

on tabpage_3.destroy
destroy(this.dw_work_hours_ship)
end on

type dw_work_hours_ship from u_base_dw_ext within tabpage_3
event ue_postitemchanged ( string as_column_name,  string as_data )
integer width = 2638
integer height = 1160
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_work_hours"
boolean border = false
end type

event ue_postitemchanged;This.SetItem(1, as_column_name, as_data)
end event

event constructor;call super::constructor;ib_updateable = true
is_select_time = 'D'
//
This.Object.chain_speed_t.Visible = 0
This.Object.shipping_lag_t.Visible = 0
This.Object.chain_speed_1.Visible = 0
This.Object.chain_speed_2.Visible = 0
This.Object.chain_speed_3.Visible = 0
This.Object.chain_speed_4.Visible = 0
This.Object.chain_speed_5.Visible = 0
This.Object.chain_speed_6.Visible = 0
This.Object.chain_speed_7.Visible = 0
This.Object.shipping_lag_1.Visible = 0
This.Object.shipping_lag_2.Visible = 0
This.Object.shipping_lag_3.Visible = 0
This.Object.shipping_lag_4.Visible = 0
This.Object.shipping_lag_5.Visible = 0
This.Object.shipping_lag_6.Visible = 0
This.Object.shipping_lag_7.Visible = 0

end event

event editchanged;String 					ls_temp	
u_string_functions	lu_string
Boolean					lb_numeric


ls_temp = Right(data,1)
If Not lu_string.nf_IsEmpty(ls_temp) Then
	CHOOSE CASE dwo.name
		CASE 'chain_speed_1'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_2'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_3'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_4'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_5'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_6'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_7'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE ELSE
			IF Not IsNumber(ls_temp) THEN
				If Len(Data) > 1 Then
					If lu_string.nf_IsEmpty(ls_temp) or ls_temp = ':' Then
						//do no not
					Else
						wf_error_handling('','severe','duration','Must be numeric, spaces, or a Colon')
						beep(1)
						idw_active_dw.SelectText ( Len(data) , 1)
						Return  1
					End if
				Else
					If not lu_string.nf_IsEmpty(ls_temp) Then
						If ls_temp = ':' Then 
							wf_error_handling('','severe','duration','Must have a hour(number) before the colon')
						Else	
							wf_error_handling('','severe','duration','Must be numeric, spaces, or a colon')
						End If
						beep(1)
						idw_active_dw.SelectText ( 1, 1)
						Return  1
				End if
			End If
		END IF
	END CHOOSE
End If

IF lb_numeric then
   wf_error_handling('','severe','duration','Must be numeric')
	beep(1)
	idw_active_dw.SelectText ( Len(data) , 1)
	Return  1
End IF

Return 0
end event

event itemchanged;call super::itemchanged;Time			lt_time,&
				lt_min,&
				lt_comp, &
				lt_temp_above, &
				lt_temp_time, &
				lt_temp_below, &
				lt_temp
				
Boolean		lb_time, &
				lb_dura, &
				lb_data_empty, &
				lb_column_above_empty, &
				lb_column_below_empty, &
				lb_temp_above, &
				lb_temp_below, &
				lb_error,&
				lb_min_error,&
				lb_more_mess, &
				lb_Invalid
				
long			ll_error, &
				ll_len,&
				ll_chain,&
				ll_count,&
				ll_time,&
				ll_temp_time,&
				ll_comp

string		ls_name,&
				ls_name1,&
				ls_modify,&
				ls_right,&
				ls_shift,&
				ls_time, &
				ls_temp,&
				ls_temp_above,&
				ls_temp_below,&
				ls_compare_byte
				
nvuo_pa_business_rules 	nvuo_pa
u_string_functions		u_string


// I did this to make the code more generic
ls_name = dwo.name
ls_right = Right(ls_name,2)
ls_name1 = Left(ls_name, Len(ls_name) - 2)
ll_len = Len(data)


wf_error_handling('','','ready','')

CHOOSE CASE ls_name1
	CASE "start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			ls_shift = dw_shift.uf_get_shift() 
			lt_min	= nvuo_pa.uf_check_min_shift_start_time(ls_shift)
 			If lt_time < lt_min Then
				wf_error_handling(ls_name,'severe','shiftmin' + ls_shift + string(lt_min),'')
				beep(1)
				Return 1
				lb_error = True
			End If
			lb_time = True
		Else
			lb_data_empty = True
		End If

	CASE "break_1_start_time"
		wf_mod_time_fields(ls_right)
  		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "break_2_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "work_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_time = SecondsAfter (Time('00:00:00'), lt_time )
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationgreat','')
				lb_error = True
			End  If
		Else
			lb_data_empty = True
		End If

	CASE "break_1_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End IF
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The 1st Break Duration change has exceeded the time set for Meal Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End If
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The Meal Duration change has exceeded the time set for the 2nd Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If

	CASE "break_2_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
		Else
			lb_data_empty = True
		End If
		
	CASE "shipping_lag"
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
		Else
			lb_data_empty = True
		End If
		
	CASE "chain_speed"
		ll_chain = Sign(Long(data))
		If ll_chain < 0 Then
			wf_error_handling(ls_name,'severe','chainneg','')
			beep(1)
			Return 1
		End If	
		ll_chain = nvuo_pa.uf_check_max_chain_speed()
		If Long(Data) > ll_chain Then
			wf_error_handling(ls_name + String(ll_chain),'severe','chain','')
			Beep (1)
			Return 1
		Else
			wf_error_handling(ls_name,'noerror','ready','')
		End If
	CASE "load_crew"
			lb_data_empty = True
End Choose							

If Not lb_data_empty Then
	ls_time = String(lt_time, 'hh:mm')
	This.Event Post ue_postitemchanged(ls_name,ls_time)
End If			
	
If	lb_time Then
	POST wf_process_time_fields(ls_right,lb_error)
End If	

return 0

end event

event itemerror;call super::itemerror;Return 2
end event

event getfocus;idw_active_dw = This
end event

type tabpage_4 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2638
integer height = 1132
long backcolor = 79741120
string text = "Receiving"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_work_hours_rec dw_work_hours_rec
end type

on tabpage_4.create
this.dw_work_hours_rec=create dw_work_hours_rec
this.Control[]={this.dw_work_hours_rec}
end on

on tabpage_4.destroy
destroy(this.dw_work_hours_rec)
end on

type dw_work_hours_rec from u_base_dw_ext within tabpage_4
event ue_postitemchanged ( string as_column_name,  string as_data )
integer width = 2638
integer height = 1160
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_work_hours"
boolean border = false
end type

event ue_postitemchanged;This.SetItem(1, as_column_name, as_data)
end event

event constructor;call super::constructor;ib_updateable = true
is_select_time = 'D'
//
This.Object.chain_speed_t.Visible = 0
This.Object.shipping_lag_t.Visible = 0
This.Object.load_crew_t.Visible = 0
This.Object.boxes_t.Visible = 0
This.Object.combos_t.Visible = 0

This.Object.chain_speed_1.Visible = 0
This.Object.chain_speed_2.Visible = 0
This.Object.chain_speed_3.Visible = 0
This.Object.chain_speed_4.Visible = 0
This.Object.chain_speed_5.Visible = 0
This.Object.chain_speed_6.Visible = 0
This.Object.chain_speed_7.Visible = 0
This.Object.shipping_lag_1.Visible = 0
This.Object.shipping_lag_2.Visible = 0
This.Object.shipping_lag_3.Visible = 0
This.Object.shipping_lag_4.Visible = 0
This.Object.shipping_lag_5.Visible = 0
This.Object.shipping_lag_6.Visible = 0
This.Object.shipping_lag_7.Visible = 0

This.Object.load_crew_1.Visible = 0
This.Object.load_crew_2.Visible = 0
This.Object.load_crew_3.Visible = 0
This.Object.load_crew_4.Visible = 0
This.Object.load_crew_5.Visible = 0
This.Object.load_crew_6.Visible = 0
This.Object.load_crew_7.Visible = 0
This.Object.box_hour_1.Visible = 0
This.Object.box_hour_2.Visible = 0
This.Object.box_hour_3.Visible = 0
This.Object.box_hour_4.Visible = 0
This.Object.box_hour_5.Visible = 0
This.Object.box_hour_6.Visible = 0
This.Object.box_hour_7.Visible = 0
This.Object.combo_hour_1.Visible = 0
This.Object.combo_hour_2.Visible = 0
This.Object.combo_hour_3.Visible = 0
This.Object.combo_hour_4.Visible = 0
This.Object.combo_hour_5.Visible = 0
This.Object.combo_hour_6.Visible = 0
This.Object.combo_hour_7.Visible = 0


end event

event editchanged;String 					ls_temp	
u_string_functions	lu_string
Boolean					lb_numeric


ls_temp = Right(data,1)
If Not lu_string.nf_IsEmpty(ls_temp) Then
	CHOOSE CASE dwo.name
		CASE 'chain_speed_1'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_2'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_3'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_4'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_5'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_6'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_7'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE ELSE
			IF Not IsNumber(ls_temp) THEN
				If Len(Data) > 1 Then
					If lu_string.nf_IsEmpty(ls_temp) or ls_temp = ':' Then
						//do no not
					Else
						wf_error_handling('','severe','duration','Must be numeric, spaces, or a Colon')
						beep(1)
						idw_active_dw.SelectText ( Len(data) , 1)
						Return  1
					End if
				Else
					If not lu_string.nf_IsEmpty(ls_temp) Then
						If ls_temp = ':' Then 
							wf_error_handling('','severe','duration','Must have a hour(number) before the colon')
						Else	
							wf_error_handling('','severe','duration','Must be numeric, spaces, or a colon')
						End If
						beep(1)
						idw_active_dw.SelectText ( 1, 1)
						Return  1
				End if
			End If
		END IF
	END CHOOSE
End If

IF lb_numeric then
   wf_error_handling('','severe','duration','Must be numeric')
	beep(1)
	idw_active_dw.SelectText ( Len(data) , 1)
	Return  1
End IF

Return 0
end event

event getfocus;idw_active_dw = This
end event

event itemchanged;call super::itemchanged;Time			lt_time,&
				lt_min,&
				lt_comp, &
				lt_temp_above, &
				lt_temp_time, &
				lt_temp_below, &
				lt_temp
				
Boolean		lb_time, &
				lb_dura, &
				lb_data_empty, &
				lb_column_above_empty, &
				lb_column_below_empty, &
				lb_temp_above, &
				lb_temp_below, &
				lb_error,&
				lb_min_error,&
				lb_more_mess, &
				lb_Invalid
				
long			ll_error, &
				ll_len,&
				ll_chain,&
				ll_count,&
				ll_time,&
				ll_temp_time,&
				ll_comp

string		ls_name,&
				ls_name1,&
				ls_modify,&
				ls_right,&
				ls_shift,&
				ls_time, &
				ls_temp,&
				ls_temp_above,&
				ls_temp_below,&
				ls_compare_byte
				
nvuo_pa_business_rules 	nvuo_pa
u_string_functions		u_string


// I did this to make the code more generic
ls_name = dwo.name
ls_right = Right(ls_name,2)
ls_name1 = Left(ls_name, Len(ls_name) - 2)
ll_len = Len(data)


wf_error_handling('','','ready','')

CHOOSE CASE ls_name1
	CASE "start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			ls_shift = dw_shift.uf_get_shift() 
			lt_min	= nvuo_pa.uf_check_min_shift_start_time(ls_shift)
 			If lt_time < lt_min Then
				wf_error_handling(ls_name,'severe','shiftmin' + ls_shift + string(lt_min),'')
				beep(1)
				Return 1
				lb_error = True
			End If
			lb_time = True
		Else
			lb_data_empty = True
		End If

	CASE "break_1_start_time"
		wf_mod_time_fields(ls_right)
  		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "break_2_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "work_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_time = SecondsAfter (Time('00:00:00'), lt_time )
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationgreat','')
				lb_error = True
			End  If
		Else
			lb_data_empty = True
		End If

	CASE "break_1_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End IF
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The 1st Break Duration change has exceeded the time set for Meal Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End If
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The Meal Duration change has exceeded the time set for the 2nd Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If

	CASE "break_2_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
		Else
			lb_data_empty = True
		End If
		
	CASE "shipping_lag"
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
		Else
			lb_data_empty = True
		End If
		
	CASE "chain_speed"
		ll_chain = Sign(Long(data))
		If ll_chain < 0 Then
			wf_error_handling(ls_name,'severe','chainneg','')
			beep(1)
			Return 1
		End If	
		ll_chain = nvuo_pa.uf_check_max_chain_speed()
		If Long(Data) > ll_chain Then
			wf_error_handling(ls_name + String(ll_chain),'severe','chain','')
			Beep (1)
			Return 1
		Else
			wf_error_handling(ls_name,'noerror','ready','')
		End If
	CASE "load_crew"
			lb_data_empty = True
End Choose							

If Not lb_data_empty Then
	ls_time = String(lt_time, 'hh:mm')
	This.Event Post ue_postitemchanged(ls_name,ls_time)
End If			
	
If	lb_time Then
	POST wf_process_time_fields(ls_right,lb_error)
End If	

return 0

end event

event itemerror;call super::itemerror;Return 2
end event

type tabpage_5 from userobject within tab_1
integer x = 18
integer y = 100
integer width = 2638
integer height = 1132
long backcolor = 79741120
string text = "Planned Transfer"
long tabtextcolor = 33554432
long tabbackcolor = 79741120
long picturemaskcolor = 536870912
dw_work_hours_pt dw_work_hours_pt
end type

on tabpage_5.create
this.dw_work_hours_pt=create dw_work_hours_pt
this.Control[]={this.dw_work_hours_pt}
end on

on tabpage_5.destroy
destroy(this.dw_work_hours_pt)
end on

type dw_work_hours_pt from u_base_dw_ext within tabpage_5
event ue_postitemchanged ( string as_column_name,  string as_data )
integer width = 2638
integer height = 1160
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_work_hours"
boolean border = false
end type

event ue_postitemchanged;This.SetItem(1, as_column_name, as_data)
end event

event constructor;call super::constructor;ib_updateable = true
is_select_time = 'D'
//
This.Object.chain_speed_t.Visible = 0
This.Object.shipping_lag_t.Visible = 0
This.Object.chain_speed_1.Visible = 0
This.Object.chain_speed_2.Visible = 0
This.Object.chain_speed_3.Visible = 0
This.Object.chain_speed_4.Visible = 0
This.Object.chain_speed_5.Visible = 0
This.Object.chain_speed_6.Visible = 0
This.Object.chain_speed_7.Visible = 0
This.Object.shipping_lag_1.Visible = 0
This.Object.shipping_lag_2.Visible = 0
This.Object.shipping_lag_3.Visible = 0
This.Object.shipping_lag_4.Visible = 0
This.Object.shipping_lag_5.Visible = 0
This.Object.shipping_lag_6.Visible = 0
This.Object.shipping_lag_7.Visible = 0

end event

event editchanged;String 					ls_temp	
u_string_functions	lu_string
Boolean					lb_numeric


ls_temp = Right(data,1)
If Not lu_string.nf_IsEmpty(ls_temp) Then
	CHOOSE CASE dwo.name
		CASE 'chain_speed_1'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_2'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_3'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_4'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_5'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_6'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE 'chain_speed_7'
			IF Not IsNumber(ls_temp) THEN
				lb_numeric = True
			End if
		CASE ELSE
			IF Not IsNumber(ls_temp) THEN
				If Len(Data) > 1 Then
					If lu_string.nf_IsEmpty(ls_temp) or ls_temp = ':' Then
						//do no not
					Else
						wf_error_handling('','severe','duration','Must be numeric, spaces, or a Colon')
						beep(1)
						idw_active_dw.SelectText ( Len(data) , 1)
						Return  1
					End if
				Else
					If not lu_string.nf_IsEmpty(ls_temp) Then
						If ls_temp = ':' Then 
							wf_error_handling('','severe','duration','Must have a hour(number) before the colon')
						Else	
							wf_error_handling('','severe','duration','Must be numeric, spaces, or a colon')
						End If
						beep(1)
						idw_active_dw.SelectText ( 1, 1)
						Return  1
				End if
			End If
		END IF
	END CHOOSE
End If

IF lb_numeric then
   wf_error_handling('','severe','duration','Must be numeric')
	beep(1)
	idw_active_dw.SelectText ( Len(data) , 1)
	Return  1
End IF

Return 0
end event

event getfocus;idw_active_dw = This
end event

event itemchanged;call super::itemchanged;Time			lt_time,&
				lt_min,&
				lt_comp, &
				lt_temp_above, &
				lt_temp_time, &
				lt_temp_below, &
				lt_temp
				
Boolean		lb_time, &
				lb_dura, &
				lb_data_empty, &
				lb_column_above_empty, &
				lb_column_below_empty, &
				lb_temp_above, &
				lb_temp_below, &
				lb_error,&
				lb_min_error,&
				lb_more_mess, &
				lb_Invalid
				
long			ll_error, &
				ll_len,&
				ll_chain,&
				ll_count,&
				ll_time,&
				ll_temp_time,&
				ll_comp

string		ls_name,&
				ls_name1,&
				ls_modify,&
				ls_right,&
				ls_shift,&
				ls_time, &
				ls_temp,&
				ls_temp_above,&
				ls_temp_below,&
				ls_compare_byte
				
nvuo_pa_business_rules 	nvuo_pa
u_string_functions		u_string


// I did this to make the code more generic
ls_name = dwo.name
ls_right = Right(ls_name,2)
ls_name1 = Left(ls_name, Len(ls_name) - 2)
ll_len = Len(data)


wf_error_handling('','','ready','')

CHOOSE CASE ls_name1
	CASE "start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			ls_shift = dw_shift.uf_get_shift() 
			lt_min	= nvuo_pa.uf_check_min_shift_start_time(ls_shift)
 			If lt_time < lt_min Then
				wf_error_handling(ls_name,'severe','shiftmin' + ls_shift + string(lt_min),'')
				beep(1)
				Return 1
				lb_error = True
			End If
			lb_time = True
		Else
			lb_data_empty = True
		End If

	CASE "break_1_start_time"
		wf_mod_time_fields(ls_right)
  		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "break_2_start_time"
		wf_mod_time_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			lt_time = Time(ls_temp)
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
			lb_time = True
		Else
			lb_data_empty = True
		End If
		
	CASE "work_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_time = SecondsAfter (Time('00:00:00'), lt_time )
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationgreat','')
				lb_error = True
			End  If
		Else
			lb_data_empty = True
		End If

	CASE "break_1_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End IF
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'break_1_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The 1st Break Duration change has exceeded the time set for Meal Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If
		
	CASE "meal_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_2_duration' + ls_right)))
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
			If lb_error Then
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','','')
					End If
				End If
			Else
				If Not u_string.nf_IsEmpty(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right)) Then 
					lt_temp_below = Time(idw_active_dw.GetItemString( 1, 'break_2_start_time' + ls_right))
					lt_temp_above = Time(idw_active_dw.GetItemString( 1, 'meal_start_time' + ls_right)) 
					ll_comp = SecondsAfter(Time('00:00:00'),lt_time)
					lt_comp = RelativeTime(lt_temp_above,ll_comp)
					If lt_comp >= lt_temp_below Then
						wf_error_handling(ls_name,'caution','duration','The Meal Duration change has exceeded the time set for the 2nd Break Start Time')
					End If
				End If
			End If
		Else
			lb_data_empty = True
		End If

	CASE "break_2_duration"
		wf_mod_dura_fields(ls_right)
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp) 
			ll_temp_time = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'work_duration' + ls_right)))
			ll_comp = SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'break_1_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),Time(idw_active_dw.GetItemString( 1, 'meal_duration' + ls_right)))
			ll_comp += SecondsAfter(Time('00:00:00'),lt_time)
			If ll_temp_time < ll_comp Then
				wf_error_handling(ls_name,'caution','durationless','')
				lb_error =  True
			End  If
		Else
			lb_data_empty = True
		End If
		
	CASE "shipping_lag"
		If not u_string.nf_IsEmpty(data) Then
			ls_temp = data
			ll_error = nvuo_pa.uf_check_5_char_time(ls_temp)
			If ll_error > 0 Then
				wf_error_handling(ls_name,'severe','invalidtime','')
				beep(1)
				Return 1
			End If
			lt_time = Time(ls_temp)
		Else
			lb_data_empty = True
		End If
		
	CASE "chain_speed"
		ll_chain = Sign(Long(data))
		If ll_chain < 0 Then
			wf_error_handling(ls_name,'severe','chainneg','')
			beep(1)
			Return 1
		End If	
		ll_chain = nvuo_pa.uf_check_max_chain_speed()
		If Long(Data) > ll_chain Then
			wf_error_handling(ls_name + String(ll_chain),'severe','chain','')
			Beep (1)
			Return 1
		Else
			wf_error_handling(ls_name,'noerror','ready','')
		End If
	CASE "load_crew"
			lb_data_empty = True
End Choose							

If Not lb_data_empty Then
	ls_time = String(lt_time, 'hh:mm')
	This.Event Post ue_postitemchanged(ls_name,ls_time)
End If			
	
If	lb_time Then
	POST wf_process_time_fields(ls_right,lb_error)
End If	

return 0

end event

event itemerror;call super::itemerror;Return 2
end event

