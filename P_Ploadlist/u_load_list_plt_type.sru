HA$PBExportHeader$u_load_list_plt_type.sru
forward
global type u_load_list_plt_type from u_netwise_dw
end type
end forward

global type u_load_list_plt_type from u_netwise_dw
integer width = 1303
integer height = 96
string dataobject = "d_plant_type_inq"
boolean border = false
event ue_graph pbm_custom01
event ue_graph_type pbm_custom02
event ue_graph_name pbm_custom03
event ue_graph_data pbm_custom04
event ue_revisions ( )
end type
global u_load_list_plt_type u_load_list_plt_type

forward prototypes
public function string uf_get_plant_type ()
public subroutine uf_disable ()
public subroutine uf_enable ()
end prototypes

on ue_graph;call u_netwise_dw::ue_graph;//w_graph		w_grp_child

//OpenSheetWithParm( w_grp_child, this, iw_frame, 0, original! )
end on

event ue_graph_type;call super::ue_graph_type;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_type, lst_graph_axis.dw, iw_frame )
end event

event ue_graph_name;call super::ue_graph_name;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_name, lst_graph_axis, iw_frame )
end event

event ue_graph_data;call super::ue_graph_data;//st_graph_axis		lst_graph_axis

//lst_graph_axis.dw	=	this

//OpenWithParm( w_response_graph_data, lst_graph_axis, iw_frame )
end event

event ue_revisions;/*****************************************************************
**   REVISION NUMBER: rev#01
**   PROJECT NUMBER:  support
**   DATE:				 july 99            
**   PROGRAMMER:      David Deal
**   PURPOSE:         added code in the constructor and ue_keydown
**                    event to write and check the ibpuser.ini
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************
**   REVISION NUMBER: 
**   PROJECT NUMBER:  
**   DATE:            
**   PROGRAMMER:      
**   PURPOSE:         
******************************************************************/
end event

public function string uf_get_plant_type ();return Trim(This.GetItemString(1, "plant_type"))
end function

public subroutine uf_disable ();This.object.plant_type.Background.Color = 12632256
This.object.plant_type.Protect = 1

end subroutine

public subroutine uf_enable ();This.object.plant_type.Background.Color = 16777215
This.object.plant_type.Protect = 0

end subroutine

event constructor;call super::constructor;String				ls_text
DataWindowChild	ldwc_type

ib_Updateable = false

This.GetChild("plant_type", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("PLTGROUP")

This.InsertRow(0)

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "Lastplanttype","")

if ls_text = '' then
	ls_text = String('            ')
end if

This.SetItem( 1, "plant_type", ls_text)
end event

event ue_keydown;call super::ue_keydown;Long	ll_CurrentRow, &
		ll_CurrentColumn

String	ls_ColumnType, &
			ls_ColumnName, &
			ls_Text,&
			ls_value
			
// Get the current row and column that has focus
ll_CurrentRow = This.GetRow()
ll_CurrentColumn = This.GetColumn()
		
// Get the current column name
ls_ColumnName = GetColumnName()

// Get the column type of the clicked field	
ls_ColumnType = Lower(This.Describe("#" + String( &
									ll_CurrentColumn) + ".ColType"))

ls_Text = GetText()

Choose Case ls_ColumnType
	Case "date", "datetime"
		//rev#01 added if
		ls_value = ProfileString('ibpuser.ini', "PAS",'u_base_dw_ext.ue_keydown', "None") 
		If ls_value = 'Y'	Then
			Choose Case True
				Case KeyDown(KeyDownArrow!)
					SetText(String(RelativeDate(Date(ls_Text),-1)))				
				Case KeyDown(KeyUpArrow!)
					SetText(String(RelativeDate(Date(ls_Text),1)))			
			End Choose
		End If
End Choose
end event

event itemfocuschanged;call super::itemfocuschanged;This.SelectText(1, 100)
end event

on u_load_list_plt_type.create
end on

on u_load_list_plt_type.destroy
end on

event itemchanged;call super::itemchanged;SetProfileString( iw_frame.is_UserINI, "Pas", "lastplanttype",trim(data))

end event

