HA$PBExportHeader$u_start_time.sru
forward
global type u_start_time from datawindow
end type
end forward

global type u_start_time from datawindow
int Width=462
int Height=92
int TabOrder=10
string DataObject="d_start_time"
boolean Border=false
boolean LiveScroll=true
end type
global u_start_time u_start_time

forward prototypes
public function string uf_get_start_time ()
public subroutine uf_set_start_time (string as_start_time)
public subroutine uf_enable ()
public subroutine uf_disable ()
end prototypes

public function string uf_get_start_time ();return String(This.GetItemTime(1, "start_time"), 'hhmm') 
end function

public subroutine uf_set_start_time (string as_start_time);This.SetItem(1, "start_time", Time(as_start_time))
This.AcceptText()
end subroutine

public subroutine uf_enable ();This.object.start_time.Background.Color = 16777215
This.object.start_time.Protect = 0

end subroutine

public subroutine uf_disable ();This.object.start_time.Background.Color = 12632256
This.object.start_time.Protect = 1

end subroutine

event constructor;String	ls_text

This.InsertRow(0)

//ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "Laststarttime","")
//
//if ls_text = '' then
//	ls_text = '06:00:00:000000'
//end if
//
//This.SetItem( 1, "start_time", Time(ls_text))
//
end event

event itemchanged;SetProfileString( iw_frame.is_UserINI, "Pas", "Laststarttime",trim(data))

end event

event getfocus;This.SelectText(1, Len(This.GetText()))
end event

