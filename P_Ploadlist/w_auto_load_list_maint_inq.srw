HA$PBExportHeader$w_auto_load_list_maint_inq.srw
forward
global type w_auto_load_list_maint_inq from w_base_response_ext
end type
type dw_division from u_division within w_auto_load_list_maint_inq
end type
type dw_plant from u_plant within w_auto_load_list_maint_inq
end type
type dw_load_list_type from u_load_list_type within w_auto_load_list_maint_inq
end type
type dw_ship_date from u_ship_date within w_auto_load_list_maint_inq
end type
type dw_calculate_load_times from u_calculate_load_times within w_auto_load_list_maint_inq
end type
type dw_start_time from u_start_time within w_auto_load_list_maint_inq
end type
type dw_loads_per_hour from u_loads_per_hour within w_auto_load_list_maint_inq
end type
type dw_load_list_by_option from u_load_list_by_option within w_auto_load_list_maint_inq
end type
type dw_load_list_complex from u_load_list_complex within w_auto_load_list_maint_inq
end type
type dw_plant_type from u_load_list_plt_type within w_auto_load_list_maint_inq
end type
type dw_load_list_plt_descr from u_base_dw_ext within w_auto_load_list_maint_inq
end type
type dw_grind_other_start_load_times from u_grind_other_start_load_times within w_auto_load_list_maint_inq
end type
end forward

global type w_auto_load_list_maint_inq from w_base_response_ext
integer x = 133
integer y = 388
integer width = 2441
integer height = 1068
string title = "Auto Load List Response "
boolean controlmenu = false
long backcolor = 12632256
event ue_enabledisable ( string as_load_option )
dw_division dw_division
dw_plant dw_plant
dw_load_list_type dw_load_list_type
dw_ship_date dw_ship_date
dw_calculate_load_times dw_calculate_load_times
dw_start_time dw_start_time
dw_loads_per_hour dw_loads_per_hour
dw_load_list_by_option dw_load_list_by_option
dw_load_list_complex dw_load_list_complex
dw_plant_type dw_plant_type
dw_load_list_plt_descr dw_load_list_plt_descr
dw_grind_other_start_load_times dw_grind_other_start_load_times
end type
global w_auto_load_list_maint_inq w_auto_load_list_maint_inq

type variables
Boolean									ib_valid_return
u_pas201									iu_pas201
s_error									istr_error_info
u_ws_pas5								iu_ws_pas5
end variables

forward prototypes
public subroutine wf_get_start_time ()
end prototypes

event ue_enabledisable(string as_load_option);String	ls_calc_times

ls_calc_times = dw_calculate_load_times.uf_get_calculate_load_times_ind()

//if ls_calc_times = 'Y' then
//	dw_start_time.uf_Enable()
//	dw_loads_per_hour.uf_Enable()
//else
//	dw_start_time.uf_Disable()
//	dw_loads_per_hour.uf_Disable()
//end if

if as_load_option = 'P' then
//	Plant
	dw_plant.Enable()
	dw_plant.taborder = 20

//	Division
	dw_division.Enable()
	dw_division.taborder = 30

//	Complex
	dw_load_list_complex.setitem(1, "complex_code", ' ')
	dw_load_list_complex.uf_Disable()
	dw_load_list_complex.taborder = 0

//	Plant Type
	dw_plant_type.setitem(1, "plant_type", ' ')
	dw_plant_type.uf_Disable()
	dw_plant_type.taborder = 0
else

//	Plant
	dw_plant.setitem(1, "location_code", ' ')
	dw_plant.setitem(1, "location_name", ' ')
	dw_plant.Disable()
	dw_plant.taborder = 0

//	Division
	dw_division.setitem(1, "division_code", ' ')
	dw_division.setitem(1, "division_description", ' ')
	dw_division.Disable()
	dw_division.taborder = 0

//	Complex
	dw_load_list_complex.uf_Enable()
	dw_load_list_complex.taborder = 20

//	Plant Type
	dw_plant_type.uf_enable()
	dw_plant_type.taborder = 30
end if

end event

public subroutine wf_get_start_time ();String	ls_division_code, &
			ls_location_code, &
			ls_ship_date, &
			ls_load_list_type, &
			ls_calculate_load_times_ind, &
			ls_start_time, &
			ls_input, &
			ls_output, &
			ls_formatted_start_time, &
			ls_load_list_opt, &
			ls_complex_code, &
			ls_plant_type, &
			ls_plant_code
			
Integer	li_loads_per_hour, li_rtn
Long		ll_pos			
			
u_string_functions	lu_string_functions			

if dw_load_list_by_option.accepttext() = -1 then return
If dw_load_list_type.AcceptText() = -1 then return
If dw_ship_date.AcceptText() = -1 then return
If dw_calculate_load_times.AcceptText() = -1 then return
If dw_loads_per_hour.AcceptText() = -1 then return
if dw_load_list_complex.accepttext() = -1 then return
if dw_plant_type.accepttext() = -1 then return

ls_load_list_opt = dw_load_list_by_option.uf_get_load_list_option()
ls_division_code = dw_division.uf_get_division()
ls_location_code = dw_plant.uf_get_plant_code()
ls_ship_date = String(dw_ship_date.uf_get_ship_date(), 'mm/dd/yyyy')
ls_load_list_type = dw_load_list_type.uf_get_load_list_type()
ls_calculate_load_times_ind = dw_calculate_load_times.uf_get_calculate_load_times_ind()
ls_start_time = dw_start_time.uf_get_start_time()
li_loads_per_hour = dw_loads_per_hour.uf_get_loads_per_hour()
ls_complex_code = dw_load_list_complex.uf_get_complex_code()
ls_plant_type = dw_plant_type.uf_get_plant_type()

ls_input = ls_location_code + '~t' + &
			  ls_ship_date + '~t' + &
			  ls_load_list_type + '~t' + &
			  ls_division_code + '~t' + &
			  ls_calculate_load_times_ind + '~t' + &
			  ls_start_time + '~t' + &
			  String(li_loads_per_hour) + '~t' + & 
			  ls_load_list_opt + '~t' + &
			  ls_complex_code + '~t' + & 
			  ls_plant_type + '~r~n'
			  
istr_error_info.se_event_name = "wf_get_start_time"			  

//li_rtn = iu_pas201.nf_pasp80br_inq_load_list_except(istr_error_info, ls_input, ls_output)
li_rtn = iu_ws_pas5.nf_pasp80fr(istr_error_info, ls_input, ls_output)

ll_pos = lu_string_functions.nf_npos(ls_output, '~t', 1, 5) + 1

ls_formatted_start_time = mid(ls_output, ll_pos, 15)
dw_start_time.uf_set_start_time(ls_formatted_start_time)

dw_load_list_plt_descr.reset()
ll_pos = lu_string_functions.nf_npos(ls_output, '~t', 1 , 10) + 1
ls_plant_code = mid(ls_output, ll_pos)
dw_load_list_plt_descr.importstring(ls_plant_code)


end subroutine

on w_auto_load_list_maint_inq.create
int iCurrent
call super::create
this.dw_division=create dw_division
this.dw_plant=create dw_plant
this.dw_load_list_type=create dw_load_list_type
this.dw_ship_date=create dw_ship_date
this.dw_calculate_load_times=create dw_calculate_load_times
this.dw_start_time=create dw_start_time
this.dw_loads_per_hour=create dw_loads_per_hour
this.dw_load_list_by_option=create dw_load_list_by_option
this.dw_load_list_complex=create dw_load_list_complex
this.dw_plant_type=create dw_plant_type
this.dw_load_list_plt_descr=create dw_load_list_plt_descr
this.dw_grind_other_start_load_times=create dw_grind_other_start_load_times
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_division
this.Control[iCurrent+2]=this.dw_plant
this.Control[iCurrent+3]=this.dw_load_list_type
this.Control[iCurrent+4]=this.dw_ship_date
this.Control[iCurrent+5]=this.dw_calculate_load_times
this.Control[iCurrent+6]=this.dw_start_time
this.Control[iCurrent+7]=this.dw_loads_per_hour
this.Control[iCurrent+8]=this.dw_load_list_by_option
this.Control[iCurrent+9]=this.dw_load_list_complex
this.Control[iCurrent+10]=this.dw_plant_type
this.Control[iCurrent+11]=this.dw_load_list_plt_descr
this.Control[iCurrent+12]=this.dw_grind_other_start_load_times
end on

on w_auto_load_list_maint_inq.destroy
call super::destroy
destroy(this.dw_division)
destroy(this.dw_plant)
destroy(this.dw_load_list_type)
destroy(this.dw_ship_date)
destroy(this.dw_calculate_load_times)
destroy(this.dw_start_time)
destroy(this.dw_loads_per_hour)
destroy(this.dw_load_list_by_option)
destroy(this.dw_load_list_complex)
destroy(this.dw_plant_type)
destroy(this.dw_load_list_plt_descr)
destroy(this.dw_grind_other_start_load_times)
end on

event open;call super::open;String	ls_calculate_ld_times_opt, ls_load_list_opt

iw_parentwindow = Message.PowerObjectParm
iu_pas201 = Create u_pas201
iu_ws_pas5	= Create u_ws_pas5

If IsValid(iw_parentwindow) Then
	iw_parentwindow.Event ue_get_data('calculate_load_times_ind')
	ls_calculate_ld_times_opt = Message.StringParm
Else
	ls_calculate_ld_times_opt = 'N'
End If

dw_calculate_load_times.uf_set_calculate_load_times_ind(ls_calculate_ld_times_opt)
//ls_calculate_ld_times_opt = dw_calculate_load_times.GetItemString(1, 'calculate_load_times_ind')

//  RevGLL  //
If ls_calculate_ld_times_opt = 'N' Then
	dw_start_time.Modify("start_time.Visible=0")
	dw_start_time.Modify("start_time_t.Visible=0")
	dw_loads_per_hour.Modify("loads_per_hour.Visible=0")
	dw_loads_per_hour.Modify("loads_per_hour_t.Visible=0")
	dw_grind_other_start_load_times.Modify("grind_start_time_t.Visible=0")
	dw_grind_other_start_load_times.Modify("grind_load_time_t.Visible=0")
	dw_grind_other_start_load_times.Modify("other_start_time_t.Visible=0")
	dw_grind_other_start_load_times.Modify("other_load_time_t.Visible=0")
	dw_grind_other_start_load_times.Modify("grind_start_time.Visible=0")
	dw_grind_other_start_load_times.Modify("grind_load_time.Visible=0")
	dw_grind_other_start_load_times.Modify("other_start_time.Visible=0")
	dw_grind_other_start_load_times.Modify("other_load_time.Visible=0")
Else
	If ls_calculate_ld_times_opt = 'W' Then
		dw_start_time.Modify("start_time.Visible=1")
		dw_start_time.Modify("start_time_t.Visible=1")
		dw_loads_per_hour.Modify("loads_per_hour.Visible=1")
		dw_loads_per_hour.Modify("loads_per_hour_t.Visible=1")
		dw_grind_other_start_load_times.Modify("grind_start_time_t.Visible=0")
		dw_grind_other_start_load_times.Modify("grind_load_time_t.Visible=0")
		dw_grind_other_start_load_times.Modify("other_start_time_t.Visible=0")
		dw_grind_other_start_load_times.Modify("other_load_time_t.Visible=0")
		dw_grind_other_start_load_times.Modify("grind_start_time.Visible=0")
		dw_grind_other_start_load_times.Modify("grind_load_time.Visible=0")
		dw_grind_other_start_load_times.Modify("other_start_time.Visible=0")
		dw_grind_other_start_load_times.Modify("other_load_time.Visible=0")
	Else
		If ls_calculate_ld_times_opt = 'C' Then
			dw_start_time.Modify("start_time.Visible=0")
			dw_start_time.Modify("start_time_t.Visible=0")
			dw_loads_per_hour.Modify("loads_per_hour.Visible=0")
			dw_loads_per_hour.Modify("loads_per_hour_t.Visible=0")
			dw_grind_other_start_load_times.Modify("grind_start_time_t.Visible=1")
			dw_grind_other_start_load_times.Modify("grind_load_time_t.Visible=1")
			dw_grind_other_start_load_times.Modify("other_start_time_t.Visible=1")
			dw_grind_other_start_load_times.Modify("other_load_time_t.Visible=1")
			dw_grind_other_start_load_times.Modify("grind_start_time.Visible=1")
			dw_grind_other_start_load_times.Modify("grind_load_time.Visible=1")
			dw_grind_other_start_load_times.Modify("other_start_time.Visible=1")
			dw_grind_other_start_load_times.Modify("other_load_time.Visible=1")
		End If	
	End If
End If
//

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "Cancel")
end event

event ue_base_ok();call super::ue_base_ok;String 	ls_location_code, &
			ls_location_text, &
			ls_division_code, &
			ls_division_text, &
			ls_complex_code, &
			ls_plant_type, &
			ls_load_list_opt, &
			ls_load_list_type, &
			ls_ship_date, &
			ls_calculate_load_times_ind, &
			ls_start_time, &
			ls_grind_start_time, &
			ls_other_start_time

Integer	li_loads_per_hour, li_grind_load_time, li_other_load_time
date		date1, date2
integer	li_rtn

u_string_functions		lu_strings

If dw_division.AcceptText() = -1 then return
If dw_load_list_type.AcceptText() = -1 then return
If dw_plant.AcceptText() = -1 then return
if dw_load_list_complex.accepttext( ) = -1 then return
if dw_plant_type.accepttext() = -1 then return
if dw_load_list_by_option.accepttext() = -1 then return
If dw_ship_date.AcceptText() = -1 then return
If dw_calculate_load_times.AcceptText() = -1 then return
If dw_start_time.AcceptText() = -1 then return
If dw_loads_per_hour.AcceptText() = -1 then return
If dw_grind_other_start_load_times.AcceptText() = -1 Then Return

ls_load_list_opt = dw_load_list_by_option.uf_get_load_list_option( )

if ls_load_list_opt = "P" then
	ls_location_code = dw_plant.uf_get_plant_code()
	if ls_location_code <= '   ' then
		iw_frame.SetMicroHelp( "Plant code is a required field.")
		dw_plant.SetFocus()
		return
	end if
	ls_location_text = dw_plant.uf_get_plant_descr()
	ls_division_code = dw_division.uf_get_division()
	if ls_division_code <= '  ' then
		iw_frame.SetMicroHelp( "Division is a required field.")
		dw_division.SetFocus()
		dw_division.Setcolumn(1)
		dw_division.SetRow(1)
		return
	end if
	ls_division_text = dw_division.uf_get_division_descr()
else
	ls_complex_code = dw_load_list_complex.uf_get_complex_code( )
	if ls_complex_code <= '  ' then
		iw_frame.setmicrohelp( "Complex Code is a required field.")
		dw_load_list_complex.setfocus()
		dw_load_list_complex.setcolumn(1)
		dw_load_list_complex.setrow(1)
		return
	end if
	ls_plant_type = dw_plant_type.uf_get_plant_type( )
	if ls_plant_type <= '  ' then
		iw_frame.setmicrohelp( "Plant Type is a required Field.")
		dw_plant_type.setfocus()
		dw_plant_type.setcolumn(1)
		dw_plant_type.setrow(1)
		return
	end if
end if

//revgll
ls_ship_date = String(dw_ship_date.uf_get_ship_date(), 'mm/dd/yyyy')
If lu_strings.nf_IsEmpty(ls_ship_date) Then
	iw_frame.SetMicroHelp("Ship Date is a required field")
	dw_ship_date.SetFocus()
	Return
End if
//
//revgll
date1 = Today()
date2 = dw_ship_date.uf_get_ship_date()

li_rtn = Daysafter(date1, date2)
if li_rtn > 7 then
	iw_frame.setmicrohelp("Ship date is greater than seven days, correct the ship date")
	dw_ship_date.setfocus()
	return
end if
//
ls_load_list_type = dw_load_list_type.uf_get_load_list_type()

//revgll
ls_calculate_load_times_ind = dw_calculate_load_times.uf_get_calculate_load_times_ind()
If ls_calculate_load_times_ind = 'W' then
	ls_start_time = dw_start_time.uf_get_start_time()
	If lu_strings.nf_IsEmpty(ls_start_time) or (ls_start_time = '0000') Then
		iw_frame.SetMicroHelp("Start Time is a required field")
		dw_start_time.SetFocus()
		Return
	end if
End if
//

li_loads_per_hour = dw_loads_per_hour.uf_get_loads_per_hour()

//  RevGLL  //
ls_grind_start_time = dw_grind_other_start_load_times.uf_get_grind_start_time()
li_grind_load_time = dw_grind_other_start_load_times.uf_get_grind_load_time()
ls_other_start_time = dw_grind_other_start_load_times.uf_get_other_start_time()
li_other_load_time = dw_grind_other_start_load_times.uf_get_other_load_time()
//

If ls_calculate_load_times_ind = 'N' Then
	ls_grind_start_time = ''
	ls_other_start_time = ''
	ls_start_time = ''
	li_grind_load_time = 0
	li_other_load_time = 0
	li_loads_per_hour = 0
Else
	If ls_calculate_load_times_ind = 'W' Then
		ls_grind_start_time = ''
		ls_other_start_time = ''
		li_grind_load_time = 0
		li_other_load_time = 0
	Else
		If ls_calculate_load_times_ind = 'C' Then
			ls_start_time = ''
			li_loads_per_hour = 0
		End If
	End If
End If
			

iw_parentwindow.Event ue_set_data('location_code',ls_location_code)
iw_parentwindow.Event ue_set_data('location_text',ls_location_text)
iw_parentwindow.Event ue_set_data('division_code',ls_division_code)
iw_parentwindow.Event ue_set_data('division_text',ls_division_text)
iw_parentwindow.Event ue_set_data('load_list_type',ls_load_list_type)
iw_parentwindow.Event ue_set_data('ship_date',ls_ship_date)
iw_parentwindow.Event ue_set_data('calculate_load_times_ind',ls_calculate_load_times_ind)
iw_parentwindow.Event ue_set_data('start_time',ls_start_time)
iw_parentwindow.Event ue_set_data('loads_per_hour',String(li_loads_per_hour))
iw_parentwindow.event ue_set_data('load_list_by_option', ls_load_list_opt)
iw_parentwindow.event ue_set_data('complex_code', ls_complex_code)
iw_parentwindow.event ue_set_data('plant_type', ls_plant_type)
iw_parentwindow.event ue_set_data('grind_start_time' , ls_grind_start_time)
iw_parentwindow.event ue_set_data('grind_load_time' , String(li_grind_load_time))
iw_parentwindow.event ue_set_data('other_start_time' , ls_other_start_time)
iw_parentwindow.event ue_set_data('other_load_time' , String(li_other_load_time))

ib_valid_return = True

CloseWithReturn(This, "OK")
end event

event ue_postopen;call super::ue_postopen;String	ls_load_list_opt

ls_load_list_opt = dw_load_list_by_option.uf_get_load_list_option()
This.Event ue_enabledisable(ls_load_list_opt)

if ls_load_list_opt = 'P' then
	dw_plant.SetFocus()
	dw_plant.SetColumn('location_code')
else
	dw_load_list_complex.SetFocus()
	dw_load_list_complex.setcolumn( 'complex_code')
	dw_load_list_complex.SelectText(1, 3)
end if

dw_calculate_load_times.AcceptText()
dw_grind_other_start_load_times.AcceptText()
This.wf_get_start_time()

end event

event close;call super::close;If IsValid(iu_pas201) Then Destroy iu_Pas201

If Not ib_valid_return Then
	CloseWithReturn(This, "Cancel")
End if
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_auto_load_list_maint_inq
integer x = 2121
integer y = 272
integer taborder = 130
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_auto_load_list_maint_inq
integer x = 2121
integer y = 148
integer taborder = 120
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_auto_load_list_maint_inq
integer x = 2121
integer y = 20
integer taborder = 100
end type

type dw_division from u_division within w_auto_load_list_maint_inq
integer x = 663
integer y = 112
integer width = 1426
integer taborder = 30
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;Long	ll_row

String	ls_text, &
			ls_description
integer	rtncode
datawindowchild	ldw_division

ls_text = This.GetText()
If Len(Trim(ls_text)) = 0 Then 
	This.SetItem(1, "division_description", "")	
	return
End if

rtncode = dw_division.getchild('division_code', ldw_division)
ll_row = ldw_division.Find('Trim(type_code) = "' + ls_text + '"', 1, ldw_division.RowCount())

If ll_row <= 0 Then
	gw_netwise_Frame.SetMicroHelp(ls_text + " is an Invalid Division Code")
	ls_description = ""
	This.SetItem(1, "division_description", ls_Description)
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	return 1
Else
	This.SetFocus()
	This.SelectText(1, Len(ls_text))
	ls_Description = ldw_division.GetItemString(ll_row, "type_desc")
	gw_netwise_frame.SetMicroHelp("Ready")
End if

This.SetItem(1, "division_description", ls_Description)

end event

type dw_plant from u_plant within w_auto_load_list_maint_inq
integer x = 704
integer y = 24
integer width = 1385
integer taborder = 20
boolean bringtotop = true
boolean minbox = true
boolean livescroll = false
end type

event itemchanged;Long					ll_row
datawindowchild	ldw_plant
integer 				rtncode

If Len(Trim(data)) = 0 Then 
	// User entered an empty plant code -- set fields to blank
	// The last row is an empty row
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
	This.ScrollToRow(This.RowCount())
	This.SetItem(1,"location_code","")
	This.SetItem(1,"location_name","")
	return 
End if

if isnull (data) then
	iw_Frame.SetMicroHelp("Plant Code is invalid")
	This.SetFocus()
	This.SelectText(1, Len(data))
	return 1
End if


rtncode = dw_plant.getchild('location_code', ldw_plant)

// Find the entered plant code in the table -- don't count the last row, it's blank
ll_row = ldw_plant.Find('location_code = "' + Trim(data) + '"', 1, ldw_plant.RowCount())
If ll_row <= 0 Then
	iw_Frame.SetMicroHelp(data + " is an Invalid Plant Code")
	This.SetFocus()
	This.SelectText(1, Len(data))
	return 1
Else
	This.SetFocus()
	This.SelectText(1, Len(data))
	This.SetItem(1, 'location_name', ldw_plant.GetItemString(ll_row, 'location_name'))
	//SetProfileString( iw_frame.is_UserINI, "Pas", "Lastplant",Trim(data))
	SetProfileString( iw_frame.is_UserINI, "Pas", "LastLoadListPlant",Trim(data))
	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
End if

return 
Parent.wf_get_start_time()

end event

event constructor;call super::constructor;String	ls_last_load_list_plant, &
			ls_last_plant

ls_last_load_list_plant = ProfileString( iw_frame.is_UserINI, "Pas", "LastLoadListPlant","")
ls_last_plant = ProfileString( iw_frame.is_UserINI, "Pas", "Lastplant","")


If IsNull(ls_last_load_list_plant) Then
	ls_last_load_list_plant = ls_last_plant
End If
	
This.SetItem( 1, "location_code", ls_last_load_list_plant)

end event

type dw_load_list_type from u_load_list_type within w_auto_load_list_maint_inq
boolean visible = false
integer x = 1499
integer y = 116
integer taborder = 0
boolean bringtotop = true
end type

type dw_ship_date from u_ship_date within w_auto_load_list_maint_inq
integer x = 18
integer y = 460
integer taborder = 60
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;String 	ls_ship_date

Date		date1, &
			date2

Integer	li_rtn

u_string_functions		lu_strings

If dw_ship_date.AcceptText() = -1 then return

ls_ship_date = String(dw_ship_date.uf_get_ship_date(), 'mm/dd/yyyy')
If lu_strings.nf_IsEmpty(ls_ship_date) Then
	iw_frame.SetMicroHelp("Ship Date is a required field")
	dw_ship_date.SetFocus()
	Return
End if

//revgll
date1 = Today()
date2 = dw_ship_date.uf_get_ship_date()

li_rtn = Daysafter(date1, date2)
if li_rtn > 7 then
	iw_frame.setmicrohelp("Ship date is greater than seven days, correct the ship date")
	dw_ship_date.setfocus()
	return 1
end if
//

Parent.wf_get_start_time()
end event

event itemerror;call super::itemerror;return 2
end event

type dw_calculate_load_times from u_calculate_load_times within w_auto_load_list_maint_inq
integer x = 1362
integer y = 408
integer width = 1019
integer taborder = 70
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;String	ls_load_list_opt, ls_calculate_ld_times_opt

dw_calculate_load_times.uf_set_calculate_load_times_ind(data)
ls_calculate_ld_times_opt = dw_calculate_load_times.GetItemString(1, 'calculate_load_times_ind')
ls_load_list_opt = dw_load_list_by_option.uf_get_load_list_option()
Parent.Event ue_enabledisable (ls_load_list_opt)

//  RevGLL  //
If ls_calculate_ld_times_opt = 'N' Then
	dw_start_time.Modify("start_time.Visible=0")
	dw_start_time.Modify("start_time_t.Visible=0")
	dw_loads_per_hour.Modify("loads_per_hour.Visible=0")
	dw_loads_per_hour.Modify("loads_per_hour_t.Visible=0")
	dw_grind_other_start_load_times.Modify("grind_start_time_t.Visible=0")
	dw_grind_other_start_load_times.Modify("grind_load_time_t.Visible=0")
	dw_grind_other_start_load_times.Modify("other_start_time_t.Visible=0")
	dw_grind_other_start_load_times.Modify("other_load_time_t.Visible=0")
	dw_grind_other_start_load_times.Modify("grind_start_time.Visible=0")
	dw_grind_other_start_load_times.Modify("grind_load_time.Visible=0")
	dw_grind_other_start_load_times.Modify("other_start_time.Visible=0")
	dw_grind_other_start_load_times.Modify("other_load_time.Visible=0")
Else
	If ls_calculate_ld_times_opt = 'W' Then
		dw_start_time.Modify("start_time.Visible=1")
		dw_start_time.Modify("start_time_t.Visible=1")
		dw_loads_per_hour.Modify("loads_per_hour.Visible=1")
		dw_loads_per_hour.Modify("loads_per_hour_t.Visible=1")
		dw_grind_other_start_load_times.Modify("grind_start_time_t.Visible=0")
		dw_grind_other_start_load_times.Modify("grind_load_time_t.Visible=0")
		dw_grind_other_start_load_times.Modify("other_start_time_t.Visible=0")
		dw_grind_other_start_load_times.Modify("other_load_time_t.Visible=0")
		dw_grind_other_start_load_times.Modify("grind_start_time.Visible=0")
		dw_grind_other_start_load_times.Modify("grind_load_time.Visible=0")
		dw_grind_other_start_load_times.Modify("other_start_time.Visible=0")
		dw_grind_other_start_load_times.Modify("other_load_time.Visible=0")
	Else
		If ls_calculate_ld_times_opt = 'C' Then
			dw_start_time.Modify("start_time.Visible=0")
			dw_start_time.Modify("start_time_t.Visible=0")
			dw_loads_per_hour.Modify("loads_per_hour.Visible=0")
			dw_loads_per_hour.Modify("loads_per_hour_t.Visible=0")
			dw_grind_other_start_load_times.Modify("grind_start_time_t.Visible=1")
			dw_grind_other_start_load_times.Modify("grind_load_time_t.Visible=1")
			dw_grind_other_start_load_times.Modify("other_start_time_t.Visible=1")
			dw_grind_other_start_load_times.Modify("other_load_time_t.Visible=1")
			dw_grind_other_start_load_times.Modify("grind_start_time.Visible=1")
			dw_grind_other_start_load_times.Modify("grind_load_time.Visible=1")
			dw_grind_other_start_load_times.Modify("other_start_time.Visible=1")
			dw_grind_other_start_load_times.Modify("other_load_time.Visible=1")
		End If	
	End If
End If
//

end event

type dw_start_time from u_start_time within w_auto_load_list_maint_inq
integer x = 1358
integer y = 712
integer width = 434
integer taborder = 80
boolean bringtotop = true
end type

type dw_loads_per_hour from u_loads_per_hour within w_auto_load_list_maint_inq
integer x = 1797
integer y = 712
integer width = 594
integer taborder = 90
boolean bringtotop = true
end type

type dw_load_list_by_option from u_load_list_by_option within w_auto_load_list_maint_inq
integer x = 18
integer y = 28
integer taborder = 10
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;String	ls_load_list_opt

Parent.Event ue_enabledisable(data)
Parent.Post wf_get_start_time()

ls_load_list_opt = dw_load_list_by_option.uf_get_load_list_option()

if data = 'P' then
	dw_plant.SetFocus()
	dw_plant.SetColumn('location_code')
else
	dw_load_list_complex.SetFocus()
	dw_load_list_complex.setcolumn('complex_code')
	dw_load_list_complex.SelectText(1, 3)
end if
end event

type dw_load_list_complex from u_load_list_complex within w_auto_load_list_maint_inq
integer x = 594
integer y = 200
integer height = 92
integer taborder = 40
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;Long					ll_row
datawindowchild	ldw_complex
integer 				rtncode

//If Len(Trim(data)) = 0 Then 
//	// User entered an empty complex code -- set fields to blank
//	// The last row is an empty row
//	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
//	This.ScrollToRow(This.RowCount())
//	This.SetItem(1,"complex_code","")
//	return 
//End if
//
//rtncode = dw_load_list_complex.getchild('complex_code', ldw_complex)

// Find the entered complex code in the table -- don't count the last row, it's blank
//ll_row = ldw_complex.Find('complex_code = "' + Trim(data) + '"', 1, ldw_complex.RowCount())
//ll_row = ldw_complex.Find('complex_code = "' + data + '"', 1, ldw_complex.RowCount())
//
//If ll_row <= 0 Then
//	iw_Frame.SetMicroHelp(data + " is an Invalid Complex Code")
//	This.SetFocus()
//	This.SelectText(1, Len(data))
//	return 1
//Else
//	This.SetFocus()
//	This.SelectText(1, Len(data))
//	SetProfileString( iw_frame.is_UserINI, "Pas", "Lastcomplexcode",data)
//	iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)
//End if
//
//return
Parent.wf_get_start_time()

end event

event itemerror;call super::itemerror;return 2
end event

type dw_plant_type from u_load_list_plt_type within w_auto_load_list_maint_inq
integer x = 603
integer y = 308
integer taborder = 50
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;Parent.Post wf_get_start_time()
end event

type dw_load_list_plt_descr from u_base_dw_ext within w_auto_load_list_maint_inq
integer x = 14
integer y = 576
integer width = 1166
integer height = 396
integer taborder = 0
boolean bringtotop = true
string dataobject = "d_load_list_plt_descr"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

type dw_grind_other_start_load_times from u_grind_other_start_load_times within w_auto_load_list_maint_inq
integer x = 1216
integer y = 796
integer taborder = 110
boolean bringtotop = true
boolean border = false
end type

