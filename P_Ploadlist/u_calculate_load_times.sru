HA$PBExportHeader$u_calculate_load_times.sru
forward
global type u_calculate_load_times from datawindow
end type
end forward

global type u_calculate_load_times from datawindow
integer width = 1010
integer height = 284
integer taborder = 10
string dataobject = "d_calculate_load_times"
boolean border = false
boolean livescroll = true
end type
global u_calculate_load_times u_calculate_load_times

type variables
Window	iw_parentwindow
end variables

forward prototypes
public subroutine uf_set_calculate_load_times_ind (string as_calculate_load_times_ind)
public function string uf_get_calculate_load_times_ind ()
end prototypes

public subroutine uf_set_calculate_load_times_ind (string as_calculate_load_times_ind);This.SetItem(1, "calculate_load_times_ind", as_calculate_load_times_ind)
end subroutine

public function string uf_get_calculate_load_times_ind ();String ls_temp
ls_temp = Trim(This.GetItemString(1, "calculate_load_times_ind"))
Return ls_temp

//return Trim(This.GetItemString(1, "calculate_load_times_ind"))
end function

event constructor;String	ls_text

iw_parentwindow = Parent

This.InsertRow(0)

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "Lastcalcloadtimes","")

If IsNull(ls_text) Then
	ls_text = 'N'
End If
	
This.SetItem( 1, "calculate_load_times_ind", ls_text)


end event

event itemchanged;SetProfileString( iw_frame.is_UserINI, "Pas", "Lastcalcloadtimes",data)

end event

on u_calculate_load_times.create
end on

on u_calculate_load_times.destroy
end on

