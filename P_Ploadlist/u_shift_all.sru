HA$PBExportHeader$u_shift_all.sru
forward
global type u_shift_all from datawindow
end type
end forward

global type u_shift_all from datawindow
integer width = 393
integer height = 100
integer taborder = 10
string dataobject = "d_shift_all"
boolean border = false
boolean livescroll = true
end type
global u_shift_all u_shift_all

forward prototypes
public function string uf_get_shift ()
public subroutine uf_set_shift (string as_shift)
public function integer uf_enable (boolean ab_enable)
public function integer uf_modified ()
end prototypes

public function string uf_get_shift ();Return This.GetItemString(1, "shift")

end function

public subroutine uf_set_shift (string as_shift);	This.SetItem(1,"shift",as_shift)
  
end subroutine

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.shift.Background.Color = 16777215
	This.object.shift.Protect = 0
Else
	This.object.shift.Background.Color = 12632256
	This.object.shift.Protect = 1
End If

Return 1
end function

public function integer uf_modified ();return 0
end function

event constructor;DataWindowChild		ldwc_type
string	st_allrow
integer 	rtncode

This.GetChild("shift", ldwc_type)

//ldwc_type.SetTransObject(SQLCA)
//ldwc_type.Retrieve("shift")
//This.InsertRow(0)
//



st_allrow = "ALL"

rtncode = This.GetChild('shift', ldwc_type)
IF rtncode = -1 THEN 
	MessageBox( "Error", "Ship Plant not a DataWindowChild") 
//	return false
end if	
// Establish the connection if not already connected
CONNECT USING SQLCA;	
// Set the transaction object for the child
ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("shift")
ldwc_type.InsertRow(1)
ldwc_type.SetItem(1,1,st_allrow)
ldwc_type.SetItem(1,2,st_allrow)

end event

on u_shift_all.create
end on

on u_shift_all.destroy
end on

