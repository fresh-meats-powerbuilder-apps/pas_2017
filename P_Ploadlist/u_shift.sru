HA$PBExportHeader$u_shift.sru
forward
global type u_shift from datawindow
end type
end forward

global type u_shift from datawindow
int Width=334
int Height=72
int TabOrder=10
string DataObject="d_shift"
boolean Border=false
boolean LiveScroll=true
end type
global u_shift u_shift

forward prototypes
public function string uf_get_shift ()
public subroutine uf_set_shift (string as_shift)
public function integer uf_enable (boolean ab_enable)
public function integer uf_modified ()
end prototypes

public function string uf_get_shift ();Return This.GetItemString(1, "shift")

end function

public subroutine uf_set_shift (string as_shift);	This.SetItem(1,"shift",as_shift)
  
end subroutine

public function integer uf_enable (boolean ab_enable);If ab_enable Then
	This.object.shift.Background.Color = 16777215
	This.object.shift.Protect = 0
Else
	This.object.shift.Background.Color = 12632256
	This.object.shift.Protect = 1
End If

Return 1
end function

public function integer uf_modified ();return 0
end function

event constructor;DataWindowChild		ldwc_type


This.GetChild("shift", ldwc_type)

ldwc_type.SetTransObject(SQLCA)
ldwc_type.Retrieve("shift")
This.InsertRow(0)

end event

