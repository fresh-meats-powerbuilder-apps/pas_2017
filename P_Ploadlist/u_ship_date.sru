HA$PBExportHeader$u_ship_date.sru
forward
global type u_ship_date from datawindow
end type
end forward

global type u_ship_date from datawindow
int Width=649
int Height=92
int TabOrder=10
string DataObject="d_ship_date"
boolean Border=false
boolean LiveScroll=true
end type
global u_ship_date u_ship_date

type variables
Window		iw_parentwindow
end variables

forward prototypes
public function date uf_get_ship_date ()
public subroutine uf_set_ship_date (date adt_ship_date)
end prototypes

public function date uf_get_ship_date ();Return This.GetItemDate(1, 'ship_date')
end function

public subroutine uf_set_ship_date (date adt_ship_date);This.SetItem(1,"ship_date",adt_ship_date)
end subroutine

event constructor;String	ls_text

iw_parentwindow = Parent
This.InsertRow(0)

ls_text = ProfileString( iw_frame.is_UserINI, "Pas", "Lastshipdate","")

if ls_text = '' then
	ls_text = String(RelativeDate(Today(),  1))
end if

This.SetItem( 1, "ship_date", Date(ls_text))

end event

event doubleclicked;String		ls_date

str_parms	lstr_parms

If This.object.ship_date.Protect = '1' Then Return -1

// Get the X and Y coordinates of the place that was clicked
lstr_parms.integer_arg[1] = iw_parentwindow.PointerX() + iw_parentwindow.WorkSpaceX() - 50
CHOOSE CASE	lstr_parms.integer_arg[1]
CASE IS > 2253
	lstr_parms.integer_arg[1] = 2253
CASE is < 113
	lstr_parms.integer_arg[1] = 113
END CHOOSE

lstr_parms.integer_arg[2] = iw_parentwindow.PointerY() + iw_parentwindow.WorkSpaceY() - 500
CHOOSE CASE	lstr_parms.integer_arg[2]
CASE IS > 1053
	lstr_parms.integer_arg[2] = 1053
CASE is < 0
	lstr_parms.integer_arg[2] = 0
END CHOOSE

lstr_parms.date_arg[1] = This.GetItemDate(1, 'ship_date')
OpenWithParm(w_Calendar, lstr_parms)
// Get the return string (date) from the calendar window
// If an empty string do not do anything
ls_date = Message.StringParm
If ls_date <> "" Then
	SetText(ls_Date)
Else
	Return
End If
end event

event itemchanged;String	ls_date

ls_date = String(Date(data),"mm/dd/yyyy") 
SetProfileString( iw_frame.is_UserINI, "Pas", "Lastshipdate",ls_date)
end event

event getfocus;This.SelectText(1, Len(This.GetText()))
end event

