$PBExportHeader$w_raw_mat_po_product.srw
$PBExportComments$Add pallet
forward
global type w_raw_mat_po_product from w_base_sheet_ext
end type
type dw_header from u_base_dw_ext within w_raw_mat_po_product
end type
type dw_rmt_detail from u_base_dw_ext within w_raw_mat_po_product
end type
end forward

global type w_raw_mat_po_product from w_base_sheet_ext
integer width = 3461
integer height = 1704
string title = "Products on Purchase Orders"
boolean maxbox = false
long backcolor = 67108864
dw_header dw_header
dw_rmt_detail dw_rmt_detail
end type
global w_raw_mat_po_product w_raw_mat_po_product

type prototypes
// pass 13 for selected color, 14 for selected text color
Function Long GetSysColor(Int index) Library "user32.dll"
 
end prototypes

type variables
Boolean		ib_async_running, &
				ib_ReInquire, &
				ib_good_product

nvuo_fab_product_code invuo_fab_product_code

Int		ii_async_commhandle, &
		ii_async_timeout_count

Long			il_ChangedRow, &
				il_lastrow

s_error		istr_error_info

u_pas201		iu_pas201

String		is_colname, &
				is_input, &
				is_ChangedColumnName, &
				is_debug, &
				is_data
				
w_base_sheet	iw_order_detail
end variables

forward prototypes
public function boolean wf_validate (long al_row)
public function boolean wf_retrieve ()
public function boolean wf_update ()
public function boolean wf_unstring_output (string as_output_string)
end prototypes

public function boolean wf_validate (long al_row);Date					ldt_temp

Long					ll_rtn, &
						ll_nbrrows

String				ls_division_code, &
						ls_fab_product, &
						ls_product_state, &
						ls_searchstring, &
						ls_temp, &
						ls_update_flag, & 
						ls_sched_date, &
						ls_shift
					

ll_nbrrows = dw_rmt_detail.RowCount()


// Check that the sched date has a value and is < current date.
ldt_temp = dw_rmt_detail.GetItemDate(al_row, "receive_date")
IF IsNull(ldt_temp) or ldt_temp < Date('01/01/0001') Then 
	iw_Frame.SetMicroHelp("Please enter a Receive Date greater than 00/00/0000")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("receive_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

IF ldt_temp > today() Then 
	iw_Frame.SetMicroHelp("Please enter a Receive Date less than or equal to current date.")
	This.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(al_row)
	dw_rmt_detail.SetColumn("receive_date")
	dw_rmt_detail.SetFocus()
	This.SetRedraw(True)
	Return False
End If

						
Return True
end function

public function boolean wf_retrieve ();Integer	li_ret

String	ls_input, &
			ls_output_values, &
			ls_product, &
			ls_product_state		
			
Long		ll_value, &
			ll_rtn

u_string_functions u_string

Call w_base_sheet::CloseQuery
If Message.ReturnValue = 1 Then return false
ls_input = Message.StringParm
If Not ib_ReInquire Then
	OpenWithParm(w_raw_mat_po_product_inq, This)
	ls_input = Message.StringParm
	If iw_frame.iu_string.nf_IsEmpty(ls_input) Then return false
Else
	ib_ReInquire = False
End if

SetPointer(HourGlass!)
SetMicroHelp("Retrieving ...")

is_debug = This.dw_header.GetItemString(1, 'fab_product_code')
ls_input = This.dw_header.GetItemString(1, "plant_code") + '~t' + &
				String(This.dw_header.GetItemDate(1, 'sched_date'), 'yyyy-mm-dd') + '~t' 
				
ls_product = This.dw_header.GetItemString(1, "fab_product_code") 
If iw_frame.iu_string.nf_IsEmpty(ls_product) Then
	ls_product = ' '
End If

ls_product_state = string(This.dw_header.GetItemnumber(1, "product_state")) 
If iw_frame.iu_string.nf_IsEmpty(ls_product_state) Then
	ls_product_state = ' '
End If
ls_input = ls_input + ls_product + '~t' + ls_product_state + '~r~n'


is_input = ls_input

li_ret = iu_pas201.nf_pasp45cr_products_on_po_inq(istr_error_info, &
									is_input, &
									ls_output_values)
								

This.dw_rmt_detail.Reset()

If li_ret = 0 Then
	This.dw_rmt_detail.ImportString(ls_output_values)

	ll_value = dw_rmt_detail.RowCount()
	If ll_value < 0 Then ll_value = 0

	dw_rmt_detail.ResetUpdate()

	IF ll_value > 0 THEN
 		dw_rmt_detail.SetFocus()
		dw_rmt_detail.ScrollToRow(1)
		dw_rmt_detail.SetColumn( "receive_date" )
		dw_rmt_detail.TriggerEvent("RowFocusChanged")
	END IF

	SetMicroHelp(String(ll_value) + " rows retrieved")
	ib_good_product = True
	This.SetRedraw( True )
	iw_frame.im_menu.mf_enable('m_save')
	Return( True )
Else
	ll_rtn = iw_frame.im_menu.mf_disable('m_save')
	ib_good_product = False
	This.SetRedraw(True)
	Return( False )
End If


return true

end function

public function boolean wf_update ();integer			li_ColNbr, &
					li_Counter

long				ll_NbrRows, &
					ll_Row, &
					ll_Deleted_Count, &
					ll_RowCount

string			ls_tpasprpt_ind , &
					ls_ColName, &
					ls_TextName, &
					ls_Update_string, &
					ls_header_string, &
					ls_output_string

dwItemStatus	lis_status

IF dw_rmt_detail.AcceptText() = -1 THEN Return( False )
IF dw_rmt_detail.ModifiedCount() + dw_rmt_detail.DeletedCount() <= 0 THEN Return( False )

SetPointer(HourGlass!)

iw_frame.SetMicroHelp("Wait... Updating the Database")

IF Not IsValid( iu_pas201 ) THEN
	iu_pas201	=  CREATE u_pas201
END IF

ll_NbrRows = dw_rmt_detail.RowCount( )

ll_row = 0

DO WHILE ll_Row <= ll_NbrRows 
	ll_Row = dw_rmt_detail.GetNextModified(ll_Row, Primary!)
	IF ll_Row > 0 THEN 
		If Not wf_validate(ll_row) Then Return False
	ELSE
		ll_Row = ll_NbrRows + 1
	END IF
LOOP

ls_header_string = dw_header.GetItemString(1,"plant_code")
ls_Update_string = iw_frame.iu_string.nf_BuildUpdateString(dw_rmt_detail)

istr_error_info.se_event_name = "wf_update"
istr_error_info.se_procedure_name = "u_pas201.nf_pasp46cr_upd_rmt_sched_po"
istr_error_info.se_message = Space(71)

If not iu_pas201.nf_pasp46cr_products_on_po_upd(istr_error_info, ls_Update_string, ls_output_string, ls_header_string) Then
	if ls_output_string > '' Then
		wf_unstring_output(ls_output_string)
	End If
	Return False
end If

//wf_retrieve()

dw_rmt_detail.SetReDraw(False)
iw_frame.SetMicroHelp("Update Successful")

ll_RowCount = dw_rmt_detail.RowCount()	
For li_Counter = 1 to ll_RowCount
	dw_rmt_detail.SetItem(li_Counter, 'update_flag', ' ')
Next
dw_rmt_detail.ResetUpdate()
dw_rmt_detail.SetSort("delivery_date A, purchase_order A, line_number A")
dw_rmt_detail.Sort()
dw_rmt_detail.GroupCalc()
dw_rmt_detail.SetFocus()
dw_rmt_detail.SetReDraw(True)

Return( True )
end function

public function boolean wf_unstring_output (string as_output_string);Date					ldt_start_date, &
						ldt_end_date, &
						ldt_temp

Long					ll_rtn, &
						ll_nbrrows
						

String				ls_purchase_order, &
						ls_line_number, &
						ls_searchstring
						

u_string_functions	lu_string_functions
					
ll_nbrrows = dw_rmt_detail.RowCount()

dw_rmt_detail.SelectRow(0,False)

//find the row that matches the product code, product state, schedule date and shift

ll_nbrrows = dw_rmt_detail.RowCount()
ls_purchase_order = lu_string_functions.nf_gettoken(as_output_string, '~t')
ls_line_number = lu_string_functions.nf_gettoken(as_output_string, '~t')

ls_SearchString = 	"purchase_order = '"+ ls_purchase_order +&
							"' and line_number = '" + ls_line_number + "'" 
	
ll_rtn = dw_rmt_detail.Find  &
					( ls_SearchString, 1, ll_nbrrows)
		
If ll_rtn > 0 Then
	dw_rmt_detail.SetRedraw(False)
	dw_rmt_detail.ScrollToRow(ll_rtn)
	dw_rmt_detail.SetRow(ll_rtn)
	dw_rmt_detail.SelectRow(ll_rtn, True)
	dw_rmt_detail.SetRedraw(True)
End If

as_output_string = lu_string_functions.nf_righttrim(as_output_string, TRUE, TRUE)
Return True
end function
event close;call super::close;If IsValid(iu_pas201) Then Destroy(iu_pas201)



end event

event deactivate;call super::deactivate;// Still Need to enable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_enable('m_print')
iw_frame.im_menu.mf_enable('m_nonvisprint')

iw_frame.im_menu.mf_enable('m_next')
iw_frame.im_menu.mf_enable('m_previous')

iw_frame.im_menu.mf_enable('m_new')
iw_frame.im_menu.mf_enable('m_addrow')
iw_frame.im_menu.mf_enable('m_save')
iw_frame.im_menu.mf_enable('m_deleterow')
iw_frame.im_menu.mf_enable('m_delete')



	

end event

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()
end on

event activate;call super::activate;// Still Need to disable iw_frame.im_menu stuff, because that toolbar is the one visible
iw_frame.im_menu.mf_disable('m_print')
iw_frame.im_menu.mf_disable('m_nonvisprint')

iw_frame.im_menu.mf_disable('m_next')
iw_frame.im_menu.mf_disable('m_previous')

iw_frame.im_menu.mf_disable('m_new')
iw_frame.im_menu.mf_disable('m_addrow')
iw_frame.im_menu.mf_disable('m_deleterow')
iw_frame.im_menu.mf_disable('m_delete')

If not ib_good_product Then
	iw_frame.im_menu.mf_disable('m_save')
End If

end event

event open;call super::open;String				ls_text, &
						ls_option, & 
						ls_temp, &
						ls_input_string, &
						ls_input_plant, &
						ls_input_product, &
						ls_input_state, &
						ls_long_description, &
						ls_location_name, ls_input_date
Long					ls_len
Date					ldt_input_date


dw_header.InsertRow(0)
dw_rmt_detail.InsertRow(0)
dw_header.ib_Updateable=False
ib_good_product = False
ib_reinquire = False

ls_input_string = Message.StringParm

ls_len = len(ls_input_string)
	if ls_len > 0 then
		Do While Not iw_frame.iu_string.nf_IsEmpty(ls_input_string)
			ls_input_plant = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
			ls_input_product = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
			ls_input_state = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
			ls_input_date = iw_frame.iu_string.nf_GetToken(ls_input_string, '~t')
		Loop	
		
		SELECT locations.LOCATION_NAME
	    INTO :ls_location_name
   	 FROM locations
   	 WHERE ( locations.LOCATION_CODE = :ls_input_plant);
	
		SELECT sku_products.LONG_DESCRIPTION  
		INTO :ls_long_description 
   	 FROM sku_products 
   	 WHERE ( sku_products.SKU_PRODUCT_CODE = :ls_input_product);
		 
		dw_header.SetItem(1, "plant_code", ls_input_plant)
		dw_header.SetItem(1, 'plant_description', ls_location_name)
		dw_header.SetItem(1, "fab_product_code", ls_input_product)
		dw_header.SetItem(1, 'fab_product_descr', ls_long_description)
		dw_header.SetItem(1, "product_state", integer(ls_input_state))
		dw_header.SetItem(1, "sched_date", date(ls_input_date))
		dw_header.acceptText()
		if message.StringParm = 'OK' then
			ib_reinquire = False
		else
			ib_reinquire = True
		End if	
		ib_good_product = False
		dw_header.ib_Updateable=False
		Message.StringParm = ''
	End if





end event

event ue_postopen;call super::ue_postopen;Environment		le_env

iu_pas201 = Create u_pas201
If Message.ReturnValue = -1 Then 
	Close(This)
	return
End if

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Raw Material Default Schedule"
istr_error_info.se_user_id = sqlca.userid

This.PostEvent("ue_query")

end event

on w_raw_mat_po_product.create
int iCurrent
call super::create
this.dw_header=create dw_header
this.dw_rmt_detail=create dw_rmt_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_header
this.Control[iCurrent+2]=this.dw_rmt_detail
end on

on w_raw_mat_po_product.destroy
call super::destroy
destroy(this.dw_header)
destroy(this.dw_rmt_detail)
end on

event ue_get_data;call super::ue_get_data;Long			ll_window_x, &
				ll_window_y, &
				ll_datawindow_x, &
				ll_datawindow_y
				
string		ls_temp				

Choose Case as_value
	Case 'data'
		message.stringparm = is_data
	Case 'xy_values'
		ll_window_x = This.PointerX()
		ll_window_y = This.PointerY()
		ll_datawindow_x = dw_rmt_detail.PointerX() + &
				Long(dw_rmt_detail.object.boxes.width) 
		ll_datawindow_y = dw_rmt_detail.PointerY()
		message.StringParm = String(ll_datawindow_x + (ll_window_x - ll_window_x)) + '~t' + &
				String(ll_datawindow_y + (ll_window_y - ll_datawindow_y)) + '~t'
	Case 'Plant' 
		Message.StringParm = dw_header.GetItemString(1, 'plant_code')
	Case 'Product'
		Message.StringParm = dw_header.GetItemString(1, 'fab_product_code')
	Case 'Product_Descr'
		Message.StringParm = dw_header.GetItemString(1, 'fab_product_descr')
	Case 'State'
		Message.StringParm = String(dw_header.GetItemNumber(1, 'product_state'))
	Case 'sched_date'
		Message.StringParm = String(dw_header.GetItemDate(1, 'sched_date'), 'mm/dd/yyyy')
		
End Choose

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_header.SetItem(1, 'plant_code', as_value)
	Case 'Plant_Desc' 
		dw_header.SetItem(1, 'plant_description', as_value)
	Case 'Product'
		dw_header.SetItem(1, 'fab_product_code', as_value)
		If as_value > ' ' Then
			dw_header.object.product_code_t.Visible = 1
			dw_header.object.fab_product_code.Visible = 1
		Else
			dw_header.object.product_code_t.Visible = 0
			dw_header.object.fab_product_code.Visible = 0
		End If
	Case 'Product_Desc'
		dw_header.SetItem(1, 'fab_product_descr', as_value)
		If as_value > ' ' Then
			dw_header.object.fab_product_descr.Visible = 1
		Else
			dw_header.object.fab_product_descr.Visible = 0
		End If
	Case 'State'
		If as_value > ' ' Then
			dw_header.object.product_state_t.Visible = 1
			dw_header.object.product_state.Visible = 1
			dw_header.SetItem(1, 'product_state', integer(as_value))
		Else
			dw_header.SetItem(1, 'product_state', 0)
			dw_header.object.product_state_t.Visible = 0
			dw_header.object.product_state.Visible = 0
		End If	
	Case 'sched_date'
		dw_header.SetItem(1, 'sched_date', Date(as_value))
End Choose

end event

event resize;call super::resize;integer li_x		
integer li_y		

li_x = (dw_rmt_detail.x * 2) + 30 
li_y = dw_rmt_detail.y + 115

if width > li_x Then
	dw_rmt_detail.width	= width - li_x
end if

if height > li_y then
	dw_rmt_detail.height	= height - li_y
end if
end event

type dw_header from u_base_dw_ext within w_raw_mat_po_product
integer y = 4
integer width = 1719
integer height = 232
integer taborder = 0
string dataobject = "d_rmt_sched_header"
boolean controlmenu = true
boolean border = false
end type

event constructor;call super::constructor;ib_updateable = False

this.object.product_code_t.Visible = 0
this.object.fab_product_code.Visible = 0
this.object.fab_product_descr.Visible = 0
this.object.product_state_t.Visible = 0
this.object.product_state.Visible = 0
end event

type dw_rmt_detail from u_base_dw_ext within w_raw_mat_po_product
integer y = 288
integer width = 3374
integer height = 1152
integer taborder = 20
string dataobject = "d_rmt_po_product_detail"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event clicked;call super::clicked;String	ls_temp
Boolean 	lb_color

If dwo.Type <> "column" Then Return

If dwo.Band <> "detail" Then Return

If row = 0 Then return

If Right(dwo.Name, 2) = '_t' Then return



end event

event constructor;call super::constructor;ib_updateable = True
This.SetItem(1, 'receive_date', '0001/01/01')
end event

event ue_postconstructor;call super::ue_postconstructor;DataWindowChild			ldwc_state

dw_rmt_detail.GetChild('product_state', ldwc_state)
ldwc_state.SetTransObject(SQLCA)
ldwc_state.Retrieve("PRDSTATE")


end event

event itemchanged;call super::itemchanged;dwItemStatus	le_RowStatus

int			li_rc, &
				li_seconds,li_temp

long			ll_char_row, &
				ll_source_row, &
				ll_plant_row, &
				ll_RowCount, &
				ll_find_row

string		ls_GetText, &
				ls_temp
Date			ldt_temp	

nvuo_pa_business_rules	u_rule


is_ColName = GetColumnName()
ls_GetText = data
ll_source_row	= GetRow()
il_ChangedRow = 0

CHOOSE CASE is_ColName
	CASE "receive_date"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid date")
			This.selecttext(1,100)
			return 1
		End If
		Date ldt_sched_Date
		ldt_temp = Date(data)
		If ldt_temp > today() Then
			iw_Frame.SetMicroHelp("Receive date must be less than or equal to current date.")
			this.setfocus( )
			this.setcolumn("receive_date") 
			This.SelectText(1, Len(data))	
			Return 1
		end if
	Case "boxes", "pounds"
		If Not IsNumber(ls_GetText) Then
			iw_frame.SetMicroHelp("Value must be a number")
			This.selecttext(1,100)
			return 1
		End if
		If Real(ls_GetText) < 0 Then
			iw_frame.SetMicroHelp("Value cannot be negative")
			This.selecttext(1,100)
			return 1
		End if
END CHOOSE

// Update the Update_Flag column so the RPC will know how to update the row
IF ll_source_row > 0 AND ll_source_row <= This.RowCount() THEN
	IF Long(This.Describe("update_flag.id")) > 0 THEN
		If iw_frame.iu_string.nf_IsEmpty(This.GetItemString(ll_source_row, "update_flag")) Then
			le_RowStatus = This.GetItemStatus(ll_source_row, 0, Primary!)
			CHOOSE CASE le_RowStatus
			CASE NewModified!, New!
				This.SetItem(ll_source_row, "update_flag", "A")
			CASE DataModified!, NotModified!
				This.SetItem(ll_source_row, "update_flag", "U")
			END CHOOSE		
		End if
	END IF
END IF

iw_frame.SetMicroHelp(iw_frame.ia_application.MicroHelpDefault)

return 0
end event

event itemerror;call super::itemerror;return (1)
end event

event ue_mousemove;call super::ue_mousemove;String			ls_name, &
					ls_code


Choose Case dwo.name
	Case 'boxes', 'pounds'
		//If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
	//	This.SetFocus()
		//il_LastRow = row
		If This.GetItemString ( row, "line_ind") = 'P' Then
			is_data = ""
			is_data = 'Multiple Production Dates Exist'
			OpenWithParm(w_order_detail_tooltip, Parent, Parent)
		End If
	Case Else
	//	il_LastRow = 0
		If IsValid(w_order_detail_tooltip) Then Close(w_order_detail_tooltip)
End Choose

end event

