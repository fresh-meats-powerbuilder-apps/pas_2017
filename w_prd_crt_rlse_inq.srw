$PBExportHeader$w_prd_crt_rlse_inq.srw
forward
global type w_prd_crt_rlse_inq from w_base_response
end type
type dw_plant from u_plant within w_prd_crt_rlse_inq
end type
type dw_area from u_sched_area_sect within w_prd_crt_rlse_inq
end type
type dw_sched_date from u_sched_date within w_prd_crt_rlse_inq
end type
end forward

global type w_prd_crt_rlse_inq from w_base_response
integer x = 1075
integer y = 485
integer width = 1614
integer height = 632
string title = "Create Release Schedule Inquire"
long backcolor = 67108864
event ue_query pbm_custom70
dw_plant dw_plant
dw_area dw_area
dw_sched_date dw_sched_date
end type
global w_prd_crt_rlse_inq w_prd_crt_rlse_inq

type variables
Boolean		ib_valid_return, &
				ib_NewPlant
				
w_base_sheet	iw_parent

u_pas201		iu_pas201

s_error		istr_error_info

DataWindowChild	idwc_temp

u_sect_functions	iu_sect_functions
end variables

on w_prd_crt_rlse_inq.create
int iCurrent
call super::create
this.dw_plant=create dw_plant
this.dw_area=create dw_area
this.dw_sched_date=create dw_sched_date
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_plant
this.Control[iCurrent+2]=this.dw_area
this.Control[iCurrent+3]=this.dw_sched_date
end on

on w_prd_crt_rlse_inq.destroy
call super::destroy
destroy(this.dw_plant)
destroy(this.dw_area)
destroy(this.dw_sched_date)
end on

event close;call super::close;If Not ib_valid_return Then
	Message.StringParm = ""
Else
	Message.StringParm = 'OK'
End if
end event

event open;call super::open;iw_parent = Message.PowerObjectParm

If Not IsValid(iw_Parent) Then
	Close(This)
	return
End if

This.Move(iw_parent.X + (iw_parent.Width / 2) - (This.Width / 2), &
			 iw_parent.Y + WorkSpaceY() + (iw_parent.Height / 2) - (This.Height / 2))

If dw_plant.RowCount() = 0 Then
	dw_plant.InsertRow(0)
End if

If dw_area.RowCount() = 0 Then
	dw_area.InsertRow(0)
End If

If dw_sched_date.RowCount() = 0 Then
	dw_sched_date.InsertRow(0)
End If

end event

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "")
end event

event ue_base_ok;call super::ue_base_ok;Date		ldt_temp

String	ls_temp, &
			ls_SearchString
Long 		ll_rtn
Integer	li_rtn, &
			li_temp

If dw_plant.AcceptText() = -1 or &
	dw_area .AcceptText() = -1 or &
	dw_sched_date .AcceptText() = -1 Then
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_plant.GetItemString(1, "location_code")) Then
	iw_frame.SetMicroHelp("Plant is a required field")
	dw_plant.SetFocus()
	return
End if

If iw_frame.iu_string.nf_IsEmpty(dw_area.GetItemString(1, "area_name_code")) Then
	iw_frame.SetMicroHelp("Area is a required field")
	dw_area .SetFocus()
	return
End if

//If iw_frame.iu_string.nf_IsEmpty(dw_area.GetItemDate(1, "sched_date")) Then
//	iw_frame.SetMicroHelp("Date is a required field")
//	dw_area .SetFocus()
//	return
//End if

ls_temp = dw_plant.uf_get_plant_code()
iw_parent.Event ue_Set_Data('Plant', ls_temp)

ls_temp = dw_plant.uf_get_plant_descr()
iw_parent.Event ue_Set_Data('Plant Desc', ls_temp)

ls_temp = dw_area.uf_get_area_code()
iw_parent.Event ue_Set_Data('Area Name Code', ls_temp)

ls_temp = dw_area.uf_get_area_descr()
iw_parent.Event ue_Set_Data('Area Descr', ls_temp)

ldt_temp = dw_sched_date.GetItemDate(1, "sched_date")

//iw_parent.Event ue_Set_Data('sched_date', ls_temp)

iw_parent.Event ue_set_data('sched_date', &
		String(ldt_temp, 'yyyy-mm-dd'))
		
ib_valid_return = True
Close(This)
end event
event ue_postopen;call super::ue_postopen;Int							li_pos, &
								li_ret
								
String						ls_sect_name, &
								ls_temp, &
								ls_plant_code, &
								ls_date

u_string_functions		lu_string

Environment					le_env


iw_parent.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
	dw_area.SetItem(1,"plant_code", ls_temp)
	dw_area.uf_get_plt_codes(ls_temp)
	dw_area.AcceptText()
End If

iw_parent.Event ue_Get_Data('Area Name Code')
ls_sect_name = Message.StringParm

if Not lu_string.nf_IsEmpty(ls_sect_name) Then 
	dw_area.SetItem(1,"area_name_code",ls_sect_name)
End If

iw_parent.Event ue_get_data('sched_date')
ls_date = Message.StringParm
if Not lu_string.nf_IsEmpty(ls_date) Then dw_sched_date.uf_set_sched_date(date(ls_date))

istr_error_info.se_app_name = 'Pas'
istr_error_info.se_window_name = "Create Release Schedule Inquire"
istr_error_info.se_user_id = sqlca.userid




end event

type cb_base_help from w_base_response`cb_base_help within w_prd_crt_rlse_inq
boolean visible = false
integer x = 1129
integer y = 596
integer taborder = 0
end type

type cb_base_cancel from w_base_response`cb_base_cancel within w_prd_crt_rlse_inq
integer x = 631
integer y = 352
integer taborder = 40
end type

type cb_base_ok from w_base_response`cb_base_ok within w_prd_crt_rlse_inq
integer x = 183
integer y = 352
integer taborder = 30
end type

type dw_plant from u_plant within w_prd_crt_rlse_inq
integer x = 82
integer y = 48
integer taborder = 10
boolean bringtotop = true
end type

event itemchanged;call super::itemchanged;String		ls_ColName

ls_ColName = GetColumnName()

CHOOSE CASE ls_ColName
	CASE "location_code"
		If isnull(data) Then
			iw_frame.SetMicroHelp("This is not a valid location")
			This.selecttext(1,100)
			return 1
		Else
			ib_NewPlant = True
			dw_area.Reset()
			dw_area.InsertRow(0)
		End If
End Choose
end event

event losefocus;call super::losefocus;String	ls_PlantCode

ls_PlantCode = This.GetItemString(1,"location_code")

//set the hidden plant code on the next window to 
//be used to retrieve the area name codes.  
if dw_area.GetItemString(1,"plant_code") = ls_PlantCode Then
	//do nothing
Else
	dw_area.SetItem(1,"plant_code", ls_PlantCode)
	dw_area.AcceptText()

//set this to true for the clicked event on the area window
	ib_NewPlant = True
End If
end event

type dw_area from u_sched_area_sect within w_prd_crt_rlse_inq
integer y = 128
integer height = 76
integer taborder = 20
boolean bringtotop = true
end type

event clicked;call super::clicked;String		ls_ColName, &
				ls_plant_code

ls_ColName = GetColumnName()
dw_plant.AcceptText()

//ib_NewPlant is set to true when the plant window loses focus
If ib_NewPlant Then
	CHOOSE CASE ls_ColName
		CASE "area_name_code"
			ls_plant_code = This.GetItemString(1,"plant_code")
			If isnull(ls_plant_code) Then
				iw_frame.SetMicroHelp("This is not a valid plant")
				return 1
			Else
				This.SetRedraw( False )
				super::uf_get_plt_codes(ls_plant_code)
				This.AcceptText()
				This.SetRedraw( True )
			End If
	End Choose
	ib_NewPlant = False
End If
return 0

end event

event itemchanged;call super::itemchanged;String		ls_ColName

ls_ColName = GetColumnName()
dw_plant.AcceptText()


end event

type dw_sched_date from u_sched_date within w_prd_crt_rlse_inq
integer x = 23
integer y = 224
integer taborder = 20
boolean bringtotop = true
end type

