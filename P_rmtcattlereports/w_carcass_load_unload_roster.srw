HA$PBExportHeader$w_carcass_load_unload_roster.srw
forward
global type w_carcass_load_unload_roster from w_netwise_sheet
end type
type st_1 from statictext within w_carcass_load_unload_roster
end type
type dw_report_opc from u_base_dw_ext within w_carcass_load_unload_roster
end type
type dw_transfer_date from u_base_dw_ext within w_carcass_load_unload_roster
end type
type dw_plant from u_plant within w_carcass_load_unload_roster
end type
end forward

global type w_carcass_load_unload_roster from w_netwise_sheet
integer x = 5
integer y = 4
integer width = 1646
integer height = 808
string title = "Carcass Load/Unload Roster"
long backcolor = 79741120
st_1 st_1
dw_report_opc dw_report_opc
dw_transfer_date dw_transfer_date
dw_plant dw_plant
end type
global w_carcass_load_unload_roster w_carcass_load_unload_roster

type variables
s_error			istr_error_info
string			Is_inquire_parm
u_ws_pas2       iu_ws_pas2
end variables

forward prototypes
public function boolean wf_update ()
public function string nf_formatdate (string input_date)
end prototypes

public function boolean wf_update ();boolean lbln_Return
string plant_code, carcass_option, ls_inputstring,user_id, app_name, transfer_date, day_of_week
integer li_rval,li_day

dw_transfer_date.AcceptText()

string dt 
transfer_date = nf_formatdate(string(dw_transfer_date.getItemDate(1,'transfer_date')))
plant_code = dw_plant.getItemString(1,'location_code')
li_day = DayNumber(dw_transfer_date.getItemDate(1,'transfer_date'))
day_of_week = string(li_day - 1)
carcass_option = dw_report_opc.getItemString(1,'carcass_loading_roster')
user_id =sqlca.userid
app_name = Message.nf_Get_App_ID()

If carcass_option = 'L' Then
	ls_inputstring = '2080' + '~t' + 'RM02' + '~t' + user_id + ' ' + carcass_option + plant_code + transfer_date + day_of_week
else
	ls_inputstring = '2080' + '~t' + 'RM02' + '~t' + user_id + ' ' + carcass_option + plant_code + transfer_date + day_of_week
end If

li_rval = iu_ws_pas2.nf_pasp15gr(ls_inputstring,istr_error_info)

If li_rval = 0 Then
	iw_frame.SetMicroHelp('Carcass Loading Report has been Initiated')
end if

SetPointer(Arrow!)

return lbln_Return
end function

public function string nf_formatdate (string input_date);string month,year,day, output_date
int stringLenght, pos, arraypos

day =mid(string(input_date),0,2)
month =mid(string(input_date),4,2)
year =mid(string(input_date),7,4)

output_date = year + '-' + day + '-' + month

return output_date
end function

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_inquire')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')
iw_frame.im_menu.mf_Disable('m_print')
iw_frame.im_menu.mf_Disable('m_filter')
iw_frame.im_menu.mf_Disable('m_find')
iw_frame.im_menu.mf_Disable('m_replace')	
iw_frame.im_menu.mf_Disable('m_clear')	
iw_frame.im_menu.mf_Disable('m_sort')	
iw_frame.im_menu.mf_Disable('m_nonvisprint')		
end event

event deactivate;iw_frame.im_menu.mf_Enable('m_inquire')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')
iw_frame.im_menu.mf_Enable('m_print')
iw_frame.im_menu.mf_Enable('m_filter')
iw_frame.im_menu.mf_Enable('m_find')
iw_frame.im_menu.mf_Enable('m_replace')	
iw_frame.im_menu.mf_Enable('m_clear')	
iw_frame.im_menu.mf_Enable('m_sort')	
iw_frame.im_menu.mf_Enable('m_nonvisprint')	


end event

event ue_postopen;call super::ue_postopen;string	ls_server_suffix

dw_plant.ib_updateable = False
dw_transfer_date.ib_updateable = False
dw_report_opc.ib_updateable = False

istr_error_info.se_app_name 		= Message.nf_Get_App_ID()
istr_error_info.se_window_name 	= "w_load_unload_rosters"
istr_error_info.se_user_id 		= sqlca.userid

iu_ws_pas2 = Create u_ws_pas2
If Message.ReturnValue = -1 Then
	Close(This)
	Return
End If
end event

on w_carcass_load_unload_roster.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_report_opc=create dw_report_opc
this.dw_transfer_date=create dw_transfer_date
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_report_opc
this.Control[iCurrent+3]=this.dw_transfer_date
this.Control[iCurrent+4]=this.dw_plant
end on

on w_carcass_load_unload_roster.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_report_opc)
destroy(this.dw_transfer_date)
destroy(this.dw_plant)
end on

event resize;//if newwidth > 2694 then
//	ole_init_mainframe_reports.Width = newwidth - 25
//end if
//
//if (newheight - 100) > 500 then
//	ole_init_mainframe_reports.Height = newheight - 25
//end if
//
end event

event close;call super::close;IF IsValid(iu_ws_pas2) THEN
	DESTROY iu_ws_pas2
END IF
end event

type st_1 from statictext within w_carcass_load_unload_roster
integer x = 219
integer y = 324
integer width = 366
integer height = 56
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 67108864
string text = "  Report Option"
boolean focusrectangle = false
end type

type dw_report_opc from u_base_dw_ext within w_carcass_load_unload_roster
string tag = "Report Option"
integer x = 155
integer y = 360
integer width = 946
integer height = 252
integer taborder = 30
string title = "Report Option"
string dataobject = "d_carcass_option"
end type

event constructor;call super::constructor;If this.rowcount() = 0 then this.insertrow(0)

//this.setitem(1,'transfer_date',today())
end event

type dw_transfer_date from u_base_dw_ext within w_carcass_load_unload_roster
integer x = 187
integer y = 188
integer width = 910
integer height = 112
integer taborder = 20
string dataobject = "d_date_transfer"
boolean border = false
end type

event constructor;call super::constructor;If this.rowcount() = 0 then this.insertrow(0)

this.setitem(1,'transfer_date',today())
end event

type dw_plant from u_plant within w_carcass_load_unload_roster
integer x = 155
integer y = 76
integer taborder = 10
boolean righttoleft = true
end type

