HA$PBExportHeader$w_pas_manual_production.srw
forward
global type w_pas_manual_production from w_base_sheet_ext
end type
type group_label_t from statictext within w_pas_manual_production
end type
type group_t from statictext within w_pas_manual_production
end type
type btnundoselected from commandbutton within w_pas_manual_production
end type
type chkmhqty from checkbox within w_pas_manual_production
end type
type chkyieldsqty from checkbox within w_pas_manual_production
end type
type chkqty from checkbox within w_pas_manual_production
end type
type chkprjqty from checkbox within w_pas_manual_production
end type
type dw_area from u_sched_area_sect within w_pas_manual_production
end type
type rb_all_products from radiobutton within w_pas_manual_production
end type
type rb_variances from radiobutton within w_pas_manual_production
end type
type dw_header from u_base_dw_ext within w_pas_manual_production
end type
type dw_scroll_product from u_base_dw_ext within w_pas_manual_production
end type
type dw_manual_production from u_base_dw_ext within w_pas_manual_production
end type
type gb_display from groupbox within w_pas_manual_production
end type
end forward

global type w_pas_manual_production from w_base_sheet_ext
integer width = 3822
integer height = 2301
string title = "Actual Production"
long backcolor = 12632256
event ue_duplicaterow pbm_custom01
group_label_t group_label_t
group_t group_t
btnundoselected btnundoselected
chkmhqty chkmhqty
chkyieldsqty chkyieldsqty
chkqty chkqty
chkprjqty chkprjqty
dw_area dw_area
rb_all_products rb_all_products
rb_variances rb_variances
dw_header dw_header
dw_scroll_product dw_scroll_product
dw_manual_production dw_manual_production
gb_display gb_display
end type
global w_pas_manual_production w_pas_manual_production

type variables
Boolean		ib_async_running

Int		ii_async_commhandle

u_pas203		iu_pas203
u_ws_pas1		iu_ws_pas1

String		is_AddRowText, &
				is_end_of_yield_flag, &
				is_compare_pa_proj_selected, &
				is_compare_yields_selected, &
				is_compare_pa_actual_selected, &
				is_compare_mat_hand_selected				

s_error		istr_error_info


end variables

forward prototypes
public function boolean wf_addrow ()
public subroutine wf_filter ()
public function integer wf_check_the_checkboxes (integer counter, ref checkbox chk, ref checkbox one, ref checkbox two)
public function boolean wf_retrieve ()
public function string wf_getheaderdata ()
public subroutine wf_adjust_grid ()
public subroutine wf_calc_variance ()
public function boolean wf_parse_string (string ls_return_cache, boolean lb_inquire)
public function boolean wf_update ()
end prototypes

event ue_duplicaterow;Long	ll_Row


ll_Row = dw_manual_production.GetRow()

If ll_Row < 1 Then iw_Frame.SetMicroHelp("There is no current row to duplicate")

dw_manual_production.RowsCopy(ll_Row, ll_Row, Primary!, dw_manual_production, &
		ll_Row + 1, Primary!)

dw_manual_production.uf_ChangeRowStatus(ll_Row, DataModified!)
dw_manual_production.SelectRow(ll_row, True)

// The new row is the one after the clicked row
ll_Row ++

dw_manual_production.SetItem(ll_Row, 'plant', '')
dw_manual_production.SetItem(ll_Row, 'quantity', 0)
dw_manual_production.SetItem(ll_Row, 'yld_quantity', 0)
dw_manual_production.SetRow(ll_Row)
dw_manual_production.SetColumn('plant')
dw_manual_production.SetFocus()

end event

public function boolean wf_addrow ();This.TriggerEvent('ue_DuplicateRow')
return True
end function

public subroutine wf_filter ();long			ll_RowCount

string		ls_filter

Decimal		ld_variance

IF rb_variances.Checked THEN
	ls_filter = "(var_quantity <> 0 )"

ELSEIF rb_all_products.Checked THEN
	ls_filter = ""
END IF

dw_manual_production.SetFilter(ls_filter)
dw_manual_production.Filter()
dw_manual_production.Sort()

ll_RowCount = dw_manual_production.RowCount()
SetMicroHelp(String(ll_RowCount) + " rows displayed")
end subroutine

public function integer wf_check_the_checkboxes (integer counter, ref checkbox chk, ref checkbox one, ref checkbox two);if(chk.checked) then
	counter++
	if(IsNull(one)) then
		one = chk
	else
		two = chk
	end if
end if
return counter
end function

public function boolean wf_retrieve ();Int		li_ret
			

Long		ll_row, &
			ll_row_count, &
			ll_qty, ll_wgt
			
Time  	lt_end_of_shift_time

String	ls_return, &
			ls_input, &
			ls_end_of_shift, &
			ls_end_of_shift_ind, &
			ls_manual_production, &
			ls_temp, ls_return_cache, ls_fromTime, ls_toTime

u_string_functions		lu_strings
DataWindowChild	dwc_end_of_shift_flag


Call w_base_sheet::closequery
If Message.ReturnValue = 1 Then return false



OpenWithParm(w_pas_manual_production_inq, This)
ls_return_cache = Message.StringParm

return wf_parse_string(ls_return_cache, true)




/*
If iw_frame.iu_string.nf_IsEmpty(ls_return_cache) Then return false

SetPointer(Hourglass!)
ls_return  = lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //Plant_Code
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //Plant_Descr
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //begin_date
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //shift
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //production_ind
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //uom_ind
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //end_of_shift
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //end_of_shift_date
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //end_of_shift_time
//ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //end_of_shift_FLAG


ls_fromTime = lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //from_time
ls_toTime = lu_strings.nf_gettoken(ls_return_cache, '~t')   + '~t' //to_time
ls_return += ls_fromTime + ls_toTime

dw_header.SetRedraw(False)
dw_header.Reset()
dw_header.ImportString(ls_return)
dw_header.SetRedraw(True)

if(dw_header.GetItemString(1, "production_ind") = 'C' OR dw_header.GetItemString(1, "production_ind") = 'U') then
	iw_frame.im_menu.mf_disable('m_save')
else
	iw_frame.im_menu.mf_enable('m_save')
end if

If (dw_header.GetItemString(1, "production_ind") = 'M') Then
	is_end_of_yield_flag = mid(ls_return,len(ls_return),1)
else
	is_end_of_yield_flag = ''
end if

dw_header.Object.from_time.visible = (dw_header.GetItemString(1, "production_ind") = 'T')		
dw_header.Object.to_time.visible   = dw_header.Object.from_time.visible
dw_header.Object.t_1.visible       = dw_header.Object.from_time.visible
dw_header.Object.t_2.visible       = dw_header.Object.from_time.visible


// now ls_return_cache should only have
// prod_ind, (group if prod_ind = g, area if prod_ind = a, empty if all)

string ls_area, ls_product_ind, ls_plant, ls_group
ls_product_ind = lu_strings.nf_gettoken(ls_return_cache, '~t')
dw_area.visible = false
ls_plant = dw_header.GetItemString(1,"plant_code")
CHOOSE CASE ls_product_ind
	case 'G'
		ls_group = lu_strings.nf_gettoken(ls_return_cache, '~t')
	case 'A'
		ls_area = lu_strings.nf_gettoken(ls_return_cache, '~t')
		dw_area.visible = true
		
		dw_area.uf_get_plt_codes(ls_plant)
		dw_area.InsertRow(1)
		
		dw_area.SetItem(1, "area_name_code", ls_area)
	case else
		lu_strings.nf_gettoken(ls_return_cache, '~t') // strip off blank space holder
		ls_group = ''
		ls_area = ''
end choose

istr_error_info.se_event_name = "wf_retrieve"
iw_frame.SetMicroHelp("Wait ... Retrieving Data")
dw_manual_production.Reset()


// I'm assuming again that header info is valid
//If dw_header.GetItemString(1, "production_ind") = 'P' Then
//	ls_indicator = 'P'
//ElseIf dw_header.GetItemString(1, "uom_ind") = 'B' Then
//	ls_indicator = 'A'
//Else
//	ls_indicator = 'W'
//End if

//dmk sr4031
dw_header.GetChild('end_of_shift', dwc_end_of_shift_flag)
CONNECT USING SQLCA;	
dwc_end_of_shift_flag.SetTransObject(SQLCA)
dwc_end_of_shift_flag.Retrieve()

ls_input = ls_plant + '~t' &
			+ String(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd") + '~t' &	
			+ dw_header.GetItemString(1, "shift") + '~t'&
			+ dw_header.GetItemString(1, "production_ind") + '~t' &
			+ ls_area + '~t' &
			+ ls_group + '~t' &
			+ ls_fromTime &
			+ ls_toTime  &
			+ '~r~n'

btnUndoSelected.Visible = (dw_header.GetItemString(1, "production_ind") = "P" AND dw_header.GetItemDate(1, "begin_date") >= Today())
wf_adjust_grid()
if(chkMHQty.Visible) then // default the variances
	chkMHQty.Checked = true
	chkYieldsQty.Checked = true
end if

If Not iu_pas203.nf_inq_man_prod(istr_error_info, &
									ls_input, &
									ls_end_of_shift, &
									ls_manual_production) Then return False

// rev#01  Change ls_end_of_shift string to Add End of Shift time. 
//ls_end_of_shift_ind = lu_strings.nf_GetToken(ls_end_of_shift, '~t')
//dw_header.SetItem(1, "end_of_shift", ls_end_of_shift_ind)
//
//ls_temp = lu_strings.nf_GetToken(ls_end_of_shift, '~t')
//If IsTime(ls_temp) Then
//	lt_end_of_shift_time = Time(ls_temp)
//	dw_header.SetItem(1, "end_of_shift_time", lt_end_of_shift_time)
//End If
//
dw_manual_production.Modify("prj_quantity.Background.Color = 12632256 prj_quantity.Protect = 1 quantity.Pointer = 'Arrow!' ")

dw_manual_production.SetRedraw(False)
li_ret = dw_manual_production.ImportString(ls_manual_production)

If li_ret >= 0 Then
	iw_frame.SetMicroHelp(String(li_ret) + ' Row(s) retrieved')
	dw_manual_production.Sort()
	If (dw_header.GetItemString(1, "production_ind") = 'U') or &
	   (dw_header.GetItemString(1, "production_ind") = 'C') or &
		(dw_header.GetItemString(1, "production_ind") = 'M') or &
		(dw_header.GetItemString(1, "production_ind") = 'Y') or &
		(dw_header.GetItemString(1, "production_ind") = 'T') Then
		
		
		dw_manual_production.Modify("quantity.Background.Color = 12632256 " + &
								"quantity.Protect = 1 quantity.Pointer = 'Arrow!' " + &
								"quantity_sum.Background.Color = 12632256 " + &
								"quantity_sum.Protect = 1 quantity_sum.Pointer = 'Arrow!'")
//dmk sr8171
		dw_manual_production.Modify("yld_quantity.Background.Color = 12632256 " + &
								"yld_quantity.Protect = 1 " + &
								"yld_quantity_sum.Background.Color = 12632256 " + &
								"yld_quantity_sum.Protect = 1 ")

	  If (dw_header.GetItemString(1, "production_ind") = 'C') Then
			wf_filter()
	  End if
	Else
		dw_manual_production.Modify("quantity.Background.Color = 16777215 " + &
								"quantity.Protect = 0 quantity.Pointer = 'Beam!' " + &
								"quantity_sum.Background.Color = 12632256 " + &
								"quantity_sum.Protect = 1 quantity_sum.Pointer = 'Arrow!'")
	//dmk SR8171
		dw_manual_production.Modify("yld_quantity.Background.Color = 16777215 " + &
								"yld_quantity.Protect = 0 " + &
								"yld_quantity_sum.Background.Color = 12632256 " + &
								"yld_quantity_sum.Protect = 1 ")
	End if
	
	dw_manual_production.ResetUpdate()
End if

//sr8171   dmk set the prev_quantity field to be used later
ll_row_count = dw_manual_production.RowCount()
ll_row = 1
If ll_row_count > 0 then
	Do
		ll_qty = dw_manual_production.GetItemNumber(ll_row, "quantity")
		ll_wgt = dw_manual_production.GetItemNumber(ll_row, "yld_quantity")
		
		if ll_qty > 0 Then
			dw_manual_production.SetItem(ll_row, "prev_avg_wgt", ll_wgt/ll_qty )	
		end if
		
		ll_row = ll_row + 1
	Loop While ll_row <= ll_row_count
	dw_manual_production.ResetUpdate()
End If



//sr7988 added new quantity fields
dw_manual_production.ScrollToRow(1)

dw_manual_production.SetColumn('yld_quantity')
dw_manual_production.SetColumn('mat_quantity')
dw_manual_production.SetColumn('var_quantity')
dw_manual_production.SetColumn('prj_quantity')
dw_manual_production.SetColumn('quantity')
dw_manual_production.SetRedraw(True)
dw_manual_production.SetFocus()
return true
*/



end function

public function string wf_getheaderdata ();string ls_data, ls_productIndicator, ls_areaOrGroup

if dw_area.visible then
	ls_productIndicator = 'A'
	ls_areaOrGroup = dw_area.GetItemString(1, "area_name_code")
elseif group_t.visible then
	ls_productIndicator = 'G'
else 
	ls_productIndicator = 'L'
	ls_areaOrGroup = ''
end if

ls_data = dw_header.Describe("DataWindow.Data") + '~t' &
		  + ls_productIndicator         + '~t' /*product indicator*/ & 
		  + ls_areaOrGroup + '~t' /* area/group/empty depending*/ 


If dw_header.GetItemString(1, "production_ind") = "C" Then
	ls_data += is_compare_pa_proj_selected + '~t' + is_compare_yields_selected + '~t' + &
					is_compare_pa_actual_selected + '~t' + is_compare_mat_hand_selected + '~t'			

Else
	ls_data += "N" + '~t' + "N" + '~t' + "N" + '~t' + "N" + '~t' 
End If
	
return ls_data
end function

public subroutine wf_adjust_grid ();
string ls_productionInd
boolean lb_visible, lb_dispVisible, lb_compares
string ls_modify


ls_productionInd = dw_header.GetItemString(1, "production_ind")
chkPrjQty.Visible = (ls_productionInd = 'C')
chkQty.Visible = chkPrjQty.Visible
chkYieldsQty.Visible = chkPrjQty.Visible
chkMHQty.Visible = chkPrjQty.Visible

//if(chkMHQty.Visible) then // default the variances
//	chkMHQty.Checked = true
//	chkYieldsQty.Checked = true
//end if

//chkPrjQty.Checked = false
//chkQty.Checked = chkPrjQty.Checked
//chkYieldsQty.Checked = chkPrjQty.Checked
//chkMHQty.Checked = chkPrjQty.Checked

dw_manual_production.Object.prj_quantity.Visible = chkPrjQty.Visible
dw_manual_production.Object.t_prj_qty.Visible = chkPrjQty.Visible
dw_manual_production.Object.pa_proj_sum.Visible = chkPrjQty.visible



lb_visible = FALSE
lb_dispVisible = false
lb_compares = false
ls_modify = "t_pa.Text = 'PA' quantity_t.Text = 'Quantity' t_yld.Text=' ' t_yld_qty.Text = 'Weight' "
dw_manual_production.Object.t_pa.Visible = false

//sr7988 add logic for compare
If (chkPrjQty.Visible) Then
	lb_visible = true
	lb_dispVisible = true
	lb_compares = true
	
	ls_modify = "t_pa.Text = 'PA Act' quantity_t.Text = 'Quantity' t_yld.Text='Yields' t_yld_qty.Text = 'Quantity' "
elseif(ls_productionInd = 'U') then
	lb_dispVisible = true
	lb_compares = true
	chkQty.Checked = true
	chkYieldsQty.Checked = true
	ls_modify = "t_pa.Text = 'Scheduled' quantity_t.Text = 'Consumption' t_yld.Text='Actual' t_yld_qty.Text = 'Consumption' "
end if


gb_display.visible = lb_dispVisible
rb_variances.visible = lb_dispVisible
rb_all_products.visible = lb_dispVisible

//gb_variance.visible = lb_visible
//rb_pa_mat.Visible = lb_visible
//rb_pa_yields.visible = lb_visible
//rb_yields_mat.Visible = lb_visible

//dmk sr8171, changes for weight.
dw_manual_production.Modify(ls_modify)

dw_manual_production.Object.t_pa.Visible = lb_compares

dw_manual_production.Object.t_mat.Visible = lb_visible
dw_manual_production.Object.t_mat_qty.Visible = lb_visible
dw_manual_production.Object.mat_quantity.Visible = lb_visible
dw_manual_production.Object.mat_quantity_sum.Visible = lb_visible

dw_manual_production.Object.t_variance.Visible = lb_compares
dw_manual_production.Object.t_var_qty.Visible = lb_compares
dw_manual_production.Object.var_quantity.Visible = lb_compares
dw_manual_production.Object.var_quantity_sum.Visible = lb_compares

dw_manual_production.Object.l_3.Visible = lb_visible
dw_manual_production.Object.l_4.Visible = lb_visible



end subroutine

public subroutine wf_calc_variance ();
//SR13313 AJG

integer li_count
string ls_expression
checkbox one, two

//If chkprjqty.Checked Then
//	dw_manual_production.object.prj_quantity.Visible = True
//	dw_manual_production.object.pa_proj_sum.Visible = True
//Else
//	dw_manual_production.object.prj_quantity.Visible = False
//	dw_manual_production.object.pa_proj_sum.Visible = False	
//End If
//
//If chkqty.Checked Then
//	dw_manual_production.object.quantity.Visible = True
//	dw_manual_production.object.quantity_sum.Visible = True
//Else
//	dw_manual_production.object.quantity.Visible = False
//	dw_manual_production.object.quantity_sum.Visible = False	
//End If
//
//If chkyieldsqty.Checked Then
//	dw_manual_production.object.yld_quantity.Visible = True
//	dw_manual_production.object.yld_quantity_sum.Visible = True
//Else
//	dw_manual_production.object.yld_quantity.Visible = False
//	dw_manual_production.object.yld_quantity_sum.Visible = False	
//End If
//
//If chkmhqty.Checked Then
//	dw_manual_production.object.mat_quantity.Visible = True
//	dw_manual_production.object.mat_quantity_sum.Visible = True
//Else
//	dw_manual_production.object.mat_quantity.Visible = False
//	dw_manual_production.object.mat_quantity_sum.Visible = False	
//End If

// set checkboxes to null
SetNull(one)
SetNull(two)

li_count = wf_check_the_checkboxes(0, chkPrjQty, one, two)
li_count = wf_check_the_checkboxes(li_count, chkQty, one, two)
li_count = wf_check_the_checkboxes(li_count, chkYieldsQty, one, two)
li_count = wf_check_the_checkboxes(li_count, chkMHQty, one, two)

ls_expression = "0"
if(li_count > 2) then
	iw_frame.SetMicroHelp("You can only select 2 checkboxes at a time for variance")
	return
elseif(li_count = 2) then
	if(dw_header.GetItemString(1, "production_ind") = 'C' AND chkPrjQty.checked) then
		ls_expression = two.Tag + " - " + one.Tag
	else
		ls_expression = one.Tag + " - " + two.Tag
	end if
end if

dw_manual_production.Modify("var_quantity.Expression = '" + ls_expression + "'")
wf_filter()

return
end subroutine

public function boolean wf_parse_string (string ls_return_cache, boolean lb_inquire);Int		li_ret
			

Long		ll_row, &
			ll_row_count, &
			ll_qty, ll_wgt
			
Time  	lt_end_of_shift_time

String	ls_return, &
			ls_input, &
			ls_end_of_shift, &
			ls_end_of_shift_ind, &
			ls_manual_production, &
			ls_temp, ls_fromTime, ls_toTime, &
			ls_check_box_val	

u_string_functions		lu_strings
DataWindowChild	dwc_end_of_shift_flag

If iw_frame.iu_string.nf_IsEmpty(ls_return_cache) Then return false

SetPointer(Hourglass!)
ls_return  = lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //Plant_Code
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //Plant_Descr
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //begin_date
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //shift
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //production_ind
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //uom_ind
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //end_of_shift
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //end_of_shift_date
ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //end_of_shift_time
//ls_return += lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //end_of_shift_FLAG


ls_fromTime = lu_strings.nf_gettoken(ls_return_cache, '~t') + '~t' //from_time
ls_toTime = lu_strings.nf_gettoken(ls_return_cache, '~t')   + '~t' //to_time
ls_return += ls_fromTime + ls_toTime

dw_header.SetRedraw(False)
dw_header.Reset()
dw_header.ImportString(ls_return)
dw_header.SetRedraw(True)

if(dw_header.GetItemString(1, "production_ind") = 'C' OR dw_header.GetItemString(1, "production_ind") = 'U') then
	iw_frame.im_menu.mf_disable('m_save')
else
	iw_frame.im_menu.mf_enable('m_save')
end if

If (dw_header.GetItemString(1, "production_ind") = 'C') Then
	iw_frame.im_menu.mf_disable('m_addrow')	
Else
	iw_frame.im_menu.mf_enable('m_addrow')
End If

If (dw_header.GetItemString(1, "production_ind") = 'M') Then
	is_end_of_yield_flag = mid(ls_return,len(ls_return),1)
Else
	is_end_of_yield_flag = ''
End if

dw_header.Object.from_time.visible = (dw_header.GetItemString(1, "production_ind") = 'T')		
dw_header.Object.to_time.visible   = dw_header.Object.from_time.visible
dw_header.Object.t_1.visible       = dw_header.Object.from_time.visible
dw_header.Object.t_2.visible       = dw_header.Object.from_time.visible


// now ls_return_cache should only have
// prod_ind, (group if prod_ind = g, area if prod_ind = a, empty if all)

string ls_area, ls_product_ind, ls_plant, ls_group
ls_product_ind = lu_strings.nf_gettoken(ls_return_cache, '~t')
dw_area.visible = false
group_t.visible = false
group_label_t.visible = false

ls_plant = dw_header.GetItemString(1,"plant_code")
SetProfileString( iw_frame.is_UserINI, "Pas", "Lastplant",Trim(ls_plant))
string grpID, grpDesc
CHOOSE CASE ls_product_ind
	case 'G'
		ls_group = lu_strings.nf_gettoken(ls_return_cache, '~t')
		grpID    = lu_strings.nf_gettoken(ls_group,        ',' )
		grpDesc  = ls_group
		ls_group = grpID
		group_t.visible = true
		group_label_t.visible = true
	case 'A'
		ls_area = lu_strings.nf_gettoken(ls_return_cache, '~t')
		dw_area.visible = true
		//dw_area.InsertRow(1)
		dw_area.uf_get_plt_codes(ls_plant)		
		dw_area.SetItem(1, "area_name_code", ls_area)
	case else
		lu_strings.nf_gettoken(ls_return_cache, '~t') // strip off blank space holder
		ls_group = ''
		ls_area = ''
end choose

is_compare_pa_proj_selected = lu_strings.nf_gettoken(ls_return_cache, '~t')
is_compare_yields_selected = lu_strings.nf_gettoken(ls_return_cache, '~t')
is_compare_pa_actual_selected = lu_strings.nf_gettoken(ls_return_cache, '~t')
is_compare_mat_hand_selected = lu_strings.nf_gettoken(ls_return_cache, '~t')

If is_compare_yields_selected = "Y" Then
	chkyieldsqty.Checked = True
	If is_compare_pa_actual_selected = "Y" Then
		chkqty.Checked = True
		chkprjqty.Checked = False
		chkmhqty.Checked = False
	Else
		chkqty.Checked = False
		If is_compare_pa_proj_selected = "Y" Then
			chkprjqty.Checked = True
			chkmhqty.Checked = False
		Else
			chkprjqty.Checked = False
			chkmhqty.Checked = True		
		End If
	End If
Else
	chkyieldsqty.Checked = False
	If is_compare_pa_actual_selected = "Y" Then
		chkqty.Checked = True
		If is_compare_pa_proj_selected = "Y" Then
			chkprjqty.Checked = True
			chkmhqty.Checked = False
		Else
			chkprjqty.Checked = False
			chkmhqty.Checked = True
		End If
	Else	
		chkqty.Checked = False
		chkprjqty.Checked = True
		chkmhqty.Checked = True		
	End If
End If

group_t.text = grpDesc

if not lb_inquire then return true

istr_error_info.se_event_name = "wf_retrieve"
iw_frame.SetMicroHelp("Wait ... Retrieving Data")
dw_manual_production.Reset()


// I'm assuming again that header info is valid
//If dw_header.GetItemString(1, "production_ind") = 'P' Then
//	ls_indicator = 'P'
//ElseIf dw_header.GetItemString(1, "uom_ind") = 'B' Then
//	ls_indicator = 'A'
//Else
//	ls_indicator = 'W'
//End if

//dmk sr4031
dw_header.GetChild('end_of_shift', dwc_end_of_shift_flag)
CONNECT USING SQLCA;	
dwc_end_of_shift_flag.SetTransObject(SQLCA)
dwc_end_of_shift_flag.Retrieve()

ls_input = ls_plant + '~t' &
			+ String(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd") + '~t' &	
			+ dw_header.GetItemString(1, "shift") + '~t'&
			+ dw_header.GetItemString(1, "production_ind") + '~t' &
			+ ls_area     + '~t' &
			+ ls_group    + '~t' &
			+ ls_fromTime + ls_toTime + &
			+ is_compare_pa_proj_selected + '~t' &
			+ is_compare_yields_selected + '~t' &			
			+ is_compare_pa_actual_selected + '~t' &			
			+ is_compare_mat_hand_selected + '~r~n'

btnUndoSelected.Visible = (dw_header.GetItemString(1, "production_ind") = "P" AND dw_header.GetItemDate(1, "begin_date") >= Today())
wf_adjust_grid()
//
//If Not iu_pas203.nf_inq_man_prod(istr_error_info, &
//									ls_input, &
//									ls_end_of_shift, &
//									ls_manual_production) Then return false


If Not iu_ws_pas1.nf_pasp36fr(istr_error_info, &
									ls_input, &
									ls_end_of_shift, &
									ls_manual_production) Then return false


// rev#01  Change ls_end_of_shift string to Add End of Shift time. 
//ls_end_of_shift_ind = lu_strings.nf_GetToken(ls_end_of_shift, '~t')
//dw_header.SetItem(1, "end_of_shift", ls_end_of_shift_ind)
//
//ls_temp = lu_strings.nf_GetToken(ls_end_of_shift, '~t')
//If IsTime(ls_temp) Then
//	lt_end_of_shift_time = Time(ls_temp)
//	dw_header.SetItem(1, "end_of_shift_time", lt_end_of_shift_time)
//End If
//
dw_manual_production.Modify("prj_quantity.Background.Color = 12632256 prj_quantity.Protect = 1 quantity.Pointer = 'Arrow!' ")

dw_manual_production.SetRedraw(False)
li_ret = dw_manual_production.ImportString(ls_manual_production)

// calculate variance if its a compare
if dw_header.GetItemString(1, 'production_ind') = 'C' or dw_header.GetItemString(1, 'production_ind') = 'U' then wf_calc_variance()



If li_ret >= 0 Then
	iw_frame.SetMicroHelp(String(li_ret) + ' Row(s) retrieved')
	dw_manual_production.Sort()
	If (dw_header.GetItemString(1, "production_ind") = 'U') or &
	   (dw_header.GetItemString(1, "production_ind") = 'C') or &
		(dw_header.GetItemString(1, "production_ind") = 'M') or &
		(dw_header.GetItemString(1, "production_ind") = 'Y') or &
		(dw_header.GetItemString(1, "production_ind") = 'T') Then
		
		dw_manual_production.Modify("quantity.Background.Color = 12632256 " + &
								"quantity.Protect = 1 quantity.Pointer = 'Arrow!' " + &
								"quantity_sum.Background.Color = 12632256 " + &
								"quantity_sum.Protect = 1 quantity_sum.Pointer = 'Arrow!'")
//dmk sr8171
		dw_manual_production.Modify("yld_quantity.Background.Color = 12632256 " + &
								"yld_quantity.Protect = 1 " + &
								"yld_quantity_sum.Background.Color = 12632256 " + &
								"yld_quantity_sum.Protect = 1 ")			
	
		If (dw_header.GetItemString(1, "production_ind") = 'C') Then
			If  is_compare_pa_proj_selected = 'Y' Then
				chkprjqty.Enabled = True
				dw_manual_production.object.prj_quantity.Visible = True
			Else
				chkprjqty.Enabled = False
				dw_manual_production.object.prj_quantity.Visible = False
			End If
			
			If  is_compare_yields_selected = 'Y' Then
				chkyieldsqty.Enabled = True
				dw_manual_production.object.yld_quantity.Visible = True
			Else
				chkyieldsqty.Enabled = False
				dw_manual_production.object.yld_quantity.Visible = False
			End If
			
			If  is_compare_pa_actual_selected = 'Y' Then
				chkqty.Enabled = True
				dw_manual_production.object.quantity.Visible = True
			Else
				chkqty.Enabled = False
				dw_manual_production.object.quantity.Visible = False
			End If
			
			If  is_compare_mat_hand_selected = 'Y' Then
				chkmhqty.Enabled = True
				dw_manual_production.object.mat_quantity.Visible = True
			Else
				chkmhqty.Enabled = False
				dw_manual_production.object.mat_quantity.Visible = False
			End If
			
			wf_filter() 
	  Else
		   dw_manual_production.object.quantity.Visible = True
			dw_manual_production.object.yld_quantity.Visible = True
	  End if
	Else
		dw_manual_production.Modify("quantity.Background.Color = 16777215 " + &
								"quantity.Protect = 0 quantity.Pointer = 'Beam!' " + &
								"quantity_sum.Background.Color = 12632256 " + &
								"quantity_sum.Protect = 1 quantity_sum.Pointer = 'Arrow!'")
	//dmk SR8171
		dw_manual_production.Modify("yld_quantity.Background.Color = 16777215 " + &
								"yld_quantity.Protect = 0 " + &
								"yld_quantity_sum.Background.Color = 12632256 " + &
								"yld_quantity_sum.Protect = 1 ")
		dw_manual_production.object.quantity.Visible = True
		dw_manual_production.object.yld_quantity.Visible = True						
	End if
	
	dw_manual_production.ResetUpdate()
End if

//sr8171   dmk set the prev_quantity field to be used later
ll_row_count = dw_manual_production.RowCount()
ll_row = 1
If ll_row_count > 0 then
	Do
		ll_qty = dw_manual_production.GetItemNumber(ll_row, "quantity")
		ll_wgt = dw_manual_production.GetItemNumber(ll_row, "yld_quantity")
		
		if ll_qty > 0 Then
			dw_manual_production.SetItem(ll_row, "prev_avg_wgt", ll_wgt/ll_qty )	
		end if
		
		ll_row = ll_row + 1
	Loop While ll_row <= ll_row_count
	dw_manual_production.ResetUpdate()
End If



//sr7988 added new quantity fields
dw_manual_production.ScrollToRow(1)

dw_manual_production.SetColumn('yld_quantity')
dw_manual_production.SetColumn('mat_quantity')
dw_manual_production.SetColumn('var_quantity')
dw_manual_production.SetColumn('prj_quantity')
dw_manual_production.SetColumn('quantity')
dw_manual_production.SetRedraw(True)
dw_manual_production.SetFocus()

return true

end function

public function boolean wf_update ();Int			li_ret, li_check
		
Long			ll_counter, &
				ll_row, &
				ll_row_count, &
				ll_SelectedRow, &
				ll_ModifiedRow,ll_hold,ll_temp, &
				ll_qty, ll_wgt, &
				ll_prev_avg_wgt, ll_avg_wgt, &
				ll_prod_avg_wgt, &
		 		ll_sales_avg_wgt

String		ls_header, &
				ls_update,ls_hold_code,ls_hold_plant, &
				ls_compare_code,ls_compare_plant, &
				ls_ind, ls_prodInd
				
boolean     lb_goahead 

If dw_manual_production.AcceptText() = -1 Then return False

SetPointer(HourGlass!)

 
ls_header = dw_header.GetItemString(1, 'plant_code') + '~t' + &
				String(dw_header.GetItemDate(1, 'begin_date'), 'yyyy-mm-dd') + '~t' + &
				dw_header.GetItemString(1, 'shift') 

//dmk sr8171
//dmk- March 2009 - check that the qty_lb_ind = 'B', signifying
// both fields have changed. If qty has changed but not weight
//recalc an average weight from previous values
//Check that both are negative or positive
ll_row = 0
ls_prodInd = dw_header.GetItemString(1, 'production_ind')
//dmk- only check quantity and weight fields for PA type
If (ls_prodInd = 'P') Then
	Do
		ll_ModifiedRow = ll_row

		ll_ModifiedRow = dw_manual_production.GetNextModified(ll_row, Primary!)
		ll_row = ll_ModifiedRow

//ll_row = dw_manual_production.Find("qty_wgt_ind > ' ' ", 1, ll_row_count)
		If ll_row > 0 Then
			ls_ind = dw_manual_production.GetItemString(ll_row,'qty_wgt_ind') 
		//If dw_manual_production.GetItemString(ll_row,'qty_wgt_ind') <> 'B' Then
			If dw_manual_production.GetItemString(ll_row,'qty_wgt_ind') = 'Q' Then	
				ll_prev_avg_wgt = dw_manual_production.GetItemNumber(ll_row, "prev_avg_wgt")
				ll_qty = dw_manual_production.GetItemNumber(ll_row, "quantity")
				ll_wgt= dw_manual_production.GetItemNumber(ll_row, "yld_quantity") 
				If ll_qty = 0 Then
					ll_avg_wgt = 0
				Else
					if ll_wgt= 0  or IsNull(ll_prev_avg_wgt) Then
						
						ls_hold_code = dw_manual_production.GetItemString(ll_row, 'fab_product_code')
						ls_hold_plant = dw_header.GetItemString(1, 'plant_code')
						CONNECT USING SQLCA;	
						SELECT plt.production_avg_wt
								,plt.sales_avg_weight
  						INTO :ll_prod_avg_wgt 
		 			 		 ,:ll_sales_avg_wgt
  				//		FROM m_mpr_db.dbo.mt_mpr_tskuplt plt 
						FROM sku_plant plt
  						WHERE ( plt.sku_product_code = :ls_hold_code ) AND  
         					( plt.plant_code  = :ls_hold_plant)   ;
					
						If SQLCA.SQLCode = 00 Then
							If ll_prod_avg_wgt > 0 Then
								ll_avg_wgt = ll_prod_avg_wgt
							Else
								ll_avg_wgt = ll_sales_avg_wgt 
							End if	
						Else
							ll_avg_wgt = 0
						End If
					Else
						ll_avg_wgt = ll_prev_avg_wgt
					End If
				End If
				dw_manual_production.SetItem(ll_row, "yld_quantity", ll_qty * ll_avg_wgt)
			End if
			ll_qty = dw_manual_production.GetItemNumber(ll_row, "quantity")
			ll_wgt= dw_manual_production.GetItemNumber(ll_row, "yld_quantity") 

			If ((ll_qty < 0) And (ll_wgt> 0)) or &
		   	 ((ll_qty > 0) And (ll_wgt< 0)) Then
				SetMicroHelp("Quantity and Weight must be both negative or both positive.")
				dw_manual_production.SetRow(ll_row)
				dw_manual_production.ScrollToRow(ll_row)
				dw_manual_production.SetColumn("quantity")
				dw_manual_production.SetFocus()
				return false
			End If
			If (ll_qty = 0) And (ll_wgt <> 0) Then
				SetMicroHelp("Weight must be zero if quantity is zero.")
				dw_manual_production.SetRow(ll_row)
				dw_manual_production.ScrollToRow(ll_row)
				dw_manual_production.SetColumn("yld_quantity")
				dw_manual_production.SetFocus()
				return false
			End If
		End If
	Loop while ll_row > 0 
End if



// This is assuming that Plant/Weight is not a valid combination
If (ls_prodInd = 'M') or (ls_prodInd = 'Y') or (ls_prodInd = 'T') Then
	
	If (ls_prodInd = 'M') and (is_end_of_yield_flag = 'N') Then
		li_ret = MessageBox("Warning", "End of Shift from Yields has not occurred.  " + &
								"Do you still wish to update PA?", StopSign!, YesNo!, 2)
		If li_ret = 2 Then 
			return False					
		end if
	End if
	
//dmk SR4031- Changed to not equal 'Y'
	If dw_header.GetItemString(1, "End_of_shift") <> 'Y' Then
		li_ret = MessageBox("Warning", "End of Shift has not occurred.  " + &
								"Do you still wish to update PA?", StopSign!, YesNo!, 2)
	Else
		li_ret = MessageBox("Update PA", "Do you wish to update PA?", Question!, YesNo!, 2)
	End if
	If li_ret = 2 Then return False
	istr_error_info.se_event_name = "wf_update"

	/*If Not iu_pas203.nf_upd_man_prod(istr_error_info, &
												ls_header, &
												ls_update) Then return false
	*/
	

	//return true
End if

long ll_NbrRows, ll_current_row

string  ls_detail
boolean lb_onlySelected

ll_NbrRows = long(dw_manual_production.describe("evaluate('sum(if(isselected(), 1, 0) for all)',1)"))
lb_onlySelected = (ll_NbrRows > 0)

if(lb_onlySelected) then
	if(MessageBox("Selected Rows", "Only selected rows will be updated to be actual production. Are you sure you wish to update only the selected rows?", Question!, YesNo!, 1) &
	= 2) Then return False
end if



If dw_header.GetItemDate(1, 'begin_date') = Today() Then
	Choose Case dw_header.GetItemString(1, 'shift')
	Case 'A'
		If Now() < Time('6 am') Then
			If MessageBox("Update A Shift", "Today's A shift does not normally begin until after 6:00 am." + &
								"  Are you sure you want to update production?", Question!, &
								YesNo!, 2) = 2 then return False
		End if
	Case 'B'
		If Now() < Time('3 pm') Then
			If MessageBox("Update B Shift", "Today's B shift does not normally begin until after 3:00 pm." + &
								"  Are you sure you want to update production?", Question!, &
								YesNo!, 2) = 2 then return False
		End if
	End Choose
End if


ll_NbrRows = dw_manual_production.RowCount()
if(lb_onlySelected) then
	ll_current_row = dw_manual_production.GetSelectedRow(0)
else 
	ll_current_row = 1
end if

ls_header += '~t'


if(not lb_onlySelected AND (ls_prodInd = 'M' or ls_prodInd = 'Y')) then
	// use this string for the actual production check
	string lsHeaderTemp
	lsHeaderTemp = ls_header + 'C' + '~r~n'
	
	istr_error_info.se_event_name = "wf_update - " + ls_prodInd
	iw_frame.SetMicroHelp("Validating Against Database ...")
	SetPointer(HourGlass!)
//	If Not iu_pas203.nf_upd_man_prod(istr_error_info, &
//													lsHeaderTemp, &
//													"") Then return false
	If Not iu_ws_pas1.nf_pasp37fr(istr_error_info, &
													lsHeaderTemp, &
													"") Then return false
end if

choose case ls_prodInd
	case 'P'
		ls_header += 'S'
	case 'M'
		ls_header += 'H'
	case 'T'  
		ls_header += 'B'
	case else 
		ls_header += 'L'
end choose
ls_header += '~r~n'


istr_error_info.se_event_name = "wf_update - " + ls_prodInd
ls_update = ''
iw_frame.SetMicroHelp("Updating Database ...")
SetPointer(HourGlass!)

Do While ll_current_row <> 0 and ll_current_row <= ll_NbrRows   
	if(not lb_onlySelected OR dw_manual_production.IsSelected(ll_current_row)) then
		ll_row = ll_current_row

		// build update string for PA
		
		////////////	added code to fix production problem ibdkdld  begin   ////////////////////////////////////////

		//add ls_hold_code,ls_hold_plant ibdkdld
		//dmk SR8171 added yld_quantity (holds weight)	
		ls_hold_code = dw_manual_production.GetItemString(ll_row, 'fab_product_code')
		ls_update += ls_hold_code + '~t'
		ls_hold_plant = dw_manual_production.GetItemString(ll_row, 'plant')
		ls_update += ls_hold_plant + '~t'
		ls_update += String(dw_manual_production.GetItemNumber(ll_row, 'quantity')) + '~t' + &
					String(dw_manual_production.GetItemNumber(ll_row, 'yld_quantity')) + '~r~n'
		ll_Counter ++
		
		// if they are equal then we are on the row I found earlier
		If ll_counter = ll_hold Then lb_goahead = true

		
		If ll_counter > 280 and ls_hold_plant = "   " Then
			//determine if i need the next row also
			If lb_onlySelected Then
				ll_temp = dw_manual_production.GetSelectedRow(ll_row) 
			Else
				ll_temp = ll_row + 1
			End If
			  
			// if they are equal need to check further else go ahead and update
			If ll_temp = ll_row + 1 Then
				iw_frame.SetMicroHelp("Line: " + string(ll_row + 1)) // pjm testing
				If ll_row + 1 <= ll_NbrRows Then // pjm added bounds checking
					ls_compare_code = dw_manual_production.GetItemString(ll_row + 1, 'fab_product_code')
					ls_compare_plant = dw_manual_production.GetItemString(ll_row + 1 , 'plant')
					// if same product code need to look further else go ahead and update
					If ls_compare_code = ls_hold_code then
						// if plant has something in it I need the next row else go ahead and update
						If ls_compare_plant > "   " Then
							//must take this row also
							ll_hold = ll_Counter + 1
						Else
							lb_goahead = true
						End If
					Else
						lb_goahead = true
					End If
				Else
					lb_goahead = true  // pjm
				End If // pjm
			Else
				lb_goahead = true
			End IF
		End If
					
		If Mod(ll_counter, 300) = 0 or lb_goahead Then
			// update the db every 300 rows
//			If Not iu_pas203.nf_upd_man_prod(istr_error_info, &
//													ls_header, &
//													ls_update) Then return false
			If Not iu_ws_pas1.nf_pasp37fr(istr_error_info, &
												ls_header, &
												ls_update) Then return false
			lb_goahead = false										
			ll_counter = 0
			ll_hold = 0
		//////////// added code to fix production problem ibdkdld  END  //////////////////////////////////////////////
			ls_update = ''
		end if
		
	end if	
	// get next row
	if lb_onlySelected then
		ll_current_row = dw_manual_production.GetSelectedRow(ll_current_row)
	else // want to save all rows, even if they weren't changed.
		ll_current_row++ //= dw_manual_production.GetNextModified(ll_current_row, Primary!)
	end if
Loop


//If not iu_pas203.nf_upd_man_prod(istr_error_info, &
//										ls_header, &
//										ls_update) Then return false
If not iu_ws_pas1.nf_pasp37fr(istr_error_info, &
										ls_header, &
										ls_update) Then return false
iw_frame.SetMicroHelp("Update Successful")


ll_current_row = dw_manual_production.GetSelectedRow(0)
Do While ll_current_row <> 0 and ll_current_row <= ll_NbrRows
	if(dw_manual_production.IsSelected(ll_current_row)) then
		dw_manual_production.SetItem(ll_current_row, 'pa_asterisk', "*")
		dw_manual_production.SetItemStatus(ll_current_row, "pa_asterisk", Primary!, NotModified!)
	end if
	ll_current_row = dw_manual_production.GetSelectedRow(ll_current_row)
Loop


dw_manual_production.SelectRow(0, False)
dw_manual_production.ResetUpdate()
				

if(ls_prodInd = 'M' or ls_prodInd = 'Y') then
	MessageBox("Success", "ACTUAL PRODUCTION UPDATE SUCCESSFUL FOR PLANT: " + dw_header.GetItemString(1, 'plant_code') + ", SHIFT: " + dw_header.GetItemString(1, 'shift')  + ".")
end if

return true

/*
// If Plant/Boxes is selected, then fire off the async rpc
// This is assuming that Plant/Weight is not a valid combination
If (ls_prodInd = 'M') or (ls_prodInd = 'Y') Then
	ls_header += '~t' + ls_prodInd + '~r~n'
	If ib_async_running Then
		MessageBox("Update PA", "PA update is already running")
		return False
	End if
	
	If (ls_prodInd = 'M') and (is_end_of_yield_flag = 'N') Then
		li_ret = MessageBox("Warning", "End of Shift from Yields has not occurred.  " + &
								"Do you still wish to update PA?", StopSign!, YesNo!, 2)
	End if
	If li_ret = 2 Then return False
//dmk SR4031- Changed to not equal 'Y'
	If dw_header.GetItemString(1, "End_of_shift") <> 'Y' Then
		li_ret = MessageBox("Warning", "End of Shift has not occurred.  " + &
								"Do you still wish to update PA?", StopSign!, YesNo!, 2)
	Else
		li_ret = MessageBox("Update PA", "Do you wish to update PA?", Question!, YesNo!, 2)
	End if
	If li_ret = 2 Then return False
	
//dmk this call just checks to see if production has been
//updated, if they are okay with that it will continue with
//the other rpc call.
	If Not iu_pas203.nf_upd_man_prod(istr_error_info, &
												ls_header, &
												ls_update) Then return false
	
	istr_error_info.se_event_name = "wf_update"

	If iu_pas203.nf_upd_man_prod_async(istr_error_info, &
												ls_header, &
												ii_async_commhandle) Then
		iw_frame.SetMicroHelp("Updating Database ...")
		Timer(1, This)
		This.TriggerEvent("Timer")
		This.Pointer = 'async.cur'
		dw_header.Modify("DataWindow.Pointer = 'async.cur'")
		dw_manual_production.Modify("DataWindow.Pointer = 'async.cur'")
		ib_async_running = True
		return True
	Else
		return False
	End if
End if

// If we get here, we know that PA is selected
ll_row_count = dw_manual_production.RowCount()
//commented out check on uom, doesn't update by weight
//sending the production ind to the rpc instead WR11859

//Choose Case dw_header.GetItemString(1, 'uom_ind')
//Case 'B'
	//ls_header += '~tA~r~n'
	ls_header += '~t' + ls_prodInd + '~r~n'
	istr_error_info.se_event_name = "wf_update - PA/Box"

	If MessageBox("Selected Rows", "Only selected rows will be updated to be " + &
					"actual production.  Are you sure you " + &
					"wish to update only the selected rows?", &
					Question!, YesNo!, 1) = 2 Then return False
 		
//Case 'W'
//	ls_header += '~tW~r~n'
//	istr_error_info.se_event_name = "wf_update - PA/Weight"
//End Choose

If dw_header.GetItemDate(1, 'begin_date') = Today() Then
	Choose Case dw_header.GetItemString(1, 'shift')
	Case 'A'
		If Now() < Time('6 am') Then
			If MessageBox("Update A Shift", "Today's A shift does not normally begin until after 6:00 am." + &
								"  Are you sure you want to update production?", Question!, &
								YesNo!, 2) = 2 then return False
		End if
	Case 'B'
		If Now() < Time('3 pm') Then
			If MessageBox("Update B Shift", "Today's B shift does not normally begin until after 3:00 pm." + &
								"  Are you sure you want to update production?", Question!, &
								YesNo!, 2) = 2 then return False
		End if
	End Choose
End if

ls_update = ''
ll_row = 0
iw_frame.SetMicroHelp("Updating Database ...")
SetPointer(HourGlass!)
Do
	ll_SelectedRow = ll_row
	ll_ModifiedRow = ll_row

	
	ll_ModifiedRow = dw_manual_production.GetNextModified(ll_row, Primary!)
	ll_SelectedRow = dw_manual_production.GetSelectedRow(ll_row)

	If (ll_ModifiedRow < ll_SelectedRow) Then
		If ll_ModifiedRow > 0 Then
			ll_row = ll_ModifiedRow
		Else
			ll_Row = ll_SelectedRow
		End if
	Elseif (ll_SelectedRow < ll_ModifiedRow) Then
		If ll_SelectedRow > 0 Then
			ll_row = ll_SelectedRow
		Else
			ll_Row = ll_ModifiedRow
		End if
	Else
		// they are equal
		ll_row = ll_ModifiedRow
	End if

	If ll_row < 1 Then Exit
////////////	added code to fix production problem ibdkdld  begin   ////////////////////////////////////////

	//add ls_hold_code,ls_hold_plant ibdkdld
	//dmk SR8171 added yld_quantity (holds weight)	
	ls_hold_code = dw_manual_production.GetItemString(ll_row, 'fab_product_code')
	ls_update += ls_hold_code + '~t'
	ls_hold_plant = dw_manual_production.GetItemString(ll_row, 'plant')
	ls_update += ls_hold_plant + '~t'
	ls_update += String(dw_manual_production.GetItemNumber(ll_row, 'quantity')) + '~t' + &
				String(dw_manual_production.GetItemNumber(ll_row, 'yld_quantity')) + '~r~n'
	ll_Counter ++
	
	// if they are equal then we are on the row I found earlier
	If ll_counter = ll_hold Then lb_goahead = true

		
	If ll_counter > 280 and ls_hold_plant = "   " Then
		//determine if i need the next row also
		ll_temp = dw_manual_production.GetSelectedRow(ll_row) 
		// if they are equal need to check further else go ahead and update
		If ll_temp = ll_row + 1 Then
			ls_compare_code = dw_manual_production.GetItemString(ll_row + 1, 'fab_product_code')
			ls_compare_plant = dw_manual_production.GetItemString(ll_row + 1 , 'plant')
			// if same product code need to look further else go ahead and update
			If ls_compare_code = ls_hold_code then
				// if plant has something in it I need the next row else go ahead and update
				If ls_compare_plant > "   " Then
					//must take this row also
					ll_hold = ll_Counter + 1
				Else
					lb_goahead = true
				End If
			Else
				lb_goahead = true
			End If
		Else
			lb_goahead = true
		End IF
	End If
					
	If Mod(ll_counter, 300) = 0 or lb_goahead Then
		// update the string every 300 rows
		If Not iu_pas203.nf_upd_man_prod(istr_error_info, &
												ls_header, &
												ls_update) Then return false
		lb_goahead = false										
		ll_counter = 0
//////////// added code to fix production problem ibdkdld  END  //////////////////////////////////////////////
		ls_update = ''
	End if
Loop while ll_row > 0 or ll_SelectedRow > 0

If not iu_pas203.nf_upd_man_prod(istr_error_info, &
										ls_header, &
										ls_update) Then return false
iw_frame.SetMicroHelp("Update Successful")

dw_manual_production.SelectRow(0, False)
dw_manual_production.ResetUpdate()
return true
*/
end function

event deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')

iw_frame.im_menu.mf_Enable('m_deleterow')

//iw_frame.im_menu.m_Holding5.m_addrow.Text = is_AddRowText 
iw_frame.im_menu.mf_set_menu_item_text("m_addrow", is_AddRowText)
end event

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')

iw_frame.im_menu.mf_Disable('m_deleterow')

//is_AddRowText = iw_frame.im_menu.m_Holding5.m_addrow.Text
//iw_frame.im_menu.m_Holding5.m_addrow.Text = 'Duplicate &Row' 

is_AddRowText = iw_frame.im_menu.mf_get_menu_item_text("m_addrow")
iw_frame.im_menu.mf_set_menu_item_text("m_addrow", "Duplicate &Row")
end event

event timer;call super::timer;Boolean	lb_ret

String	ls_header


If iu_pas203.nf_async_complete(ii_async_commhandle) Then
	ls_header = Space(20)
	lb_ret = iu_ws_pas1.nf_pasp38fr(istr_error_info, &
												ls_header)


	This.Pointer = ''
	dw_header.Modify("DataWindow.Pointer = ''")
	dw_manual_production.Modify("DataWindow.Pointer = ''")
	
	If lb_ret Then iw_frame.SetMicroHelp("Update Successful")

	iu_pas203.nf_Release_CommHandle(ii_async_commhandle)
	ii_async_commhandle = 0
	ib_async_running = False
	Timer(0, This)
End if
end event

event ue_postopen;call super::ue_postopen;If Not IsValid(iu_pas203) Then
	iu_pas203 = Create u_pas203
	If Message.ReturnValue = -1 Then 
		Close(This)
		Return
	End if
End if

iu_ws_pas1 = Create u_ws_pas1

istr_error_info.se_app_name = Message.nf_Get_App_ID()
istr_error_info.se_window_name = "Man/Prod"
istr_error_info.se_user_id = sqlca.userid


//sr7988 default radio buttons
This.rb_variances.PostEvent(Clicked!)
//This.rb_yields_mat.PostEvent(Clicked!)

string ls_bg
ls_bg = dw_header.Describe("plant_descr.Background.Color")
dw_area.Modify("DataWindow.Detail.Color='" + ls_bg + "'")
dw_area.Modify("area_name_code.background.color = '" + ls_bg + "'")
dw_area.Object.area_name_code.protect = 1
dw_area.InsertRow(1)



string ls_parms
ls_parms = Message.StringParm

// if values passed in, call wf_retrieve and use that data else run inquire.
// for some reason, opening this window from file, open passes the program name in as message.stringparm so check for that.
If not iw_frame.iu_string.nf_IsEmpty(ls_parms) AND ls_parms <> "w_pas_manual_production" Then
	wf_parse_string(ls_parms, false)
end if
iw_frame.im_menu.m_file.m_inquire.TriggerEvent(Clicked!)


end event

event resize;call super::resize;integer li_x		= 28
integer li_y		= 233

If This.Height > dw_manual_production.Y Then
	dw_manual_production.Height = This.Height - dw_manual_production.Y - 121
End if


end event

on closequery;call w_base_sheet_ext::closequery;If KeyDown(KeyShift!) And KeyDown(KeyControl!) Then 
	Message.ReturnValue = 0
	return
End if

If ib_async_running Then
	MessageBox("Production Update", "This window cannot be closed until the Update task is completed")
	Message.ReturnValue = 1
End if
end on

on w_pas_manual_production.create
int iCurrent
call super::create
this.group_label_t=create group_label_t
this.group_t=create group_t
this.btnundoselected=create btnundoselected
this.chkmhqty=create chkmhqty
this.chkyieldsqty=create chkyieldsqty
this.chkqty=create chkqty
this.chkprjqty=create chkprjqty
this.dw_area=create dw_area
this.rb_all_products=create rb_all_products
this.rb_variances=create rb_variances
this.dw_header=create dw_header
this.dw_scroll_product=create dw_scroll_product
this.dw_manual_production=create dw_manual_production
this.gb_display=create gb_display
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.group_label_t
this.Control[iCurrent+2]=this.group_t
this.Control[iCurrent+3]=this.btnundoselected
this.Control[iCurrent+4]=this.chkmhqty
this.Control[iCurrent+5]=this.chkyieldsqty
this.Control[iCurrent+6]=this.chkqty
this.Control[iCurrent+7]=this.chkprjqty
this.Control[iCurrent+8]=this.dw_area
this.Control[iCurrent+9]=this.rb_all_products
this.Control[iCurrent+10]=this.rb_variances
this.Control[iCurrent+11]=this.dw_header
this.Control[iCurrent+12]=this.dw_scroll_product
this.Control[iCurrent+13]=this.dw_manual_production
this.Control[iCurrent+14]=this.gb_display
end on

on w_pas_manual_production.destroy
call super::destroy
destroy(this.group_label_t)
destroy(this.group_t)
destroy(this.btnundoselected)
destroy(this.chkmhqty)
destroy(this.chkyieldsqty)
destroy(this.chkqty)
destroy(this.chkprjqty)
destroy(this.dw_area)
destroy(this.rb_all_products)
destroy(this.rb_variances)
destroy(this.dw_header)
destroy(this.dw_scroll_product)
destroy(this.dw_manual_production)
destroy(this.gb_display)
end on

event close;call super::close;If IsValid(iu_pas203) Then
	Destroy iu_pas203
End if

Destroy iu_ws_pas1
end event

event ue_fileprint;call super::ue_fileprint;String					ls_temp

DataWindowChild		dwc_header, &
							dwc_detail

DataStore				lds_print


If not ib_print_ok Then return

lds_print = Create u_print_datastore
lds_print.DataObject = 'd_man_prod_print'

lds_print.GetChild('dw_header', dwc_header)
lds_print.GetChild('dw_detail', dwc_detail)

//rev#03 Changed sharedata to importstring.

//dw_header.ShareData(dwc_header)
//dw_manual_production.ShareData(dwc_detail)

ls_temp = dw_header.object.datawindow.data
dwc_header.ImportString(ls_temp)

ls_temp = dw_manual_production.object.datawindow.data
dwc_detail.ImportString(ls_temp)

/*
//sr7988 
IF rb_pa_mat.Checked THEN
	dwc_detail.Modify("var_quantity.Expression = 'quantity - mat_quantity'")
End if

IF rb_pa_yields.Checked THEN
	dwc_detail.Modify("var_quantity.Expression = 'quantity - yld_quantity'")
End if*/

//rev#02 Changed the color from transparent.
dwc_header.Modify("plant_code.Border='0' plant_descr.Border='0' begin_date.Border='0' shift.Border='0' " + &
	"plant_code.background.color ='16777215' plant_descr.background.color ='16777215' begin_date.background.color ='16777215' shift.background.color ='16777215' " + &
	"plant_code_t.background.color ='16777215' begin_date_t.background.color ='16777215' shift_t.background.color ='16777215' " + &
	"end_of_shift.Border='0' end_of_shift_time.Border='0' end_of_shift.background.color ='16777215' end_of_shift_time.background.color ='16777215'" + &
	"production_ind.background.color ='16777215' uom_ind.background.color ='16777215'" + &
	"end_of_shift_t.background.color ='16777215' production_ind_t.background.color ='16777215' uom_ind_t.background.color ='16777215'")

// sr 7988 added yld, mh and variance quantity fields
//dmk sr8171 changes to print weight
dwc_detail.Modify("fab_product_code.Border='0' fab_product_descr.Border='0' plant.Border='0' " + &
	"quantity.Border='0' quantity_sum.Border='0' plant.Background.Color='16777215'")
If (dw_header.GetItemString(1, "production_ind") = 'C') Then
	dwc_detail.Modify("yld_quantity.Border='0' yld_quantity_sum.Border='0' " + &
	"mat_quantity.Border='0' mat_quantity_sum.Border='0' " + &
	"var_quantity.Border='0' var_quantity_sum.Border='0' " )
Else
	dwc_detail.Modify("t_pa.Visible = False")
	dwc_detail.Modify("t_yld.Visible = False")
//	dwc_detail.Modify("t_yld_qty.Visible = False")
	dwc_detail.Modify("t_mat.Visible = False")
	dwc_detail.Modify("t_mat_qty.Visible = False")
	dwc_detail.Modify("t_variance.Visible = False")
	dwc_detail.Modify("t_var_qty.Visible = False")
	dwc_detail.Modify("l_2.Visible = False")
	dwc_detail.Modify("l_3.Visible = False")
	dwc_detail.Modify("l_4.Visible = False")
//	dwc_detail.Modify("yld_quantity.Visible = False")
	dwc_detail.Modify("mat_quantity.Visible = False")
	dwc_detail.Modify("var_quantity.Visible = False")
//	dwc_detail.Modify("yld_quantity_sum.Visible = False")
	dwc_detail.Modify("mat_quantity_sum.Visible = False")
	dwc_detail.Modify("var_quantity_sum.Visible = False")
	If (dw_header.GetItemString(1, "production_ind") = 'M') Or &
		(dw_header.GetItemString(1, "production_ind") = 'Y') Then
		dwc_detail.Modify("t_yld_qty.Visible = False")
		dwc_detail.Modify("yld_quantity.Visible = False")
		dwc_detail.Modify("yld_quantity_sum.Visible = False")
	Else
		dwc_detail.Modify("t_yld.Text=' ' t_yld_qty.Text = 'Weight' ")
		dwc_detail.Modify("yld_quantity.Border='0' yld_quantity_sum.Border='0' ")
	End if
End if
 
lds_print.Print()

Destroy lds_print

return
end event

event ue_revisions;call super::ue_revisions;/*****************************************************************
**   REVISION NUMBER: rev#01
**   PROJECT NUMBER:  support
**   DATE:				 july 99            
**   PROGRAMMER:      Rich Muckey
**   PURPOSE:         Added End of Shift Time.
**                    
******************************************************************
**   REVISION NUMBER: rev#02
**   PROJECT NUMBER:  support
**   DATE:            july 99
**   PROGRAMMER:      Rich Muckey
**   PURPOSE:         changed the background color for the header
**							 because of the pb bug where it spools several
**							 megs if the color is transparent.
******************************************************************
**   REVISION NUMBER: rev#03 
**   PROJECT NUMBER:  support
**   DATE:            july 99
**   PROGRAMMER:      Rich Muckey
**   PURPOSE:         Changed the print from share data to import
**							 string.
******************************************************************
**   REVISION NUMBER: Rev #04
**   PROJECT NUMBER:  SR4031
**   DATE:            Nov 2008          
**   PROGRAMMER:      Donna Keairns     
**   PURPOSE:         added logic for multiple end of shift ind        
******************************************************************/

end event

type group_label_t from statictext within w_pas_manual_production
integer x = 1247
integer y = 339
integer width = 176
integer height = 74
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 268435456
string text = "Group:"
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type group_t from statictext within w_pas_manual_production
integer x = 1426
integer y = 339
integer width = 951
integer height = 74
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 268435456
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type btnundoselected from commandbutton within w_pas_manual_production
integer x = 1287
integer y = 448
integer width = 464
integer height = 93
integer taborder = 20
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "Undo Selected"
end type

event clicked;long ll_NbrRows, ll_current_row
integer li_ret
string ls_header, ls_detail




boolean     lb_goahead 


ll_NbrRows = long(dw_manual_production.describe("evaluate('sum(if(isselected(), 1, 0) for all)',1)"))

if(ll_NbrRows = 0) then
	iw_frame.SetMicroHelp("Select the products to UNDO the actual update.")
	return
else
	li_ret = MessageBox("Warning", "The selected products will be set back to projected quantities. Continue?", StopSign!, YesNo!, 2)
	If li_ret = 2 Then return
end if

SetPointer(HourGlass!)

ll_NbrRows = dw_manual_production.RowCount()
ll_current_row = dw_manual_production.GetSelectedRow(0)

ls_header = dw_header.GetItemString(1, "plant_code")                     + '~t' &
			 + string(dw_header.GetItemDate(1, "begin_date"), "yyyy-mm-dd") + '~t' &
			 + dw_header.GetItemString(1, "shift")                          + '~t' &
			 + 'U'                                                          + '~r~n'
ls_detail = ""
string ls_hold_code, ls_hold_plant, ls_compare_code, ls_compare_plant
long ll_hold, ll_temp, ll_counter, ll_row
Do While ll_current_row <> 0 and ll_current_row <= ll_NbrRows
	if(dw_manual_production.IsSelected(ll_current_row) /*and dw_manual_production.GetItemString(ll_current_row, 'pa_asterisk') = '*'*/) then
		// update PA, set all actual production values to projected quantities.

		
		
		ll_row = ll_current_row

		
		ls_hold_code = dw_manual_production.GetItemString(ll_row, 'fab_product_code')
		ls_hold_plant = dw_manual_production.GetItemString(ll_row, 'plant')
		ll_Counter ++
		
		/*ls_detail += dw_manual_production.GetItemString(ll_current_row, 'fab_product_code')     + '~t' &
		          +  dw_manual_production.GetItemString(ll_current_row, 'plant')                + '~t' &
                +  String(dw_manual_production.GetItemNumber(ll_current_row, 'quantity'))     + '~t'  &
					 +  String(dw_manual_production.GetItemNumber(ll_current_row, 'yld_quantity')) + '~r~n'*/
					 
		ls_detail += ls_hold_code + '~t' &
					 +  ls_hold_plant + '~t' &
					 +  String(dw_manual_production.GetItemNumber(ll_current_row, 'quantity'))     + '~t'  &
					 +  String(dw_manual_production.GetItemNumber(ll_current_row, 'yld_quantity')) + '~r~n'
					 
		// if they are equal then we are on the row I found earlier
		If ll_counter = ll_hold Then lb_goahead = true

		
		If ll_counter > 280 and ls_hold_plant = "   " Then
			//determine if i need the next row also
			ll_temp = dw_manual_production.GetSelectedRow(ll_row) 
			// if they are equal need to check further else go ahead and update
			If ll_temp = ll_row + 1 Then
				ls_compare_code = dw_manual_production.GetItemString(ll_row + 1, 'fab_product_code')
				ls_compare_plant = dw_manual_production.GetItemString(ll_row + 1 , 'plant')
				
				// if same product code need to look further else go ahead and update
				If ls_compare_code = ls_hold_code then
					// if plant has something in it I need the next row else go ahead and update
					If ls_compare_plant > "   " Then
						//must take this row also
						ll_hold = ll_Counter + 1
					Else
						lb_goahead = true
					End If
				Else
					lb_goahead = true
				End If
			Else
				lb_goahead = true
			End IF
		End If
					
		If Mod(ll_counter, 300) = 0 or lb_goahead Then // update the db every 300 rows
			//If Not iu_pas203.nf_upd_man_prod(istr_error_info, ls_header, ls_detail) Then return 0
			If Not iu_ws_pas1.nf_pasp37fr(istr_error_info, ls_header, ls_detail) Then return 0
			lb_goahead = false										
			ll_counter = 0
			ls_detail = ''
		end if
	end if
	ll_current_row = dw_manual_production.GetSelectedRow(ll_current_row)
Loop

//If len(ls_detail) > 0 and Not iu_pas203.nf_upd_man_prod(istr_error_info, ls_header, ls_detail) Then return 0 
If len(ls_detail) > 0 and Not iu_ws_pas1.nf_pasp37fr(istr_error_info, ls_header, ls_detail) Then return 0 
iw_frame.SetMicroHelp("Update Successful.")


ll_current_row = dw_manual_production.GetSelectedRow(0)
Do While ll_current_row <> 0 and ll_current_row <= ll_NbrRows
	if(dw_manual_production.IsSelected(ll_current_row)) then

		dw_manual_production.SetItem(ll_current_row, 'pa_asterisk', "")
		dw_manual_production.SetItem(ll_current_row, 'quantity', 0)
		dw_manual_production.SetItem(ll_current_row, 'yld_quantity', 0)
		
		dw_manual_production.SetItemStatus(ll_current_row, "pa_asterisk", Primary!, NotModified!)
		dw_manual_production.SetItemStatus(ll_current_row, "quantity", Primary!, NotModified!)
		dw_manual_production.SetItemStatus(ll_current_row, "yld_quantity", Primary!, NotModified!)
	end if
	ll_current_row = dw_manual_production.GetSelectedRow(ll_current_row)
Loop


















/*
Int			li_ret, li_check
		
Long			ll_counter, &
				ll_row, &
				ll_row_count, &
				ll_SelectedRow, &
				ll_ModifiedRow,ll_hold,ll_temp, &
				ll_qty, ll_wgt, &
				ll_prev_avg_wgt, ll_avg_wgt, &
				ll_prod_avg_wgt, &
		 		ll_sales_avg_wgt

String		ls_header, &
				ls_update,ls_hold_code,ls_hold_plant, &
				ls_compare_code,ls_compare_plant, &
				ls_ind, ls_prodInd
				


If dw_manual_production.AcceptText() = -1 Then return False


ll_row = 0
ls_prodInd = dw_header.GetItemString(1, 'production_ind')


long ll_NbrRows, ll_current_row

string  ls_detail
boolean lb_onlySelected


Do While ll_current_row <> 0 and ll_current_row <= ll_NbrRows
	if(not lb_onlySelected OR dw_manual_production.IsSelected(ll_current_row)) then
		ll_row = ll_current_row

		// build update string for PA
		
		////////////	added code to fix production problem ibdkdld  begin   ////////////////////////////////////////

		//add ls_hold_code,ls_hold_plant ibdkdld
		//dmk SR8171 added yld_quantity (holds weight)	
		ls_hold_code = dw_manual_production.GetItemString(ll_row, 'fab_product_code')
		ls_update += ls_hold_code + '~t'
		ls_hold_plant = dw_manual_production.GetItemString(ll_row, 'plant')
		ls_update += ls_hold_plant + '~t'
		ls_update += String(dw_manual_production.GetItemNumber(ll_row, 'quantity')) + '~t' + &
					String(dw_manual_production.GetItemNumber(ll_row, 'yld_quantity')) + '~r~n'
		ll_Counter ++
		
		// if they are equal then we are on the row I found earlier
		If ll_counter = ll_hold Then lb_goahead = true

		
		If ll_counter > 280 and ls_hold_plant = "   " Then
			//determine if i need the next row also
			ll_temp = dw_manual_production.GetSelectedRow(ll_row) 
			// if they are equal need to check further else go ahead and update
			If ll_temp = ll_row + 1 Then
				ls_compare_code = dw_manual_production.GetItemString(ll_row + 1, 'fab_product_code')
				ls_compare_plant = dw_manual_production.GetItemString(ll_row + 1 , 'plant')
				// if same product code need to look further else go ahead and update
				If ls_compare_code = ls_hold_code then
					// if plant has something in it I need the next row else go ahead and update
					If ls_compare_plant > "   " Then
						//must take this row also
						ll_hold = ll_Counter + 1
					Else
						lb_goahead = true
					End If
				Else
					lb_goahead = true
				End If
			Else
				lb_goahead = true
			End IF
		End If
					
		If Mod(ll_counter, 300) = 0 or lb_goahead Then
			// update the db every 300 rows
			If Not iu_pas203.nf_upd_man_prod(istr_error_info, &
													ls_header, &
													ls_update) Then return false
			lb_goahead = false										
			ll_counter = 0
		//////////// added code to fix production problem ibdkdld  END  //////////////////////////////////////////////
			ls_update = ''
		end if
		
	end if	
	// get next row
	if lb_onlySelected then
		ll_current_row = dw_manual_production.GetSelectedRow(ll_current_row)
	else // want to save all rows, even if they weren't changed.
		ll_current_row++ //= dw_manual_production.GetNextModified(ll_current_row, Primary!)
	end if
Loop


If not iu_pas203.nf_upd_man_prod(istr_error_info, &
										ls_header, &
										ls_update) Then return false
iw_frame.SetMicroHelp("Update Successful")

dw_manual_production.SelectRow(0, False)
dw_manual_production.ResetUpdate()
return true


*/








end event

type chkmhqty from checkbox within w_pas_manual_production
string tag = "mat_quantity"
integer x = 3013
integer y = 701
integer width = 77
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "none"
end type

event clicked;wf_calc_variance()
end event

type chkyieldsqty from checkbox within w_pas_manual_production
string tag = "yld_quantity"
integer x = 2619
integer y = 701
integer width = 77
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "none"
end type

event clicked;wf_calc_variance()
end event

type chkqty from checkbox within w_pas_manual_production
string tag = "quantity"
integer x = 2249
integer y = 701
integer width = 77
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
end type

event clicked;wf_calc_variance()
end event

type chkprjqty from checkbox within w_pas_manual_production
string tag = "prj_quantity"
integer x = 1916
integer y = 701
integer width = 77
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
end type

event clicked;wf_calc_variance()
end event

type dw_area from u_sched_area_sect within w_pas_manual_production
integer x = 51
integer y = 339
integer width = 1057
integer height = 74
integer taborder = 50
end type

type rb_all_products from radiobutton within w_pas_manual_production
integer x = 2432
integer y = 480
integer width = 344
integer height = 64
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "All Products"
end type

event clicked;wf_filter()
end event

type rb_variances from radiobutton within w_pas_manual_production
integer x = 2428
integer y = 400
integer width = 344
integer height = 67
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Variances"
boolean checked = true
end type

event clicked;wf_filter()
end event

type dw_header from u_base_dw_ext within w_pas_manual_production
integer x = 4
integer y = 10
integer width = 3379
integer height = 317
integer taborder = 30
string dataobject = "d_pas_man_prod_header"
boolean border = false
end type

on constructor;call u_base_dw_ext::constructor;ib_updateable = False
This.InsertRow(0)
end on

type dw_scroll_product from u_base_dw_ext within w_pas_manual_production
integer x = 26
integer y = 458
integer width = 1192
integer height = 80
integer taborder = 10
string dataobject = "d_scroll_product"
boolean border = false
end type

event constructor;call super::constructor;This.InsertRow(0)
ib_updateable = False
end event

event editchanged;call super::editchanged;Long	ll_row, &
		ll_first_row, &
		ll_last_row

ll_row = dw_manual_production.Find("fab_product_code >= '" + data + "'",1,dw_manual_production.RowCount()+1)

If ll_row > 0 Then 
	dw_manual_production.ScrollToRow(ll_row)
	dw_manual_production.SetRow(ll_row + 1)
End If

ll_first_row = Long(dw_manual_production.Object.DataWindow.FirstRowOnPage)
ll_last_row = Long(dw_manual_production.Object.DataWindow.LastRowOnPage)

If ll_row > ll_first_row and ll_row <= ll_last_row Then 
	dw_manual_production.SetRedraw(False)
	dw_manual_production.ScrollToRow(ll_row + ll_last_row - ll_first_row)
	dw_manual_production.ScrollToRow(ll_row)
	dw_manual_production.SetRow(ll_row + 1)
	dw_manual_production.SetRedraw(True)
End If
end event

type dw_manual_production from u_base_dw_ext within w_pas_manual_production
integer x = 37
integer y = 771
integer width = 3661
integer height = 1488
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_pas_manual_production"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;call super::itemchanged;DataWindowChild	ldwc_plant

Long			ll_rowcount

String		ls_ProductCode

//data = GetText()
//row = This.GetRow()
ll_rowcount = This.RowCount()


Choose Case dwo.name
Case "quantity" 
	If Not IsNumber(data) Then
		iw_frame.SetMicroHelp("Quantity must be a number")
		Return 1
	End If
//	If Dec(GetText()) < 0  Then
//		iw_frame.SetMicroHelp("Quantity must be greater than zero")
//		Return 1
//	End If

	If data = String(This.GetItemNumber(row, 'quantity', Primary!, True)) Then
		This.SetItemStatus(row, 0, Primary!, NotModified!)
	End if
	//dmk SR8171
	//dmk March 2009  Set the qty_lb_ind, 'B', both qty and lbs fields 
	// have changed, if ''P', pounds have changed, if 'Q', quantity has
	// changed.  wf_update will check that both have changed
	If (dw_header.GetItemString(1, "production_ind") = 'M') or &
		(dw_header.GetItemString(1, "production_ind") = 'Y') or &
		(dw_header.GetItemString(1, "production_ind") = 'C') Then
		// do nothing
	Else
		IF This.GetItemString(row, "qty_wgt_ind") = 'B' Then
			//do nothing
		ELSE
			IF This.GetItemString(row, "qty_wgt_ind") = 'P' THEN
				This.SetItem( row, "qty_wgt_ind", 'B')
			ELSE
				This.SetItem(row, "qty_wgt_ind", 'Q')
			END IF
		END IF
	End If

Case 'plant'

	// Make sure they entered a valid plant
	If Not iw_frame.iu_string.nf_IsEmpty(data) Then
		This.GetChild('plant', ldwc_plant)
		If ldwc_plant.Find("location_code = '" + data + "'", 1, &
							ldwc_plant.RowCount()) < 1 Then
			iw_Frame.SetMicroHelp(data + " is an invalid Destination Plant")
			return 1
 		End if
	End if

	//Make sure there are no duplicate destination plants
	ls_ProductCode = This.GetItemString(row, 'fab_product_code')

	If This.Find("plant = '" + data + "' And fab_product_code = '" + &
						ls_ProductCode + "'", 1, row - 1) > 0 And row > 1 Then
		iw_Frame.SetMicroHelp("Destination Plants cannot be duplicates")
		return 1
	End if

	If This.Find("plant = '" + data + "' And fab_product_code = '" + &
						ls_ProductCode + "'", row + 1, ll_rowcount) > 0 And &
						row < ll_rowcount Then
		iw_Frame.SetMicroHelp("Destination Plants cannot be duplicates")
		return 1
	End if
//dmk sr8171
Case 'yld_quantity'
	If (dw_header.GetItemString(1, "production_ind") = 'M') or &
		(dw_header.GetItemString(1, "production_ind") = 'Y') or &
		(dw_header.GetItemString(1, "production_ind") = 'C') Then
		// do nothing
	Else
		IF This.GetItemString(row, "qty_wgt_ind") = 'B' Then
			//do nothing
		ELSE
			IF This.GetItemString(row, "qty_wgt_ind") = 'Q' THEN
				This.SetItem( row, "qty_wgt_ind", 'B')
			ELSE
				This.SetItem( row, "qty_wgt_ind", 'P')
			END IF
  		END IF
	End if
	
End Choose

If dw_header.GetItemString(1, 'production_ind') = 'P' And &
	dw_header.GetItemString(1, 'uom_ind') = 'B' Then
		// only Select rows for Pa/Boxes
		// Select all rows for this product

		If iw_frame.iu_string.nF_IsEmpty(ls_ProductCode) Then
				ls_ProductCode = This.GetItemString(row, 'fab_product_code')
		End if
		row = 0
		Do
			row = This.Find("fab_product_code = '" + ls_ProductCode + "'", &
										row + 1, ll_rowcount)
			If row < 1 Then Exit
			This.SelectRow(row, True)
			If This.GetItemStatus(row, 0, Primary!) = NotModified! Then
				This.uf_ChangeRowStatus(row, DataModified!)			
			End if
		Loop While row > 0 And row < ll_rowcount
End if
Return 0
end event

event itemerror;call super::itemerror;Choose Case This.GetColumnName() 
Case "quantity" 
	If Not IsNumber(data) Then
		iw_frame.SetMicroHelp("Quantity must be a number")
	End If
	If Dec(data) < 0  Then
		iw_frame.SetMicroHelp("Quantity must be greater than zero")
	End If
End Choose


This.SelectText(1, Len(This.GetText()))
return 1
end event

event rbuttondown;call super::rbuttondown;m_man_prod_popup	lm_popup

If dw_header.GetItemString(1, 'production_ind') = 'P' And &
	dw_header.GetItemString(1, 'uom_ind') = 'B' Then
		// only Select rows for Pa/Boxes

		lm_popup = Create m_man_prod_popup

		lm_popup.m_manualproduction.PopMenu(iw_frame.PointerX(), iw_frame.PointerY())

		Destroy lm_popup

Else
	iw_frame.SetMicroHelp("Popup menu is only valid for PA/Boxes")
End if
end event

event clicked;call super::clicked;Boolean		lb_product_code_is_the_same

Long	ll_row

String	ls_columnName, & 
			ls_product, ls_prodInd
			
ls_prodInd = dw_header.GetItemString(1, 'production_ind')

If (ls_prodInd = 'P' or ls_prodInd = 'M' or ls_prodInd = 'T' or ls_prodInd = 'Y') And &
	dw_header.GetItemString(1, 'uom_ind') = 'B' Then
		// only Select rows for Pa/Boxes
	ls_columnName = dwo.Name
	If row > 0 and ls_ColumnName <> 'quantity' Then
		This.SelectRow(row, Not IsSelected(row))
		ls_product = This.GetItemString(row, 'fab_product_code')
		ll_row = row + 1
		lb_product_code_is_the_same = true
		do while lb_product_code_is_the_same
			If ll_row > This.Rowcount() Then
				lb_product_code_is_the_same = false
			Else
				if This.GetItemString(ll_row, 'fab_product_code') = ls_product then
					This.SelectRow(ll_Row, Not IsSelected(ll_Row))
					ll_row ++
				else
					lb_product_code_is_the_same = false
				End If
			End if
		Loop
		ll_row = row - 1
		lb_product_code_is_the_same = true
		do while lb_product_code_is_the_same
			If ll_row < 1 Then
				lb_product_code_is_the_same = false
			Else
				if This.GetItemString(ll_row, 'fab_product_code') = ls_product then
					This.SelectRow(ll_Row, Not IsSelected(ll_Row))
					ll_row --
				else
					lb_product_code_is_the_same = false
				End If
			End if
		Loop
	End if
End if


end event

event doubleclicked;call super::doubleclicked;Boolean	lb_Selectable

Long		ll_ClickedRow

String	ls_ColumnType, &
			ls_Protected, &
			ls_ClickedColumn

ll_ClickedRow = row
ls_ClickedColumn = dwo.Name

IF ll_ClickedRow < 1 or Lower(ls_ClickedColumn) = "datawindow" THEN Return

If This.Describe("DataWindow.ReadOnly") = 'yes' Then lb_selectable = True

// Get the column type of the clicked field
ls_ColumnType = Lower(This.Describe(ls_ClickedColumn + ".ColType"))

ls_protected = This.Describe(ls_ClickedColumn+".Protect")
If Not IsNumber(ls_protected) Then
	// then it is an expression, evaluate it
	// first char is default value, then tab, then expression
	// the whole thing is surrounded by quotes
	ls_protected = This.Describe('Evaluate("' + Mid(ls_protected, &
									Pos(ls_protected, '~t') + 1, &
									Len(ls_protected) - Pos(ls_protected, '~t') - 1) + &
									'", ' + String(row) + ')')
End if

// Only display the clock if the column has edit capablilities
//If This.Describe(ls_ClickedColumn + ".Edit.DisplayOnly") = "yes" OR &
//     This.Describe(ls_ClickedColumn + ".DDDW.AllowEdit") = "no" OR &
//	  This.Describe(ls_ClickedColumn + ".DDLB.AllowEdit") = "no" OR &
//	  This.Describe(ls_ClickedColumn + ".TabSequence") = "0" OR &
//	  ls_protected = '1' Then
//	Return
//End If

end event

on itemfocuschanged;call u_base_dw_ext::itemfocuschanged;This.SelectText(1, 100)
end on

event sqlpreview;call super::sqlpreview;MessageBox("SQLPreview", sqlsyntax)
end event

on getfocus;call u_base_dw_ext::getfocus;This.SelectText(1, 100)
end on

event ue_retrieve;call super::ue_retrieve;
DataWindowChild	ldwc_plant

This.SetTransObject(sqlca)

This.GetChild('plant', ldwc_plant)
ldwc_plant.SetTransObject(SQLCA)
ldwc_plant.Retrieve()
//iw_frame.iu_netwise_data.nf_GetLocations(ldwc_plant)
end event

type gb_display from groupbox within w_pas_manual_production
integer x = 2395
integer y = 339
integer width = 424
integer height = 304
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long textcolor = 33554432
long backcolor = 12632256
string text = "Display"
end type

