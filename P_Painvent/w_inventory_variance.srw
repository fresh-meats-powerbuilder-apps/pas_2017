HA$PBExportHeader$w_inventory_variance.srw
forward
global type w_inventory_variance from w_base_sheet_ext
end type
type st_inventory_date from statictext within w_inventory_variance
end type
type em_variance from editmask within w_inventory_variance
end type
type dw_inventory_variance from u_base_dw_ext within w_inventory_variance
end type
type rb_variances from radiobutton within w_inventory_variance
end type
type rb_all_products from radiobutton within w_inventory_variance
end type
type rb_adjust from radiobutton within w_inventory_variance
end type
type rb_update from radiobutton within w_inventory_variance
end type
type ddlb_date from dropdownlistbox within w_inventory_variance
end type
type st_variance from statictext within w_inventory_variance
end type
type gb_options from groupbox within w_inventory_variance
end type
type gb_display from groupbox within w_inventory_variance
end type
type st_1 from statictext within w_inventory_variance
end type
type st_2 from statictext within w_inventory_variance
end type
type sle_transmission from singlelineedit within w_inventory_variance
end type
type dw_plant from u_plant within w_inventory_variance
end type
end forward

global type w_inventory_variance from w_base_sheet_ext
integer x = 411
integer y = 388
integer width = 3145
integer height = 1564
string title = "Inventory Variance"
long backcolor = 67108864
st_inventory_date st_inventory_date
em_variance em_variance
dw_inventory_variance dw_inventory_variance
rb_variances rb_variances
rb_all_products rb_all_products
rb_adjust rb_adjust
rb_update rb_update
ddlb_date ddlb_date
st_variance st_variance
gb_options gb_options
gb_display gb_display
st_1 st_1
st_2 st_2
sle_transmission sle_transmission
dw_plant dw_plant
end type
global w_inventory_variance w_inventory_variance

type prototypes

end prototypes

type variables
Boolean                ib_SaveInProgress, &
                            ib_re_inquire 

Int		ii_update_rec_occurs, &
                            ii_CommHnd

String                    is_LocationType, &
                            is_Option

u_pas203              iu_pas203
u_ws_pas1		iu_ws_pas1

s_error                  istr_error_info

DataWindowChild   idwc_plant, &
                            idwc_source_plant
                           
                           


end variables

forward prototypes
public subroutine wf_sort ()
public function boolean wf_update ()
public function boolean wf_retrieve ()
public subroutine wf_filter ()
end prototypes

public subroutine wf_sort ();dw_inventory_variance.SetReDraw( False )
dw_inventory_variance.Sort()
wf_filter()
dw_inventory_variance.ScrollToRow(1)
dw_inventory_variance.SetReDraw( True )

end subroutine

public function boolean wf_update ();boolean			lb_Return, &
					lb_NoMessageBox

integer			li_rc

character		lc_Plant[3]

string			ls_Plant, &
					ls_UpdateSource, &
					ls_Tab, &
					ls_InventoryDate, &
					ls_ProcedureOption, &
					ls_PassWord, &
					ls_ServerName, &
					ls_temp
					

date				ld_date

IF ib_SaveInProgress THEN
	MessageBox( "Working", is_Option + " in Progress.  Please Wait...", Information! )
	Return( False )
END IF

IF dw_inventory_variance.RowCount( ) < 1 THEN Return( False )

IF rb_adjust.checked THEN
	is_Option = "Adjust"
ELSE
	is_Option = "Update"
End If

If dw_inventory_variance.GetItemNumber(1, "plant_inv_total") = 0 Then
	MessageBox(is_Option, is_Option + " cannot be completed because all material values are zero.")
	Return False
End if
ls_Tab	= "~t"
lc_Plant = dw_plant.uf_Get_Plant_Code()
//ld_date	= date(em_inventory_date.text)
ld_date	= date(ddlb_date.text)
If ld_Date <> Today() Then
	MessageBox("Save", "Save can only be done on current day.")
	return False
End if
ls_Plant	= lc_Plant

//IF trim(ls_Plant) = "" OR NOT IsDate(em_inventory_date.text) THEN
IF trim(ls_Plant) = "" OR NOT IsDate(ddlb_date.text) THEN
	Return( False )
ELSE	
	IF trim(is_LocationType) = "" THEN Return( False )

	If sle_transmission.Text = 'No' Then
		If MessageBox("Transmission", "Transmission is not complete.  Do you want to continue?", &
			Exclamation!, YesNo!, 2) = 2 Then Return False
		// Set this do stop further messageboxes from appearing
		lb_NoMessageBox = True
	End if
		

//	CHOOSE CASE is_LocationType	
//		CASE 'W', 'X', 'Y', 'T'
//			ls_ProcedureOption = 'F'
//			is_Option = "Update"
//			If Not lb_NoMessageBox Then 
//				If MessageBox(is_Option, "Are you sure you want to " + is_Option + "?", &
//						Question!, YesNo!) = 2 Then return False
//			End if
//		CASE ELSE
			IF rb_adjust.checked THEN
				ls_ProcedureOption = 'A'
				is_Option = "Adjust"
				If Not lb_NoMessageBox Then 
					If MessageBox(is_Option, "You are about to " + is_Option + &
					".  Do you want to continue?", &
					StopSign!, YesNo!) = 2 Then return False
				End if

			ELSE
				ls_ProcedureOption = 'L'
				is_Option = "Update"
				If Not lb_NoMessageBox Then 
					If MessageBox(is_Option, "Are you sure you want to " + is_Option + "?", &
									Question!, YesNo!) = 2 Then return False
				End if
			END IF
//	END CHOOSE
	ls_InventoryDate = String(ld_date, 'yyyy-mm-dd')
END IF

istr_error_info.se_function_name		= "wf_update"
istr_error_info.se_event_name			= ""
istr_error_info.se_procedure_name	= "nf_pasp21br"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF

IF ii_CommHnd < 1 THEN 
	ii_CommHnd = iu_Pas203.nf_Get_Async_CommHandle()
end if

ls_UpdateSource	= ls_Plant + ls_Tab + ls_InventoryDate + ls_Tab + ls_ProcedureOption
//lb_Return			= iu_pas203.nf_upd_invent_var( istr_error_info, &
//																	ls_UpdateSource, &
//																	ii_commhnd, &
//																	True)

lb_Return			= iu_ws_pas1.nf_pasp21fr( istr_error_info, &
																	ls_UpdateSource)
																	
If lb_return Then
	MessageBox( is_Option, is_option + " Successfully Completed." )
End if
ib_re_inquire = True
wf_retrieve()
//IF lb_Return THEN 
//	Timer(1, This)
//	iw_frame.SetMicroHelp("Updating the Database...")
//
//	This.Pointer = 'async.cur'
//	dw_inventory_variance.Modify("DataWindow.Pointer = 'async.cur'")
//	dw_plant.Modify("DataWindow.Pointer = 'async.cur'")
//	em_variance.Pointer = 'async.cur'
//	st_inventory_date.Pointer = 'async.cur'
//	st_variance.Pointer = 'async.cur'
//	rb_adjust.Pointer = 'async.cur'
//	rb_update.Pointer = 'async.cur'
//	rb_all_products.Pointer = 'async.cur'
//	rb_variances.Pointer = 'async.cur'
//
//	ib_saveinprogress = True
//END IF

Return( lb_Return )
end function

public function boolean wf_retrieve ();int				li_rc

long				ll_row, &
					ll_rowcount, &
					ll_value 

character		lc_plant[3], &
					lc_inv_date[10]
Char					lc_Transmission

boolean			lb_return

string			ls_input, &
					ls_source, &
					ls_tab,&
					ls_Transmission

Call w_base_sheet::closequery
IF Message.ReturnValue = 1 THEN Return False

IF NOT ib_re_inquire THEN
	OpenWithParm(w_inventory_variance_inq, This)
	If Message.StringParm <> 'Y' Then return False
ELSE
	ib_re_inquire = False
END IF

dw_inventory_variance.Reset()
lc_Plant 			= dw_plant.uf_Get_Plant_Code()
is_LocationType	= iw_frame.iu_netwise_data.nf_getlocationtype( lc_Plant )

//CHOOSE CASE is_LocationType	
//	CASE 'W', 'X', 'Y', 'T'
//		gb_options.visible 	= False
//		rb_adjust.visible = False
//		rb_update.visible = False
//	CASE ELSE
		gb_options.visible 	= True
		rb_adjust.visible = True
		rb_update.visible = True
//END CHOOSE

iw_frame.SetMicroHelp("Wait.. Inquiring Database")
SetPointer(HourGlass!)

sle_transmission.Text = 'No'

lc_inv_date	= string(Date(ddlb_date.text), "yyyy-mm-dd")

istr_error_info.se_function_name		= "wf_retrieve"
istr_error_info.se_event_name			= ""
istr_error_info.se_procedure_name	= "nf_pasp21br"
istr_error_info.se_message				= Space(70)

IF Not IsValid( iu_pas203 ) THEN
	iu_pas203	=  CREATE u_pas203
END IF

ls_tab = "~t"

ls_input		= lc_plant + ls_tab + lc_inv_date + ls_tab
//lb_return	= iu_pas203.nf_inq_invent_var( istr_Error_Info, &
//														ls_input, &
//														lc_Transmission, &
//														ls_source )
ls_Transmission = lc_Transmission
lb_return	= iu_ws_pas1.nf_pasp20fr( istr_Error_Info, &
														ls_input, &
														ls_Transmission, &
														ls_source )
 
 lc_Transmission = ls_Transmission
 
This.SetRedraw( False )
IF lb_return THEN
	If lc_Transmission = 'Y' Then 
		sle_transmission.Text = 'Yes'
	End if

	ll_value = dw_inventory_variance.ImportString(ls_source)
	CHOOSE CASE ll_value 
		CASE 0	//	End of file, too many rows
			MessageBox( "Import Error", "End of file, too many rows." )
			lb_return = False
		CASE -2	//	Not enough columns
			MessageBox( "Import Error", "Not enough columns." )
			lb_return = False
		CASE -3	//	Invalid argument
			MessageBox( "Import Error", "Invalid argument." )
			lb_return = False
		CASE -4 	//	Invalid input
			MessageBox( "Import Error", "Invalid input." )
			lb_return = False
		CASE ELSE
			dw_inventory_variance.ResetUpdate()
			IF dw_inventory_variance.RowCount() > 0 THEN
				wf_sort()
			END IF
			lb_return = True
	END CHOOSE
END IF

SetRedraw( True )
Return( lb_return )
 
end function

public subroutine wf_filter ();long			ll_RowCount

string		ls_filter

Decimal		ld_variance

IF rb_variances.Checked THEN
	em_variance.visible = True	
	st_variance.visible = True
	em_variance.getdata(ld_variance)
	ls_filter = "((comp_variance * 100) > " + &
		string(ld_variance) + ")"

//	ls_filter = &
//		"(plant_invt <> pa_invt) AND " + &
//		"(comp_variance  > " + &
//		string(abs(dec(em_variance.text))/100) + ")"
ELSEIF rb_all_products.Checked THEN
	em_variance.visible = False
	st_variance.visible = False
	ls_filter = ""
END IF

dw_inventory_variance.SetFilter(ls_filter)
dw_inventory_variance.Filter()
dw_inventory_variance.SetSort("fab_product_code, product_state, product_status, plant_invt, pa_invt")
dw_inventory_variance.Sort()

ll_RowCount = dw_inventory_variance.RowCount()
SetMicroHelp(String(ll_RowCount) + " rows displayed")

end subroutine

event timer;call super::timer;//boolean		lb_return
//
//integer		li_rc
//
//string		ls_update_source
//
//If iu_Pas203.nf_Async_Complete( ii_CommHnd ) Then
//
//	lb_return = iu_pas203.nf_upd_invent_var( istr_error_info, &
//															ls_update_source, &
//															ii_commhnd, &
//															False)
//	lb_return = iu_ws_pas1.nf_pasp21fr( istr_error_info, &
//															ls_update_source)
//	Timer(0, This)
//	ib_saveinprogress = False
//
//	This.Pointer = ''
//	dw_inventory_variance.Modify("DataWindow.Pointer = ''")
//	dw_plant.Modify("DataWindow.Pointer = ''")
//	em_variance.Pointer = ''
//	st_inventory_date.Pointer = ''
//	st_variance.Pointer = ''
//	rb_adjust.Pointer = ''
//	rb_update.Pointer = ''
//	rb_all_products.Pointer = ''
//	rb_variances.Pointer = ''
//
//	If lb_return Then
//		MessageBox( is_Option, is_option + " Successfully Completed." )
//	End if
//	ib_re_inquire = True
//	wf_retrieve()
////ELSE
////	Timer(0, This)
////	ib_saveinprogress = False
////	MessageBox( is_Option, is_option + " Error." )
////END IF
//END IF
//
//
end event

on deactivate;call w_base_sheet_ext::deactivate;iw_frame.im_menu.mf_Enable('m_new')
iw_frame.im_menu.mf_Enable('m_delete')
iw_frame.im_menu.mf_Enable('m_addrow')
iw_frame.im_menu.mf_Enable('m_deleterow')

end on

event close;call super::close;IF ii_commhnd > 0 THEN	iu_Pas203.nf_Release_CommHandle( ii_CommHnd ) 

IF IsValid( iu_pas203 ) THEN	DESTROY iu_pas203

IF IsValid( iu_ws_pas1 ) THEN	DESTROY iu_ws_pas1


end event

event ue_postopen;call super::ue_postopen;If Not IsValid(iu_pas203) Then
	iu_pas203	=  CREATE u_pas203
	If Message.ReturnValue = -1 Then Close(This)
End If
This.PostEvent("ue_query")

iu_ws_pas1  = Create u_ws_pas1
end event

on closequery;call w_base_sheet_ext::closequery;If KeyDown(KeyShift!) And KeyDown(KeyControl!) Then
	Message.ReturnValue = 0
	return
End if
IF ib_saveinprogress THEN
	MessageBox( "Working", "Window cannot be Closed. "+ is_Option + " in Progress.  Please Wait.", Information! )
	Message.ReturnValue = 1
	Return
END IF


end on

on ue_query;call w_base_sheet_ext::ue_query;wf_retrieve()

end on

event open;call super::open;istr_error_info.se_app_name 		= "Pas"
istr_error_info.se_window_name 	= "w_inventory_variance"
istr_error_info.se_user_id 		= sqlca.userid

end event

event resize;call super::resize;integer li_x		= 98
integer li_y		= 378

//dw_inventory_variance.width	= width - (50 + li_x)
//dw_inventory_variance.height	= height - (125 + li_y)

if il_BorderPaddingWidth >  li_x Then
                li_x = il_BorderPaddingWidth
end if

if il_BorderPaddingHeight > li_y Then
                li_y = il_BorderPaddingHeight
End If


dw_inventory_variance.width	= width -  li_x
dw_inventory_variance.height	= height -  li_y
end event

on w_inventory_variance.create
int iCurrent
call super::create
this.st_inventory_date=create st_inventory_date
this.em_variance=create em_variance
this.dw_inventory_variance=create dw_inventory_variance
this.rb_variances=create rb_variances
this.rb_all_products=create rb_all_products
this.rb_adjust=create rb_adjust
this.rb_update=create rb_update
this.ddlb_date=create ddlb_date
this.st_variance=create st_variance
this.gb_options=create gb_options
this.gb_display=create gb_display
this.st_1=create st_1
this.st_2=create st_2
this.sle_transmission=create sle_transmission
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_inventory_date
this.Control[iCurrent+2]=this.em_variance
this.Control[iCurrent+3]=this.dw_inventory_variance
this.Control[iCurrent+4]=this.rb_variances
this.Control[iCurrent+5]=this.rb_all_products
this.Control[iCurrent+6]=this.rb_adjust
this.Control[iCurrent+7]=this.rb_update
this.Control[iCurrent+8]=this.ddlb_date
this.Control[iCurrent+9]=this.st_variance
this.Control[iCurrent+10]=this.gb_options
this.Control[iCurrent+11]=this.gb_display
this.Control[iCurrent+12]=this.st_1
this.Control[iCurrent+13]=this.st_2
this.Control[iCurrent+14]=this.sle_transmission
this.Control[iCurrent+15]=this.dw_plant
end on

on w_inventory_variance.destroy
call super::destroy
destroy(this.st_inventory_date)
destroy(this.em_variance)
destroy(this.dw_inventory_variance)
destroy(this.rb_variances)
destroy(this.rb_all_products)
destroy(this.rb_adjust)
destroy(this.rb_update)
destroy(this.ddlb_date)
destroy(this.st_variance)
destroy(this.gb_options)
destroy(this.gb_display)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.sle_transmission)
destroy(this.dw_plant)
end on

event activate;call super::activate;iw_frame.im_menu.mf_Disable('m_new')
iw_frame.im_menu.mf_Disable('m_delete')
iw_frame.im_menu.mf_Disable('m_addrow')
iw_frame.im_menu.mf_Disable('m_deleterow')

end event

event ue_fileprint;call super::ue_fileprint;DataStore	lds_print

If not ib_print_ok Then return

lds_print = Create u_print_datastore
lds_print.DataObject = 'd_inventory_variance_print'
dw_inventory_variance.ShareData(lds_print)

lds_print.Object.plant_t.Text = 'Plant:  ' + dw_plant.uf_get_Plant_Code() + '  ' + &
	dw_plant.uf_get_Plant_Descr()
lds_print.Print()


end event

event ue_get_data;call super::ue_get_data;Choose Case as_value
	Case 'Plant' 
		Message.StringParm = dw_plant.GetItemString(1, 'location_code')
End Choose

end event

event ue_set_data;call super::ue_set_data;Choose Case as_data_item
	Case 'Plant' 
		dw_plant.SetItem(1, 'location_code', as_value)
End Choose
end event

type st_inventory_date from statictext within w_inventory_variance
integer x = 9
integer y = 116
integer width = 251
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "Inv Date:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_variance from editmask within w_inventory_variance
event ue_enchange pbm_enchange
integer x = 1065
integer y = 108
integer width = 247
integer height = 92
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
alignment alignment = right!
borderstyle borderstyle = stylelowered!
string mask = "##0"
boolean spin = true
double increment = 1
string minmax = "0~~100"
end type

on ue_enchange;wf_sort()
end on

on constructor;text = "10"
end on

type dw_inventory_variance from u_base_dw_ext within w_inventory_variance
integer x = 23
integer y = 236
integer width = 3049
integer height = 1172
integer taborder = 0
string dataobject = "d_inventory_variance"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type rb_variances from radiobutton within w_inventory_variance
integer x = 1943
integer y = 56
integer width = 343
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "Variances"
boolean checked = true
end type

on clicked;//wf_sort()
em_variance.visible = True	
st_variance.visible = True

wf_filter()
end on

type rb_all_products from radiobutton within w_inventory_variance
integer x = 1943
integer y = 124
integer width = 407
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "All Products"
end type

on clicked;//wf_sort()
em_variance.visible = False
st_variance.visible = False

wf_filter()

end on

type rb_adjust from radiobutton within w_inventory_variance
integer x = 2432
integer y = 56
integer width = 247
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "Adjust"
boolean checked = true
end type

type rb_update from radiobutton within w_inventory_variance
integer x = 2432
integer y = 124
integer width = 279
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "Update"
end type

type ddlb_date from dropdownlistbox within w_inventory_variance
integer x = 265
integer y = 104
integer width = 389
integer height = 224
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
boolean sorted = false
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on constructor;string		ls_today, &
				ls_yesterday

ls_today			= String(today(), "mm/dd/yyyy" )
ls_yesterday	= String( RelativeDate ( today(), -1 ) , "mm/dd/yyyy" )

AddItem ( ls_today )
AddItem ( ls_yesterday )
SelectItem ( 1 )


end on

type st_variance from statictext within w_inventory_variance
integer x = 677
integer y = 116
integer width = 366
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "Variance (%) >"
alignment alignment = right!
boolean focusrectangle = false
end type

type gb_options from groupbox within w_inventory_variance
integer x = 2391
integer width = 352
integer height = 212
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "Options"
end type

type gb_display from groupbox within w_inventory_variance
integer x = 1906
integer width = 453
integer height = 212
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
string text = "Display"
end type

type st_1 from statictext within w_inventory_variance
integer x = 1330
integer y = 100
integer width = 370
integer height = 60
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "Transmission"
boolean focusrectangle = false
end type

type st_2 from statictext within w_inventory_variance
integer x = 1330
integer y = 152
integer width = 261
integer height = 68
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean enabled = false
string text = "Complete"
boolean focusrectangle = false
end type

type sle_transmission from singlelineedit within w_inventory_variance
integer x = 1710
integer y = 112
integer width = 174
integer height = 88
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
long backcolor = 67108864
boolean autohscroll = false
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type dw_plant from u_plant within w_inventory_variance
integer x = 91
integer y = 12
integer width = 1586
integer taborder = 10
end type

event constructor;call super::constructor;ib_Updateable = False
ib_protected = True
TriggerEvent( "ue_insertrow" )

end event

