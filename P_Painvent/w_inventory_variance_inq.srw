HA$PBExportHeader$w_inventory_variance_inq.srw
forward
global type w_inventory_variance_inq from w_base_response_ext
end type
type st_1 from statictext within w_inventory_variance_inq
end type
type ddlb_date from dropdownlistbox within w_inventory_variance_inq
end type
type dw_plant from u_plant within w_inventory_variance_inq
end type
end forward

global type w_inventory_variance_inq from w_base_response_ext
integer x = 635
integer y = 724
integer width = 1664
integer height = 380
string title = "Inventory Variance Inquiry"
long backcolor = 67108864
st_1 st_1
ddlb_date ddlb_date
dw_plant dw_plant
end type
global w_inventory_variance_inq w_inventory_variance_inq

type variables
w_inventory_variance    iw_Parent_Window
 
datawindowchild            idwc_master, &
                                   idwc_slave
end variables

event open;call super::open;iw_Parent_Window = Message.PowerObjectParm
If Not IsValid(iw_Parent_Window) Then
	Close(This)
	return 
End if

This.Title = iw_ParentWindow.Title + " Inquire"

iw_frame.SetMicroHelp("Ready")

If Not iw_frame.iu_string.nf_IsEmpty(iw_parent_window.dw_plant.uf_Get_Plant_Code()) Then
	dw_plant.uf_Set_Plant_Code(iw_parent_window.dw_plant.uf_Get_Plant_Code() )
End If

//em_inventory_date.text = iw_parentWindow.em_inventory_date.text
ddlb_date.text = iw_parent_Window.ddlb_date.text 
						

end event

on w_inventory_variance_inq.create
int iCurrent
call super::create
this.st_1=create st_1
this.ddlb_date=create ddlb_date
this.dw_plant=create dw_plant
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.ddlb_date
this.Control[iCurrent+3]=this.dw_plant
end on

on w_inventory_variance_inq.destroy
call super::destroy
destroy(this.st_1)
destroy(this.ddlb_date)
destroy(this.dw_plant)
end on

event ue_base_cancel;call super::ue_base_cancel;CloseWithReturn(This, "N")
end event

event ue_base_ok;call super::ue_base_ok;string		ls_plant
Long			ll_row

IF dw_plant.AcceptText() = -1 THEN return

ls_plant =  dw_plant.GetItemString( 1, "location_code" ) 
IF iw_frame.iu_string.nf_IsEmpty(ls_plant) THEN
	dw_plant.SetColumn("location_code")
	dw_plant.SetFocus()
	dw_plant.SelectText(1, Len(ls_plant))
	iw_frame.SetMicroHelp("Plant Code is a required field")
	Return
END IF

IF iw_parent_window.dw_plant.uf_Set_Plant_Code( dw_plant.nf_Get_Plant_Code()) <> 0 THEN Return
//IF NOT iw_parent_window.dw_plant.uf_SetPlant_Desc( dw_plant.uf_Get_Plant_Descr()) THEN Return

//iw_parent_window.em_inventory_date.text = em_inventory_date.text
iw_parent_window.ddlb_date.text = ddlb_date.text

CloseWithReturn(This, "Y")
end event

event ue_postopen;call super::ue_postopen;String			ls_temp
u_string_functions		lu_string

iw_parent_window.Event ue_Get_Data('Plant')
ls_temp = Message.StringParm

If Not lu_string.nf_IsEmpty(ls_temp) Then
	dw_plant.uf_set_plant_code(ls_temp)
End If
end event

type cb_base_help from w_base_response_ext`cb_base_help within w_inventory_variance_inq
integer x = 1303
integer y = 140
integer width = 251
integer taborder = 40
end type

type cb_base_cancel from w_base_response_ext`cb_base_cancel within w_inventory_variance_inq
integer x = 1010
integer y = 140
integer width = 251
integer taborder = 30
end type

type cb_base_ok from w_base_response_ext`cb_base_ok within w_inventory_variance_inq
integer x = 718
integer y = 140
integer width = 251
integer taborder = 20
end type

type st_1 from statictext within w_inventory_variance_inq
integer x = 9
integer y = 124
integer width = 242
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 67108864
boolean enabled = false
string text = "Inv. Date"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_date from dropdownlistbox within w_inventory_variance_inq
integer x = 270
integer y = 112
integer width = 389
integer height = 224
integer taborder = 10
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean sorted = false
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

on constructor;string		ls_today, &
				ls_yesterday

ls_today			= String(today(), "mm/dd/yyyy" )
ls_yesterday	= String( RelativeDate ( today(), -1 ) , "mm/dd/yyyy" )

AddItem ( ls_today )
AddItem ( ls_yesterday )
SelectItem ( 1 )


end on

type dw_plant from u_plant within w_inventory_variance_inq
integer x = 101
integer y = 12
integer width = 1477
end type

event constructor;call super::constructor;ib_updateable = True
end event

