HA$PBExportHeader$u_netwise_dw2.sru
forward
global type u_netwise_dw2 from datawindow
end type
end forward

global type u_netwise_dw2 from datawindow
integer width = 686
integer height = 400
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
event ue_postconstructor ( )
event ue_retrieve ( )
end type
global u_netwise_dw2 u_netwise_dw2

type variables
PROTECTED:
BOOLEAN ib_DB_Reconnectable

PRIVATE:
BOOLEAN ib_DBDead
INTEGER ii_retry_count
end variables

event ue_postconstructor();// Do stuff after the constructor
end event

event ue_retrieve();//This is where you put SetTransObject and Retrieve
end event

event constructor;This.event ue_retrieve()
This.Postevent("ue_postconstructor")
end event

event dberror;transaction 		lu_transaction
ClassDefinition 	lu_ClassDef


if (sqldbcode = 10005 or sqldbcode = 10025) &
	and not ib_DBDead &
		and ii_retry_count < 2  &
			and ib_DB_Reconnectable then
			
	ib_DBDead = true
	ii_retry_count += 1
	
	lu_ClassDef = sqlca.classdefinition
	do while IsValid(lu_ClassDef)
		if lu_ClassDef.name = "u_netwise_transaction" then
			sqlca.postevent("ue_reconnect")
			this.postevent("ue_retrieve",0,0)
			exit
		end if
		
		lu_ClassDef = lu_ClassDef.Ancestor		
	loop
	
	ib_DBDead = false
else
	messagebox("Datawindow error in object '" + this.classname( ) + "'" , &
					"A Database error occurred.~r~n Error Code " + &
					string(sqldbcode) + &
					"~r~n Error Message "  + &
					sqlerrtext)	

end if

return 1 // Do not display default error message.
end event

on u_netwise_dw2.create
end on

on u_netwise_dw2.destroy
end on

