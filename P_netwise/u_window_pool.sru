HA$PBExportHeader$u_window_pool.sru
forward
global type u_window_pool from nonvisualobject
end type
type st_window_list from structure within u_window_pool
end type
end forward

type st_window_list from structure
	string		s_window_type
	w_netwise_sheet_ole		w_window
end type

global type u_window_pool from nonvisualobject autoinstantiate
end type

type variables
private:
st_window_list ist_WindowList[]

end variables

forward prototypes
public subroutine uf_remove_pooled_window (long al_window_index)
public subroutine uf_close_all_windows ()
public subroutine uf_add_pooled_window (w_netwise_sheet_ole aw_window)
public function window uf_get_pooled_window (string as_window_name, long a_param)
public function window uf_get_pooled_window (string as_window_name, string a_param)
public function window uf_get_pooled_window (string as_window_name, window a_param)
end prototypes

public subroutine uf_remove_pooled_window (long al_window_index);if al_window_index >= LowerBound(ist_WindowList) &
 and al_window_index <= UpperBound(ist_WindowList) then
	ist_WindowList[al_window_index].s_window_type 		= ""
end if
end subroutine

public subroutine uf_close_all_windows ();long ll_NextItem

//Find an unused window that is the same type
for ll_NextItem=LowerBound(ist_WindowList)  to UpperBound(ist_WindowList)
	if ist_WindowList[ll_NextItem].s_window_type > "" then
			ist_WindowList[ll_NextItem].w_window.wf_exit_pool()
	end if
next
end subroutine

public subroutine uf_add_pooled_window (w_netwise_sheet_ole aw_window);long ll_NextItem

if aw_window.wf_is_poolable() then
	ll_NextItem = LowerBound(ist_WindowList) - 1
	//Find a open position to add a new window.
	do until ll_NextItem > UpperBound(ist_WindowList)
		ll_NextItem += 1
	
		if ll_NextItem > UpperBound(ist_WindowList) then 
			exit
		elseif ist_WindowList[ll_NextItem].s_window_type = "" then
			exit
		end if
	loop
	
	ist_WindowList[ll_NextItem].w_window			= aw_window
	ist_WindowList[ll_NextItem].s_window_type 	= aw_window.classname()
	ist_WindowList[ll_NextItem].w_window.ii_ID 	= ll_NextItem
end if



end subroutine

public function window uf_get_pooled_window (string as_window_name, long a_param);long ll_NextItem
w_netwise_sheet_ole w_New_Window

//Find an unused window that is the same type
for ll_NextItem=LowerBound(ist_WindowList)  to UpperBound(ist_WindowList)
	if ist_WindowList[ll_NextItem].s_window_type = as_window_name then
		if ist_WindowList[ll_NextItem].w_window.ib_pooled then
			// 01/24/2005 ** Elwin McKernan ** unpool a window before showing it. 
			ist_WindowList[ll_NextItem].w_window.ib_pooled = false 
			// 
			
			Message.longparm = a_param
			ist_WindowList[ll_NextItem].w_window.windowstate = Normal!
			ist_WindowList[ll_NextItem].w_window.event open( )
			return ist_WindowList[ll_NextItem].w_window
		end if
	end if
next

OpenSheetWithParm(w_New_Window, a_param, as_window_name, iw_frame,0, iw_frame.im_menu.iao_ArrangeOpen)

if w_New_Window.wf_is_poolable() then
	ll_NextItem = LowerBound(ist_WindowList) - 1
	//Find a open position to add a new window.
	do until ll_NextItem > UpperBound(ist_WindowList)
		ll_NextItem += 1
	
		if ll_NextItem > UpperBound(ist_WindowList) then 
			exit
		elseif ist_WindowList[ll_NextItem].s_window_type = "" then
			exit
		end if
	loop

	ist_WindowList[ll_NextItem].w_window			= w_New_Window
	ist_WindowList[ll_NextItem].s_window_type 	= as_window_name
	ist_WindowList[ll_NextItem].w_window.ii_ID 	= ll_NextItem
end if

return w_New_Window


end function

public function window uf_get_pooled_window (string as_window_name, string a_param);long ll_NextItem
w_netwise_sheet_ole w_New_Window

//Find an unused window that is the same type
for ll_NextItem=LowerBound(ist_WindowList)  to UpperBound(ist_WindowList)
	if ist_WindowList[ll_NextItem].s_window_type = as_window_name then
		if ist_WindowList[ll_NextItem].w_window.ib_pooled then
			// 01/24/2005 ** Elwin McKernan ** unpool a window before showing it. 
			ist_WindowList[ll_NextItem].w_window.ib_pooled = false 
			// 
			Message.stringparm = a_param
			ist_WindowList[ll_NextItem].w_window.windowstate = Normal!
			ist_WindowList[ll_NextItem].w_window.event open( )
			return ist_WindowList[ll_NextItem].w_window
		end if
	end if
next

OpenSheetWithParm(w_New_Window, a_param, as_window_name, iw_frame,0, iw_frame.im_menu.iao_ArrangeOpen)

if w_New_Window.wf_is_poolable() then
	ll_NextItem = LowerBound(ist_WindowList) - 1
	//Find a open position to add a new window.
	do until ll_NextItem > UpperBound(ist_WindowList)
		ll_NextItem += 1
	
		if ll_NextItem > UpperBound(ist_WindowList) then 
			exit
		elseif ist_WindowList[ll_NextItem].s_window_type = "" then
			exit
		end if
	loop

	ist_WindowList[ll_NextItem].w_window			= w_New_Window
	ist_WindowList[ll_NextItem].s_window_type 	= as_window_name
	ist_WindowList[ll_NextItem].w_window.ii_ID 	= ll_NextItem
end if

return w_New_Window

end function

public function window uf_get_pooled_window (string as_window_name, window a_param);long ll_NextItem
w_netwise_sheet_ole w_New_Window

//Find an unused window that is the same type
for ll_NextItem=LowerBound(ist_WindowList)  to UpperBound(ist_WindowList)
	if ist_WindowList[ll_NextItem].s_window_type = as_window_name then
		if ist_WindowList[ll_NextItem].w_window.ib_pooled then
			// 01/24/2005 ** Elwin McKernan ** unpool a window before showing it. 
			ist_WindowList[ll_NextItem].w_window.ib_pooled = false 
			// 
			Message.PowerObjectParm = a_param
			ist_WindowList[ll_NextItem].w_window.windowstate = Normal!
			ist_WindowList[ll_NextItem].w_window.event open( )
			return ist_WindowList[ll_NextItem].w_window
		end if
	end if
next

OpenSheetWithParm(w_New_Window, a_param, as_window_name, iw_frame,0, iw_frame.im_menu.iao_ArrangeOpen)

if w_New_Window.wf_is_poolable() then
	ll_NextItem = LowerBound(ist_WindowList) - 1
	//Find a open position to add a new window.
	do until ll_NextItem > UpperBound(ist_WindowList)
		ll_NextItem += 1
	
		if ll_NextItem > UpperBound(ist_WindowList) then 
			exit
		elseif ist_WindowList[ll_NextItem].s_window_type = "" then
			exit
		end if
	loop

	ist_WindowList[ll_NextItem].w_window			= w_New_Window
	ist_WindowList[ll_NextItem].s_window_type 	= as_window_name
	ist_WindowList[ll_NextItem].w_window.ii_ID 	= ll_NextItem
end if

return w_New_Window
end function

on u_window_pool.create
call super::create
TriggerEvent( this, "constructor" )
end on

on u_window_pool.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

